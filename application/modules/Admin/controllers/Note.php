﻿<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Note extends MX_Controller {
  private $staff_permission = array();
  private $access_permission = true;
  public function __construct() {

    parent:: __construct();
    
    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->library('table');
    $this->load->library('pagination');
    $this->load->model('common_model');
    $this->load->model('login_model');
    $this->load->model('note_model');
    $this->load->helper(array('common_helper'));
    $this->load->helper(array('url'));
    $this->load->helper(array('form')); 
    $user_id = $this->session->userdata('logged_ins')['id'];
    if(empty($user_id)) {
      redirect('admin/login');
    }
    if ($this->session->userdata('logged_ins')['user_role'] == 5) {
      $uid = $this->session->userdata('logged_ins')['id'];
      $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
      if (!empty($permission_list)) 
      {
       foreach ($permission_list as $k => $v) 
       {
        $this->staff_permission[$v['permission_id']] = $v;
      }
    }
  }
}
public function permission_access($permission_id=2)
{
  if ($this->session->userdata('logged_ins')['user_role'] != 1) 
  {
   if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
   {
    $this->access_permission = false;
  }
}
}
/*show all the notes*/
public function note_list()
{
  $data['menuactive'] = $this->uri->segment(2); 
  $data['note_list'] = $this->note_model->getAllnotes();
  $data['care_giver'] = $this->note_model->getAllwhere("cp_users",array("user_role" => 4));
  $data['staff_permission'] = $this->staff_permission;
  $this->permission_access(2);
  if ($this->access_permission) {
    $this->template->set('title', 'Note');
    $this->template->load('admin_dashboard_layout', 'contents', 'notes_list',$data);
  } else {
    $this->template->set('title', 'Note - Staff');
    $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
  }
}
/*View single note details*/
public function view_note()
{
 $data['menuactive'] = $this->uri->segment(2);
 $segment = $this->uri->segment("3");
 $data['singledata'] = $this->note_model->getNote($segment);
 $data['care_giver'] = $this->note_model->getAllwhere("cp_users",array("user_role" => 4));
 $data['staff_permission'] = $this->staff_permission;
 $this->permission_access(2);
 if ($this->access_permission) {
  $this->template->set('title', 'Note');
  $this->template->load('admin_dashboard_layout', 'contents', 'view_notedetail',$data);
} else {
  $this->template->set('title', 'Note - Staff');
  $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
}
}
/*Delete note*/
public function delete_note()
{
 $data['menuactive'] = $this->uri->segment(2);
 $segment = $this->uri->segment("3");
 $this->note_model->delete("cp_note",array("note_id" => $segment));
 $this->session->set_flashdata('success', 'Note Successfully deleted');
 redirect('admin/note_list');
}
}    
?>
