<?php 
$i = ++$offset;

 if(!empty($Allappointments)){
	
	 foreach($Allappointments as $appointment){
$category_name = $this->Common_model_new->getCategory($appointment['doctor_category_id']);
	
 ?>
                        <li class="loadData" >                     
       
                            <div class="pro_imgOne" id="myData_<?php echo $i;?>">

<?php if($appointment['doctor_pic']){ ?>							
                                <img src="<?php echo base_url(); ?>uploads/doctor/<?php echo $appointment['doctor_pic']; ?>" class="" alt="">
<?php }else{ ?>
 <img src="<?php echo base_url(); ?>assets/images/default_pro_img.png" class="" alt="">
<?php } ?>								
                            </div>
                            <div class="dr_name">                                
                                <h4><?php echo $appointment['doctor_name']; ?> </h4>
                                <h5><?php echo $category_name; ?></h5>
                            </div>
                            <div class="dr_date">   
                                <h4><?php echo date('j F Y',strtotime($appointment['avdate_date'])); ?></h4>
                                <h5><?php echo $appointment['avtime_text']; ?></h5>
                            </div>
                            <div class="dr_viw">   
                                <div class="btn-group new-music-btn">
								<a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
								<div class="dropdown-menu">
								<a class="dropdown-item" href="<?php echo base_url(); ?>edit_appointment/<?php echo $appointment['dr_appointment_id']; ?>"><i class="fa fa-edit"></i>Edit</a>
                                        <a class="dropdown-item" href="<?php echo base_url(); ?>view_appointment/<?php echo $appointment['dr_appointment_id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i>View</a>
                                        <a class="dropdown-item" onclick="return confirm('Are you sure delete this record?')" href="<?php echo base_url(); ?>delete_appointment/<?php echo $appointment['dr_appointment_id']; ?>" ><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>

										
                                  </div>
                                </div>                               
                            </div>
                        </li>
 <?php }}else{ ?>
 <li>No records found</li>
 <?php } ?>
 
