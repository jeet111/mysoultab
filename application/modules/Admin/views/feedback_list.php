<?php
if ($staff_permission[3]['add_permission'] == 1) {
	?>
	<!-- <div class="bx_user">
		<div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_feature_category">Add Feature Category</a></div>
	</div> -->
	<?php
} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {
	?>
	<!-- <div class="bx_user">
		<div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_feature_category">Add Feature Category</a></div>
	</div> -->
	<?php
}
?>


<div class="container-fluid">
	<div class="dash-counter users-main my-sts-table">
		<div class="row">
			<div class="col-md-12">
				<div class="">
					<div class="card-body">
						<div class="status-cng"></div>
						<?php if ($this->session->flashdata('success')) { ?>
							<div class="alert alert-success message">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<?php echo $this->session->flashdata('success'); ?></div>
							<?php } ?>

							<!-- <div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_test_type">Add test type</a></div> -->

							<?php //echo "<pre>";  print_r($userData); ?>

							<table class="table table-hover tab_comn" id="sampleTable">
								<thead>
									<tr>
										<th>S no.</th>
										<th>Email</th>
										<th>Mobile No.</th>
										<th>How much like</th>
										<th>No of freinds</th>
										<th>Message</th>
										<th>Date time</th>
										<!-- <th>Action</th> -->
									</tr>
								</thead>
								<tbody>
									<?php
									$i = 1;
									foreach ($feedData as $list) {

										?>
										<tr>
											<td><?php echo $i++; ?></td>
											<td><?php echo $list->email; ?></td>
											<td><?php echo $list->mobile; ?></td>
											<td><?php	if ($list->how_muchlike==1) { ?>
												<img class="img-responsive" src="<?php echo base_url();?>assets/images/dissappoint.png">
											<?php	} elseif ($list->how_muchlike==2) { ?>
												<img class="img-responsive" src="<?php echo base_url();?>assets/images/sad.png">
											<?php } else if ($list->how_muchlike==3){ ?>
												<img class="img-responsive" src="<?php echo base_url();?>assets/images/neutral.png">
											<?php }else if ($list->how_muchlike==4){ ?>
												<img class="img-responsive" src="<?php echo base_url();?>assets/images/neutral.png">
											<?php }else{ ?>
												<img class="img-responsive" src="<?php echo base_url();?>assets/images/excellent.png">
												<?php } ?></td>
												<td><?php echo $list->no_people; ?></td>

												<td><?php echo $list->feed_desc; ?></td>
												<td><?php echo $list->created; ?></td>

											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>




		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

		<script type="text/javascript">
			$(document).on('click','.activation_cls',function(){
				var ticket_id = $(this).attr('data-id');
				var user_id = $(this).attr('data-to');	
				$.ajax({
					type: "POST",
					url: '<?php echo base_url(); ?>Admin/Tickes/getTicketData/',
					data: 'ticket_id='+ticket_id+'&user_id='+user_id,
            //dataType: "json",
            success: function(data)
            {
            	getDetail = JSON.parse(data);

            	console.log();


            	var statusHTML = '<ul class="rooms">';
            	$.each(getDetail,function (index, room) {
            		//console.log(room.reply_id );
            		
            		if(room.by_admin=="") {
            			//console.log('user');
            			statusHTML +='<li class="empty">By User: ';
            		}else{
            			//console.log('Admin');
            			statusHTML +='<li class="full">By Admin: ';
            		}
            		statusHTML += room.reply_desc + '</li>';
            	});

            	statusHTML += '</ul>';
            	$('#roomList'+ticket_id).html(statusHTML);

            }
        });
			});	
		</script>