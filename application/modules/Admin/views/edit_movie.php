<style>
.red {
	color: red;
}
.red1 {
	color: red;
}


#progress-wrp {
	border: 1px solid #0099CC;
	padding: 1px;
	position: relative;
	border-radius: 3px;
	margin: 10px;
	text-align: left;
	background: #fff;
	box-shadow: inset 1px 3px 6px rgba(0, 0, 0, 0.12);
}
#progress-wrp .progress-bar {
	height: 20px;
	border-radius: 3px;
	background-color: #f39ac7;
	width: 0;
	box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);
}
#progress-wrp .status {
	top: 3px;
	left: 50%;
	position: absolute;
	display: inline-block;
	color: #000000;
}
</style>
<div class="container-fluid">
	<div class="dash-counter">
		<div class="Schedule_main_one">
			<div class="users-main">
				<?php
				echo "<h2>Edit Movie</h2>";
				?>
				<div id="image_msg"></div>


				<div class="btn_topBack">
					<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">
						Back
					</a>
				</div>

				<form class="form-horizontal add_movie_form" action="" method="post" enctype="multipart/form-data" name="AddUsers" id="movie-form" >
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Title:<span class="field_req">*</span></label>
							<div class="col-md-8">
								<input type="text" placeholder="Movie Title" class="form-control" name="movie_title" id="username" value="<?php echo $editMovie->movie_title; ?>">
								<?php echo form_error('movie_title'); ?>
							</div>
						</div>
					</div>
					<input type="hidden" name="movie_id" id="movie_id" value="<?php echo $this->uri->segment('3'); ?>" >
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Description:<span class="field_req">*</span></label>
							<div class="col-md-8">
								<textarea name="movie_description" id="" class="form-control" placeholder="Movie Description" value="<?php echo set_value('movie_description'); ?>"><?php echo $editMovie->movie_desc; ?></textarea>
								<?php echo form_error('movie_description'); ?>
							</div>
						</div>
					</div>


					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Type:<span class="field_req">*</span></label>
							<div class="col-md-8">

								<select name="movie_type" type="text" class="form-control" >
									<option value="">Select</option>
									<?php foreach($category_name as $category){ ?>
										<option value="<?php echo $category->movie_category_id; ?>" <?php if($category->movie_category_id == $editMovie->category_id){ ?>selected<?php } ?>><?php echo  $category->movie_category_name; ?> </option>
									<?php } ?>
								</select>

								<!-- <input name="music_artist" type="text" placeholder="Music Artist" class="form-control"  id="" value="<?php echo set_value('music_artist'); ?>"> -->
								<?php echo form_error('movie_type'); ?>
							</div>
						</div>
					</div>


					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Artist:<span class="field_req">*</span></label>
							<div class="col-md-8">

				<!-- <select name="music_artist" type="text" class="form-control" >
					<option value="">Select</option>
					<option value='1' <?php if($editMusic->music_artist=='1'){echo 'selected=="selected"';} ?>>Arijit singh</option>
					<option value='2' <?php if($editMusic->music_artist=='2'){echo 'selected=="selected"';} ?>>Diljit dosanjh</option>
					<option value='3' <?php if($editMusic->music_artist=='3'){echo 'selected=="selected"';} ?>>Atif aslam</option>
					<option value='4' <?php if($editMusic->music_artist=='4'){echo 'selected=="selected"';} ?>>Neha kakkar</option>
				</select> -->

				<input name="movie_artist" type="text" placeholder="Movie Artist" class="form-control"  id="" value="<?php echo $editMovie->movie_artist; ?>">
				<?php echo form_error('movie_artist'); ?>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="form-group">
			<label class="control-label col-md-4" for=""> file:<span class="field_req">*</span></label>
			<div class="col-md-8">
				<div class="input-group">
					<span class="input-group-btn">
						<!-- <span class="btn btn-default btn-files"> -->
							<input type="file" id="profile_imagess" class="form-control" name="picture" onchange="loadFile1(event)">
							<!-- </span> -->
						</span>
						<div id="progress-wrp" style="display:none"><div class="progress-bar"></div ><div class="status">0%</div></div>
						<div id="flupmsg"></div>
					</div>
					<div>
						<span id="demo1" style="color: red"></span>
					</div>
					<video width="100%" controls id="dfds">
						<source src="<?php echo base_url(); ?>uploads/movie/<?php echo $editMovie->movie_file;?>" type="video/mp4">

						</video>


						<!-- <img id='img-upload' src="<?php //echo base_url().'uploads/music/defualt_music.png' ?>"> -->



				<!-- <div class="bnt_sub">
					<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit" onclick="showFileSize()">
				</div> -->
			</div>
		</div>
	</div>


	<div class="col-md-8">
		<div class="form-group">
			<label class="control-label col-md-4" for=""> Image:<span class="field_req">*</span></label>
			<div class="col-md-8">
				<div class="input-group">
					<span class="input-group-btn">
						<!-- <span class="btn btn-default btn-files"> -->
							<input type="file" id="profile_images" class="form-control" name="pictureFile" onchange="loadFile(event)">
							<!-- </span> -->
						</span>
						<div id="flupmsgs"></div>
					</div>
					<div>
						<span id="demo" style="color: red"></span>
					</div>


					<?php if($editMovie->movie_image==''){ ?>
						<img id='img-upload' src="<?php echo base_url().'uploads/movie/image/defualt_movie.png' ?>">
					<?php }else{ ?>
						<img id='img-upload' src="<?php echo base_url().'uploads/movie/image/'.$editMovie->movie_image?>">

					<?php } ?>
					<div class="bnt_sub">
						<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit" onclick="showFileSize()">


					</div>
				</div>
			</div>
		</div>



	</form>
</div>
</div>
</div>
</div>

<!--Added by 95 on 5-2-2019 For Doctor category for validation-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>
<script>

	$(".add_movie_form").on('submit',(function(e)
	{
		e.preventDefault();
		var isvalidate=$(".add_movie_form").valid();
		if(isvalidate)
		{
			$("#loders").append('<img id="loading_img" style="width:20px" src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif" />');


			$.ajax({
				url : "<?php echo base_url(); ?>Admin/Movie/ajax_movie_edit",
				type: "POST",
				data: new FormData(this),
				dataType: "json",
				contentType: false,
				cache: false,
				processData:false,
				success: function(data) {
					$("#loading_img").hide();
					if(data.code == 1){
						$("#loading_img").hide();
						$("#sub").prop('disabled', true);

						$("#image_msg").html('<div class="alert alert-success">'+data.message+'<div>');
						setTimeout(function(){


							$("#image_msg").html("");
            //location.reload();
            window.location.href = '<?php echo base_url(); ?>admin/movie_list';
        }, 1000);
					}else{
						$("#loading_img").hide();

						$("#image_msg").html('<div class="alert alert-danger">'+data.message+'<div>');

					}
				}
			});
		}
	}));
	function loadFile (event) {
		var pcFile = $('#profile_images').val().split('\\').pop();
		var pcExt     = pcFile.split('.').pop();
		var output = document.getElementById('img-upload');
		if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"
			|| pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){
			$('#img-upload').show();
		output.src = URL.createObjectURL(event.target.files[0]);
		$('#flupmsgs').html('');
	}else{
		$('#flupmsgs').html('Please select only Image file.');
		$('#img-upload').hide();
	}
};


function loadFile1 (event) {
	var pcFile = $('#profile_imagess').val().split('\\').pop();
	var pcExt     = pcFile.split('.').pop();
	var output = document.getElementById('img-uploads');
	if(pcExt == "mp4" || pcExt == "3gp" || pcExt == "avi" || pcExt == "wmv"){
		$('#img-uploads').show();
		output.src = URL.createObjectURL(event.target.files[0]);
		$('#flupmsg').html('');
	}else{
		$('#flupmsg').html('Please select only vedio file.');
		$('#img-uploads').hide();
	}
};

</script>
<script type="text/javascript">
	$(document).ready(function() {
		$.validator.addMethod("regx", function(value, element, regexpr) {
			return regexpr.test(value);
		}, "Please upload valid extension vedio formate file.");
		$('#movie-form').validate({
			rules: {
				movie_title: 'required',
				movie_description:'required',
				movie_artist:'required',
				movie_type:'required',

			},
			messages: {
				movie_title: 'Please enter movie title.',
				movie_description:'Please enter movie description.',
				movie_artist:'Please enter an artist name.',
				movie_type:'Please select a movie type.'

			}
		});
	});

	$(document).ready(function(){
		var progress_bar_id 		= '#progress-wrp';
		$('#profile_imagess').change(function(e){
			var pcFile = $('#profile_imagess').val().split('\\').pop();
			var pcExt     = pcFile.split('.').pop();
			var output = document.getElementById('img-uploads');
			if(pcExt == "mp4" || pcExt == "3gp" || pcExt == "avi" || pcExt == "wmv"){
				$('#flupmsg').hide();
				$('#progress-wrp').show();


				$(progress_bar_id +" .progress-bar").css("width", "0%");
				$(progress_bar_id + " .status").text("0%");
           // var file = this.files[0];
           var file = this.files[0];
           var form = new FormData();
           form.append('360_image_upload', file);

           $.ajax({
           	url : "<?php echo base_url(); ?>Admin/Movie/image_upload",
           	type: "POST",
           	cache: false,
           	contentType: false,
           	processData: false,
           	data : form,
           	dataType: "json",
           	xhr: function(){
		//upload Progress
		var xhr = $.ajaxSettings.xhr();
		if (xhr.upload) {
			xhr.upload.addEventListener('progress', function(event) {
				var percent = 0;
				var position = event.loaded || event.position;
				var total = event.total;
				if (event.lengthComputable) {
					percent = Math.ceil(position / total * 100);
				}
				//update progressbar
				$(progress_bar_id +" .progress-bar").css("width", + percent +"%");
				$(progress_bar_id + " .status").text(percent +"%");
			}, true);
		}
		return xhr;
	},
	success: function(data){



		$("#progress-wrp").hide();
		$("#dfds").show();
		var fileInput = document.getElementById('profile_imagess');
		var fileUrl = window.URL.createObjectURL(fileInput.files[0]);
		$("#dfds").attr("src", fileUrl);

	}
});
       }
   });
	});
</script>