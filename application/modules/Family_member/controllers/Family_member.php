﻿<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Family_member extends MX_Controller {
  public function __construct() {
    parent:: __construct();
    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->library('table');
    $this->load->library('pagination');   
    $this->load->model('common_model_new');
    $this->load->helper(array('common_helper'));
    $this->load->helper(array('url'));
    $this->load->helper(array('form')); 
	
  }
  public function dashboard() {
    $data = array();
    $data['menuactive'] = $this->uri->segment(1);
	
	
	
    $this->template->set('title', 'Home');
    $this->template->load('family_member_layout', 'contents', 'dashboard', $data);
  }
  
  public function update_profile()
  {
    $data = array();
    $data['menuactive'] = $this->uri->segment(1);
	$checkLogin = $this->session->userdata('logged_in');	
	$data['singleData'] = $this->common_model_new->getsingle('cp_users',array('id' => $checkLogin['id']));
	
	if($_POST['submit']){
		
		 if ($_FILES['profile_image']['error'] == 0) {
            $data['upload_path'] = 'uploads/profile_images/';
            $data['allowed_types'] = 'jpg|png|gif|jpeg';
            $data['max_size'] = '5000';
            $data['encrypt_name'] = true;

            $this->load->library('upload', $data);


            if ($this->upload->do_upload('profile_image')) {
                $attachment_data = array('upload_data' => $this->upload->data());
                $uploadfile = $attachment_data['upload_data']['file_name'];

			}
		 }
		 
		 if($_FILES['profile_image']['name']){
			 
			 $image = $uploadfile;
		 }else{
			 $image = $data['singledata']->profile_image;
		 }
		
	     $array = array(
            'mobile' => $_POST['mobile'],
			'name' => $_POST['name_user'],
			'profile_image' => $image,
         );	
         $this->common_model_new->updateData('cp_users',$array,array("id" => $checkLogin['id']));
         $this->session->set_flashdata('success', 'Successfully Inserted');
		 redirect('family_member_dashboard');
	}	
    $this->template->set('title', 'Home');
    $this->template->load('family_member_layout', 'contents', 'update_profile', $data);
  }
  
  public function logout(){
        $this->session->unset_userdata('user_id');
        $this->session->sess_destroy();
        redirect('family_member_login');
      }
  
}     
?>
