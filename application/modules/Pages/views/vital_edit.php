<style>  .text_heading {
        border-bottom: 0;margin:0 0 14px 0;
	}
	.form-group > label:not(.error) {
    color: #000;
}
.addNote a, input.btn.btn-primary {
    margin: 0 !important;
}
</style>
	<div class=" text_heading">

  	<h3 class="text-center"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Edit Vital	</h3>

  		<div class="back_reminder"><a href="javascript:window.history.back()" class="btn btn-primary">Back</a></div>
  

  </div>
  <aside class="right-side">
  	<div class="content photo-list">
  		<div class="photo-listMain">

  			<!--- Success Message --->
  			<?php if ($this->session->flashdata('success_msg')) { ?>
  				<div class="container box">
  					<div class="alert alert-success message" id="success_msg">
  						<button type="button" class="close" data-dismiss="alert">x</button>
  						<p align="center"><?php echo $this->session->flashdata('success_msg'); ?></p>
  					</div>
  				</div>
  			<?php } ?>
  			<?php if (!empty($resp) && $resp != '') {
					$email_invalid_message = $resp['message'];
				} else {
					$email_invalid_message = "";
				} ?>
  			<input type="hidden" name="message" value="<?php echo $email_invalid_message; ?>">

  			<input type="hidden" name="user_id" value="<?php echo $vital_edit_arr['user_id']; ?>">

  			<?php if (!empty($vital_edit_arr) && $vital_edit_arr != '') { ?>
  				<form id="edit_vital" enctype="multipart/form-data" method="POST" class="frm_add" autocomplete="off">
  					<input type="hidden" name="vital_sign_id" value="<?php echo $vital_edit_arr['vital_sign_id']; ?>">

  					<div class="row">
  						<div class="col-sm-6 col-md-6">
  							<div class="bootstrap-timepicker">
  								<div class="form-group">
  									<label>Vital Update:<span class="red_star">*</span></label>
  									<div class="input-group date shedule" id="datepickers2">
  										<input type="text" class="form-control" readonly="" id="vital_edit_date" name="date" value="<?php echo $vital_edit_arr['date']; ?>" placeholder="Select your vital add date">
  										<?php echo form_error('vital_edit_date'); ?>
  										<div class="input-group-addon">
  											<span class="glyphicon glyphicon-calendar"></span>
  										</div>
  									</div>
  								</div>
  							</div>
  						</div>
  						<div class="col-sm-6 col-md-6">
  							<div class="form-group">
  								<label>Checkup Type<font style="color:red;">*</font>:</label>
  								<select name="test_type" type="text" class="form-control" id="test_type">
  									<option value="">Select</option>
  									<?php
										if (!empty($test_type_arr) && $test_type_arr != '') {
											foreach ($test_type_arr as $k) {
										?>
  											<option value="<?php echo $k['test_type']; ?>" <?php if ($k['test_type'] == $vital_edit_arr['test_type']) {
																									echo 'selected=selected';
																								} ?>><?php echo $k['test_type']; ?>
  											</option>
  									<?php }
										} ?>
  								</select>
  								<?php echo form_error('test_type'); ?>
  							</div>
  						</div>
  					</div>

  					<div class="row">
  						<div class="col-sm-6 col-md-6">
  							<div class="form-group">
  								<label>Ur Doctor.<font style="color:red;">*</font>:</label>
  								<select name="doctor" type="text" class="form-control">
  									<option value="">Select</option>
  									<?php
										if (!empty($doctor_arr) && $doctor_arr != '') {
											foreach ($doctor_arr as $k) {
										?>
  											<option value="<?php echo $k['doctor_name']; ?>" <?php if ($k['doctor_name'] == $vital_edit_arr['doctor']) {
																									echo 'selected=selected';
																								} ?>><?php echo $k['doctor_name']; ?>
  											</option>
  									<?php }
										} ?>
  								</select>
  								<?php echo form_error('doctor'); ?>
  							</div>
  						</div>
  						<div class="col-sm-6 col-md-6">
  							<div class="form-group">
  								<label>Checkup Type Unit<font style="color:red;">*</font>:</label>
  								<input type="text" class="form-control" id="unit" name="unit" value="<?php echo $vital_edit_arr['unit']; ?>" placeholder="Enter unit type">
  								<?php echo form_error('unit'); ?>
  							</div>
  						</div>
  					</div>

  					<div class="box-footer">
  						<input type="submit" name="submit" class="btn btn-primary" value="update">
  					</div>
  				</form>
  			<?php } ?>
  		</div>
  	</div>
  </aside>


  <script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>

  <script type="text/javascript">
  	$(document).ready(function() {

  		setTimeout(function() {
  			$('#success_msg').fadeOut('fast');
  		}, 2500);

  		$('#edit_vital').validate({
  			rules: {
  				vital_edit_date: 'required',
  				test_type: 'required',
  				doctor: 'required',
  				unit: 'required',
  			},

  			messages: {
  				vital_edit_date: 'Please select a vital  date.',
  				test_type: 'Please select a checkup Type.',
  				doctor: 'Please select a doctor.',
  				unit: 'Please enter unit yype',
  			}
  		});

  	});


  	$(function() {
  		$('#datepickers2').datetimepicker({
  			ignoreReadonly: true,
  			format: 'YYYY-MM-DD',
  			maxDate: moment()
  		});
  		$('#vital_edit_date').datetimepicker({
  			ignoreReadonly: true,
  			format: 'YYYY-MM-DD',
  			maxDate: moment()
  		});
  	});
  </script>

  <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>