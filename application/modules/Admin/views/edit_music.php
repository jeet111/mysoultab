<style>
.red {
	color: red;
}
.red1 {
	color: red;
}


#progress-wrp {
	border: 1px solid #0099CC;
	padding: 1px;
	position: relative;
	border-radius: 3px;
	margin: 10px;
	text-align: left;
	background: #fff;
	box-shadow: inset 1px 3px 6px rgba(0, 0, 0, 0.12);
}
#progress-wrp .progress-bar {
	height: 20px;
	border-radius: 3px;
	background-color: #f39ac7;
	width: 0;
	box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);
}
#progress-wrp .status {
	top: 3px;
	left: 50%;
	position: absolute;
	display: inline-block;
	color: #000000;
}

div#video_loader {
	position: fixed;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	background-color: #0a0a0aa6;
	z-index: 999999;
	text-align: center;
	padding-top: 23%;
}
div#video_loader img#loading_img {
	width: 70px !important;
}
</style>
<!-- <div class="back_ad"> -->
	<!-- <a class="admin_back" href="<?php echo base_url(); ?>admin/music_list" >Back</a></div> -->
	<div class="container-fluid">
		<div class="dash-counter">
			<div class="Schedule_main_one">
				<div class="users-main">
					<?php
					echo "<h2>Edit Music</h2>";
					?>
					<div class="btn_topBack">
						<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">
							Back
						</a>
					</div>
					<div id="image_msg"></div>
					<div id="loders"></div>
					<?php //echo "<pre>"; print_r($editMusic); echo "</pre>"; ?>






					<form class="form-horizontal add_movie_form" action="" method="post" enctype="multipart/form-data" name="AddUsers" id="music-form" >
						<div class="col-md-8">
							<div class="form-group">
								<label class="control-label col-md-4" for="">Music Title:<span class="field_req">*</span></label>
								<div class="col-md-8">
									<input type="text" placeholder="Music Title" class="form-control" name="music_title" id="username" value="<?php echo $editMusic->music_title; ?>">
									<?php echo form_error('article_title'); ?>
								</div>
							</div>
						</div>
						<div class="col-md-8">
							<div class="form-group">
								<label class="control-label col-md-4" for="">Music Description:<span class="field_req">*</span></label>
								<div class="col-md-8">
									<textarea name="music_description" id="" class="form-control" placeholder="Music Description" value="<?php echo set_value('music_description'); ?>"><?php echo $editMusic->music_desc; ?></textarea>
									<?php echo form_error('music_description'); ?>
								</div>
							</div>
						</div>

						<input type="hidden" name="music_id" id="music_id" value="<?php echo $this->uri->segment('3'); ?>" >

						<div class="col-md-8">
							<div class="form-group">
								<label class="control-label col-md-4" for="">Music Type:<span class="field_req">*</span></label>
								<div class="col-md-8">

									<select name="music_type" type="text" class="form-control" >
										<option value="">Select</option>
										<?php foreach($category_name as $category){ ?>
											<option value="<?php echo $category->music_category_id; ?>" <?php if($category->music_category_id == $editMusic->category_id){ ?>selected<?php } ?>><?php echo  $category->music_category_name; ?> </option>
										<?php } ?>
									</select>
									<!-- <input name="music_artist" type="text" placeholder="Music Artist" class="form-control"  id="" value="<?php echo set_value('music_artist'); ?>"> -->
									<?php echo form_error('music_type'); ?>
								</div>
							</div>
						</div>

						<div class="col-md-8">
							<div class="form-group">
								<label class="control-label col-md-4" for="">Music Artist:<span class="field_req">*</span></label>
								<div class="col-md-8">

				<!-- <select name="music_artist" type="text" class="form-control" >
					<option value="">Select</option>
					<option value='1' <?php if($editMusic->music_artist=='1'){echo 'selected=="selected"';} ?>>Arijit singh</option>
					<option value='2' <?php if($editMusic->music_artist=='2'){echo 'selected=="selected"';} ?>>Diljit dosanjh</option>
					<option value='3' <?php if($editMusic->music_artist=='3'){echo 'selected=="selected"';} ?>>Atif aslam</option>
					<option value='4' <?php if($editMusic->music_artist=='4'){echo 'selected=="selected"';} ?>>Neha kakkar</option>
				</select> -->

				<input name="music_artist" type="text" placeholder="Music Artist" class="form-control"  id="" value="<?php echo $editMusic->music_artist ?>">
				<?php echo form_error('music_artist'); ?>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="form-group">
			<label class="control-label col-md-4" for="">Music file:<span class="field_req">*</span></label>
			<div class="col-md-8">
				<div class="input-group">
					<span class="input-group-btn">
						<!-- <span class="btn btn-default btn-files"> -->
							<input type="file" id="profile_imagess" class="form-control" name="picture" onchange="loadFile1(event)">
							<!-- </span> -->
						</span>
						<div id="flupmsg"></div>
					</div>
					<div>
						<span id="demo1" style="color: red"></span>
					</div>
					<div id="progress-wrp" style="display:none"><div class="progress-bar"></div ><div class="status">0%</div></div>
					<audio controls="controls" id="dfds"> <source src= "<?php echo base_url(); ?>uploads/music/<?php echo $editMusic->music_file; ?>" type="audio/mpeg" > </source> </audio>
					<!-- <img id='img-upload' src="<?php //echo base_url().'uploads/music/defualt_music.png' ?>"> -->



				<!-- <div class="bnt_sub">
					<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit" onclick="showFileSize()">
				</div> -->
			</div>
		</div>
	</div>


	<div class="col-md-8">
		<div class="form-group">
			<label class="control-label col-md-4" for="">Music Image:<span class="field_req">*</span></label>
			<div class="col-md-8">
				<div class="input-group">
					<span class="input-group-btn">
						<!-- <span class="btn btn-default btn-files"> -->
							<input type="file" id="profile_images" class="form-control" name="pictureFile" onchange="loadFile(event)">
							<!-- </span> -->
						</span>
						<div id="flupmsgs"></div>
					</div>
					<div>
						<span id="demo" style="color: red"></span>
					</div>


					<?php if($editMusic->music_image==''){ ?>
						<img id='img-upload' src="<?php echo base_url().'uploads/music/defualt_music.png' ?>">
					<?php }else{ ?>
						<img id='img-upload' src="<?php echo base_url().'uploads/music/image/'.$editMusic->music_image?>">

					<?php } ?>
					<div class="bnt_sub">
						<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit" onclick="showFileSize()">

					<!-- <a class="cancel-btn btn" href="javascript:window.history.back()">
                        Back
                    </a> -->
                </div>
            </div>
        </div>
    </div>



</form>
</div>
</div>
</div>
</div>

<!--Added by 95 on 5-2-2019 For Doctor category for validation-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>
<script>
	$(".add_movie_form").on('submit',(function(e)
	{
		e.preventDefault();
		var isvalidate=$(".add_movie_form").valid();
		if(isvalidate)
		{
			$("#loders").append('<div id="video_loader" class="loading_class"><img id="loading_img" style="width:20px" src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif" /></div>');


			$.ajax({
				url : "<?php echo base_url(); ?>Admin/Music/ajax_music_edit",
				type: "POST",
				data: new FormData(this),
				dataType: "json",
				contentType: false,
				cache: false,
				processData:false,
				success: function(data) {
					$("#loading_img").hide();

					$("#video_loader").hide();
					if(data.code == 1){
						$("#loading_img").hide();
						$("#video_loader").hide();
						$("#sub").prop('disabled', true);

						$("#image_msg").html('<div class="alert alert-success">'+data.message+'<div>');
						setTimeout(function(){


							$("#image_msg").html("");
            //location.reload();
            window.location.href = '<?php echo base_url(); ?>admin/music_list';
        }, 1000);
					}else{
						$("#loading_img").hide();
						$("#video_loader").hide();
						$("#image_msg").html('<div class="alert alert-danger">'+data.message+'<div>');

					}
				}
			});
		}
	}));

	function loadFile (event) {
		var pcFile = $('#profile_images').val().split('\\').pop();
		var pcExt     = pcFile.split('.').pop();
		var output = document.getElementById('img-upload');
		if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"
			|| pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){
			$('#img-upload').show();
		output.src = URL.createObjectURL(event.target.files[0]);
		$('#flupmsgs').html('');
	}else{
		$('#flupmsgs').html('Please select only Image file.');
		$('#img-upload').hide();
	}
};


function loadFile1 (event) {
	var pcFile = $('#profile_imagess').val().split('\\').pop();
	var pcExt     = pcFile.split('.').pop();
	var output = document.getElementById('img-uploads');
	if(pcExt == "mp3"){
		$('#img-uploads').show();
		output.src = URL.createObjectURL(event.target.files[0]);
		$('#flupmsg').html('');
	}else{
		$('#flupmsg').html('Please select only audio file.');
		$('#img-uploads').hide();
	}
};

</script>
<script type="text/javascript">
	$(document).ready(function() {
		$.validator.addMethod("regx", function(value, element, regexpr) {
			return regexpr.test(value);
		}, "Please upload valid extension mp3 file.");
		$('#music-form').validate({
			rules: {
				music_title: 'required',
				music_description:'required',
				music_artist:'required',
				music_type:'required',

			},
			messages: {
				music_title: 'Please enter music title.',
				music_description:'Please enter music description.',
				music_artist:'Please enter an artist name.',
				music_type:'Please select a music type.'

			}
		});
	});

	/*Created by 95 on 6-2-2019 For Image validation*/
	// function showFileSize(){
	// 	var img  = document.getElementById('profile_imagess').value;

	// 	var img1  = document.getElementById('profile_images').value;


	// 	if(img==='' || img===null){
	// 		document.getElementById("demo1").innerHTML="Please choose a music file.";
	// 	}else{
	// 		document.getElementById("demo1").innerHTML=" ";
	// 	}


	// 	if(img1==='' || img1===null){
	// 		document.getElementById("demo").innerHTML="Please choose a Image file.";
	// 	}else{
	// 		document.getElementById("demo").innerHTML=" ";
	// 	}



	// }

	$(document).ready(function(){
		var progress_bar_id 		= '#progress-wrp';
		$('#profile_imagess').change(function(e){
			var pcFile = $('#profile_imagess').val().split('\\').pop();
			var pcExt     = pcFile.split('.').pop();
			var output = document.getElementById('img-uploads');
			if(pcExt == "mp3"){
				$('#flupmsg').hide();
				$("#dfds").hide();
				$('#progress-wrp').show();


				$(progress_bar_id +" .progress-bar").css("width", "0%");
				$(progress_bar_id + " .status").text("0%");
           // var file = this.files[0];
           var file = this.files[0];
           var form = new FormData();
           form.append('360_image_upload', file);

           $.ajax({
           	url : "<?php echo base_url(); ?>Admin/Music/image_upload",
           	type: "POST",
           	cache: false,
           	contentType: false,
           	processData: false,
           	data : form,
           	dataType: "json",
           	xhr: function(){
		//upload Progress
		var xhr = $.ajaxSettings.xhr();
		if (xhr.upload) {
			xhr.upload.addEventListener('progress', function(event) {
				var percent = 0;
				var position = event.loaded || event.position;
				var total = event.total;
				if (event.lengthComputable) {
					percent = Math.ceil(position / total * 100);
				}
				//update progressbar
				$(progress_bar_id +" .progress-bar").css("width", + percent +"%");
				$(progress_bar_id + " .status").text(percent +"%");
			}, true);
		}
		return xhr;
	},
	success: function(data){



		$("#progress-wrp").hide();
		$("#dfds").show();
		var fileInput = document.getElementById('profile_imagess');
		var fileUrl = window.URL.createObjectURL(fileInput.files[0]);
		$("#dfds").attr("src", fileUrl);

	}
});
       }
   });
	});
</script>