<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Custom_model extends CI_Model{
    function __construct() {
        
    }
	
/*Created by 95 for get all the doctor category with pagination*/
public function getDoctorCategoryList($limit, $start){
		$startt = max(0, ( $start -1 ) * $limit);
	   $this->db->select('*');
       $this->db->from('cp_doctor_categories');
       $this->db->limit($limit, $startt);
       $query = $this->db->get();
       $result = $query->result_array();
       return $result;
      }

/*Created by 95 for get all the doctors by category ID*/
function getDoctorList($id){
       $this->db->select('*');
       $this->db->from('cp_doctor');
       $this->db->where("FIND_IN_SET($id, doctor_category_id)");
       $query = $this->db->get();
       $result = $query->result_array();
       return $result;
    }
 
/*Created by 95 for get categories by doctor*/
public function getCategoryByDoctor($value='')
{
	$this->db->select("*");
	$this->db->from('cp_doctor_categories');
	$this->db->where('dc_id',$value);
	$query=$this->db->get();
	return $query->result();

}

/*Created by 95 for get main category*/
public function getMainCategory($value='')
{
	$this->db->select("*");
	$this->db->from('cp_doctor_categories');
	$this->db->where('dc_id',$value);
	$query=$this->db->get();
	return $query->result_array();

}





}