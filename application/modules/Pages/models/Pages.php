

<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Pages extends MX_Controller {



  public function __construct() {

      parent:: __construct();

      $this->load->library('session');

      $this->load->library('form_validation');

      $this->load->model('common_model');
      $this->load->view('sidebar');


      $this->load->helper(array('common_helper'));

      $this->load->helper(array('url'));

      $this->load->helper(array('form'));      

  }

    

  public function index() {

      $data = array();

      $data['menuactive'] = $this->uri->segment(1);

      $this->template->set('title', 'Home');

      $this->template->load('home_layout', 'contents', 'home', $data);

  }



  public function about() {

      $data = array();

      $data['menuactive'] = $this->uri->segment(1);

      $this->template->set('title', 'About');

      $this->template->load('page_layout', 'contents', 'about', $data);

  }



  public function contact() {

      $data = array();

      $data['menuactive'] = $this->uri->segment(1);

      $this->template->set('title', 'Contact');

      $this->template->load('page_layout', 'contents', 'contact', $data);

  }



  public function how_it_work() {

      $data = array();

      $data['menuactive'] = $this->uri->segment(1);

      $this->template->set('title', 'How IT Work');

      $this->template->load('page_layout', 'contents', 'how-it-work', $data);

  }



  public function signup(){



    

 

  	  $data = array();

      $data['menuactive'] = $this->uri->segment(1);

      if(isset($_POST['submitsignup'])){





        echo "jashdfj";

    die(); 

      $this->form_validation->set_rules('username', 'username', 'trim|required|min_length[5]|max_length[12]');

		  $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[6]');

		  $this->form_validation->set_rules('re_password', 'password confirmation', 'trim|required|matches[password]');

		  $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[cp_users.email]');

		  $this->form_validation->set_rules('mobile', 'mobile', 'trim|required|min_length[5]|max_length[14]|numeric');

          $this->form_validation->set_rules('user_role', 'userrole', 'trim|required');

          if($this->form_validation->run() == false)

	    {

	        $this->template->set('title', 'Signup');

      		$this->template->load('home_layout', 'contents', 'signup', $data);

	    }else{

        $otp = rand(0000,9999);

      	$data = array(

	        'username' => $this->input->post('username'),

	        'email' =>  $this->input->post('email'),

	        'mobile' => $this->input->post('mobile'),

	        'password' => md5($this->input->post('password')),

	        'user_role' => $this->input->post('user_role'),

	        'create_user' => date('Y-m-d H:i:s'),

	        'status' =>0,

          'otp' => $otp

	      );



      	$insertId = $this->common_model->addRecords('cp_users', $data);

          if ($insertId) {

              $email = $data['email'];

              $password = $this->input->post('password');

              $message ="Your account has been Registered";

              $message ="Please verify account this OTP".$otp;

              $subject = "New user registration";



              sendEmail($email,$subject,$message);

          }

          $message = array('message' => 'User has been added successfully please check your email and activate your account.<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');

            $this->session->set_flashdata('success', $message);

            redirect('otp_verify');

         }

      }else{

      $this->template->set('title', 'Signup');

      $this->template->load('home_layout', 'contents', 'signup', $data);

      }

  }

  public function signup1(){

      $data = array();

      $data['menuactive'] = $this->uri->segment(1);

      if(isset($_POST['submit'])){

        $otp = rand(0000,9999);

        $data = array(

          'username' => $this->input->post('username'),

          'email' =>  $this->input->post('email'),

          'mobile' => $this->input->post('mobile'),

          'password' => md5($this->input->post('password')),

          'user_role' => $this->input->post('user_role'),

          'create_user' => date('Y-m-d H:i:s'),

          'status' =>0,

          'otp' => $otp

        );



        $insertId = $this->common_model->addRecords('cp_users', $data);

          if ($insertId) {

              $email = $data['email'];

              $password = $this->input->post('password');



              //$message = "Your account has been Registered. Please  <a href='".base_url()."activate/".$insertId."'>Activate account Login </a> with the following details.<br>Email:".$email."<br>Password:".$password." ";

              $message ="Your account has been Registered";

              $message ="Please verify account this OTP".$otp;

              $subject = "New user registration";



              sendEmail($email,$subject,$message);

          }

          $message = array('message' => 'User has been added successfully please check your email and activate your account.<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');

            $this->session->set_flashdata('success', $message);

            redirect('otp_verify');

      }else{

      $this->template->set('title', 'Signup');

      $this->template->load('home_layout', 'contents', 'signup', $data);

      }

  }

  public function verify_account_otp()

  {

      $data = array();

      $data['menuactive'] = $this->uri->segment(1);

      if($this->input->post('submit')){

          $this->form_validation->set_rules('otp_number', 'otp', 'required');

          $otp = $this->input->post('otp_number');

          if ($this->form_validation->run() == true) {

            $checkUser = $this->common_model->getSingleRecordById('cp_users',array('otp'=> $otp));

            if($checkUser && $checkUser['user_role'] !== 1){ //customer

              $where_condition = array('id' => $checkUser['id']);

              $upd = $this->common_model->updateRecords('cp_users',array('status' => 1),$where_condition);

              if($upd >=1)

                $data['id']=$checkUser['id'];

                //$data['success_msg'] = 'Your account verifed, please login now.';

                //redirect('login');

                $this->session->set_flashdata('flashSuccess', 'Your account verifed, please login now');

                redirect('/login');

            } else {

              $data['error_msg'] = 'Wrong OTP, please try again.';

            }

          }

      }

      $this->template->set('title', 'OTP');

      $this->template->load('home_layout', 'contents', 'verify_account_otp', $data);

  }



  public function login1(){

      $data = array();

      $data['menuactive'] = $this->uri->segment(1);

      if(isset($_POST['submit'])){

          $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');

          $this->form_validation->set_rules('password', 'password', 'trim|required');

          if($this->form_validation->run() == false)

          {

              $this->template->set('title', 'Signup');

              $this->template->load('home_layout', 'contents', 'login', $data);

          }

          else{

              $con = array(

                   'email' => $this->input->post('email'),

                   'password' =>  md5($this->input->post('password')));

              $checkLogin = $this->common_model->checkRow($con);

              if($checkLogin){

                if($checkLogin['user_role'] != 1){

                    if($checkLogin['status'] == 0){

                        $this->session->set_flashdata('error', 'Your account is not activated yet, Please contact to support.');

                        redirect('login');

                    }

                    else{

                        $this->load->library('session');

                        $this->session->set_userdata('isUserLoggedIn',TRUE);

                        $this->session->set_userdata('logged_in', $checkLogin);

                        redirect('dashboard');

                        echo "Logged in successfully";

                    }

                }else{

                  $error = base64_encode('Invalid email or password');

                  redirect('login?error='.$error);

                }

          	}else{

                  $error = base64_encode('Invalid email or password');

                  redirect('login?error='.$error);

              }

          }

      }else{

      $this->template->set('title', 'Login');

      $this->template->load('home_layout', 'contents', 'login', $data);

      }

  }

  public function login(){

      $data = array();

      $data['menuactive'] = $this->uri->segment(1);

      if(isset($_POST['submit'])){

      	$email = $this->input->post('email');

      	$userdata = $this->common_model->getSingleRecordById('cp_users',array('email'=>$email));

      	if($userdata){

        $con = array(

               'email' => $this->input->post('email'),

               'password' =>  md5($this->input->post('password')));

          $checkLogin = $this->common_model->checkRow($con);

          if($checkLogin){

            if($checkLogin['user_role'] != 1){

                if($checkLogin['status'] == 0){

                    $this->session->set_flashdata('error', 'User inactive or deleted by admin.');

                    $this->template->load('home_layout', 'contents', 'login', $data);

                }

                else{

                    $this->load->library('session');

                    $this->session->set_userdata('isUserLoggedIn',TRUE);

                    $this->session->set_userdata('logged_in', $checkLogin);

                    redirect('dashboard');

                    echo "Logged in successfully";

                }

            }else{

              // $error = base64_encode('The inserted Email is not associated with any account

              // ');

              // redirect('login?error='.$error);

              $this->session->set_flashdata('error', 'The inserted email is not associated with any account.');

               $this->template->load('home_layout', 'contents', 'login', $data);

            }

        }else{

              $this->session->set_flashdata('error', 'The inserted password is not correct.');

               $this->template->load('home_layout', 'contents', 'login', $data);

              // $error = base64_encode('The inserted Email is not associated with any account

              // ');

              // redirect('login?error='.$error);

          }

        }else{

        	$this->session->set_flashdata('error', 'The inserted email is not associated with any account.');

               $this->template->load('home_layout', 'contents', 'login', $data);

        }  

      }else{

      $this->template->set('title', 'Login');

      $this->template->load('home_layout', 'contents', 'login', $data);

      }

  }



	public function activate(){

		$id =  $this->uri->segment(2);

		//fetch user details

		$user = $this->common_model->getSingleRecordById('cp_users',array('id'=>$id));

		//if code matches

		if($user['status'] == 0){

			//update user active status

			$data['status'] = 1;

			$query = $this->common_model->activate($data, $id);

 

			if($query){

				$this->session->set_flashdata('message', 'User activated successfully');

			}

			else{

				$this->session->set_flashdata('message', 'Something went wrong in activating account');

			}

		}

		redirect('login');

	}



  public function dashboard(){

    $checkLogin = $this->session->userdata('logged_in');

    if(!empty($checkLogin))

    {

      $data['user_data'] = $this->common_model->getSingleRecordById('cp_users',array('id' => $user_id));

      $this->template->set('title', 'User Dashboard');

      $this->template->load('user_dashboard_layout', 'contents', 'user_dashboard', $data);

    }else{

        redirect('login');

    }

  }



  public function user_email_list(){

    $data['menuactive'] = $this->uri->segment(1);

    $checkLogin = $this->session->userdata('logged_in');

    if(!empty($checkLogin))

    {

      $data['user_data'] = $this->common_model->jointwotable('cp_emails', 'user_id', 'cp_users', 'id',array('to_email ' => $checkLogin['email'],'email_status' => 1),'');

      //$data['user_data'] = $this->common_model->getAllRecordsById('cp_emails',array('to_email ' => $checkLogin['email']));

      $this->template->set('title', 'Mailbox');

      $this->template->load('user_dashboard_layout', 'contents', 'user_emails', $data);

    }else{

        redirect('login');

    }

  }

  public function user_sendemail_list(){

    $data['menuactive'] = $this->uri->segment(1);

    $checkLogin = $this->session->userdata('logged_in');

    if(!empty($checkLogin))

    {

      $data['user_data'] = $this->common_model->getAllRecordsById('cp_emails',array('user_id' => $checkLogin['id']));

      //print_r($data); die;

      $this->template->set('title', 'Mailbox');

      $this->template->load('user_dashboard_layout', 'contents', 'senduser_emails', $data);

    }else{

        redirect('login');

    }

  }



  public function sendemail(){

    //print_r($_POST); die;

    $checkLogin = $this->session->userdata('logged_in');

    if(!empty($checkLogin))

    {

        $data = array(

          'to_email' => $this->input->post('email_to'),

          'message' => $this->input->post('email_message'),

          'subject' => $this->input->post('subject'),

          'user_id' => $checkLogin['id'],

          'create_email' => date('d-m-y'),

          'email_status' => 1

        );

        //print_r($data); die;

        $insertId = $this->common_model->addRecords('cp_emails', $data);

        if ($insertId) {

              $email = $data['to_email'];

              $message =$data['message'];

              $subject = $data['subject'];

              sendEmail($email,$subject,$message);

              //redirect('email_list');

          }



    }else{

        redirect('email_list');

    }



  }

  public function delete_emails(){

    if(isset($_POST["id"]))

    {

     foreach($_POST["id"] as $id)

     {

      $where_condition = array('email_id' => $id);

      $upd = $this->common_model->updateRecords('cp_emails',array('email_status' => 3),$where_condition);

      if($upd){

        echo 'success';

      }

     }

  	}

  }



  public function user_photos(){

  	$data['menuactive'] = $this->uri->segment(1);

    $checkLogin = $this->session->userdata('logged_in');

    if(!empty($checkLogin))

    {

      $data['user_photo_data'] = $this->common_model->getAllRecordsById('cp_user_photo',array('user_id' => $checkLogin['id']));

      //print_r($data); die;

      $this->template->set('title', 'Photos');

      $this->template->load('user_dashboard_layout', 'contents', 'user_photos', $data);

    }else{

        redirect('login');

    }

  }

  public function validateData() { 

    if ($this->input->post("email") != '') {

      $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[cp_users.email]');

      if ($this->form_validation->run() == true) {

          die('true');

      } else {

          die('false');        

      }        

    }

    if ($this->input->post("mobile") != '') {

      $this->form_validation->set_rules('mobile', 'Mobile', 'required|is_unique[cp_users.mobile]');

      if ($this->form_validation->run() == true) {

          die('true');

      } else {

          die('false');        

      }        

    }

    if ($this->input->post("username") != '') {

      $this->form_validation->set_rules('username', 'Username', 'required|is_unique[cp_users.username]');

      if ($this->form_validation->run() == true) {

          die('true');

      } else {

          die('false');        

      }        

    }

  }



  public function ForgetPassword(){

    $data = array();

    $data['error_msg']="";

    $data['success_msg'] = "";

    if($this->input->post('submit')) {

      $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email');

      $email = $this->input->post('user_email');

      if ($this->form_validation->run() == true) {

        $checkUser = $this->common_model->getSingleRecordById('cp_users',array('email'=> $email,'status' => '1'));

        //print_r($checkUser); die;

        if($checkUser && $checkUser['user_role'] !== 1){ //customer

          //$reset_link = $this->generateRandomString();

          $otp = rand(0000,9999);

          $where_condition = array('id' => $checkUser['id']);

          $upd = $this->common_model->updateRecords('cp_users',array('status' => 0,'otp' => $otp),$where_condition);

          if($upd >=1)

          $message = "One time reset otp password:".$otp;

          $subject = "Care Pro Forgot password";

          sendEmail('votivephppushpendra@gmail.com', $subject, $message);

          //echo $x;

          $data['success_msg'] = 'Please check your email and reset your password.';

          redirect('otp_page');

        } else {

           $this->session->set_flashdata('error', 'The inserted email is not associated with any account.');

           $this->template->load('home_layout', 'contents', 'forgot-password', $data);

        }

      }

    }

    $this->template->set('title', 'Forgot Password');

    $this->template->load('home_layout', 'contents' , 'forgot-password', $data);

  }



  public function otp_page()

  {

      $data = array();

      $data['menuactive'] = $this->uri->segment(1);

      if($this->input->post('submit')){

          $this->form_validation->set_rules('otp_number', 'otp', 'required');

          $otp = $this->input->post('otp_number');

          if ($this->form_validation->run() == true) {

            $checkUser = $this->common_model->getSingleRecordById('cp_users',array('otp'=> $otp));

            //print_r($checkUser); die;

            if($checkUser && $checkUser['user_role'] !== 1){ //customer

              $where_condition = array('id' => $checkUser['id']);

              $upd = $this->common_model->updateRecords('cp_users',array('status' => 1),$where_condition);

              if($upd >=1)

                $data['id']=$checkUser['id'];

                $this->template->load('home_layout', 'contents', 'reset_password', $data);

            } else {

              //$data['error_msg'] = 'Wrong Otp, please try again.';

              $this->session->set_flashdata('error', 'Wrong OTP, please try again.');

              $this->template->load('home_layout', 'contents', 'otp_page', $data);

            }

          }

      }

      $this->template->set('title', 'OTP');

      $this->template->load('home_layout', 'contents', 'otp_page', $data);

  }



  public function userResetPassword() {

      $data = array();

      $data['menuactive'] = $this->uri->segment(1);

      if($this->input->post('submit')){

          $this->form_validation->set_rules('new_password', 'new password', 'required');

          $this->form_validation->set_rules('confirm_password', 'password confirmation', 'trim|required|matches[new_password]');

          $id = $this->input->post('user_id');

          $new_password = md5($this->input->post('new_password')); 

          if ($this->form_validation->run() == true) {

            $checkUser = $this->common_model->getSingleRecordById('cp_users',array('id'=> $id));

            //print_r($checkUser); die;

            if($checkUser && $checkUser['user_role'] !== 1){ //customer

              $where_condition = array('id' => $id);

              $upd = $this->common_model->updateRecords('cp_users',array('password' => $new_password),$where_condition);

              if($upd >=1)

              //$data['success_msg'] = 'Your reset password succcessfully.';

              $this->session->set_flashdata('success', 'Your reset password succcessfully.');

              $this->template->load('home_layout', 'contents', 'otp_page', $data);

              redirect('login');

            } else {

              //$data['error_msg'] = 'Something Wrong, please try again.';

              $this->session->set_flashdata('error', 'Something Wrong, please try again.');

            }

          }

      }

      $this->template->set('title', 'Reset Password');

      $this->template->load('home_layout', 'contents', 'reset_password', $data);

  }



  public function generateRandomString($length = 12) {

    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    $charactersLength = strlen($characters);

    $randomString = '';

    for ($i = 0; $i < $length; $i++) {

      $randomString .= $characters[rand(0, $charactersLength - 1)];

    }

    return $randomString;

  }



  public function usercontact_query(){

      $data = array();

      $data['menuactive'] = $this->uri->segment(1);

      if(isset($_POST['submit'])){

      $this->form_validation->set_rules('fullname', 'fullname', 'trim|required');

      $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');

      $this->form_validation->set_rules('comment', 'comment', 'trim|required');

          if($this->form_validation->run() == false)

      {

          $this->template->set('title', 'Contact');

          $this->template->load('page_layout', 'contents', 'contact', $data);

      }else{

        $data = array(

          'full_name' => $this->input->post('fullname'),

          'email' =>  $this->input->post('email'),

          'comment' => $this->input->post('comment'),

          'create_date' => date('Y-m-d H:i:s'),

          'status' =>0 

        );

        $insertId = $this->common_model->addRecords('contact_us', $data);

          if ($insertId) {

              $message = "Your message has been sumitted.";

              $this->session->set_flashdata('success', $message);

              redirect('contact');

          }

          $message = array('message' => 'Your message has not been sumitted.');

            $this->session->set_flashdata('success', $message);

            redirect('contact');

         }

      }else{

      $this->template->set('title', 'Contact');

      $this->template->load('page_layout', 'contents', 'contact', $data);

      }

  }



  public function logout(){

    $this->session->unset_userdata('user_id');

    $this->session->sess_destroy();

    redirect('login');

  }

  



}     



?>



