<?php if(!empty($data['user_data'])){print_r($data['user_data']);}  ?>
<div class="container-fluid">
	<div class="dash-counter users-main my-sts-table user-list">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<div class="my_info_one">
							<div class="Schedule_main_one">
								<div class="dash-counter users-main">
									<h2>View Profile</h2>
									<div class="row">
										<div class="col-md-3">
											<div class="card faq-left">
												<div class="social-profile">
													<img class="img-fluid" src="img/profile.jpg" alt="">
												</div>
												<div class="card-block">
													<h4>Josephin Villa<?php echo $seller_data['firstname'];?></h4>
													<h5>Mathematics</h5>
													<p>Lorem ipsum dolor sit amet, consectet
													ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte</p>
													<ul class="list_prof">
														<li>
															<i class="fa fa-phone"></i>
															+(1234) - 5678910
														</li>
														<li>
															<i class="fa fa-globe"></i>
															<a href="#" target="_blank">www.sts.com</a>
														</li>
														<li>
															<i class="fa fa-envelope"></i>
															<a href="#">demo@demo.com</a>
														</li>
													</ul>
													<div class="faq-profile-btn">
														<button type="button" class="faq-profBtn">Follows</button>
														<button type="button" class="faq-profBtn">Message</button>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-9">
											<div class="general-info">
												<div class="card-header">
													<h5 class="card-header-text">About Me</h5>
													<p>I am Guru</p>
												</div>
												<div class="rrow">
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-5 col-sm-5">
																<div class="f_name">Full Name:</div>
															</div>
															<div class="col-md-7 col-sm-7">
																<div class="f_nameOne">Josephine Villa</div>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-5 col-sm-5">
																<div class="f_name">Email:</div>
															</div>
															<div class="col-md-7 col-sm-7">
																<div class="f_nameOne">
																	<a href="#">demo@demo.com</a>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="rrow">
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-5 col-sm-5">
																<div class="f_name">State:</div>
															</div>
															<div class="col-md-7 col-sm-7">
																<div class="f_nameOne">New York</div>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-5 col-sm-5">
																<div class="f_name">Country:</div>
															</div>
															<div class="col-md-7 col-sm-7">
																<div class="f_nameOne">USA</div>
															</div>
														</div>
													</div>
												</div>
												<div class="rrow">
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-5 col-sm-5">
																<div class="f_name">Contact:</div>
															</div>
															<div class="col-md-7 col-sm-7">
																<div class="f_nameOne">(0123) - 45678910 </div>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-5 col-sm-5">
																<div class="f_name"></div>
															</div>
															<div class="col-md-7 col-sm-7">
																<div class="f_nameOne"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											<div class="row">
												<div class="col-md-6">
													<div class="general-info">
														<div class="card-header">
															<h5 class="card-header-text">Information</h5>
														</div>
														<div class="rrow">
															<div class="rrow">
																<div class="col-md-5 col-sm-6">
																	<div class="f_name">Cource Name:</div>
																</div>
																<div class="col-md-7 col-sm-6">
																	<div class="f_nameOne">Mathematics</div>
																</div>
															</div>
															<div class="rrow">
																<div class="col-md-5 col-sm-6">
																	<div class="f_name">Description:</div>
																</div>
																<div class="col-md-7 col-sm-6">
																	<div class="f_nameOne">Full Time Teaching</div>
																</div>
															</div>
															<div class="rrow">
																<div class="col-md-5 col-sm-6">
																	<div class="f_name">Website Url:</div>
																</div>
																<div class="col-md-7 col-sm-6">
																	<div class="f_nameOne">sts.com</div>
																</div>
															</div>
															<div class="rrow">
																<div class="col-md-5 col-sm-6">
																	<div class="f_name">Email:</div>
																</div>
																<div class="col-md-7 col-sm-6">
																	<div class="f_nameOne"><a href="#">demo@demo.com</a></div>
																</div>
															</div>
															<div class="rrow">
																<div class="col-md-5 col-sm-6">
																	<div class="f_name">Address:</div>
																</div>
																<div class="col-md-7 col-sm-6">
																	<div class="f_nameOne">123, USA</div>
																</div>
															</div>
															
														</div>
													</div>
												</div>
												
												
												
												
												
												<div class="col-md-6">
													<div class="general-info">
														<div class="card-header">
															<h5 class="card-header-text">Additional Information</h5>
														</div>
														<div class="rrow">
															
															<div class="rrow">
																<div class="col-md-5 col-sm-6">
																	<div class="f_name">Contact Person:</div>
																</div>
																<div class="col-md-7 col-sm-6">
																	<div class="f_nameOne">Josephin Villa</div>
																</div>
															</div>
															
															<div class="rrow">
																<div class="col-md-5 col-sm-6">
																	<div class="f_name">Email:</div>
																</div>
																<div class="col-md-7 col-sm-6">
																	<div class="f_nameOne"><a href="#">demo@demo.com</a></div>
																</div>
															</div>
															
															<div class="rrow">
																<div class="col-md-5 col-sm-6">
																	<div class="f_name">Mobile:</div>
																</div>
																<div class="col-md-7 col-sm-6">
																	<div class="f_nameOne">(0123) - 45678910</div>
																</div>
															</div>
															
															<div class="rrow">
																<div class="col-md-5 col-sm-6">
																	<div class="f_name">Fax:</div>
																</div>
																<div class="col-md-7 col-sm-6">
																	<div class="f_nameOne">(0123) - 45678910</div>
																</div>
															</div>
															
															<div class="rrow">
																<div class="col-md-5 col-sm-6">
																	<div class="f_name">Landline:</div>
																</div>
																<div class="col-md-7 col-sm-6">
																	<div class="f_nameOne">(0123) - 45678910</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>