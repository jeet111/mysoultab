<?php

if (isset($staff_permission[3]['add_permission']) == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_story">Add Story</a></div>

	</div>

	<?php

} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_story">Add Story</a></div>

	</div>

	<?php

}

?>





<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="row">

			<div class="col-md-12">

				<div class="">

					<div class="card-body">

						<div class="status-cng"></div>

						<?php if ($this->session->flashdata('success')) { ?>

							<div class="alert alert-success message">

								<button type="button" class="close" data-dismiss="alert">x</button>

								<?php echo $this->session->flashdata('success'); ?></div>

							<?php } ?>



							<!-- <div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_test_type">Add test type</a></div> -->

							<table class="table table-hover tab_comn" id="sampleTable">

								<thead>

									<tr>

										<th>S no.</th>

										<th>Member Name</th>

										<th>Story File</th>

										<th>Create Date</th>

										<th>Action</th>

									</tr>

								</thead>

								<tbody>

									<?php

									$i = 1;

									foreach ($featurescat_list as $list) { 



										?> 

										<tr>

											<td><?php echo $i++; ?></td>

											<td><?php echo $list->member_name; ?></td>

											<td>

												<?php $allowed = array("png","PNG","jpg","JPG","JEPG","jpeg", "gif", "GIF");

												$filename = $list->story_file;

												$ext = pathinfo($filename, PATHINFO_EXTENSION);

												if (!in_array($ext, $allowed)) { ?>

													

													<video width="320" height="240" controls>

														<source src="<?php echo base_url('uploads/member_stories/'.$list->story_file) ?>" type="video/mp4">





														</video>   



													<?php }else{ ?>

														<img id='img-upload' src="<?php echo base_url('uploads/member_stories/'.$list->story_file) ?>">

													<?php } ?>







													





												</td>

												<td><?php echo date('d-m-Y', strtotime($list->create_date)); ?></td>





												<td>

													<div class="link-del-view">

														<?php

														if (isset($staff_permission[4]['edit_permission']) == 1) {

															?>

															<div class="tooltip-2">

																<a href="<?php echo base_url(); ?>admin/edit_story/<?php echo $list->id; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>

																<span class="tooltiptext">Edit</span>

															</div>

															<?php

														} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

															?>

															<div class="tooltip-2">

																<a href="<?php echo base_url(); ?>admin/edit_story/<?php echo $list->id; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>

																<span class="tooltiptext">Edit</span>

															</div>

															<?php

														}

														?>

														

														<?php

														if (isset($staff_permission[4]['delete_permission']) == 1) {

															?>

															<div class="tooltip-2">

																<a href="<?php echo base_url(); ?>admin/delete_story/<?php echo $list->id; ?>" onclick="return confirm('Are you sure, you want to delete story ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

															</div>

															<?php

														} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

															?>

															<div class="tooltip-2">

																<a href="<?php echo base_url(); ?>admin/delete_story/<?php echo $list->id; ?>" onclick="return confirm('Are you sure, you want to delete story ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

															</div>

															<?php

														}

														?>

														

													</div>



												</div>

											</td>

										</tr>

									<?php } ?>

								</tbody>

							</table>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div> 