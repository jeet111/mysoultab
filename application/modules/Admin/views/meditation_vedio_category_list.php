<?php

if (isset($staff_permission[14]['add_permission']) == 1) {

	?>

	<div class="bx_user">  

		<div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_meditation_video_category">Add Category</a></div>

	</div>

	<?php

} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_meditation_video_category">Add Category</a></div>

	</div>

	<?php

}

?>



<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="row">

			<div class="col-md-12">

				<div class="">

					<?php //echo "<pre>"; print_r($acknowledgeData); echo "</pre>";?>

					<div class="card-body">

						<div class="status-cng"></div>

						<?php if ($this->session->flashdata('success')) { ?>

							<div class="alert alert-success message">

								<button type="button" class="close" data-dismiss="alert">x</button>

								<?php echo $this->session->flashdata('success'); ?></div>

							<?php } ?>

							<div id="deletemsg"></div>



							<!-- <div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_movie_category">Add Category</a></div> -->

							<table class="table table-hover tab_comn" id="sampleTable">

								<thead>

									<tr>

										<th>S no.</th>

										<th>Category Name</th>

										<th>Icon</th>

										<th>Status</th>

										<th>Action</th>

									</tr>

								</thead>

								<tbody>

									<?php

									$i = 1;

									foreach ($category_list as $list) {



										?>

										<tr class="card_one">

											<td><?php echo $i++; ?></td>

											<td><?php echo $list->movie_category_name; ?></td>

											<td><img src="<?php echo base_url('uploads/meditation_videos/category_icon/'.$list->movie_category_icon) ?>" height="50px" width="100px">    </td>

											<td>

												<?php if($list->movie_category_status==1){ ?>



													<span class="testing_<?php echo $list->movie_category_id; ?>"><button type="" class="btn ybt btn-xs"  disabled="">Active</button></span>



												<?php }else{ ?>



													<span class="testing_<?php echo $list->movie_category_id; ?>"><button type="" class="btn xbt btn-xs"  disabled="">Inactive</button></span>





												<?php } ?>



											</td>



											<td>

												<div class="link-del-view">

													<?php

													if (isset($staff_permission[14]['view_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/view_meditation_vedio_category/<?php echo $list->movie_category_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

															<span class="tooltiptext">View</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/view_meditation_vedio_category/<?php echo $list->movie_category_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

															<span class="tooltiptext">View</span>

														</div>

														<?php

													}

													?>

													

													<?php

													if (isset($staff_permission[14]['edit_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/edit_meditation_vedio_category/<?php echo $list->movie_category_id; ?>">

																<i class="fa fa-edit" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">Edit</span>

														</div>



														<?php if($list->movie_category_status == 1) { ?>

															<div class="tooltip-2"><a href="javascript:void(0);" id="statusmoviecat_<?php echo $list->movie_category_id; ?>"  data-movie_id="<?php echo $list->movie_category_id; ?>"><span class="test_<?php echo $list->movie_category_id; ?>"><i class="fa fa-toggle-on" aria-hidden="true"></i></span></a>

																<span class="tooltiptext">Active</span>

															</div>

														<?php }else { ?>

															<div class="tooltip-2"><a href="javascript:void(0);" id="statusmoviecat_<?php echo $list->movie_category_id; ?>" data-movie_id="<?php echo $list->movie_category_id; ?>"><span class="test_<?php echo $list->movie_category_id; ?>"><i class="fa fa-toggle-off" aria-hidden="true"></i></span></a>

																<span class="tooltiptext">Deactive</span>

															</div> 

														<?php } ?>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/edit_meditation_vedio_category/<?php echo $list->movie_category_id; ?>">

																<i class="fa fa-edit" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">Edit</span>

														</div>



														<?php if($list->movie_category_status == 1) { ?>

															<div class="tooltip-2"><a href="javascript:void(0);" id="statusmoviecat_<?php echo $list->movie_category_id; ?>"  data-movie_id="<?php echo $list->movie_category_id; ?>"><span class="test_<?php echo $list->movie_category_id; ?>"><i class="fa fa-toggle-on" aria-hidden="true"></i></span></a>

																<span class="tooltiptext">Active</span>

															</div>

														<?php }else { ?>

															<div class="tooltip-2"><a href="javascript:void(0);" id="statusmoviecat_<?php echo $list->movie_category_id; ?>" data-movie_id="<?php echo $list->movie_category_id; ?>"><span class="test_<?php echo $list->movie_category_id; ?>"><i class="fa fa-toggle-off" aria-hidden="true"></i></span></a>

																<span class="tooltiptext">Deactive</span>

															</div> 

														<?php } ?>

														<?php

													}

													?>

													

													<?php

													if (isset($staff_permission[14]['delete_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="javascript:void(0);" data-id="<?php echo $list->movie_category_id; ?>" id="deletemoviecat_<?php echo $list->movie_category_id; ?>">

																<i class="fa fa-trash-o" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">Delete</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="javascript:void(0);" data-id="<?php echo $list->movie_category_id; ?>" id="deletemoviecat_<?php echo $list->movie_category_id; ?>">

																<i class="fa fa-trash-o" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">Delete</span>

														</div>

														<?php

													}

													?>

													

													



												</div>



											</div>

										</td>

									</tr>

								<?php } ?>

							</tbody>

						</table>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<script>

	$(document).on('click','[id^="deletemoviecat_"]',function(){

		var delete_movie = $(this).attr('data-id');

		if(confirm("Are you sure, you want to delete this category ?")){

			$.ajax({

				type: "POST",

				url: '<?php echo base_url(); ?>Admin/Meditation_vedio/delete_meditation_vedio_category/',

				data: 'delete_movie='+delete_movie,

//dataType: "json",

success: function(data)

{





	setTimeout(function(){

		$("#deletemsg").html("<div class='alert alert-success'>Deleted Successfully</div>");

		location.reload();

//window.location.href = email_url+"email_list";

}, 1000);



}

});

			$(this).parents(".card_one").animate({ backgroundColor: "#fbc7c7" }, "fast")

			.animate({ opacity: "hide" }, "slow");

		}

	});





	$(document).on('click','[id^="statusmoviecat_"]',function(){

		var movie_id = $(this).attr('data-movie_id');

		$.ajax({

			type: "POST",

			url: '<?php echo base_url(); ?>Admin/Meditation_vedio/category_status',

			data: 'movie_id='+movie_id,

// dataType: "json",

success: function(data)

{

	if(data == 1){

		$(".test_"+movie_id).html('<i class="fa fa-toggle-on" aria-hidden="true"></i>');

		$(".testing_"+movie_id).html('<button type="" class="btn ybt btn-xs"  disabled="">Active</button>');

//$("#deletemsg").html("<div class='alert alert-success'>Category active successfully</div>");

$('#deletemsg').html("<div class='alert alert-success'>Category activate successfully</div>").fadeIn().delay(1000).fadeOut();

}else{

	$(".test_"+movie_id).html('<i class="fa fa-toggle-off" aria-hidden="true"></i>');

	$(".testing_"+movie_id).html('<button type="" class="btn xbt btn-xs"  disabled="">Inactive</button>');

// $("#deletemsg").html("<div class='alert alert-success'>Category deactive successfully</div>");

$('#deletemsg').html("<div class='alert alert-success'>Category inactivate successfully</div>").fadeIn().delay(1000).fadeOut();

}

}

});



	});

</script>