<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Article extends MX_Controller {
  public function __construct() {
    parent:: __construct();
    $this->load->library('session');
    $this->load->library('form_validation');      
    $this->load->model('activities_model');
    $this->load->model('Common_model_new');
    $this->load->model('common_model_new');
    $this->load->helper(array('common_helper'));
    $this->load->helper(array('url'));
    $this->load->helper(array('form'));
    not_login();      
  }
  public function add_article()
  {
    $data['menuactive'] = $this->uri->segment(1);
  // echo $this->uri->segment(1);
  // die;
    $this->form_validation->set_rules('article_title','Article Title','required');
    $this->form_validation->set_rules('artist_name','Artist Name','required');
    $this->form_validation->set_rules('article_type','Article Type','required');
    $this->form_validation->set_rules('article_description','Article Description','required');
    if ($this->form_validation->run() == TRUE) 
    {  
      if($_POST['submit']){
              //Check whether user upload picture
        if(!empty($_FILES['pictureFile']['name'])){
          $config['upload_path'] = 'uploads/articles/';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['picture']['name'];
                  //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('pictureFile')){
            $uploadData = $this->upload->data();
            $picture = $uploadData['file_name'];
          }else{
            $picture = '';
          }
        }else{
          $picture = '';
        }
        if(!empty($_FILES['pictureFile1']['name'])){ 
          $config['upload_path'] = 'uploads/articles/';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
                  //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('pictureFile1')){ 
           $attachment_data = array('upload_data' => $this->upload->data());
           $picture1 = $attachment_data['upload_data']['file_name'];
         }else{
          $picture1 = '';
        }
      }else{
        $picture1 = '';
      }
      if(!empty($_FILES['pictureFile2']['name'])){ 
        $config['upload_path'] = 'uploads/articles/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                  //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('pictureFile2')){ 
         $attachment_data = array('upload_data' => $this->upload->data());
         $picture2 = $attachment_data['upload_data']['file_name'];
         /*For image resize*/
         $dirpath = $attachment_data['upload_data']['full_path'];
         $this->load->library('image_lib');
         $configer =  array(
          'image_library'   => 'gd2',
          'source_image'    =>  $dirpath,
          'maintain_ratio'  =>  TRUE,
          'width'           =>  205,
          'height'          =>  205,
        );
         $this->image_lib->clear();
         $this->image_lib->initialize($configer);
         $this->image_lib->resize();
       }else{
        $picture2 = '';
      }
    }else{
      $picture2 = '';
    }
    $data = array(
      'article_title' => $this->input->post('article_title'),
      'article_desc' => $this->input->post('article_description'),
      'article_image' => $picture2,
      'artist_image' => $picture1,
      'article_icon' => $picture,
      'artist_name' => $this->input->post('artist_name'),
      'user_id'=>$this->session->userdata('logged_in')['id'],
      'category_id'=>$this->input->post('article_type'),
      'article_created' => date('Y-m-d h:i:s'),
    );
    $this->Common_model_new->insertData('cp_articles',$data);
    $this->session->set_flashdata('success', 'Article Added Successfully.');
    redirect('articles');
  }
} 
$data['categorys']= $this->Common_model_new->getAllwhere("cp_article_category",array("article_category_status" =>1));
$this->template->set('title', 'Articles');
$this->template->load('new_user_dashboard_layout', 'contents' , 'add_article',$data);
}
public function ajax_article_add()
{
  if ($_FILES["pictureFile1"]["error"] > 0 && $_FILES["pictureFile2"]["error"] > 0)
  {
   //$return = array('code' => 2,'message' => 'File loading error!');
  }else{ 
   $config1['upload_path'] = 'uploads/articles/';
   $config1['allowed_types'] = 'jpg|jpeg|png|gif';
           //$data['max_size'] = '5000';
   $config1['encrypt_name'] = true;
   $this->load->library('upload', $config1);
   if ($this->upload->do_upload('pictureFile1')) {  
    $attachment_data = array('upload_data' => $this->upload->data());
    $uploadfile = $attachment_data['upload_data']['file_name'];
      // $return = array('code' => 1,'message' => 'Successfully added');  
  }else{ 
  $return = array('code' => 2,'message' => ''/*strip_tags($this->upload->display_errors())*/);
}
$config2['upload_path'] = 'uploads/articles/';
$config2['allowed_types'] = 'jpg|jpeg|png|gif';
           //$data['max_size'] = '5000';
$config2['encrypt_name'] = true;
$this->upload->initialize($config2);
if ($this->upload->do_upload('pictureFile2')) {
  $attachment_data_image = array('upload_data' => $this->upload->data());
  $picturefile = $attachment_data_image['upload_data']['file_name'];
      //  $return = array('code' => 1,'message' => 'Successfully added'); 
  /*For image resize*/
  $this->load->library('image_lib');
  $configer =  array(
    'image_library'   => 'gd2',
    'source_image'    =>  $attachment_data_image['upload_data']['full_path'],
    'maintain_ratio'  =>  TRUE,
    'width'           =>  150,
    'height'          =>  150,
  );
  $this->image_lib->clear();
  $this->image_lib->initialize($configer);
  $this->image_lib->resize();
}else{ 
$return = array('code' => 2,'message' => ''/*strip_tags($this->upload->display_errors())*/);
}
if($return['code'] != 2){ 
 $array = array(
   'article_title' => $this->input->post('article_title'),
   'article_desc' => $this->input->post('article_description'),
   'artist_image' =>$uploadfile,
   'article_image' => $picturefile,     
   'artist_name' => $this->input->post('artist_name'),    
   'user_id'=>$this->session->userdata('logged_in')['id'],
   'category_id' =>$this->input->post('article_type'),          
   'article_created' => date('Y-m-d h:i:s'),
 );
 $this->Common_model_new->insertData("cp_articles",$array);
 $return = array('code' => 1,'message' => 'Successfully added');
}
}
echo json_encode($return);
}
/*Created by 95 for edit article*/
public function edit_article($article_id='')
{
 $data['menuactive'] = $this->uri->segment(1);
 $this->form_validation->set_rules('article_title','Article Title','required');
 $this->form_validation->set_rules('artist_name','Artist Name','required');
 $this->form_validation->set_rules('article_type','Article Type','required');
 $this->form_validation->set_rules('article_description','Article Description','required');
 $data['article']=$this->common_model_new->getsingle('cp_articles',array('article_id'=>$article_id));
 $data['articleid'] = $article_id;
 if ($this->form_validation->run() == TRUE) 
 {  
  if($_POST['submit']){
              //Check whether user upload picture
    if(!empty($_FILES['pictureFile']['name'])){
      $config['upload_path'] = 'uploads/articles/';
      $config['allowed_types'] = 'jpg|jpeg|png|gif';
      $config['file_name'] = $_FILES['picture']['name'];
                  //Load upload library and initialize configuration
      $this->load->library('upload',$config);
      $this->upload->initialize($config);
      if($this->upload->do_upload('pictureFile')){
        $uploadData = $this->upload->data();
        $picture = $uploadData['file_name'];
      }else{
        $picture = $data['article']->article_icon;
      }
    }else{
      $picture = $data['article']->article_icon;
    }
    if(!empty($_FILES['pictureFile1']['name'])){ 
      $config['upload_path'] = 'uploads/articles/';
      $config['allowed_types'] = 'jpg|jpeg|png|gif';
                  //Load upload library and initialize configuration
      $this->load->library('upload',$config);
      $this->upload->initialize($config);
      if($this->upload->do_upload('pictureFile1')){ 
       $attachment_data = array('upload_data' => $this->upload->data());
       $picture1 = $attachment_data['upload_data']['file_name'];
     }else{
      $picture1 = '';
    }
  }else{
    $picture1 = $data['article']->artist_image;
  }
  if(!empty($_FILES['pictureFile2']['name'])){ 
    $config['upload_path'] = 'uploads/articles/';
    $config['allowed_types'] = 'jpg|jpeg|png|gif';
                  //Load upload library and initialize configuration
    $this->load->library('upload',$config);
    $this->upload->initialize($config);
    if($this->upload->do_upload('pictureFile2')){ 
     $attachment_data = array('upload_data' => $this->upload->data());
     $picture2 = $attachment_data['upload_data']['file_name'];
     /*For image resize*/
     $dirpath = $attachment_data['upload_data']['full_path'];
     $this->load->library('image_lib');
     $configer =  array(
      'image_library'   => 'gd2',
      'source_image'    =>  $dirpath,
      'maintain_ratio'  =>  TRUE,
      'width'           =>  205,
      'height'          =>  205,
    );
     $this->image_lib->clear();
     $this->image_lib->initialize($configer);
     $this->image_lib->resize();
   }else{
    $picture2 = '';
  }
}else{
  $picture2 = $data['article']->article_image;
}
$data = array(
  'article_title' => $this->input->post('article_title'),
  'article_desc' => $this->input->post('article_description'),
  'article_image' => $picture2,
  'artist_image' => $picture1,
  'article_icon' => $picture,
  'artist_name' => $this->input->post('artist_name'),
  'user_id'=>$this->session->userdata('logged_in')['id'],     
  'category_id'=>$this->input->post('article_type'),
  'article_created' =>date('Y-m-d H:i:s')
);
$this->common_model_new->updateData('cp_articles',$data,array('article_id'=>$article_id));
      //$this->common_model_new->updateData('cp_articles',$data,array('article_id'=>$article_id));
$this->session->set_flashdata('success', 'Article Updated Successfully.');
redirect('articles');
}
}  
$data['categorys']= $this->common_model_new->getAllwhere("cp_article_category",array("article_category_status" =>1));
$this->template->set('title', 'Articles');
$this->template->load('new_user_dashboard_layout', 'contents' , 'edit_article',$data);
}
public function ajax_article_edit()
{
 $article_id = $this->input->post('article_id');
 $checkLogin = $this->session->userdata('logged_in');
 $singleData = $this->Common_model_new->getsingle("cp_articles",array("article_id" => $article_id));
 $config1['upload_path'] = 'uploads/articles/';
 $config1['allowed_types'] = 'jpg|png|gif|jpeg';
           //$data['max_size'] = '5000';
 $config1['encrypt_name'] = true;
 $this->load->library('upload', $config1);
 if(!empty($_FILES["pictureFile1"]["name"])){ 
  if ($this->upload->do_upload('pictureFile1')) { 
    $attachment_data = array('upload_data' => $this->upload->data());
    $uploadfile = $attachment_data['upload_data']['file_name'];
      // $return = array('code' => 1,'message' => 'Successfully added');  
  }else{ 
   $return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
 }
}
$config2['upload_path'] = 'uploads/articles/';
$config2['allowed_types'] = 'jpg|png|gif|jpeg';
           //$data['max_size'] = '5000';
$config2['encrypt_name'] = true;
$this->upload->initialize($config2);
if(!empty($_FILES["pictureFile2"]["name"])){ 
  if ($this->upload->do_upload('pictureFile2')) {
    $attachment_data_image = array('upload_data' => $this->upload->data());
    $picturefile = $attachment_data_image['upload_data']['file_name'];
      //  $return = array('code' => 1,'message' => 'Successfully added'); 
    /*For image resize*/
    $this->load->library('image_lib');
    $configer =  array(
      'image_library'   => 'gd2',
      'source_image'    =>  $attachment_data_image['upload_data']['full_path'],
      'maintain_ratio'  =>  TRUE,
      'width'           =>  150,
      'height'          =>  150,
    );
    $this->image_lib->clear();
    $this->image_lib->initialize($configer);
    $this->image_lib->resize();
  }else{ 
   $return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
 }
}
     //print_r($uploadfile);die;
if(!empty($picturefile)){ 
  $picfile = $picturefile;
}else{ 
  $picfile = $singleData->article_image;
}
if(!empty($uploadfile)){ 
  $upfile = $uploadfile;
}else{
  $upfile = $singleData->artist_image;
}
if(!empty($upfile) && !empty($picfile)){
 if($return['code'] != 2){ 
   $array = array(
     'article_title' => $this->input->post('article_title'),
     'article_desc' => $this->input->post('article_description'),
     'article_image' => $picfile,
     'artist_image' => $upfile,
     'artist_name' => $this->input->post('artist_name'),     
     'user_id' => $this->session->userdata('logged_in')['id'], 
     'category_id' => $this->input->post('article_type'),
          // 'music_image' => $pictureFile,
     'article_created' =>date('Y-m-d H:i:s')
   );
   $this->common_model_new->updateData('cp_articles',$array,array('article_id'=>$article_id));
   $return = array('code' => 1,'message' => 'Successfully updated');
 }
}
echo json_encode($return);
}
/*Created by 95 for show all the activities*/
 //  public function article_list()
  // { 
  //     $data['menuactive'] = $this->uri->segment(1);
 //        $this->template->set('title', 'All Activities');
 //        $this->template->load('user_dashboard_layout_music1', 'contents', 'articles_list',$data);    
  // }
public function article_list()
{ 
  $data['menuactive'] = $this->uri->segment(1);
  $data['popular_array'] = $this->common_model_new->getAllPopularArticle();
  $data['recent_array']  = $this->common_model_new->getAllRecentArticle();
  $data['category_array'] = $this->common_model_new->getAllCategoryArticle();
  $data['myarticle'] = $this->common_model_new->getAllMyArticle();
  $data['UserFavarticle']  = $this->common_model_new->getAllMyFavArticle();
      // echo "<pre>";
      // print_r($data['UserFavMovie']);
      // die;
  $this->template->set('title', 'All Activities');
  $this->template->load('user_dashboard_layout_music1', 'contents', 'articles_list',$data);   
}
public function AllCategories()
{
  $user_id = $this->session->userdata('logged_in')['id'];

  $data['menuactive'] = $this->uri->segment(1);
  $data['category_array'] = $this->common_model_new->getAllCategoryArticle();
  $data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'Articles',"user_id"=>$user_id));
  $this->template->set('title', 'Article Categories');
  $this->template->load('new_user_dashboard_layout', 'contents', 'view_all_article',$data);
}
public function AllCategoriessearch()
{
  $data['menuactive'] = $this->uri->segment(1);
  $article_search = str_replace('%20', ' ', $this->uri->segment("2"));
  $data['article_search'] = $article_search;
  $data['category_array'] = $this->common_model_new->getcatsearcharticle($article_search);
  $this->template->set('title', 'Article Categories');
  $this->template->load('user_dashboard_layout_music1', 'contents', 'view_all_articles_search',$data);
}
public function delete_article()
{
  $segment = $this->uri->segment("2");
  $checkLogin = $this->session->userdata('logged_in');
  $delete_article = $this->input->post("delete_article");
  $image_name = $this->common_model_new->getAllwhereorderby('cp_article_favourite', array('article_id' => $delete_article,'user_id'=>$checkLogin['id']),'article_fav_id','DESC');
  foreach ($image_name as $image) {
   $this->common_model_new->delete("cp_article_favourite", array('article_fav_id'=> $image->article_fav_id));
 }
 $movieDetail =$this->common_model_new->getsingle("cp_articles",array("article_id" => $delete_article));
 $articleImagePath=getcwd().'/uploads/articles/';
 $articleImage=$movieDetail->article_image;
 unlink($articleImagePath.$articleImage);
 $articleImagePath=getcwd().'/uploads/articles/';
 $artistImage=$movieDetail->artist_image;
 unlink($articleImagePath.$artistImage);
 $articleImagePath=getcwd().'/uploads/articles/';
 $articleiconImage=$movieDetail->article_icon;
 unlink($articleImagePath.$articleiconImage);
    //   $movieFilePath=getcwd().'/uploads/movie/';
    // $movieFile=$movieDetail->movie_file;
    //   unlink($movieFilePath.$movieFile);
 $this->common_model_new->delete('cp_articles',array('article_id' =>$delete_article));
}
public function read_article()
{ 
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    $segment = base64_decode($this->uri->segment("2"));
    $data['detail_article'] = $this->common_model_new->getsingle("cp_articles",array("article_id" => $segment));
    $data['check_article'] = $this->common_model_new->getsingle("cp_article_favourite",array("article_id" => $segment,"user_id" => $checkLogin['id']));
    $category_id = base64_decode($this->uri->segment("3")); 
    $data['popular_article'] = $this->common_model_new->AllRecentArticle($category_id);
    //$data['popular_article'] = $this->common_model_new->getAllorlimittrashnew('cp_articles','article_id','desc','5');
    //echo "<pre>"; print_r($data['popular_article']);die;
  //echo '<pre>';print_r($data['popular_article']);die;
    $this->template->set('title', 'Detail Article');
    $this->template->load('new_user_dashboard_layout', 'contents', 'detail_article',$data);
  }else{
    redirect('login');
  }
}
public function view_all_article($viewtype='')
{ 
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  $category_id = base64_decode($this->uri->segment(3));
  if($viewtype=='popular'){
   $data['menuactive'] = $this->uri->segment(1); 
   $data['type']='popular';
   $data['popular_array'] = $this->common_model_new->getAllPopularArticle($category_id);
    //$data['popular_count'] = 
     //$data['PlayMovie']  = $this->movie_model->getAllPlayMovie();
   $this->template->set('title', 'Popular Articles');
   $this->template->load('user_dashboard_layout_music1', 'contents', 'view_all_article',$data);
 }elseif($viewtype=='recent'){
  $data['menuactive'] = $this->uri->segment(1);
      //die('ioiuouoiu');
  $data['type']='recent';
  $data['recent_array']  = $this->common_model_new->getAllRecentArticle($category_id);
     //$data['PlayMovie']  = $this->movie_model->getAllPlayMovie();
  $this->template->set('title', 'Recent Articles');
  $this->template->load('user_dashboard_layout_music1', 'contents', 'view_all_article',$data);
}elseif($viewtype=='my'){
  $data['menuactive'] = $this->uri->segment(1);
  $data['type']='my';
  $data['UserArticleData']  = $this->common_model_new->getAllMyArticle($category_id);
     //$data['PlayMovie']  = $this->movie_model->getAllPlayMovie();
  $this->template->set('title', 'My Articles');
  $this->template->load('user_dashboard_layout_music1', 'contents', 'view_all_article',$data);
}elseif ($viewtype=='favorite'){
  $data['menuactive'] = $this->uri->segment(1);
  $data['type']='favorite';
  $data['UserFavArticl']  = $this->common_model_new->getAllMyFavArticle($category_id);
    // $data['PlayMovie']  = $this->movie_model->getAllPlayMovie();
  $this->template->set('title', 'Favourite Articles');
  $this->template->load('user_dashboard_layout_music1', 'contents', 'view_all_article',$data);
}else{
  $data['menuactive'] = $this->uri->segment(1);
      //$data['type']='favorite';
  $data['category_array'] = $this->common_model_new->getAllCategoryArticle();
    // $data['PlayMovie']  = $this->movie_model->getAllPlayMovie();
  $this->template->set('title', 'Article Categories');
  $this->template->load('user_dashboard_layout_music1', 'contents', 'view_all_article',$data);
}
}
public function ArticlesBycategories($category_id='')
{
  $data['menuactive'] = $this->uri->segment(1);
  $category_id = base64_decode($category_id);
  $data['popular_array'] = $this->common_model_new->getAllPopularArticle($category_id);
  $data['recent_array']  = $this->common_model_new->getAllRecentArticle($category_id);
  $data['category_array'] = $this->common_model_new->getAllCategoryArticle();
  $data['myarticle'] = $this->common_model_new->getAllMyArticle($category_id);
  $data['UserFavarticle']  = $this->common_model_new->getAllMyFavArticle($category_id);
  $this->template->set('title', 'All Activities');
  $this->template->load('user_dashboard_layout_music1', 'contents', 'articles_list',$data); 
    //  $data['CategoryArticles'] = $this->common_model_new->getAllwhereorderby("cp_articles",array("category_id" =>$category_id),"article_id","DESC");
    //   $this->template->set('title', 'Article Categories');
    // $this->template->load('user_dashboard_layout', 'contents', 'view_all_article_categories',$data);
}
public function article_list_search()
{
  $data['menuactive'] = $this->uri->segment(1);
  $segment = base64_decode($this->uri->segment(2));
  $checkLogin = $this->session->userdata('logged_in');
  $article_search = str_replace('%20', ' ', $this->uri->segment("3"));
  $data['article_search'] = $article_search;
  $data['popular_array'] = $this->common_model_new->getlikepopulararticle($article_search,$segment);
  $data['recent_array']  = $this->common_model_new->getlikerecentarticle($article_search,$segment);
  $data['category_array'] = $this->common_model_new->getcatsearcharticle($article_search,$segment);
  $data['myarticle'] = $this->common_model_new->getlikemyarticle($article_search,$checkLogin['id'],$segment);
  $data['UserFavarticle']  = $this->common_model_new->getlikefavarticle($article_search,$checkLogin['id'],$segment);
  $this->template->set('title', 'Article');
  $this->template->load('user_dashboard_layout_music1', 'contents', 'articles_list_search',$data);
}
public function view_all_article_search($viewtype='')
{
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  $category_id = base64_decode($this->uri->segment(3));
  $segmentsec = $this->uri->segment("4");
  $segment2 = str_replace("%20"," ",$segmentsec);
  $data['segment2'] = $segment2;
  if($viewtype=='popular'){
   $data['menuactive'] = $this->uri->segment(1); 
   $data['type']='popular';
   $data['popular_array'] = $this->common_model_new->getlikepopulararticleview($segment2,$category_id);
     //$data['PlayMovie']  = $this->movie_model->getAllPlayMovie();
   $this->template->set('title', 'Popular Articles');
   $this->template->load('user_dashboard_layout_music1', 'contents', 'new_view_article_search',$data);
 }elseif($viewtype=='recent'){
  $data['menuactive'] = $this->uri->segment(1);
      //die('ioiuouoiu');
  $data['type']='recent';
  $data['recent_array']  = $this->common_model_new->getlikerecentarticleview($segment2,$category_id);
     //$data['PlayMovie']  = $this->movie_model->getAllPlayMovie();
  $this->template->set('title', 'Recent Articles');
  $this->template->load('user_dashboard_layout_music1', 'contents', 'new_view_article_search',$data);
}elseif($viewtype=='my'){
  $data['menuactive'] = $this->uri->segment(1);
  $data['type']='my';
  $data['UserArticleData']  = $this->common_model_new->getlikemyarticleview($segment2,$checkLogin['id'],$category_id);
     //$data['PlayMovie']  = $this->movie_model->getAllPlayMovie();
  $this->template->set('title', 'My Articles');
  $this->template->load('user_dashboard_layout_music1', 'contents', 'new_view_article_search',$data);
}elseif ($viewtype=='favorite'){
  $data['menuactive'] = $this->uri->segment(1);
  $data['type']='favorite';
  $data['UserFavArticl']  = $this->common_model_new->getlikefavarticleview($segment2,$checkLogin['id'],$category_id);
    // $data['PlayMovie']  = $this->movie_model->getAllPlayMovie();
  $this->template->set('title', 'Favourite Articles');
  $this->template->load('user_dashboard_layout_music1', 'contents', 'new_view_article_search',$data);
}else{
  $data['menuactive'] = $this->uri->segment(1);
      //$data['type']='favorite';
  $data['category_array'] = $this->common_model_new->getAllCategoryArticle();
    // $data['PlayMovie']  = $this->movie_model->getAllPlayMovie();
  $this->template->set('title', 'Article Categories');
  $this->template->load('user_dashboard_layout_music1', 'contents', 'new_view_article_search',$data);
}
}
}