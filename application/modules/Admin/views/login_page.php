<div class="main_admin_sec">
  <div class="main_admin_sec_inner">
    <div class="admin_log_sec">
      <a class="Admin_log_logo" href="<?php echo base_url();?>admin/login"><img src="<?php echo base_url();?>assets/img/logo-icon.png" style="width:100px; height: 100px; margin-left: 135px; text-align: center;"></a>
      <h2>Admin Login</h2>
      <p class="text-center">
            <?php //echo validation_errors();
            if(isset($_GET['error'])) {
              $error = base64_decode($_GET['error']);

              echo '<div class="has-error" align="center"><font color="red">'.$error.'</font></div>';
            } ?>
          </p>
          <form name="AdminLogin" id="AdminLogin" action="<?php echo base_url() ?>admin/do_login" method="post">

            <div class="form-group">
              <input type="email" class="form-control" name="user_email" placeholder="Email"  value="<?php echo !empty($user['user_email'])?$user['user_email']:''; ?>">
              <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>
            </div>

            <div class="form-group">
              <input type="password" class="form-control" name="user_password" placeholder="Password">
              <?php echo form_error('user_password','<span class="help-block">','</span>'); ?>
            </div>

            <div class="form-group">
              <input type="submit" name="loginSubmit" class="btn-primary" value="Submit"/>
              <a href="<?php echo base_url() ?>admin/forgot/password" class="forgot_cls">FORGOT PASSWORD ?</a>
            </div>
          </form>
        </div>
      </div>
    </div>