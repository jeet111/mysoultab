<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Story extends MX_Controller {

	private $staff_permission = array();
	private $access_permission = true;
	public function __construct(){

		parent:: __construct();

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model('Common_model');
		$this->load->model('login_model');
		$this->load->model('Feature_model');

		$this->load->helper(array('url'));
		$this->load->helper(array('form'));

		$user_id = $this->session->userdata('logged_ins')['id'];
		if(empty($user_id)) {
			redirect('admin/login');
		}

		if ($this->session->userdata('logged_ins')['user_role'] == 5) {
			$uid = $this->session->userdata('logged_ins')['id'];
			$permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
			if (!empty($permission_list)) 
			{
				foreach ($permission_list as $k => $v) 
				{
					$this->staff_permission[$v['permission_id']] = $v;
				}
			}
		}

	}

	public function permission_access($permission_id=6)
	{
		if ($this->session->userdata('logged_ins')['user_role'] != 1) 
		{
			if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
			{
				$this->access_permission = false;
			}
		}
	}

	public function story_list()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$data['featurescat_list'] = $this->Common_model->getAllorderby('cp_member_stories','id','desc');
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'Tab features');
			$this->template->load('admin_dashboard_layout', 'contents', 'stories_list', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}




	public function feature_list()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$data['featurescat_list'] = $this->Common_model->getAllorderby('cp_tabfeatures','id','desc');
		//$data['featurescat_list'] = $this->Common_model->getAllorderby('cp_tabfeatures','id','desc');
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'Tab features');
			$this->template->load('admin_dashboard_layout', 'contents', 'features_list.php', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}


	public function generateRandomString($length = 12) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}


	public function add_story()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$this->form_validation->set_rules('member_name', 'Member Name', 'required');
		$this->form_validation->set_rules('story_desc', 'Story Description', 'required');

		$member_name = $this->input->post('member_name');
		$story_desc = $this->input->post('story_desc');

		if ($this->form_validation->run() == TRUE)
		{

			/*for vedio file*/
			$random = $this->generateRandomString(10);
			$ext = pathinfo($_FILES['feature_icon']['name'], PATHINFO_EXTENSION);
			$file_name = $random.".".$ext;
			$dirpath = './././uploads/member_stories/'.$file_name;
			if(move_uploaded_file($_FILES['feature_icon']['tmp_name'], $dirpath)){
				$data['new_pic'] = $file_name;
			}
			if(!empty($data['new_pic'])){
				$picture =$data['new_pic'];
			}else{
				$picture ='';
			}


			$array = array(
				'member_name	' => $member_name,
				'story_desc'=> $story_desc,
				'story_file'=>$picture,
				'create_date' => date('Y-m-d H:i:s')
			);
				//echo '<pre>';print_r($array );die;
			$test_type_id = $this->Feature_model->insertData('cp_member_stories',$array);

			//}
			
			$this->session->set_flashdata('success', 'Story Successfully Added.');
			redirect('admin/story_list');

		}
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'Member stories');
			$this->template->load('admin_dashboard_layout', 'contents', 'add_story', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}



	// Edit stroy
	public function edit_story()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$this->form_validation->set_rules('member_name', 'Member Name', 'required');
		$this->form_validation->set_rules('story_desc', 'Story Description', 'required');
		$segment = $this->uri->segment(3);
		$data['singleData'] = $this->Common_model->getsingle('cp_member_stories',array('id' => $segment));
		// echo "<pre>";
		// print_r($data['singleData']);
		// die;
		$member_name = $this->input->post('member_name');
		$story_desc = $this->input->post('story_desc');

		if ($this->form_validation->run() == TRUE)
		{

			/*for vedio file*/
			$random = $this->generateRandomString(10);
			$ext = pathinfo($_FILES['feature_icon']['name'], PATHINFO_EXTENSION);
			$file_name = $random.".".$ext;
			$dirpath = './././uploads/member_stories/'.$file_name;
			if(move_uploaded_file($_FILES['feature_icon']['tmp_name'], $dirpath)){
				$data['new_pic'] = $file_name;
			}
			if(!empty($data['new_pic'])){
				$picture =$data['new_pic'];
			}else{
				$picture ='';
			}


			if(!empty($picture)){

				$array = array(
					'member_name	' => $member_name,
					'story_desc'=> $story_desc,
					'story_file'=>$picture,
					'create_date' => date('Y-m-d H:i:s')
				);
				//echo '<pre>';print_r($array );die;
			//$test_type_id = $this->Feature_model->insertData('cp_member_stories',$array);
				$this->Common_model->updateData('cp_member_stories',$array,array('id' => $segment));
			//}

			}else{

				$array = array(
					'member_name	' => $member_name,
					'story_desc'=> $story_desc,
					'story_file'=>$data['singleData']->story_file,
					'create_date' => date('Y-m-d H:i:s')
				);
				//echo '<pre>';print_r($array );die;
			//$test_type_id = $this->Feature_model->insertData('cp_member_stories',$array);
				$this->Common_model->updateData('cp_member_stories',$array,array('id' => $segment));
			}


			$this->session->set_flashdata('success', 'Story Successfully Updated.');
			redirect('admin/story_list');

		}
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'Member stories');
			$this->template->load('admin_dashboard_layout', 'contents', 'edit_story', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}


	
	public function delete_story()
	{
		$segment = $this->uri->segment('3');
		$this->Feature_model->delete('cp_member_stories',array('id' => $segment));
		//$this->Common_model->delete('cp_vital_test_type_unit',array('test_type_id' => $segment));
		$this->session->set_flashdata('success', 'Successfully deleted');
		redirect('admin/story_list');
	}




}