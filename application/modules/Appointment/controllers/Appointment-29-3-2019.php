<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment extends MX_Controller {

  public function __construct() {
      parent:: __construct();
      $this->load->library('session');
      $this->load->library('form_validation'); 
       $this->form_validation->set_error_delimiters('<div class="error">', '</div>');    
	  $this->load->model('common_model_new');
      $this->load->helper(array('common_helper'));
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));      
  }
  
  /*Created by 95 for show appointment list*/
  public function appointment_list()
  {
	  if($this->session->userdata('logged_in')){
		   $data = array();
      $data['menuactive'] = $this->uri->segment(1);
	 
    $data['Allappointments'] = $this->common_model_new->getAllappointments($this->session->userdata('logged_in')['id']);
	  $this->template->set('title', 'Appointment');
      $this->template->load('user_dashboard_layout', 'contents', 'appointment_list', $data);
	   }else{
		   redirect('login');
	   }
  }


  /*Created by 95 for single appointment delete*/
  public function delete_appointment()
  {

    $delete_id = $this->uri->segment('2');

    $this->common_model_new->delete("doctor_appointments",array("dr_appointment_id" => $delete_id));
     $this->session->set_flashdata('success', 'Successfully deleted');
    redirect('appointment_list'); 
  }


  /*Created by 95 for delete multiple appointments*/
  public function appointments_all_del()
  {
    $bodytype = $this->input->post("bodytype");
    $explode = explode(',',$bodytype);
    
    foreach($explode as $del){
      $this->common_model_new->delete("doctor_appointments",array("dr_appointment_id" => $del));
    }
    echo '1';
  }

  /*Created by 95 for update or edit appointment*/
  public function edit_appointment($id)
  {
    if($this->session->userdata('logged_in')){
       $data = array();
      $data['menuactive'] = $this->uri->segment(1);
   
    if(isset($_POST['submit'])){
       
  $appointment_date = $this->input->post('reminder_date');
                   $date_id  = $this->input->post('date_id');
                 $doctor_id = $this->input->post('doctor_id');
                $selector  = $this->input->post('selector');

                if($selector=='Morning'){
                  $time_text='09:00 AM to 12:00 PM';
                }else if($selector=='Noon'){
                  $time_text='12:00 PM to 03:00 PM';
                }else if($selector=='Evening'){
                  $time_text='03:00 PM to 06:00 PM';
                }else if($selector=='Night'){
                  $time_text='06:00 PM to 09:00 PM';
                }
                
              $time_id = $this->input->post('time_id');
          $data=array(
                    'avdate_date'=>$appointment_date,
                    'avdate_created_date'=>date('Y-m-d H:i:s')
                  );

        $where=array('avdate_id'=>$date_id,'avdate_dr_id'=>$doctor_id);

      $result1 = $this->common_model_new->updateAvdate($data,$where);
          $data=array(
                      'avtime_text'=>$time_text,
                      'avtime_day_slot'=>$selector,
                      'avtime_created_date'=>date('Y-m-d H:i:s')
                    );

          $wheretime=array('avtime_id'=>$time_id,
                           'avtime_date_id'=>$date_id
                         );
          $result = $this->common_model_new->updateAvtime($data,$wheretime);


          if($this->input->post('remind')){
              $remind = $this->input->post('remind');
            }else{

              $remind=0;
            }

          $appointment=array('appointments_reminder'=>$remind);
          $where_appointment=array('dr_appointment_id'=>$id);

          $this->common_model_new->updateData('doctor_appointments',$appointment,$where_appointment);
          
          

          if($result1 AND $result){
            $this->session->set_flashdata('success', 'Appointment updated successfully.');
          redirect('appointment_list');
           
         }else{

          $this->session->set_flashdata('failed', 'Failed Please try again.');
          redirect('appointment_list');
         }

      }
    $data['EditAppointment'] = $this->common_model_new->getAppointment($id);
    $this->template->set('title', 'Appointment');
      $this->template->load('user_dashboard_layout_activities', 'contents', 'edit_appointment', $data);
     }else{
       redirect('login');
     }
  }

public function view_slots()
  {
	  $date = $this->input->post('date');
	  $doctor_id = $this->input->post('doctor_id');
	 $time_slot = $this->common_model_new->jointwotablegroupby('dr_available_date', 'avdate_id', 'dr_available_time', 'avtime_date_id','dr_available_time.avtime_day_slot',array('dr_available_date.avdate_dr_id' => $doctor_id,'dr_available_date.avdate_date' => $date),'*');
	 $EditAppointment = $this->common_model_new->getAppointment($doctor_id);
	 if(!empty($time_slot)){
	 $data = ' <div class="form-group">
              <label>Time:<span class="red_star">*</span></label>
				<ul class="list_time">'; 
				if(!empty($class="new_selector")){ 
				foreach($time_slot as $slot){
			   
         $data .= '<li>
			<input type="radio"';
       if($EditAppointment[0]->avtime_text==$slot->avtime_day_slot){ 
	   
	   $data .= 'checked';

	   } 
			$data .= 'class="timeslots" id="f-option_'.$slot->avtime_id.'" data-slot="'.$slot->avtime_day_slot.'" name="selector" value="'.$slot->avtime_day_slot.'">
			<label for="f-option_'.$slot->avtime_id.'">'.$slot->avtime_day_slot.'</label>
			<div class="check"></div>
		</li>';                                  
                                  
                $data .= '</ul>
               </div>';		
	 }}}else{
         $data .= '<div class="no-record">No date available on this time</div>';
		}	
		
		
		echo $data;
  }
  
 public function view_timeslot()
  {
	  $slot_id = $this->input->post("slot_id");
	  $doctor_id = $this->input->post("doctor_id");
	  $date_id = $this->input->post("date_id");
	  
	  
	  
	  $dropdown = $this->common_model_new->getsingle("dr_available_date",array("avdate_dr_id" => $doctor_id,"avdate_date" => $date_id));
	  
	  $newtime = $this->common_model_new->getAllwhere("dr_available_time",array("avtime_date_id" => $dropdown->avdate_id,"avtime_day_slot" => $slot_id));
	  $data = "<div class='tm-team-list-title'> </div>
								<div class='tm-team-list-value dat'>					
			                        <div class='bootstrap-timepicker'>    
		                                <div class='input-group date' id='datetimepickerhs'>";
	   if(!empty($newtime)){
	 $data .= "<select name='avail_time' class='form-control newAv'>";
			  
		  foreach($newtime as $new_avail_time){
			 $data .= "<option value=".$new_avail_time->avtime_id.">".$new_avail_time->avtime_text."</option>";
			 
		  }
		  $data .= "</select>";
	   }else{
		  $data .= "Not available time for this date";
	  }
	  $data .= '</div></div></div>';
  
	 echo $data;
	 
  } 
  

}