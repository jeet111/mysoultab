<?php defined('BASEPATH') OR exit('No direct script access allowed');



/*

| -------------------------------------------------------------------------

| URI ROUTING

| -------------------------------------------------------------------------

| This file lets you re-map URI requests to specific controller functions.

|

| Typically there is a one-to-one relationship between a URL string

| and its corresponding controller class/method. The segments in a

| URL normally follow this pattern:

|

|	example.com/class/method/id/

|

| In some instances, however, you may want to remap this relationship

| so that a different class/function is called than the one

| corresponding to the URL.

|

| Please see the user guide for complete details:

|

|	https://codeigniter.com/user_guide/general/routing.html

|

| -------------------------------------------------------------------------

| RESERVED ROUTES

| -------------------------------------------------------------------------

|

| There are three reserved routes:

|

|	$route['default_controller'] = 'welcome';

|

| This route indicates which controller class should be loaded if the

| URI contains no data. In the above example, the "welcome" class

| would be loaded.

|

|	$route['404_override'] = 'errors/page_missing';

|

| This route will tell the Router which controller/method to use if those

| provided in the URL cannot be matched to a valid route.

|

|	$route['translate_uri_dashes'] = FALSE;

|

| This is not exactly a route, but allows you to automatically route

| controller and method names that contain dashes. '-' isn't a valid

| class or method name character, so it requires translation.

| When you set this option to TRUE, it will replace ALL dashes in the

| controller and method URI segments.

|

| Examples:	my-controller/index	-> my_controller/index

|		my-controller/my-method	-> my_controller/my_method

*/

//$route['default_controller'] = 'main';


$route['default_controller'] = 'Pages/Login';

$route['about-us'] = 'Pages/Login/about';

$route['faqs'] = 'Pages/Login/faqs';  

$route['tempfaqs'] = 'Pages/Login/temp_faqs';

$route['member-stories'] = 'Pages/Login/member_stories';        

$route['subscriptions'] = 'Pages/Login/subscriptions';

$route['pricing'] = 'Pages/Login/subscriptions';

$route['contact-us'] = 'Pages/Login/contact';

//$route['contact'] = 'Pages/Login/contact';

$route['features'] = 'Pages/Login/features';

$route['survey'] = 'Pages/Login/survey';     

$route['how-it-work'] = 'Pages/Login/how_it_work';

$route['privacy-policy'] = 'Pages/Login/privacy_policy';

$route['term-of-use'] = 'Pages/Login/term_and_condition';

//$route['term_and_condition'] = 'Pages/Login/term_and_condition';

$route['contact_query'] = 'Pages/Login/usercontact_query';

$route['signup/(:any)'] = 'Pages/Login/signup/$1';
$route['yoga'] = 'Pages/Yoga/yoga'; 
$route['admin/yoga_video_list'] = 'Admin/Yoga_video/yoga_video_list'; 
$route['admin/add_yoga_video'] = 'Admin/Yoga_video/add_yoga_video';
$route['admin/delete_yoga_video/(:any)'] = 'Admin/Yoga_video/delete_yoga_video/$1';  
$route['admin/view_yoga_video/(:any)'] = 'Admin/Yoga_video/view_yoga_video/$1';
$route['admin/edit_yoga_video/(:any)'] = 'Admin/Yoga_video/edit_yoga_video/$1';

$route['checktet/(:any)'] = 'Pages/Login/checktet/$1';
$route['camera'] = 'Pages/Camera/camera'; 



$route['otp_verify'] = 'Pages/Login/verify_account_otp';

$route['signup1'] = 'Pages/Login/signup1';

$route['login'] = 'Pages/Login/login';

$route['otp_page'] = 'Pages/Login/otp_page';

$route['activate/(:any)'] = 'Pages/Login/activate/$1';

$route['dashboard'] = 'Pages/Login/dashboard';

$route['dashboard_new'] = 'Pages/Login/dashboard_new';
$route['dashboard_game'] = 'Pages/Login/dashboard_game';

$route['email_list'] = 'Pages/user_email_list';

$route['send_email_list'] = 'Pages/user_sendemail_list';



$route['transactions'] = 'Pages/user_transacctions';

$route['address_book'] = 'Pages/address_book';



$route['view_address/(:any)'] = 'Pages/view_address/$1';



$route['edit_address/(:any)'] = 'Pages/edit_address/$1';

$route['call'] = 'Pages/Call/call';

$route['devices'] = 'Pages/Devices/devices';

$route['edit_device_setting/(:any)'] = 'Pages/Devices/edit_device_setting/$1';

$route['devices/update_status'] = 'Pages/Devices/update_status';

$route['yoga'] = 'Pages/Yoga/yoga'; 

$route['sprituality'] = 'Pages/Sprituality/sprituality';    

$route['add_address'] = 'Pages/add_address';

$route['delete_address/(:any)']  = 'Pages/delete_address/$1';

$route['savsettings']  = 'Pages/savsettings';	

$route['all_tickets'] = 'Pages/Tickets/ticket_list';

$route['genrate_ticket'] = 'Pages/Tickets/genrate_ticket';

$route['view_ticket/(:any)'] = 'Pages/Tickets/view_ticke_detail/$1';

$route['view_conversation/(:any)'] = 'Pages/Tickets/view_conversation/$1';



$route['reply_to_ticket/(:any)'] = 'Pages/Tickets/reply_to_ticket/$1';













//$route['sendemail']  = 'Pages/sendemail';

$route['sendemail']  = 'Attachment/sendemail';

$route['delete_emails']  = 'Pages/delete_emails';

$route['user_photos']  = 'Pages/user_photos';

$route['uploading_image'] = 'Pages/upload_user_images';

$route['delete_photos']  = 'Pages/delete_photos';

$route['delete_photo']  = 'Pages/delete_photo';

$route['addphotos_favourite'] = 'Pages/addphotos_favourite';

$route['admin/doctor_appointmensts/(:any)'] = 'Admin/Doctor/doctor_appointmensts/$1';

$route['music_list/(:any)'] = 'Pages/Music/search_newmusic_list/$1';



$route['photos_favourite'] = 'Pages/photos_favourite';

$route['reminder_list']  = 'Reminder/reminder_list';

$route['add_reminder']  = 'Reminder/add_reminder';

$route['delete_reminder/(:any)']  = 'Reminder/delete_reminder/$1';

$route['edit_reminder/(:any)']  = 'Reminder/edit_reminder/$1';



$route['logout'] = 'Pages/logout';

$route['user/logout'] = 'Pages/logout';

$route['validate/email'] = 'Pages/Login/validateEmail';

$route['validate/users/data'] = 'Pages/Login/validateData';



$route['validatedata'] = 'Pages/Login/validateData';



$route['forgotpassword'] = 'Pages/Login/ForgetPassword';

$route['reset_password'] = 'Pages/Login/userResetPassword';

$route['add_music'] = 'Pages/Music/add_music';

$route['edit_music/(:any)'] = 'Pages/Music/edit_music/$1';

$route['play_music'] = 'Pages/Music/play_music';

$route['getinfo_music/(:any)'] = 'Pages/Music/getinfo_music/$1';

$route['getinfo_movie/(:any)'] = 'Pages/Movie/getinfo_movie/$1';



$route['order-with-grab'] = 'Pages/OrderWithGrab';

$route['travel-with-grab'] = 'Pages/TravelWithGrab';

$route['load-orders'] = 'Pages/LoadOrders';



//$route['check/userdata'] = 'Users/validateUserData';

$route['user/login'] = 'Users/login';

$route['user/asign_login'] = 'Users/asign_login';

//$route['user/logout'] = 'Users/logout';

$route['user/forgot'] = 'Users/ForgetPassword';



$route['facebook/login'] = 'Users/facebook_login';

$route['google/login'] = 'Users/google_login';

$route['instagram/login'] = 'Users/instagram_Login';

$route['linked/login'] = 'Users/linked_Login';

$route['reddit/login'] = 'Users/reddit_Login';

$route['pinterest/login'] = 'Users/pinterest_Login';

$route['vk/login'] = 'Users/vk_Login';

$route['tumblr/login'] = 'Users/tumblr_Login';

$route['okru/login'] = 'Users/okru_Login';



$route['social/logout'] = 'Users/social_logout';

$route['trash_email'] = 'Pages/Attachment/trash_email';



$route['profile'] = 'Users/User_roles/profile_view';

$route['validate/userdata'] = 'Users/ValidateUserData';

$route['update/profile'] = 'Users/User_roles/updateProfile';



$route['change/password'] = 'Users/User_roles/ChangePassword';

$route['update/password'] = 'Users/User_roles/UpdatePassword';



$route['404_override'] = '';

$route['translate_uri_dashes'] = FALSE;



$route['admin/dashboard'] = 'Admin/Admin_user/dashboard';



$route['admin/set-date'] = 'Admin/Admin_user/SetDate';



$route['admin/list-date'] = 'Admin/Admin_user/listDate';





$route['admin/login'] = 'Admin/Admin_login/login';

$route['admin/forgot/password'] = 'Admin/Admin_login/adminForgetPassword';

$route['reset/password/(:any)'] = 'Admin/Admin_login/AdminResetPassword';

$route['reset/admin/password'] = 'Admin/Admin_login/ResetAdminPassword';

$route['admin/do_login'] = 'Admin/Admin_login/signin';



$route['admin/logout'] = 'Admin/Admin_user/logout';



$route['admin/adminprofile'] = 'Admin/Admin_user/adminProfile';

$route['admin/adminedit'] = 'Admin/Admin_user/adminEditProfile';

$route['admin/changepassword'] = 'Admin/Admin_user/change_password';

$route['admin/updatepassword'] = 'Admin/Admin_user/update_Password';



$route['validate/user/data'] = 'Admin/Admin_user/validateData';



$route['admin/faq_list'] = 'Admin/Admin_faq/faq_list';

$route['admin/add_faq'] = 'Admin/Admin_faq/add_faq';

$route['admin/add_faq/(:any)'] = 'Admin/Admin_faq/add_faq';

$route['admin/delete_faq'] = 'Admin/Admin_faq/delete_faq';



$route['admin/plan_list'] = 'Admin/Admin_plan/plan_list';

$route['admin/add_plan'] = 'Admin/Admin_plan/add_plan';
$route['admin/add_plan/(:any)'] = 'Admin/Admin_plan/add_plan';
$route['admin/edit_plan/(:any)'] = 'Admin/Admin_plan/add_plan';

$route['admin/delete_plan'] = 'Admin/Admin_plan/delete_plan';



$route['admin/testimonial_list'] = 'Admin/Admin_testimonial/testimonial_list';

$route['admin/add_testimonial'] = 'Admin/Admin_testimonial/add_testimonial';

$route['admin/add_testimonial/(:any)'] = 'Admin/Admin_testimonial/add_testimonial';

$route['admin/delete_testimonial'] = 'Admin/Admin_testimonial/delete_testimonial';



$route['admin/users/list'] = 'Admin/Admin_user/users';

$route['admin/add/user'] = 'Admin/Admin_user/adduser_person';

$route['admin/add/family'] = 'Admin/Admin_user/adduser_family';

$route['admin/add/caregiver'] = 'Admin/Admin_user/adduser_caregiver';

$route['admin/edit/user/(:any)'] = 'Admin/Admin_user/adduser_person/$1';

$route['admin/view_person/user/(:any)'] = 'Admin/Admin_user/viewuser/$1';

$route['admin/delete/user'] = 'Admin/Admin_user/deleteuserRecords';

$route['admin/user/status'] = 'Admin/Admin_user/updateStatusUser';

$route['admin/user/verify'] = 'Admin/Admin_user/verifyStatusUser';



$route['admin/family_member/list'] = 'Admin/Admin_user/family_member';

$route['admin/addfamily_member'] = 'Admin/admin_user/adduser_family';

$route['admin/editfamily/family_member/(:any)'] = 'Admin/admin_user/adduser_family/$1';

$route['admin/viewfamily_member/family_member/(:any)'] = 'Admin/Admin_user/viewfamily/$1';

$route['admin/delete/family_member'] = 'Admin/Admin_user/deleteuserRecords';

$route['admin/family_member/status'] = 'Admin/Admin_user/updateStatusUser';



$route['admin/caregiver/list'] = 'Admin/Admin_user/caregiver_list';

$route['admin/addcaregiver'] = 'Admin/admin_user/adduser_caregiver';

$route['admin/editcare/caregiver/(:any)'] = 'Admin/admin_user/adduser_caregiver/$1';

$route['admin/viewcaregiver/caregiver/(:any)'] = 'Admin/Admin_user/viewcaregiver/$1';

$route['admin/delete/caregiver'] = 'Admin/Admin_user/deleteuserRecords';

$route['admin/caregiver/status'] = 'Admin/Admin_user/updateStatusUser';



$route['admin/userphoto/list'] = 'Admin/Admin_user/user_photo';

$route['admin/addphoto'] = 'Admin/admin_user/adduser_photo';

$route['admin/editphoto/(:any)'] = 'Admin/admin_user/adduser_photo/$1';

$route['admin/view_photo/photo/(:any)'] = 'Admin/Admin_user/viewuser_photo/$1';

$route['admin/delete/photo'] = 'Admin/Admin_user/deleteuserphotoRecords';

$route['admin/photo/status'] = 'Admin/Admin_user/updateStatusphoto';



$route['admin/email_list/list'] = 'Admin/Admin_user/email_list';

$route['admin/addtransport'] = 'Admin/Admin_user/addTransport';

$route['admin/edittransport/(:any)'] = 'Admin/Admin_user/addTransport/$1';

$route['admin/view/email/(:any)'] = 'Admin/Admin_user/viewEmail/$1';

$route['admin/delete/email'] = 'Admin/Admin_user/deleteemailRecords';

$route['admin/email/status'] = 'Admin/Admin_user/updateStatusemails';



$route['admin/dc_category/list'] = 'Admin/Admin_user/doctor_categories';

$route['admin/addtrip'] = 'Admin/Admin_user/addTrip';

$route['admin/edittrip/(:any)'] = 'Admin/Admin_user/addTrip/$1';

$route['admin/view/dc_cat/(:any)'] = 'Admin/Admin_user/viewDc_cat/$1';

$route['admin/delete/trip'] = 'Admin/Admin_user/deleteTripRecords';

$route['admin/trip/status'] = 'Admin/Admin_user/updateStatusTrip';



$route['admin/translations/list'] = 'Admin/Admin_user/Translations';

$route['admin/addtranslation'] = 'Admin/Admin_user/addTranslation';

$route['admin/edittranslation/(:any)'] = 'Admin/Admin_user/addTranslation/$1';

$route['admin/delete/translate'] = 'Admin/Admin_user/deleteTranslationRecords';

$route['admin/translate/status'] = 'Admin/Admin_user/updateStatusTranslation';



$route['admin/subcategories/list'] = 'Admin/Admin_user/Subcategories';

$route['admin/addsubcategory'] = 'Admin/Admin_user/addSubcategory';

$route['admin/editsubcategory/(:any)'] = 'Admin/Admin_user/addSubcategory/$1';

$route['admin/delete/subcategory'] = 'Admin/Admin_user/deleteSubcategoryRecords';

$route['admin/subcategory/status'] = 'Admin/Admin_user/updateStatusSubcategory';





//$route['admin/doctor_schedules/(:any)'] = 'Admin/Doctor/doctor_shedule_list/$1';

$route['admin/doctor_shedules/(:any)'] = 'Admin/Doctor/doctor_shedule_list/$1';











$route['admin/categories/list'] = 'Admin/Admin_user/Categories';

$route['admin/addcategory'] = 'Admin/Admin_user/addCategory';

$route['admin/editcategory/(:any)'] = 'Admin/Admin_user/addCategory/$1';

$route['admin/viewcategory/(:any)'] = 'Admin/Admin_user/viewCategory/$1';

$route['admin/delete/category'] = 'Admin/Admin_user/deleteCategoryRecords';

$route['admin/category/status'] = 'Admin/Admin_user/updateStatusCategory';



$route['admin/faqs/list'] = 'Admin/Admin_user/Faqs';

$route['admin/addfaq'] = 'Admin/Admin_user/addFaq';

$route['admin/editfaq/(:any)'] = 'Admin/Admin_user/addFaq/$1';

$route['admin/viewfaq/(:any)'] = 'Admin/Admin_user/viewFaq/$1';

$route['admin/delete/faq'] = 'Admin/Admin_user/deleteFaqRecords';

$route['admin/faq/status'] = 'Admin/Admin_user/updateStatusFaq';



$route['admin/works/list'] = 'Admin/Admin_user/Works';

$route['admin/addwork'] = 'Admin/Admin_user/addWork';

$route['admin/editwork/(:any)'] = 'Admin/Admin_user/addWork/$1';

$route['admin/viewwork/(:any)'] = 'Admin/Admin_user/viewWork/$1';

$route['admin/delete/work'] = 'Admin/Admin_user/deleteWorkRecords';

$route['admin/work/status'] = 'Admin/Admin_user/updateStatusWork';



$route['admin/stores/list'] = 'Admin/Admin_user/Stores';

$route['admin/addstore'] = 'Admin/Admin_user/addStore';

$route['admin/editstore/(:any)'] = 'Admin/Admin_user/addStore/$1';

$route['admin/viewstore/(:any)'] = 'Admin/Admin_user/viewStore/$1';

$route['admin/delete/store'] = 'Admin/Admin_user/deleteStoreRecords';

$route['admin/store/status'] = 'Admin/Admin_user/updateStatusStore';



$route['admin/ratings/list'] = 'Admin/Admin_user/Ratings';

$route['admin/viewrating/(:any)'] = 'Admin/Admin_user/viewRating/$1';

$route['admin/delete/rating'] = 'Admin/Admin_user/deleteRatingRecords';

$route['admin/rating/status'] = 'Admin/Admin_user/updateStatusRating';



$route['admin/notifications/list'] = 'Admin/Admin_user/Notifications';

$route['admin/viewnotification/(:any)'] = 'Admin/Admin_user/viewNotification/$1';

$route['admin/delete/notification'] = 'Admin/Admin_user/deleteNotificationRecords';



$route['admin/transcations/list'] = 'Admin/Admin_user/Transcations';

$route['admin/viewtranscation/(:any)'] = 'Admin/Admin_user/viewTranscation/$1';



$route['admin/messages/list'] = 'Admin/Admin_user/Messages';

$route['admin/viewmessage/(:any)'] = 'Admin/Admin_user/viewMessage/$1';

$route['admin/delete/message'] = 'Admin/Admin_user/deleteMessageRecords';



$route['admin/homesection/(:any)'] = 'Content/homeSection/$1';

$route['update/homesection/(:any)'] = 'Content/updatehomeSection/$1';



/*Created by 95 on 4-1-2019 For Doctor Management  Start*/

$route['admin/doctor-category/list'] = 'Admin/Admin_user/doctors';

$route['admin/doctor-category/Add'] = 'Admin/Admin_user/adddoctor_category';

$route['admin/doctor-category/edit/(:any)'] = 'Admin/Admin_user/editdoctor_category/$1';

$route['admin/doctor-category/viewdoctor_category/(:any)'] = 'Admin/Admin_user/viewdoctor_category/$1';

$route['admin/doctor-category/delete'] = 'Admin/Admin_user/deletedoctor_categoryRecords';



$route['admin/add_doctor'] = 'Admin/Doctor/add_doctor';

$route['admin/doctor_list'] = 'Admin/Doctor/doctor_list';

$route['admin/edit_doctor/(:any)'] = 'Admin/Doctor/edit_doctor/$1';

$route['admin/view_doctor/(:any)'] = 'Admin/Doctor/view_doctor/$1';



$route['admin/delete_doctor/(:any)'] = 'Admin/Doctor/delete_doctor/$1';

/*Created by 95 on 4-1-2019 For Doctor Management  End*/

$route['add_Favourite_Photo'] = 'Pages/add_Favourite_Photo';



$route['doctor_speciality'] = 'Pages/doctor_category';



$route['doctor_speciality/(:any)'] = 'Pages/doctor_category/$1';



//$route['view_caregory'] = 'Pages/view_caregory/$1';



$route['doctors/(:any)'] = 'Pages/view_speciality/$1';





$route['admin/add_shedule/(:any)'] = 'Admin/Doctor/add_doctor_shedule/$1';

$route['admin/doctor_shedules'] = 'Admin/Doctor/doctor_shedule_list';



$route['admin/doctor_schedules'] = 'Admin/Doctor/doctor_shedule_list';



$route['admin/delete_shedule_date/(:any)'] = 'Admin/Doctor/delete_shedule_date/$1';

$route['admin/view_shedule_times/(:any)/(:any)'] = 'Admin/Doctor/view_shedule_times/$1/$1';



$route['admin/view_schedule_times/(:any)/(:any)'] = 'Admin/Doctor/view_shedule_times/$1/$1';



$route['admin/delete_shedule_time/(:any)'] = 'Admin/Doctor/delete_shedule_time/$1';

$route['admin/edit_shedule/(:any)'] = 'Admin/Doctor/edit_shedule/$1';



$route['admin/edit_shedule_time/(:any)/(:any)/(:any)'] = 'Admin/Doctor/edit_shedule_time/$1/$1/$1';





$route['admin/edit_schedule_time/(:any)/(:any)/(:any)'] = 'Admin/Doctor/edit_shedule_time/$1/$1/$1';


$route['show_activities']  = 'Reminder/show_activities';

$route['show_activities/add-event']  = 'Reminder/add_event'; 

//$route['show_activities/getData']  = 'Reminder/add_event';  

$route['medicine_list'] = 'Pages/Medicine/show_medicine';





$route['add_medicine'] = 'Pages/Medicine/add_medicine';

$route['add_medicine/(:any)'] = 'Pages/Medicine/add_medicine/$1';



$route['admin/view_music_category/(:any)'] = 'Admin/Music/view_music_category/$1';









$route['delete_shedule/(:any)']  = 'Pages/Medicine/delete_medicineShedule/$1';



$route['edit_shedule/(:any)']  = 'Pages/Medicine/edit_medicine/$1';



$route['edit_schedule/(:any)']  = 'Pages/Medicine/edit_medicine/$1';

$route['view_shedule/(:any)']  = 'Pages/Medicine/view_shedule/$1';



$route['view_schedule/(:any)']  = 'Pages/Medicine/view_shedule/$1';



$route['view_doctor/(:any)']  = 'Pages/view_doctor/$1';



$route['thank_you']  = 'Pages/thank_you';



$route['admin/medicine_shedules'] = 'Admin/Doctor/medicine_shedules';



$route['admin/medicine_schedules'] = 'Admin/Doctor/medicine_shedules';



$route['admin/view_medicine_shedule/(:any)'] = 'Admin/Doctor/view_medicine_shedule/$1';



$route['admin/view_medicine_schedule/(:any)'] = 'Admin/Doctor/view_medicine_shedule/$1';



$route['admin/delete_medicine/(:any)'] = 'Admin/Doctor/delete_medicine/$1';



$route['admin/doctor_appointmensts'] = 'Admin/Doctor/doctor_appointmensts';



$route['admin/view_appointment/(:any)'] = 'Admin/Doctor/view_appointment/$1';



$route['view_apointment_activities/(:any)'] = 'Pages/Activities/view_apointment_activities/$1';



$route['view_reminder_activities/(:any)'] = 'Pages/Activities/view_reminder_activities/$1';

//$route['view_grocery/(:any)'] = 'Pages/Grocery/view_grocery/$1';

$route['view_grocery/(:any)/(:any)'] = 'Pages/Grocery/view_grocery/$1/$2';

$route['view_favorite_grocery/(:any)'] = 'Pages/Grocery/view_favorite_grocery/$1';



// $route['appointment_list'] = 'Appointment/appointment_list';

// $route['edit_appointment/(:any)'] = 'Appointment/edit_appointment/$1';





$route['appointment_list'] = 'Appointment/appointment_list';





$route['delete_appointment/(:any)']  = 'Appointment/delete_appointment/$1';





$route['edit_appointment/(:any)']  = 'Appointment/edit_appointment/$1';







$route['send_acknowledge'] = 'Pages/Acknowledge/send_acknowledge';



$route['acknowledge_list'] = 'Pages/Acknowledge/acknowledge_list';



$route['acknowledge_detail/(:any)'] = 'Pages/Acknowledge/acknowledge_detail/$1';



//$route['test_report'] = 'Pages/Testreport/test_report';





$route['testreport_list'] = 'Pages/Testreport/testreport_list';



$route['add_testreport'] = 'Pages/Testreport/add_testreport';



$route['edit_testreport/(:any)'] = 'Pages/Testreport/edit_testreport/$1';



$route['view_testreport/(:any)'] = 'Pages/Testreport/view_testreport/$1';



$route['send_testreport/(:any)'] = 'Pages/Testreport/send_testreport/$1';



$route['delete_testreport/(:any)']  = 'Pages/Testreport/delete_testreport/$1';

$route['admin/test_type_list'] = 'Admin/Test_type/test_type_list';



$route['admin/add_test_type'] = 'Admin/Test_type/add_test_type';



$route['admin/edit_test_type/(:any)'] = 'Admin/Test_type/edit_test_type/$1';



$route['admin/delete_test_type/(:any)'] = 'Admin/Test_type/delete_test_type/$1';



$route['admin/staff_list'] = 'Admin/StaffManagement/staff_list';

$route['admin/add_staff'] = 'Admin/StaffManagement/add_staff';

$route['admin/delete_staff/(:any)'] = 'Admin/StaffManagement/delete_staff/$1';

$route['admin/edit_staff/(:any)'] = 'Admin/StaffManagement/edit_staff/$1';

$route['admin/check_email'] = 'Admin/StaffManagement/check_email';

$route['admin/check_username'] = 'Admin/StaffManagement/check_username';

$route['admin/check_mobile'] = 'Admin/StaffManagement/check_mobile';

$route['admin/submit_permission'] = 'Admin/StaffManagement/submit_permission';

$route['admin/update_status'] = 'Admin/StaffManagement/update_status';

/*Vital sign routs*/



$route['admin/submit_token'] = 'Admin/Admin_user/submit_token';



/*vital test type*/

$route['admin/vital_sign_list'] = 'Admin/Vital_sign/vital_sign_list';

$route['admin/add_vital_sign'] = 'Admin/Vital_sign/add_vital_sign';

$route['admin/edit_vital_sign/(:any)'] = 'Admin/Vital_sign/edit_vital_sign/$1';

$route['admin/delete_vital_sign/(:any)'] = 'Admin/Vital_sign/delete_vital_sign/$1';







/*Tab feature category list*/

$route['admin/feature_category_list'] = 'Admin/Tabfeatures/feature_category_list';

$route['admin/add_feature_category'] = 'Admin/Tabfeatures/add_feature_cate';

$route['admin/edit_feature_category/(:any)'] = 'Admin/Tabfeatures/edit_feature_category/$1';

$route['admin/delete_feature_category/(:any)'] = 'Admin/Tabfeatures/delete_feature_category/$1';





// about us editor



$route['admin/ticket_list'] = 'Admin/Tickes/tickes_list';

$route['admin/view_detail/(:any)'] = 'Admin/Tickes/view_detail/$1';

$route['admin/reply_to_ticket/(:any)'] = 'Admin/Tickes/reply_to_ticket/$1';

$route['admin/view_ticket_conversation/(:any)'] = 'Admin/Tickes/view_ticket_conversation/$1';

//$route['admin/delete_feature_category/(:any)'] = 'Admin/Tabfeatures/delete_feature_category/$1';



$route['admin/feedback_list'] = 'Admin/Feedback/feedback_list';



$route['admin/about-us'] = 'Admin/Aboutus/add_edit_function';



$route['admin/mail_templates'] = 'Admin/Mail_template/mail_template_list';

$route['admin/edit_template/(:any)'] = 'Admin/Mail_template/edit_template/$1';

$route['admin/view_template/(:any)'] = 'Admin/Mail_template/view_template/$1';



// Pges cms

$route['admin/pages_list'] = 'Admin/Pages_cms/pages_list';

$route['admin/edit_page/(:any)'] = 'Admin/Pages_cms/edit_page/$1';



$route['admin/footer_content_list'] = 'Admin/Footer_cms/footer_content_list';

$route['admin/edit_footer_content/(:any)'] = 'Admin/Footer_cms/edit_footer_content/$1';





// End

/*Tab feature category list*/

$route['admin/feature_list'] = 'Admin/Tabfeatures/feature_list';

$route['admin/add_feature'] = 'Admin/Tabfeatures/add_feature';

$route['admin/edit_feature/(:any)'] = 'Admin/Tabfeatures/edit_feature/$1';

$route['admin/delete_feature/(:any)'] = 'Admin/Tabfeatures/delete_feature/$1';







/*member stories*/

$route['admin/story_list'] = 'Admin/Story/story_list';

$route['admin/add_story'] = 'Admin/Story/add_story';

$route['admin/edit_story/(:any)'] = 'Admin/Story/edit_story/$1';

$route['admin/delete_story/(:any)'] = 'Admin/Story/delete_story/$1';



/*vital sign*/

$route['admin/vital_sign'] = 'Admin/Vital_sign_detail/vital_sign';

$route['admin/view_vital_sign/(:any)'] = 'Admin/Vital_sign_detail/view_vital_sign/$1';

//$route['admin/add_vital_sign'] = 'Admin/Vital_sign/add_vital_sign';

//$route['admin/edit_vital_sign/(:any)'] = 'Admin/Test_type/edit_test_type/$1';

$route['admin/delete_vital_detail/(:any)'] = 'Admin/Vital_sign_detail/delete_vital_detail/$1';





/***End***/

$route['add_acknowledge'] = 'Pages/Acknowledge/add_acknowledge';



$route['edit_acknowledge/(:any)'] = 'Pages/Acknowledge/edit_acknowledge/$1';



$route['delete_acknowledge/(:any)'] = 'Pages/Acknowledge/delete_acknowledge/$1';







$route['admin/subscriptions'] = 'Admin/Subscriptions/subscriptions';
$route['admin/news-letter-subscribers'] = 'Admin/Subscriptions/news_letter_subscribers';



$route['admin/acknowledge_list'] = 'Admin/Acknowledge/acknowledge_list';

$route['admin/add_acknowledge'] = 'Admin/Acknowledge/add_acknowledge';

$route['admin/edit_acknowledge/(:any)'] = 'Admin/Acknowledge/edit_acknowledge/$1';

$route['admin/delete_acknowledge/(:any)'] = 'Admin/Acknowledge/delete_acknowledge/$1';



$route['admin/view_acknowledge/(:any)'] = 'Admin/Acknowledge/view_acknowledge/$1';



$route['admin/articles_list'] = 'Admin/Article/articles_list';

$route['admin/add_article'] = 'Admin/Article/add_article';

$route['admin/edit_article/(:any)'] = 'Admin/Article/edit_article/$1';

$route['admin/delete_article/(:any)'] = 'Admin/Article/delete_article/$1';

$route['admin/view_article/(:any)'] = 'Admin/Article/view_article/$1';



/*Sprituality routs start*/

$route['admin/meditation_articles_list'] = 'Admin/Meditation_article/meditation_articles_list';

$route['admin/add_meditation_article'] = 'Admin/Meditation_article/add_meditation_article';

$route['admin/edit_meditation_article/(:any)'] = 'Admin/Meditation_article/edit_meditation_article/$1';

$route['admin/delete_meditation_article/(:any)'] = 'Admin/Meditation_article/delete_meditation_article/$1';

$route['admin/view_meditation_article/(:any)'] = 'Admin/Meditation_article/view_meditation_article/$1';









/*Meditaion Article Category start*/

$route['admin/add_meditation_article_category'] = 'Admin/Meditation_article/add_meditation_article_category';

$route['admin/edit_meditation_article_category/(:any)'] = 'Admin/Meditation_article/edit_meditation_article_category/$1';

$route['admin/meditation_article_category_list'] = 'Admin/Meditation_article/meditation_article_category_list';

$route['admin/delete_meditation_article_category/(:any)'] = 'Admin/Meditation_article/delete_meditation_article_category/$1';

$route['admin/view_meditation_article_category/(:any)'] = 'Admin/Meditation_article/view_meditation_article_category/$1';

/*Meditaion Article Category end*/









// Meditation Vedio category rout start



$route['admin/add_meditation_video_category'] = 'Admin/Meditation_vedio/add_meditation_vedio_category';

$route['admin/meditation_video_category_list'] = 'Admin/Meditation_vedio/meditation_vedio_category_list';   

$route['admin/edit_meditation_vedio_category/(:any)'] = 'Admin/Meditation_vedio/edit_meditation_vedio_category/$1';

$route['admin/view_meditation_vedio_category/(:any)'] = 'Admin/Meditation_vedio/view_meditation_vedio_category/$1';



//$route['admin/delete_meditation_vedio_category/(:any)'] = 'Admin/Meditation_vedio/delete_meditation_vedio_category/$1';  



// Meditation Vedio category rout end







// Meditation Vedio category rout start



$route['admin/add_meditation_video'] = 'Admin/Meditation_vedio/add_meditation_vedio';

$route['admin/meditation_video_list'] = 'Admin/Meditation_vedio/meditation_vedio_list'; 

$route['admin/delete_meditation_vedio/(:any)'] = 'Admin/Meditation_vedio/delete_meditation_vedio/$1';  

$route['admin/edit_meditation_video/(:any)'] = 'Admin/Meditation_vedio/edit_meditation_vedio/$1';

$route['admin/view_meditation_video/(:any)'] = 'Admin/Meditation_vedio/view_meditation_video/$1';



// Meditation Vedio category rout end





/*End*/

$route['detail_article'] = 'Pages/detail_article';

$route['detail_article/(:any)'] = 'Pages/detail_article/$1';

$route['detail_article_fav/(:any)'] = 'Pages/detail_article_fav/$1';

$route['delete_article/(:any)'] = 'Pages/delete_article/$1';

$route['article_list'] = 'Pages/article_list';

$route['music_detail/(:any)'] = 'Pages/Music/detail_music/$1';



$route['view_all_music/(:any)'] = 'Pages/Music/view_all_music/$1';

//$route['view_all_music/(:any)/(:any)'] = 'Pages/Music/view_all_music_like/$1/$2';

$route['view_all_music/(:any)/(:any)'] = 'Pages/Music/view_all_music_search/$1/$2';

$route['music_list'] = 'Pages/Music/music_list';

$route['delete_favorite/(:any)'] = 'Pages/Music/delete_music/$1';

$route['movie_list'] = 'Pages/Music/movie_list';

$route['movie_list/(:any)'] = 'Pages/Movie/search_newmovie_list/$1';

$route['movie_detail'] = 'Pages/Music/movie_detail';

//$route['articles'] = 'Pages/articles';

$route['favorite_articles'] = 'Pages/favorite_articles';

$route['view_All_articles'] = 'Pages/Article/view_all_article';







$route['admin/music_list'] = 'Admin/Music/music_list';

$route['admin/add_music'] = 'Admin/Music/add_music';

$route['admin/edit_music/(:any)'] = 'Admin/Music/edit_music/$1';

$route['admin/delete_music/(:any)'] = 'Admin/Music/delete_music/$1';

$route['admin/view_music/(:any)'] = 'Admin/Music/view_music/$1';



$route['admin/movie_list'] = 'Admin/Movie/movie_list';

$route['admin/add_movie'] = 'Admin/Movie/add_movie';

$route['admin/edit_movie/(:any)'] = 'Admin/Movie/edit_movie/$1';

$route['admin/delete_movie/(:any)'] = 'Admin/Movie/delete_movie/$1';

$route['admin/view_movie/(:any)'] = 'Admin/Movie/view_movie/$1';







$route['movie_detail/(:any)'] = 'Pages/Movie/detail_movie/$1';

$route['movie_list'] = 'Pages/Movie/movie_list';

$route['delete_favoriteMovie/(:any)'] = 'Pages/Movie/delete_movie/$1';

$route['movie_list'] = 'Pages/Movie/movie_list';

$route['movie_detail'] = 'Pages/Movie/movie_detail';

$route['favoritemovie_list'] = 'Pages/FavoritMovie/Favoritemovie_list';





$route['favoritemusic_detail/(:any)'] = 'Pages/FavoritMusic/detail_favoritemusic/$1';

$route['favoritemusic_list'] = 'Pages/FavoritMusic/Favoritemusic_list';

$route['delete_favoritemusic/(:any)'] = 'Pages/FavoritMusic/delete_favoritemusic/$1';

$route['favoritemusic_list'] = 'Pages/FavoritMusic/Favoritemusic_list';

$route['favoritemusic_detail'] = 'Pages/FavoritMusic/favoritemusic_detail';

$route['detail_favorite_music/(:any)'] = 'Pages/FavoritMusic/favorite_music_detail/$1';

$route['favoritemusic_detail_page/(:any)'] = 'Pages/FavoritMusic/detail_music/$1';

$route['favoritemovie_detail_page/(:any)'] = 'Pages/FavoritMovie/detail_movie/$1';



$route['weekly_weather'] = 'Pages/weekly_weather';

$route['social'] = 'Pages/social'; 


$route['add_social'] = 'Pages/add_social';
$route['edit_social/(:any)'] = 'Pages/edit_social/$1';
$route['delete_social/(:any)'] = 'Pages/delete_social/$1';

$route['transpotation'] = 'Pages/transpotation'; 
$route['add_transpotation'] = 'Pages/add_transpotation'; 
$route['edit_transpotation/(:any)'] = 'Pages/edit_transpotation/$1';
$route['delete_transpotation/(:any)'] = 'Pages/delete_transpotation/$1';

$route['contact_detail'] = 'Pages/contact_detail';

$route['user/update_profile'] = 'Pages/update_profile';

$route['change_password'] = 'Pages/change_password';

$route['favorite_restaurant'] = 'Pages/favorite_restaurant_list';

$route['view_all_categories'] = 'Pages/Music/view_all_categories';

$route['view_all_categories/(:any)'] = 'Pages/Music/view_all_categories_search/$1';



$route['view_favorite_restaurant/(:any)'] = 'Pages/view_favorite_restaurant/$1';

//$route['favorite_restaurant'] = 'Pages/favorite_restaurant_list';

$route['add_pickup_medicine'] = 'Pages/Medicine/add_pickup_medicine';

$route['pickupmedicine_list'] = 'Pages/Medicine/pickupmedicine_list';

$route['delete_pickup/(:any)'] = 'Pages/Medicine/delete_pickup/$1';

$route['note_list'] = 'Pages/Note/note_list';

$route['add_note'] = 'Pages/Note/add_note';

$route['edit_note/(:any)'] = 'Pages/Note/edit_note/$1';

$route['view_note/(:any)'] = 'Pages/Note/view_note/$1';

$route['delete_note/(:any)'] = 'Pages/Note/delete_note/$1';

/*---------------family member START-----------------------------*/



$route['family_member_dashboard'] = 'Family_member/dashboard';

$route['family_member/update_profile'] = 'Family_member/update_profile';

$route['family_member_login'] = 'Family_member/Familymember_login';



$route['family_member/add_contact'] = 'Family_member/Familymember_contact/add_contact';

$route['family_member/contact_list'] = 'Family_member/Familymember_contact/contact_list';

$route['family_member/delete_contact/(:any)'] = 'Family_member/Familymember_contact/delete_contact/$1';

$route['family_member/contact_edit/(:any)'] = 'Family_member/Familymember_contact/contact_edit/$1';



$route['family_member/view_contact/(:any)'] = 'Family_member/Familymember_contact/view_contact/$1';



//$route['nearby_restaurant'] = 'Pages/nearby_restaurant';

$route['nearby_restaurant'] = 'Pages/get_all_restaurants';

$route['nearby_restaurant/radius/(:any)'] = 'Pages/radius_restaurant/$1';

$route['nearby_restaurant/(:any)'] = 'Pages/getsearchdata/$1';

$route['nearby_restaurant/(:any)/(:any)'] = 'Pages/getsearchdata/$1/$2';



$route['view_restaurant/(:any)/(:any)'] = 'Pages/view_restaurant/$1/$2';



/*Articles start*/

//$route['articles'] = 'Pages/Article/article_list';



$route['articles'] = 'Pages/Article/AllCategories';

$route['articles/(:any)'] = 'Pages/Article/AllCategoriessearch/$1';

$route['add_article'] = 'Pages/Article/add_article';

$route['edit_article/(:any)'] = 'Pages/Article/edit_article/$1';

$route['read_article/(:any)/(:any)'] = 'Pages/Article/read_article/$1/$1';

$route['view_All_articles/(:any)/(:any)'] = 'Pages/Article/view_all_article/$1/$2';

$route['view_All_articles/(:any)/(:any)/(:any)'] = 'Pages/Article/view_all_article_search/$1/$2/$3';

$route['category_articles/(:any)'] = 'Pages/Article/ArticlesBycategories/$1';

$route['category_articles/(:any)/(:any)'] = 'Pages/Article/article_list_search/$1/$2';

/*Articles end*/





$route['remove_my_music/(:any)'] = 'Pages/Music/remove_my_music/$1';







$route['view_appointment/(:any)'] = 'Appointment/view_appointment/$1';



$route['view_appointment_notification/(:any)'] = 'Appointment/view_appointment_notification/$1';



$route['grocery'] = 'Pages/Grocery/grocery_list';

$route['grocery/radius/(:any)'] = 'Pages/Grocery/radius_grocery/$1';

$route['grocery/(:any)'] = 'Pages/Grocery/getsearchdata/$1';

$route['grocery/(:any)/(:any)'] = 'Pages/Grocery/getsearchdata/$1/$2';

$route['favorite_grocery'] = 'Pages/Grocery/favorite_grocery_list';



/*---------------family member END-----------------------------*/



/*---------------Caregiver START-----------------------------*/

$route['caregiver_dashboard'] = 'Caregiver/dashboard';

$route['caregiver_login'] = 'Caregiver/Caregiver_login';

/*---------------Caregiver END-----------------------------*/





$route['add_contact'] = 'Pages/UserContact/add_contact';

$route['contact_list'] = 'Pages/UserContact/contact_list';

$route['delete_contact/(:any)'] = 'Pages/UserContact/delete_contact/$1';

$route['contact_edit/(:any)'] = 'Pages/UserContact/contact_edit/$1';

$route['view_contact/(:any)'] = 'Pages/UserContact/view_contact/$1';



$route['admin/contacts_list'] = 'Admin/Contacts/contacts_list';

$route['admin/delete_contacts/(:any)'] = 'Admin/Contacts/delete_contacts/$1';





$route['favorite_article_list'] = 'Pages/favarticle_list';



$route['all_banks'] = 'Pages/all_banks';

$route['all_banks/radius/(:any)'] = 'Pages/radius_bank/$1';

$route['all_banks/(:any)'] = 'Pages/search_bank_list/$1';

$route['all_banks/(:any)/(:any)'] = 'Pages/search_bank_list/$1/$2';





$route['favorite_banks'] = 'Pages/favorite_banks';

//$route['view_bank/(:any)'] = 'Pages/view_bank/$1';

$route['view_bank/(:any)/(:any)'] = 'Pages/view_bank/$1/$2';

$route['view_favorite_bank/(:any)'] = 'Pages/view_favorite_bank/$1';



$route['admin/unit_list'] = 'Admin/Bank/unit_unit';



$route['admin/set_unit'] = 'Admin/Bank/set_unit';



$route['admin/pharmacompany_list'] = 'Admin/Pharma_company/pharmacompany_list';

$route['admin/add_pharmacompany'] = 'Admin/Pharma_company/add_pharmacompany';

$route['admin/edit_pharmacompany/(:any)'] = 'Admin/Pharma_company/edit_pharmacompany/$1';

$route['admin/delete_pharmacompany/(:any)'] = 'Admin/Pharma_company/delete_pharmacompany/$1';


// Option managmet routs
$route['admin/app_list'] = 'Admin/Opetion_management/social_option_list';

$route['admin/add_app'] = 'Admin/Opetion_management/add_app';

$route['admin/edit_app/(:any)'] = 'Admin/Opetion_management/edit_app/$1';

$route['admin/delete_app/(:any)'] = 'Admin/Opetion_management/delete_app/$1';

// End
$route['admin/view_app/(:any)'] = 'Admin/Opetion_management/view_app/$1';



$route['admin/restaurant_list'] = 'Admin/Restaurant/restaurant_list';

$route['admin/favorite_restaurant_list'] = 'Admin/Restaurant/favorite_restaurant_list';

$route['admin/view_restaurant/(:any)'] = 'Admin/Restaurant/view_restaurant/$1';



$route['admin/delete_fav_restaurant/(:any)'] = 'Admin/Restaurant/delete_fav_restaurant/$1';



$route['admin/delete_restaurant/(:any)'] = 'Admin/Restaurant/delete_restaurant/$1';

$route['admin/favorite_grocery_list'] = 'Admin/Grocery/favorite_grocery_list';

$route['admin/view_grocery/(:any)'] = 'Admin/Grocery/view_grocery/$1';

$route['admin/delete_fav_grocery/(:any)'] = 'Admin/Grocery/delete_fav_grocery/$1';



$route['admin/add_music_category'] = 'Admin/Music/add_music_category';



$route['admin/edit_music_category/(:any)'] = 'Admin/Music/edit_music_category/$1';



$route['admin/music_category_list'] = 'Admin/Music/music_category_list';



$route['admin/delete_music_category/(:any)'] = 'Admin/Music/delete_music_category/$1';





$route['admin/add_movie_category'] = 'Admin/Movie/add_movie_category';

$route['admin/movie_category_list'] = 'Admin/Movie/movie_category_list';

$route['admin/edit_movie_category/(:any)'] = 'Admin/Movie/edit_movie_category/$1';

$route['admin/view_movie_category/(:any)'] = 'Admin/Movie/view_movie_category/$1';







/*Bank start*/

$route['admin/bank_list'] = 'Admin/Bank/bank_list';

$route['admin/view_bank/(:any)'] = 'Admin/Bank/view_bank/$1';

$route['admin/delete_bank/(:any)'] = 'Admin/Bank/delete_bank/$1';

/*Bank end*/



/*Favourite Banks start*/

$route['admin/favourite_banks'] = 'Admin/Bank/favourite_bank_list';

$route['admin/view_favourite_bank/(:any)'] = 'Admin/Bank/view_favourite_bank/$1';

$route['admin/delete_favourite_bank/(:any)'] = 'Admin/Bank/delete_favourite_bank/$1';

/*Favourite Banks end*/







/*Grocery start*/

$route['admin/grocery_list'] = 'Admin/Grocery/grocery_list';

$route['admin/view_grocery/(:any)'] = 'Admin/Grocery/view_grocery/$1';

$route['admin/delete_grocery/(:any)'] = 'Admin/Grocery/delete_grocery/$1';

/*Grocery end*/



/*Favourite grocery start*/

$route['admin/favourite_grocery'] = 'Admin/Grocery/favourite_grocery_list';

$route['admin/view_favourite_grocery/(:any)'] = 'Admin/Grocery/view_favourite_grocery/$1';

$route['admin/delete_favourite_grocery/(:any)'] = 'Admin/Grocery/delete_favourite_grocery/$1';

/*Favourite grocery end*/





/*Note Start*/

$route['admin/note_list'] = 'Admin/Note/note_list';

$route['admin/view_note/(:any)'] = 'Admin/Note/view_note/$1';

$route['admin/delete_note/(:any)'] = 'Admin/Note/delete_note/$1';

/*Note End*/



/*My music start*/

$route['mymusic_list'] = 'Pages/MyMusic/mymusic_list';

$route['mymusic_detail/(:any)'] = 'Pages/MyMusic/mymusic_detail/$1';

$route['delete_mymusic/(:any)'] = 'Pages/MyMusic/delete_mymusic/$1';

/*My music end*/









/*Banner start*/

$route['admin/music_banner_list'] = 'Admin/Banner/banner_list';

$route['admin/add_music_banner'] = 'Admin/Banner/add_banner';

$route['admin/edit_music_banner/(:any)'] = 'Admin/Banner/edit_banner/$1';

$route['admin/delete_music_banner/(:any)'] = 'Admin/Banner/delete_banner/$1';

$route['admin/view_music_banner/(:any)'] = 'Admin/Banner/view_banner/$1';

/*Banner end*/







/*Movie Banner start*/

$route['admin/movie_banner_list'] = 'Admin/MovieBanner/moviebanner_list';

$route['admin/add_movie_banner'] = 'Admin/MovieBanner/add_moviebanner';

$route['admin/edit_movie_banner/(:any)'] = 'Admin/MovieBanner/edit_moviebanner/$1';

$route['admin/delete_movie_banner/(:any)'] = 'Admin/MovieBanner/delete_moviebanner/$1';

$route['admin/view_movie_banner/(:any)'] = 'Admin/MovieBanner/view_moviebanner/$1';

/*Movie Banner end*/





/*Meditation video Banner start*/

$route['admin/meditation_video_banner_list'] = 'Admin/MeditationvideoBanner/meditation_video_banner_list';

$route['admin/add_meditation_video_banner'] = 'Admin/MeditationvideoBanner/add_meditation_video_banner';

$route['admin/edit_meditation_video_banner/(:any)'] = 'Admin/MeditationvideoBanner/edit_meditation_video_banner/$1';

$route['admin/delete_meditation_video_banner/(:any)'] = 'Admin/MeditationvideoBanner/delete_meditation_video_banner/$1';

$route['admin/view_meditation_video_banner/(:any)'] = 'Admin/MeditationvideoBanner/view_meditation_video_banner/$1';

/*Meditation video Banner end*/







/*Article Category start*/

$route['admin/add_article_category'] = 'Admin/Article/add_article_category';

$route['admin/edit_article_category/(:any)'] = 'Admin/Article/edit_article_category/$1';

$route['admin/article_category_list'] = 'Admin/Article/article_category_list';

$route['admin/delete_article_category/(:any)'] = 'Admin/Article/delete_article_category/$1';

$route['admin/view_article_category/(:any)'] = 'Admin/Article/view_article_category/$1';

/*Article Category end*/





/*Add movie by user start*/

$route['add_movie'] = 'Pages/Movie/add_movie';

$route['edit_movie/(:any)'] = 'Pages/Movie/edit_movie/$1';

$route['view_allmovies/(:any)'] = 'Pages/Movie/view_allmovies/$1';

$route['view_allmovies/(:any)/(:any)'] = 'Pages/Movie/all_movie_search/$1/$2';



$route['view_all_movies_categories'] = 'Pages/Movie/view_All_Categories';

$route['view_all_movies_category/(:any)'] = 'Pages/Movie/movies_by_categories/$1';

/*Add movie by user end*/





/*Reminder Notifications*/



$route['view_notification/(:any)'] = 'Reminder/Reminder/view_notification/$1';

$route['view_reminder/(:any)'] = 'Reminder/Reminder/view_reminder/$1';



//$route['view_all_Notification'] = 'Reminder/Reminder/view_all_Notification';

/*End*/





//$route['admin/delete/email'] = 'Admin/Contacts/deletecontactRecords';



$route['payment/(:any)'] = 'Pages/Login/payment_process/$1';

//$route['payment-process'] = 'Pages/Payments/ProcessPayment';





$route['make-payment/(:any)/(:any)'] = 'Pages/Payments/ViewPayment/$1/$1';

$route['payment-process'] = 'Pages/Payments/ProcessPayment';

$route['success/(:any)'] = 'Pages/Payments/paymentsuccess/$1';

$route['failed/(:any)'] = 'Pages/Payments/paymentfailed/$1';