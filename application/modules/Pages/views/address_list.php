<style>
    .rem_add .btn.btn-primary {
        margin: 0px 0 14px !important;
    }
.text_heading {
    border-bottom: 0;
}
</style>
<div class=" text_heading">
    <?php if ($this->session->flashdata('success')) { ?>

        <div class="alert alert-success message">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <?php echo $this->session->flashdata('success'); ?>
        </div>

    <?php } ?>
    <h3 class="text-center"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Address Book</h3>


    <?php
    if (is_array($addresses)) {
        foreach ($addresses as $list) {
            $cnt_addresses = count($addresses);
            $addres_id = $list->address_id;
            if ($cnt_addresses == 1) { ?>
              <div class="rem_add">  <a href="<?php echo base_url(); ?>edit_address/<?php echo $addres_id; ?>" class="btn btn-primary">Edit</a></div>
            <?php } else { ?>
                <div class="rem_add"><a href="<?php echo base_url(); ?>add_address" class="btn btn-primary">Add</a> </div>
            <?php }    ?>
    <?php }
    }else{ ?>
    <div class="rem_add"> <a href="<?php echo base_url(); ?>add_address" class="btn btn-primary">Add </a></div>

  <?php  }
    ?>

</div>

</h1>

</div>

<div id="expire_msg"></div>
<section class="content photo-list">
    <div class="photo-listMain reminder_listMain">
        <div class="table-responsive tb_swt">
            <table class="table table-hover table-bordered table-mailbox send-user-mail tab_medi" id="sampleTable">
                <thead>
                    <tr>
                        <th>Address</th>
                        <th>Created/Updated</th>
                        <th>Action</th>                    </tr>
                </thead>
                <tbody>
                    <?php

                    $i = 1;

                    //echo "<pre>"; print_r($user_data);

                    if (!empty($addresses)) {
                        foreach ($addresses as $list) { ?>
                            <tr>
                                <td data-label="Address"><?php echo $list->address; ?></td>
                                <td data-label="Created/Updated"><?php echo $list->created; ?></td>
                                <td data-label="Action" class="act">

                                    <a href="<?php echo base_url(); ?>view_address/<?php echo $list->address_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

                                    <a href="<?php echo base_url(); ?>edit_address/<?php echo $list->address_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                                    <a href="<?php echo base_url(); ?>delete_address/<?php echo $list->address_id; ?>" onclick="return confirm('Are you sure, you want to remove this address ?')"><i class="fa fa-trash" aria-hidden="true"></i></a>

                                </td>
                            </tr>
                    <?php }
                    } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
</aside>

<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>