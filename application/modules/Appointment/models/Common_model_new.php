<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_model_new extends CI_Model{

    function __construct() {

        

    }

	

		public function insertData($table,$datainsert)

	{

		$this->db->insert($table,$datainsert);

		return $this->db->insert_id();

	}

	

	public function updateData($table,$data,$where)

	{



		//die('kahkhk');

	   $this->db->update($table,$data,$where);

	   return $this->db->affected_rows();

	}

	

	public function delete($table,$where)

	{

	   $this->db->delete($table,$where);

	   return;

	}

	public function deleteFaqs($table,$where)

	{

		extract($where);

		$this->db->where('faq_id', $faq_id);

		$this->db->update($table, array('status' => 3));

		return true;

	}

	

	public function getsingle($table,$where)

	{

		$q = $this->db->get_where($table,$where);
		// echo $this->db->last_query();

		// die;
		return $q->row();

	}



    

	public function check_login_credentials($postdata){ 

		extract( $postdata ); 

		$this->db->select('*');

		//$this->db->from('admin');

		$this->db->from('admin');

		$this->db->where('username', $uname); 

		$this->db->where('password', md5($password));

		//$this->db->where('role', 1); 

		$query = $this->db->get();

		return $query->row();

		

	}

	

	 public function get_row_with_con($table,$where)

	 {

	 	$this->db->select('*');

		$this->db->from($table);

		$this->db->where($where);

		$query = $this->db->get();

		//echo $this->db->last_query();

		//die;

		return $query->row(); 

	 } 

    

   public function getAllwhereorderby($table,$where,$order_id,$order_by)

	{

		$this->db->select('*');

		$this->db->order_by($order_id,$order_by);

		$q = $this->db->get_where($table,$where);	

		$num_rows = $q->num_rows();		

		if($num_rows >0)

		{

			foreach($q->result() as $row)

			{

				$data[] = $row;

			}

			$q->free_result();

			return $data;

		}

	}

	

	public function getAll($table)

	{

		$this->db->select('*');		

		$q = $this->db->get($table);		

		$num_rows = $q->num_rows();		

		if($num_rows >0)

		{

			foreach($q->result() as $row)

			{

				$data[] = $row;

			}

			$q->free_result();

			return $data;

		}

	}

	

	

    public function getAllFaq()

    {

        $this->db->select('*');

        $this->db->from("faqs");

        $this->db->where("status !=", 3);

        $query = $this->db->get();

        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

       return $result;

    }

	

	public function getAllor($table,$order_id,$order_by)

	{

		$this->db->select('*');	

        $this->db->order_by($order_id,$order_by);		

		$q = $this->db->get($table);		

		$num_rows = $q->num_rows();		

		if($num_rows >0)

		{

			foreach($q->result() as $row)

			{

				$data[] = $row;

			}

			$q->free_result();

			return $data;

		}

	}

	

	

	public function getAllwhere($table,$where)

	{ 

		$this->db->select('*');

		$q = $this->db->get_where($table,$where);

		$num_rows = $q->num_rows();

		if($num_rows >0)

		{

			foreach($q->result() as $row)

			{

				$data[] = $row;

			}

			$q->free_result();

			return $data;

		}

	}

	

	function jointwotablenn($table, $field_first, $table1, $field_second,$where='',$field,$order_id,$order_by) {



        $this->db->select($field);

	   $this->db->order_by( $order_id, $order_by);

        $this->db->from("$table");

        $this->db->join("$table1", "$table1.$field_second = $table.$field_first"); 

        if($where !=''){

        $this->db->where($where); 

        }

        $q = $this->db->get();
	

        if($q->num_rows() > 0) {

            foreach($q->result() as $rows) {

                $data[] = $rows;

            }

            $q->free_result();

            return $data;

        }

    }

     public function lang_data(){

        if($this->session->userdata("filterLanguage1")){

            $lang = $this->session->userdata("filterLanguage1");

        }else{

            $lang = 1;

        }

        $this->db->select('*');

        $this->db->from('language_contents');

        $this->db->where('content_language_id',$lang);

        $query = $this->db->get();

        $result = $query->result_array();

        return $result;

    }

	

	 function getfootercategories($lang){



       $this->db->select('*');	   

       $this->db->from("gc_footer_category_detail");

        $this->db->join("gc_footer_data_detail", "gc_footer_data_detail.cat_id = gc_footer_category_detail.cat_id","RIGHT"); 

       $this->db->where("gc_footer_data_detail.status", 1);

	   $this->db->where("gc_footer_category_detail.status", 1);

	   $this->db->where("gc_footer_category_detail.lang_id", $lang);

       

       $this->db->group_by("gc_footer_category_detail.cat_name");

       $this->db->order_by("gc_footer_category_detail.cat_id", "ASC");

       $query = $this->db->get();        

   //echo $this->db->last_query();die;    

       foreach ($query->result() as $category)

   {

       $return[$category->cat_id] = $category;

       $return[$category->cat_id]->subs = $this->get_sub_categories($category->cat_id,$lang); // Get the categories sub categories

   }



   return $return;   

   }

   

   public function get_sub_categories($category_id,$lang)

{

   $this->db->where('cat_id', $category_id);

    $this->db->where('status',1); 

 $this->db->where('lang_id',$lang);	

   $query = $this->db->get('gc_footer_data_detail');

  //echo $this->db->last_query();

  return $query->result();



}



function jointwotable($table, $field_first, $table1, $field_second,$where='',$field) {



        $this->db->select($field);

        $this->db->from("$table");

        $this->db->join("$table1", "$table1.$field_second = $table.$field_first","left"); 

        if($where !=''){

        $this->db->where($where); 

        }

        $q = $this->db->get();
		// echo $this->db->last_query();die;
        if($q->num_rows() > 0) {

          return $q->row();

             

        }else{

			return false;

		}

    }







/*Created by 95 for show appointmets*/

    public function getAllappointments($user_id)

    {

    	 $this->db->select('*');

      	 $this->db->from('doctor_appointments as t1');

	     $this->db->join('cp_doctor as t2', 't1.doctor_id = t2.doctor_id', 'LEFT');

	     $this->db->join('cp_users as t3', 't3.id = t1.user_id', 'LEFT');

	     $this->db->join('dr_available_date as t4', 't4.avdate_id = t1.dr_appointment_date_id', 'LEFT');

	     $this->db->join('dr_available_time as t5', 't5.avtime_id = t1.dr_appointment_time_id', 'LEFT');

	     $this->db->where('t3.user_role!=','1');

		 $this->db->where('t3.id',$user_id);

	     $query = $this->db->get();

         return $query->result_array();

    }





   	/*Created by 95 for show single appointment*/

    public function getAppointment($id)

    {
		$checkLogin = $this->session->userdata('logged_in')['id'];


		$sql = "select apt.dr_appointment_id,apt.status,apt.doctor_id,d.doctor_id,d.other_doctor_name,d.doctor_name,d.doctor_email,d.doctor_mob_no,d.fax_num,d.doctor_address from doctor_appointments apt
		join cp_doctor d on d.doctor_id = apt.doctor_id
		where apt.user_id= $checkLogin AND apt.dr_appointment_id= $id";
		$q = $this->db->query($sql);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
    	//  $this->db->select('*');

      	//  $this->db->from('doctor_appointments as t1');

      	//  $this->db->where('t1.dr_appointment_id',$id);

      	//  //$this->db->join('cp_doctor as t2', 't1.doctor_id = t2.doctor_id', 'LEFT');

	    //  $this->db->join('cp_users as t2', 't2.id = t1.user_id', 'LEFT');

	    //  $this->db->join('dr_available_date as t3', 't3.avdate_id = t1.dr_appointment_date_id', 'LEFT');

	    //  $this->db->join('dr_available_time as t4', 't4.avtime_id = t1.dr_appointment_time_id', 'LEFT');

	    //  $this->db->where('t2.user_role!=','1');

	    //  $query = $this->db->get();

        //  return $query->result();

    }





    /*Created by 95 to updat appointment date only*/

    public function updateAvdate($data,$where)

	{

	   $this->db->update('dr_available_date',$data,$where);

	   return $this->db->affected_rows();

	}



	/*Created by 95 to updat appointment time only*/

	public function updateAvtime($data,$wheretime)

	{

	   $this->db->update('dr_available_time',$data,$wheretime);

	   return $this->db->affected_rows();

	}





	function jointwotablegroupby($table, $field_first, $table1, $field_second,$groupby,$where='',$field) {



        $this->db->select($field);

        $this->db->from("$table");

		$this->db->group_by($groupby);

        $this->db->join("$table1", "$table1.$field_second = $table.$field_first"); 

        if($where !=''){

        $this->db->where($where); 

        }

        $q = $this->db->get();

        if($q->num_rows() > 0) {

            foreach($q->result() as $rows) {

                $data[] = $rows;

            }

            $q->free_result();

            return $data;

        }

    }



function jointhreetablegroupby($table, $field_first, $table1, $field_second,$table2,$field_third,$value,$groupby,$where='',$field) {



        $this->db->select($field);

        $this->db->from("$table");

		$this->db->group_by($groupby);

        $this->db->join("$table1", "$table1.$field_second = $table.$field_first"); 

		$this->db->join("$table2", "$table2.$field_third = $table.$value");

        if($where !=''){

        $this->db->where($where); 

        }

        $q = $this->db->get();

        if($q->num_rows() > 0) {

            foreach($q->result() as $rows) {

                $data[] = $rows;

            }

            $q->free_result();

            return $data;

        }

    }



	function jointwotablegroupby_order($table, $field_first, $table1, $field_second,$groupby,$where='',$field,$order_id,$order_by) {



        $this->db->select($field);

        $this->db->from("$table");

		$this->db->group_by($groupby);

		$this->db->order_by($order_id,$order_by);

        $this->db->join("$table1", "$table1.$field_second = $table.$field_first"); 

        if($where !=''){

        $this->db->where($where); 

        }

        $q = $this->db->get();

        if($q->num_rows() > 0) {

            foreach($q->result() as $rows) {

                $data[] = $rows;

            }

            $q->free_result();

            return $data;

        }

    }

	public function getCategory($id){

       $id = explode(',',$id);

       $this->db->select('dc_name');

       $this->db->from('cp_doctor_categories');

       $this->db->where_in('dc_id',$id);

       $query = $this->db->get();

       $result = $query->result_array();

       $last_names1 = array_column($result, 'dc_name');

       $result_data = implode(', ',$last_names1);

       return $result_data;

     }

	

	public function getDetailField($where){ 

		$whereCondititon = $where['search'];

		$doctor_name = $whereCondititon["doctor_name"];

		$username = $whereCondititon["username"];

		$checkLogin = $this->session->userdata('logged_in');

		$userid = $checkLogin['id'];

    		

		$sql = 'SELECT `doctor_appointments`.* ,`cp_users`.`username`,`cp_users`.`id`,`cp_doctor`.`doctor_category_id`,`cp_doctor`.`doctor_pic`,`cp_doctor`.`doctor_name`,`dr_available_date`.`avdate_dr_id`, `dr_available_date`.`avdate_date`,`dr_available_time`.`avtime_text`,`dr_available_time`.`avtime_day_slot` FROM `doctor_appointments` JOIN `cp_users` ON `cp_users`.`id` = `doctor_appointments`.`user_id` JOIN `dr_available_date` ON `doctor_appointments`.`dr_appointment_date_id` = `dr_available_date`.`avdate_id` JOIN `cp_doctor` ON `doctor_appointments`.`doctor_id` = `cp_doctor`.`doctor_id` JOIN `dr_available_time` ON `doctor_appointments`.`dr_appointment_time_id` = `dr_available_time`.`avtime_id` WHERE `doctor_appointments`.`status` = 0 AND `doctor_appointments`.`user_id` ='.$userid; 

		

		if(!empty($whereCondititon["doctor_name"])){

			$sql .= " AND `cp_doctor`.`doctor_name` LIKE '$doctor_name%'";

			

		}

		if(!empty($whereCondititon["datepicker"]) && !empty($whereCondititon["datepicker2"])){

			$sql .= ' AND `dr_available_date`.`avdate_date` BETWEEN "'.$whereCondititon["datepicker"].'" AND "'.$whereCondititon["datepicker2"].'"';

		}

		

		if(!empty($whereCondititon["datepicker"]) && empty($whereCondititon["datepicker2"])){

			$sql .= ' AND `dr_available_date`.`avdate_date`="'.$whereCondititon["datepicker"].'"';

		}

		

		if(empty($whereCondititon["datepicker"]) && !empty($whereCondititon["datepicker2"])){

			$sql .= ' AND `dr_available_date`.`avdate_date`="'.$whereCondititon["datepicker2"].'"';

		}

		

		if(!empty($whereCondititon["selector"])){

			$sql .= ' AND `dr_available_time`.`avtime_day_slot`="'.$whereCondititon["selector"].'"';

		}

				

		$q=$this->db->query($sql);	



//print_r($this->db->last_query());die;

		$num_rows = $q->num_rows();

	

		if($num_rows >0)

		{	

			foreach($q->result() as $row)

			{

				$data[] = $row;

			}

			$q->free_result();

			return $data;

		}

		

	}

	

		public function getAllorderby($table,$order_id,$order_by)

	{

		$this->db->select('*');	

        $this->db->order_by($order_id,$order_by);		

		$q = $this->db->get($table);		

		$num_rows = $q->num_rows();		

		if($num_rows >0)

		{

			foreach($q->result() as $row)

			{

				$data[] = $row;

			}

			$q->free_result();

			return $data;

		}

	}

	

	public function getAllappointmentsquery($per_page,$page)

	{

		$checkLogin = $this->session->userdata('logged_in');

		

		$sql = 'SELECT * FROM `doctor_appointments` as `t1` LEFT JOIN `cp_doctor` as `t2` ON `t1`.`doctor_id` = `t2`.`doctor_id` LEFT JOIN `cp_users` as `t3` ON `t3`.`id` = `t1`.`user_id` LEFT JOIN `dr_available_date` as `t4` ON `t4`.`avdate_id` = `t1`.`dr_appointment_date_id` LEFT JOIN `dr_available_time` as `t5` ON `t5`.`avtime_id` = `t1`.`dr_appointment_time_id` WHERE `t3`.`user_role` != 1 AND `t3`.`id` = '.$checkLogin['id'].' ORDER BY `dr_appointment_id` DESC LIMIT '.$page.','.$per_page;

		 $q = $this->db->query($sql);

		 $data =  array() ;

		 

		//echo $this->db->last_query();die;

		 foreach($q->result_array() as $row)

			{

				$data[]  = $row;

				

			}	

             $ret['rows'] = $data;

	  $sql2 = 'SELECT * FROM `doctor_appointments` as `t1` LEFT JOIN `cp_doctor` as `t2` ON `t1`.`doctor_id` = `t2`.`doctor_id` LEFT JOIN `cp_users` as `t3` ON `t3`.`id` = `t1`.`user_id` LEFT JOIN `dr_available_date` as `t4` ON `t4`.`avdate_id` = `t1`.`dr_appointment_date_id` LEFT JOIN `dr_available_time` as `t5` ON `t5`.`avtime_id` = `t1`.`dr_appointment_time_id` WHERE `t3`.`user_role` != 1 AND `t3`.`id` = '.$checkLogin['id'].' ORDER BY `dr_appointment_id` DESC';

	    $q = $this->db->query($sql2);

            $data = array();

			foreach($q->result_array() as $row)

			{

				$data[]  = $row;

			}

			$ret['num_rows'] = count($data);

			//print_r($ret);die;

			return $ret;			

		

	}

	

	 function getAllDoctorappointmets($table, $where='',$field,$group_by) {

  

      $this->db->select($field);

  

  $this->db->group_by($group_by); 

      $this->db->from("$table");

     

       if($where !=''){

      $this->db->where($where); 

       }

       $q = $this->db->get();



       if($q->num_rows() > 0) {

           foreach($q->result_array() as $rows) {

               $data[] = $rows;

           }

           $q->result_array();

           return $data;

       }

   }

	

	public function getappointmentview($appointment_id)

	{

		$checkLogin = $this->session->userdata('logged_in');

		

		$sql = 'SELECT `doctor_appointments`.* ,`cp_users`.`username`,`cp_users`.`id`,`cp_doctor`.`doctor_category_id`,`cp_doctor`.`doctor_pic`,`cp_doctor`.`doctor_name`,`dr_available_date`.`avdate_dr_id`, `dr_available_date`.`avdate_date`,`dr_available_time`.`avtime_text`,`dr_available_time`.`avtime_day_slot` FROM `doctor_appointments` JOIN `cp_users` ON `cp_users`.`id` = `doctor_appointments`.`user_id` JOIN `dr_available_date` ON `doctor_appointments`.`dr_appointment_date_id` = `dr_available_date`.`avdate_id` JOIN `cp_doctor` ON `doctor_appointments`.`doctor_id` = `cp_doctor`.`doctor_id` JOIN `dr_available_time` ON `doctor_appointments`.`dr_appointment_time_id` = `dr_available_time`.`avtime_id` WHERE `doctor_appointments`.`dr_appointment_id` ='.$appointment_id; 

		 $q=$this->db->query($sql);	

        if($q->num_rows() > 0) {

          return $q->row();

             

        }else{

			return false;

		}

	}
/************GEt All schedule list */
	function getAllList( $user_id)
{
	$sql = "select DISTINCT apt.dr_appointment_id,apt.dr_appointment_date_id,apt.dr_appointment_time_id,apt.status,apt.doctor_id,d.doctor_id,d.other_doctor_name,d.doctor_name,d.doctor_email,d.doctor_mob_no,d.fax_num,d.doctor_address from doctor_appointments apt
	join cp_doctor d on d.doctor_id = apt.doctor_id
	where apt.user_id= $user_id AND apt.status ='1'";
	
	// print_r($sql);
	
	
	$q = $this->db->query($sql);
	$data =  array();
	foreach ($q->result_array() as $row) {
		$data[]  = $row;
	}
	return $data;
}

}