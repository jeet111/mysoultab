<style type="text/css">
  html {
    width: 100%;
    overflow-x: hidden;
    overflow-y: hidden;
  }
  
</style>

<div class="signup-page  sign_up_m" id="style-4">
  <section class="signup login-s_n">
    <!-- <img src="images/signup-bg.jpg" alt=""> -->
    <div class="container">
      <div class="row">

        <div class="main_sig_sec_n">
          <div class="sign_head_s">  <h2 class="form-title rig_tx">Register Information</h2> </div>
          <div class="all_log_in sn_up">



            <div class="col-md-6">    

             <div class="signup-content log_right_s">


              <?php
              if($this->session->flashdata('success')) {
               $message = $this->session->flashdata('success');
               ?>
               <div style="color:green;text-align: center;"><?php echo $message['message']; ?></div>
             <?php } ?>
             <form method="POST" id="xyz" class="signup-form" action="<?php echo base_url();?>signup/<?php echo base64_encode($plan_id); ?>" enctype="multipart/form-data">
               <input type="hidden" name="plan_id" value="<?php echo $plan_id; ?>">



               <div class="form-group reg_one1 reg_t">
                <i class="fa fa-user-o" aria-hidden="true"></i>
                <input type="text" class="form-input" name="firstname" id="firstname" placeholder="Enter your first name" value="<?php echo set_value('firstname'); ?>" onkeypress="return onlyAlphabets(event,this);"/>
                <td class="error">
                 <?php echo form_error('firstname'); ?>
               </td>
             </div>

             <div class="form-group reg_one1 ">
              <i class="fa fa-user-o" aria-hidden="true"></i>
              <input type="text" class="form-input" name="lastname" id="lastname" placeholder="Enter your last name" value="<?php echo set_value('lastname'); ?>" onkeypress="return onlyAlphabets(event,this);"/>
              <td class="error">
               <?php echo form_error('lastname'); ?>
             </td>
           </div>

           <div class="form-group reg_one1 reg_six">
            <i class="fa fa-map-marker" aria-hidden="true"></i>
            <input type="text" class="form-input" name="address" id="address" placeholder="Enter your address" value="<?php echo set_value('address'); ?>"/>
            <td class="error">
             <?php echo form_error('address'); ?>
           </td>
         </div>


         <div class="form-group reg_one1 reg_six3">
          <i class="fa fa-map-marker" aria-hidden="true"></i>
          <input type="text" class="form-input" name="city" id="city" placeholder="City" value="<?php echo set_value('city'); ?>" />

          <td class="error">
           <?php echo form_error('city'); ?>
         </td>
       </div>


       <div class="form-group reg_one1 reg_six4">
        <i class="fa fa-map-marker" aria-hidden="true"></i>

        <input type="text" class="form-input" name="state" id="state" placeholder="State" value="<?php echo set_value('state'); ?>" />
        <td class="error">
         <?php echo form_error('state'); ?>
       </td>
     </div>

     <div class="form-group reg_one1 reg_six5">
      <i class="fa fa-map-marker" aria-hidden="true"></i>

      <input type="text" class="form-input" name="zipcode" id="zipcode" placeholder="Zip code" value="<?php echo set_value('zipcode'); ?>" />


      <td class="error">
       <?php echo form_error('zipcode'); ?>
     </td>
   </div>


 </div>




</div>


<div class="col-md-6">    
 <div class="signup-content log_right_s">
  <div class="form-group reg_one1 reg_one">
   <i class="fa fa-envelope-o" aria-hidden="true"></i>
   <input type="email" class="form-input " name="email" id="email" placeholder="Enter your email" value="<?php echo set_value('email'); ?>" />
   <td class="error">
    <?php echo form_error('email'); ?>
  </td>
</div>

<div class="form-group reg_one1 reg_th">
 <i class="fa fa-map-o" aria-hidden="true"></i>
 <input type="text" class="form-input" name="area_code" id="area_code" placeholder="Area code" value="<?php echo set_value('area_code'); ?>" />
 <td class="error">
  <?php echo form_error('area_code'); ?>
</td>
</div>

<div class="form-group reg_one1 reg_eig">
 <i class="fa fa-mobile" aria-hidden="true"></i>
 <input type="text" class="form-input" name="mobile" id="mobile" placeholder="Enter your phone number" value="<?php echo set_value('mobile'); ?>" />
 <td class="error">
  <?php echo form_error('mobile'); ?>
</td>
</div>



<div class="form-group reg_one1">
 <i class="fa fa-building-o" aria-hidden="true"></i>
 <input type="text" class="form-input" name="company_school" id="company_school" placeholder="Company/ School" value="<?php echo set_value('company_school'); ?>" />
 <td class="error">
  <?php echo form_error('company_school'); ?>
</td>
</div>

<!-- <div class="form-group reg_one1 reg_fo">
                        <i class="fa fa-user-o" aria-hidden="true"></i>
                        <input type="text" class="form-input" name="username" id="username" placeholder="Enter username" value="<?php echo set_value('username'); ?>" onkeypress="return onlyAlphabets(event,this);"/>
                        <td class="error">
                           <?php echo form_error('username'); ?>
                        </td>
                      </div> -->

                    <!--  <div class="form-group reg_one1 reg_fiv">
                        <label class="radio-inline">
                           <input type="radio" name="gender" value="1" checked>
                           <span> Male</span>
                        </label>
                        <label class="radio-inline">
                           <input type="radio" name="gender" value="2">
                           <span> Female </span>
                        </label>
                        <td class="error">
                           <?php echo form_error('gender'); ?>
                        </td>
                      </div> -->



                     <!-- <div class="form-group reg_one1 reg_sev">
                       <i class="fa fa-user-o" aria-hidden="true"></i>
                       <input type="text" class="form-input" name="user_id" id="user_id" placeholder="Enter your user id" value="<?php echo set_value('user_id'); ?>" />
                       <td class="error">
                        <?php echo form_error('user_id'); ?>
                     </td>
                   </div> -->


                  <!-- <div class="form-group reg_one1 reg_nin">
                     <i class="fa fa-calendar" aria-hidden="true"></i>
                     <input type="text" class="form-input" name="dob" id="dob" placeholder="Enter your dob" value="<?php echo set_value('dob'); ?>" />
                     <td class="error">
                        <?php echo form_error('dob'); ?>
                     </td>
                   </div> -->

                   <div class="form-group reg_one1 reg_ten">
                    <i class="fa fa-lock" aria-hidden="true"></i>
                    <input type="password" class="form-input" name="password" id="password" placeholder="Enter your password" value="<?php echo set_value('password'); ?>" />
                    <span toggle="#password" class="toggle-password"></span>
                    <td class="error">
                     <?php echo form_error('password'); ?>
                   </td>
                 </div>
                 <div class="form-group reg_one1 reg_el">
                  <i class="fa fa-lock" aria-hidden="true"></i>
                  <input type="password" class="form-input" name="re_password" id="re_password" placeholder="Repeat your password" value="<?php echo set_value('re_password'); ?>" />
                  <span toggle="#password" class="toggle-password"></span>
                  <td class="error">
                    <?php echo form_error('re_password'); ?>
                  </td>
                </div>
               <?php /* ?>
               <div class="form-group">
                  <select class="form-input" name="user_role" id="user_role">
                     <option value="">Select user type</option>
                     <option value="4" <?php if(set_value('user_role') == 4) echo"selected";?>>Caregiver</option>
                     <option value="3" <?php if(set_value('user_role') == 3) echo"selected";?>>Family Member</option>
                     <option value="2"<?php if(set_value('user_role') == 2) echo"selected";?>>User</option>
                  </select>
                  <td class="error"><?php echo form_error('user_role'); ?></td>
               </div>
               <?php */ ?>

               <!-- </form> -->



             </div>






           </div>





      <!-- <div class="col-md-6">
        <div class="sign_right_s">
         <div class="log_left_s_m">
            <h1 class="head_txt_s"><img src="https://mobivdigital.com/carepro/assets/images/logo.png"></h1>
            <h3>Welcome to Soultab</h3>
            <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. It is a long established fact that a reader will be when at its layout.</span>
         </div></div>
       </div> -->




     </div>


     <div class="sig_bott_s">

       <div class="form-group reg_one1 reg_tw">
        <div class="agreeterm">
          <input type="checkbox" name="agreeterm" id="agreeterm" class="agree-term" value="1" />
          <label for="agree-term" class="label-agree-term"><span>I agree all statements in <a href="term_and_condition" class="term-service">Terms of service</a></span></label>
        </div>
        <td class="error">
          <?php echo form_error('agree-term'); ?>
        </td>
      </div>
      <div class="loader-image" id="loading2" style="display:none;height: auto;width: auto;">
        <img src="<?php echo base_url('assets/images/ajax-loader-blue.gif') ?>" alt="" />
      </div>
      <div class="form-group reg_one1 reg_othr">
        <input type="submit" name="nnsubmit" id="btnsubmit" class="form-submit" value="Continue"/>
        <!-- <input type="submit" name="nnsubmit" id="btnsubmit" class="form-submit" value="Buy Now"/> -->
      </div>
    </form>

    <p class="loginhere">
      Already have an account ? <a href="login" class="loginhere-link">Login here</a>
    </p>
  </div>



</div>

</div>

</div>
</section>
</div>










<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>



<script type="text/javascript">
  $("#xyzs").validate({
         // Specify validation rules
         rules: {
          // username:{
          //   required: true,
          //   maxlength: 30,
          // },
          firstname:{
            required: true
          },
          lastname:{
            required: true
          },
          // gender:{
          //   required: true
          // },
          address:{
            required: true
          },

          city:{
            required: true
          },

          state:{
            required: true
          },

          zipcode:{
            required: true
          },
          area_code:{
            required: true
          },
          company_school:{
            required: true
          },
          email: {
           required: true,
           email: true,
           maxlength: 40,
           remote: {
            url: '<?php base_url(); ?>'+"validate/users/data",
            type: "post",
          }
        },
        mobile:{
         required:true,
         number:true,
         minlength: 10,
         maxlength: 14,
       },
       password: {
         required: true,
         minlength: 8,
         maxlength: 30,
         pwcheck:true
       },
       re_password : {
         required:true,
         minlength : 6,
         maxlength: 30,
         equalTo : "#password"
       },
            //user_role : {
            //required:true
            //},
            agreeterm : {
              required:true
            }
          },
         // Specify validation error messages
         messages: {

          firstname:{
            required:"Please enter your first name",
          },
          lastname:{
            required:"Please enter your last name",
          },
          // username: {
          //   required:"Please enter your user name.",
          //   maxlength: "Please enter maximum 30 characters."
          // },
          // user_id: {
          //   required: "Please enter your user id ",
          //   remote: 'This userid is already exit, please login.',
          //   minlength:'Please enter at least 6 characters.',
          //   useridmatch:'Please enter number and words only.',
          // },

          city:{
            required: "Please enter your city.",
          },

          state:{
            required: "Please enter your state",
          },

          zipcode:{
            required: "Please enter your zipcode",
          },
          area_code:{
            required: "Please enter your area code",
          },
          company_school:{
            required: "Please enter your company or school name",
          },

          email: {
            required: "Please enter your email.",
            email: "Please enter valid email.",
            remote: "Email id is already exist, please login.",
            maxlength: "Please enter maximum 40 characters.",
          },
          mobile:{
            required:"Please enter your phone number.",
            number:"Please enter only number",
            minlength: "Please enter at least 10 number.",
            maxlength: "Please enter maximum 14 number.",
          },
          address:{
            required: "Please enter address",
          },
          password: {
           required: "Please enter your password.",
           minlength: "Password must be at least 8 characters.",
           maxlength: "Password at maximum 30 characters.",
           pwcheck:"Must contain at least one number and one uppercase and lowercase and  punctuation mark letter, and at least 8 or more characters."
         },
         re_password: {
           required: "Please enter your confirm password.",
           equalTo: "Not match your confirm password.",
         },
            //user_role: {
            //required: "Please select your user type.",
            //},
            agreeterm: {
              required: "Please check terms and conditions.",
            }
          },
         // Make sure the form is submitted to the destination defined
         // in the "action" attribute of the form when valid
         submitHandler: function(form) {
          form.submit();
         //document.getElementById('btnsubmit').style.display = 'none';
          //document.getElementById('loading2').style.display = 'block';
          return false;
        }
      });
    </script>
