
<section class="h__inner_sec wow fadeInUp" data-wow-delay=".2s">
    <div class="container">
        <div class="row">
            <div class="hed-sec-top">
                <h2>FAQs</h2>
                <div class="brdcurm"> <a href="#">Home</a> <i class="fa fa-angle-right"></i> <span>FAQs</span> </div>
            </div>
        </div></div>
    </section>

    <main id="main_inner">
        <section  class="teacherSection " data-aos="fade-up">
            <div class="container " >
                <div id="next_sec"></div>
                <div   id="section2"  class="bigHeading wow fadeInDown animated">
                  <div class="bigText">FAQs</div>
                  <h2 class="margin-bottom-xxl text-center">Frequently<strong> Asked Questions</strong></h2>
              </div>
              <div class="sec_one_box">
                <div class="row">
                    <div class="new_faqs_sec">
                        <div class="faq">
                           <?php foreach ($faqsData as $key => $faq) { ?>

                                <?php  $Sno= $key+1; ?>
                                <div class="faqitem">
                                    <div class="header">
                                        <h4><?php echo $Sno.' '.$faq->question; ?></h4>
                                        <div class="pl_min_s"> <b class="plus_icon"><img src="<?php echo base_url();?>assets/images/plus.png"></b>
                                            <b class="minus_icon"><img src="<?php echo base_url();?>assets/images/minus.png"></b></div>
                                        </div>
                                        <div class="content"><?php echo $faq->answer; ?></div>
                                    </div>
                                <?php  } ?> 
                               


                                  <!--  <div class="faqitem">
                                    <div class="header">
                                        <h4> 2 Why do we use it? </h4>
                                        <div class="pl_min_s">  <b class="plus_icon"><img src="<?php echo base_url();?>assets/images/plus.png"></b>
                                            <b class="minus_icon"><img src="<?php echo base_url();?>assets/images/minus.png"></b></div>
                                        </div>
                                        <div class="content">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</div>
                                    </div>
                                       <div class="faqitem">
                                        <div class="header">
                                            <h4>3 Where can I get some?</h4>
                                            <div class="pl_min_s">   <b class="plus_icon"><img src="<?php echo base_url();?>assets/images/plus.png"></b>
                                                <b class="minus_icon"><img src="<?php echo base_url();?>assets/images/minus.png"></b></div>
                                            </div>
                                            <div class="content">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</div>
                                        </div>
                                        <div class="faqitem">
                                            <div class="header">
                                                <h4> 4  Where does it come from?  </h4>
                                                <div class="pl_min_s">   <b class="plus_icon"><img src="<?php echo base_url();?>assets/images/plus.png"></b>
                                                    <b class="minus_icon"><img src="<?php echo base_url();?>assets/images/minus.png"></b></div>
                                                </div>
                                                <div class="content">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</div>
                                            </div> -->










                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </main> 
