<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Vital_sign extends MX_Controller {

	private $staff_permission = array();
	private $access_permission = true;
	public function __construct(){

		parent:: __construct();

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model('Common_model');
		$this->load->model('login_model');
		$this->load->helper(array('url'));
		$this->load->helper(array('form'));

		$user_id = $this->session->userdata('logged_ins')['id'];
		if(empty($user_id)) {
			redirect('admin/login');
		}

		if ($this->session->userdata('logged_ins')['user_role'] == 5) {
			$uid = $this->session->userdata('logged_ins')['id'];
			$permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
			if (!empty($permission_list)) 
			{
				foreach ($permission_list as $k => $v) 
				{
					$this->staff_permission[$v['permission_id']] = $v;
				}
			}
		}

	}

	public function permission_access($permission_id=6)
	{
		if ($this->session->userdata('logged_ins')['user_role'] != 1) 
		{
			if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
			{
				$this->access_permission = false;
			}
		}
	}

	public function vital_sign_list()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$data['vitalSign_list'] = $this->Common_model->getAllorderby('cp_vital_test_type','id','desc');
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'Vital Sign Test Type Management');
			$this->template->load('admin_dashboard_layout', 'contents', 'vital_sign_list', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}


	public function add_vital_sign()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$this->form_validation->set_rules('test_type', 'Test Type', 'required');
		$this->form_validation->set_rules('test_unit', 'Test Unit', 'required');

		$test_type = $this->input->post('test_type');
		$test_unit = $this->input->post('test_unit');

		if ($this->form_validation->run() == TRUE)
		{

			$array = array(
				'test_type' => $test_type,
				'create_date' => date('Y-m-d H:i:s')
			);
			 //echo '<pre>';print_r($array );die;
			$test_type_id = $this->Common_model->insertData('cp_vital_test_type',$array);

			$Unitarray = array(
				'unit' => $test_unit,
				'test_type_id'=>$test_type_id,
				'create_date' => date('Y-m-d H:i:s')
			);

			

			$this->Common_model->insertData('cp_vital_test_type_unit',$Unitarray);
			

			$this->session->set_flashdata('success', 'Vital sign Test type Successfully Added');
			redirect('admin/vital_sign_list');

		}
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'Vital Sign Test Type Management');
			$this->template->load('admin_dashboard_layout', 'contents', 'add_vital_sign', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}

	public function edit_vital_sign()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$this->form_validation->set_rules('test_type', 'Test type', 'required');
		$this->form_validation->set_rules('test_unit', 'Test Unit', 'required');

		$segment = $this->uri->segment('3');

		$test_type = $this->input->post('test_type');
		$test_unit = $this->input->post('test_unit');

		$data['testData'] = $this->Common_model->getsingle('cp_vital_test_type',array('id' => $segment));

		// echo $data['singleData']->id;
		// die;

		$data['unitData'] = $this->Common_model->getsingle('cp_vital_test_type_unit',array('test_type_id' => $segment));

		
		
		$data['singleData']= (object)array_merge((array)$data['testData'],(array)$data['unitData']);





		if ($this->form_validation->run() == TRUE)
		{
			$array = array(
				'test_type' => $test_type,
				'update_date' => date('Y-m-d H:i:s'),
			);

			$Unitarray = array(
				'unit' => $test_unit,
				'update_date' => date('Y-m-d H:i:s')
			);


			// echo '<pre>';print_r($array );die;
			$this->Common_model->updateData('cp_vital_test_type',$array,array('id' => $segment));

			$this->Common_model->updateData('cp_vital_test_type_unit',$Unitarray,array('test_type_id' => $segment));

			$this->session->set_flashdata('success', 'Successfully updated');
			redirect('admin/vital_sign_list');

		}
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'Vital Sign Test Type Management');
			$this->template->load('admin_dashboard_layout', 'contents' , 'add_vital_sign', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}
	
	public function delete_vital_sign()
	{
		$segment = $this->uri->segment('3');
		$this->Common_model->delete('cp_vital_test_type',array('id' => $segment));
		$this->Common_model->delete('cp_vital_test_type_unit',array('test_type_id' => $segment));
		$this->session->set_flashdata('success', 'Successfully deleted');
		redirect('admin/vital_sign_list');
	}


}