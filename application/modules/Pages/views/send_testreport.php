<!-- jQuery UI 1.10.3 -->

<aside class="right-side">

  <section class="content-header no-margin">

    <h1 class="text-center">

      <!-- <i class="fa fa-paper-plane"></i>  -->

      <i class="fa fa-file-text" aria-hidden="true"></i>

      Send Test Report

      <div class="back_reminder"><a href="javascript:window.history.back()" class="btn btn-danger">Back</a></div>

    </h1>

  </section>

  <?php //echo "<pre>"; print_r($bestData); echo "</pre>"; ?>

  <section class="content photo-list">

    <div class="photo-listMain">





      <?php if ($this->session->flashdata('success')) { ?>

        <div class="alert alert-success message">

          <button type="button" class="close" data-dismiss="alert">x</button>

          <?php echo $this->session->flashdata('success'); ?></div>

        <?php } ?> 





        <form id="send_testreport" enctype="multipart/form-data" method="post" class="frm_add">

          <input type="hidden" name="testReport_id" value="<?php echo $bestData[0]->report_id; ?>">

          <div class="row">

            <div class="col-md-6">

              <div class="form-group">

               <div class="bx_med"> <label>Report File:</label> </div>

              <div class="bx_meSec">

                <?php if($bestData[0]->image==''){ ?>

                  N.A.



                <?php }else{

                  $ext = explode('.',$bestData[0]->image); ?>





                  <?php if($ext[1]=="pdf" ){ ?>

                    <a target="_blank" href="<?php echo base_url(); ?>uploads/test_report/<?php echo $bestData[0]->image; ?>"><?php echo $$bestData[0]->image; ?></a>

                  <?php  }elseif($ext[1]=="docx" || $ext[1]=="csv"){ ?>

                    <a href="<?php echo base_url(); ?>uploads/test_report/<?php echo $bestData[0]->image; ?>"><?php echo $bestData[0]->image; ?>

                  </a>  

                <?php }

                elseif($ext[1]=="jpg" || $ext[1]=="png" || $ext[1]=="jpeg" || $ext[1]=="gif"){ ?>

                  <img height="50px" width="50px"  src="<?php echo base_url().'uploads/test_report/'.$bestData[0]->image; ?>">





                <?php }elseif($ext[1]=="txt"){ ?>

                  <a href="<?php echo base_url().'uploads/test_report/'.$bestData[0]->image; ?>" download><?php echo $bestData[0]->image; ?></a>



                <?php }} ?>



                <!-- <select name="doctor" id="doctor_id" class="form-control" >

                  <option value="">Select Doctor</option>

                  <?php foreach ($Doctors as $Doctor){ ?>

                    <option value="<?php echo $Doctor->doctor_id; ?>" <?php if($reportData[0]->doctor_id==$Doctor->doctor_id){echo 'selected="selected"';} ?>><?php echo $Doctor->doctor_name; ?></option>

                  <?php } ?>

                </select>

                <?php echo form_error('doctor'); ?> -->



              </div>

              </div>

            </div>

            <div class="col-md-6">

              <div class="form-group">

             <div class="bx_med">     <label>Select Caregiver:</label> </div>

               <div class="bx_meSec">  <select name="caregiver" id="caregiver_id" class="form-control" >

                  <option value="">Select Caregiver</option>

                  <?php foreach ($caregivers as $caregiver){ ?>

                    <option value="<?php echo $caregiver->id; ?>" ><?php echo $caregiver->name; ?></option>

                  <?php } ?>

                </select>

                <?php echo form_error('caregiver'); ?>

</div>



              </div>

            </div>

          </div>





          <div class="row">

            <div class="col-md-6">

              <div class="form-group">

             <div class="bx_med">     <label>Test Report Title:</label> </div>



                <!-- <div class="bx_med">Test Report Title:</div> -->

                <div class="bx_meSec"><?php echo $bestData[0]->test_report_title; ?></div>

                <!-- <select name="doctor" id="doctor_id" class="form-control" >

                  <option value="">Select Doctor</option>

                  <?php foreach ($Doctors as $Doctor){ ?>

                    <option value="<?php echo $Doctor->doctor_id; ?>" <?php if($reportData[0]->doctor_id==$Doctor->doctor_id){echo 'selected="selected"';} ?>><?php echo $Doctor->doctor_name; ?></option>

                  <?php } ?>

                </select> -->

                <?php echo form_error('doctor'); ?>

              </div>

            </div>

            <div class="col-md-6">

              <div class="form-group">

            <div class="bx_med">      <label>Test Type:</label> </div>

                <div class="bx_meSec"><?php echo $bestData[0]->test_type; ?></div>

                <!-- <select name="caregiver" id="caregiver_id" class="form-control" >

                  <option value="">Select Caregiver</option>

                  <?php foreach ($caregivers as $caregiver){ ?>

                    <option value="<?php echo $caregiver->id; ?>" ><?php echo $caregiver->name; ?></option>

                  <?php } ?>

                </select> -->

                <?php echo form_error('caregiver'); ?>

              </div>

            </div>

          </div>





          <div class="row">

            <div class="col-md-6">

              <div class="form-group">

             <div class="bx_med">     <label>Doctor Name:</label> </div>



                <!-- <div class="bx_med">Test Report Title:</div> -->

                <div class="bx_meSec"><?php echo $bestData[0]->doctor_name; ?></div>

                <!-- <select name="doctor" id="doctor_id" class="form-control" >

                  <option value="">Select Doctor</option>

                  <?php foreach ($Doctors as $Doctor){ ?>

                    <option value="<?php echo $Doctor->doctor_id; ?>" <?php if($reportData[0]->doctor_id==$Doctor->doctor_id){echo 'selected="selected"';} ?>><?php echo $Doctor->doctor_name; ?></option>

                  <?php } ?>

                </select> -->

                <?php echo form_error('doctor'); ?>

              </div>

            </div>

            <div class="col-md-6">

              <div class="form-group">

             <div class="bx_med">     <label>Description:</label> </div>

                <div class="bx_meSec"><?php echo $bestData[0]->description; ?></div>

                <!-- <select name="caregiver" id="caregiver_id" class="form-control" >

                  <option value="">Select Caregiver</option>

                  <?php foreach ($caregivers as $caregiver){ ?>

                    <option value="<?php echo $caregiver->id; ?>" ><?php echo $caregiver->name; ?></option>

                  <?php } ?>

                </select> -->

                <?php echo form_error('caregiver'); ?>

              </div>

            </div>

          </div>



          <div id="flupmsgs" style="color: red;"></div>

          <div class="row">

            <div class="col-md-6">

              <div class="form-group">

               <div class="bx_med">   <label>Remark:</label> </div>

            <div class="bx_meSec">    <textarea class="form-control" name="remark" rows="3"  ><?php echo $reportData[0]->description; ?></textarea> 

                <?php echo form_error('remark'); ?> </div>

              </div>

            </div>

          </div>

          <div class="box-footer">

            <input type="submit" name="submit" id="btnSub" class="btn btn-primary" value="Send" id="up_but">

          </div>

        </div>

      </form>

    </div>

  </section>

</aside>

<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

<script type="text/javascript">

  $(document).ready(function() {

    $("#send_testreport").validate({

      rules: {

//report_title: "required",

//doctor: "required",

//caregiver: "required",

remark: "required",

},

messages: {

//report_title: "Please enter test report title.",

//doctor: "Please select a doctor.",

//caregiver: "Please select a caregiver.",

remark: "Please enter remark.",

},

});

  });



</script>  



<script type="text/javascript">

  $(function () {

    $("#btnSub").click(function () {

      var doctor_id = $("#doctor_id");

      var caregiver_id = $("#caregiver_id");





            //alert(ddlFruits);

            if ((doctor_id.val() == "") && (caregiver_id.val() == "")){

                //If the "Please Select" option is selected display error.



                $('#flupmsgs').html('Please select at least one caregiver or doctor.');

                //alert("Please select an option!");

                return false;

              }

              return true;

            });

  });

</script>

<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>