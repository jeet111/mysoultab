<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Chat extends MX_Controller {
  public function __construct() {
      parent:: __construct();
      $this->load->library('session');
      $this->load->library('form_validation');
      $this->form_validation->set_error_delimiters('<div class="error">', '</div>');       
       $this->load->model('contact_model');
       $this->load->model('common_model');
      $this->load->helper(array('common_helper'));
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));      
  }
  /*Created by 95 for show all user's contact*/
  public function contact_list()
  {
    $data['menuactive'] = $this->uri->segment(1);
    $checkLogin = $this->session->userdata('logged_in');
    if(!empty($checkLogin))
    {
      $data['ContactData']  = $this->contact_model->getAllwhereorderby('cp_user_contact',array('contact_user_id'=> $this->session->userdata('logged_in')['id']),'contact_id','desc');
      $this->template->set('title', 'Contact');
      $this->template->load('user_dashboard_layout', 'contents', 'contact_list',$data);
    }else{
     redirect('login'); 
   }
 }
 /*Created by 95 for delete multiple contacts*/
  public function contact_all_del()
  {
    $bodytype = $this->input->post("bodytype");
    $explode = explode(',',$bodytype);
    foreach($explode as $del){
      $this->contact_model->delete("cp_user_contact",array("contact_id" => $del));
    }
    echo '1';
  }
/*Created by 95 for single contact delete*/
  public function delete_contact()
  {
    $delete_id = $this->uri->segment('2');
    $this->contact_model->delete("cp_user_contact",array("contact_id" => $delete_id));
     $this->session->set_flashdata('success', 'Successfully deleted');
    redirect('contact_list'); 
  }
/*Created by 95 for add contact*/
  public function add_contact()
  {
    $data['menuactive'] = $this->uri->segment(1);
    $checkLogin = $this->session->userdata('logged_in');
    if(!empty($checkLogin))
    {
      $this->form_validation->set_rules('contact_name', 'Contact Name', 'required');
      $this->form_validation->set_rules('contact_number', 'Contact Number', 'required');
     if ($this->form_validation->run() == TRUE) 
     {
      if($_POST['submit']){

            //Check whether user upload picture
            if(!empty($_FILES['picture']['name'])){
                $config['upload_path'] = 'uploads/contact/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|txt|doc';
                $config['file_name'] = $_FILES['picture']['name'];
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('picture')){
                    $uploadData = $this->upload->data();
                    $picture = $uploadData['file_name'];
                }else{

                    $picture = '';
                }
            }else{

                $picture = '';
            }
                  $data=array(
                  'contact_name'=>$this->input->post('contact_name'),
                  'contact_mobile_number'=>$this->input->post('contact_number'),
                  'contact_user_id'=>$this->session->userdata('logged_in')['id'],
                  'user_contact_image'=>$picture
                  );
       $insert = $this->contact_model->insertData('cp_user_contact',$data);
       if($insert){
        $this->session->set_flashdata('success','Contact added successfully');
        redirect('contact_list');
       }
      }
    }
      $this->template->set('title', 'Contact');
      $this->template->load('user_dashboard_layout', 'contents', 'add_contact', $data);
    }else{
     redirect('login'); 
   }
 }
/*Created by 95 for edit contact*/
  public function contact_edit($contact_id)
  {
    $data['menuactive'] = $this->uri->segment(1);
    $checkLogin = $this->session->userdata('logged_in');
    if(!empty($checkLogin))
    {
      if($_POST['submit']){
      $this->form_validation->set_rules('contact_name', 'Contact Name', 'required');
      $this->form_validation->set_rules('contact_number','Contact Number', 'required');
    if ($this->form_validation->run() == TRUE) 
     {
          //Check whether user upload picture
            if(!empty($_FILES['picture']['name'])){
                $config['upload_path'] = 'uploads/contact/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|txt|doc';
                $config['file_name'] = $_FILES['picture']['name'];
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('picture')){
                    $uploadData = $this->upload->data();
                    $picture = $uploadData['file_name'];
                }else{

                    $picture = '';
              }

               $data=array(
                  'contact_name'=>$this->input->post('contact_name'),
                  'contact_mobile_number'=>$this->input->post('contact_number'),
                  'contact_user_id'=>$this->session->userdata('logged_in')['id'],
                  'user_contact_image'=>$picture
                  );
        $this->contact_model->updateData('cp_user_contact',$data,array('contact_id' => $contact_id));
        $this->session->set_flashdata('success','Contact updated successfully');
        redirect('contact_list');

        }else{

                $picture = '';
            }

       $data=array(
                  'contact_name'=>$this->input->post('contact_name'),
                  'contact_mobile_number'=>$this->input->post('contact_number'),
                  'contact_user_id'=>$this->session->userdata('logged_in')['id'],
                  );
        $this->contact_model->updateData('cp_user_contact',$data,array('contact_id' => $contact_id));
        $this->session->set_flashdata('success','Contact updated successfully');
        redirect('contact_list');     
     }
   }
     $data['ContactData'] = $this->contact_model->getContact($contact_id);
      $this->template->set('title', 'Contact');
      $this->template->load('user_dashboard_layout', 'contents', 'edit_contact',$data);
    }else{
     redirect('login'); 
   }
 }

public function view_contact($contact_id='')
{
    
    $data['menuactive'] = $this->uri->segment(1);
    $checkLogin = $this->session->userdata('logged_in');
    if(!empty($checkLogin))
    {


      $data['UserContact']=$this->contact_model->getsingle('cp_user_contact',array('contact_id'=>$contact_id,'contact_user_id'=>$this->session->userdata('logged_in')['id']));
  
      $this->template->set('title', 'Contacts');
      $this->template->load('user_dashboard_layout_contact', 'contents', 'usercontact_detail',$data);
    
    }else{
      redirect('login');    
    }

}
public function chat(){
    
    $user=$this->contact_model->getsingle("cp_users",array("id"=>$this->session->userdata('logged_in')['id']));
    $users=$this->contact_model->getAllwhere("cp_users",array("id!="=>$this->session->userdata('logged_in')['id']));
    //
    
    if(!empty($this->uri->segment(4))){
          //echo $this->uri->segment(4);die;
          $user=$this->contact_model->getsingle("cp_users",array("id"=>$this->uri->segment(4)));
          $data['username'] = $user->username;
          $data['id'] = $user->id;
     
    }else{
          $user=$this->contact_model->getsingle("cp_users",array("id"=>$this->session->userdata('logged_in')['id']));
          $data['username'] = $user->username;
          $data['id'] = $user->id;
    }
    $data['user'] = $user;
    $data['users'] = $users;
    $this->load->view('chat1.php',$data);
  }

 
}