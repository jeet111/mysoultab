<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscriptions extends MX_Controller {

 private $staff_permission = array();
 private $access_permission = true;
 public function __construct(){   
  parent:: __construct();
  $this->load->library('session');
  $this->load->library('form_validation');
  $this->load->model('bank_model');
  $this->load->model('login_model');
  $this->load->model('subscription_model');
  $this->load->helper(array('url'));
  $this->load->helper(array('form'));
  $user_id = $this->session->userdata('logged_ins')['id'];
  if(empty($user_id)) {
   redirect('admin/login');
 }

 if ($this->session->userdata('logged_ins')['user_role'] == 5) 
 {
  $uid = $this->session->userdata('logged_ins')['id'];
  $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
  if (!empty($permission_list)) 
  {
   foreach ($permission_list as $k => $v) 
   {
    $this->staff_permission[$v['permission_id']] = $v;
  }
}
}
}

public function permission_access($permission_id=16)
{
 if ($this->session->userdata('logged_ins')['user_role'] != 1) 
 {
  if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
  {
   $this->access_permission = false;
 }
}
}

public function subscriptions()
{

  $data['menuactive'] = $this->uri->segment(2);
  if (isset($_POST['btnSend'])) 
  {
   $emails = $this->input->post('emails');
   $subject = $this->input->post('subject');
   $title = $this->input->post('title');
   $message = $this->input->post('message');

   for ($i=0; $i < count($emails); $i++) 
   { 
    sendEmail($email, $subject, $message);
  }
  $data['activities_msg'] = 'Message has been send to subscribers!';
}

$data['subscription_list'] = $this->subscription_model->getAll('subscriptions');

$data['staff_permission'] = $this->staff_permission;

$this->permission_access(16);
if ($this->access_permission) {
 $this->template->set('title', 'Banks');
 $this->template->load('admin_dashboard_layout', 'contents', 'subscriptions', $data);
} else {
 $this->template->set('title', 'Admin Panel - Staff');
 $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
}
}


public function news_letter_subscribers()
{

  $data['menuactive'] = $this->uri->segment(2);
//   if (isset($_POST['btnSend'])) 
//   {
//    $emails = $this->input->post('emails');
//    $subject = $this->input->post('subject');
//    $title = $this->input->post('title');
//    $message = $this->input->post('message');

//    for ($i=0; $i < count($emails); $i++) 
//    { 
//       sendEmail($email, $subject, $message);
//    }
//    $data['activities_msg'] = 'Message has been send to subscribers!';
// }

  $data['subscription_list'] = $this->subscription_model->getAll('subscriptions');

  $data['staff_permission'] = $this->staff_permission;

  $this->permission_access(16);
  if ($this->access_permission) {
    $this->template->set('title', 'News letter subscribers');
    $this->template->load('admin_dashboard_layout', 'contents', 'news_subs', $data);
  } else {
   $this->template->set('title', 'Admin Panel - Staff');
   $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
 }
}

}