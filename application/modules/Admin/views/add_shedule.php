<div class="container-fluid">
	<div class="dash-counter">
		<div class="Schedule_main_one">
			<div class="users-main">
				<?php
				echo "<h2>Add Schedule</h2>";
				?>






				<form class="form-horizontal" action="" method="post" enctype="multipart/form-data" name="Adddoctor" id="add_shedule">

					<!-- <input type="hidden" name="doctor_id" value="<?php //echo $id; ?>"> -->

					<?php if ($this->session->flashdata('success')) { ?>
						<div class="alert alert-success message">
							<button type="button" class="close" data-dismiss="alert">x</button>
							<?php echo $this->session->flashdata('success'); ?></div>
						<?php }else if($this->session->flashdata('faild_date')){ ?>

							<div class="alert alert-danger message">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<?php echo $this->session->flashdata('faild_date'); ?></div>
							<?php } ?>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-4" for="">Schedule Date:<span class="field_req">*</span></label>


										<div class="col-md-8">
											<div class="input-group date shedule" id="datetimepicker">
												<input type="text" name="shedule_date" id="" class="form-control">
												<div class="input-group-addon">
													<span class="glyphicon glyphicon-calendar"></span>
												</div>
											</div>

										</div>

								<!-- <div class="col-md-8 date" >
									<input type="text" placeholder="Shedule date" class="form-control" name="shedule_date" id="shedule_dates">

									<div class="input-group-addon">

                                </div>
									<?php echo form_error('shedule_date'); ?>

								</div> -->
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-4" for="">Schedule Time:<span class="field_req">*</span></label>
								<div class="col-md-8">
									<select name="shedules[]" id="sheduless" class="form-control" multiple="multiple" >
										<option value="06:00 AM">06:00 AM</option>
										<option value="07:00 AM">07:00 AM</option>
										<option value="08:00 AM">08:00 AM</option>
										<option value="09:00 AM">09:00 AM</option>
										<option value="10:00 AM">10:00 AM</option>
										<option value="11:00 AM">11:00 AM</option>
										<option value="12:00 PM">12:00 PM</option>
										<option value="01:00 PM">01:00 PM</option>
										<option value="02:00 PM">02:00 PM</option>
										<option value="03:00 PM">03:00 PM</option>
										<option value="04:00 PM">04:00 PM</option>
										<option value="05:00 PM">05:00 PM</option>
										<option value="06:00 PM">06:00 PM</option>
										<option value="07:00 PM">07:00 PM</option>
										<option value="08:00 PM">08:00 PM</option>
										<option value="09:00 PM">09:00 PM</option>
										<option value="10:00 PM">10:00 PM</option>
										<option value="11:00 PM">11:00 PM</option>

									</select>
									<?php echo form_error('shedules'); ?>

								</div>
							</div>
						</div>
					</div>


					<!-- <div class="row">
					<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-4" for="">Doctor Address:<span class="field_req">*</span></label>
								<div class="col-md-8">
									<textarea name="doctor_address" id="doctor_address" class="form-control"><?php echo $singleData->doctor_address; ?></textarea>
									<?php echo form_error('doctor_address'); ?>

								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-4" for="">Mobile No:<span class="field_req">*</span></label>
								<div class="col-md-8">
									<input type="text" placeholder="Mobile No" class="form-control" name="doctor_mobile" id="doctor_mobile" value="<?php echo $singleData->doctor_mob_no; ?>">
									<?php echo form_error('doctor_mobile'); ?>

								</div>
							</div>
						</div>
					</div>
					<div class="row">
					<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-4" for="">Doctor Fees:<span class="field_req">*</span></label>
								<div class="col-md-8">
									<input type="text" placeholder="Doctor Fees" class="form-control" name="doctor_fees" id="doctor_fees" value="<?php echo $singleData->doctor_fees; ?>">
									<?php echo form_error('doctor_fees'); ?>

								</div>
							</div>
						</div>
					</div> -->

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit" >
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<!--Added by 95 on 5-2-2019 For Doctor category for validation-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		$("#add_shedule").validate({
			rules: {
				shedule_date: "required",
				"shedules[]": "required",
			},
			messages: {
				shedule_date: "Please select date",
				shedules: "Please select at least on time",
			}
		});
	});






// $(document).ready(function() {
//          $("#add_shedule").validate({
//             rules: {
//                 shedule_date: "required",
// 				"shedules[]": "required",
// 				},
// 				message:{
// 				shedule_date:"Please select date",
// 				shedules:"Please select at least on time",
// 				},

//         });
//     });

/*Created by 95 on 6-2-2019 For Image validation*/
function showFileSize(){
	var img  = document.getElementById('profile_images').value;
	if(img==='' || img===null){
		document.getElementById("demo").innerHTML="Please choose an image.";
	}else{
		document.getElementById("demo").innerHTML=" ";
	}
}




  /*$(document).ready(function() {
    $("#shedule_dates").datepicker({minDate: 0});
});*/

// $(function () {
// 	var today = new Date();
//   $('#shedule_dates').datetimepicker({
//     ignoreReadonly: true,
// 	format: 'YYYY-MM-DD',
// 	minDate: today
//   });
// });

$(function () {
	var today = new Date();
	$('#datetimepicker').datetimepicker({
		ignoreReadonly: true,
		format: 'YYYY-MM-DD',
		minDate: today
	});
});

$(document).ready(function() {
	$('#sheduless').multiselect({
		includeSelectAllOption: true,
		allSelectedText: 'No option left ...'
	});
});

</script>