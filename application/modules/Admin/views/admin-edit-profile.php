<div class="container-fluid">
	<div class="dash-counter">
		<div class="Schedule_main_one">
			<div class="users-main">
				<h2>Edit Profile</h2>
				<?php
			      if($this->session->flashdata('success')) {
			      $message = $this->session->flashdata('success');
			    ?>
			      <div class="<?php echo $message['class'] ?>"><?php echo $message['message']; ?></div>
			    <?php } ?>


			    <?php //echo '<pre>'; print_r($admin_data); echo '</pre>'; ?>
				<form class="form-horizontal" action="admin/adminedit" method="post" enctype="multipart/form-data" name="EditProfile" id="EditProfile">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-4" for="">Email:<span class="field_req">*</span></label>
								<div class="col-md-8">
									<input type="text" placeholder="Enter email address" class="form-control" name="email" id="email" value="<?php echo $admin_data[0]['email']; ?>"> 
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-4" for="">Mobile No:<span class="field_req">*</span></label>
								<div class="col-md-8">
									<input type="text" placeholder="Enter mobile number" class="form-control" name="mobile" id="mobile" value="<?php echo $admin_data[0]['mobile']; ?>">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-4" for="">User Name:<span class="field_req">*</span></label>
								<div class="col-md-8">
									<input type="text" placeholder="Enter user name" class="form-control" name="username" id="username" value="<?php echo $admin_data[0]['username']; ?>">
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-4" for="">Profile Image:<span class="field_req">*</span></label>
								<div class="col-md-8">
									<div class="input-group">
										<input type="file" id="picture" class="form-control showvalidation" name="picture" onchange="loadFile(event)">

										<!-- <input type="file" id="user_profile_image" class="form-control showvalidation" name="user_profile_image" onchange="readUrl(event)"> -->
							            <!-- <span class="input-group-btn">
							                <span class="btn btn-default btn-file">
							                    Browse… <input type="file" id="profile_image" class="form-control" name="profile_image">
							                </span>
							            </span> -->
										<!-- <input type="hidden" class="form-control" name="user_profile_image" id="user_profile_image" value="<?php echo $admin_data[0]['profile_image']; ?>">
										<input type="hidden" class="form-control" name="exting_profile_image" id="exting_profile_image" value="<?php echo $admin_data[0]['profile_image']; ?>"> -->

									<div id="flupmsg"></div>
							        </div>


							        <?php if(empty($admin_data[0]['profile_image'])){ ?>

							        <img id='img-upload' src="<?php echo base_url().'uploads/profile_images/user.png' ?>">
							        <?php }else{ ?>


									<img id='img-upload' src="<?php echo base_url().'uploads/profile_images/'.$admin_data[0]['profile_image'] ?>">
									<?php } ?>
								</div>
							</div>
						</div>

						<!-- <div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-4" for="">Profile Image:<span class="field_req">*</span></label>
								<div class="col-md-8">
									<div class="input-group">
							            <span class="input-group-btn">
							                <span class="btn btn-default btn-file">
							                    Browse… <input type="file" id="profile_image" class="form-control" name="profile_image">
							                </span>
							            </span>
										<input type="hidden" class="form-control" name="user_profile_image" id="user_profile_image" value="<?php echo $admin_data[0]['profile_image']; ?>">
										<input type="hidden" class="form-control" name="exting_profile_image" id="exting_profile_image" value="<?php echo $admin_data[0]['profile_image']; ?>">
							        </div>
									<img id='img-upload' src="<?php echo base_url().'uploads/profile_images/'.$admin_data[0]['profile_image'] ?>">
								</div>
							</div>
						</div> -->
					</div>	
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<input type="submit" class="btn btn-default up_but" class="form-control" name="update" value="Update">
						</div>
					</div>	
				</form>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	function loadFile (event) { 
		 var pcFile = $('#picture').val().split('\\').pop();
  var pcExt     = pcFile.split('.').pop(); 
  var output = document.getElementById('img-upload');
  if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"
             || pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){
		//$(".up_but").prop('disabled', false);
        $('#flupmsg').html('');			 
      
       output.src = URL.createObjectURL(event.target.files[0]);
	   $('#img-upload').show();
      $('#flupmsg').html('');
  }else{
      $('#flupmsg').html('Please select only Image file');
      $('#img-upload').hide();
	  //$(".up_but").prop('disabled', true);
  }
};
</script>