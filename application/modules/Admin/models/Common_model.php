<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Common_model extends CI_Model{
	function __construct() {

	}

	public function insertData($table,$datainsert)
	{
		$this->db->insert($table,$datainsert);
		return $this->db->insert_id();
	}

	public function updateData($table,$data,$where)
	{
		$this->db->update($table,$data,$where);
		return $this->db->affected_rows();
	}

	public function delete($table,$where)
	{
		$this->db->delete($table,$where);
		return;
	}


	public function getsingle($table,$where)
	{
		$q = $this->db->get_where($table,$where);
		return $q->row();
	}

	public function getsingledoctorappoint($table,$where)
	{
		$q = $this->db->get_where($table,$where);
		return $q->num_rows();
	}

	public function getAllwhereorderby($table,$where,$order_id,$order_by)
	{
		$this->db->select('*');
		$this->db->order_by($order_id,$order_by);
		$q = $this->db->get_where($table,$where);
		$num_rows = $q->num_rows();
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}

	function getAllRecordsById($table,$conditions)



	{



		$query = $this->db->get_where($table,$conditions);



		return $query->result_array();



	}

	function getAllUsers()
	{
		$this->db->select('*');
		$this->db->from('cp_users');
		$this->db->where('user_role !=',1);
		$this->db->where('user_role !=',5);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getAll($table)
	{
		$this->db->select('*');
		$q = $this->db->get($table);
		$num_rows = $q->num_rows();
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}

	public function getAllorderby($table,$order_id,$order_by)
	{
		$this->db->select('*');
		$this->db->order_by($order_id,$order_by);
		$q = $this->db->get($table);
		$num_rows = $q->num_rows();
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}



	public function getAllwhere($table,$where)
	{
		$this->db->select('*');
		$q = $this->db->get_where($table,$where);

		// echo $this->db->last_query();
		// die;

		$num_rows = $q->num_rows();
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}

	function jointwotablenn($table, $field_first, $table1, $field_second,$where='',$field,$order_id,$order_by) {

		$this->db->select($field);
		$this->db->order_by( $order_id, $order_by);
		$this->db->from("$table");
		$this->db->join("$table1", "$table1.$field_second = $table.$field_first");
		if($where !=''){
			$this->db->where($where);
		}
		$q = $this->db->get();


       	// echo $this->db->last_query();
       	// die;


		if($q->num_rows() > 0) {
			foreach($q->result() as $rows) {
				$data[] = $rows;
			}
			$q->free_result();
			return $data;
		}
	}

	/*Created by 95 for add doctor shedule date*/
	public function insertDoctorShedule($data)
	{
		$this->db->insert('dr_available_date', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	/*Created by 95 for add doctor shedule time*/
	public function insertDoctorTime($data)
	{
		$this->db->insert('dr_available_time', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

    // /*Created by 95 for show all the shedules*/
    // public function getAllShedules()
    // {
    // 	 $this->db->select('*');
    //   	 $this->db->from('dr_available_date as t1');
	   //   $this->db->join('dr_available_time as t2', 't1.avdate_id = t2.avtime_date_id', 'LEFT');
	   //   $this->db->order_by('avdate_id','desc');
	   //   $query = $this->db->get();
    //      return $query->result_array();
    // }


	/*Created by 95 for updated shedule date*/
	public function getAllShedules($id='')
	{

		$this->db->select('*');
		$this->db->from('dr_available_date as t1');
		$this->db->where('t1.avdate_id',$id);
		$this->db->join('dr_available_time as t2', 't1.avdate_id = t2.avtime_date_id', 'LEFT');
		$query = $this->db->get();
		return $query->result_array();

	}


	/*Created by 95 for show all the shedule dates*/
	public function getAllSheduleDates()
	{
		$this->db->select('*');
		$this->db->from('dr_available_date as t1');
		$this->db->join('cp_doctor as t2', 't1.avdate_dr_id = t2.doctor_id', 'LEFT');
		$this->db->order_by('avdate_id','desc');
		$query = $this->db->get();
		return $query->result_array();
	}


	/*Created by 95 for show all the shedule time slots with single date*/
	public function getAllSheduleTimes($id='')
	{
		$this->db->select('*');
		$this->db->from('dr_available_time as t1');
		$this->db->where('t1.avtime_date_id',$id);
		$this->db->join('dr_available_date as t2', 't2.avdate_id = t1.avtime_date_id', 'LEFT');
		$this->db->order_by('t1.avtime_id','desc');
		$query = $this->db->get();
		return $query->result_array();
	}


	/*Created by 95 for delete all the shedules time slots by one click*/
	public function DeleteAllTimeShedulesByDate($id='')
	{
		$this->db->where('avdate_id', $id);
		$this->db->delete('dr_available_date');
		$this->db->affected_rows();
		if($this->db->affected_rows()){

			$this->db->where('avtime_date_id', $id);
			$this->db->delete('dr_available_time');
			$no	= $this->db->affected_rows();
			return $no;
		}
	}

	/*Created by 95 for delete one shedule time slote */
	public function DeleteOneSheduleTimeSlot($id='')
	{
		$this->db->where('avtime_id', $id);
		$this->db->delete('dr_available_time');
	}

	/*Created by 95 for updated or edit shedule time slots*/
	public function getSheduleTimes($id='')
	{
		$this->db->select('*');
		$this->db->from('dr_available_time');
		$this->db->where('avtime_id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

	/*Created by 95 for show all the medicine shedule*/
	public function getAllMedicineShedule()
	{
		$this->db->select('*');
		$this->db->from('medicine_schedule as t1');
		$this->db->join('cp_doctor as t2', 't1.doctor_id = t2.doctor_id', 'LEFT');

		$this->db->join('cp_users as t3', 't3.id = t1.user_id', 'LEFT');
		$this->db->order_by('medicine_id','desc');
		$this->db->where('t3.user_role!=','1');
		$query = $this->db->get();
		return $query->result_array();
	}


	/*Created by 95 for show single medicine shedule*/
	public function getMedicineShedule($id='')
	{
		$this->db->select('*');
		$this->db->from('medicine_schedule as t1');
		$this->db->where('t1.medicine_id',$id);
		$this->db->join('cp_users as t2', 't2.id = t1.user_id', 'LEFT');

		$this->db->join('cp_doctor as t3', 't3.doctor_id = t1.doctor_id', 'LEFT');
	     //$this->db->order_by('t1.avtime_id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	/*Created by 95 for show appointmets*/
	public function getAllappointments()
	{
		$this->db->select('*');
		$this->db->from('doctor_appointments as t1');
		$this->db->join('cp_doctor as t2', 't1.doctor_id = t2.doctor_id', 'LEFT');
		$this->db->join('cp_users as t3', 't3.id = t1.user_id', 'LEFT');
		$this->db->join('dr_available_date as t4', 't4.avdate_id = t1.dr_appointment_date_id', 'LEFT');
		$this->db->join('dr_available_time as t5', 't5.avtime_id = t1.dr_appointment_time_id', 'LEFT');
		$this->db->where('t3.user_role!=','1');
		$query = $this->db->get();
		return $query->result_array();
	}

	/*Created by 95 for show single appointment*/
	public function getAppointment($id='')
	{
		$this->db->select('*');
		$this->db->from('doctor_appointments as t1');
		$this->db->where('t1.dr_appointment_id',$id);
		$this->db->join('cp_doctor as t2', 't1.doctor_id = t2.doctor_id', 'LEFT');
		$this->db->join('cp_users as t3', 't3.id = t1.user_id', 'LEFT');
		$this->db->join('dr_available_date as t4', 't4.avdate_id = t1.dr_appointment_date_id', 'LEFT');
		$this->db->join('dr_available_time as t5', 't5.avtime_id = t1.dr_appointment_time_id', 'LEFT');
		$this->db->where('t3.user_role!=','1');
		$query = $this->db->get();
		return $query->result();
	}



	/*Created by 95 for check shedule date is exist or not*/
	public function checkDate($shedule_date,$doctor_id)
	{
		$this->db->select('*');
		$this->db->from('dr_available_date');
		$this->db->where('avdate_dr_id',$doctor_id);
		$this->db->where('avdate_date',$shedule_date);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			return $query->result_array();
		}else{
			return 0;
		}



	}




	public function checkTimeSlotes($date_id)
	{

		$this->db->select('*');
		$this->db->from('dr_available_time');
		$this->db->where('avtime_date_id',$date_id);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			return $query->result_array();
		}else{
			return 0;
		}
	}


	/*Created by 95 for update doctor shedule date and times*/
	public function updateOnlyDate($date_array,$where)
	{
		$this->db->update('dr_available_date',$date_array,$where);
		return $this->db->affected_rows();
	}

	/*Created by 95 for updated shedule date*/
	public function getAllShedulesTime($id='')
	{

		$this->db->select('*');
		$this->db->from('dr_available_time');
		$this->db->where('avtime_date_id',$id);
	     //$this->db->join('dr_available_time as t2', 't1.avdate_id = t2.avtime_date_id', 'LEFT');
		$query = $this->db->get();
		return $query->result_array();

	}

	public function getAllappointmentsnew($per_page,$page)
	{
		$startt = max(0, ( $page -1 ) * $per_page);
		$sql = 'SELECT * FROM `doctor_appointments` as `t1` LEFT JOIN `cp_doctor` as `t2` ON `t1`.`doctor_id` = `t2`.`doctor_id` LEFT JOIN `cp_users` as `t3` ON `t3`.`id` = `t1`.`user_id` LEFT JOIN `dr_available_date` as `t4` ON `t4`.`avdate_id` = `t1`.`dr_appointment_date_id` LEFT JOIN `dr_available_time` as `t5` ON `t5`.`avtime_id` = `t1`.`dr_appointment_time_id` WHERE `t3`.`user_role` != "1" ORDER BY `t1`.`doctor_id` DESC LIMIT '.$startt.','.$per_page;

		$q = $this->db->query($sql);
		$data =  array() ;
		 //echo $this->db->last_query();die;

		foreach($q->result_array() as $row)
		{
			$data[]  = $row;
		}

		$ret['rows'] = $data;

		$sql2 = 'SELECT * FROM `doctor_appointments` as `t1` LEFT JOIN `cp_doctor` as `t2` ON `t1`.`doctor_id` = `t2`.`doctor_id` LEFT JOIN `cp_users` as `t3` ON `t3`.`id` = `t1`.`user_id` LEFT JOIN `dr_available_date` as `t4` ON `t4`.`avdate_id` = `t1`.`dr_appointment_date_id` LEFT JOIN `dr_available_time` as `t5` ON `t5`.`avtime_id` = `t1`.`dr_appointment_time_id` WHERE `t3`.`user_role` != "1" ORDER BY `t1`.`doctor_id` DESC';
		$q = $this->db->query($sql2);
		$data = array();
		foreach($q->result_array() as $row)
		{
			$data[]  = $row;
		}

		$ret['num_rows'] = count($data);

		return $ret;
	}

	public function getDetailField($where){
		$whereCondititon = $where['search'];
		$doctor_name = $whereCondititon["doctor_name"];
		$username = $whereCondititon["username"];

		$sql = 'SELECT `doctor_appointments`.* ,`cp_users`.`username`,`cp_users`.`id`,`cp_doctor`.`doctor_name`,`dr_available_date`.`avdate_dr_id`, `dr_available_date`.`avdate_date`,`dr_available_time`.`avtime_text`,`dr_available_time`.`avtime_day_slot` FROM `doctor_appointments` JOIN `cp_users` ON `cp_users`.`id` = `doctor_appointments`.`user_id` JOIN `dr_available_date` ON `doctor_appointments`.`dr_appointment_date_id` = `dr_available_date`.`avdate_id` JOIN `cp_doctor` ON `doctor_appointments`.`doctor_id` = `cp_doctor`.`doctor_id` JOIN `dr_available_time` ON `doctor_appointments`.`dr_appointment_time_id` = `dr_available_time`.`avtime_id` WHERE `doctor_appointments`.`status` = 0';

		if(!empty($whereCondititon["username"])){
			$sql .= " AND `cp_users`.`username` LIKE '$username%'";
		}


		if(!empty($whereCondititon["doctor_name"])){
			$sql .= " AND `cp_doctor`.`doctor_name` LIKE '$doctor_name%'";


		}


		if(!empty($whereCondititon["datepicker"]) && !empty($whereCondititon["datepicker2"])){
			$sql .= ' AND `dr_available_date`.`avdate_date` BETWEEN "'.$whereCondititon["datepicker"].'" AND "'.$whereCondititon["datepicker2"].'"';
		}

		if(!empty($whereCondititon["datepicker"]) && empty($whereCondititon["datepicker2"])){
			$sql .= ' AND `dr_available_date`.`avdate_date`="'.$whereCondititon["datepicker"].'"';
		}

		if(empty($whereCondititon["datepicker"]) && !empty($whereCondititon["datepicker2"])){
			$sql .= ' AND `dr_available_date`.`avdate_date`="'.$whereCondititon["datepicker2"].'"';
		}


		if(!empty($whereCondititon["selector"])){
			$sql .= ' AND `dr_available_time`.`avtime_day_slot`="'.$whereCondititon["selector"].'"';
		}

		$q=$this->db->query($sql);

//print_r($this->db->last_query());die;
		$num_rows = $q->num_rows();

		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}

	}

	function jointwotable($table, $field_first, $table1, $field_second,$where='',$field) {

		$this->db->select($field);
		$this->db->from("$table");
		$this->db->join("$table1", "$table1.$field_second = $table.$field_first","left");
		if($where !=''){
			$this->db->where($where);
		}
		$q = $this->db->get();
		if($q->num_rows() > 0) {
			return $q->row();

		}else{
			return false;
		}
	}


	public function getnewscheduledata($per_page,$page)
	{
		$startt = max(0, ( $page -1 ) * $per_page);
		$sql = 'SELECT * FROM `dr_available_date` as `t1` LEFT JOIN `cp_doctor` as `t2` ON `t1`.`avdate_dr_id` = `t2`.`doctor_id`  WHERE `t1`.`status` = 0 ORDER BY `t2`.`doctor_id` DESC LIMIT '.$startt.','.$per_page;

		$q = $this->db->query($sql);
		$data =  array() ;
		 //echo $this->db->last_query();die;

		foreach($q->result_array() as $row)
		{
			$data[]  = $row;
		}

		$ret['rows'] = $data;

		$sql2 = 'SELECT * FROM `dr_available_date` as `t1` LEFT JOIN `cp_doctor` as `t2` ON `t1`.`avdate_dr_id` = `t2`.`doctor_id`  WHERE `t1`.`status` = 0 ORDER BY `t2`.`doctor_id` DESC';

		$q = $this->db->query($sql2);
		$data = array();
		foreach($q->result_array() as $row)
		{
			$data[]  = $row;
		}

		$ret['num_rows'] = count($data);

		return $ret;

	}

	public function getDetailFieldschedule($where)
	{
		//ORDER BY `avdate_id` DESC

		$whereCondititon = $where['search'];
		$doctor_name = $whereCondititon["doctor_name"];

		$sql = 'SELECT * FROM `dr_available_date` as `t1` LEFT JOIN `cp_doctor` as `t2` ON `t1`.`avdate_dr_id` = `t2`.`doctor_id`  WHERE `t1`.`status` = 0';

		if(!empty($whereCondititon["datepicker"]) && !empty($whereCondititon["datepicker2"])){
			$sql .= ' AND `t1`.`avdate_date` BETWEEN "'.$whereCondititon["datepicker"].'" AND "'.$whereCondititon["datepicker2"].'"';
		}

		if(!empty($whereCondititon["datepicker"]) && empty($whereCondititon["datepicker2"])){
			$sql .= ' AND `t1`.`avdate_date`="'.$whereCondititon["datepicker"].'"';
		}

		if(!empty($whereCondititon["doctor_name"])){
			$sql .= " AND `t2`.`doctor_name` LIKE '$doctor_name%'";
		}
		$q=$this->db->query($sql);

//print_r($this->db->last_query());die;
		$num_rows = $q->num_rows();

		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}

	}

}