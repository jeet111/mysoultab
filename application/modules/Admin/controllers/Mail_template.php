<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Mail_template extends MX_Controller {

	private $staff_permission = array();
	private $access_permission = true;
	public function __construct(){

		parent:: __construct();

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model('Common_model');
		$this->load->model('login_model');
		$this->load->model('Feature_model');

		$this->load->helper(array('url'));
		$this->load->helper(array('form'));

		$user_id = $this->session->userdata('logged_ins')['id'];
		if(empty($user_id)) {
			redirect('admin/login');
		}

		if ($this->session->userdata('logged_ins')['user_role'] == 5) {
			$uid = $this->session->userdata('logged_ins')['id'];
			$permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
			if (!empty($permission_list)) 
			{
				foreach ($permission_list as $k => $v) 
				{
					$this->staff_permission[$v['permission_id']] = $v;
				}
			}
		}

	}

	public function permission_access($permission_id=6)
	{
		if ($this->session->userdata('logged_ins')['user_role'] != 1) 
		{
			if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
			{
				$this->access_permission = false;
			}
		}
	}

	public function mail_template_list()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$data['temlate_list'] = $this->Common_model->getAllorderby('mail_template','template_id ','desc');
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'Mail templates');
			$this->template->load('admin_dashboard_layout', 'contents', 'mail_template_list', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}


	public function edit_template(){
		$data['menuactive'] = $this->uri->segment(2);
    //$this->form_validation->set_rules('template_id','Template Id','required');
    //$this->form_validation->set_rules('subject','Subject','required');
		$template_id = $this->uri->segment('3');
		$data['singleData'] = $this->Common_model->getsingle('mail_template',array('template_id' => $template_id));
    //$this->form_validation->set_rules('description','Description','required');
    //$this->form_validation->set_rules('template_id','Template Id','required');
		$this->form_validation->set_rules('template_name','Template Name','required');
		$this->form_validation->set_rules('subject','Subject','required');
		$this->form_validation->set_rules('editor1','Description','required');

		if($this->form_validation->run()==TRUE)
		{
			$template_name = $_POST['template_name'];
			$subject = $_POST['subject'];
			$description = $_POST['editor1'];
			$arrayData = array(
				'template_name'=>$template_name,
				'subject'=>$subject,
				'description'=>$description,
				'updated_date'=>date('Y-m-d H:i:s')
			);

      // print_r($arrayData);
      // die;

			$this->Common_model->updateData('mail_template',$arrayData,array('template_id' =>$template_id));     

      //$this->Common_model->updateData('about_page',$array,array('template_id' => 1));
			$this->session->set_flashdata('success', 'Successfully updated.');
			redirect('admin/mail_templates');
		}
		$data['staff_permission'] = $this->staff_permission;

		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'Mail Template');
			$this->template->load('admin_dashboard_layout', 'contents', 'template_editor', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}


	public function delete_feature_category()
	{
		$segment = $this->uri->segment('3');
		$this->Feature_model->delete('cp_tabfeature_category',array('id' => $segment));
		//$this->Common_model->delete('cp_vital_test_type_unit',array('test_type_id' => $segment));
		$this->session->set_flashdata('success', 'Successfully deleted');
		redirect('admin/feature_category_list');
	}


	public function view_template()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$template_id = $this->uri->segment('3');
		$data['singleData'] = $this->Common_model->getsingle('mail_template',array('template_id' => $template_id));


		$data['staff_permission'] = $this->staff_permission;

		$this->permission_access(7);
		if ($this->access_permission) {
			$this->template->set('title', 'Mail Template');
			$this->template->load('admin_dashboard_layout', 'contents', 'view_template_detail', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}

	}

	public function getFeture()
	{
		$fetr_id = $this->input->post('feature_id');
		$fetrData= $this->Common_model->getsingle('cp_tabfeatures',array('id' => $fetr_id));


		echo json_encode($fetrData);
	}	

}