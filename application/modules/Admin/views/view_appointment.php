<div class="container-fluid">
<div class="dash-counter users-main my-sts-table user-list">
<div class="">
<div class="col-md-12">
<div class="">
<div class="card-body">
<div class="my_info_one">
<div class="Schedule_main_one">
<div class=" users-main">
<h2>View Appointment</h2>

<div class="btn_topBack">
	<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">
		Back
	</a>
</div>

<div class="row">

<div class="col-md-12">
	<div class="general-info">
		<div class="card-header">
			<?php //echo "<pre>"; print_r($appointment); echo "<pre>"; ?>
			<!-- <h5 class="card-header-text">View Doctor</h5> -->
			
		</div>
		<div class="rrow">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-5 col-sm-5">
						<div class="f_name">User Name:</div>
					</div>
					<div class="col-md-7 col-sm-7">
						<div class="f_nameOne"><?php echo $appointment[0]->username;?></div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-5 col-sm-5">
						<div class="f_name">Doctor Name:</div>
					</div>
					<div class="col-md-7 col-sm-7">
						<div class="f_nameOne"><?php echo $appointment[0]->doctor_name;?></div>
					</div>
				</div>
			</div>
		</div>
		<div class="rrow">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-5 col-sm-5">
						<div class="f_name">Appointment Date:</div>
					</div>
					<div class="col-md-7 col-sm-7">
						<div class="f_nameOne"><?php echo $appointment[0]->avdate_date;?></div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-5 col-sm-5">
						<div class="f_name">Appointment Time:</div>
					</div>
					<div class="col-md-7 col-sm-7">
						<div class="f_nameOne"><?php echo $appointment[0]->avtime_text;?></div>
					</div>
				</div>
			</div>
		</div>
		<div class="rrow">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-5 col-sm-5">
						<div class="f_name">Appointment Time Slot:</div>
					</div>
					<div class="col-md-7 col-sm-7">
						<div class="f_nameOne"><?php echo $appointment[0]->avtime_day_slot;
							?></div>
						</div>
					</div>
				</div>
				
				<!-- <div class="col-md-6">
					<div class="row">
						<div class="col-md-5 col-sm-5">
							<div class="f_name">Created Date:</div>
						</div>
						<div class="col-md-7 col-sm-7">
							<div class="f_nameOne"><?php //echo date('d-m-Y',strtotime($view_doctor->doctor_created));?></div>
						</div>
					</div>
				</div> -->
				
			</div>
		</div>
		
		<!-- <a class="cancel-btn btn" href="javascript:window.history.back()">
			Back
		</a> -->
	</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>