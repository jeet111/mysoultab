<?php

if (isset($staff_permission[9]['add_permission']) == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn">

			<a href="<?php echo base_url(); ?>admin/add_app">

				Add App

			</a>

		</div>

	</div>

	<?php

} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn">

			<a href="<?php echo base_url(); ?>admin/add_app">

				Add App

			</a>

		</div>

	</div>

	<?php

}

?>





<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="row">

			<div class="col-md-12">

				<div class="">

					<?php //echo "<pre>"; print_r($pharmaData); echo "</pre>";?>

					<div class="card-body">

						<div class="status-cng"></div>

						<?php if ($this->session->flashdata('success')) { ?>

							<div class="alert alert-success message">

								<button type="button" class="close" data-dismiss="alert">x</button>

								<?php echo $this->session->flashdata('success'); ?></div>

							<?php } ?>



							<!-- <div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_pharmacompany">Add Pharma Company</a></div> -->

							<table class="table table-hover tab_comn" id="sampleTable">

								<thead>

									<tr>

										<th>S no.</th>

										<th>Name</th>

										<th>Url</th>							

										<th>App Icon</th>							

										<th>Action</th>

									</tr>

								</thead>

								<tbody>

									<?php 

									$i = 1;

									foreach ($appData as $pharma) {

										?>

										<tr>

											<td><?php echo $i++; ?></td>

											<td><?php echo $pharma->name; ?></td>

											<td><?php echo $pharma->url; ?></td>

											<td> <img src="<?php echo $pharma->icon; ?>"></td>

											<td>

												<div class="link-del-view">



													<?php

													if (isset($staff_permission[9]['edit_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/edit_app/<?php echo $pharma->id; ?>">

																<i class="fa fa-edit" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">Edit</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/edit_app/<?php echo $pharma->id; ?>">

																<i class="fa fa-edit" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">Edit</span>

														</div>

														<?php

													}

													?>



													<?php

													if (isset($staff_permission[9]['delete_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/delete_app/<?php echo $pharma->id; ?>" onclick="return confirm('Delete this pharma company ?')">

																<i class="fa fa-trash-o" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">Delete</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/delete_app/<?php echo $pharma->id; ?>" onclick="return confirm('Delete this app ?')">

																<i class="fa fa-trash-o" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">Delete</span>

														</div>

														<?php

													}

													?>







												</div>



											</div>

										</td>

									</tr>

								<?php } ?>

							</tbody>

						</table>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>