  <?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  class MeditationvideoBanner extends MX_Controller {
    private $staff_permission = array();
    private $access_permission = true;
    public function __construct(){   
      parent:: __construct();
      $this->load->library('session');
      $this->load->library('form_validation');
      $this->load->model('MeditationVideoBanner_model');
      $this->load->model('Common_model');
      $this->load->model('login_model');
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));
      $user_id = $this->session->userdata('logged_ins')['id'];
      if(empty($user_id)){
        redirect('admin/login');
      }

      if ($this->session->userdata('logged_ins')['user_role'] == 5) {
        $uid = $this->session->userdata('logged_ins')['id'];
        $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
        if (!empty($permission_list)) 
        {
           foreach ($permission_list as $k => $v) 
           {
              $this->staff_permission[$v['permission_id']] = $v;
           }
        }
      }
    }

    public function permission_access($permission_id=14)
    {
      if ($this->session->userdata('logged_ins')['user_role'] != 1) 
      {
         if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
         {
            $this->access_permission = false;
         }
      }
    }
    
    /*show all banners*/
    public function meditation_video_banner_list()
    {
     $data['menuactive'] = $this->uri->segment(2);
     $data['MovieBannerData'] = $this->MeditationVideoBanner_model->getAllmeditationbanner();

     $data['staff_permission'] = $this->staff_permission;
     
     $this->permission_access(14);
     if ($this->access_permission) {
      $this->template->set('title', 'Meditation Viedo Banner');
      $this->template->load('admin_dashboard_layout', 'contents' , 'meditation_video_banner_list', $data);
     } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }

   /*Delete banner*/
   public function delete_meditation_video_banner($moviebanner_id='')
   {

    $MovieBannerdata=$this->MeditationVideoBanner_model->getsingle('cp_meditation_videos_banner',array('movie_banner_id'=>$moviebanner_id));
    $imageFilePath=getcwd().'/uploads/meditation_videos/banners/';
    $imageFile=$MovieBannerdata->movie_banner_image;
    unlink($imageFilePath.$imageFile);
    $vedioFilePath= $imageFilePath=getcwd().'/uploads/meditation_videos/banners/files/';
    $vedioFile=$MovieBannerdata->movie_banner_file;
    unlink($vedioFilePath.$vedioFile);
    $this->MeditationVideoBanner_model->delete('cp_meditation_videos_banner',array('movie_banner_id' =>$moviebanner_id));
    $this->session->set_flashdata('success', 'Deleted successfully.');
    redirect('admin/meditation_video_banner_list');
  }

  /*Add banner*/
  public function add_meditation_video_banner()
  {
   $data['menuactive'] = $this->uri->segment(2);
   $this->form_validation->set_rules('banner_title','Banner Title','required');
   if ($this->form_validation->run() == TRUE) 
   {  
    if($_POST['submit']){

      /*for audio file*/
      $random = $this->generateRandomString(10);
      $ext = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
      $file_name = $random.".".$ext;
      $dirpath = './././uploads/meditation_videos/banners/files/'.$file_name;
      if(move_uploaded_file($_FILES['picture']['tmp_name'], $dirpath)){
       $data['new_pic'] = $file_name;
     }
     if(!empty($data['new_pic'])){
      $picture =$data['new_pic'];
    }else{
      $picture ='';
    }


    /*for image file*/
    $random1 = $this->generateRandomString(10);
    $ext = pathinfo($_FILES['pictureFile']['name'], PATHINFO_EXTENSION);
    $file_name1 = $random1.".".$ext;
    $dirpath = './././uploads/meditation_videos/banners/'.$file_name1;
    if(move_uploaded_file($_FILES['pictureFile']['tmp_name'], $dirpath)){
     $data['banner'] = $file_name1;
   }
   if(!empty($data['banner'])){
    $pictureFile =$data['banner'];
  }else{
    $pictureFile ='';
  }


  $data = array(
    'movie_banner_text' => $this->input->post('banner_title'),
    'movie_banner_image' => $pictureFile,
    'movie_banner_file'=>$picture,
    'movie_banner_created' =>date('Y-m-d H:i:s'),
    'movie_banner_status'=>1
  );
  $this->MeditationVideoBanner_model->insertData('cp_meditation_videos_banner',$data);
  $this->session->set_flashdata('success', 'Banner added successfully.');
  redirect('admin/meditation_video_banner_list');
}
}

$data['staff_permission'] = $this->staff_permission;

    $this->permission_access(14);
    if ($this->access_permission) {
      $this->template->set('title', 'Meditation Viedo Banner');
      $this->template->load('admin_dashboard_layout', 'contents' , 'add_meditation_video_banner', $data);
    } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
    }
}

public function generateRandomString($length = 12) {
 $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
 $charactersLength = strlen($characters);
 $randomString = '';
 for ($i = 0; $i < $length; $i++) {
   $randomString .= $characters[rand(0, $charactersLength - 1)];
 }
 return $randomString;
}

/*Edit banner*/
public function edit_meditation_video_banner($banner_id='')
{
 $data['menuactive'] = $this->uri->segment(2);
 $this->form_validation->set_rules('banner_title','Banner Title','required');
 if ($this->form_validation->run() == TRUE) 
 {  
   if($_POST['submit']){

     /*for audio file*/
     $random = $this->generateRandomString(10);
     $ext = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
     $file_name = $random.".".$ext;
     $dirpath = './././uploads/meditation_videos/banners/files/'.$file_name;
     if(move_uploaded_file($_FILES['picture']['tmp_name'], $dirpath)){
       $data['new_pic'] = $file_name;
     }
     if(!empty($data['new_pic'])){
      $picture =$data['new_pic'];
    }else{
      $picture ='';
    }


    /*for image file*/
    $random1 = $this->generateRandomString(10);
    $ext = pathinfo($_FILES['pictureFile']['name'], PATHINFO_EXTENSION);
    $file_name1 = $random1.".".$ext;
    $dirpath = './././uploads/meditation_videos/banners/'.$file_name1;
    if(move_uploaded_file($_FILES['pictureFile']['tmp_name'], $dirpath)){
     $data['banner'] = $file_name1;
   }
   if(!empty($data['banner'])){
    $pictureFile =$data['banner'];
  }else{
    $pictureFile ='';
  }



  if(!empty($pictureFile)){
    $data = array(
      'movie_banner_text' => $this->input->post('banner_title'),
      'movie_banner_image' => $pictureFile,
      'movie_banner_created' =>date('Y-m-d H:i:s'),
    );
    $this->MeditationVideoBanner_model->updateData('cp_meditation_videos_banner',$data,array('movie_banner_id'=>$banner_id));
    $this->session->set_flashdata('success', 'Banner updated successfully.');
    redirect('admin/meditation_video_banner_list');

  }else if(!empty($picture)){

    $data = array(
      'movie_banner_text' => $this->input->post('banner_title'),
        //'music_banner_image' => $pictureFile,
      'movie_banner_file' => $picture,
      'movie_banner_created' =>date('Y-m-d H:i:s'),
    );
    $this->MeditationVideoBanner_model->updateData('cp_meditation_videos_banner',$data,array('movie_banner_id'=>$banner_id));
    $this->session->set_flashdata('success', 'Banner updated successfully.');
    redirect('admin/meditation_video_banner_list');
    


  }else if(empty($pictureFile)|| $pictureFile=='' && empty($picture)|| $picture==''){



    $data = array(
      'movie_banner_text' => $this->input->post('banner_title'),
      'movie_banner_created' =>date('Y-m-d H:i:s'),
    );
    $this->MeditationVideoBanner_model->updateData('cp_meditation_videos_banner',$data,array('movie_banner_id'=>$banner_id));
    $this->session->set_flashdata('success', 'Banner updated successfully.');
    redirect('admin/meditation_video_banner_list');
  }
}
}
$data['editBanner']=$this->MeditationVideoBanner_model->getsingle('cp_meditation_videos_banner',array('movie_banner_id'=>$banner_id));

$data['staff_permission'] = $this->staff_permission;

    $this->permission_access(14);
    if ($this->access_permission) {
      $this->template->set('title', 'Meditation Viedo Banner');
      $this->template->load('admin_dashboard_layout','contents','edit_meditation_video_banner',$data);
    } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
    }
}

/*view single banner detail*/
public function view_meditation_video_banner($banner_id='')
{
 $data['menuactive'] = $this->uri->segment(2);
 $data['viewMovieBanner']=$this->MeditationVideoBanner_model->getsingle('cp_meditation_videos_banner',array('movie_banner_id'=>$banner_id));

 $data['staff_permission'] = $this->staff_permission;
 
    $this->permission_access(14);
    if ($this->access_permission) {
      $this->template->set('title', 'Meditation Viedo Banner');
      $this->template->load('admin_dashboard_layout', 'contents', 'view_meditation_video_banner', $data);
    } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
    }
}

/*Update banner status*/
public function updateBanner() {
  $banner_id = $this->input->post('banner_id');
  $con['conditions'] = array(
    'movie_banner_id'=> $banner_id
  );
  $data['users_data'] = $this->login_model->getRows('cp_meditation_videos_banner',$con);
  if($data['users_data'][0]['movie_banner_status']==1){
    $return = $this->login_model->updateRecords("cp_meditation_videos_banner", array('movie_banner_status' => 0), array('movie_banner_id'=> $banner_id));
    echo '2';
  }else{
    $return = $this->login_model->updateRecords("cp_meditation_videos_banner", array('movie_banner_status' => 1), array('movie_banner_id'=> $banner_id));
    echo '1';	
  }

}

public function ajax_banner_add()
{
  if ($_FILES["picture"]["error"] > 0 && $_FILES["pictureFile"]["error"] > 0)
  {
   $return = array('code' => 2,'message' => 'File loading error!');
 }else{ 

   $config1['upload_path'] = 'uploads/meditation_videos/banners/files/';
   $config1['allowed_types'] = 'mp4|flv|wmv|webm|vob|ogg|mpeg|mpg|3gp';

           //$data['max_size'] = '5000';
   $config1['encrypt_name'] = true;

   $this->load->library('upload', $config1);

   if ($this->upload->do_upload('picture')) {  
    $attachment_data = array('upload_data' => $this->upload->data());
    $uploadfile = $attachment_data['upload_data']['file_name'];
			// $return = array('code' => 1,'message' => 'Successfully added');	
  }else{ 
   $return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
 }


 $config2['upload_path'] = 'uploads/meditation_videos/banners/';
 $config2['allowed_types'] = 'jpg|png|gif|jpeg';
           //$data['max_size'] = '5000';
 $config2['encrypt_name'] = true;

 $this->upload->initialize($config2);


 if ($this->upload->do_upload('pictureFile')) {
  $attachment_data_image = array('upload_data' => $this->upload->data());
  $picturefile1 = $attachment_data_image['upload_data']['file_name'];

      //  $return = array('code' => 1,'message' => 'Successfully added');		   
}else{ 
 $return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));

}
		// echo $picturefile.'hii';die;

if($return['code'] != 2){  
 $array = array(
  'movie_banner_text' => $this->input->post('banner_title'),

  'movie_banner_file'=>$uploadfile,
  'movie_banner_image'=>$picturefile1,
  'movie_banner_created' =>date('Y-m-d H:i:s'),
  'movie_banner_status'=>1
);
 $this->Common_model->insertData("cp_meditation_videos_banner",$array);

 $return = array('code' => 1,'message' => 'Successfully added.');
}
}
echo json_encode($return); 
}

public function image_upload()
{
  $config1['upload_path'] = 'uploads/meditation_videos/banners/';
  $config1['allowed_types'] = 'mp4|flv|wmv|webm|vob|ogg|mpeg|mpg|3gp';

           //$data['max_size'] = '5000';
  $config1['encrypt_name'] = true;

  $this->load->library('upload', $config1);


  if ($this->upload->do_upload('picture')) {
    $attachment_data = array('upload_data' => $this->upload->data());
    $uploadfile = $attachment_data['upload_data']['file_name'];
    $return = array("code" => 1);
  }
  echo json_encode($return);
}

public function ajax_banner_edit()
{
  $banner_id = $this->input->post('banner_id');

  $singleData = $this->Common_model->getsingle("cp_meditation_videos_banner",array("movie_banner_id" => $banner_id));


  //print_r($_FILES["picture"]["name"]);
 // print_r($_FILES["pictureFile"]["name"]);die;
  $config1['upload_path'] = 'uploads/meditation_videos/banners/files/';
  $config1['allowed_types'] = 'mp4|flv|wmv|webm|vob|ogg|mpeg|mpg|3gp';

           //$data['max_size'] = '5000';
  $config1['encrypt_name'] = true;

  $this->load->library('upload', $config1);

  if(!empty($_FILES["picture"]["name"])){ 
    if ($this->upload->do_upload('picture')) { 
      $attachment_data = array('upload_data' => $this->upload->data());
      $uploadfile = $attachment_data['upload_data']['file_name'];
			// $return = array('code' => 1,'message' => 'Successfully added');	
    }else{ 
     $return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
   }
 }

 $config2['upload_path'] = 'uploads/meditation_videos/banners/';
 $config2['allowed_types'] = 'jpg|png|gif|jpeg';
           //$data['max_size'] = '5000';
 $config2['encrypt_name'] = true;

 $this->upload->initialize($config2);

 if(!empty($_FILES["pictureFile"]["name"])){ 
  if ($this->upload->do_upload('pictureFile')) {
    $attachment_data_image = array('upload_data' => $this->upload->data());
    $picturefile1 = $attachment_data_image['upload_data']['file_name'];
      //  $return = array('code' => 1,'message' => 'Successfully added');		   
  }else{ 
   $return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));

 }
}

if(!empty($picturefile1)){
  $picfile = $picturefile1;
}else{
  $picfile = $singleData->movie_banner_image;
}

if(!empty($uploadfile)){
  $upfile = $uploadfile;
}else{
  $upfile = $singleData->movie_banner_file;
}

if($return['code'] != 2){ 
  $array = array(
    'movie_banner_text' => $this->input->post('banner_title'),
    'movie_banner_file'=>$upfile,
    'movie_banner_image'=>$picfile,
    'movie_banner_created' =>date('Y-m-d H:i:s'),
    'movie_banner_status'=>1
  );
  $this->Common_model->updateData("cp_meditation_videos_banner",$array,array('movie_banner_id' => $this->input->post("banner_id")));

  $return = array('code' => 1,'message' => 'Successfully updated.');
}


echo json_encode($return);
}




}