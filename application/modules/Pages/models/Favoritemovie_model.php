<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Favoritemovie_model extends CI_Model{
    function __construct() {
        
    }
	
		public function insertData($table,$datainsert)
	{
		$this->db->insert($table,$datainsert);
		return $this->db->insert_id();
	}
	
	public function updateData($table,$data,$where)
	{
	   $this->db->update($table,$data,$where);
	   return $this->db->affected_rows();
	}
	
	public function delete($table,$where)
	{
	   $this->db->delete($table,$where);
	   return;
	}
	public function deleteFaqs($table,$where)
	{
		extract($where);
		$this->db->where('faq_id', $faq_id);
		$this->db->update($table, array('status' => 3));
		return true;
	}
	
	public function getsingle($table,$where)
	{
		$q = $this->db->get_where($table,$where);
		return $q->row();
	}

    
	public function check_login_credentials($postdata){ 
		extract( $postdata ); 
		$this->db->select('*');
		//$this->db->from('admin');
		$this->db->from('admin');
		$this->db->where('username', $uname); 
		$this->db->where('password', md5($password));
		//$this->db->where('role', 1); 
		$query = $this->db->get();
		return $query->row();
		
	}
	
	 public function get_row_with_con($table,$where)
	 {
	 	$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		//echo $this->db->last_query();
		//die;
		return $query->row(); 
	 } 
    
   public function getAllwhereorderby($table,$where,$order_id,$order_by)
	{
		$this->db->select('*');
		$this->db->order_by($order_id,$order_by);
		$q = $this->db->get_where($table,$where);	
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	
	public function getAll($table)
	{
		$this->db->select('*');		
		$q = $this->db->get($table);		
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	
	
    public function getAllFaq()
    {
        $this->db->select('*');
        $this->db->from("faqs");
        $this->db->where("status !=", 3);
        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
       return $result;
    }
	
	public function getAllor($table,$order_id,$order_by)
	{
		$this->db->select('*');	
        $this->db->order_by($order_id,$order_by);		
		$q = $this->db->get($table);		
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	
	
	public function getAllwhere($table,$where)
	{ 
		$this->db->select('*');
		$q = $this->db->get_where($table,$where);
		$num_rows = $q->num_rows();
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	
	function jointwotablenn($table, $field_first, $table1, $field_second,$where='',$field,$order_id,$order_by) {

        $this->db->select($field);
	   $this->db->order_by( $order_id, $order_by);
        $this->db->from("$table");
        $this->db->join("$table1", "$table1.$field_second = $table.$field_first"); 
        if($where !=''){
        $this->db->where($where); 
        }
        $q = $this->db->get();
        if($q->num_rows() > 0) {
            foreach($q->result() as $rows) {
                $data[] = $rows;
            }
            $q->free_result();
            return $data;
        }
    }
     public function lang_data(){
        if($this->session->userdata("filterLanguage1")){
            $lang = $this->session->userdata("filterLanguage1");
        }else{
            $lang = 1;
        }
        $this->db->select('*');
        $this->db->from('language_contents');
        $this->db->where('content_language_id',$lang);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
	
	 function getfootercategories($lang){

       $this->db->select('*');	   
       $this->db->from("gc_footer_category_detail");
        $this->db->join("gc_footer_data_detail", "gc_footer_data_detail.cat_id = gc_footer_category_detail.cat_id","RIGHT"); 
       $this->db->where("gc_footer_data_detail.status", 1);
	   $this->db->where("gc_footer_category_detail.status", 1);
	   $this->db->where("gc_footer_category_detail.lang_id", $lang);
       
       $this->db->group_by("gc_footer_category_detail.cat_name");
       $this->db->order_by("gc_footer_category_detail.cat_id", "ASC");
       $query = $this->db->get();        
   //echo $this->db->last_query();die;    
       foreach ($query->result() as $category)
   {
       $return[$category->cat_id] = $category;
       $return[$category->cat_id]->subs = $this->get_sub_categories($category->cat_id,$lang); // Get the categories sub categories
   }

   return $return;   
   }
   
   public function get_sub_categories($category_id,$lang)
{
   $this->db->where('cat_id', $category_id);
    $this->db->where('status',1); 
 $this->db->where('lang_id',$lang);	
   $query = $this->db->get('gc_footer_data_detail');
  //echo $this->db->last_query();
  return $query->result();

}

function jointwotable($table, $field_first, $table1, $field_second,$where='',$field) {

        $this->db->select($field);
        $this->db->from("$table");
        $this->db->join("$table1", "$table1.$field_second = $table.$field_first","left"); 
        if($where !=''){
        $this->db->where($where); 
        }
        $q = $this->db->get();
        if($q->num_rows() > 0) {
          return $q->row();
             
        }else{
			return false;
		}
    }

/*Created by 95 for get main category*/
public function getMainCategory1($value)
{
	 //echo "test";die;
	$this->db->select("*");
	$this->db->from('cp_doctor_categories');
	$this->db->where('dc_id',$value);
	$query=$this->db->get();
	return $query->result_array();

}
/*Created by 95 for get all the doctors by category ID*/
function getDoctorList($id){
       $this->db->select('*');
       $this->db->from('cp_doctor');
       $this->db->where("FIND_IN_SET($id, doctor_category_id)");
       $query = $this->db->get();
       $result = $query->result_array();
       return $result;
    }


    /*Created by 95 on 28-2-2019 for get the multiple categories of a doctor*/
   public function getCategory1($id){
     $id = explode(',',$id);
     $this->db->select('dc_name');
     $this->db->from('cp_doctor_categories');
     $this->db->where_in('dc_id',$id);
     $query = $this->db->get();
     $result = $query->result_array();
     $last_names1 = array_column($result, 'dc_name');
     $result_data = implode(',',$last_names1);
     return $result_data;
   }



  /*Created by 95 on 28-2-2019 for get the single appointment detail*/
   public function getAppointmentDetail($id='')
   {
   	$this->db->select('*');
    $this->db->from('doctor_appointments a'); 
    $this->db->join('cp_doctor b', 'b.doctor_id=a.doctor_id', 'left');
    $this->db->join('dr_available_date c', 'c.avdate_id=a.dr_appointment_date_id', 'left');

    $this->db->join('dr_available_time d', 'd.avtime_id=a.dr_appointment_time_id', 'left');
    $this->db->where('a.dr_appointment_id',$id);
    $query = $this->db->get(); 
    if($query->num_rows() != 0)
    {
        return $query->result();
    }
    else
    {
        return false;
    }
   }
   
   public function getAppointmentActivities($appointment_id)
   {
	   $sql = "SELECT * FROM `doctor_appointments` JOIN `cp_doctor` ON `cp_doctor`.`doctor_id` = `doctor_appointments`.`doctor_id` LEFT JOIN `dr_available_date` ON `dr_available_date`.`avdate_id` = `doctor_appointments`.`dr_appointment_date_id` LEFT JOIN `dr_available_time` ON `dr_available_time`.`avtime_id` = `doctor_appointments`.`dr_appointment_time_id` WHERE `doctor_appointments`.`dr_appointment_id` = $appointment_id";

		
		$q = $this->db->query($sql);
		 
		
        if($q->num_rows() > 0) {
          return $q->row();
             
        }else{
			return false;
		}		
			
   }
   
     public function getAllappointments($user_id)
    {
    	 $this->db->select('*');
      	 $this->db->from('doctor_appointments as t1');
	     $this->db->join('cp_doctor as t2', 't1.doctor_id = t2.doctor_id', 'LEFT');
	     $this->db->join('cp_users as t3', 't3.id = t1.user_id', 'LEFT');
	     $this->db->join('dr_available_date as t4', 't4.avdate_id = t1.dr_appointment_date_id', 'LEFT');
	     $this->db->join('dr_available_time as t5', 't5.avtime_id = t1.dr_appointment_time_id', 'LEFT');
	     $this->db->where('t3.user_role!=','1');
		 $this->db->where('t3.id',$user_id);
	     $query = $this->db->get();
         return $query->result_array();
    }
	
	 public function getsearchdata($like,$cat_id)
   {
	    $sql = "SELECT * FROM `cp_doctor` WHERE FIND_IN_SET($cat_id,`doctor_category_id`) AND `doctor_name` LIKE '$like%'
ORDER BY `doctor_id` DESC";
		
		$q = $this->db->query($sql);		
		 $data =  array() ;		  
		 foreach($q->result_array() as $row)
			{
				 $data[]  = $row;
			}
			
			return $data;
   }


   /*Created by 95 for Get all the activities for show dates hilighted in calender*/
   function getAllappointmets($table, $field_first, $table1, $field_second,$where='',$field,$group_by,$order_id,$order_by) {
       $this->db->select($field);
	   $this->db->order_by( $order_id, $order_by);
	   $this->db->group_by($group_by); 
       $this->db->from("$table");
       $this->db->join("$table1", "$table1.$field_second = $table.$field_first"); 
        if($where !=''){
       $this->db->where($where); 
        }
        $q = $this->db->get();
        if($q->num_rows() > 0) {
            foreach($q->result_array() as $rows) {
                $data[] = $rows;
            }
            $q->result_array();
            return $data;
        }
    }

	function jointhreetable($table, $field_first, $table1, $field_second,$table2,$field_third,$value,$where='',$field,$order_id, $order_by) {

        $this->db->select($field);
		$this->db->order_by($order_id, $order_by);
        $this->db->from("$table");
        $this->db->join("$table1", "$table1.$field_second = $table.$field_first");
        $this->db->join("$table2", "$table2.$field_third = $table.$value");		
        if($where !=''){
        $this->db->where($where); 
        }
        $q = $this->db->get();
        if($q->num_rows() > 0) {
            foreach($q->result() as $rows) {
                $data[] = $rows;
            }
            $q->free_result();
            return $data;
        }
    }
	
	function jointhreetablenn($table, $field_first, $table1, $field_second,$table2,$field_third,$value,$where='',$field) {

        $this->db->select($field);		
        $this->db->from("$table");
        $this->db->join("$table1", "$table1.$field_second = $table.$field_first");
        $this->db->join("$table2", "$table2.$field_third = $table.$value");		
        if($where !=''){
        $this->db->where($where); 
        }
        $q = $this->db->get();
        if($q->num_rows() > 0) {
          return $q->row();
             
        }else{
			return false;
		}
    }
	
	/*Created by 95 for Get all the activities for show dates hilighted in calender*/
  function getAllDoctorappointmets($table, $where='',$field,$group_by) {
  
      $this->db->select($field);
  
  $this->db->group_by($group_by); 
      $this->db->from("$table");
     
       if($where !=''){
      $this->db->where($where); 
       }
       $q = $this->db->get();

       if($q->num_rows() > 0) {
           foreach($q->result_array() as $rows) {
               $data[] = $rows;
           }
           $q->result_array();
           return $data;
       }
   }
   
   public function getAllorlimit($table,$order_id,$order_by,$limit)
	{
		$this->db->select('*');	
        $this->db->order_by($order_id,$order_by);
        $this->db->limit($limit);		
		$q = $this->db->get($table);		
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	
	// public function movie_listing($per_page,$page)
	// {
	// 	$sql = 'SELECT * FROM `movie` ORDER BY `movie_id` DESC LIMIT '.$page.','.$per_page;
	   
	//    $q = $this->db->query($sql);
	// 	 $data =  array() ;
	// 	 //echo $this->db->last_query();die;
		 
	// 	 foreach($q->result_array() as $row)
	// 		{
	// 			 $data[]  = $row;
	// 		}
			
	// 		$ret['rows'] = $data;
			
	//   $sql2 = 'SELECT * FROM `movie` ORDER BY `movie_id` DESC';
	//     $q = $this->db->query($sql2);
 //            $data = array();
	// 		foreach($q->result_array() as $row)
	// 		{
	// 			 $data[]  = $row;
	// 		}
			
	// 		$ret['num_rows'] = count($data);
			
	// 		return $ret;
	// }



	public function Favoritemovie_listing($per_page,$page)
	{

		//echo "<pre>";


		$user_id = $this->session->userdata('logged_in')['id'];

		$sql= 'SELECT * FROM cp_movie_favorite a JOIN movie b ON a.movie_id = b.movie_id where a.user_id='.$user_id.' ORDER BY `movie_fav_id` DESC LIMIT '.$page.','.$per_page;


	
		//die();
		//$sql = 'SELECT * FROM `cp_music_favorite` where `user_id`= '.$user_id.' ORDER BY `music_fav_id` DESC LIMIT '.$page.','.$per_page;
	   
	   $q = $this->db->query($sql);
		 $data =  array() ;
		 //echo $this->db->last_query();die;
		 
		 foreach($q->result_array() as $row)
			{

				$musicDeleteId = $this->favoritemovie_model->getAllwhere('cp_movie_trash',array('movie_id' => $row['movie_id']));
					if($musicDeleteId){
					continue;
					}


				 $data[]  = $row;
			}
			
			$ret['rows'] = $data;
			
	  $sql2 = 'SELECT * FROM cp_movie_favorite a LEFT JOIN movie b ON a.movie_id = b.movie_id where a.user_id='.$user_id.' ORDER BY `movie_fav_id` DESC';
	    $q = $this->db->query($sql2);
		//echo $this->db->last_query();die;	
            $data = array();
			foreach($q->result_array() as $row)
			{
				$musicDeleteId = $this->favoritemovie_model->getAllwhere('cp_movie_trash',array('movie_id' => $row['movie_id']));
					if($musicDeleteId){
					continue;
					}
				
				 $data[]  = $row;
			}
		
			$ret['num_rows'] = count($data);
			
			return $ret;
	}



}