﻿<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Caregiver extends MX_Controller {

  public function __construct() {

    parent:: __construct();

    $this->load->library('session');

    $this->load->library('form_validation');

    $this->load->library('table');

    $this->load->library('pagination');   

    $this->load->model('common_model_new');

    $this->load->helper(array('common_helper'));

    $this->load->helper(array('url'));

    $this->load->helper(array('form'));  

not_login();    

  }

  public function dashboard() {

    $data = array();

    $data['menuactive'] = $this->uri->segment(1);

		$checkLogin = $this->session->userdata('logged_in');
	
		if(!empty($checkLogin) && $checkLogin !=''){
			$user_id = $checkLogin['id'];
			$data['caregiver_array'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'Caregiver_profile',"user_id"=>$user_id));
		}
    $this->template->set('title', 'Dashboard');
    $this->template->load('new_user_dashboard_layout', 'contents', 'dashboard', $data);
  }

  public function logout(){

        $this->session->unset_userdata('user_id');

        $this->session->sess_destroy();

        redirect('caregiver_login');

      }

  

}     

?>

