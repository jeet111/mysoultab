<!-- <section class="h__inner_sec wow fadeInUp" data-wow-delay=".2s">
    <div class="container">
        <div class="row">
            <div class="hed-sec-top">
                <h2>Subcription</h2>
                <div class="brdcurm"> <a href="<?php echo base_url();?>">Home</a> <i class="fa fa-angle-right"></i> <span>subcription</span> </div>
            </div>
        </div></div>
    </section> -->


    <main id="main_inner">

        <section  class="teacherSection ">

            <div class="container " >

                <div class="subsc_h_sn">
                    <div   id="section2"  class="bigHeading">
                      <div class="bigText">Pricing</div>
                      <h2 class="margin-bottom-xxl text-center">Our<strong> pricing</strong></h2>
                  </div></div>



                  <div class="sec_one_box">

                    <div class="row">


                      <div class=" subcription">

                         <div class=" subcription_one">

                            <div class="member_story_s_one">

                                <div class="col-md-7">
                                 <div class="member_story_s_on_l ">
                                     <img src="<?php echo base_url();?>assets/images/subsc_img.jpg">

                                     <!-- <span class="vid_butt"><a href="#"> <img src="<?php echo base_url();?>assets/images/play_button.png"></a></span> -->

                                 </div>
                             </div>

                             <div class="col-md-5">
                                 <div class="subsc_story_s">
                                   <h2>Why Soul Tab is Affordable for Everyone ?</h2> 
                                   <p>We aim to provide an affordable solution to the elderly who are going through a tough time during this global COVID-19 pandemic. With the global recession affecting the older age group the most, incomes have dwindled while the expenses have skyrocketed. Keeping this in perspective, we tried keeping the price as low as we could.</p>

                               </div>
                           </div>



                       </div>

                   </div>

               </div>




           </div>
       </div>


   </div>

</section>





<section  class="How_it_works_s m_b_t" data-aos="fade-up">

    <div class="container">
     <div   id="section2"  class="bigHeading wow fadeInDown animated">
      <div class="bigText">Works</div>
      <h2 class="margin-bottom-xxl text-center">How <strong>it works</strong></h2>
  </div>



  <div class="How_it_works_s_on">
    <div class="How_it_works_s_on_main">
        <div class="home_five_box  wow fadeInDown  animated"> 

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="categorie_item">
                  <div class="cate_item_block hi-icon-effect-8">
                    <div class="cate_item_social hi-icon"><img src="<?php echo base_url();?>assets/images/work_one.png"></div>
                    <h1>Select plan & Click Subscribe</h1>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="categorie_item">
              <div class="cate_item_block hi-icon-effect-8">
                <div class="cate_item_social hi-icon"><img src="<?php echo base_url();?>assets/images/work_two.png"></div>
                <h1>Register & Make Payment</h1>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="categorie_item">
          <div class="cate_item_block hi-icon-effect-8">
            <div class="cate_item_social hi-icon"><img src="<?php echo base_url();?>assets/images/work_three.png"></div>
            <h1>Receive Tablet in Mail Activate tablet and enjoy</h1>
        </div>
    </div>
</div>


</div>
</div>
</div>


</div>

</section>


<div class="more_butt_sec-ar ne_d_i">
  <span class="down_aro">
    <a href="#next_sec2"> <img src="<?php echo base_url();?>assets/images/down_arroe.png"> </a></span>
</div>



<section  class="our_plan_s" data-aos="fade-up">
    <div class="container">

     <div id="next_sec2"></div>
     

     <div   id="section2"  class="bigHeading wow fadeInDown animated">
      <div class="bigText">Plans</div>
      <h2 class="margin-bottom-xxl text-center">Choose your<strong> Subscription</strong></h2>
  </div>




  <div class="our_plan_s_main">



    <div id="generic_price_table">   
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!--PRICE HEADING START-->

                        <!--//PRICE HEADING END-->
                    </div>
                </div>
            </div>
            <div class="container">

                <!--BLOCK ROW START-->
                <div class="row">

                  <div class="price_tab_s_new  wow fadeInDown animated">

                    <?php foreach ($subscriptionData as $sub) { ?>
                        <div class="col-md-4">

                            <!--PRICE CONTENT START-->
                            <div class="generic_content clearfix">

                                <!--HEAD PRICE DETAIL START-->
                                <div class="generic_head_price clearfix">

                                    <!--HEAD CONTENT START-->
                                    <div class="generic_head_content clearfix">

                                        <!--HEAD START-->
                                        <div class="head_bg"></div>
                                        <div class="head">
                                            <span><?php echo $sub->plan_name; ?></span>
                                        </div>
                                        <!--//HEAD END-->

                                    </div>
                                    <!--//HEAD CONTENT END-->

                                    <!--PRICE START-->
                                    <div class="generic_price_tag clearfix">    
                                        <span class="price">
                                            <span class="sign">$</span>
                                            <span class="currency"><?php echo $sub->amount; ?></span>
                                            <!-- <span class="cent">.99</span> -->
                                            <!-- <span class="month">/MON</span> -->
                                        </span>
                                    </div>
                                    <!--//PRICE END-->

                                </div>                            
                                <!--//HEAD PRICE DETAIL END-->

                                <!--FEATURE LIST START-->
                                <div class="generic_feature_list">
                                    <ul>
                                        <li><span><?php echo $sub->description; ?></span></li>
                                            <!-- <li><span>Lorem Ipsum</span>  is simply dummy</li>
                                            <li><span>Lorem Ipsum</span>  is simply dummy</li>
                                            <li><span>Lorem Ipsum</span>  is simply dummy</li>
                                            <li><span>Lorem Ipsum</span>  is simply dummy</li> -->
                                        </ul>
                                    </div>
                                    <!--//FEATURE LIST END-->

                                    <!--BUTTON START-->

                                    <div class="generic_price_btn clearfix">
                                        <!-- <form action="<?php //echo base_url('signup');?>/<?php //echo base64_encode($sub->id); ?>" method="post"> -->
                                            <!-- <input type="hidden" name="plan_id" value="<?php echo $sub->id; ?>"> -->
                                            <a class="" href="<?php echo base_url('signup');?>/<?php echo base64_encode($sub->id); ?>">Get started</a>
                                            <!-- </form> -->
                                        </div>
                                        <!--//BUTTON END-->

                                    </div>
                                    <!--//PRICE CONTENT END-->

                                </div>

                            <?php } ?>
                        <?php /* ?>

                        <div class="col-md-4">

                            <!--PRICE CONTENT START-->
                            <div class="generic_content active clearfix">

                                <!--HEAD PRICE DETAIL START-->
                                <div class="generic_head_price clearfix">

                                    <!--HEAD CONTENT START-->
                                    <div class="generic_head_content clearfix">

                                        <!--HEAD START-->
                                        <div class="head_bg"></div>
                                        <div class="head">
                                            <span>Standard</span>
                                        </div>
                                        <!--//HEAD END-->

                                    </div>
                                    <!--//HEAD CONTENT END-->

                                    <!--PRICE START-->
                                    <div class="generic_price_tag clearfix">    
                                        <span class="price">
                                            <span class="sign">$</span>
                                            <span class="currency">199</span>
                                            <span class="cent">.99</span>
                                            <span class="month">/MON</span>
                                        </span>
                                    </div>
                                    <!--//PRICE END-->

                                </div>                            
                                <!--//HEAD PRICE DETAIL END-->

                                <!--FEATURE LIST START-->
                                <div class="generic_feature_list">
                                    <ul>
                                     <li><span>Lorem Ipsum</span>  is simply dummy</li>
                                     <li><span>Lorem Ipsum</span>  is simply dummy</li>
                                     <li><span>Lorem Ipsum</span>  is simply dummy</li>
                                     <li><span>Lorem Ipsum</span>  is simply dummy</li>
                                     <li><span>Lorem Ipsum</span>  is simply dummy</li>
                                 </ul>
                             </div>
                             <!--//FEATURE LIST END-->

                             <!--BUTTON START-->
                             <div class="generic_price_btn clearfix">
                                <a class="" href="">Get started</a>
                            </div>
                            <!--//BUTTON END-->

                        </div>
                        <!--//PRICE CONTENT END-->

                    <!-- </div> -->
                    <div class="col-md-4">

                        <!--PRICE CONTENT START-->
                        <div class="generic_content clearfix">

                            <!--HEAD PRICE DETAIL START-->
                            <div class="generic_head_price clearfix">

                                <!--HEAD CONTENT START-->
                                <div class="generic_head_content clearfix">

                                    <!--HEAD START-->
                                    <div class="head_bg"></div>
                                    <div class="head">
                                        <span>Unlimited</span>
                                    </div>
                                    <!--//HEAD END-->

                                </div>
                                <!--//HEAD CONTENT END-->

                                <!--PRICE START-->
                                <div class="generic_price_tag clearfix">    
                                    <span class="price">
                                        <span class="sign">$</span>
                                        <span class="currency">299</span>
                                        <span class="cent">.99</span>
                                        <span class="month">/MON</span>
                                    </span>
                                </div>
                                <!--//PRICE END-->

                            </div>                            
                            <!--//HEAD PRICE DETAIL END-->

                            <!--FEATURE LIST START-->
                            <div class="generic_feature_list">
                                <ul>
                                    <li><span>Lorem Ipsum</span>  is simply dummy</li>
                                    <li><span>Lorem Ipsum</span>  is simply dummy</li>
                                    <li><span>Lorem Ipsum</span>  is simply dummy</li>
                                    <li><span>Lorem Ipsum</span>  is simply dummy</li>
                                    <li><span>Lorem Ipsum</span>  is simply dummy</li>
                                </ul>
                            </div>
                            <!--//FEATURE LIST END-->

                            <!--BUTTON START-->
                            <div class="generic_price_btn clearfix">
                                <a class="" href="">Get started</a>
                            </div>
                            <!--//BUTTON END-->

                        </div>
                        <!--//PRICE CONTENT END-->

                    </div>

                    <?php */ ?>

                </div>
            </div>  
            <!--//BLOCK ROW END-->

        </div>
    </section>             

</div>


</div>
</div>


</section>




</main>  