<aside class="right-side">
	<section class="content-header no-margin">
		<h1 class="text-center"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Doctors List</h1>
	</section>
	<section class="doc_cate">
		<div class="doc_cateMidd">
			<div class="row">
				<div class="tm-box-col-wrapper col-lg-3 col-sm-6 col-md-3 col-xs-12 dermatologist">
					<article class="themetechmount-box themetechmount-box-team themetechmount-teambox-view-overlay">
						<div class="themetechmount-post-item">
							<div class="themetechmount-team-image-box">
								<span class="themetechmount-item-thumbnail">
									<span class="themetechmount-item-thumbnail-inner">
										<img src="http://brivona.themetechmount.com/brivona-overlay/wp-content/uploads/sites/10/2018/11/doctor-team-001-1-380x500.jpg" class="attachment-themetechmount-img-teamoverlay size-themetechmount-img-teamoverlay wp-post-image" alt="">
									</span>
								</span>
								<div class="themetechmount-overlay">
									<div class="themetechmount-icon-box themetechmount-box-link">
									</div>
								</div>
							</div>
							<div class="themetechmount-box-content">
								<div class="themetechmount-box-title">
									<h4>
										<a href="http://brivona.themetechmount.com/brivona-overlay/team-member/dr-metthew-wood/">Dr. Metthew Wood</a>
									</h4>
								</div>
								<div class="themetechmount-team-position">Cardiologist, Neurology Specialist</div>
							</div>
						</div>
					</article>
				</div>
			</div>
		</section>
	</aside>