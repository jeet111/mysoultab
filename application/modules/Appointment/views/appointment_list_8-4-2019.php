<aside class="right-side">    
    <section class="content-header no-margin">
        <h1 class="text-center"><i class="fa fa-list"></i> Appointment List <div class="rem_add">
            <!-- <a href="#" class="btn btn-primary" >Add</a> -->
            </div>
        </h1>
    </section>
    <section class="content photo-list">
	    <div class="photo-listMain reminder_listMain"> 
            <div class="row">
                    <div class="col-md-4">    
                        <div class="form-group new_selector">
                            <label>Doctor Name:<span class="red_star">*</span></label>
                            <select name="doctor_name" id="doctor_name" class="form-control">
                                <option value="">Please select Doctor Name</option>
                                <option value="33">Swt</option>
                                <option value="32">Abhinav</option>
                                <option value="26">Smith jonson</option>
                                <option value="23">Abhisar garwal</option>
                                <option value="20">Rahul jaiswal</option>
                                <option value="18">Aman Verma</option>
                                <option value="17">Animesh Rathore</option>
                                <option value="14">Dharmendra Raghuwanshi</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="control-label" for="">Username</label>
                        <input type="text" name="username" id="username" value="" placeholder="Username" class="form-control">
                       </div>
                    </div>
                    <div class="col-md-4">
                        <div class="bootstrap-timepicker">
                            <div class="form-group">
                                <label>Date:<span class="red_star">*</span></label>
                                <div class="input-group date costom_err" id="datetimepickerhs">
                                    <input id="datetimepickerh" type="text" name="reminder_date" class="form-control hasDatepicker" value="2019-04-30"  >
                                    <span class="input-group-addon">
                                        <span class="glyphicon-calendar glyphicon"></span>
                                    </span>
                                </div>
                            </div>
                        </div>             
                    </div>
                </div>
                <div class="row">   
                                      
                    <div class="col-md-4">
                        <div class="bootstrap-timepicker" id="slots_avail">
                            <div class="form-group">
                                <label>Time:<span class="red_star">*</span></label>
                                <ul class="list_time">
                                    <li>
                                        <input type="radio" class="timeslots" id="f-option_720" data-slot="Morning" name="selector" value="Morning">
                                        <label for="f-option_720">Morning</label>
                                        <div class="check"></div>     
                                    </li>                                                
                                    <li>
                                        <input type="radio" class="timeslots" id="f-option_722" data-slot="Noon" name="selector" value="Noon">
                                        <label for="f-option_722">Noon</label>
                                        <div class="check"></div>     
                                    </li>                                                
                                    <li>
                                        <input type="radio" checked="" class="timeslots" id="f-option_723" data-slot="Evening" name="selector" value="Evening">
                                        <label for="f-option_723">Evening</label>
                                        <div class="check"></div>     
                                    </li>
                                </ul>
                            </div>
                        </div>             
                    </div>
                    <div class="col-md-4">
                        <div class="box-footer">
                            <input type="submit" name="submit" class="btn btn-primary" value="Submit">
                        </div>
                    </div>
                </div>
                <div class="bx_list">
                    <ul class="list_appi">
                        <li>                            
                            <div class="pro_imgOne">                        
                                <img src="http://votivelaravel.in/carepro/uploads/doctor/happydoctor.jpg" class="" alt="">                        
                            </div>
                            <div class="dr_name">                                
                                <h4>Doctor Ran </h4>
                                <h5>Neurosurgeon</h5>
                            </div>
                            <div class="dr_date">   
                                <h4>17 Oct. 2018</h4>
                                <h5>8:15 PM</h5>
                            </div>
                            <div class="dr_viw">   
                                <div class="btn-group new-music-btn">
                                    <a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#"><i class="fa fa-eye" aria-hidden="true"></i>View</a>
                                        <a class="dropdown-item" href="#"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>      
                                  </div>
                                </div>                               
                            </div>
                        </li>
                        <li>                            
                            <div class="pro_imgOne">                        
                                <img src="http://votivelaravel.in/carepro/uploads/doctor/happydoctor.jpg" class="" alt="">                        
                            </div>
                            <div class="dr_name">                                
                                <h4>Doctor Ran </h4>
                                <h5>Neurosurgeon</h5>
                            </div>
                            <div class="dr_date">   
                                <h4>17 Oct. 2018</h4>
                                <h5>8:15 PM</h5>
                            </div>
                            <div class="dr_viw">   
                                <div class="btn-group new-music-btn">
                                    <a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#"><i class="fa fa-eye" aria-hidden="true"></i>View</a>
                                        <a class="dropdown-item" href="#"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>      
                                  </div>
                                </div>                               
                            </div>
                        </li>
                        <li>                            
                            <div class="pro_imgOne">                        
                                <img src="http://votivelaravel.in/carepro/uploads/doctor/happydoctor.jpg" class="" alt="">                        
                            </div>
                            <div class="dr_name">                                
                                <h4>Doctor Ran </h4>
                                <h5>Neurosurgeon</h5>
                            </div>
                            <div class="dr_date">   
                                <h4>17 Oct. 2018</h4>
                                <h5>8:15 PM</h5>
                            </div>
                            <div class="dr_viw">   
                                <div class="btn-group new-music-btn">
                                    <a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#"><i class="fa fa-eye" aria-hidden="true"></i>View</a>
                                        <a class="dropdown-item" href="#"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>      
                                  </div>
                                </div>                               
                            </div>
                        </li>
                        <li>                            
                            <div class="pro_imgOne">                        
                                <img src="http://votivelaravel.in/carepro/uploads/doctor/happydoctor.jpg" class="" alt="">                        
                            </div>
                            <div class="dr_name">                                
                                <h4>Doctor Ran </h4>
                                <h5>Neurosurgeon</h5>
                            </div>
                            <div class="dr_date">   
                                <h4>17 Oct. 2018</h4>
                                <h5>8:15 PM</h5>
                            </div>
                            <div class="dr_viw">   
                                <div class="btn-group new-music-btn">
                                    <a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#"><i class="fa fa-eye" aria-hidden="true"></i>View</a>
                                        <a class="dropdown-item" href="#"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>      
                                  </div>
                                </div>                               
                            </div>
                        </li>
                        <li>                            
                            <div class="pro_imgOne">                        
                                <img src="http://votivelaravel.in/carepro/uploads/doctor/happydoctor.jpg" class="" alt="">                        
                            </div>
                            <div class="dr_name">                                
                                <h4>Doctor Ran </h4>
                                <h5>Neurosurgeon</h5>
                            </div>
                            <div class="dr_date">   
                                <h4>17 Oct. 2018</h4>
                                <h5>8:15 PM</h5>
                            </div>
                            <div class="dr_viw">   
                                <div class="btn-group new-music-btn">
                                    <a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#"><i class="fa fa-eye" aria-hidden="true"></i>View</a>
                                        <a class="dropdown-item" href="#"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>      
                                  </div>
                                </div>                               
                            </div>
                        </li>
                        <li>                            
                            <div class="pro_imgOne">                        
                                <img src="http://votivelaravel.in/carepro/uploads/doctor/happydoctor.jpg" class="" alt="">                        
                            </div>
                            <div class="dr_name">                                
                                <h4>Doctor Ran </h4>
                                <h5>Neurosurgeon</h5>
                            </div>
                            <div class="dr_date">   
                                <h4>17 Oct. 2018</h4>
                                <h5>8:15 PM</h5>
                            </div>
                            <div class="dr_viw">   
                                <div class="btn-group new-music-btn">
                                    <a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#"><i class="fa fa-eye" aria-hidden="true"></i>View</a>
                                        <a class="dropdown-item" href="#"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>      
                                  </div>
                                </div>                               
                            </div>
                        </li>
                    </ul>
                </div>           
        </div>
    </section>
</aside>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>
               