<div class="container-fluid">
	<div class="dash-counter users-main my-sts-table">
		<div class="row">
			<div class="col-md-12">
				<div class="">
					<?php //echo "<pre>"; print_r($MusicData); echo "</pre>";?>
					<div class="card-body">
						<div class="status-cng"></div>
						<?php if ($this->session->flashdata('success')) { ?>
                    <div class="alert alert-success message">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <?php echo $this->session->flashdata('success'); ?></div>
                <?php } ?>
					<table class="table table-hover tab_comn" id="sampleTable">
						<thead>
							<tr>
								<th>S no.</th>
								<th>Restaurant Name</th>
								<th>Username</th>					
															
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i = 1;
						foreach ($restaurant_data as $restaurant) {
							$username_value = $this->Common_model->getsingle("cp_users",array("id" => $restaurant->user_id));
								?>
							<tr>
								<td><?php echo $i++; ?></td>
								<td><?php echo $restaurant->rest_name; ?></td>
								<td><?php echo $username_value->username; ?></td>
								
								
								<td>
									<div class="link-del-view">
										<div class="tooltip-2">
											<a href="<?php echo base_url(); ?>admin/view_restaurant/<?php echo $restaurant->rest_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
											<span class="tooltiptext">View</span>
										</div>
										<div class="tooltip-2">
											<a href="<?php echo base_url(); ?>admin/delete_restaurant/<?php echo $restaurant->rest_id; ?>" onclick="return confirm('Delete this record?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

											
										</div>
										
										
									</div>
									
							</div>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>
</div>
</div>