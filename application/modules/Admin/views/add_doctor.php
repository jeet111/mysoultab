<style>

	.lgn label.error {

		font-size:12px;

	}

	#dol{

		position: absolute;

		margin: 4px;

	}

</style>

<div class="container-fluid">

	<div class="dash-counter">

		<div class="Schedule_main_one">

			<div class="users-main">

				<?php

				echo "<h2>Add Doctor</h2>";

				//echo $this->uri->segment(2);

				?>

				<div class="btn_topBack">

					<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">

						Back

					</a>

				</div>

				<form class="form-horizontal" action="" method="post" enctype="multipart/form-data" name="Adddoctor" id="doctor_add">

					<?php if ($this->session->flashdata('success')) { ?>

						<div class="alert alert-success message">

							<button type="button" class="close" data-dismiss="alert">x</button>

							<?php echo $this->session->flashdata('success'); ?></div>

						<?php } ?>

						<div class="row">

							<div class="col-md-6">

								<div class="form-group">

									<label class="control-label col-md-4" for="">Doctor Name:<span class="field_req">*</span></label>

									<div class="col-md-8">

										<input type="text" placeholder="Doctor Name" class="form-control" name="doctor_name" id="doctor_name" value="<?php if(!empty($singleData->doctor_name)){ echo $singleData->doctor_name;} ?>">

										<?php echo form_error('doctor_name'); ?>

									</div>

								</div>

							</div>

							<div class="col-md-6">

								<div class="form-group">

									<label class="control-label col-md-4" for="">Doctor Category:<span class="field_req">*</span></label>

									<div class="col-md-8 multidrop">

										<select name="doctor_category[]" id="doctor_category" class="form-control" multiple >

											<?php foreach($doctor_category as $doctor_cat){

												$exp_sub2 = explode(',',$singleData->doctor_category_id);

												$inarrayinterior = in_array($doctor_cat->dc_id,$exp_sub2);

												?>

												<option value="<?php echo $doctor_cat->dc_id; ?>" <?php if($inarrayinterior){ echo 'selected'; }?>><?php echo ucfirst($doctor_cat->dc_name); ?></option>

											<?php } ?>

										</select>

										<?php echo form_error('doctor_category'); ?>

									</div>

								</div>

							</div>

						</div>

						<div class="row">

							<div class="col-md-6">

								<div class="form-group">

									<label class="control-label col-md-4" for="">Mobile No:<span class="field_req">*</span></label>

									<div class="col-md-8">

										<input type="text" placeholder="Mobile No" class="form-control" name="doctor_mobile" id="doctor_mobile" value="<?php if(!empty($singleData->doctor_mob_no)){ echo $singleData->doctor_mob_no;} ?>">

										<?php echo form_error('doctor_mobile'); ?>

									</div>

								</div>

							</div>

				

							<div class="col-md-6">

								<div class="form-group">

									<label class="control-label col-md-4" for="">Email:<span class="field_req">*</span></label>

									<div class="col-md-8">

										<input type="text" placeholder="Email" class="form-control" name="doctor_email" id="doctor_email" value="<?php if(!empty($singleData->doctor_email)){ echo $singleData->doctor_email;} ?>">

										<?php echo form_error('doctor_email'); ?>

									</div>

								</div>

							</div>
											</div>
											<div class="row">
							<div class="col-md-6">

								<div class="form-group">

									<label class="control-label col-md-4" for="">Profile image:<span class="field_req"></span></label>

									<div class="col-md-8">

										<div class="input-group lgn">

											<span class="input-group-btn">

												<!-- <span class="btn btn-default btn-files"> -->

													<input type="file" id="profile_images" class="form-control" name="picture"  onchange="readURL(event)">

													<!-- <img id="blah" src="#" alt="your image" /> -->

													<!-- </span> -->

												</span>

												<div id="flupmsg" style="color: red"></div>

											</div>

											<div>

												<span id="demo" style="color: red"></span>

											</div>

											<div class="image_box">

												<img id="img-upload" class="imgclas">

											</div>

										</div>

									</div>

								</div>

						

								<div class="col-md-6">

									<div class="form-group">

										<label class="control-label col-md-4" for="">Fax Number:<span class="field_req"></span></label>

										<div class="col-md-8">

											<input type="text" placeholder="Fax number" class="form-control" name="fax_num" id="fax_num" value="<?php if(!empty($singleData->fax_num)){  echo $singleData->fax_num; }?>">

											<?php echo form_error('fax_num'); ?>

										</div>

									</div>

								</div>

								</div>

<div class="row">

								<div class="col-md-6">

									<div class="form-group"> 

										<label class="control-label col-md-4" for="">Doctor Address:<span class="field_req"></span></label>

										<div class="col-md-8">

											<textarea name="doctor_address" id="doctor_address" class="form-control" placeholder="Doctor Address"><?php if(!empty($singleData->doctor_address)){ echo $singleData->doctor_address;} ?></textarea>

											<?php echo form_error('doctor_address'); ?>

										</div>

									</div>

								</div>

						

					</div>

					<div class="form-group">

						<div class="col-sm-12">

							<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit">

							<!-- <a class="cancel-btn btn" href="javascript:window.history.back()">

												Back

											</a> -->

										</div>

									</div>

								</form>

							</div>

						</div>

					</div>

				</div>

				<!--Added by 95 on 5-2-2019 For Doctor category for validation-->

				<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

				<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>

				<script type="text/javascript">

					$(document).ready(function() {

						$.validator.addMethod("regx", function(value, element, regexpr) {

							return regexpr.test(value);

						}, "Please upload valid extension jpg,jpeg,png file.");

						$("#doctor_add").validate({

							rules: {

								doctor_name: "required",
								doctor_category:"required",
								doctor_mobile:"required",
								doctor_email:"required",
								fax_num:"required",
								doctor_address:"required",

								//doctor_email: "required",



								doctor_email:{

									required: true,

									email: true,

								},

								doctor_mobile: {

									required:true,

									number:true,

									minlength:10,

									maxlength:16,

								},

								doctor_fees: {

									required:true,

									number:true,

								},

								picture:{

									required:false,

									//regx: /\.(jpe?g|png|gif|bmp)$/i,

								}

							},

							messages: {
								fax_num: "Please enter Fax number",
								doctor_address:"Please enter doctor address",
								doctor_name: "Please enter doctor name.",

								//doctor_email: "Please enter doctor email.",

								doctor_email:{

									required: "Please enter an email.",

									email: "Please enter a valid email.",

								},

								"doctor_category[]":"Please select category.",

								//doctor_address:"Please enter address.",

								doctor_mobile:"Please enter mobile number.",

								doctor_fees:{

									required:"Please enter fees.",

									number:"Please enter numeric value.",

								},

								// fax_num:{

								// 	required: "Please enter fax number.",

								// 	number: "Please enter valid fax number.",

								// },

								doctor_mobile:{

									required:"Please enter mobile number.",

									number:"Please enter only number.",

									minlength:"Please enter at least 10 digits.",

									maxlength:"Please do not enter more than 16 digits.",

								},

								picture:{

									required:"<font size='2.9px'>Please select an image.</font>",

									regx: "Allow formates jpeg,png,gif,bmp.",

								}

							},

						});

					});

					/*Created by 95 on 6-2-2019 For Image validation*/

// function showFileSize(){

//    var img  = document.getElementById('profile_images').value;

//    	var file = img.lastIndexOf('.');

//    	var fileType	= img.slice(file);

//     if(img==='' || img===null){

//     	document.getElementById("demo").innerHTML="Please choose an image.";

// 	}else if(fileType!='.jpg'||fileType!='.png'||fileType!='.gif'||fileType!='.jpeg'){

// 			document.getElementById("demo").innerHTML="Only allowed image formats jpg,png,gif,jpeg";

// 		}else{

// 			document.getElementById("demo").innerHTML=" ";

// 		}

// 	}

function showFileSize(){

	var img  = document.getElementById('profile_images').value;

	if(img==='' || img===null){

		document.getElementById("demo").innerHTML="Please choose an image.";

	}else{

		document.getElementById("demo").innerHTML=" ";

	}

}

// /*Created by 95 on 6-2-2019 For Image validation*/

// function showFileSize(){

//    var img  = document.getElementById('profile_images').value;

//     if(img==='' || img===null){

//     	document.getElementById("demo").innerHTML="Please choose an image.";

// 	}else{

// 			document.getElementById("demo").innerHTML=" ";

// 		}

// 	}

/*Created by 95 on 22-2-2019 for disable alphabets in dr. fees field*/

$('#doctor_fees').keypress(function (e) {

	var regex = new RegExp("^[0-9]+$");

	var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);

	if (regex.test(str)) {

		return true;

	}

	e.preventDefault();

	return false;

});

/*Created by 95 on 22-2-2019 for disable alphabets in dr. mobile field*/

$('#doctor_mobile').keypress(function (e) {

	var regex = new RegExp("^[0-9]+$");

	var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);

	if (regex.test(str)) {

		return true;

	}

	e.preventDefault();

	return false;

});

</script>

<script type="text/javascript">

	$(document).ready(function() {

		$('#doctor_category').multiselect({

			includeSelectAllOption: true,

			allSelectedText: 'No option left ...'

		});

	});

// function readURL(input) {

//         if (input.files && input.files[0]) {

//             var reader = new FileReader();

//             reader.onload = function (e) {

//                 $('#img-upload')

//                     .attr('src', e.target.result)

//                     .width(150)

//                     .height(200);

//             };

//             reader.readAsDataURL(input.files[0]);

//         }

//     }

$('.image_box').hide();

function readURL (event) {

	var pcFile = $('input[type=file]').val().split('\\').pop();

	var pcExt     = pcFile.split('.').pop();

	var output = document.getElementById('img-upload');

	if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"

		|| pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){

		output.src = URL.createObjectURL(event.target.files[0]);

	$('.image_box').show();

}else{

	$('.image_box').hide();

}

};

</script>

<style>

	article, aside, figure, footer, header, hgroup,

	menu, nav, section { display: block; }

</style>