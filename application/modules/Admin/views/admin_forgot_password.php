<div class="main_admin_sec">
  <div class="main_admin_sec_inner forgot_admin">
    <div class="admin_log_sec">

      <a class="Admin_log_logo" href="<?php echo base_url();?>admin/login"><img src="<?php echo base_url();?>assets/img/logo-icon.png" style="width:100px; height: 100px; margin-left: 135px; text-align: center;"></a>
      <h2>Forgot Password</h2>
      <span  class="help-block <?php echo (empty($error_msg)?'success_msg':'error_msg'); ?>">
        <?php echo (empty($error_msg)?$success_msg:$error_msg); ?></span>

        <form id="form_id" action='<?php echo base_url()."admin/forgot/password" ?>' method="post">

          <div class="form-group">
            <input type="email" class="form-control" name="user_email" placeholder="Email"  value="<?php echo set_value('user_email'); ?>">
            <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>
          </div>

          <div class="form-group">
            <input type="submit" name="passwordSubmit" class="btn-primary" value="RESET"/>
            <a class="btn btn-md" href="<?php echo base_url()."admin/login" ?>">Cancel</a>
          </div>
        </form>

      </div>
    </div>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
  <script type="text/javascript">

    $("#form_id").validate({
      rules: {
        user_email:  {
          required: true,

        },
        passwordSubmit: {
          required: true,

        }
      }

    });
  </script>