<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2017, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Array Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/helpers/array_helper.html
 */

// ------------------------------------------------------------------------

if ( ! function_exists('element'))
{
	/**
	 * Element
	 *
	 * Lets you determine whether an array index is set and whether it has a value.
	 * If the element is empty it returns NULL (or whatever you specify as the default value.)
	 *
	 * @param	string
	 * @param	array
	 * @param	mixed
	 * @return	mixed	depends on what the array contains
	 */
	function element($item, array $array, $default = NULL)
	{
		return array_key_exists($item, $array) ? $array[$item] : $default;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('random_element'))
{
	/**
	 * Random Element - Takes an array as input and returns a random element
	 *
	 * @param	array
	 * @return	mixed	depends on what the array contains
	 */
	function random_element($array)
	{
		return is_array($array) ? $array[array_rand($array)] : $array;
	}
}

// --------------------------------------------------------------------

if ( ! function_exists('elements'))
{
	/**
	 * Elements
	 *
	 * Returns only the array items specified. Will return a default value if
	 * it is not set.
	 *
	 * @param	array
	 * @param	array
	 * @param	mixed
	 * @return	mixed	depends on what the array contains
	 */
	function elements($items, array $array, $default = NULL)
	{
		$return = array();

		is_array($items) OR $items = array($items);

		foreach ($items as $item)
		{
			$return[$item] = array_key_exists($item, $array) ? $array[$item] : $default;
		}

		return $return;
	}
}

/*add these by milan rohit 25/02/2021*/
if ( ! function_exists('setting_btn_arr'))
{
	function setting_btn_arr($insertId)
	{
		$created = date('Y-m-d H:i:s');
		$updated = date('Y-m-d H:i:s');

		$setting_btn_arr = array(
			
			// array(
			// 	'user_id' => $insertId ,
			// 	'btnname' => 'Call',
			// 	'btnicon' => 'https://www.mysoultab.com/assets/images/btn/per_call.png',
			// 	'settings'=>'1',
			// 	'type' =>'1',
			// 	'tab_name'=>'personal',
			// 	'isSocialList'=>'1',
			// 	'created'=>$created,
			// 	'updated'=>$updated
			// ),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Email',


				'btnicon' => base_url().'assets/images/btn/per_email.png',

				'settings'=>'1',
				'type' =>'2',
				'tab_name'=>'personal',
				'isSocialList'=>'1',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Yoga',
				'btnicon' => base_url().'assets/images/btn/per_weather.png',
				'settings'=>'1',
				'type' =>'3',
				'tab_name'=>'personal',
				'isSocialList'=>'0',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Social',
				'btnicon' => base_url().'assets/images/btn/social_icon.png',
				'settings'=>'1',
				'type' =>'4',
				'tab_name'=>'personal',
				'isSocialList'=>'1',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Transportation',
				'btnicon' => base_url().'assets/images/btn/transpotation.png',
				'settings'=>'1',
				'type' =>'5',
				'tab_name'=>'personal',
				'isSocialList'=>'1',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Sprituality',
				'btnicon' => base_url().'assets/images/btn/icon_spirituality.png',
				'settings'=>'1',
				'type' =>'6',
				'tab_name'=>'personal',
				'isSocialList'=>'0',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'DailyRoutine',
				'btnicon' => base_url().'assets/images/btn/daily_routine.png',
				'settings'=>'1',
				'type' =>'7',
				'tab_name'=>'personal',
				'isSocialList'=>'0',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Calendar',
				'btnicon' => base_url().'assets/images/btn/calender.png',
				'settings'=>'1',
				'type' =>'8',
				'tab_name'=>'personal',
				'isSocialList'=>'0',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Music',
				'btnicon' => base_url().'assets/images/btn/youtube.png',
				'settings'=>'1',
				'type' =>'9',
				'tab_name'=>'Entertainment',
				'isSocialList'=>'1',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'weather',
				'btnicon' => base_url().'assets/images/btn/per_weather.png',
				'settings'=>'1',
				'type' =>'10',
				'tab_name'=>'Entertainment',
				'isSocialList'=>'1',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Photo',
				'btnicon' => base_url().'assets/images/btn/per_photo.png',
				'settings'=>'1',
				'type' =>'11',
				'tab_name'=>'Entertainment',
				'isSocialList'=>'0',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Movies',
				'btnicon' => base_url().'assets/images/btn/youtube.png',
				'settings'=>'1',
				'type' =>'12',
				'tab_name'=>'Entertainment',
				'isSocialList'=>'1',
				'created'=>$created,
				'updated'=>$updated
			),
		
			array(
				'user_id' => $insertId ,
				'btnname' => 'Articles',
				'btnicon' => base_url().'assets/images/btn/articles_icon.png',
				'settings'=>'1',
				'type' =>'13',
				'tab_name'=>'Entertainment',
				'isSocialList'=>'0',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Game',
				'btnicon' => base_url().'assets/images/btn/game_icon.png',
				'settings'=>'1',
				'type' =>'14',
				'tab_name'=>'Entertainment',
				'isSocialList'=>'1',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Internet',
				'btnicon' => base_url().'assets/images/btn/Internet_icon.png',
				'settings'=>'1',
				'type' =>'15',
				'tab_name'=>'Entertainment',
				'isSocialList'=>'1',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'News',
				'btnicon' => base_url().'assets/images/btn/articles_icon.png',
				'settings'=>'1',
				'type' =>'16',
				'tab_name'=>'Entertainment',
				'isSocialList'=>'1',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Camera',
				'btnicon' => base_url().'assets/images/btn/camera.png',
				'settings'=>'1',
				'type' =>'17',
				'tab_name'=>'Entertainment',
				'isSocialList'=>'0',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Doctor Appointment',
				'btnicon' => base_url().'assets/images/btn/dr_appointment.png',
				'settings'=>'1',
				'type' =>'18',
				'tab_name'=>'Concierge',
				'isSocialList'=>'0',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Medicine Schedule',
				'btnicon' => base_url().'assets/images/btn/medicine_schedule.png',
				'settings'=>'1',
				'type' =>'19',
				'tab_name'=>'Concierge',
				'isSocialList'=>'0',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Test Report',
				'btnicon' => base_url().'assets/images/btn/dr_appointment.png',
				'settings'=>'1',
				'type' =>'20',
				'tab_name'=>'Concierge',
				'isSocialList'=>'0',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Note',
				'btnicon' => base_url().'assets/images/btn/articles_icon.png',
				'settings'=>'1',
				'type' =>'21',
				'tab_name'=>'Concierge',
				'isSocialList'=>'0',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Vital Signs',
				'btnicon' => base_url().'assets/images/btn/icon_vital.png',
				'settings'=>'1',
				'type' =>'22',
				'tab_name'=>'Concierge',
				'isSocialList'=>'0',
				'created'=>$created,
				'updated'=>$updated
			),
			array(
				'user_id' => $insertId ,
				'btnname' => 'Pharmacy Pickup',
				'btnicon' => base_url().'assets/images/btn/pharmacypickup.png',
				'settings'=>'1',
				'type' =>'23',
				'tab_name'=>'Concierge',
				'isSocialList'=>'1',
				'created'=>$created,
				'updated'=>$updated
			),
		);

		return  $setting_btn_arr;
	}
}
