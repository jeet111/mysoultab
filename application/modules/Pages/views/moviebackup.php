<aside class="right-side new-music mus_list">

	<div class="searchbar-wrap">

		<div class="src_sbmit">

			<input autocomplete="off" class="form-control" id="movie_search" name="movie_search" placeholder="Search Movie" value="<?php echo htmlspecialchars($this->uri->segment('2')); ?>" type="text">

			<button type="submit" id="btn_music" value="Submit"><i class="fa fa-search"></i></button>				

		</div>

		<a href=" <?php echo base_url();?>add_movie" class="add_music_btn" ><i class="fa fa-film" aria-hidden="true"></i>Add Movie</a>

	</div>

	<section class="content-header no-margin">



	</section>


	<section class="content weather-bg news_section">
		<!-- setting start -->
		<div class="col-md-12">

			<div id="msg_show"></div>

			<div class="col-md-10">
				<h3>Movies</h3>

				<?php $current_user_id = $_SESSION['logged_in']['id'];
         foreach($btnsetingArray as $res){
          $userid = $res->user_id;
          if($current_user_id == $userid){
            if($res->settings == 1) { ?>
						<div class="tooltip-2"><a data-btn="<?php echo 'movies'?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
							<span class="tooltiptext">Active</span>
						</div>
					<?php }else { ?>
						<div class="tooltip-2 "><a data-btn="<?php echo 'movies'?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
							<span class="tooltiptext">Deactive</span>
						</div>
					<?php } ?>
				<?php } 
				}?>
			</div>
		</section>

    <?php /* ?>
	<section class="content weather-bg news_section">
		
		<!-- setting start -->
		<div class="col-md-12">
			<div class="col-md-10">
				<h3>Movies</h3>
				<?php if(!empty($btnsetingArray)){ ?>
					<?php if($btnsetingArray[0]->settings=='1'){ ?>

						<label><input type="radio" name="btnshow" value='1' checked="checked">Now Movies showing on the mysoultab APP<br></label>

					<?php }else{ ?>

						<label> <input type="radio" name="btnshow" value='1' checked="checked">Now Movies not showing on the mysoultab APP </label>

					<?php } ?>

				<?php } ?> 
			</div>
			<div class="col-md-2 delete-email">
				<button class="btn btn-primary chk">Settings</button>
			</div> 
			<div id="para" style="display: none "> 



				<form id="submit_form" method="post" action="<?php echo base_url()?>savsettings "> 
					<div class="col-md-10">
						<label> <input type="checkbox" name="butn_seting" id="butn_seting" value="">Please check if want to show the Movies button in mysoultab App </label>
						<input type="hidden" name="btn" value="movies">
					</div>
					<div class="col-md-2">
						<input class="btn btn-primary chkdd" type="submit" name="submit" value="submit" >
					</div>
				</form>
				<div id="response"></div>
			</div>

			<!-- </div> -->

			<!-- End -->
		</section>

		<?php */ ?>
		<section class="doc_cate">

			<div class="row">

				<div class="col-md-9">

					<div class="bx_leftScrollmain mCustomScrollbar">

						<div class="bx_leftScroll">

							<div id="bx_slid">

								<div class="main_sld">

									<div class="sdl">

										<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

											<!-- <ol class="carousel-indicators"> -->

												<?php 

						  	//$j=0;

						  	//foreach ($music_banner as $banner) { ?>

						  		<!--  <li data-target="#carousel-example-generic" data-slide-to="<?php //echo $j; ?>" class="active"></li> -->

						  		<?php //$j++;}?>

						  		<!-- </ol>						  -->

						  		<?php //print_r($movie_banner); echo "<pre>";  ?>

						  		<div class="carousel-inner" role="listbox">

						  			<?php 



						  			$i=0;

						  			foreach ($movie_banner as $banner) { ?>



						  				<?php if($i==0){ ?>

						  					<div class="item active">

						  						<img style="height:100%;" src="<?php echo base_url().'uploads/movie/banners/'.$banner->movie_banner_image;?>">						     

						  					</div>

						  				<?php }else{ ?>



						  					<div class="item ">

						  						<img style="height:100%;" src="<?php echo base_url().'uploads/movie/banners/'.$banner->movie_banner_image;?>">						     

						  					</div>



						  				<?php } ?>



						  				<?php $i++;} ?>



						  			</div>				   		

						  			<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">

						  				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>

						  				<span class="sr-only">Previous</span>

						  			</a>

						  			<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">

						  				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>

						  				<span class="sr-only">Next</span>

						  			</a>     

						  		</div>

						  	</div>

						  </div>

						</div>

						<div class="widget-header">

							<h2>Populars</h2>

							<div class="wid-header-rhs">

								<!-- <a href=""> View all </a> -->

								<?php if(count($popular_count_array) > 10){ ?>

									<a href="<?php echo base_url();?>view_allmovies/popular"> View all </a>

								<?php } ?>

							</div>

						</div>

						<div class="demo">

							<div class="item">                        

							</div>

							<div class="item">

								<ul id="content-slider" class="content-slider">

									<?php //print_r($popular_array); echo "<pre>";die; ?>

									<?php foreach ($popular_array as $popular) { ?>

										<li>

											<div class="rail-thumb">



												<a class="dropdown-item" href="#" onclick="getId('<?php echo $popular->movie_id; ?>')">

													<img src="<?php echo base_url().'uploads/movie/image/'.$popular->movie_image; ?>">

													<span class="playbutton">

														<i class="fa fa-play-circle"></i>

													</span>			    			

												</a>

											</div>

											<div class="rail-content" title="">

												<a href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($popular->movie_id); ?>"><?php echo $popular->movie_title;  ?> </a>

												<div class="btn-group new-music-btn">

													<a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>

													<div class="dropdown-menu">	

														<a class="dropdown-item" href="#" onclick="getId('<?php echo $popular->movie_id; ?>')">Play Now</a>



														<a class="dropdown-item download_movie_<?php echo $popular->movie_id; ?>" href="<?php echo base_url(); ?>Pages/Movie/download_movie/<?php echo $popular->movie_id; ?>" data-id="<?php echo $popular->movie_id; ?>">Download</a>

														<!-- <a class="dropdown-item" href="#">Download</a> -->

														<!-- <a class="dropdown-item" href="#">Edit</a> -->

														<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($popular->movie_id); ?>">Get info</a>

													</div>

												</div>			    			

											</div>

										</li>



									<?php } ?>



								</ul>

							</div>



						</div>





						<div class="widget-header">

							<h2>Recents</h2>

							<div class="wid-header-rhs">

								<!-- <a href="#"> View all </a> -->

								<?php if(count($popular_count_array) > 10){ ?>

									<a href="<?php echo base_url();?>view_allmovies/recent"> View all </a>

								<?php } ?>

							</div>

						</div>



						<ul id="content-slider" class="content-slider">

							<?php foreach ($recent_array as $recent) { ?>



								<li>

									<div class="rail-thumb">

										<a class="dropdown-item mobi_dip" data-mobi="<?php echo base64_encode($recent->movie_id); ?>" href="javascript:void(0);" onclick="getId('<?php echo $recent->movie_id; ?>')">

											<img src="<?php echo base_url().'uploads/movie/image/'.$recent->movie_image; ?>" class="mobi_dip" data-mobi="<?php echo base64_encode($recent->movie_id); ?>">

											<span class="playbutton">

												<i class="fa fa-play-circle" class="mobi_dip" data-mobi="<?php echo base64_encode($recent->movie_id); ?>"></i>

											</span>			    			

										</a>

									</div>

									<div class="rail-content" title="">

										<a href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($recent->movie_id); ?>"> <?php echo $recent->movie_title;  ?> </a>

										<div class="btn-group new-music-btn">

											<a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>

											<div class="dropdown-menu">



												<a class="dropdown-item" href="#" onclick="getId('<?php echo $recent->movie_id; ?>')">Play Now</a>

												<a class="dropdown-item download_movie_<?php echo $recent->movie_id; ?>"  href="<?php echo base_url(); ?>Pages/Movie/download_movie/<?php echo $recent->movie_id; ?>" data-id="<?php echo $recent->movie_id; ?>">Download</a>

												<!-- <a class="dropdown-item" href="#">Download</a> -->

												<!-- <a class="dropdown-item" href="#">Edit</a> -->

												<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($recent->movie_id); ?>">Get info</a>

											</div>

										</div>			    			

									</div>



								</li>

							<?php } ?>



						</ul>



						<div class="widget-header">

							<h2>Categories</h2>

							<div class="wid-header-rhs">

								<!-- <a href=""> View all </a> -->

								<?php if(count($category_count_array) > 10){ ?>

									<a href="<?php echo base_url();?>view_all_movies_categories"> View all </a>

								<?php } ?>

								<!-- <a href="<?php //echo base_url();?>view_allmovies/categories"> View all </a> -->

							</div>  

						</div>





						<ul id="content-slider" class="content-slider">



							<?php foreach ($category_array as $category) { ?>

								<li>

									<div class="rail-thumb">



										<a href="<?php echo base_url(); ?>view_all_movies_category/<?php echo base64_encode($category->movie_category_id); ?>">

											<img src="<?php echo base_url().'uploads/movie/category_icon/'.$category->movie_category_icon; ?>">		    

										</a>  			

									</div>

									<div class="rail-content" title="">

										<!-- <a href="#">  -->

											<a href="<?php echo base_url(); ?>view_all_movies_category/<?php echo base64_encode($category->movie_category_id); ?>">

												<?php echo $category->movie_category_name;  ?> 

											</a>



										</div>



									</li>                      



								<?php } ?>



							</ul>





							<?php if(!empty($mymovie)){ ?>



								<div class="widget-header">

									<h2>My Movie</h2>

									<div class="wid-header-rhs">

										<!-- <a href="#"> View all </a> -->

										<?php if(count($mymovie_count_array) > 10){ ?>

											<a href="<?php echo base_url();?>view_allmovies/my"> View all </a>

										<?php } ?>

									</div>

								</div>

								<ul id="content-slider" class="content-slider">

									<?php foreach ($mymovie as $movie) { ?>                                

										<li class="card_one">

											<div class="rail-thumb">

												<a class="dropdown-item" href="#" onclick="getId('<?php echo $movie->movie_id; ?>')">

													<img src="<?php echo base_url().'uploads/movie/image/'.$movie->movie_image; ?>">



													<span class="playbutton">

														<i class="fa fa-play-circle"></i>

													</span>	

												</a>		    			

											</div>

											<div class="rail-content" title="">

												<a href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($movie->movie_id); ?>"> <?php echo $movie->movie_title;  ?> </a>

												<div class="btn-group new-music-btn">

													<a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>

													<div class="dropdown-menu music_dropdown">



														<a class="dropdown-item" href="#" onclick="getId('<?php echo $movie->movie_id; ?>')">Play Now</a>

														<!-- <a class="dropdown-item" href="#">Download</a> -->

														<a class="dropdown-item download_movie_<?php echo $movie->movie_id; ?>" href="<?php echo base_url(); ?>Pages/Movie/download_movie/<?php echo $movie->movie_id; ?>" data-id="<?php echo $movie->movie_id; ?>">Download</a>

														<a class="dropdown-item" href="<?php echo base_url();?>edit_movie/<?php echo $movie->movie_id; ?>">Edit</a>

														<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($movie->movie_id); ?>">Get info</a>

														<a class="dropdown-item" data-id="<?php echo $movie->movie_id; ?>" id="deletemoviecat_<?php echo $movie->movie_id; ?>" href="javascript:void(0);">Delete</a>

													</div>

												</div>			    			

											</div>

										</li>



									<?php } ?>

								</ul>

							<?php } ?>







							<?php if(!empty($UserFavMovie)){ ?>

								<div class="widget-header">

									<h2>Favorite Movies </h2>

									<div class="wid-header-rhs">

										<!-- <a href="#"> View all </a> -->

										<?php if(count($favorite_count_array) > 10){ ?>

											<a href="<?php echo base_url();?>view_allmovies/favorite"> View all </a>

										<?php } ?>

									</div>

								</div>			    

								<ul id="content-slider" class="content-slider">

									<?php foreach ($UserFavMovie as $movie) {  ?>                                

										<li>

											<div class="rail-thumb">

												<a class="dropdown-item" href="#" onclick="getId('<?php echo $movie->movie_id; ?>')">

													<img src="<?php echo base_url().'uploads/movie/image/'.$movie->movie_image; ?>">



													<span class="playbutton">

														<i class="fa fa-play-circle"></i>

													</span>

												</a>		    			

											</div>

											<div class="rail-content" title="">

												<a href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($movie->movie_id); ?>"> <?php echo $movie->movie_title;  ?> </a>

												<div class="btn-group new-music-btn">

													<a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>

													<div class="dropdown-menu">



														<a class="dropdown-item" href="#" onclick="getId('<?php echo $movie->movie_id; ?>')">Play Now</a>

														<a class="dropdown-item download_movie_<?php echo $movie->movie_id; ?>" href="<?php echo base_url(); ?>Pages/Movie/download_movie/<?php echo $movie->movie_id; ?>" data-id="<?php echo $movie->movie_id; ?>" >Download</a>

														<!-- <a class="dropdown-item" href="#">Download</a> -->

														<!-- <a class="dropdown-item" href="#">Edit</a> -->

														<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($movie->movie_id); ?>">Get info</a>

													</div>

												</div>			    			

											</div>



										</li>



									<?php } ?>

								</ul>





							<?php } ?>







						</div>

					</div>

				</div>



				<?php //echo "<pre>"; print_r($PlayMovie); echo "</pre>"; ?>  



				<div class="col-md-3 mrg_non">

					<div class="bx_right bx_movieList">					

						<div class="listing_sng mCustomScrollbar" id="test"> 

							<div class="bx_mus"  id="test">	

								<?php if(!empty($PlayMovie)){ ?>

									<video  controls autoplay style="width:100%; height:auto;" >

										<source id="moivedata" data-myval src="<?php echo base_url().'uploads/movie/'.$PlayMovie[0]->playmovie_file; ?>" type="video/mp4">

											Sorry, your browser doesn't support the video element.

										</video>					 

									<?php } ?>  

								</div>

								<div class="cont-wrap">

									<ul class="getinfolist" >

										<li>

											<p>Name:</p>

											<p><?php if(!empty($PlayMovie[0]->playmovie_title)){ echo $PlayMovie[0]->playmovie_title; }else{ echo "N.A."; }?></p>

										</li>								

										<li>

											<p>Artist:</p>

											<p>

												<?php if(!empty($PlayMovie[0]->playmovie_artist)){ echo $PlayMovie[0]->playmovie_artist; }else{echo "N.A.";} ?>			

											</p>

										</li>

								<!-- <li>

									<p>Album:</p>

									<p><a href="#">Test Songs 68</a></p>

								</li> -->

								<li>

									<p>Description:</p>

									<p>

										<?php if(!empty($PlayMovie[0]->playmovie_desc)){ echo $PlayMovie[0]->playmovie_desc; }else{ echo "N.A.";} ?>							

									</p>

								</li>								

							</ul>

						</div>						

					</div>

				</div>

			</div>

		</div>

	</section>

</aside>



<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->

<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->



<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightslider.css"/>

<script src="<?php echo base_url(); ?>assets/js/lightslider.js"></script> 

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.css">

<script src="<?php echo base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>



<script type="text/javascript">

	$(document).on('click','[id^="deletemoviecat_"]',function(){ 

		var delete_movie = $(this).attr('data-id');

		if(confirm("Are you sure you want to delete movie?")){	

			$.ajax({

				type: "POST", 

				url: '<?php echo base_url(); ?>Pages/Movie/delete_movie/',

				data: 'delete_movie='+delete_movie,

            //dataType: "json",

            success: function(data)

            {	

			//console.log(data);	

			setTimeout(function(){

            //$("#deletemsg").html("<div class='alert alert-success'>Deleted Successfully</div>");



            // alert('Deleted Successfully');

            // location.reload();



        }, 1000);

		}

	});

			$(this).parents(".card_one").animate({ backgroundColor: "#fbc7c7" }, "fast")

			.animate({ opacity: "hide" }, "slow");

		}

	});

</script>







<script>

	$(document).ready(function() {

		$(".content-slider").lightSlider({

			loop:true,

			keyPress:true,

			responsive: [

			{

				breakpoint:1200,

				settings: {

					item:4,

				}                   

			},

			{

				breakpoint:1100,

				settings: {

					item:3,

				}

			},

			{

				breakpoint:520,

				settings: {

					item:2,

				}

			},

			{

				breakpoint:360,

				settings: {

					item:1,

				}

			}

			],

		});

		$('#image-gallery').lightSlider({

			gallery:true,

			item:1,

			thumbItem:9,

			slideMargin: 0,    

			speed:500,

			auto:true,

			loop:true,

			onSliderLoad: function() {

				$('#image-gallery').removeClass('cS-hidden');

			}  

		});

	});

</script>



<script>

	(function($){

		$(window).on("load",function(){



			$("a[rel='load-content']").click(function(e){

				e.preventDefault();

				var url=$(this).attr("href");

				$.get(url,function(data){

						$(".content .mCSB_container").append(data); //load new content inside .mCSB_container

						//scroll-to appended content 

						$(".content").mCustomScrollbar("scrollTo","h2:last");

					});

			});



			$(".content").delegate("a[href='top']","click",function(e){

				e.preventDefault();

				$(".content").mCustomScrollbar("scrollTo",$(this).attr("href"));

			});

		});

	})(jQuery);

</script>



<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).on('click','.updateStatus',function(){
		var update_status = $(this).attr('data-btn');
		var update_st = $(this).attr('data-st');
    //alert(update_status+'-'+update_st);
    $.ajax({
    	url: '<?php echo base_url();?>Pages/pages/change_button_status',
    	type: "POST",
    	data: {'btn':update_status},
    	dataType: "json",
    	success:function(data){
    		setTimeout(function(){
    			$("#msg_show").html("<div class='alert alert-success'>"+data.status+"</div>");
    			location.reload();

    		}, 1500);
    	}
    }); 
});
</script>

<script type="text/javascript">

	function getId(movie_id) {

		$.ajax({

			type: "POST", 

			url: '<?php echo base_url(); ?>Pages/Movie/Paythismovie/',

			data: {'movie_id': movie_id},

            //dataType: "json",







            success: function(data)

            {	





            	$("#test").html(data);

            }

        });







	}



	$(document).on('click','[class^="download_movie_"]',function(){ 

		var movie_id = $(this).attr("data-id");

		window.location = "<?php echo base_url(); ?>Pages/Movie/download_movie/"+movie_id;



	});



	$(document).on('click','[class^="mobi_dip"]',function(){ 

		var movie_id = $(this).attr("data-mobi");

		window.location = "<?php echo base_url(); ?>getinfo_movie/"+movie_id;



	});



	



	$(document).on('click','#btn_music',function(e){ 

		var movie_search = $("#movie_search").val();



		window.location = "<?php echo base_url(); ?>movie_list/"+movie_search;	

	}); 

</script>