<div class="container-fluid">
<div class="dash-counter users-main my-sts-table user-list">
<div class="">
<div class="col-md-12">
	<div class="">
		<div class="card-body">
			<div class="my_info_one">
				<div class="Schedule_main_one">
					<div class=" users-main">
						<h2>View Note</h2>
						<div class="btn_topBack">
							<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">
								Back
							</a>
						</div>
						<div class="row">
							<?php //echo "<pre>"; print_r($singledata); echo "</pre>"; ?>
							<div class="col-md-12">
								<div class="general-info">
									<div class="card-header">
										<h5 class="card-header-text">About Note</h5>
									</div>
									<div class="rrow">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-6 col-sm-6">
													<div class="row">
														<div class="col-md-5 col-sm-5">
															<div class="f_name"><b>Username:</b></div>
														</div>
														<div class="col-md-7 col-sm-7">
															<div class="f_nameOne">
																<?php
																echo $singledata[0]->username;?></div>
															</div>
														</div>
													</div>
													<div class="col-md-6 col-sm-6">
														<div class="row">
															<div class="col-md-5 col-sm-5">
																<div class="f_name"><b>From caregiver:</b></div>
															</div>
															<div class="col-md-7 col-sm-7">
																<div class="f_nameOne"><?php
																foreach ($care_giver as $value){
																	if($singledata[0]->from_caregiver==$value->id){
																		echo $value->username;
																	}
																}
																?>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-sm-6">
													<div class="row">
														<div class="col-md-5 col-sm-5">
															<div class="f_name"><b>To caregiver:</b></div>
														</div>
														<div class="col-md-7 col-sm-7">
															<div class="f_nameOne"><?php
															foreach ($care_giver as $value){
																if($singledata[0]->to_caregiver==$value->id){
																	echo $value->username;
																}
															}
															?>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="row">
													<div class="col-md-5 col-sm-5">
														<div class="f_name"><b>Created Date:</b></div>
													</div>
													<div class="col-md-7 col-sm-7">
														<div class="f_nameOne">
															<?php
															echo date('d-m-Y',strtotime($singledata[0]->create_date));
															?>
														</div>
													</div>
												</div>
											</div>

										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12">
												<div class="row">
													<div class="col-md-2 col-sm-2">
														<div class="f_name"><b>Description:</b></div>
													</div>
													<div class="col-md-10 col-sm-10">
														<div class="f_nameOne decri">
															<?php echo $singledata[0]->description;?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php $admin_url = $this->uri->segment(2);
							$url = 'admin/email_list/list';
							if ($admin_url == 'adminprofile') {
								$url = 'admin/dashboard';
							}?>
			<!-- <a class="cancel-btn btn" href="javascript:window.history.back()">
					Back
				</a> -->
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<style type="text/css">
ul.abs {
list-style: none;
margin: 5px 0px 0px;
padding: 0px;
}
ul.abs li {
display: inline-block;
color: #ffdd2a;
font-size: 16px;
}
</style>