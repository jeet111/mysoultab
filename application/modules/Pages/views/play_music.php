<div class="show_check">				
					<div class="queue-header">
						<div class="hd_que">
							<h5 class="pull-left">Queue</h5>
						</div>
						<div class="main_chk">
							<div class="bx_chkAll">
								<span class="select-input">
									<input class="chk_bx" id="checkall" type="checkbox">
									<span> Select All</span>
								</span>
							</div>
							<div class="bx_rgt">
								
								<a href="javascript:void(0);" class="clear-queue-popover btn_delete" data-id="" id="btn_delete"> Remove</a>
							</div>
						</div>
					</div>


					<div class="listing_sng mCustomScrollbar">
						<ul class="list_songs playlist1" id="playlist">
							<?php
                         if(!empty($playlist_music)){
							foreach($playlist_music as $playlist){ ?>
							<li class="active test_play">
								<div class="select-input_one"><input type="checkbox" name="sub_chk[]" value="<?php echo $playlist->music_playlist_id; ?>" data-id="<?php echo $playlist->music_playlist_id; ?>" class="sub_chk" ></div>
								<a href="<?php echo base_url(); ?>uploads/music/<?php echo $playlist->music_file; ?>"><div class="queue-songlist-lhs">
									<div class="queue-songlist-img">
										<div class="song-current">
											<img alt="" src="<?php echo base_url(); ?>uploads/music/image/<?php echo $playlist->music_image; ?>">
										</div>
									</div>
									<div class="queue-songlist-info">
										<p class="click_image_change_<?php echo $playlist->music_id; ?>" data-image-change="<?php echo $playlist->music_id; ?>"><?php echo $playlist->music_title; ?></p>
										<span><?php echo $playlist->music_artist; ?></span>
									</div>
								</div></a>
								<div class="queue-songlist-rhs queue-play-list">
									<div class="btn-group new-music-btn">
										<button class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
										<div class="dropdown-menu">
											
											<button class="dropdown-item" href="javascript:void(0);" id="remove_play_now_<?php echo $playlist->music_playlist_id; ?>" data-music-remove="<?php echo $playlist->music_playlist_id; ?>">Remove</button>
											
											<button class="dropdown-item download_music_<?php echo $playlist->music_id; ?>" href="<?php echo base_url(); ?>Pages/Music/download_music/<?php echo $playlist->music_id; ?>" id="testi_<?php echo $playlist->music_id; ?>" data-music-id="<?php echo $playlist->music_id; ?>" >Download</button>
											
											<button class="dropdown-item" id="getinfo_m_<?php echo $playlist->music_id ?>" data-getinfo="<?php echo base64_encode($playlist->music_id); ?>" >Get info</button>
										</div>
									</div>
								</div>
							</li>
							
						<?php } ?>

						 <?php }else{ ?>	
						<div class="music_norecord">No record found</div> 
						 <?php } ?>
						</ul>
					</div>
</div>

					<div class="bx_mus">
						<div class="ply_sng">
							<div class="sng-crnt">
								<?php if($play_music->music_image){ ?>
								<img alt="" src="<?php echo base_url(); ?>uploads/music/image/<?php echo $play_music->music_image; ?>">
							<?php }else{ ?>
							<img src="<?php echo base_url().'uploads/music/defualt_music.png' ?>">
							<?php } ?>
							</div>
							<div class="sng-txt">
								<h4><?php echo $play_music->music_title; ?></h4>
								<p><?php echo $play_music->music_artist; ?></p>
							</div>
						</div>
						
						 <audio id="audio" preload="auto" tabindex="0" controls="" type="audio/mpeg">
        <source type="audio/mp3" id="audiotest" src="<?php echo base_url(); ?>uploads/music/<?php echo $play_music->music_file; ?>">
        Sorry, your browser does not support HTML5 audio.
    </audio>
						
					</div>
				
				</div>
<script>
var audio;
var playlist;
var tracks;
var current;

init();
function init(){
    current = 0;
    audio = $('audio');
    playlist = $('.playlist1');
    tracks = playlist.find('li a');
    len = tracks.length - 1;
    audio[0].volume = .10;
    audio[0].play();
    playlist.find('a').click(function(e){ 
        e.preventDefault();
        link = $(this);
        current = link.parent().index();
        run(link, audio[0]);
    });
    audio[0].addEventListener('ended',function(e){
       
        if(current == len){
            current = 0;
            link = playlist.find('a')[0];
        }else{
			 current++;
            link = playlist.find('a')[current];    
        }
        run($(link),audio[0]);
    });
}

function run(link, player){
        player.src = link.attr('href');
		
        par = link.parent();
        par.addClass('active').siblings().removeClass('active');
        audio[0].load();
        audio[0].play();
}

</script>