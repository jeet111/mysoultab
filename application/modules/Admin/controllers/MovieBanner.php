  <?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  class MovieBanner extends MX_Controller {
    private $staff_permission = array();
    public function __construct(){   
      parent:: __construct();
      $this->load->library('session');
      $this->load->library('form_validation');
      $this->load->model('movieBanner_model');
	    $this->load->model('Common_model');
      $this->load->model('login_model');
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));
      $user_id = $this->session->userdata('logged_ins')['id'];
      if(empty($user_id)){
        redirect('admin/login');
      }

      if ($this->session->userdata('logged_ins')['user_role'] == 5) {
        $uid = $this->session->userdata('logged_ins')['id'];
        $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
        if (!empty($permission_list)) 
        {
           foreach ($permission_list as $k => $v) 
           {
              $this->staff_permission[$v['permission_id']] = $v;
           }
        }
      }
    }
    
    /*show all banners*/
    public function moviebanner_list()
    {
       $data['menuactive'] = $this->uri->segment(2);
       $data['MovieBannerData'] = $this->movieBanner_model->getAllMoviebanner();
       $this->template->set('title', 'Movie Banner');
       $data['staff_permission'] = $this->staff_permission;
       $this->template->load('admin_dashboard_layout', 'contents' , 'movie_banner_list', $data);
    }

   /*Delete banner*/
   public function delete_moviebanner($moviebanner_id='')
   {

    $MovieBannerdata=$this->movieBanner_model->getsingle('cp_movie_banner',array('movie_banner_id'=>$moviebanner_id));

    

    $imageFilePath=getcwd().'/uploads/movie/banners/';
    $imageFile=$MovieBannerdata->movie_banner_image;
    unlink($imageFilePath.$imageFile);

    $vedioFilePath= $imageFilePath=getcwd().'/uploads/movie/banners/files/';
    $vedioFile=$MovieBannerdata->movie_banner_file;
    unlink($vedioFilePath.$vedioFile);

    


    $this->movieBanner_model->delete('cp_movie_banner',array('movie_banner_id' =>$moviebanner_id));

    $this->session->set_flashdata('success', 'Deleted successfully.');
    redirect('admin/movie_banner_list');
  }

  /*Add banner*/
  public function add_moviebanner()
  {
     $data['menuactive'] = $this->uri->segment(2);
    $this->form_validation->set_rules('banner_title','Banner Title','required');
    if ($this->form_validation->run() == TRUE) 
     {  
    if($_POST['submit']){

      /*for audio file*/
       $random = $this->generateRandomString(10);
       $ext = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
       $file_name = $random.".".$ext;
       $dirpath = './././uploads/movie/banners/files/'.$file_name;
       if(move_uploaded_file($_FILES['picture']['tmp_name'], $dirpath)){
             $data['new_pic'] = $file_name;
        }
        if(!empty($data['new_pic'])){
          $picture =$data['new_pic'];
        }else{
          $picture ='';
        }


        /*for image file*/
       $random1 = $this->generateRandomString(10);
       $ext = pathinfo($_FILES['pictureFile']['name'], PATHINFO_EXTENSION);
       $file_name1 = $random1.".".$ext;
       $dirpath = './././uploads/movie/banners/'.$file_name1;
       if(move_uploaded_file($_FILES['pictureFile']['tmp_name'], $dirpath)){
             $data['banner'] = $file_name1;
        }
        if(!empty($data['banner'])){
          $pictureFile =$data['banner'];
        }else{
          $pictureFile ='';
        }


      $data = array(
        'movie_banner_text' => $this->input->post('banner_title'),
        'movie_banner_image' => $pictureFile,
        'movie_banner_file'=>$picture,
        'movie_banner_created' =>date('Y-m-d H:i:s'),
        'movie_banner_status'=>1
      );
      $this->movieBanner_model->insertData('cp_movie_banner',$data);
      $this->session->set_flashdata('success', 'Banner added successfully.');
      redirect('admin/movie_banner_list');
     }
   }
    $this->template->set('title', 'Movie Banner');
    $data['staff_permission'] = $this->staff_permission;
    $this->template->load('admin_dashboard_layout', 'contents' , 'add_movie_banner', $data);
  }
  
  public function generateRandomString($length = 12) {
   $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
   $charactersLength = strlen($characters);
   $randomString = '';
   for ($i = 0; $i < $length; $i++) {
       $randomString .= $characters[rand(0, $charactersLength - 1)];
   }
   return $randomString;
   }

  /*Edit banner*/
  public function edit_moviebanner($banner_id='')
  {
     $data['menuactive'] = $this->uri->segment(2);
    $this->form_validation->set_rules('banner_title','Banner Title','required');
  if ($this->form_validation->run() == TRUE) 
     {  
   if($_POST['submit']){

         /*for audio file*/
       $random = $this->generateRandomString(10);
       $ext = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
       $file_name = $random.".".$ext;
       $dirpath = './././uploads/movie/banners/files/'.$file_name;
       if(move_uploaded_file($_FILES['picture']['tmp_name'], $dirpath)){
             $data['new_pic'] = $file_name;
        }
        if(!empty($data['new_pic'])){
          $picture =$data['new_pic'];
        }else{
          $picture ='';
        }


        /*for image file*/
       $random1 = $this->generateRandomString(10);
       $ext = pathinfo($_FILES['pictureFile']['name'], PATHINFO_EXTENSION);
       $file_name1 = $random1.".".$ext;
       $dirpath = './././uploads/movie/banners/'.$file_name1;
       if(move_uploaded_file($_FILES['pictureFile']['tmp_name'], $dirpath)){
             $data['banner'] = $file_name1;
        }
        if(!empty($data['banner'])){
          $pictureFile =$data['banner'];
        }else{
          $pictureFile ='';
        }



      if(!empty($pictureFile)){
      $data = array(
        'movie_banner_text' => $this->input->post('banner_title'),
        'movie_banner_image' => $pictureFile,
        'movie_banner_created' =>date('Y-m-d H:i:s'),
      );
      $this->movieBanner_model->updateData('cp_movie_banner',$data,array('movie_banner_id'=>$banner_id));
      $this->session->set_flashdata('success', 'Movie updated successfully.');
      redirect('admin/movie_banner_list');

      }else if(!empty($picture)){

      $data = array(
        'movie_banner_text' => $this->input->post('banner_title'),
        //'music_banner_image' => $pictureFile,
        'movie_banner_file' => $picture,
        'movie_banner_created' =>date('Y-m-d H:i:s'),
      );
      $this->movieBanner_model->updateData('cp_movie_banner',$data,array('movie_banner_id'=>$banner_id));
      $this->session->set_flashdata('success', 'Movie updated successfully.');
      redirect('admin/movie_banner_list');
    


     }else if(empty($pictureFile)|| $pictureFile=='' && empty($picture)|| $picture==''){

      

      $data = array(
        'movie_banner_text' => $this->input->post('banner_title'),
        'movie_banner_created' =>date('Y-m-d H:i:s'),
      );
      $this->movieBanner_model->updateData('cp_movie_banner',$data,array('movie_banner_id'=>$banner_id));
      $this->session->set_flashdata('success', 'Banner updated successfully.');
      redirect('admin/movie_banner_list');
     }
    }
   }
    $data['editBanner']=$this->movieBanner_model->getsingle('cp_movie_banner',array('movie_banner_id'=>$banner_id));
    $this->template->set('title', 'Movie Banner');
    $data['staff_permission'] = $this->staff_permission;
    $this->template->load('admin_dashboard_layout','contents','edit_movie_banner',$data);
  }

  /*view single banner detail*/
  public function view_moviebanner($banner_id='')
    {
       $data['menuactive'] = $this->uri->segment(2);
     $data['viewMovieBanner']=$this->movieBanner_model->getsingle('cp_movie_banner',array('movie_banner_id'=>$banner_id));
     $this->template->set('title', 'Movie Banner');
     $data['staff_permission'] = $this->staff_permission;
     $this->template->load('admin_dashboard_layout', 'contents' , 'view_movie_banner', $data);
   }

   /*Update banner status*/
   public function updateBanner() {
    $banner_id = $this->input->post('banner_id');
    $con['conditions'] = array(
      'movie_banner_id'=> $banner_id
    );
    $data['users_data'] = $this->login_model->getRows('cp_movie_banner',$con);
    if($data['users_data'][0]['movie_banner_status']==1){
      $return = $this->login_model->updateRecords("cp_movie_banner", array('movie_banner_status' => 0), array('movie_banner_id'=> $banner_id));
	  echo '2';
    }else{
      $return = $this->login_model->updateRecords("cp_movie_banner", array('movie_banner_status' => 1), array('movie_banner_id'=> $banner_id));
	  echo '1';	
    }
    
  }
  
  public function ajax_banner_add()
  {
	 if ($_FILES["picture"]["error"] > 0 && $_FILES["pictureFile"]["error"] > 0)
  {
   $return = array('code' => 2,'message' => 'File loading error!');
  }else{ 

           $config1['upload_path'] = 'uploads/movie/banners/files/';
		   $config1['allowed_types'] = 'mp4|flv|wmv|webm|vob|ogg|mpeg|mpg|3gp';
           
           //$data['max_size'] = '5000';
            $config1['encrypt_name'] = true;

            $this->load->library('upload', $config1);

            if ($this->upload->do_upload('picture')) {  
                $attachment_data = array('upload_data' => $this->upload->data());
                $uploadfile = $attachment_data['upload_data']['file_name'];
			// $return = array('code' => 1,'message' => 'Successfully added');	
		 }else{ 
			$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
		 }
		 
		 
		 $config2['upload_path'] = 'uploads/movie/banners/';
         $config2['allowed_types'] = 'jpg|png|gif|jpeg';
           //$data['max_size'] = '5000';
         $config2['encrypt_name'] = true;

         $this->upload->initialize($config2);


          if ($this->upload->do_upload('pictureFile')) {
          $attachment_data_image = array('upload_data' => $this->upload->data());
           $picturefile1 = $attachment_data_image['upload_data']['file_name'];
		  
      //  $return = array('code' => 1,'message' => 'Successfully added');		   
		 }else{ 
			$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
			
		 }
		// echo $picturefile.'hii';die;
		
		 if($return['code'] != 2){  
		 $array = array(
	       'movie_banner_text' => $this->input->post('banner_title'),
           
           'movie_banner_file'=>$uploadfile,
		   'movie_banner_image'=>$picturefile1,
           'movie_banner_created' =>date('Y-m-d H:i:s'),
           'movie_banner_status'=>1
	   );
	   $this->Common_model->insertData("cp_movie_banner",$array);
				
$return = array('code' => 1,'message' => 'Successfully added.');
		 }
	}
	echo json_encode($return); 
  }
  
  public function image_upload()
	{
		$config1['upload_path'] = 'uploads/movie/banners/';
		   $config1['allowed_types'] = 'mp4|flv|wmv|webm|vob|ogg|mpeg|mpg|3gp';
           
           //$data['max_size'] = '5000';
            $config1['encrypt_name'] = true;

            $this->load->library('upload', $config1);


            if ($this->upload->do_upload('picture')) {
                $attachment_data = array('upload_data' => $this->upload->data());
                $uploadfile = $attachment_data['upload_data']['file_name'];
			$return = array("code" => 1);
		 }
		 echo json_encode($return);
	}
	
	public function ajax_banner_edit()
	{
		$banner_id = $this->input->post('banner_id');
		
		$singleData = $this->Common_model->getsingle("cp_movie_banner",array("movie_banner_id" => $banner_id));
		
		
  //print_r($_FILES["picture"]["name"]);
 // print_r($_FILES["pictureFile"]["name"]);die;
           $config1['upload_path'] = 'uploads/movie/banners/files/';
		   $config1['allowed_types'] = 'mp4|flv|wmv|webm|vob|ogg|mpeg|mpg|3gp';
           
           //$data['max_size'] = '5000';
            $config1['encrypt_name'] = true;

            $this->load->library('upload', $config1);

          if(!empty($_FILES["picture"]["name"])){ 
            if ($this->upload->do_upload('picture')) { 
                $attachment_data = array('upload_data' => $this->upload->data());
                $uploadfile = $attachment_data['upload_data']['file_name'];
			// $return = array('code' => 1,'message' => 'Successfully added');	
		 }else{ 
			$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
		 }
		  }
		 
		 $config2['upload_path'] = 'uploads/movie/banners/';
         $config2['allowed_types'] = 'jpg|png|gif|jpeg';
           //$data['max_size'] = '5000';
         $config2['encrypt_name'] = true;

         $this->upload->initialize($config2);

        if(!empty($_FILES["pictureFile"]["name"])){ 
          if ($this->upload->do_upload('pictureFile')) {
          $attachment_data_image = array('upload_data' => $this->upload->data());
           $picturefile1 = $attachment_data_image['upload_data']['file_name'];
      //  $return = array('code' => 1,'message' => 'Successfully added');		   
		 }else{ 
			$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
			
		 }
		}
		 
		 if(!empty($picturefile1)){
			 $picfile = $picturefile1;
		 }else{
			 $picfile = $singleData->movie_banner_image;
		 }
		 
		 if(!empty($uploadfile)){
			 $upfile = $uploadfile;
		 }else{
			 $upfile = $singleData->movie_banner_file;
		 }
		
		 if($return['code'] != 2){ 
		  $array = array(
	       'movie_banner_text' => $this->input->post('banner_title'),
           
           'movie_banner_file'=>$upfile,
		   'movie_banner_image'=>$picfile,
           'movie_banner_created' =>date('Y-m-d H:i:s'),
           'movie_banner_status'=>1
	   );
	   $this->Common_model->updateData("cp_movie_banner",$array,array('movie_banner_id' => $this->input->post("banner_id")));
				
$return = array('code' => 1,'message' => 'Successfully updated.');
		 }
	
	
	echo json_encode($return);
	}
	
  
  
  
  }