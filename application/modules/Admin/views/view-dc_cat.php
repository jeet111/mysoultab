<div class="container-fluid">
	<div class="dash-counter users-main my-sts-table user-list">
		<div class="">
			<div class="col-md-12">
				<div class="">
					<div class="card-body">
						<div class="my_info_one">
							<div class="Schedule_main_one">
								<div class=" users-main">
									<h2>View Doctor Category</h2>
									<div class="row">
										<div class="col-md-3">
											<div class="card faq-left">
												<div class="social-profile">
													<?php if (!empty($dc_categories_data['dc_icon'])){ ?>
														<img class="img-fluid" src="<?php echo base_url().'uploads/doctor_category/'.$dc_categories_data['dc_icon'];?>" alt="" style="background: #333;">
													<?php } else { ?>
														<img class="img-fluid" src="<?php echo base_url().'uploads/profile_images/user.png' ?>">
													<?php } 
													?>

												</div>
												<div class="card-block">
													<h4><?php echo $dc_categories_data['firstname'];?></h4>
												</div>
											</div>
										</div>
										<div class="col-md-9">
											<div class="general-info">
												<div class="card-header">
													<h5 class="card-header-text">About Category</h5>
													<p><?php echo $dc_categories_data['dc_name'];?></p>
												</div>
												<div class="rrow">
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-5 col-sm-5">
																<div class="f_name">Category Name:</div>
															</div>
															<div class="col-md-7 col-sm-7">
																<div class="f_nameOne"><?php echo $dc_categories_data['dc_name'];?></div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<?php $admin_url = $this->uri->segment(2);
											$url = 'admin/email_list/list';
											if ($admin_url == 'adminprofile') {
												$url = 'admin/dashboard';
											}?>
											<a class="cancel-btn btn" href="<?php echo $url; ?>">
												Back
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>