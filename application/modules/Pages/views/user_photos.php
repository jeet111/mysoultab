<!-- Right side column. Contains the navbar and content of the page -->
<style>
 .loading {
  background: transparent url('http://thinkfuture.com/wp-content/uploads/2013/10/loading_spinner.gif') center no-repeat;
}
button[disabled], html input[disabled] {
    cursor: pointer !important;
}
.sc_photo_list_item:hover .sc_photo_list_item_photo{
  height: 200px !important;
    width: 100%;position:relative !important;transition:none !important;
}
.sc_photo_list_item_photo {
    height: 200px;
    width: 100%;
    background-size: cover;
    background-repeat: no-repeat;
}
.row.tz-gallery img{
  height:100%;width:100%;
}
.banpic img {
    height: 100%;
    width: 100%;
}
.buttondiv.clearfix.text-center {
    padding: 20px 0;
    display: inline-block;
}
.rem_add {
    float: left;
    width: 100%;
    margin: 12px 0 auto;
    text-align: center;
}
div#video_loader {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: #0a0a0aa6;
  z-index: 999999;
  text-align: center;
  padding-top: 23%;
}
.row.tz-gallery  div {
    margin-bottom: 12px;
}
div#video_loader img#loading_img {
  width: 70px !important;
}
.text_heading{
  border:none;
}
</style>
<script type="text/javascript" src=" <?php echo base_url(); ?>assets/js/jquery-latest.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/user_dashboard/css/jquery.fancybox.css" media="screen"/>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/user_dashboard/js/jquery.fancybox.pack.js"></script> 
<script type="text/javascript">
  $(document).ready(function() {
    $(".fancybox").fancybox();
  });
</script>
<div class=" text_heading">

    <h3 class="text-center">
      <i class="fa fa-picture-o" aria-hidden="true"></i> All Photos
    </h3>
    <div class="tooltip-2">
        <h2>Display Photos button on Mobile App

        <?php if(!empty($btnsetingArray)){ 
         foreach($btnsetingArray as $res){
          if ($res->settings == 1) { ?>
            <label class="switch ">
                <input type="checkbox" checked data-btn="<?php echo 'Photo' ?>" class="updateStatus">
                <span class="slider round"></span>
            </label>

        <?php } else { ?>
            <label class="switch space">
                <input type="checkbox" data-btn="<?php echo 'Photo' ?>" class="updateStatus">
                <span class="slider round"></span>
            </label>


        <?php } ?>
        <?php }
        }
         ?>
      </div>
      <div class="rem_add">
      <a href="#" class="btn button btn-primary opn_mdl"><i class="fa fa-plus"></i> Add Photo</a>

      </div>

</div>

<?php /* ?>
  <section class="content weather-bg news_section">
    <!-- setting start -->
    <div class="col-md-12">
      <div class="col-md-10">
        <h3>All Photos</h3>
        <?php if(!empty($btnsetingArray)){ ?>
          <?php if($btnsetingArray[0]->settings=='1'){ ?>
           <label><input type="radio" name="btnshow" value='1' checked="checked">Now All Photos showing on the mysoultab APP<br></label>
         <?php }else{ ?>
           <label> <input type="radio" name="btnshow" value='1' checked="checked">Now All Photos not showing on the mysoultab APP </label>
         <?php } ?>
       <?php } ?> 
     </div>
     <div class="col-md-2 delete-email">
      <button class="btn btn-primary chk">Settings</button>
    </div> 
    <div id="para" style="display: none "> 
      <form id="submit_form" method="post" action="<?php echo base_url()?>savsettings "> 
        <div class="col-md-10">
          <label> <input type="checkbox" name="butn_seting" id="butn_seting" value="">Please check if want to show the All Photos button in mysoultab App </label>
          <input type="hidden" name="btn" value="all_photos">
        </div>
        <div class="col-md-2">
         <input class="btn btn-primary chkdd" type="submit" name="submit" value="submit" >
       </div>
     </form>
     <div id="response"></div>
   </div>
   <!-- </div> -->
   <!-- End -->
 </section>
 <?php */ ?>
 <!-- Main content -->
 <div class=" text_heading">

    <div class="buttondiv clearfix text-center">
      <?php 
      if(!empty($user_photo_data)){ ?>
        <!-- <a href="photos_favourite" class="bttn"><i class="fa fa-heart-o"></i> Favorite Photo</a> -->
        <button  type="button" id="btn_delete" ><i class="fa fa-trash-o"></i> Delete Photo </button>
      <?php } ?>
      <!-- <a href="#" class="bttn" id="btn_delete"><i class="fa fa-trash-o"></i> Photo Delete</a> -->
    </div>
      </div>
    <?php if($this->session->flashdata('susccess')){ ?>
     <div class="alert alert-success">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <?php echo $this->session->flashdata('susccess'); ?>
    </div>
  <?php } ?>
  <div id="deletemsg" style="text-align: center;"></div>
  <div class="row tz-gallery"> <!--photos row start-->
    <?php 
    if(!empty($user_photo_data)){
      // echo "<pre>";print_r($user_photo_data);
      foreach ($user_photo_data as $key => $value) {

        $fav = $this->common_model->getSingleRecordById('cp_photo_favourite',array('photo_id'=> $value['u_photo_id']));
        
        
        ?>
        <div class="col-xs-6 col-sm-4 col-md-3">
          <div class="sc_photo_list_item ">
            <div class="sc_photo_list_item_photo" style="background-image:url('<?php echo base_url().'uploads/photos/'.$value['u_photo']; ?>')">
                          <!--<a href="<?php echo base_url().'uploads/photos/'.$value['u_photo']; ?>" class="lightbox">
                          </a>-->
                          <!-- <img class="wp-post-image loading" alt="" src="<?php echo base_url().'uploads/photos/'.$value['u_photo']; ?>"> -->
                          <!-- <img class="lazy wp-post-image" src="<?php //echo base_url(); ?>assets/images/lazy.png" alt="P51 Mustang fighter" class="lazy article_img_320 img-responsive" data-original="<?php //echo base_url().'uploads/photos/'.$value['u_photo']; ?>"> -->
                          <!-- <label class="checkbox_con">
                            <input type="checkbox" class="photo_check_box" value="<?php echo $value['u_photo_id'];?>">
                            <span class="checkmark"></span>
                          </label> -->
                          <div class="red box <?php echo $value['u_photo_id'];?>">
                            <!-- <div class="sc_team_item_hover">
                              <div class="sc_team_item_socials" style="display: inline-block;">
                                <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
                                  <div class="sc_socials_item"> -->
                                    <?php 
                                    // $u_photo_id= $value['u_photo_id'];
                                    // $photo_id =$fav['photo_id']; 
                                    // _dx($photo_id);
                                    ?>
                                    <!----------------------new swati ---------------- -->            
                                    <!-- <a href="#" class="social_icons social_facebook btn_favourite re_<?php echo $u_photo_id; ?>" data-id="<?php echo $u_photo_id;?>" id="btn_favourite_<?php echo $u_photo_id; ?>"  <?php if($fav['photo_id']){ ?> title="Favourite" <?php }else{ ?> title="Unfavourite" <?php } ?>><?php if($fav['photo_id']){ ?><i class="fa fa-heart noopencls_<?php echo $u_photo_id; ?>" aria-hidden="true"></i><?php }else{ ?><i class="fa fa-heart-o"></i><?php } ?></a>
                                    <a href="#" class="social_icons social_facebook btn_favourite renew2_<?php echo $u_photo_id; ?>" style="display:none" id="btn_favourite_<?php echo $u_photo_id; ?>" data-id="<?php echo $u_photo_id;?>" title="Unfavourite"><i style="display:none" class="fa fa-heart-o opencls_<?php echo $u_photo_id; ?>" aria-hidden="true"></i></a>
                                    <a href="#" style="display:none" data-id="<?php echo $u_photo_id;?>" class="social_icons social_facebook btn_favourite renew_<?php echo $u_photo_id; ?>" id="btn_favourite_<?php echo $u_photo_id; ?>" title="Favourite"><i style="display:none" class="fa fa-heart opencls2_<?php echo $u_photo_id; ?>" aria-hidden="true"></i></a> -->
                                    <!-------------------------------------------------------- -->      
                                  <!-- </div> -->
                                  <!-- <div class="sc_socials_item">
                                    <a href="<?php echo base_url().'uploads/photos/'.$value['u_photo']; ?>" class="social_icons social_twitter lightbox" id="btnnew_delete"><i class="fa fa-eye"></i></a>
                                  </div>
                                  <div class="sc_socials_item"> -->
                                    <!-- <a href="#" class="social_icons social_twitter btnnew_delete" id="btnnew_delete"><i class="fa fa-trash-o"></i></a> -->
                                  <!-- </div>
                                </div>
                              </div>
                            </div> -->
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php }}else{
                   echo '<div class="photo-list-empty">
                   <span class="blnk_photo"><i class="fa fa-picture-o" aria-hidden="true"></i></span>
                   <div class="nodataContainer"><h4 class="nodataFound">Photos not found</h4></div>
                   </div>';
                 } ?>
               </div>
             </div>
           </section>
         </aside>
         <div class="modal fade deleteModal" id="deleteModal" role="dialog">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete</h4>
              </div>
              <div class="modal-body">
                <h3>Are you sure ? You want to delete this</h3>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="ok" data-dismiss="modal">Ok</button>
                <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="photo-upload" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-picture-o   "></i> Add New Photo</h4>
              </div>
              <form method="post" action="" id="user_images" enctype="multipart/form-data">
               <div id="img_msg"></div>
               <div class="modal-body">
                <div class="">
                 <label style="color:red">Note :  upload image only 300px * 300px aprox dimension of  images for better result</label>
                 <div class="clearfix mrT2 pd2 uploader">
                   <div id="loders"></div>  
                   <div class="banpic"><img src="assets/user_dashboard/img/default.png" alt="banner-image">
                     <div id="photo_validation"></div>
                   </div>
                   <div class="img-upload">
                    <label for="imgInp" class="img-upload__label"><i class="fa fa-cloud-upload"></i> Upload Photo</label>
                    <input class="img-upload__input" type="file" 
                    name="imgInp" id="imgInp" >
                  </div>
                  <input type="submit" name="submit" class="btn btn-primary pull-right" value="Add Photo" >
                </div>
              </div>
            </div>                     
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
      <script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>
      <!-- <script src="assets/user_dashboard/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script> -->
      <script src="assets/admin/js/jquery.validate.js"></script>
      <!-- script added by 95 for add or remove css calss to show enable or disable delete photo button start -->

      <script type="text/javascript">
        $(document).on('click','.updateStatus',function(){
          var update_status = $(this).attr('data-btn');
          var update_st = $(this).attr('data-st');
    //alert(update_status+'-'+update_st);
    $.ajax({
      url: '<?php echo base_url();?>Pages/pages/change_button_status',
      type: "POST",
      data: {'btn':update_status},
      dataType: "json",
      success:function(data){
        setTimeout(function(){
          $("#msg_show").html("<div class='alert alert-success'>"+data.status+"</div>");
          location.reload();

        }, 1500);
      }
    }); 
  });

</script>

<script type="text/javascript">
  $(document).ready(function(){
    $('input[type="checkbox"]').click(function(){
      if($(this).prop("checked") == true){
        $("#btn_delete").addClass("enbl_btn");
      }
      else if($(this).prop("checked") == false){
       var check_count = $('input:checkbox:checked').length;
       if(check_count==0){
         $("#btn_delete").removeClass("enbl_btn");
       }
     }
   });
  });
</script>
<!-- script added by 95 for add or remove css calss to show enable or disable delete photo button end -->
<!-- Script added by 95 for enable or disable delete photo button start-->
<script type="text/javascript">
  var checkboxes = $("input[type='checkbox']")
  submitButt = $("#btn_delete");
  checkboxes.click(function() {
    submitButt.attr("disabled", !checkboxes.is(":checked"));
  });
</script>
<!-- Script added by 95 for enable or disable delete photo button end-->
<!-- Script added by 95 for add photos in fvrt section start-->
<script type="text/javascript">
 $(document).on('click','[id^="btn_favourite_"]',function(){
  var id = $(this).attr('data-id');
  $.ajax({
    type:'POST',
    url:"<?php echo base_url().'add_Favourite_Photo'?>",
    data: 'id='+id,            
    success:function(data){ 
     $(".re_"+id).hide();
     $(".noopencls_"+id).hide();
     if(data == '1'){ 
      $(".opencls_"+id).hide();
      $(".opencls2_"+id).show();  
      $(".renew2_"+id).hide();       
      $(".renew_"+id).show();
      $("#deletemsg").html('<div class="alert alert-success">'+'Successfully added to your favourite list.'+'</div>');
      setTimeout(function(){
        $("#deletemsg").html("");
      }, 1500);
    }else{ 
      $(".renew_"+id).hide();
      $(".opencls2_"+id).hide();  
      $(".opencls_"+id).show();   
      $(".renew2_"+id).show();
      $("#deletemsg").html('<div class="alert alert-success">'+'Successfully removed from your favourite list.'+'</div>');
      setTimeout(function(){
        $("#deletemsg").html("");
      }, 1500);     
    }        
  }
});
}); 
</script>
<!-- Script added by 95 for add photos in fvrt section end-->
<script type="text/javascript">
  $(document).ready(function(){
    $('input[type="checkbox"]').click(function(){
      var inputValue = $(this).attr("value");
      $("." + inputValue).toggle();
    });
  });
</script>
<style type="text/css">
  label.checkbox_con{
    position: absolute !important;
    top: 0px;
  }
  .box{
    display:none;
  }
  .red{
    padding: 0px;
    margin: 0px;
    background: rgba(0, 0, 0, 0.6);
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    filter: alpha(opacity=0);
    text-align: center;
    -webkit-transition: all ease .3s;
    -moz-transition: all ease .3s;
    -ms-transition: all ease .3s;
    -o-transition: all ease .3s;
    transition: all ease .3s;
    height: auto;
    border:0px;
  }
  .sc_photo_list_item:hover .sc_photo_list_item_photo {
    /*background: #000;*/
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    filter: alpha(opacity=0);
    text-align: center;
    -webkit-transition: all ease .3s;
    -moz-transition: all ease .3s;
    -ms-transition: all ease .3s;
    -o-transition: all ease .3s;
    transition: all ease .3s;
    height: auto;
  }
  .sc_photo_list_item:hover img {
    opacity: 0.3;
  }
</style>
<script>
    //js for photo upload model start
    $(document).ready(function(){
      $(".opn_mdl").click(function(){
        $("#photo-upload").modal();
      });
    });
    //js for photo upload model end
    //js for photo upload start
    function readURL(input) {
      var image = input.files[0]['name'];
      var image_regex =  /^.*\.(jpeg|JPEG|jpg|JPG|png|PNG)$/;
      if (!image_regex.test(image)) {
               // alert("Only jpeg, jpg, png file are accepted");
               $("#photo_validation").html("<div style='font-size:18px;color:red'>Only jpeg, jpg, png file are accepted</div>");
               return false;
             }
             if (input.files && input.files[0]) {
               $("#photo_validation").html(" ");  
               var reader = new FileReader();
               reader.onload = function (e) {
                $('.banpic img').attr('src', e.target.result);
              }
              reader.readAsDataURL(input.files[0]);
            }
          }
          $("#imgInp").change(function(){
            readURL(this);
          });
//js for photo upload start                
</script>  
<script type="text/javascript">
  $(function() {
    $("#user_images").validate({
      rules: {
        imgInp: {
          required: true,
        }
      },
      messages: {
        imgInp: {
          required: "<font style='font-weight:normal;color:red;'>Please select image</font>",
        },
      },
    });
  });
  $("#user_images").on('submit',(function(e) 
  {  
   e.preventDefault();
   var isvalidate=$("#user_images").valid();
   if(isvalidate)
   {
     $("#loders").append('<div id="video_loader" class="loading_class"><img id="loading_img" style="width:20px" src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif" /></div>');
     $.ajax({
      url : "<?php echo base_url(); ?>Pages/ajax_photo_add",
      type: "POST",             
      data: new FormData(this), 
      dataType: "json",
      contentType: false,       
      cache: false,             
      processData:false,        
      success: function(data) {  
       $("#loading_img").hide();
       $("#video_loader").hide();
       if(data.code == '1'){
        $("#loading_img").hide();
        $("#video_loader").hide();
        $("#img_msg").html('<div class="alert alert-success">Photo have been successfully added<div>');
        setTimeout(function(){
          $("#img_msg").html(""); 
          location.reload();
        }, 1000);
      }else{
       $("#img_msg").html('<div class="alert alert-danger">'+data.message+'<div>');
       setTimeout(function(){
        $("#img_msg").html(""); 
        return false;
      }, 1000);$("#loading_img").hide();
       $("#video_loader").hide();
     }      
   }
 });
   }
 }));
</script>
<script type="text/javascript">
 // $(document).ready(function(){  
 //    $('#btn_delete1').click(function(){
 //          var email_url =  "<?php echo base_url()?>";
 //          if($("#deleteModal").modal('show'))
 //          {
 //           var id = [];
 //           $(':checkbox:checked').each(function(i){
 //            id[i] = $(this).val();
 //           });
 //           if(id.length === 0) //tell you if the array is empty
 //           {
 //            alert("Please Select atleast one checkbox");
 //           }
 //           else
 //           {
 //            $.ajax({
 //             url:'<?php echo base_url().'delete_photos'?>',
 //             method:'POST',
 //             data:{id:id},
 //             success:function()
 //             {
 //              for(var i=0; i<id.length; i++)
 //              {
 //                $("#deletemsg").html("Photos delete successfully");
 //                setTimeout(function(){
 //                  $("#deletemsg").html("Photos delete successfully");
 //                    window.location.href = email_url+"user_photos";
 //                }, 1000);
 //              }
 //             }
 //            });
 //           }
 //          }
 //          else
 //          {
 //           return false;
 //          }
 //    });
 // });
 // $(document).ready(function(){
 //    $('.btn_favourite').click(function(){
 //        var id = [];
 //       $(':checkbox:checked').each(function(i){
 //        id[i] = $(this).val();
 //      });
 //      if(confirm("Are you sure you want to add favourite or unfavourite image?"))
 //      {
 //       $.ajax({
 //        url:"<?php //echo base_url().'addphotos_favourite'?>",
 //        method:"POST",
 //        data:{id:id},
 //        success:function(data)
 //        { 
 //            $("#loading").hide();  
 //            //$("#deletemsg").html("Photo add favourite or unfavourite successfully");
  //    if(data = 'success'){
  //      var msg = 'Successfully Favourite';
  //    }else{
  //      var msg = 'Successfully Unfavourite';
  //    }
 //            setTimeout(function(){
 //                $("#deletemsg").html('<div class="alert alert-success">'+msg+'</div>');
 //                window.location.reload(true);
 //            }, 1000);
 //        }
 //       })
 //      }
 //      else
 //      {
 //       return false;
 //      }
 //    });
 // });
 $(document).on('click', '#btn_delete', function(){
  var id = [];
  $(':checkbox:checked').each(function(i){
    id[i] = $(this).val();
  });
  if(confirm("Are you sure you want to delete photo ?"))
  {
   $.ajax({
    url:"<?php echo base_url().'delete_photos'?>",
    method:"POST",
    data:{id:id},
    success:function(data)
    {
      $("#loading").hide();  
      $("#deletemsg").html("<div class='alert alert-success'>Photos delete successfully");
      setTimeout(function(){
        $("#deletemsg").html("");
        window.location.reload(true);
      }, 1000);
    }
  })
 }
 else
 {
   return false;
 }
});
 $(document).on('click', '.btnnew_delete', function(){
  var id = [];
  $(':checkbox:checked').each(function(i){
    id[i] = $(this).val();
  });
  if(confirm("Are you sure you want to delete photo ?"))
  {
   $.ajax({
    url:"<?php echo base_url().'delete_photos'?>",
    method:"POST",
    data:{id:id},
    success:function(data)
    {
      $("#loading").hide();  
      $("#deletemsg").html("Photo delete successfully");
      setTimeout(function(){
        $("#deletemsg").html("");
        window.location.reload(true);
      }, 1000); 
    }
  })
 }
 else
 {
   return false;
 }
});
</script>
