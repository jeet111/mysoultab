<?php

if (isset($staff_permission[5]['add_permission']) == 1) {

   ?>

   <div class="bx_user">

      <div class="add-btn">

         <a href="<?php echo base_url(); ?>admin/add_staff">

         Add Staff

         </a>

      </div>

   </div>

   <?php

} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

   ?>

   <div class="bx_user">

      <div class="add-btn">

         <a href="<?php echo base_url(); ?>admin/add_staff">

         Add Staff

         </a>

      </div>

   </div>

   <?php

}

?>



<div class="container-fluid">

   <div class="dash-counter users-main my-sts-table">

      <div class="row">

         <div class="col-md-12">

            <div class="">

               <div class="card-body">

                  <div class="status-cng"></div>

                  <?php if ($this->session->flashdata('success')) { ?>

                  <div class="alert alert-success message">

                     <button type="button" class="close" data-dismiss="alert">x</button>

                     <?php echo $this->session->flashdata('success'); ?>

                  </div>

                  <?php } ?>

                  <!-- <div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_test_type">Add test type</a></div> -->

                  <table class="table table-hover tab_comn" id="sampleTable">

                     <thead>

                        <tr>

                           <th>S no.</th>

                           <th>First Name</th>

                           <th>Last Name</th>

                           <!-- <th>User Name</th> -->

                           <th>Email</th>

                           <th>Mobile</th>

                           <th>Create Date</th>

                           <th>Action</th>

                        </tr>

                     </thead>

                     <tbody>

                        <?php

                           $i = 1;

                           foreach ($staff_list as $list) {

                           	?>

                              <tr>

                                 <td><?php echo $i++; ?></td>

                                 <td><?php echo $list['name']; ?></td>

                                 <td><?php echo $list['lastname']; ?></td>

                                 <!-- <td><php echo $list->username; ?></td> -->

                                 <td><?php echo $list['email']; ?></td>

                                 <td><?php echo $list['mobile']; ?></td>

                                 <td><?php echo date('d-m-Y', strtotime($list['create_user'])); ?></td>

                                 <td>

                                    <div class="link-del-view">

                                       <?php

                                       if (isset($staff_permission[5]['edit_permission']) == 1) {

                                          ?>

                                          <div class="tooltip-2">

                                              <a href="<?php echo base_url(); ?>admin/edit_staff/<?php echo $list['id']; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>

                                              <span class="tooltiptext">Edit</span>

                                          </div>

                                          <?php

                                       } elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

                                          ?>

                                          <div class="tooltip-2">

                                              <a href="<?php echo base_url(); ?>admin/edit_staff/<?php echo $list['id']; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>

                                              <span class="tooltiptext">Edit</span>

                                          </div>

                                          <?php

                                       }

                                       ?>

                                       

                                       <?php

                                       if (isset($staff_permission[5]['delete_permission']) == 1) {

                                          ?>

                                          <div class="tooltip-2">

                                             <a href="<?php echo base_url(); ?>admin/delete_staff/<?php echo $list['id']; ?>" onclick="return confirm('Are you sure, you want to delete this staff ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

                                          </div>

                                          <?php

                                       } elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

                                          ?>

                                          <div class="tooltip-2">

                                             <a href="<?php echo base_url(); ?>admin/delete_staff/<?php echo $list['id']; ?>" onclick="return confirm('Are you sure, you want to delete this staff ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

                                          </div>

                                          <?php

                                       }

                                       ?>

                                       

                                       <?php

                                       if (isset($staff_permission[5]['edit_permission']) == 1) {

                                          ?>

                                          <div class="tooltip-2">

                                             <?php $user_id = $list['id']; ?>

                                             <a href="javascript:void(0)" onclick="update_status('<?php echo $user_id; ?>');">

                                                <?php

                                                if ($list['status'] == 1) {

                                                   ?><i class="fa fa-toggle-on" aria-hidden="true"></i><?php

                                                } else {

                                                   ?><i class="fa fa-toggle-off" aria-hidden="true"></i><?php

                                                }

                                                ?>

                                                

                                             </a>

                                             <span class="tooltiptext">

                                                Status

                                             </span>

                                          </div>



                                          <div class="tooltip-2">

                                             <a href="javascript:void(0)" data-toggle="modal" data-target="#accessModal<?php echo $list['id']; ?>">

                                                <i class="fa fa-lock" aria-hidden="true"></i>

                                             </a>

                                             <span class="tooltiptext">

                                                Access

                                             </span>

                                          </div>

                                          <?php

                                       } elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

                                          ?>

                                          <div class="tooltip-2">

                                             <?php $user_id = $list['id']; ?>

                                             <a href="javascript:void(0)" onclick="update_status('<?php echo $user_id; ?>');">

                                                <?php

                                                if ($list['status'] == 1) {

                                                   ?><i class="fa fa-toggle-on" aria-hidden="true"></i><?php

                                                } else {

                                                   ?><i class="fa fa-toggle-off" aria-hidden="true"></i><?php

                                                }

                                                ?>

                                                

                                             </a>

                                             <span class="tooltiptext">

                                                Status

                                             </span>

                                          </div>



                                          <div class="tooltip-2">

                                             <a href="javascript:void(0)" data-toggle="modal" data-target="#accessModal<?php echo $list['id']; ?>">

                                                <i class="fa fa-lock" aria-hidden="true"></i>

                                             </a>

                                             <span class="tooltiptext">

                                                Access

                                             </span>

                                          </div>

                                          <?php

                                       }

                                       ?>

                                       



                                       <!-- Modal -->

                                       <div id="accessModal<?php echo $list['id']; ?>" class="modal fade" role="dialog">

                                         <div class="modal-dialog">



                                           <!-- Modal content-->

                                           <div class="modal-content">

                                             <div class="modal-header">

                                               <button type="button" class="close" data-dismiss="modal">&times;</button>

                                               <h4 class="modal-title">Access Permission</h4>

                                             </div>

                                             <div class="modal-body">

                                                

                                                <div id="responseMsg<?php echo $list['id']; ?>">

                                                </div>

                                                <form class="form-horizontal" onsubmit="return false;" id="permissionForm<?php echo $list['id']; ?>" action="#" method="post">

                                                     <input type="hidden" name="user_id" value="<?php echo $list['id']; ?>"> 

                                                     <div class="form-group" style="display: block;">

                                                       <label class="control-label col-sm-4" for="email">Dashboard:</label>

                                                       <div class="col-sm-8">

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="1_1" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][1]) && $list['permission_id'][1]['view_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               View

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="1_2" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][1]) && $list['permission_id'][1]['add_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Add

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="1_3" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][1]) && $list['permission_id'][1]['edit_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Edit

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="1_4" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][1]) && $list['permission_id'][1]['delete_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Delete

                                                            </label>

                                                       </div>

                                                     </div>



                                                     <div class="form-group" style="display: block;">

                                                       <label class="control-label col-sm-4" for="email">Users Management:</label>

                                                       <div class="col-sm-8">

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="2_1" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][2]) && $list['permission_id'][2]['view_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               View

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="2_2" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][2]) && $list['permission_id'][2]['add_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Add

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="2_3" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][2]) && $list['permission_id'][2]['edit_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Edit

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="2_4" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][2]) && $list['permission_id'][2]['delete_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Delete

                                                            </label>

                                                       </div>

                                                     </div>



                                                     <div class="form-group" style="display: block;">

                                                       <label class="control-label col-sm-4" for="email">Doctor Management:</label>

                                                       <div class="col-sm-8">

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="3_1" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][3]) && $list['permission_id'][3]['view_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               View

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="3_2" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][3]) && $list['permission_id'][3]['add_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Add

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="3_3" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][3]) && $list['permission_id'][3]['edit_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Edit

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="3_4" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][3]) && $list['permission_id'][3]['delete_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Delete

                                                            </label>

                                                       </div>

                                                     </div>



                                                     <div class="form-group" style="display: block;">

                                                       <label class="control-label col-sm-4" for="email">Test Type:</label>

                                                       <div class="col-sm-8">

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="4_1" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][4]) && $list['permission_id'][4]['view_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               View

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="4_2" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][4]) && $list['permission_id'][4]['add_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Add

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="4_3" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][4]) && $list['permission_id'][4]['edit_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Edit

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="4_4" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][4]) && $list['permission_id'][4]['delete_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Delete

                                                            </label>

                                                       </div>

                                                     </div>



                                                     <div class="form-group" style="display: block;">

                                                       <label class="control-label col-sm-4" for="email">Staff Maagement:</label>

                                                       <div class="col-sm-8">

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="5_1" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][5]) && $list['permission_id'][5]['view_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               View

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="5_2" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][5]) && $list['permission_id'][5]['add_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Add

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="5_3" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][5]) && $list['permission_id'][5]['edit_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Edit

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="5_4" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][5]) && $list['permission_id'][5]['delete_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Delete

                                                            </label>

                                                       </div>

                                                     </div>





                                                     <div class="form-group" style="display: block;">

                                                       <label class="control-label col-sm-4" for="email">Vital Sign Test Type:</label>

                                                       <div class="col-sm-8">

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="6_1" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][6]) && $list['permission_id'][6]['view_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               View

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="6_2" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][6]) && $list['permission_id'][6]['add_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Add

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="6_3" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][6]) && $list['permission_id'][6]['edit_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Edit

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="6_4" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][6]) && $list['permission_id'][6]['delete_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Delete

                                                            </label>

                                                       </div>

                                                     </div>



                                                     <div class="form-group" style="display: block;">

                                                       <label class="control-label col-sm-4" for="email">Vital Sign:</label>

                                                       <div class="col-sm-8">

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="7_1" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][7]) && $list['permission_id'][7]['view_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               View

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="7_2" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][7]) && $list['permission_id'][7]['add_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Add

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="7_3" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][7]) && $list['permission_id'][7]['edit_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Edit

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="7_4" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][7]) && $list['permission_id'][7]['delete_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Delete

                                                            </label>

                                                       </div>

                                                     </div>



                                                     <div class="form-group" style="display: block;">

                                                       <label class="control-label col-sm-4" for="email">Acknowledge:</label>

                                                       <div class="col-sm-8">

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="8_1" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][8]) && $list['permission_id'][8]['view_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               View

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="8_2" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][8]) && $list['permission_id'][8]['add_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Add

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="8_3" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][8]) && $list['permission_id'][8]['edit_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Edit

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="8_4" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][8]) && $list['permission_id'][8]['delete_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Delete

                                                            </label>

                                                       </div>

                                                     </div>



                                                     <div class="form-group" style="display: block;">

                                                       <label class="control-label col-sm-4" for="email">Pharma Company:</label>

                                                       <div class="col-sm-8">

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="9_1" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][9]) && $list['permission_id'][9]['view_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               View

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="9_2" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][9]) && $list['permission_id'][9]['add_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Add

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="9_3" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][9]) && $list['permission_id'][9]['edit_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Edit

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="9_4" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][9]) && $list['permission_id'][9]['delete_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Delete

                                                            </label>

                                                       </div>

                                                     </div>



                                                     <div class="form-group" style="display: block;">

                                                       <label class="control-label col-sm-4" for="email">Banks:</label>

                                                       <div class="col-sm-8">

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="10_1" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][10]) && $list['permission_id'][10]['view_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               View

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="10_2" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][10]) && $list['permission_id'][10]['add_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Add

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="10_3" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][10]) && $list['permission_id'][10]['edit_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Edit

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="10_4" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][10]) && $list['permission_id'][10]['delete_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Delete

                                                            </label>

                                                       </div>

                                                     </div>



                                                     <div class="form-group" style="display: block;">

                                                       <label class="control-label col-sm-4" for="email">Restaurants:</label>

                                                       <div class="col-sm-8">

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="11_1" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][11]) && $list['permission_id'][11]['view_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               View

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="11_2" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][11]) && $list['permission_id'][11]['add_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Add

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="11_3" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][11]) && $list['permission_id'][11]['edit_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Edit

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="11_4" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][11]) && $list['permission_id'][11]['delete_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Delete

                                                            </label>

                                                       </div>

                                                     </div>





                                                     <div class="form-group" style="display: block;">

                                                       <label class="control-label col-sm-4" for="email">Grocerys:</label>

                                                       <div class="col-sm-8">

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="12_1" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][12]) && $list['permission_id'][12]['view_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               View

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="12_2" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][12]) && $list['permission_id'][12]['add_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Add

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="12_3" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][12]) && $list['permission_id'][12]['edit_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Edit

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="12_4" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][12]) && $list['permission_id'][12]['delete_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Delete

                                                            </label>

                                                       </div>

                                                     </div>



                                                     <div class="form-group" style="display: block;">

                                                       <label class="control-label col-sm-4" for="email">Entertainment:</label>

                                                       <div class="col-sm-8">

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="13_1" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][13]) && $list['permission_id'][13]['view_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               View

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="13_2" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][13]) && $list['permission_id'][13]['add_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Add

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="13_3" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][13]) && $list['permission_id'][13]['edit_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Edit

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="13_4" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][13]) && $list['permission_id'][13]['delete_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Delete

                                                            </label>

                                                       </div>

                                                     </div>



                                                     <div class="form-group" style="display: block;">

                                                       <label class="control-label col-sm-4" for="email">Spirituality:</label>

                                                       <div class="col-sm-8">

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="14_1" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][14]) && $list['permission_id'][14]['view_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               View

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="14_2" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][14]) && $list['permission_id'][14]['add_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Add

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="14_3" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][14]) && $list['permission_id'][14]['edit_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Edit

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="14_4" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][14]) && $list['permission_id'][14]['delete_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Delete

                                                            </label>

                                                       </div>

                                                     </div>



                                                     <div class="form-group" style="display: block;">

                                                       <label class="control-label col-sm-4" for="email">Distance Units:</label>

                                                       <div class="col-sm-8">

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="15_1" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][15]) && $list['permission_id'][15]['view_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               View

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="15_2" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][15]) && $list['permission_id'][15]['add_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Add

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="15_3" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][15]) && $list['permission_id'][15]['edit_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Edit

                                                            </label>

                                                            <label class="checkbox-inline">

                                                               <input type="checkbox" name="staff_permission[]" value="15_4" <?php echo (!empty($list['permission_id']) ? (!empty($list['permission_id'][15]) && $list['permission_id'][15]['delete_permission'] == 1 ? 'checked' : '') : ''); ?>>

                                                               Delete

                                                            </label>

                                                       </div>

                                                     </div>



                                                     <br>

                                                     <div class="form-group">

                                                       <div class="col-sm-offset-2 col-sm-10">

                                                         <?php 

                                                            $form_id = 'permissionForm'.$list['id']; 

                                                            $user_id = $list['id'];

                                                         ?>

                                                         <button type="submit" onclick="submit_access('<?php echo $form_id; ?>','<?php echo $user_id; ?>');" class="btn btn-default">Submit</button>

                                                       </div>

                                                     </div>



                                                   </form>



                                             </div>

                                             <div class="modal-footer">

                                               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                             </div>

                                           </div>



                                         </div>

                                       </div>



                                    </div>

                                 </td>

                              </tr>

                              <?php 

                           } 

                           ?>

                     </tbody>

                  </table>

               </div>

            </div>

         </div>

      </div>

   </div>

</div>



<script type="text/javascript">

   function submit_access(form_id,user_id) {

       //var formData = new FormData($("#"+form_id)[0]);

       var form = $('#'+form_id)[0];

       var data = new FormData(form);

       $.ajax({

           type: 'POST',

           url: "<?php echo base_url().'admin/submit_permission'; ?>",

           data: data,

           //dataType: "text",

           processData: false,

           contentType: false,

           cache: false,

           success: function(resultData) { 

               console.log(resultData);

               if (parseInt(resultData) === 1) {

                  $("#responseMsg"+user_id).html('<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> Permission has been set successfully!</div>');

                  setTimeout(function() {

                     location.reload();

                  }, 5000);

               } else {

                  $("#responseMsg"+user_id).html('<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Some internal issue occured.</div>');

               }

               

           },

           error: function (errorData) {

               console.log(errorData);

               $("#responseMsg"+user_id).html('<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Some internal issue occured.</div>');

           }

       });

   }



   function update_status(user_id) {

      if (confirm('Are you sure you want to update this?')) {

         $.ajax({

              type: 'POST',

              url: "<?php echo base_url().'admin/update_status'; ?>",

              data: {user_id:user_id},

              //dataType: "text",

              /*processData: false,

              contentType: false,

              cache: false,*/

              success: function(resultData) { 

                  console.log(resultData);

                  if (parseInt(resultData) === 1) {

                     alert("Status has been updated successfully!");

                     setTimeout(function() {

                        location.reload();

                     }, 3000);

                  } else {

                     alert("Some internal issue occured!");

                  }

                  

              },

              error: function (errorData) {

                  console.log(errorData);

                  alert("Some internal issue occured!");

              }

         });

      }

   }

</script>