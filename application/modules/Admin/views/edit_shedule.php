<div class="container-fluid">
	<div class="dash-counter">
		<div class="Schedule_main_one">
			<div class="users-main">
				<?php
				echo "<h2>Edit Shedule</h2>";
				?>

				<form class="form-horizontal" action="" method="post" enctype="multipart/form-data" name="Adddoctor" id="add_shedule">

					<input type="hidden" name="doctor_id" value="<?php echo $editData[0]['avdate_dr_id'] ?>">


					<?php if ($this->session->flashdata('success')) { ?>
						<div class="alert alert-success message">
							<button type="button" class="close" data-dismiss="alert">x</button>
							<?php echo $this->session->flashdata('success'); ?></div>
						<?php } ?>


						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Shedule Date:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<input type="text" placeholder="Shedule date" class="form-control" name="shedule_date" id="shedule_dates" value="<?php echo date('Y-m-d' ,strtotime($editData[0]['avdate_date'])); ?>">
										<?php echo form_error('shedule_date'); ?>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Shedule Time:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<select name="shedules[]" id="sheduless" class="form-control" multiple="multiple" >

											<option value="06:00 AM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='06:00 AM') echo 'selected'; }?>>06:00 AM</option>
											<option value="07:00 AM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='07:00 AM') echo 'selected'; }?>>07:00 AM</option>
											<option value="08:00 AM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='08:00 AM') echo 'selected'; }?>>08:00 AM</option>
											<option value="09:00 AM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='09:00 AM') echo 'selected'; }?>>09:00 AM</option>
											<option value="10:00 AM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='10:00 AM') echo 'selected'; }?>>10:00 AM</option>
											<option value="11:00 AM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='11:00 AM') echo 'selected'; }?>>11:00 AM</option>
											<option value="12:00 PM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='12:00 PM') echo 'selected'; }?>>12:00 PM</option>
											<option value="01:00 PM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='01:00 PM') echo 'selected'; }?>>01:00 PM</option>
											<option value="02:00 PM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='02:00 PM') echo 'selected'; }?>>02:00 PM</option>
											<option value="03:00 PM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='03:00 PM') echo 'selected'; }?>>03:00 PM</option>
											<option value="04:00 PM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='04:00 PM') echo 'selected'; }?>>04:00 PM</option>
											<option value="05:00 PM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='05:00 PM') echo 'selected'; }?>>05:00 PM</option>
											<option value="06:00 PM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='06:00 PM') echo 'selected'; }?>>06:00 PM</option>
											<option value="07:00 PM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='07:00 PM') echo 'selected'; }?>>07:00 PM</option>
											<option value="08:00 PM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='08:00 PM') echo 'selected'; }?>>08:00 PM</option>
											<option value="09:00 PM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='09:00 PM') echo 'selected'; }?>>09:00 PM</option>
											<option value="10:00 PM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='10:00 PM') echo 'selected'; }?>>10:00 PM</option>
											<option value="11:00 PM" <?php foreach ($edittimeData as $value){ if($value['avtime_text']=='11:00 PM') echo 'selected'; }?>>11:00 PM</option>

										</select>
										<?php echo form_error('shedules'); ?>

									</div>
								</div>
							</div>


						</div>


						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit" >
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<!--Added by 95 on 5-2-2019 For Doctor category for validation-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#add_shedule").validate({
				rules: {
					shedule_date: "required",
					"shedules[]": "required",
				},
				messages: {
					shedule_date: "Please select date",
					shedules: "Please select at least on time",
				}
			});
		});
// $(document).ready(function() {
//          $("#add_shedule").validate({
//             rules: {
//                 shedule_date: "required",
// 				"shedules[]": "required",
// 				},
// 				message:{
// 				shedule_date:"Please select date",
// 				shedules:"Please select at least on time",
// 				},
//         });
//     });
/*Created by 95 on 6-2-2019 For Image validation*/
function showFileSize(){
	var img  = document.getElementById('profile_images').value;
	if(img==='' || img===null){
		document.getElementById("demo").innerHTML="Please choose an image.";
	}else{
		document.getElementById("demo").innerHTML=" ";
	}
}
 /* $(document).ready(function() {
    $("#shedule_dates").datepicker({minDate: 0});
});*/

$(function () {
	//var today = new Date();
	$('#shedule_dates').datetimepicker({
		ignoreReadonly: true,
		format: 'YYYY-MM-DD',
	//minDate: today
});
});




$(document).ready(function() {
	$('#sheduless').multiselect({
		includeSelectAllOption: true,
		allSelectedText: 'No option left ...'
	});
});
</script>