<style>
input.btn.btn-primary {
    max-width: 176px !important;
    margin: 0 !important;
}
.text_heading {
    border-bottom: 0;
    margin: 0 0 14px 0;
}
.text_heading {
    border-bottom: 0;
}
.form-group > label:not(.error) {
    color: #000;
}
</style>
<div class="text_heading">

        <h3 class="text-center">

          <!-- <i class="fa fa-paper-plane"></i>  -->

          <i class="fa fa-file-text" aria-hidden="true"></i>

          Edit Test Report
    </h3>

    <div class="back_reminder"><a href="<?php echo base_url(); ?>testreport_list" class="btn btn-primary">Back</a></div>


    </div>

    <?php //echo "<pre>"; print_r($reportData); echo "</pre>"; ?>    

    <section class="content photo-list">

        <div class="photo-listMain">            

          <form id="edit_testreport" enctype="multipart/form-data" method="post" class="frm_add">

            <div class="row">

                    <div class="col-sm-6 col-md-6">                        

                      <div class="form-group">

                          <label>Test Report Title:</label>

                          <input type="text" name="report_title" class="form-control valid" id="" placeholder="Test Report Title" value="<?php echo $reportData[0]->test_report_title; ?>">

                          <?php echo form_error('report_title'); ?>

                      </div>                                  

                    </div>

                    <div class="col-sm-6 col-md-6">                        

                      <div class="form-group">

                          <label>Select Doctor:</label>

                          <select name="doctor" class="form-control">

                            <option value="">Select Doctor</option>

                          <?php foreach ($Doctors as $Doctor){ ?>

                          <option value="<?php echo $Doctor->doctor_id; ?>" <?php if($reportData[0]->doctor_id==$Doctor->doctor_id){echo 'selected="selected"';} ?>><?php if(!empty($Doctor->doctor_name)){ echo $Doctor->doctor_name; } else{$Doctor->other_doctor_name;}?></option>

                          <?php } ?>

                          </select>

                          <?php echo form_error('doctor'); ?>

                      </div>                                  

                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-6 col-md-6">

                      <div class="form-group">

                          <label>Select Test Type:</label>

                          <select name="test_type" class="form-control">

                            <option value="">Select Test Type</option>

                            <?php foreach ($Testtypes as $type) { ?>

  <option value="<?php echo $type->id; ?>" <?php if($reportData[0]->test_type_id==$type->id){echo 'selected="selected"';} ?> ><?php echo $type->test_type; ?> </option>

                         <?php } ?>

                          </select>

                          <?php echo form_error('test_type'); ?>

                      </div>





                      <div class="form-group">

                          <label>Write Description Here About Medicine:</label>

                          <textarea class="form-control" name="report_description" rows="3" placeholder="Write Description Here About medicine."><?php echo $reportData[0]->description; ?></textarea>

                          <?php echo form_error('report_description'); ?>

                      </div>               

                    </div> 

                    <div class="col-sm-6 col-md-6">

                      <div class="form-group">

                          <label>Pick Test Report:</label>

                          <input type="file" name="picture" class="form-control valid" id="reportFile" placeholder="Medicine Name" value="" onchange="readUrl(event)">

                          <div id="flupmsgs"></div>



                         <!-- <div class="profile-up"> -->

                          <?php if($reportData[0]->image==''){ ?>

        <img  style="width:50%" id='output' src="<?php echo base_url().'uploads/articles/default_icon.png' ?>">



      <?php }else{

       $explode = explode(".",$reportData[0]->image);

	   ?>

       <?php if($explode[1] == 'jpg' || $explode[1] == 'jpeg' || $explode[1] == 'gif' || $explode[1] == 'png'){ ?>

        <img style="width:50%" id='output' src="<?php echo base_url().'uploads/test_report/'.$reportData[0]->image; ?>">

	   <?php }else{ ?>

	   <?php echo $reportData[0]->image; ?>

	   <img  id='output' style="width:50%" src="">

	   <?php } ?>

      <?php } ?>

    <!--  </div> -->

                      </div>



                    </div>



                    









                </div>

                <div class="row">

                  <div class="col-sm-6 col-md-6">

                                     

                    </div>  

                </div>

                <div class="box-footer">

                    <input type="submit" name="submit" class="btn btn-primary" value="Update Test Report" id="up_but">

                </div>               

            </form>              

        </div>

    </section>

</aside>

<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

<script type="text/javascript">

    $(document).ready(function() {

    $("#edit_testreport").validate({

            rules: {

                report_title: "required",

                 doctor: "required",

                 test_type: "required",

                 report_description: "required",

             },

        

            messages: {

                report_title: "Please enter test report title.",

                 doctor: "Please select a doctor.",

                 test_type: "Please select a test type.",

                 report_description: "Please enter description.",

            },

        });



    });





    function readUrl (event) {

     var pcFile = $('#reportFile').val().split('\\').pop();

  var pcExt     = pcFile.split('.').pop();

  var output = document.getElementById('output');

  if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"

             || pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"||pcExt == "DOC" ||pcExt == "doc" || pcExt == "docx" ||pcExt == "DOCX" ||pcExt == "TXT" ||pcExt == "txt" ||pcExt == "pdf"||pcExt == "PDF"){

    $("#up_but").prop('disabled', false);

        $('#flupmsgs').html('');       

      

       output.src = URL.createObjectURL(event.target.files[0]);

     $('#output').show();

      $('#flupmsgs').html('');

  }else{

      $('#flupmsgs').html('Please select a valid file.');

      $('#output').hide();

    $("#up_but").prop('disabled', true);

  }

};

</script>

<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>







    





    



                