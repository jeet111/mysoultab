<div class="home_slider fullscreen">
<!--   <div id="demo">
   <div id="owl-demo" class="owl-carousel">
    <div class="item"> -->
     <div class="home-slider-img">
      <img src="<?php echo base_url(); ?>assets/images/banner.jpg" alt="Elderly Watching Tablet" >
    </div>
<!--   </div>
</div>
</div> --> 
<div class="banner_ser   animated">
  <div class="banner_txt_l">
    <b>companion for elderly to organize their lives in a better way </b>
  <!--  <p>why user should </p>
   <h1>buy by app </h1> -->
   <div class="buy_butt">    <span class="bat_bann"> <a href="<?php echo base_url(); ?>pricing">BUY Now</a></span></div>
 </div>
</div>
<!-- <div class="main-srch-sub">
  <a href="#next_sec"><img src="assets/images/down_icon.png"></a>
</div> -->
</div>
</article>
<main id="main">
  <section  class="teacherSection " data-aos="fade-up">
    <div class="container " >
      <div id="next_sec"></div>
      <div   id="section2"  class="bigHeading wow fadeInDown animated"> 
        <div class="bigText">Work</div>
        <h2 class="margin-bottom-xxl text-center">Why are Seniors<strong> Buying Soul Tab?</strong></h2>
      </div>
      <div class="sec_one_box">
        <div class="row">
          <?php $i = 1; ?>
          <?php foreach ($testiData as $key => $value) {?>
            <div class="col-md-4">
              <div class="sec_one_box_one wow fadeInDown animated">
                <div class="img_s">
                  <img src="<?php echo base_url(); ?>uploads/<?php echo $value->image; ?>" alt="Testimonial <?php echo $key+1;  ?>">
                </div>  
                <div class="txt_im">
                 <h3><?php echo $value->name; ?></h3>
                 <p><?php echo $value->description; ?></p>
               </div>
             </div>
           </div>
           <?php if ($i++ == 3) break; ?>
         <?php } ?>
        <!--  <div class="col-md-4">
          <div class="sec_one_box_one wow fadeInDown animated">
            <div class="img_s">
             <img src="assets/images/box2.jpg">
           </div>  
           <div class="txt_im">
             <h3>JÖRG LANGHOF </h3>
             <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
           </div>
         </div>
       </div>
       <div class="col-md-4">
        <div class="sec_one_box_one wow fadeInDown animated">
          <div class="img_s">
           <img src="assets/images/box3.jpg">
         </div>  
         <div class="txt_im">
           <h3>JÖRG LANGHOF </h3>
           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
         </div>
       </div>
     </div> -->
     <div class="more_butt_sec">
      <span class="more_b">
        <a href="<?php echo base_url(); ?>member-stories">Load More</a>
      </span>
    </div>
  </div>
</div>
</div>
</section>
<section class="whole_family">
  <div class="container">
    <div class="whole_family_main">
     <div class="col-md-6"> 
      <div class="whole_family_main_l">
       <div class="bigHeading wow fadeInDown animated">
        <div class="bigText">Family</div>
        <h2 class="margin-bottom-xxl text-center">for the <strong>Whole Family</strong></h2>
      </div>
      <p>Soul Tab caters to the whole family, assisting in daily scheduling, connecting with the loved ones through social media apps like Skype, Facebook, Instagram, or staying updated about the latest news, everything is just one click away. Besides this, the app provides y videos and spirituality guides that help in keeping the emotional and mental balance you need. </p>
    </div>
  </div>
  <div class="col-md-6"> 
    <div class="whole_family_main_r wow fadeInDown animated">
      <img src="<?php echo base_url(); ?>assets/images/tab.png" alt="SoulTab Tablet">
    </div>
  </div>
</div>
</div>
</section>
<section class="effortless_sec">
  <div class="container">
    <div class="effortless_sec_on">
      <div class="bigHeading wow fadeInDown animated">
        <div class="bigText">Effortless</div>
        <h2 class="margin-bottom-xxl text-center">Easy & <strong>Effortless</strong></h2>
      </div>
    </div>
  </div>
  <div class="effortless_sec_two">
    <div class="container">
      <div class="effortless_sec_two_m">
        <div class="home_five_box  wow fadeInDown  animated"> 

          <div class="col-md-3 col-sm-6 col-xs-12">
           <div class="feature_sec_tab_l_on  wow fadeInUp" data-wow-delay=".0s">
            <div class="flip-card">
              <div class="flip-card-inner">
                <div class="flip-card-front">
                  <img src="<?php echo base_url(); ?>assets/images/call.png" alt="Avatar" >
                  <h3>Call</h3>
                </div>
                <div class="flip-card-back">
                  <h1><a href="#" data-toggle="modal" data-target="#myModal"> Learn More </a></h1> 
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="feature_sec_tab_l_on wow fadeInUp" data-wow-delay=".2s">
            <div class="flip-card">
              <div class="flip-card-inner">
                <div class="flip-card-front">
                  <img src="<?php echo base_url(); ?>assets/images/email.png" alt="Avatar" >
                  <h3>Email</h3>
                </div>
                <div class="flip-card-back">
                 <h1><a href="#" data-toggle="modal" data-target="#myModal4"> Learn More </a></h1> 
               </div>
             </div>
           </div>
         </div>
       </div>

       <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="feature_sec_tab_l_on wow fadeInUp" data-wow-delay=".2s">
          <div class="flip-card">
            <div class="flip-card-inner">
              <div class="flip-card-front">
                <img src="<?php echo base_url(); ?>assets/images/Transpottation.png" alt="Avatar" >
                <h3>Transportation</h3>
              </div>
              <div class="flip-card-back">
                <h1><a href="#" data-toggle="modal" data-target="#myModal2"> Learn More </a></h1> 
              </div>
            </div>
          </div></div> 
        </div>


        <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="feature_sec_tab_l_on wow fadeInUp" data-wow-delay=".0s">
           <div class="flip-card">
            <div class="flip-card-inner">
              <div class="flip-card-front">
                <img src="<?php echo base_url(); ?>assets/images/Spirituality.png" alt="Avatar" >
                <h3>Spirituality</h3>
              </div>
              <div class="flip-card-back">
                <h1><a href="#" data-toggle="modal" data-target="#myModal3"> Learn More </a></h1> 
              </div>
            </div>
          </div></div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="feature_sec_tab_l_on wow fadeInUp" data-wow-delay=".0s">
            <div class="flip-card">
              <div class="flip-card-inner">
                <div class="flip-card-front">
                  <img src="<?php echo base_url(); ?>assets/images/Doctor_appoi.png" alt="Avatar" >
                  <h3>Doctor Appointment</h3>
                </div>
                <div class="flip-card-back">
                 <h1><a href="#" data-toggle="modal" data-target="#myModal5"> Learn More </a></h1> 
               </div>
             </div>
           </div>
         </div>
       </div>

       <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="feature_sec_tab_l_on wow fadeInUp" data-wow-delay=".0s">
          <div class="flip-card">
            <div class="flip-card-inner">
              <div class="flip-card-front">
                <img src="<?php echo base_url(); ?>assets/images/Medicine_sec.png" alt="Avatar" >
                <h3>Medicine Management</h3>
              </div>
              <div class="flip-card-back">
               <h1><a href="#" data-toggle="modal" data-target="#myModal6"> Learn More </a></h1> 
             </div>
           </div>
         </div>
       </div>
     </div>


     <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="feature_sec_tab_l_on wow fadeInUp" data-wow-delay=".0s">
        <div class="flip-card">
          <div class="flip-card-inner">
            <div class="flip-card-front">
              <img src="<?php echo base_url(); ?>assets/images/Vital_Sians.png" alt="Avatar" >
              <h3>Vital Signs</h3>
            </div>
            <div class="flip-card-back">
             <h1><a href="#" data-toggle="modal" data-target="#myModal7"> Learn More </a></h1> 
           </div>
         </div>
       </div></div> 
     </div>



     <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="feature_sec_tab_l_on wow fadeInUp" data-wow-delay=".0s">
        <div class="flip-card">
          <div class="flip-card-inner">
            <div class="flip-card-front">
              <img src="<?php echo base_url(); ?>assets/images/photo.png" alt="Avatar" >
              <h3>Photo</h3>
            </div>
            <div class="flip-card-back">
             <h1><a href="#" data-toggle="modal" data-target="#myModal8"> Learn More </a></h1> 
           </div>
         </div>
       </div></div> 
     </div>


     <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="feature_sec_tab_l_on wow fadeInUp" data-wow-delay=".2s">
        <div class="flip-card">
          <div class="flip-card-inner">
            <div class="flip-card-front">
              <img src="<?php echo base_url(); ?>assets/images/music.png" alt="Avatar" >
              <h3>Music</h3>
            </div>
            <div class="flip-card-back">
             <h1><a href="#" data-toggle="modal" data-target="#myModal9"> Learn More </a></h1> 
           </div>
         </div>
       </div></div>
     </div>

     <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="feature_sec_tab_l_on wow fadeInUp" data-wow-delay=".2s">
        <div class="flip-card">
          <div class="flip-card-inner">
            <div class="flip-card-front">
              <img src="<?php echo base_url(); ?>assets/images/game.png" alt="Avatar" >
              <h3>Games</h3>
            </div>
            <div class="flip-card-back">
             <h1><a href="#" data-toggle="modal" data-target="#myModal10"> Learn More </a></h1> 
           </div>
         </div>
       </div></div>
     </div>



     <div class="buy_butt"> <span class="bat_bann vhv_l"> <a href="<?php echo base_url();?>features">Load More</a></span></div>
     <div class="more_butt_sec-ar">
            <!-- <span class="down_aro">
              <a href="#next_sec1">  <img src="assets/images/down_arroe.png">   </a></span> -->
            </div>
          </div>
        </div>
      </div></div>
    </section>
    <section class="member_story_s">
      <div class="container">
        <div id="next_sec1"></div>
        <div class="bigHeading wow fadeInDown  animated">
          <div class="bigText">Stories</div>
          <h2 class="margin-bottom-xxl text-center">Member<strong>Stories</strong></h2>
        </div>
        <?php foreach ($memberStoryData as  $story) { ?>
          <?php if($story->id % 2 == 0){ ?>
            <div class="member_story_s_one">
              <div class="col-md-7">
                <div class="member_story_s_on_l wow fadeInDown  animated">
                  <img src="<?php echo base_url(); ?>assets/images/video_img_one.jpg">
                  <!-- <span class="vid_butt"><a href="#"> <img src="assets/images/play_button.png"></a></span> -->
                </div>
              </div>
              <div class="col-md-5">
                <div class="member_story_s_on_r wow fadeInDown  animated">
                  <h2><?php echo $story->story_desc; ?></h2> 
                  <p><?php echo $story->member_name; ?></p>
                  <!-- <span class="click_plus"><a href=""> <img src="assets/images/plus_icon.png"> </a> </span> -->
                </div>
              </div>
            </div>
          <?php }else{ ?>
            <div class="member_story_s_three">
              <div class="col-md-5">
                <div class="member_story_s_three_r_l wow fadeInDown   animated">
                  <h2><?php echo $story->story_desc; ?></h2> 
                  <p><?php echo $story->member_name; ?></p>
                </div>
              </div>
              <div class="col-md-7">
                <div class="member_story_s_three_r_r wow fadeInDown   animated">
                  <img src="<?php echo base_url(); ?>assets/images/video_img_one.jpg">
                  <!-- <span class="vid_butt"><a href="#"> <img src="assets/images/play_button.png"></a></span> -->
                </div>
              </div>       
            </div>
          <?php } ?>
        <?php } ?>
      <!-- <div class="member_story_s_three">
        <div class="col-md-5">
          <div class="member_story_s_three_r_l wow fadeInDown   animated">
            <h2>“Why Tom love the app”</h2> 
            <p>TOM LANGHOF</p>
          </div>
        </div>
        <div class="col-md-7">
          <div class="member_story_s_three_r_r wow fadeInDown   animated">
            <img src="assets/images/video_img_one.jpg">
            <span class="vid_butt"><a href="#"> <img src="assets/images/play_button.png"></a></span>
          </div>
        </div>       
      </div> -->
    </div>
  </section>
    <?php /* ?>
    <section class="member_story_s">
      <div class="container">
        <div id="next_sec1"></div>
        <div class="bigHeading wow fadeInDown  animated">
          <div class="bigText">Stories</div>
          <h2 class="margin-bottom-xxl text-center">Member<strong>Stories</strong></h2>
        </div>
        <?php foreach ($memberStoryData as  $story) { ?>
          <?php //if($story->id % 2 == 0){ ?>
            <div class="member_story_s_one">
              <div class="col-md-7">
                <div class="member_story_s_on_l wow fadeInDown  animated">
                  <img src="assets/images/video_img_one.jpg">
                  <span class="vid_butt"><a href="#"> <img src="assets/images/play_button.png"></a></span>
                </div>
              </div>
              <div class="col-md-5">
                <div class="member_story_s_on_r wow fadeInDown  animated">
                  <h2><?php echo $story->story_desc; ?></h2> 
                  <p><?php echo $story->member_name; ?></p>
                  <!-- <span class="click_plus"><a href=""> <img src="assets/images/plus_icon.png"> </a> </span> -->
                </div>
              </div>
            </div>
            <?php //}else{ ?>
        <!-- <div class="member_story_s_three">
          <div class="col-md-5">
            <div class="member_story_s_three_r_l wow fadeInDown   animated">
              <h2><?php echo $story->story_desc; ?></h2> 
              <p><?php echo $story->member_name; ?></p>
            </div>
          </div>
          <div class="col-md-7">
            <div class="member_story_s_three_r_r wow fadeInDown   animated">
              <img src="assets/images/video_img_one.jpg">
              <span class="vid_butt"><a href="#"> <img src="assets/images/play_button.png"></a></span>
            </div>
          </div>       
        </div>
      -->
      <?php //} ?>
    <?php } ?>
      <!-- <div class="member_story_s_three">
        <div class="col-md-5">
          <div class="member_story_s_three_r_l wow fadeInDown   animated">
            <h2>“Why Tom love the app”</h2> 
            <p>TOM LANGHOF</p>
          </div>
        </div>
        <div class="col-md-7">
          <div class="member_story_s_three_r_r wow fadeInDown   animated">
            <img src="assets/images/video_img_one.jpg">
            <span class="vid_butt"><a href="#"> <img src="assets/images/play_button.png"></a></span>
          </div>
        </div>       
      </div> -->
    </div>
  </section>
  <?php */ ?>
    <?php /* ?>
    <section class="member_story_s" >
      <div class="container">
        <div id="next_sec1"></div>
        <div class="bigHeading wow fadeInDown  animated">
          <div class="bigText">Stories</div>
          <h2 class="margin-bottom-xxl text-center">Member<strong>Stories</strong></h2>
        </div>
        <div class="member_story_s_one">
          <?php foreach ($memberStoryData as  $story) { ?>
           <?php 
           // $allowed = array("png","PNG","jpg","JPG","JEPG","jpeg", "gif", "GIF");
           // $filename = $story->story_file;
           // $ext = pathinfo($filename, PATHINFO_EXTENSION);
           // if (!in_array($ext, $allowed)) { 
           if($story->id % 2 == 0){ 
             ?>
             <div class="col-md-7">
               <div class="member_story_s_on_l wow fadeInDown  animated">
                <!-- <video width="320" height="240" controls>
                  <source src="<?php echo base_url('uploads/member_stories/'.$story->story_file) ?>" type="video/mp4">
                  </video>   --> 
                  <img src="assets/images/video_img_one.jpg">
                  <span class="vid_butt"><a href="#"> <img src="assets/images/play_button.png"></a></span>
                </div>
              </div>
              <div class="col-md-5">
               <div class="member_story_s_on_r wow fadeInDown  animated">
                <h2><?php echo $story->story_desc; ?></h2> 
                <p><?php echo $story->member_name; ?></p>
                <span class="click_plus"><a href=""> <img src="assets/images/plus_icon.png"> </a> </span>
              </div>
            </div>
          </div>
        <?php }else{ ?>
          <div class="member_story_s_two">
            <div class="col-md-7">
             <div class="member_story_s_two_r_l wow fadeInDown   animated">
               <h2><?php echo $story->story_desc; ?></h2> 
               <p><?php echo $story->member_name; ?></p>
               <span class="click_plus"><a href=""> <img src="assets/images/plus_icon.png"> </a> </span>
             </div>
           </div>
           <div class="col-md-5">
             <div class="member_story_s_two_r_r wow fadeInDown   animated">
               <img src="<?php echo base_url(); ?>uploads/member_stories/<?php echo $story->story_file; ?>">
             </div>
           </div>
         <?php } ?>
       <?php } ?>
       <div class="buy_butt"> <span class="bat_bann"> <a href="<?php echo base_url(); ?>member-stories">Load More</a></span></div>
     </div>
   </div>
 </section>
 <?php */ ?>
</main>  
<!-- Modal -->
<div class="modal box_mod_s fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content fir_box_sn">
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Call</h4>
        </div> -->
        <div class="modal-body">
         <h4 class="modal-title">Call</h4>
         <p>Imagine if you can call someone to assist you with daily needs, like order you a taxi, schedule your doctor appointment, order you food from restaurant, wouldn't that make your life easier? This feature does exactly that, your on demand assistant to help take away your stress and enjoy the life!.</p>
       </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-default cen_b" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal box_mod_s fade" id="myModal2" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content fir_box_sn"> 
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Call</h4>
        </div> -->
        <div class="modal-body">
         <h4 class="modal-title">Transportation</h4>
         <p>Need to book a cab, Soul Tab comes with pre-installed booking applications like Lyft and Uber that allow you to choose the kind of cab you want. You don’t need anyone’s help to call you a cab. Just enter your pickup and dropping point, select the cab, and choose the payment method and you are good to go. Soul Tab makes sure that transportation is available to you at your doorstep.</p>
       </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-default cen_b" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal box_mod_s fade" id="myModal3" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content fir_box_sn">
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Call</h4>
        </div> -->
        <div class="modal-body">
         <h4 class="modal-title">Spirituality</h4>
         <p>Besides all the essential features mentioned before, the Personal section is added with arguably the most relevant and essential feature. The Spirituality section contains videos and audio clips of spiritual guides, psychologists, yoga experts and philosophers who will help you experience the divine energies within you. Meditation and spiritual lessons have shown to be highly effective in relaxing the mind and increasing the brain’s functioning potential.</p>
       </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-default cen_b" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal box_mod_s fade" id="myModal4" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content fir_box_sn">
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Call</h4>
        </div> -->
        <div class="modal-body">
         <h4 class="modal-title">Email</h4>
         <p>Connect to long-lost friends, ex-colleagues, extended family, and all the others who are dear to you through email. It is a great way to share your thoughts, relevant and interesting information, and greet the ones you care for through voice & video messages, greeting cards, photos, and much more. You don't have to worry about spams and scams, Soul Tab uses a smartly designed algorithm that stops such emails from being dropped in your inbox.</p>
       </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-default cen_b" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal box_mod_s fade" id="myModal5" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content fir_box_sn">
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Call</h4>
        </div> -->
        <div class="modal-body">
         <h4 class="modal-title">Doctor Appointment</h4>
         <p>Make sure that you book appointments with your physicians or specialist on time. This feature allows you to save contacts of medical practitioners and book appointments online. You will be regularly reminded when the appointment is scheduled so you don’t miss it.</p>
       </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-default cen_b" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal box_mod_s fade" id="myModal6" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content fir_box_sn"> 
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Call</h4>
        </div> -->
        <div class="modal-body">
          <h4 class="modal-title">Medicine Managements
          </h4>
          <p>Never miss your medicine using the medicine schedule feature. The feature allows you to enter the name of the medicine, day, and time so you know when to take which medicine. A reminder will beep at set time with acknowledgement on tab screen to help you take the medicine on time.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default cen_b" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal box_mod_s fade" id="myModal7" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content fir_box_sn">
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Call</h4>
        </div> -->
        <div class="modal-body">
          <h4 class="modal-title">Vital Signs</h4>
          <p>You can keep an eye at vital signs of your body with just a simple click. This feature will add clinical measurements into a digital journal that you can refer to any time in future to keep track of your health.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default cen_b" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal box_mod_s fade" id="myModal8" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content fir_box_sn">
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Call</h4>
        </div> -->
        <div class="modal-body">
          <h4 class="modal-title">Photo</h4>
          <p>This feature will save all your photos and memories on your tab so you can have a look at them later, put them in a favorite list or share with family with a simple click of button, because there is no limit in clicking pictures.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default cen_b" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


  <!-- Modal -->
  <div class="modal box_mod_s fade" id="myModal9" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content fir_box_sn">
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Call</h4>
        </div> -->
        <div class="modal-body">
         <h4 class="modal-title">Music</h4>
         <p>Listen to melodic music everywhere you go and sing along. More the music, better the health. Soultab comes installed with all your favourite music.</p>
       </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-default cen_b" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal box_mod_s fade" id="myModal10" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content fir_box_sn">
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Call</h4>
        </div> -->
        <div class="modal-body">
          <h4 class="modal-title">Games</h4>
          <p>Play amazing and interesting games on your tab and stay happy. Soultab comes with default games which you can play anytime you want. Games can also stimulate your imagination, boost memory helping you adapt and solve problems.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default cen_b" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <script>
    $(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();
      // Store hash
      var hash = this.hash;
      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>
