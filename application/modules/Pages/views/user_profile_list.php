<style>
	table#sampleTable {
		table-layout: inherit !important;
	}
	.card-body {
		float: left;
		width: 100%;
	}
	.addNote a,
	.btn.btn-primary {
		margin: 0px 0 15px !important;
	}
	.text_heading {
		border-bottom: none !important;
	}
	table#sampleTable th {
		border-bottom: none;
	}
	.table-responsive {
		overflow-x: initial !important;
	}
	.alert-success {
		margin-top: 17px;
		float: left;
		width: 100%;
	}
	.text_heading {
		border-bottom: 0;
		margin: 20px 0 auto;
	}
</style>
<div class=" text_heading">
	<div class="rem_add">
		<a href="<?php echo base_url() ?>user_profile_add" class="btn btn-primary">Add User</a>
	</div>
</div>
<div class="container-fluid">
	<div class="dash-counter users-main my-sts-table">
		<div class="row">
			<div class="col-md-12">
				<div class="">
					<div class="card-body">
						<div class="status-cng"></div>
						<?php if ($this->session->flashdata('success_msg')) { ?>
							<div class="alert alert-success message" id="success_msg">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<p align="center"><?php echo $this->session->flashdata('success_msg'); ?></p>
							</div>
						<?php } ?>
						<div id="success_message"></div>
						<table class="table table-hover tab_comn" id="sampleTable">
							<thead>
								<tr>
									<th>Sr no.</th>
									<th>Username</th>
									<th>Email</th>
									<th>Action</th>
								</tr>
							</thead>

							<tbody>
								<?php
								if (!empty($userprofile_list) && $userprofile_list != '') {
									//_dx($userprofile_list);
									foreach ($userprofile_list as $key => $user) {
								?>
										<tr>
											<td data-label="Sr no."><?php echo ($key + 1); ?></td>
											<td data-label="Username"><?php echo $user['username']; ?></td>
											<td data-label="Email"><?php echo $user['email']; ?></td>
											<td data-label="Action">
												<?php $user_id = $user['id']; ?>
												<a href="<?php echo base_url(); ?>user_profile_edit/<?php echo $user['id']; ?>" class="edit_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;
												<a href="javascript:void(0)" onclick="delete_user('<?php echo $user_id; ?>');"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
											</td>
											<input type="hidden" name="id" value="<?php echo $user_id; ?>">
										</tr>
									<?php }
								} else { ?>
									<td colspan="4">
										<p align="center">No data available</p>
									</td>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>

<script>
	setTimeout(function() {
		$('#success_msg').fadeOut('fast');
	}, 2500);

	$(document).ready(function() {
		setTimeout(function() {
			$('#success_msg').fadeOut('fast');
		}, 2500);
	});

	function delete_user(user_id) {

		var id = user_id;
		var url = "<?php echo base_url() . 'delete_user/'; ?>";
		var url_mix = url + id;
		$.ajax({
			type: 'POST',
			url: url_mix,
			data: {
				id: id
			},
			success: function(resultData) {
				if (parseInt(resultData) != '') {
					//console.log("here");
					var msg = "User has been deleted successfully";
					$("#success_message").html("<div class='alert alert-success' id='hide_msg'>" + msg + "</div>");
					setTimeout(function() {
						$('#hide_msg').fadeOut('fast');
					}, 2500);
					location.reload();
				}
			},
		});
	}
</script>