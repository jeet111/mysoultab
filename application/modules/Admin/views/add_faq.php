<div class="container-fluid">
  <div class="dash-counter">
    <div class="Schedule_main_one">
      <div class="users-main">
        <h2><?php echo (!empty($faq_detail) ? 'Edit' : 'Add'); ?> FAQ</h2>
        <div class="btn_topBack">
          <a class="cancel-btn btn bk_btn" href="<?php echo base_url().'admin/faq_list'; ?>">
            Back
          </a>
        </div>

         <?php
         if (!empty($success)) {
            ?>
            <div class="alert alert-success alert-dismissible">
               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
               <strong>Success!</strong> <?php echo $success; ?>
            </div>
            <?php
         }

         if (!empty($error)) {
            ?>
            <div class="alert alert-danger alert-dismissible">
               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
               <strong>Error!</strong> <?php echo $error; ?>
            </div>
            <?php
         }
         ?>
         


        <form class="form-horizontal" action="<?php echo base_url(); ?>admin/add_faq<?php echo (!empty($faq_detail['id']) ? '/'.$faq_detail['id'] : ''); ?>" method="post" enctype="multipart/form-data" name="AddFaq" id="AddFaq">
            <input type="hidden" name="faq_id" value="<?php echo (!empty($faq_detail['id']) ? $faq_detail['id'] : ''); ?>">
            <div class="form-group">
              <label class="control-label col-sm-2" for="question">Question:</label>
              <div class="col-sm-10">
                <input type="text" value="<?php echo (!empty($faq_detail['question']) ? $faq_detail['question'] : ''); ?>" class="form-control" name="question" id="question" placeholder="Enter question">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="answer">Answer:</label>
              <div class="col-sm-10">
                <textarea class="form-control" name="answer" id="answer" placeholder="Enter answer"><?php echo (!empty($faq_detail['answer']) ? $faq_detail['answer'] : ''); ?></textarea>
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="btnSubmit" value="Submit" class="btn btn-default"><?php echo (!empty($faq_detail) ? 'Update' : 'Submit'); ?></button>
              </div>
            </div>

        </form>
      </div>
    </div>
  </div>
</div>
      
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

      <script>
        $(document).ready(function () {

            $("#AddFaq").validate({
                rules: {
                    question: {
                        required: true
                    },
                    answer: {
                        required: true
                    }
                },
                messages: {
                    question: {
                        required: "This field is required."
                    },
                    answer: {
                        required: "This field is required."
                    }
                },
                submitHandler: function (form) { // for demo
                    form.submit();
                }
            });

        });
      </script>