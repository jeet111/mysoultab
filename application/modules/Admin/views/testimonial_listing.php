<?php

if (isset($staff_permission[18]['add_permission']) == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn">

			<a href="<?php echo base_url(); ?>admin/add_testimonial">

				Add Testimonial

			</a>

		</div>

	</div>

	<?php

} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn">

			<a href="<?php echo base_url(); ?>admin/add_testimonial">

				Add Testimonial

			</a>

		</div>

	</div>

	<?php	

}

?>



<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="card-body">

			<div class="status-cng"></div>

			<?php

			if($this->session->flashdata('success')) {

				$message = $this->session->flashdata('success');

				?>

				<div class="<?php echo $message['class'] ?>"><?php echo $message['message']; ?></div>

			<?php } ?>



			<div id="deletemsg"></div>





			<table class="table table-hover tab_comn " id="sampleTable">

				<thead>

					<tr>

						<th>Sno.</th>

						<th>Name</th>

						<th>Image</th>

						<th>Designation</th>

						<th>description</th>

						<th>Action</th>

					</tr>

				</thead>

				<tbody>

					<?php 

					foreach ($testimonial_listing as $key => $user) 

					{

						?>

						<tr>

							<td><?php echo ($key+1); ?></td>

							<td><?php echo $user['name']; ?></td>

							<td><img style="width:200px;" src="<?php echo base_url().'uploads/'.$user['image']; ?>"></td>

							<td><?php echo $user['designation']; ?></td>

							<td><?php echo $user['description']; ?></td>

							<td>

								<div class="link-del-view">

									<?php

									if (isset($staff_permission[18]['edit_permission']) == 1) {

										?>

										<div class="tooltip-2">

											<a href="admin/add_testimonial/<?php echo $user['id']; ?>">

												<i class="fa fa-edit" aria-hidden="true"></i>

											</a>

											<span class="tooltiptext">Edit</span>

										</div>

										<?php

									} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

										?>

										<div class="tooltip-2">

											<a href="admin/add_testimonial/<?php echo $user['id']; ?>">

												<i class="fa fa-edit" aria-hidden="true"></i>

											</a>

											<span class="tooltiptext">Edit</span>

										</div>

										<?php

									}

									?>

									

									<?php

									if (isset($staff_permission[18]['delete_permission']) == 1) {

										?>

										<div class="tooltip-2">

											<a href="javascript:void(0);" data-id="<?php echo $user['id']; ?>" id="deletetestimonial_<?php echo $user['id']; ?>">

												<i class="fa fa-trash-o" aria-hidden="true"></i>

											</a>

											<span class="tooltiptext">

												Delete

											</span>

											<!-- <a href="javascript:void(0)" onclick="deleteStatus(<?php echo $user['id']; ?>,'admin/delete/user')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span> -->

										</div>

										<?php

									} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

										?>

										<div class="tooltip-2">

											<a href="javascript:void(0);" data-id="<?php echo $user['id']; ?>" id="deletetestimonial_<?php echo $user['id']; ?>">

												<i class="fa fa-trash-o" aria-hidden="true"></i>

											</a>

											<span class="tooltiptext">

												Delete

											</span>

										</div>

										<?php

									}

									?>

									

									

								</div>

							</td>

						</tr>

					<?php } ?>

				</tbody>

			</table>

		</div>

	</div>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>



<script type="text/javascript">







	$(document).on('click','[id^="deletetestimonial_"]',function(){

		var testimonial_id = $(this).attr('data-id');

		if(confirm("Are you sure, you want to delete this testimonial ?")){

			$.ajax({

				type: "POST",

				url: '<?php echo base_url(); ?>admin/delete_testimonial',

				data: 'testimonial_id='+testimonial_id,

            //dataType: "json",

            success: function(data)

            {

            	if(parseInt(data)==1){

            		alert('Testimonial has been deleted!');

            		setTimeout(function(){

            			location.reload();

            		}, 3000);

            	} else {

            		alert('Some internal issue occured!');

            	}

            }

        });

		}

	});

	

	

	

	$(document).on('click','.activation_cls',function(){

		

		var activation_id = $(this).attr('data-activation');

		var user_id = $(this).attr('data-id');	

		$.ajax({

			type: "POST",

			url: '<?php echo base_url(); ?>Admin/Admin_user/activation_status/',

			data: 'activation_id='+activation_id+'&user_id='+user_id,

            //dataType: "json",

            success: function(data)

            {

            	if(data == 1){

            		$("#btton_change_"+user_id).html("<button type='button' data-activation='1' data-id='"+user_id+"' class='btn ybt btn-xs activation_cls'  >Active</button>");

            	}else{

            		$("#btton_change_"+user_id).html("<button type='button' data-activation='0' data-id='"+user_id+"' class='btn xbt btn-xs activation_cls'  >Inactive</button>");

            	}

            }

        });

	});	

</script>