 <style>
  .text_heading {
    border-bottom: 0;
}
.back_reminder .btn.btn-primary {
    margin: 0px 0 15px 0 !important;
}
.back_reminder a {
    margin: 30px 0 !important;
    padding: 10px 28px !important;
    max-width: 126px !important;
    width: 100% !important;
    border-radius: 50px !important;
  }
  .photo-listMain {
    background: #fff;
    padding: 20px;
    box-sizing: border-box;
}
</style>
<div class="text_heading">
        <h3><i class="fa fa-pencil-square-o"></i> Add Event
        </h3>
        <div class="back_reminder"><a href="<?php echo base_url(); ?>show_activities" class="btn btn-primary">Back</a></div>

        <div id="success_message"></div>
</div>
    <section class="content photo-list">
        <div class="photo-listMain">            
           <form id="reminder">
                    <?php /* ?>
                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="alert alert-success message">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <?php echo $this->session->flashdata('success'); ?></div>
                <?php } ?>          
                <?php */?>
                <div class="row"> 
                    <div class="col-sm-6 col-md-6">
                        <div class="form-group">
                            <label for="">Title:<span class="red_star">*</span></label>
                            <input type="text" class="form-control" id="event_title" name="event_title" value="" placeholder="Event Title">
                            <td class="error"><?php echo form_error('event_title'); ?></td>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                        <div class="bootstrap-timepicker">
                            <div class="form-group">
                                <label>Date:<span class="red_star">*</span></label>
                                <div class="input-group date costom_err" id="datetimepicker">  
                                    <input type='text' class="form-control" readonly name="event_date" value="" placeholder="Event Date" />
                                    <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                  </span> 
                                 
                              </div>
                              <td class="error"><?php echo form_error('event_date'); ?></td>

                          </div>
                      </div>             
                  </div>
                            
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Description:<span class="red_star">*</span></label>
                        <textarea class="form-control" id="event_description" name="event_description"  placeholder="Event Description"></textarea>
                        <td class="error"><?php echo form_error('event_description'); ?></td>                            
                    </div>
                </div>                  
            </div>
            <div class="box-footerOne">
                
                <input type="submit" name="submit" class="btn btn-primary" value="Submit">
                <input type="reset" name="cancel" class="btn btn-primary" value="Cancel" onclick="window.location='<?php echo base_url(); ?>show_activities';">
                 
            </div>               
        </form>              
    </div>
</section>

<!--Added by 95 on 5-2-2019 For Doctor category for validation-->



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

<script type="text/javascript">
	$(function () {

$('#datetimepicker').datetimepicker({
    ignoreReadonly: true,
    format: 'YYYY-MM-DD',
    // maxDate: moment()
});
});

  
</script>

<script type="text/javascript">
    $('#reminder').validate({
        rules: {
            event_title: "required",
            event_description:"required",
            event_date:"required",
        },
        messages: {
            event_title: "Please enter event title.",
            event_description:"Please enter event descripiton.",
            event_date:"Please select event date.",
        },
        submitHandler: function(form){
            $.ajax({
                url: '<?php echo base_url();?>Reminder/add_event1',
                type: 'post',
                data: $(form).serialize(),
                success: function(response) {
                    if(response==1){
                        $('#success_message').fadeIn().html('<div class="alert alert-success message"><button type="button" class="close" data-dismiss="alert">x</button>Event added successfully.</div>');
                        setTimeout(function() {
                            $('#success_message').fadeOut("milliseconds");
                        }, 500 );
                    }
                    location.href = '<?php echo base_url();?>show_activities';
                }            
            });
        }
    });
</script>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>


