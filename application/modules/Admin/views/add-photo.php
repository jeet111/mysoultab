<div class="container-fluid">
	<div class="dash-counter">
		<div class="Schedule_main_one">
			<div class="users-main">
				<?php
					$act = 'Add';
					if ($this->uri->segment(2) == 'editphoto') {
						$act = 'Edit';
					}
				echo '<h2>'.$act.' Photo</h2>';
				?>
				<form class="form-horizontal" action="admin/addphoto" method="post" enctype="multipart/form-data" name="AddUsers" id="AddUsers">
					<?php if(empty($users_data)) { ?>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Email:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<input type="text" placeholder="Enter email address" class="form-control" name="email" id="email" > 
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">User Name:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<input type="text" placeholder="Enter user name" class="form-control" name="username" id="username">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Password:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<input type="password" placeholder="Enter password" class="form-control" name="password" id="password" >
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Confirm Password:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<input type="password" placeholder="Enter confirm password" class="form-control" name="cpassword" id="cpassword" >
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Mobile No:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<input type="text" placeholder="Enter mobile number" class="form-control" name="mobile" id="mobile" >
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Profile Image:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<div class="input-group">
								            <span class="input-group-btn">
								                <span class="btn btn-default btn-file">
								                    Browse… <input type="file" id="profile_image" class="form-control" name="profile_image">
								                </span>
								            </span>
											<input type="hidden" class="form-control" name="user_profile_image" id="user_profile_image">
								        </div>
										<img id='img-upload' src="<?php echo base_url().'uploads/profile_images/user.png' ?>">
									</div>
								</div>
							</div>
						</div>
													
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit">
							</div>
						</div>
					<?php } else {
	                    foreach ($users_data as $user) {}
	                	?>
                		<input type="hidden" class="form-control" value="<?php echo $user['u_photo_id']?>" class="form-control" name="id" data-required="true">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">User Name:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<select name="user_id" class="form-control" data-size="7" data-style="btn btn-primary btn-round" title="Single Select" tabindex="-98" required="">
				                        <option style="color: purple" value="">Select Person</option>
				                        <?php if(!empty($user_data)){foreach ($user_data as $key ) {?>
				                        <option style="color:  black" value="<?php echo $key['id']; ?>" <?php if($user['user_id'] == $key['id']){ echo 'selected'; } ?>><?php echo $key['username']; ?></option>
				                          <?php } }?>
				                      </select>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Profile Image:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<div class="input-group">
								            <span class="input-group-btn">
								                <span class="btn btn-default btn-file">
								                    Browse… <input type="file" id="profile_image" class="form-control" name="u_photo">
								                </span>
								            </span>
											<input type="hidden" class="form-control" name="user_profile_image" id="user_profile_image" value="<?php echo $user['u_photo']; ?>">
											<input type="hidden" class="form-control" name="exting_profile_image" id="exting_profile_image" value="<?php echo $user['u_photo']; ?>">
								        </div>
								        <?php if (!empty($user['u_photo'])){ ?>
								        	<img id='img-upload' src="<?php echo base_url().'uploads/photos/'.$user['u_photo'] ?>">
								        <?php } else { ?>
											<img id='img-upload' src="<?php echo base_url().'uploads/photos/user.png' ?>">
								        <?php } ?>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<input type="submit" class="btn btn-default up_but" class="form-control" name="update" value="Update">
								<?php
						 			$admin_url = $this->uri->segment(2);
									$url = 'admin/users/list';
									if ($admin_url == 'adminprofile') {
										$url = 'admin/dashboard';
									}elseif($user['user_role'] == '3'){
										 $url = 'admin/family_member/list';
									}elseif($user['user_role'] == '4'){
										 $url = 'admin/caregiver/list';
									}?>
									<a class="cancel-btn btn" href="<?php echo $url; ?>">
										Back
									</a>
							</div>
						</div>	
					<?php } ?>
				</form>
			</div>
		</div>
	</div>
</div>
