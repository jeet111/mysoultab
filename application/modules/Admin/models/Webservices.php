<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Webservices extends MX_Controller {

  public function __construct(){   

    parent:: __construct();

       if ($_POST) { $this->param = $_POST;}else{ $this->param = json_decode(file_get_contents("php://input"),true); }
        $userData = $this->param['user_language'];
        $GLOBALS['user_lang'] = $userData;
  }
  public function addPhoto(){

       $data = $_POST;
      $object_info = $data;
      $required_parameter = array('user_id');
      $chk_error = check_required_value($required_parameter, $object_info);
      if ($chk_error) {
            $resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));
            //$this->response($resp);
            echo json_encode($resp);
      }
     if(!empty($_FILES['photo_image']['name'])) { 
           $random = $this->generateRandomString(10);
           $ext = pathinfo($_FILES['photo_image']['name'], PATHINFO_EXTENSION);
           $file_name = $random.".".$ext;
           $target_dir = "uploads/photos/";
           $target_file = $target_dir .$file_name;
           if (move_uploaded_file($_FILES["photo_image"]["tmp_name"], $target_file)){
              $photoFile = $file_name;
           }
      }
      $post_data = array
        (
          'user_id'=> $data['user_id'],
          'u_photo_created' => date('Y-m-d h:i:s'),
          'u_photo_status' => 1,  
          'u_photo'        =>$photoFile,   
       );
      $photoId = $this->common_model->addRecords('cp_user_photo', $post_data);
      if($photoId) 
      {
        $photoData = $this->common_model->getSingleRecordById('cp_user_photo', array('u_photo_id' => $photoId));
        $photoData1 = array(
                  'id' => $photoData['u_photo_id'],
                  'user_id' => $photoData['user_id'],
                  'image_url'=>base_url().'uploads/photos/'.$photoData['u_photo']
               );
        $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'photo added successfully', 'response' => $photoData1);
      }else{
       $resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => message(18)));
      }
      //$this->response($resp);
      echo json_encode($resp);
        
  }
   public function generateRandomString($length = 5) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
     public function  orderImage($email_id){
       $this->db->select('attachment_file');
         $this->db->from('cp_emails_attachment');
         $this->db->where('email_id',$email_id);
         $query = $this->db->get();
         $result = $query->result_array();
         foreach($result as $result_details){
          $imageArray[]  = base_url().'uploads/email/'.$result_details['attachment_file'];
         }
         return $imageArray;
    }
    public function send_mail() {
        $data = $_POST;
        $object_info = $data;
        $required_parameter = array('to_email','subject','message','user_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));
            echo json_encode($resp);die;
        }
        $post_data = array
        (
          'to_email'=> $data['to_email'],
          'subject'=> $data['subject'],
          'message'=> $data['message'],
          'user_id'=> $data['user_id'],
          'create_email' => date('Y-m-d'),
          'email_status' => 1,     
        );
        $emailId = $this->common_model->addRecords('cp_emails', $post_data);
        if(!empty($_FILES['attachment_file']['name'])) {  
          $count = count($_FILES['attachment_file']['name']);
              foreach($_FILES['attachment_file']['name'] as $key=>$val){

                   $random = $this->generateRandomString(10);
                   $ext = pathinfo($_FILES['attachment_file']['name'][$key], PATHINFO_EXTENSION);
                   $file_name = $random.".".$ext;
                   $target_dir = "uploads/email/";
                   $target_file = $target_dir .$file_name;
                   //echo $file_name;die;
                   if (move_uploaded_file($_FILES["attachment_file"]["tmp_name"][$key], $target_file)){
                     $productFile = $file_name;
                   }
                  $orderGalleryData = array('attachment_file'=>$productFile,'email_id'=>$emailId,);

                  //print_r("<pre/>");
                  //print_r($orderGalleryData);
                 // die;
                  $this->db->insert('cp_emails_attachment',$orderGalleryData);
              }
        } 

        //echo "test";die;
        if($emailId) 
        {

            $attachment = $this->orderImage($emailId);
           // print_r("<pre/>");
            //print_r($attachment);
           // die;
            $user_name = $data['to_email'];
            $subject = $data['subject'];
            $message = $data['message'];
            $chk_mail= sendEmail1($user_name,$subject,$message,$attachment);
            $emailData = $this->common_model->getSingleRecordById('cp_emails', array('email_id' => $emailId));
           if($emailData['create_email'] == "0000-00-00"){
             $emails_create = $emailData['create_email'];
            }else{
             $emails_create = date("M d, Y", strtotime($emailData['create_email']));
            }

            //print_r("<pre/>");
            //print_r($emailData);
            //die;
           $emailData1 = array(
                  'to_email' => $emailData['to_email'],
                  'subject' => $emailData['subject'],
                  'message' => $emailData['message'],
                  'user_id' => $emailData['user_id'],
                  'create_date' => $emails_create,
                  'images'=>$this->orderImage($emailData['email_id'])
               );

         
           $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'email create successfully', 'response' => $emailData1);
        } else 
        {
          $resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
        }
        echo json_encode($resp);
    }

}
?>