<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class StaffModel extends CI_Model{
    function __construct() {}
	
	public function getTxt()
	{
		return "Hello";
	}

	function getAllRecordsById($table,$conditions)
	{
		$query = $this->db->get_where($table,$conditions);
		return $query->result_array();
	}

	function getSingleRecordById($table,$conditions)
	{
		$query = $this->db->get_where($table,$conditions);
		return $query->row_array();
	}

	public function insertData($table,$datainsert)
	{
		$this->db->insert($table,$datainsert);
		return $this->db->insert_id();
	}

	public function addRecords($table,$datainsert)
	{
		$this->db->insert($table,$datainsert);
		return $this->db->insert_id();
	}
	
	public function updateData($table,$data,$where)
	{
		$this->db->update($table,$data,$where);
		return $this->db->affected_rows();
	}

	public function updateRecords($table,$data,$where)
	{
		$this->db->update($table,$data,$where);
		return $this->db->affected_rows();
	}
	
	public function delete($table,$where)
	{
		$this->db->delete($table,$where);
		return;
	}

	function getAllRecords($table)
	{

		$query = $this->db->get($table);
		return $query->result_array();
	}
	
}
?>