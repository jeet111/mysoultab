	<!-- Home -->
	<div class="home">
		<div class="home_slider_container">
			<div class="owl-carousel owl-theme home_slider">
				<!-- Slide -->
				<div class="slide">
					<div class="background_image" style="background-image:url(assets/images/index.jpg)"></div>
				</div>
				<!-- Slide -->
				<div class="slide">
					<div class="background_image" style="background-image:url(assets/images/index.jpg)"></div>
				</div>

				<!-- Slide -->
				<div class="slide">
					<div class="background_image" style="background-image:url(assets/images/index.jpg)"></div>
				</div>
			</div>
			
			<!-- Home Slider Dots -->
			<div class="home_slider_dots_container">
				<div class="home_container">
						<div class="container">
							<div class="row">
								<div class="col">
									<div class="home_content text-center">
										<div class="home_title">Health Care</div>
										<div class="home-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
										<div class="booking_form_container">
											<form action="#" class="booking_form">
												<div class=" align-items-start justify-content-start">
													<div><!--<button class="booking_button trans_200"></button>-->
													
													
													<a href="javascript:void(0);" class="booking_button trans_200"><i class="fa fa-arrow-circle-o-right"></i>How It Work</a></div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<div class="home_slider_dots">
					<ul id="home_slider_custom_dots" class="home_slider_custom_dots">
						<li class="home_slider_custom_dot active"><span></span></li>
						<li class="home_slider_custom_dot"><span></span></li>
						<li class="home_slider_custom_dot"><span></span></li>
					</ul>
				</div>
			</div>
			<div class="home_slider_botm">
				<div class="home_container">
					<div class="container">
						<div class="row">
							<div class=" align-items-start justify-content-start">
								<div>
									<a href="<?php echo base_url(); ?>login"><button class="booking_button trans_200"><span><i class="fa fa-user"></i></span> User (Person) Login</button></a>
									<!--<a href="<?php echo base_url(); ?>family_member_login"><button class="booking_button trans_200"><span><i class="fa fa-users"></i></span> Family Member Login</button></a>
									<a href="<?php echo base_url(); ?>caregiver_login"><button class="booking_button trans_200"><span><i class="fa fa-user-secret"></i></span> Caregiver Login</button></a>-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
