<?php defined('BASEPATH') or exit('No direct script access allowed');



class Appointment extends MX_Controller
{



	public function __construct()
	{

		parent::__construct();

		$this->load->library('session');

		$this->load->library('form_validation');
		// $this->load->model('pdf');


		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->load->model('Common_model_new');

		$this->load->model('common_model_new');

		$this->load->helper(array('common_helper'));

		$this->load->helper(array('url'));

		$this->load->helper(array('form'));
	}
public function change_status(){
	$id = $this->input->post('cancelAppButton');
	$user_id = $this->session->userdata('logged_in')['id'];
	$getdata = $this->common_model_new->getsingle('doctor_appointments',array('doctor_id' =>  $id,'user_id'=>$user_id));
	
	
		$button_status = $this->common_model_new->updateData("doctor_appointments", array(
			'status'=>0,
		),array("user_id" => $user_id,'doctor_id'=> $id ));
	
		$message="Cancel Appointment Successfully";
		$this->session->set_flashdata('success', $message);
		$error['status'] = $message;
		echo json_encode( $error);
	
}


	/*Created by 95 for show appointment list*/

	public function appointment_list()

	{
		$user_id = $this->session->userdata('logged_in')['id'];
 

		if ($this->session->userdata('logged_in')) {

			$data = array();

			$data['menuactive'] = $this->uri->segment(1);

			$data['page'] = 0;

			$pagecon['per_page'] = 30;

			$Allappointments1 = $this->common_model_new->getAllappointmentsquery($pagecon['per_page'], $data['page']);


			$data['doctor_name_data'] = $this->common_model_new->getAllorderby('cp_doctor', 'doctor_id', 'desc');

			$data['Doctors']  = $this->common_model_new->getAllor('cp_doctor','doctor_name','desc');
			$data['DoctorsSpeciality']  = $this->common_model_new->getAllor('cp_doctor_categories','dc_name','asc');


			$data['Allappointments'] = $Allappointments1['rows'];

			$data['count_total'] = $Allappointments1['num_rows'];

			$data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns", array("btnname" => 'Doctor Appointment', "user_id" => $user_id));
			 $data['appointmentResultNew'] = $this->Common_model_new->getAllList($user_id);
			$data['appointmentResult'] = $this->common_model_new->getAllwhere("doctor_appointments", array("user_id" => $user_id));

			$this->template->set('title', 'Appointment');

			$this->template->load('new_user_dashboard_layout', 'contents', 'appointment_list', $data);
		} else {

			redirect('login');
		}
	}
	public function getResult()
	{
	  $doctorID = $this->input->post('doctorID');


		if(!empty($doctorID) && $doctorID !=''){
			$data['doctor_list']  = $this->common_model_new->getsingle('cp_doctor',array('doctor_id'=>$doctorID));
			foreach ($data as $all_doc) {

				$array[] = array(
					'doctor_id' => $all_doc->doctor_id,
				  'other_doctor_name' => $all_doc->other_doctor_name, 
				  'doctor_name' => $all_doc->doctor_name,
				  'doctor_email' => $all_doc->doctor_email,
				  'doctor_address' => $all_doc->doctor_address,
				  'doctor_mob_no' => $all_doc->doctor_mob_no,
				  'fax_num' => $all_doc->fax_num,
				  'portal' => $all_doc->website

				);  		
		  
			}
			

			$cat_all = $array;
			echo json_encode( $cat_all);
		
		}else{
			$error['status'] = 'empty data sent for update record';
			echo json_encode( $error);
		}
		
	}

	public function add_doctor(){ 
		$data = array();
		$data['menuactive'] = $this->uri->segment(1);
		$doctor_name = $this->input->post('doctor_name');
		$doctor_address = $this->input->post('doctor_address'); 
		$doctor_email = $this->input->post('doctor_email');
		$doctor_phone_number = $this->input->post('doctor_phone_number'); 	
		$doctor_fax = $this->input->post('doctor_fax');
		$doctor_portal = $this->input->post('doctor_portal');
		$speciality = $this->input->post('speciality');

		$array =array(
			'doctor_name' => $doctor_name,
			'doctor_email'=> $doctor_email,
			'doctor_mob_no' => $doctor_phone_number,
			'doctor_email' => $doctor_email,
			'fax_num'=>$doctor_fax,
			'website'=>$doctor_portal,			
			'doctor_address'=>$doctor_address,
			'doctor_created' => date('Y-m-d H:i:s')
		);
		$result = $this->Common_model_new->insertData('cp_doctor',$array);
		$doctor_id = $this->db->insert_id();
		if($result){
			$array =array(
				'dc_id' => $doctor_id,
				'dc_name'=> $speciality,
				'dc_created' => date('Y-m-d H:i:s')
			);
			$result = $this->Common_model_new->insertData('cp_doctor_categories',$array);
			echo 1;
		}else{
			echo 0;
		}
	}
	public function add_appointment(){ 
		$username = $this->session->userdata('logged_in')['name'];
		$userid = $this->session->userdata('logged_in')['id'];	
		$doctor_id = $this->input->post('doctor_id');
		

		$doctor_name = $this->input->post('doctor_name');
		$doctor_address = $this->input->post('doctor_address'); 
		$email = trim($this->input->post('doctor_email'));
		$doctor_phone_number = $this->input->post('doctor_phone_number'); 	
		$doctor_fax = $this->input->post('doctor_fax');
		$doctor_portal = $this->input->post('doctor_portal');
		$app_date = $this->input->post('app_date');
		$set_reminder =$this->input->post('set_reminder');

		$app_time = $this->input->post('app_time');
		$data = $this->common_model_new->getAllwhere("doctor_appointments", array("doctor_id" => $doctor_id, "user_id" => $userid,'dr_appointment_date_id'=>$app_date ,"dr_appointment_time_id"=>$app_time));	
		// _dx($data);
		if($data){
			echo 2;
		}
		else{
			$array =array(
				'doctor_id' =>$doctor_id,
				'user_id'=>$userid ,
				'dr_appointment_date_id'=>$app_date,
				'dr_appointment_time_id'=>$app_time,
				'status'=>'1',
				'appointments_reminder'=>$set_reminder
			);
			// print_r($array);
			$result = $this->Common_model_new->insertData('doctor_appointments',$array);
			$appointment_id = $this->db->insert_id();
// _dx($appointment_id);
			if($result){	
				// echo 1;
				$message="Appointment schedule successfully!";
				$this->session->set_flashdata('success', $message);

				$error['status'] = $message;
				$error['appointmentId']= $appointment_id ;
				echo json_encode( $error);

			
			// /*start email sent code*/
			// $subject ="New Appointment";
			// $message ="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
			// <html xmlns='http://www.w3.org/1999/xhtml'>
			// 	<head>
			// 		<link href='https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i' rel='stylesheet' />
			// 		<link rel='stylesheet' type='text/css' href='https://www.mysoultab.com/assets/styles/caregivermail.css'>
			// 		<title>Caregiver</title>
			// 		<style>
			// 		section {
			// 			width: 40%;
			// 			min-height: 200px;
			// 			height: 200px;
			// 			color: black;
						
			// 			padding: 5%;
			// 			padding-left: 0px;
			// 			float: left;
			// 			background-color: white;
			// 	  }
			// 	  section.two {
			// 			width: 40%;
			// 			min-height: 200px;
			// 			height: 200px;
			// 			color: black;
						
			// 			padding: 5%;
						
			// 			float: left;
			// 			background-color: white;
			// 	  }
			// 	  section.one1{
			// 		  width: 20%;
			// 			min-height: 100px;
			// 			height: 100px;
			// 			color: black;
						
			// 			padding: 5%;
			// 			padding-left:0px;
			// 			float: left;
			// 			background-color: white;
			// 	  }
			// 	  section.two2{
			// 		  width: 60%;
			// 			min-height: 100px;
			// 			height: 100px;
			// 			color: black;
						
			// 			padding: 5%;
			// 			padding-left: 0px;
			// 			float: left;
			// 			background-color: white;
			// 	  }
			// 	  .bottom-line {
			
			// 			border-bottom: 1px solid #000;
			// 			padding-bottom: 20px;
			// 	  }
			// 	  .body {
		  
			// 		  padding:0 !important;
			// 		  margin:0 !important; 
			// 		  display:block !important; 
			// 		  min-width:100% !important; 
			// 		  width:100% !important; 
			// 		  background:#f4f4f4; 
			// 		  -webkit-text-size-adjust:none;
			// 	  }
		  
			// 	  .td.container {
			// 		  width:650px; 
			// 		  min-width:650px; 
			// 		  font-size:0pt; 
			// 		  line-height:0pt; 
			// 		  margin:0; 
			// 		  font-weight:normal; 
			// 		  padding:55px 0px;
			// 	  }
		  
			// 	  td.sec1{
			// 		  padding-bottom: 20px;
			// 	  }
		  
			// 	  td.p30-15 {
			// 		  padding: 25px 30px 25px 30px; 
			// 		  background: #fff;
			// 	  }
			// 	  th.column-top {
			// 		  font-size:0pt; 
			// 		  line-height:0pt; 
			// 		  padding:0; 
			// 		  margin:0; 
			// 		  font-weight:normal; 
			// 		  vertical-align:top;
			// 	  }
			// 	  td.img.m-center {
			// 		  font-size:0pt; 
			// 		  line-height:0pt; 
			// 		  text-align:left;
			// 	  }
			// 	  img.img.m-center {
			// 		  padding-left: 230px;
			// 	  }
			// 	  td.h3.pb20{
			// 		  color:#114490; 
			// 		  font-family:'Noto Sans', Arial,sans-serif; 
			// 		  font-size:24px; 
			// 		  line-height:32px; 
			// 		  text-align:center; 
			// 		  padding-bottom:0px;
			// 		  padding-top: 10px
			// 	  }
			// 	  td.text.pb20{
			// 		  color:#000; 
			// 		  font-family:'Noto Sans', Arial,sans-serif; 
			// 		  font-size:14px; 
			// 		  line-height:26px; 
			// 		  text-align:left; 
			// 		  padding-bottom:10px;
			// 		  font-weight: 700;
			// 	  }
			// 	  td.text.pb201{
			// 		  color:#777777; 
			// 		  font-family:'Noto Sans', Arial,sans-serif; 
			// 		  font-size:14px; 
			// 		  line-height:26px; 
			// 		  text-align:left; 
			// 		  padding-bottom:10px;
			// 	  }
			// 	  td.text.pb202{
			// 		  color:#000; 
			// 		  font-family:'Noto Sans', Arial,sans-serif; 
			// 		  font-size:24px; 
			// 		  line-height:26px; 
			// 		  text-align:left; 
			// 		  padding-bottom:10px;
			// 	  }
			// 	  td.text1.pb201{
			// 		  color:#000; 
			// 		  font-family:'Noto Sans', Arial,sans-serif; 
			// 		  font-size:14px; 
			// 		  line-height:26px; 
			// 		  text-align:center; 
			// 		  padding-top: 5px; 
			// 		  font-weight: 600;
			// 	  }
			// 	  td.text1.pb2012{
			// 		  color:#000; 
			// 		  font-family:'Noto Sans', Arial,sans-serif; 
			// 		  font-size:14px; 
			// 		  line-height:26px; 
			// 		  text-align:center; 
			// 		  padding-top: 5px; 
			// 		  font-weight: 600;
			// 		  padding-bottom: 15px;
			// 	  }
			// 	  td.p30-155{
			// 		  padding: 5px 5px ;
			// 	  }
			// 	  td.sec2{
			// 		  border-collapse: collapse; 
			// 		  border-spacing: 0;
		  
			// 	  }
			// 	  img.sec2{
			// 		  padding: 0px; 
			// 		  margin: 0; 
			// 		  outline: none; 
			// 		  text-decoration: none; 
			// 		  -ms-interpolation-mode: bicubic; 
			// 		  border: 0px solid orange; 
			// 		  border-radius:8px; 
			// 		  display: block;
			// 		  float:left;
			// 		  color: #000000;
			// 	  }
			// 	  td.p30-151{
			// 		  padding: 70px 0px;
			// 	  }
			// 	  td.text-footer1.pb10{
			// 		  color:#999999; 
			// 		  font-family:'Noto Sans', Arial,sans-serif; 
			// 		  font-size:16px; 
			// 		  line-height:20px; 
			// 		  text-align:left; 
			// 		  padding-bottom:0px;
			// 	  }
			// 	  td.text-footer1.pb20{
			// 		  color:#999999; 
			// 		  font-family:'Noto Sans', Arial,sans-serif; 
			// 		  font-size:16px; 
			// 		  line-height:20px; 
			// 		  text-align:left; 
			// 		  padding-bottom:10px;
			// 	  }
			// 	  td.p30-152{
			// 		  padding: 0px 0px;
			// 	  }
			// 	  td.sec3{
			// 		  padding-bottom: 30px;
			// 	  }
			// 	  td.text1.pb20{
			// 		  color:#000; 
			// 		  font-family:'Noto Sans', Arial,sans-serif; 
			// 		  font-size:24px; 
			// 		  line-height:26px; 
			// 		  text-align:left;
			// 	  }
			// 	  td.sec4{
			// 		  font-size:0pt; 
			// 		  line-height:0pt; 
			// 		  text-align:left;
			// 	  }
			// 		</style>
			// 	</head>
   
			// 	<body class='body'>
			// 		<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#f4f4f4'>
			// 			<tr>
			// 				<td align='center' valign='top'>
			// 				<table width='650' border='0' cellspacing='0' cellpadding='0' class='mobile-shell'>
			// 					<tr>
			// 						<td class='td container'>
			// 							<!-- Header -->
			// 							<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			// 							<tr>
			// 								<td class='sec1'>
			// 									<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			// 										<tr>
			// 										<td class='p30-15'>
			// 											<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			// 												<tr>
			// 													<th class='column-top' width='145'>
			// 													<table width='100%' border='0' cellspacing='0' cellpadding='0 ' class='bottom-line'>
			// 														<tr>
			// 															<td class='img m-center'><img src='https://www.mysoultab.com/assets/images/logo.png' width='100' height='50' border='0' alt='' class='img m-center'/></td>
			// 														</tr>
			// 													</table>
			// 													</th>
			// 												</tr>
			// 											</table>
			// 											<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			// 												<tr>
			// 													<td class='h3 pb20'>Welcome to SoulTab</td>
			// 												</tr>
			// 												<tr>
			// 													<td class='text pb20'>Hello,
			// 													</td>
			// 												</tr>
													
			// 												<tr>
			// 													<td class='text pb201'>New Appointment Information
			// 													</td>
			// 												</tr>
			// 												<tr>
			// 													<td class='text pb201'>Here is your caregiver information:</a>
			// 													</td>
			// 												</tr>
			// 												<tr>
			// 													<td class='text1 pb201'>User Name:  $username
			// 													</td>
			// 												</tr>
											
			// 												<tr>
			// 													<td class='text1 pb201'>Appointment Date: $app_date 
			// 													</td>
			// 												</tr>
															
			// 												<tr>
			// 													<td class='text pb201'>Appointment Time:$app_time 
			// 													</td>
			// 												</tr>
			// 											</table>
			// 											<section class='one1'>
			// 												<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			// 													<tr>
			// 													<td class='p30-155' bgcolor='#ffffff'>
			// 														<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			// 															<tr>
			// 																<td align='center' valign='top' class='sec2' style=''><img border='0' vspace='0' hspace='0' class='sec2' src='https://www.mysoultab.com/assets/images/team_two.jpg'alt='D' width='100%'>
			// 																</td>
			// 															</tr>
			// 														</table>
			// 													</td>
			// 													</tr>
			// 												</table>
			// 											</section>
												
			// 											<!-- END Article / Title + Copy + Button -->
			// 											<!-- Footer -->
			// 											<section class='one'>
			// 												<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			// 													<tr>
			// 													<td class='p30-152' bgcolor='#ffffff'>
			// 														<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			// 															<tr>
			// 																<td align='left' class='sec3'>
			// 																<table>
			// 																	<tr>
			// 																		<td class='text1 pb20'>Technical Support</td>
			// 																	</tr>
			// 																</table>
			// 																</td>
			// 															</tr>
			// 															<tr>
			// 																<td class='text-footer1 pb20'>Mail us at:</td>
			// 															</tr>
			// 															<tr>
			// 																<td class='text-footer1 pb20' >info@mysoultab.com</td>
			// 															</tr>
			// 															<tr>
			// 																<td class='text-footer1 pb20'>Web :<a href='#'>mysoultab.com</a></td>
			// 															</tr>
			// 															<tr>
			// 																<td class='text-footer1 pb20'>Tel no : 847 4501055</td>
			// 															</tr>
			// 														</table>
			// 													</td>
			// 													</tr>
			// 												</table>
			// 											</section>
			// 											<section class='two'>
			// 												<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			// 													<tr>
			// 													<td class='p30-152' bgcolor='#ffffff'>
			// 														<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			// 															<tr>
			// 																<td align='left' class='sec3'>
			// 																<table>
			// 																	<tr>
			// 																		<td class='text1 pb20'>Information</td>
			// 																	</tr>
			// 																</table>
			// 																</td>
			// 															</tr>
			// 															<tr>
			// 																<td class='text-footer1 pb20'>Contact us</td>
			// 															</tr>
			// 															<tr>
			// 																<td class='text-footer1 pb20'>About us </td>
			// 															</tr>
			// 															<tr>
			// 																<td class='text-footer1 pb20'>Privacy policy</td>
			// 															</tr>
			// 															<tr>
			// 																<td class='text-footer1 pb20'>Terms of Use</td>
			// 															</tr>
			// 														</table>
			// 													</td>
			// 													</tr>
			// 												</table>
			// 											</section>
			// 											<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			// 												<tr>
			// 													<td class='p30-152'  bgcolor='#ffffff'>
			// 													<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			// 														<tr>
			// 															<td align='center'>
			// 																<table border='0' cellspacing='0' cellpadding='0'>
			// 																<tr>
			// 																	<td class='img' width='55' class='sec4'><a href='#' target='_blank'><img src='https://www.mysoultab.com/assets/images/t8_ico_facebook.jpg' width='38' height='38' border='0' alt='' /></a></td>
			// 																	<td class='img' width='55' class='sec4'><a href='#' target='_blank'><img src='https://www.mysoultab.com/assets/images/t8_ico_twitter.jpg' width='38' height='38' border='0' alt='' /></a></td>
			// 																	<td class='img' width='55' class='sec4'><a href='#' target='_blank'><img src='https://www.mysoultab.com/assets/images/t8_ico_instagram.jpg' width='38' height='38' border='0' alt='' /></a></td>
			// 																	<td class='img' width='38' class='sec4'><a href='#' target='_blank'><img src='https://www.mysoultab.com/assets/images/t8_ico_linkedin.jpg' width='38' height='38' border='0' alt='' /></a></td>
			// 																</tr>
			// 																</table>
			// 															</td>
			// 														</tr>
			// 													</table>
			// 													</td>
			// 												</tr>
			// 											</table>
			// 										</td>
			// 										</tr>
			// 									</table>
			// 								</td>
			// 							</tr>
			// 							</table>
			// 						</td>
			// 					</tr>
			// 				</table>
			// 				</td>
			// 			</tr>
			// 		</table>
			// 	</body>
			// 	</html>";
			// /*end email sent code*/

			// $chk_mail2 = new_send_mail($message, $subject,$email,$imageArray='');
				
				// mail($to,$subject,$message,$headers);
				
			}else{
				echo 0;
			}
		}
			
		
	}


	/*Created by 95 for single appointment delete*/

	public function delete_appointment($doctor_id)

	{

		$user_id = $this->session->userdata('logged_in')['id'];


		$this->common_model_new->delete("doctor_appointments", array("dr_appointment_id" => $doctor_id));

		$this->session->set_flashdata('success', 'Successfully deleted');

		redirect('appointment_list');
	}





	/*Created by 95 for delete multiple appointments*/

	public function appointments_all_del()

	{

		$bodytype = $this->input->post("bodytype");

		$explode = explode(',', $bodytype);



		foreach ($explode as $del) {

			$this->common_model_new->delete("doctor_appointments", array("dr_appointment_id" => $del));
		}

		echo '1';
	}


	public function edit_appointment($id)

	{
	
			$data = array();
			$data['menuactive'] = $this->uri->segment(1);
			$data['EditAppointment'] = $this->common_model_new->getAppointment($id);
			// echo "<pre>";
			// print_r($data);
			$data['AllDoctorsList'] = $this->common_model_new->getAll("cp_doctor");		
		
			$this->template->set('title', 'Appointment');
			$this->template->load('new_user_dashboard_layout', 'contents', 'edit_appointment',$data);
			// echo json_encode($return);

		}

	public function edit_appointment1(){
	
		$appId= $this->input->post('appId');
			$dName  = $this->input->post('dName');
			$appDate = $this->input->post('appDate');
			$appTime  = $this->input->post('appTime');
			$set_reminder = $this->input->post('set_reminder');

			$array = 
			array(
								'doctor_id' => $dName,	
								'user_id' => $this->session->userdata('logged_in')['id'],	
								'dr_appointment_date_id' => $appDate,	
								'dr_appointment_time_id' => $appTime,	
								'appointments_reminder' => $set_reminder,
								'status'=>'1'

			);
			// print_r($array);
			// die;
			$result = 	$this->common_model_new->updateData('doctor_appointments',$array,array('dr_appointment_id'=>$appId));
			if($result){
				echo 1;
			}else{
				echo 0;
			}
	}
	/*Created by 95 for update or edit appointment*/

	// public function edit_appointment($id)

	// {
	// 	if ($this->session->userdata('logged_in')) {
	// 		$data = array();
	// 		$data['menuactive'] = $this->uri->segment(1);

	// 		$data['EditAppointment'] = $this->common_model_new->getAppointment($id);
	// 		$data['AllDoctorsList'] = $this->common_model_new->getAll("cp_doctor");

	// 		// echo "<pre>";
	// 		// print_r($data);
	// 		// $Doctor_dates = $this->common_model_new->getAllDoctorappointmets("dr_available_date", array("avdate_dr_id" => $data['EditAppointment'][0]->doctor_id), "avdate_date", "avdate_date");

	// 		// foreach ($Doctor_dates as $value) {

	// 		// 	$dates[] = array('date' => $value['avdate_date']);
	// 		// }
	// 		// $data['doctorDates'] = $dates;
	// 		// $data['time_slot'] = $this->common_model_new->jointwotablegroupby_order('dr_available_date', 'avdate_id', 'dr_available_time', 'avtime_date_id', 'dr_available_time.avtime_day_slot', array('dr_available_date.avdate_dr_id' => $data['EditAppointment'][0]->avdate_dr_id, 'dr_available_date.avdate_date' => $data['EditAppointment'][0]->avdate_date), '*', 'avtime_day_slot_id', 'asc');

	// 	// $data['slot_time'] = $this->common_model_new->getAllwhere("dr_available_time", array("avtime_date_id" => $data['EditAppointment'][0]->avdate_id, "avtime_day_slot" => $data['EditAppointment'][0]->avtime_day_slot));	
	// 	// echo '<pre>';print_r($data['slot_time']);die;


	// 	if (isset($_POST['submit'])) {

	// 			$appointment_date = $this->input->post('reminder_date');

	// 			$date_id  = $this->input->post('date_id');

	// 			$doctor_id = $this->input->post('doctor_id');

	// 			$selector  = $this->input->post('selector');

	// 			$avail_time = $this->input->post('avail_time');

	// 		$check_time_slot = $this->common_model_new->jointwotablegroupby_order('dr_available_date', 'avdate_id', 'dr_available_time', 'avtime_date_id', 'dr_available_time.avtime_day_slot', array('dr_available_date.avdate_dr_id' => $doctor_id, 'dr_available_date.avdate_date' => $appointment_date), '*', 'avtime_day_slot_id', 'asc');
	// 			if (!empty($check_time_slot)) {
	// 				$available_date = $this->common_model_new->getsingle('dr_available_date', array('avdate_dr_id' => $doctor_id, 'avdate_date' => $this->input->post('reminder_date')));

	// 			if ($this->input->post('remind')) {
	// 					$remind = $this->input->post('remind');
	// 				} else {
	// 					$remind = 0;
	// 				}
	// 				$check_appointment = $this->common_model_new->jointwotable("doctor_appointments", "doctor_id", "dr_available_date", "avdate_dr_id", array("doctor_appointments.doctor_id" => $doctor_id, "dr_available_date.avdate_date" => $this->input->post('reminder_date'), "doctor_appointments.dr_appointment_time_id" => $avail_time), "*");

	// 				//echo $this->db->last_query();die;

	// 				//echo $check_appointment->dr_appointment_date_id;die;

	// 				$time_check =  $this->common_model_new->getsingle("dr_available_time", array("avtime_id" => $avail_time));

	// 				$new_appint = $this->common_model_new->getsingle("doctor_appointments", array("doctor_id" => $doctor_id, "dr_appointment_date_id" => $check_appointment->dr_appointment_date_id, "dr_appointment_time_id" => $time_check->avtime_id));

	// 				//echo '<pre>';print_r($new_appint);die;

	// 				//echo $this->db->last_query();die;

	// 				if (empty($new_appint)) {



	// 					$array = array(

	// 						'doctor_id' => $doctor_id,

	// 						'user_id' => $this->session->userdata('logged_in')['id'],

	// 						'dr_appointment_date_id' => $available_date->avdate_id,

	// 						'dr_appointment_time_id' => $avail_time,

	// 						'appointments_reminder' => $remind

	// 					);



	// 					$where_appointment = array('dr_appointment_id' => $id);



	// 					$this->common_model_new->updateData('doctor_appointments', $array, $where_appointment);





	// 					$this->session->set_flashdata('success', 'Appointment updated successfully.');

	// 					redirect('appointment_list');
	// 				} else {

	// 					$data['message'] = 'This appointment time is already scheduled by a user, please select any other.';
	// 				}
	// 			} else {

	// 				$data['message'] = 'Please select appointment time';
	// 			}
	// 		}



	// 		$this->template->set('title', 'Appointment');

	// 		$this->template->load('new_user_dashboard_layout', 'contents', 'edit_appointment', $data);
	// 	} else {

	// 		redirect('login');
	// 	}
	// }





	public function getCategory($id)
	{

		$id = explode(',', $id);

		$this->db->select('dc_name');

		$this->db->from('cp_doctor_categories');

		$this->db->where_in('dc_id', $id);

		$query = $this->db->get();

		$result = $query->result_array();

		$last_names1 = array_column($result, 'dc_name');

		$result_data = implode(',', $last_names1);

		return $result_data;
	}
	public function sendFax(){
		$data = array();
		$data['username'] = $this->input->post('username');
		$sendAppointmentId = $this->input->post('sendAppointmentId');
		$data['mobile'] = $this->input->post('mobile');
		$data['dr_appointment_date_id'] = $this->input->post('dr_appointment_date_id');
		$data['dr_appointment_time_id'] = $this->input->post('dr_appointment_time_id');
		$data['doctor_name'] = $this->input->post('doctor_name');
		$data['fax_num'] = $this->input->post('fax_num');
		$pdfFilePath = APPPATH.$sendAppointmentId . "." ."pdf";
		header('Content-Type: application/pdf');
		require_once __DIR__ . '../../../../../mpdf/vendor/autoload.php';

		$mpdf = new \Mpdf\Mpdf();
		//$mpdf->AddPage('L');
		$html = $this->load->view('ViwewFileLocation', $data,true); 
		$mpdf->WriteHTML($html);
		
		$mpdf->Output($pdfFilePath,'F');
		

		/*$payStub=new mPDF();
		$payStub->SetTitle('My title');
		$html = $this->load->view('ViwewFileLocation', $data,true); 
		$payStub->WriteHTML($html);
		$payStub->Output($pdfFilePath,"F");
		*/
		/*$this->load->library('m_pdf');
		$mpdf = new m_pdf();
		$obj = $mpdf->load("en-GB-x","A4","","",10,10,10,10,6,3);
		$obj->SetImportUse();
		$obj->AddPage('L');
		$html = $this->load->view('ViwewFileLocation', $data,true); 
		$obj->WriteHTML($html);
		$obj->Output($pdfFilePath,"F");*/
		require_once(APPPATH . '/third_party/interfax/vendor/autoload.php');
		$interfax = new Interfax\Client(['username' => 'jeet.singh', 'password' => '!nt3rFax65']);
		// check balance of $interfax->getBalance();
		exit();
		try {
			$fax = $interfax->deliver(['faxNumber' => '+12242035560', 'file' => $pdfFilePath]);
		
		} catch (Interfax\Exception\RequestException $e) {
			echo $e->getMessage();
			// contains text detail that is available
			echo $e->getStatusCode();
			// the http status code that was received
			throw $e->getWrappedException();
			// The underlying Guzzle exception that was caught by the Interfax Client.
		}

		//$obj->Output($pdfFilePath, "f");
	}
}
