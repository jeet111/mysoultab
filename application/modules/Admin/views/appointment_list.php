<style>
	ul.list_time {

		list-style: none;

		padding: 0px;

		margin: 0px;

	}
</style>
<section class="content weather-bg news_section">
	<!-- setting start -->
	<div class="col-md-12">

		<div id="msg_show"></div>

</section>
<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="row">

			<div class="col-md-12">

				<div class="">

					<div class="card-body">

						<div class="status-cng"></div>

						<?php if ($this->session->flashdata('success')) { ?>

							<div class="alert alert-success message">

								<button type="button" class="close" data-dismiss="alert">x</button>

								<?php echo $this->session->flashdata('success'); ?>
							</div>

						<?php } ?>

						<?php  //echo "<pre>"; print_r($medicineShedule); echo "</pre>"; 
						?>



						<form id="appointment_search" name="appointment_search" method="post" class="form-appointment" action="">

							<div class="row">



								<div class="col-md-4">

									<div class="form-group">

										<label class="control-label" for="">From Date</label>

										<div class="input-group date shedule" id="datepickers">

											<input type='text' class="form-control" readonly id="datepicker" name="datepicker" value="" placeholder="From Date" />

											<div class="input-group-addon">

												<span class="glyphicon glyphicon-calendar"></span>

											</div>



										</div>

									</div>

								</div>



								<div class="col-md-4">

									<div class="form-group">

										<label class="control-label" for="">To Date</label>

										<div class="input-group date shedule" id="datepickers2">

											<input type='text' class="form-control" readonly id="datepicker2" name="datepicker2" value="" placeholder="To Date" />

											<div class="input-group-addon">

												<span class="glyphicon glyphicon-calendar"></span>

											</div>



										</div>

									</div>

								</div>



								<div class="col-md-4">

									<div class="form-group">

										<label class="control-label" for="">Doctor Name</label>

										<input type="text" name="doctor_name" id="doctor_name" value="" class="form-control">

									</div>

								</div>

							</div>



							<div class="row">



								<div class="col-md-4">

									<div class="form-group">

										<label class="control-label" for="">Username</label>

										<input type="text" name="username" id="username" value="" placeholder="Username" class="form-control" />

									</div>

								</div>

								<div class="col-md-6">

									<div class="form-group">

										<label class="control-label" for="">Time Slot</label>

										<ul class="list_time">

											<li>

												<input type="radio" id="f-option" name="selector" value="Morning">

												<label for="f-option">Morning</label>

												<div class="check"></div>

											</li>

											<li>

												<input type="radio" id="s-option" name="selector" value="Noon">

												<label for="s-option">Noon</label>

												<div class="check">
													<div class="inside"></div>
												</div>

											</li>

											<li>

												<input type="radio" id="t-option" name="selector" value="Evening">

												<label for="t-option">Evening</label>

												<div class="check">
													<div class="inside"></div>
												</div>

											</li>



										</ul>

									</div>

								</div>

							</div>



							<div class="row">

								<div class="col-md-6">

									<div class="form-group">

										<input type="submit" name="submit" id="submit" class="btn btn-primary" value="Submit" />

									</div>

								</div>

							</div>

						</form>

						<div id="appointment_search_data">

							<table class="table table-hover tab_comn" id="sampleTable">

								<thead>

									<tr>

										<th>S no.</th>

										<th>User Name</th>

										<th>Doctor Name</th>

										<th>Appointment Date</th>

										<th>Appointment Time</th>

										<th>Appointment Time Slot</th>

										<th>Action</th>

									</tr>

								</thead>

								<tbody>

									<?php



									$count = max(0, ($this->uri->segment(3) - 1) * $per_page1);

									if ($this->uri->segment(3) == '') {

										$i = 1;
									} else {

										$i = $count + 1;
									}

									foreach ($user_appointments as $appointment) {

									?>

										<tr>

											<td><?php echo $i++; ?></td>

											<td><?php echo $appointment['username']; ?></td>

											<td><?php echo $appointment['doctor_name']; ?></td>



											<td><?php echo $appointment['avdate_date']; ?></td>

											<td><?php echo $appointment['avtime_text']; ?></td>



											<td><?php echo $appointment['avtime_day_slot']; ?></td>



											<td>



												<?php

												if (isset($staff_permission[3]['view_permission']) == 1) {

												?>

													<div class="link-del-view">

														<div class="tooltip-2">

															<a href="admin/view_appointment/<?php echo $appointment['dr_appointment_id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

															<span class="tooltiptext">View</span>

														</div>

													</div>

												<?php

												} elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

												?>

													<div class="link-del-view">

														<div class="tooltip-2">

															<a href="admin/view_appointment/<?php echo $appointment['dr_appointment_id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

															<span class="tooltiptext">View</span>

														</div>

													</div>

												<?php

												}

												?>



											</td>

										</tr>

									<?php } ?>

								</tbody>

							</table>

							<?php //echo $links; 
							?>

						</div>





					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).on('click', '.updateStatus', function() {
		var update_status = $(this).attr('data-btn');
		var update_st = $(this).attr('data-st');
		//alert(update_status+'-'+update_st);
		$.ajax({
			url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
			type: "POST",
			data: {
				'btn': update_status
			},
			dataType: "json",
			success: function(data) {
				setTimeout(function() {
					$("#msg_show").html("<div class='alert alert-success'>" + data.status + "</div>");
					location.reload();

				}, 1500);
			}
		});
	});
</script>

<script>
	$(function() {



		$('#datepickers').datetimepicker({

			ignoreReadonly: true,

			format: 'YYYY-MM-DD',



		});

	});

	$(function() {



		$('#datepickers2').datetimepicker({

			ignoreReadonly: true,

			format: 'YYYY-MM-DD',



		});

	});







	$(document).on('submit', '#appointment_search', function(e) {

		event.preventDefault();

		$.ajax

		({

			url: "<?php echo base_url(); ?>Admin/Doctor/appointment_search",

			type: "POST",

			data: $('#appointment_search').serializeArray(),

			//dataType: "json",

			success: function(response)

			{

				$('#appointment_search_data').html(response);

			}

		});

	});
</script>