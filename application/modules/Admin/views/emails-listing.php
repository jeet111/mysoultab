<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="row">

			<div class="col-md-12">

				<div class="">

					<div class="card-body">

						<div class="status-cng"></div>

						<?php

						if($this->session->flashdata('success')) {

							$message = $this->session->flashdata('success');

							?>

							<div class="<?php echo $message['class'] ?>"><?php echo $message['message']; ?></div>

						<?php } ?>



						<div id="deletemsg"></div>



						<table class="table table-hover tab_comn" id="sampleTable" width="100%">

							<thead>

								<tr>

									<th>S no.</th>



									<th>From Email</th>

									<th>Subject</th>



									<th>Action</th>

								</tr>

							</thead>

							<tbody>

								<?php  foreach ($emails_data as $key => $email) {

									$data['users'] = $this->login_model->getSingleRecordById('cp_users',array('id' =>$email['user_id']));



									?>

									<tr>

										<td><?php echo ($key+1); ?></td>



										<td><?php echo $data['users']['email']; ?></td>

										<td><?php echo $email['subject']; ?></td>



										<td>

											<div class="link-del-view">

												<?php

												if (isset($staff_permission[2]['view_permission']) == 1) {

													?>

													<div class="tooltip-2">

														<a href="admin/view/email/<?php echo $email['email_id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

														<span class="tooltiptext">View</span>

													</div>

													<?php

												} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

													?>

													<div class="tooltip-2">

														<a href="admin/view/email/<?php echo $email['email_id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

														<span class="tooltiptext">View</span>

													</div>

													<?php

												}

												?>

												

												<?php

												if (isset($staff_permission[2]['delete_permission']) == 1) {

													?>

													<div class="tooltip-2">

														<a href="javascript:void(0);" data-id="<?php echo $email['email_id']; ?>" id="deletemoviecat_<?php echo $email['email_id']; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

														<!-- <a href="javascript:void(0)" onclick="deleteStatus(<?php echo $email['email_id']; ?>,'admin/delete/email')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span> -->

													</div>

													<?php

												} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

													?>

													<div class="tooltip-2">

														<a href="javascript:void(0);" data-id="<?php echo $email['email_id']; ?>" id="deletemoviecat_<?php echo $email['email_id']; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

														<!-- <a href="javascript:void(0)" onclick="deleteStatus(<?php echo $email['email_id']; ?>,'admin/delete/email')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span> -->

													</div>

													<?php

												}

												?>

												

												<?php /* if($email['email_status'] == 1) { ?>

												<div class="tooltip-2"><a href="javascript:void(0);" onclick="return updateStatus(<?php echo $email['email_id']; ?>, 'admin/email/status');"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>

													<span class="tooltiptext">Active</span>

												</div>

												<?php }else { ?>

												<div class="tooltip-2"><a href="javascript:void(0);" onclick="return updateStatus(<?php echo $email['email_id']; ?>, 'admin/email/status');"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>

													<span class="tooltiptext">Deactive</span>

												</div>

											<?php } */ ?>

										</div>

									</td>

								</tr>

							<?php } ?>

						</tbody>

					</table>

				</div>

			</div>

		</div>

	</div>

</div>

</div>







<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>





<script type="text/javascript">

	$(document).on('click','[id^="deletemoviecat_"]',function(){







		var delete_email = $(this).attr('data-id');

		if(confirm("Are you sure, you want to delete this email ?")){

			$.ajax({

				type: "POST",

				url: '<?php echo base_url(); ?>Admin/Admin_user/deleteemailRecords/',

				data: 'id='+delete_email,

            //dataType: "json",

            success: function(data)

            {





            	setTimeout(function(){

            		$("#deletemsg").html("<div class='alert alert-success'>Deleted Successfully.</div>");

            		location.reload();

//window.location.href = email_url+"email_list";

}, 1000);



            }

        });

			$(this).parents(".card_one").animate({ backgroundColor: "#fbc7c7" }, "fast")

			.animate({ opacity: "hide" }, "slow");

		}

	});

</script>