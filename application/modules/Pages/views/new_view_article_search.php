
<aside class="right-side new-music artic_list">
	<div class="searchbar-wrap">
 		<div class="src_sbmit">
			<input autocomplete="off" class="form-control" id="article_search" name="article_search" placeholder="Search Article" value="<?php echo $segment2; ?>" type="text">
			 <button type="submit" id="btn_music" value="Submit"><i class="fa fa-search"></i></button>				
		</div>		
	</div>
	<section class="content-header no-margin">

		<?php if($type=='popular'){ ?>

			<h1 class="text-center"><i class="fa fa-list"></i> All Popular Articles <div class="rem_add">
			<?php }elseif($type=='recent'){ ?>
			<h1 class="text-center"><i class="fa fa-list"></i> All Latest Articles <div class="rem_add">
			<?php }elseif($type=='my'){ ?>
			<h1 class="text-center"><i class="fa fa-list"></i> All My Articles <div class="rem_add">
			<?php }elseif($type=='favorite'){ ?>
			<h1 class="text-center"><i class="fa fa-list"></i> All Favorite Articles <div class="rem_add">
			<?php }else{ ?>
				<h1 class="text-center"><i class="fa fa-list"></i> All Article Categories <div class="rem_add">
			<?php } ?>
        	<a href="javascript:window.history.back()" class="btn btn-primary">Back</a></div></h1>
    </section>

    
<?php 
if(!empty($popular_array) || !empty($recent_array) || !empty($myarticle) || !empty($UserFavarticle)){
?>

 <?php if($type=='popular'){ ?>   


	<section class="doc_cate">
		<div class="row">
			<div class="col-md-12">
				<div class="bx_leftScrollmain mCustomScrollbar">
					<div class="bx_leftScroll">
						<div id="combine_tab">						    
						    <div class="box_viewFull">
						    	<div class="row">
						    		
						    	<?php foreach ($popular_array as $popular) { ?>

						    		<div class="col-md-4">	    	
					                	<div class="rail-thumb">
					                   		<a class="dropdown-item" href="#">

					                   			<img src="<?php echo base_url().'uploads/articles/'.$popular['article_image'];?>" class="mCS_img_loaded">

					                   			
					                   		</a>
							    		</div>
							    		<div class="rail-content" title="">
							    			<a href="<?php echo base_url();?>read_article/<?php echo $popular['article_id']; ?>"><?php echo $popular['article_title']; ?></a>						    				
							    		</div>					                
						    		</div>

						    	<?php } ?>
						    		
						    	</div>
					    	</div>
						</div>						
					</div>
				</div>
			</div>					
		</div>	
	</section>





<?php }elseif($type=='recent'){ ?>

<section class="doc_cate">
		<div class="row">
			<div class="col-md-12">
				<div class="bx_leftScrollmain mCustomScrollbar">
					<div class="bx_leftScroll">
						<div id="combine_tab">						    
						    <div class="box_viewFull">
						    	<div class="row">
						    		
						    	<?php foreach ($recent_array as $recent) { ?>

						    		<div class="col-md-4">	    	
					                	<div class="rail-thumb">
					                   		<a class="dropdown-item" href="#">

					                   			<img src="<?php echo base_url().'uploads/articles/'.$recent['article_image'];?>" class="mCS_img_loaded">

					                   			
					                   		</a>
							    		</div>
							    		<div class="rail-content" title="">
							    			<a href="<?php echo base_url();?>read_article/<?php echo $recent['article_id']; ?>"><?php echo $recent['article_title']; ?></a>						    				
							    		</div>					                
						    		</div>

						    	<?php } ?>
						    		
						    	</div>
					    	</div>
						</div>						
					</div>
				</div>
			</div>					
		</div>	
	</section>

<?php }elseif($type=='my'){ ?>

<section class="doc_cate">
		<div class="row">
			<div class="col-md-12">
				<div class="bx_leftScrollmain mCustomScrollbar">
					<div class="bx_leftScroll">
						<div id="combine_tab">						    
						    <div class="box_viewFull">
						    	<div class="row">
						    		
						    	<?php foreach ($UserArticleData as $my) { ?>

						    		<div class="col-md-4">	    	
					                	<div class="rail-thumb">
					                   		<a class="dropdown-item" href="#">

					                   			<img src="<?php echo base_url().'uploads/articles/'.$my['article_image'];?>" class="mCS_img_loaded">

					                   			
					                   		</a>
							    		</div>
							    		<div class="rail-content" title="">
							    			<a href="<?php echo base_url();?>read_article/<?php echo $my['article_id']; ?>"><?php echo $my['article_title']; ?></a>						    				
							    		</div>					                
						    		</div>

						    	<?php } ?>
						    		
						    	</div>
					    	</div>
						</div>						
					</div>
				</div>
			</div>					
		</div>	
	</section>


<?php }elseif($type=='favorite'){ ?>

<section class="doc_cate">
		<div class="row">
			<div class="col-md-12">
				<div class="bx_leftScrollmain mCustomScrollbar">
					<div class="bx_leftScroll">
						<div id="combine_tab">						    
						    <div class="box_viewFull">
						    	<div class="row">
						    		
						    	<?php foreach ($UserFavArticl as $Favourite) { ?>

						    		<div class="col-md-4">	    	
					                	<div class="rail-thumb">
					                   		<a class="dropdown-item" href="#">

					                   			<img src="<?php echo base_url().'uploads/articles/'.$Favourite['article_image'];?>" class="mCS_img_loaded">

					                   			
					                   		</a>
							    		</div>
							    		<div class="rail-content" title="">
							    			<a href="<?php echo base_url();?>read_article/<?php echo $Favourite['article_id']; ?>"><?php echo $Favourite['article_title']; ?></a>						    				
							    		</div>					                
						    		</div>

						    	<?php } ?>
						    		
						    	</div>
					    	</div>
						</div>						
					</div>
				</div>
			</div>					
		</div>	
	</section>

<?php }else{ ?>

<section class="doc_cate">
		<div class="row">
			<div class="col-md-12">
				<div class="bx_leftScrollmain mCustomScrollbar">
					<div class="bx_leftScroll">
						<div id="combine_tab">						    
						    <div class="box_viewFull">
						    	<div class="row">
						    		
						    	<?php foreach ($category_array as $category) { ?>

						    		<div class="col-md-4">	    	
					                	<div class="rail-thumb">
					                   		<a class="dropdown-item" href="#">

					                   			<img src="<?php echo base_url().'uploads/articles/category_icon/'.$category->article_category_icon;?>" class="mCS_img_loaded">

					                   			
					                   		</a>
							    		</div>
							    		<div class="rail-content" title="">
							    			<a href="<?php echo base_url();?>category_articles/<?php echo base64_encode($category->article_category_id); ?>">
							    				<?php echo $category->article_category_name; ?>
							    					
							    				</a>						    				
							    		</div>					                
						    		</div>

						    	<?php } ?>
						    		
						    	</div>
					    	</div>
						</div>						
					</div>
				</div>
			</div>					
		</div>	
	</section>

<?php } ?>
<?php }else{ ?>

						<div class="photo-list-empty">
		    	<span class="blnk_photo"><i class="fa fa-picture-o" aria-hidden="true"></i></span>
		    	<h4>Articles not found</h4>
		    </div>

					<?php } ?>
</aside>		


<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->

<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightslider.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightslider.js"></script> 
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.css">
<script src="<?php echo base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>

<script>	
    	 $(document).ready(function() {
			$(".content-slider").lightSlider({
                loop:true,
                keyPress:true
            });
            $('#image-gallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:9,
                slideMargin: 0,    
                speed:500,
                auto:true,
                loop:true,
                onSliderLoad: function() {
                    $('#image-gallery').removeClass('cS-hidden');
                }  
            });
		});
			$(document).on('click','#btn_music',function(e){ 
	var article_search = $("#article_search").val();
	var segment1 = '<?php echo $this->uri->segment('2'); ?>';
	var segment2 = '<?php echo $this->uri->segment('3'); ?>';
	
window.location = "<?php echo base_url(); ?>view_All_articles/"+segment1+'/'+segment2+'/'+article_search;	
	});
</script>
<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>