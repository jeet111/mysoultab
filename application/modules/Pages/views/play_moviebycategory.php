<?php if($type=='popular'){ ?>

<!-- By popular -->

	<section class="doc_cate">
		<div class="row">
			<div class="col-md-9">
				<div class="bx_leftScrollmain mCustomScrollbar">
				<div class="bx_leftScroll">
					<div class="bx_viewAll">

						

						<div class="inside-pageinfo-wrap" >

						
							<div class="bx_moviMain">
								<video  controls autoplay style="width:100%; height:auto;" >
					        		<source id="moivedata" data-myval="" src="<?php echo base_url().'uploads/movie/'.$play_movie1->playmovie_file; ?>" type="video/mp4">
					          		
					        	</video>
							</div>
							<div class="bx_movName">
								<h5><?php echo $play_movie1->playmovie_title; ?></h5>
								<span><?php echo $play_movie1->playmovie_views; ?> views</span>
							</div>
							<div class="mov_dec">
								<h4>Description</h4>
								<p><?php echo $play_movie1->playmovie_desc; ?></p>
							</div>											
						</div>							
					</div>	
				</div>
			</div>
			</div>
			<div class="col-md-3 mrg_non">
				<div class="bx_right bx_movRight">					
					<div class="up_box">
						<h5 class="pull-left">Up next</h5>
						<div class="back-info">
							<a href="javascript:window.history.back()"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</a>
						</div>
					</div>
					<div class="listing_sng mCustomScrollbar">
						<ul class="list_songs">

							<?php foreach ($popular_array as $popular) { ?>

							<li>
								
								<div class="queue-movie">
									<div class="mov_qu">
										<a href="javascript:void(0);" onclick="getId('<?php echo $popular->movie_id; ?>')">
									<img alt="" src="<?php echo base_url().'uploads/movie/image/'.$popular->movie_image; ?>">
									<span class="iconplay"><i class="fa fa-play-circle"></i></span>
								</a>
								
									</div>
									<div class="nam_view">
										
										<h5 >
											<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($popular->movie_id); ?>">
											<?php echo $popular->movie_title; ?>
										</a>
										</h5>
										
										<span><?php echo $popular->movie_view; ?> Views</span>
										
									</div>
								</div>							
							</a>
							</li>
						<?php } ?>
							
							
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php }elseif ($type=='recent'){?>

<!-- By recent  -->
<section class="doc_cate">
		<div class="row">
			<div class="col-md-9">
				<div class="bx_leftScrollmain mCustomScrollbar">
				<div class="bx_leftScroll">
					<div class="bx_viewAll">

						

						<div class="inside-pageinfo-wrap" >

						
							<div class="bx_moviMain">
								<video  controls autoplay style="width:100%; height:auto;" >
					        		<source id="moivedata" data-myval="" src="<?php echo base_url().'uploads/movie/'.$play_movie1->playmovie_file; ?>" type="video/mp4">
					          		
					        	</video>
							</div>
							<div class="bx_movName">
								<h5><?php echo $play_movie1->playmovie_title; ?></h5>
								<span><?php echo $play_movie1->playmovie_views; ?> views</span>
							</div>
							<div class="mov_dec">
								<h4>Description</h4>
								<p><?php echo $play_movie1->playmovie_desc; ?></p>
							</div>											
						</div>							
					</div>	
				</div>
			</div>
			</div>
			<div class="col-md-3 mrg_non">
				<div class="bx_right bx_movRight">					
					<div class="up_box">
						<h5 class="pull-left">Up next</h5>
						<div class="back-info">
							<a href="javascript:window.history.back()"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</a>
						</div>
					</div>
					<div class="listing_sng mCustomScrollbar">
						<ul class="list_songs">

							<?php foreach ($recent_array as $recent) { ?>

							<li>
								
								<div class="queue-movie">
									<div class="mov_qu">
									<a href="javascript:void(0);" onclick="getId('<?php echo $recent->movie_id; ?>')">
									<img alt="" src="<?php echo base_url().'uploads/movie/image/'.$recent->movie_image; ?>">
									<span class="iconplay"><i class="fa fa-play-circle"></i></span>
								</a>
								
									</div>
									<div class="nam_view">
										
										<h5 >
											<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($recent->movie_id); ?>">
											<?php echo $recent->movie_title; ?>
										</a>
										</h5>
										
										<span><?php echo $recent->movie_view; ?> Views</span>
										
									</div>
								</div>
								</a>							

							</li>
						<?php } ?>
							
							
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php }elseif ($type=='my') { ?>

<!-- By my movies -->
<section class="doc_cate">
		<div class="row">
			<div class="col-md-9">
				<div class="bx_leftScrollmain mCustomScrollbar">
				<div class="bx_leftScroll">
					<div class="bx_viewAll">

						

						<div class="inside-pageinfo-wrap" >

						
							<div class="bx_moviMain">
								<video  controls autoplay style="width:100%; height:auto;" >
					        		<source id="moivedata" data-myval="" src="<?php echo base_url().'uploads/movie/'.$play_movie1->playmovie_file; ?>" type="video/mp4">
					          		
					        	</video>
							</div>
							<div class="bx_movName">
								<h5><?php echo $play_movie1->playmovie_title; ?></h5>
								<span><?php echo $play_movie1->playmovie_views; ?> views</span>
							</div>
							<div class="mov_dec">
								<h4>Description</h4>
								<p><?php echo $play_movie1->playmovie_desc; ?></p>
							</div>											
						</div>							
					</div>	
				</div>
			</div>
			</div>
			<div class="col-md-3 mrg_non">
				<div class="bx_right bx_movRight">					
					<div class="up_box">
						<h5 class="pull-left">Up next</h5>
						<div class="back-info">
							<a href="javascript:window.history.back()"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</a>
						</div>
					</div>
					<div class="listing_sng mCustomScrollbar">
						<ul class="list_songs">

							<?php foreach ($UserMovieData as $mymovie) { ?>

							<li>
								
								<div class="queue-movie">
									<div class="mov_qu">

									<a href="javascript:void(0);" onclick="getId('<?php echo $mymovie->movie_id; ?>')">
									<img alt="" src="<?php echo base_url().'uploads/movie/image/'.$mymovie->movie_image; ?>">
									<span class="iconplay"><i class="fa fa-play-circle"></i></span>
								</a>
								
									</div>
									<div class="nam_view">
										
										<h5 >
											<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($mymovie->movie_id); ?>">
											<?php echo $mymovie->movie_title; ?>
										</a>
										</h5>
										
										<span><?php echo $mymovie->movie_view; ?> Views</span>
										
									</div>
								</div>							
							</a>
							</li>
						<?php } ?>
							
							
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php }elseif ($type=='favorite') {?>

<!-- By favorite -->

<section class="doc_cate">
		<div class="row">
			<div class="col-md-9">
				<div class="bx_leftScrollmain mCustomScrollbar">
				<div class="bx_leftScroll">
					<div class="bx_viewAll">

						

						<div class="inside-pageinfo-wrap" >

						
							<div class="bx_moviMain">
								<video  controls autoplay style="width:100%; height:auto;" >
					        		<source id="moivedata" data-myval="" src="<?php echo base_url().'uploads/movie/'.$play_movie1->playmovie_file; ?>" type="video/mp4">
					          		
					        	</video>
							</div>
							<div class="bx_movName">
								<h5><?php echo $play_movie1->playmovie_title; ?></h5>
								<span><?php echo $play_movie1->playmovie_views; ?> views</span>
							</div>
							<div class="mov_dec">
								<h4>Description</h4>
								<p><?php echo $play_movie1->playmovie_desc; ?></p>
							</div>											
						</div>							
					</div>	
				</div>
			</div>
			</div>
			<div class="col-md-3 mrg_non">
				<div class="bx_right bx_movRight">					
					<div class="up_box">
						<h5 class="pull-left">Up next</h5>
						<div class="back-info">
							<a href="javascript:window.history.back()"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</a>
						</div>
					</div>
					<div class="listing_sng mCustomScrollbar">
						<ul class="list_songs">

							<?php foreach ($UserFavMovie as $FavMovie) { ?>

							<li>
								
								<div class="queue-movie">
									<div class="mov_qu">
									<a href="javascript:void(0);" onclick="getId('<?php echo $FavMovie->movie_id; ?>')">
									<img alt="" src="<?php echo base_url().'uploads/movie/image/'.$FavMovie->movie_image; ?>">
									<span class="iconplay"><i class="fa fa-play-circle"></i></span>
								</a>
								
									</div>
									<div class="nam_view">
										
										<h5 >

											<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($FavMovie->movie_id); ?>">
											<?php echo $FavMovie->movie_title; ?>
										</a>
										</h5>
										
										<span><?php echo $FavMovie->movie_view; ?> Views</span>
										
									</div>
								</div>
								</a>							

							</li>
						<?php } ?>
							
							
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php }else{ ?>


<!-- By Categories  -->

<section class="doc_cate">
		<div class="row">
			<div class="col-md-9">
				<div class="bx_leftScrollmain mCustomScrollbar">
				<div class="bx_leftScroll">
					<div class="bx_viewAll">

						

						<div class="inside-pageinfo-wrap" >

						
							<div class="bx_moviMain">
								<video  controls autoplay style="width:100%; height:auto;" >
					        		<source id="moivedata" data-myval="" src="<?php echo base_url().'uploads/movie/'.$play_movie1->playmovie_file; ?>" type="video/mp4">
					          		
					        	</video>
							</div>
							<div class="bx_movName">
								<h5><?php echo $play_movie1->playmovie_title; ?></h5>
								<span><?php echo $play_movie1->playmovie_views; ?> views</span>
							</div>
							<div class="mov_dec">
								<h4>Description</h4>
								<p><?php echo $play_movie1->playmovie_desc; ?></p>
							</div>											
						</div>							
					</div>	
				</div>
			</div>
			</div>
			<div class="col-md-3 mrg_non">
				<div class="bx_right bx_movRight">					
					<div class="up_box">
						<h5 class="pull-left">Up next</h5>
						<div class="back-info">
							<a href="javascript:window.history.back()"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</a>
						</div>
					</div>
					<div class="listing_sng mCustomScrollbar">
						<ul class="list_songs">

							<?php foreach ($MoviesBycategories as $movie) { ?>

							<li>
								
								<div class="queue-movie">
									<div class="mov_qu">

									<a href="javascript:void(0);" onclick="getId('<?php echo $movie->movie_id; ?>')">
									<img alt="" src="<?php echo base_url().'uploads/movie/image/'.$movie->movie_image; ?>">
									<span class="iconplay"><i class="fa fa-play-circle"></i></span>
								</a>
								
									</div>
									<div class="nam_view">
										
										<h5 >
											<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($movie->movie_id); ?>">
											<?php echo $movie->movie_title; ?>

										</a>
										</h5>
										
										<span><?php echo $movie->movie_view; ?> Views</span>
										
									</div>
								</div>
								</a>							

							</li>
						<?php } ?>
							
							
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>


<?php } ?>
     
  

<!-- <div class="bx_moviMain">
	<video  controls autoplay style="width:100%; height:auto;" >
		<source id="moivedata" data-myval="" src="<?php echo base_url().'uploads/movie/'.$play_movie1->playmovie_file; ?>" type="video/mp4">
		
	</video>
</div>
<div class="bx_movName">
	<h5><?php echo $play_movie1->playmovie_title; ?></h5>
	<span><?php echo $play_movie1->playmovie_views; ?> views</span>
</div>
<div class="mov_dec">
	<h4>Description</h4>
	<p><?php echo $play_movie1->playmovie_desc; ?></p>
</div> -->