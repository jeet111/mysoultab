<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Dailyroutine extends MX_Controller {



	public function __construct() {

		parent:: __construct();

		$this->load->library('session');

		$this->load->library('form_validation');

		$this->load->model('activities_model');

		$this->load->model('Common_model_new');

		$this->load->model('common_model_new');

		$this->load->model('movie_model');

		$this->load->helper(array('common_helper'));

		$this->load->helper(array('url'));

		$this->load->helper(array('form'));

		not_login();

	}

  

public function dailyroutine(){
    $data['menuactive'] = '';
    $checkLogin = $this->session->userdata('logged_in');
    $user_id = $this->session->userdata('logged_in')['id'];
    
    if(!empty($checkLogin))
    {
      $data['menuactive'] = $this->uri->segment(1);

		$submit = isset($_POST) ? $_POST : '';
		
		if(!empty($submit) && $submit !=''){
			
				$check_dailyroutine = $this->Common_model_new->getsingle("cp_dailyroutine",array("created" => date('Y-m-d'),"user_id" => $this->session->userdata('logged_in')['id']));
				if(!empty($check_dailyroutine) && $check_dailyroutine!=''){

					$morning = $this->input->post('morning');
					$noon = $this->input->post('noon');
					$evening = $this->input->post('evening');
					$dinner = $this->input->post('dinner');
					$bedtime = $this->input->post('bedtime');	
					
					$array =array(
						'user_id' => $this->session->userdata('logged_in')['id'],
						'morning' => implode(",", $morning),
						'noon' => implode(",", $noon),
						'evening' => implode(",", $evening),
						'dinner' => implode(",", $dinner),
						'bedtime' =>  implode(",", $bedtime),
						'created' => date('Y-m-d')
					);

					unset($_POST);
					$chk_msg= "Today dailyroutine is already submitted so not allowed";
					$this->session->set_flashdata('failed',$chk_msg);

//					if(empty($morning) && empty($noon) && empty($evening) && empty($dinner) && empty($bedtime)){
//						unset($_POST);
//						$chk_msg= "0 dailyroutine is not allowed";
//						$this->session->set_flashdata('failed',$chk_msg);
//					}else{
//						//$update = $this->Common_model_new->updateData('cp_dailyroutine',$array,array('id' => $check_dailyroutine->id));
//						if(!empty($update) && $update !=''){
//
//							//_dx($update);
//
//							//unset($_POST);
//							$update_msg= "Dailyroutine updated successfully";
//							$this->session->set_flashdata('success',$update_msg);
//						}
//					}
					
				}else{
					$array =array(
					'user_id' => $this->session->userdata('logged_in')['id'],
					'morning' => implode(",", $this->input->post('morning')),
					'noon' => implode(",", $this->input->post('noon')),
					'evening' => implode(",", $this->input->post('evening')),
					'dinner' => implode(",", $this->input->post('dinner')),
					'bedtime' => implode(",", $this->input->post('bedtime')),
					'created' => date('Y-m-d'));
			
					$insert = $this->Common_model_new->insertData('cp_dailyroutine',$array);
					if(!empty($insert) && $insert !=''){
						unset($_POST);
						$insert_msg= "Dailyroutine added successfully";
						$this->session->set_flashdata('success',$insert_msg);
					}
				}
			
			if($this->session->userdata('logged_in')['cp_users_id'] != 0){
				$userDetail_cp = $this->Common_model_new->getSingleRecordById('cp_users', array('id' => $this->session->userdata('logged_in')['cp_users_id']));
				$to = $userDetail_cp['email'];
			} else {
				$to = $this->session->userdata('logged_in')['email'];
			}

      $to = "pratap11191@gmail.com";

      $subject = 'Dailyroutine Report';

      $message = $this->emailTemp();

      	// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Additional headers
		$headers .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
		$headers .= 'From: Birthday Reminder <birthday@example.com>' . "\r\n";
		$headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
		$headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";

		// Mail it
		//mail($to, $subject, $message, $headers);
      	
      }

      $data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'dailyroutine',"user_id"=>$user_id));

      $check_dailyroutine = $this->Common_model_new->getsingle("cp_dailyroutine",array("created" => date('Y-m-d'),"user_id" => $this->session->userdata('logged_in')['id']));
   	  $data['check_dailyroutine'] = $check_dailyroutine;
     
      $this->template->set('title', 'Dailyroutine');

      $this->template->load('new_user_dashboard_layout', 'contents', 'dailyroutine', $data);
    } 
    else{

      redirect('login');
  
    } 
  }


  public function emailTemp()
  {
  	$check_dailyroutine = $this->Common_model_new->getsingle("cp_dailyroutine",array("created" => date('Y-m-d'),"user_id" => $this->session->userdata('logged_in')['id']));
  	if(!empty($check_dailyroutine)){
        $morning = explode(",", $check_dailyroutine->morning);
        $noon  = explode(",", $check_dailyroutine->noon);
        $evening = explode(",", $check_dailyroutine->evening);
        $notes = explode(",", $check_dailyroutine->notes);
        $dinner = explode(",", $check_dailyroutine->dinner);
        $bedtime = explode(",", $check_dailyroutine->bedtime);

     } else {
        $morning = [];
        $noon  = [];
        $evening = [];
        $notes = [];
        $dinner = [];
        $bedtime = [];
     }

     $output = '<table class="table">
    <thead>
      <tr>
        <th>Time</th>
        <th>Meal</th>
        <th>Snack</th>
        <th>Medicine</th>
        <th>Walk</th>
        <th>Yoga</th>
        <th>Meditation</th>
        <th>Nap</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>morning</td>';

     
        $output = $output.'<td><input name="morning[]" type="checkbox" value="Meal"'; 
        if(in_array("Meal", $morning)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="morning[]" type="checkbox" value="Snack"'; 
        if(in_array("Snack", $morning)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="morning[]" type="checkbox" value="Medicine"'; 
        if(in_array("Medicine", $morning)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="morning[]" type="checkbox" value="Walk"'; 
        if(in_array("Walk", $morning)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="morning[]" type="checkbox" value="Yoga"'; 
        if(in_array("Yoga", $morning)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="morning[]" type="checkbox" value="Meditation"'; 
        if(in_array("Meditation", $morning)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="morning[]" type="checkbox" value="Nap"'; 
        if(in_array("Nap", $morning)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        
        $output = $output.'</tr><tr>
        <td>noon</td>';

        $output = $output.'<td><input name="noon[]" type="checkbox" value="Meal"'; 
        if(in_array("Meal", $noon)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="noon[]" type="checkbox" value="Snack"'; 
        if(in_array("Snack", $noon)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="noon[]" type="checkbox" value="Medicine"'; 
        if(in_array("Medicine", $noon)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="noon[]" type="checkbox" value="Walk"'; 
        if(in_array("Walk", $noon)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="noon[]" type="checkbox" value="Yoga"'; 
        if(in_array("Yoga", $noon)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="noon[]" type="checkbox" value="Meditation"'; 
        if(in_array("Meditation", $noon)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="noon[]" type="checkbox" value="Nap"'; 
        if(in_array("Nap", $noon)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'</tr><tr>
        <td>evening</td>';

         $output = $output.'<td><input name="evening[]" type="checkbox" value="Meal"'; 
        if(in_array("Meal", $evening)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="evening[]" type="checkbox" value="Snack"'; 
        if(in_array("Snack", $evening)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="evening[]" type="checkbox" value="Medicine"'; 
        if(in_array("Medicine", $evening)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="evening[]" type="checkbox" value="Walk"'; 
        if(in_array("Walk", $evening)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="evening[]" type="checkbox" value="Yoga"'; 
        if(in_array("Yoga", $evening)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="evening[]" type="checkbox" value="Meditation"'; 
        if(in_array("Meditation", $evening)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="evening[]" type="checkbox" value="Nap"'; 
        if(in_array("Nap", $evening)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'</tr><tr>
        <td>dinner</td>';

         $output = $output.'<td><input name="dinner[]" type="checkbox" value="Meal"'; 
        if(in_array("Meal", $dinner)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="dinner[]" type="checkbox" value="Snack"'; 
        if(in_array("Snack", $dinner)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="dinner[]" type="checkbox" value="Medicine"'; 
        if(in_array("Medicine", $dinner)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="dinner[]" type="checkbox" value="Walk"'; 
        if(in_array("Walk", $dinner)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="dinner[]" type="checkbox" value="Yoga"'; 
        if(in_array("Yoga", $dinner)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="dinner[]" type="checkbox" value="Meditation"'; 
        if(in_array("Meditation", $dinner)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="dinner[]" type="checkbox" value="Nap"'; 
        if(in_array("Nap", $dinner)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'</tr><tr>
        <td>bedtime</td>';
        
      	$output = $output.'<td><input name="bedtime[]" type="checkbox" value="Meal"'; 
        if(in_array("Meal", $bedtime)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="bedtime[]" type="checkbox" value="Snack"'; 
        if(in_array("Snack", $bedtime)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="bedtime[]" type="checkbox" value="Medicine"'; 
        if(in_array("Medicine", $bedtime)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="bedtime[]" type="checkbox" value="Walk"'; 
        if(in_array("Walk", $bedtime)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="bedtime[]" type="checkbox" value="Yoga"'; 
        if(in_array("Yoga", $bedtime)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="bedtime[]" type="checkbox" value="Meditation"'; 
        if(in_array("Meditation", $bedtime)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="bedtime[]" type="checkbox" value="Nap"'; 
        if(in_array("Nap", $bedtime)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'</tr></tbody></table>';

  		return $output;
  }



   

 
	



}
