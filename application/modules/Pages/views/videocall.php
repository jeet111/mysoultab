<style>
	table#sampleTable th {
		color: #000;
		text-align: left;
	}

	.btn.btn-primary {
		margin: 30px 0 !important;
		padding: 10px 28px !important;
		max-width: 153px !important;
		width: 100% !important;
		border-radius: 50px !important;
	}

	tbody tr td:last-child {
		text-align: left;
	}

	.Games h4 {
		color: #000;
		text-align: center;
		padding-bottom: 4px;
		margin-top: 0 !important;
	}

	thead tr th:last-child {
		text-align: left;
	}

	.col-lg-12.video {
		margin: 0 auto;
		text-align: center;
	}
	.tab_medi td a.edit_btn {
    margin: 0 !important;
}
input#callUserName {
    padding: 9px 19px;
    border: 1px solid #ddd;
    width: 50%;
}
@media(min-width: 1600px)
{
input[type=radio] {
    margin: 4px 0 0;
    margin-top: 1px\9;
    line-height: normal;
    margin-left:0px;
}
}


@media(max-width: 1366px){
input[type=radio] {
    margin: 4px 0 0;
    margin-top: 1px\9;
    line-height: normal;
    margin-left:0px;
}
}



@media(min-width: 1920px){
input[type=radio] {
    margin: 4px 0 0;
    margin-top: 1px\9;
    line-height: normal;
    margin-left:0px;
}
}

@media(max-width: 1280px){
input[type=radio] {
    margin: 4px 0 0;
    margin-top: 1px\9;
    line-height: normal;
    margin-left:0px;
}
}

@media(max-width: 1280px){
 
 .row.vd{
 	margin-left: 200px;
 }
}

@media(min-width: 1600px){
 
 .row.vd{
 	margin-left: 350px;
 }
}

@media(min-width: 1920px){
 
 .row.vd{
 	margin-left: 500px;
 }
}

@media(min-width: 1366px){
 
 .row.vd{
 	margin-left: 250px;
 }
}
@media (max-width: 1366px){
.row.vd {
    margin-left: 250px;
}
}

video.vd{
		height:240px;
		width:325px;
		margin-left: 0px;
	}
li{
	display: inline-block;
	width:33%;
	text-align: center;
}
</style>
<script>
	var userEmail = "<?php echo $this->session->userdata('logged_in')['email']; ?>";
</script>
<!-- <link rel="stylesheet" href="style/style.css"/>
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300' rel='stylesheet' type='text/css'> -->
<script src="<?php echo base_url(); ?>assets/new_user_dashboard/js/sinch.min.js"></script>

<div class="row">
	
	<div class="col-lg-12 " id="caregiverDetails" style="margin-top:30px;">
	<?php
		if(!empty($userDetail_cp) && $userDetail_cp !='' && $this->session->userdata('logged_in')['user_role'] == "2" ) {
			$email = $userDetail_cp[0]->email;
			$name = $userDetail_cp[0]->name;
		}else{
			$email = $userDetail_cp[0]['email'];
			$name = $userDetail_cp[0]['name'];
		} 
		?>
		<ul>
			<?php
		if(!empty($userDetail_cp) && $userDetail_cp !='' && $this->session->userdata('logged_in')['user_role'] == "2" ) {
			for($i = 0;$i < sizeof($userDetail_cp);$i++){ ?>
				<li>
			 	    <input type="radio" name="radioName" value="<?php echo $userDetail_cp[$i]->email;  ?>" /> 
				 	<label><?php echo $userDetail_cp[$i]->name;  ?></label></li>
			<?php } 
		}?>
	</ul>
	
			
	</div>
	<div class="col-lg-12 video ">
	<?php // check if logged in user is caregiver or user
	if($this->session->userdata('logged_in')['user_role'] == "2"){
		if($userDetail_cp){
			?>
			<button id="call" class=" btnvideo button  btn btn-primary">Start video call</button>
		<?php } else { ?>
			<button id="add_caregiver" class=" btnvideo button  btn btn-primary">Start video call</button>
		<?php } 
	}else if($this->session->userdata('logged_in')['user_role'] == "4"){ // caregiver ?>
		<button id="call" class=" btnvideo button  btn btn-primary">Start video call</button>	
	<?php } ?> 

</div>
	<div class="col-lg-12 video ">
		<?php
           if($userDetail_cp){
		?>
		<input id="callUserName" type="hidden"  placeholder="Username (alice)" value="<?php echo $email; ?>" readonly>
		<input id="callUserNameTxt"  placeholder="Username (alice)" value="<?php echo $name; ?>" readonly>
		<input type="hidden" id="care_status" value="1">
	<?php } else { ?>
		<input type="hidden" id="care_status" value="0">
	<?php } ?>
		<button id="hangup" class="btn btn-primary" style="display:none;">Hangup</button>
		<button id="answer" class="btn btn-primary" style="display:none;">Answer</button>
	</div>
</div>
<div class="clearfix"><br></div>
<video id="videooutgoing" autoplay muted></video>
<video id="videoincoming" class="vd" autoplay></video>

<div id="callLog">
</div>
<div class="error">
</div>
</div>
</div>
<!------------>
<section class="content photo-list">
	<div class="photo-listMain reminder_listMain">

		<div class="row">
			<div class="col-lg-12">
				<div class="Games">
					<h4> Missed Video Calls </h4>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<?php if ($this->session->flashdata('success')) { ?>
  <div class="alert alert-success message">
   <button type="button" class="close" data-dismiss="alert">x</button>
   <?php echo $this->session->flashdata('success'); ?></div>
 <?php } ?>  
				<div class="card-body ">
					<?php
					if(!empty($callrecorddata)){ ?>
					<table class="table table-hover table-bordered table-mailbox send-user-mail tab_medi">
						<thead>
							<tr>
								<th>No.</th>
								<th>Name</th>
								<th>Date Recieved</th>
								<th>Time Recieved</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<?php
							   $countii = 0;
								foreach ($callrecorddata as $value) {
									$countii++;
							?>
							<tr>
								<th scope="row"><?php echo $countii; ?></th>
								<td><?php echo $value->name; ?></td>
								<td><span class="badge badge-info"><?php echo $value->date_recieved; ?></span></td>
								<td><?php echo $value->duration; ?></td>
								<td><a href="#" class="btn btn-primary edit_btn"><?php echo $value->status; ?></a></td>
							</tr>
							<?php } 
							?>

						</tbody>
					</table>
				<?php } else { ?>
					<h2>Data Not Found!</h2>
				<?php } ?>
				</div>

			</div>

		</div>
	</div>
	<!-- /# column -->
</section>
</div>

<div class="container hide">


	<div class="clearfix"></div>

	<div class="frame small">
		<div class="inner loginBox">
			<h3 id="login">Sign in</h3>
			<form id="userForm">
				<input id="username" placeholder="USERNAME"><br>
				<input id="password" type="password" placeholder="PASSWORD"><br>
				<button id="loginUser">Login</button>
				<button id="createUser">Create</button>
			</form>
			<div id="userInfo">
				<h3><span id="username"></span></h3>
				<button id="logOut">Logout</button>
			</div>
		</div>


		<div class="frame">
			<h3>Video Call</h3>
			<div id="call_old">
				<form id="newCall">
					<input id="callUserName1" placeholder="Username (alice)"><br>
					<button id="call1">Call</button>
					<button id="hangup1" >Hangup</button>
					<button id="answer1">Answer</button>

				</form>
			</div>
			<div class="clearfix"><br></div>
			<video id="videooutgoing" autoplay muted></video>
			<video id="videoincoming" autoplay></video>

			<div id="callLog">
			</div>
			<div class="error">
			</div>
		</div>
	</div>
	<script src="<?php echo base_url(); ?>assets/new_user_dashboard/js/sinchvideo.js"></script>
<script type="text/javascript">
	function add_caregiver(){
		return location.reload();
		window.location.href = "<?php echo base_url(); ?>user_profile";
	};
	$("#add_caregiver").click(function(){
		
		confirmo.init({
			yesBg:'green',
			noBg:'red'
		});
		confirmo.show({
			msg:"Please Add a Caregiver in your account for video call.",
			callback:function (){
				return add_caregiver();
			}
		});
	});
	$('#caregiverDetails input').on('change', function() {
		var email = $('input[name=radioName]:checked', '#caregiverDetails').val();
		$('#callUserName').val(email);
		$('#callUserNameTxt').val($('input[name=radioName]:checked', '#caregiverDetails').next('label').text());
		
});
</script>
