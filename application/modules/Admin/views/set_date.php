<style type="text/css">
	select.col-md-12 {
		border: 1px solid #ccc;
		padding: 3px 10px;
		height: 34px;
		line-height: 0px;
		color: #999;
	}
</style>
<div class="container-fluid">
	<div class="dash-counter">
		<div class="Schedule_main_one">
			<div class="users-main">

				<?php //echo "<pre>"; print_r($unitData); echo "</pre>"; ?>

				<?php
				echo "<h2>" ?><?php if($this->uri->segment('3')){ ?>Edit Test type<?php }else{ ?>Set Date <?php } ?></h2>

				<div class="btn_topBack">
					<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">
						Back
					</a>
				</div>

				<form class="form-horizontal" action="" method="post" enctype="multipart/form-data" name="set_units" id="set_unit">
					<?php if ($this->session->flashdata('success')) { ?>
						<div class="alert alert-success message">
							<button type="button" class="close" data-dismiss="alert">x</button>
							<?php echo $this->session->flashdata('success'); ?></div>
						<?php } ?>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Set Date:<span class="field_req">*</span></label>
									<div class="col-md-6">
										<div class="input-group date shedule" id="datetimepicker">
											<input type="text" name="shedule_date" id="" class="form-control">
											<div class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</div>
										</div>

									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Time slot:<span class="field_req">*</span></label>
									<div class="col-md-6">
										<div class="">
											<select name="setTime" class="col-md-12">
												<option value="">select</option>
												<option value="12 am">12 am</option>
												<option value="04 am">04 am</option>
												<option value="08 am">08 am</option>
												<option value="12 pm">12 pm</option>
												<option value="04 pm">04 pm</option>
												<option value="08 pm">08 pm</option>
											</select>
										</div>

									</div>
								</div>

							</div>
							<span id="spanWhatever"></span>

						</div>



						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit">

							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!--Added by 95 on 5-2-2019 For Doctor category for validation-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>
	<script type="text/javascript">

		$(document).ready(function(){
			$("#set_unit").validate({
				rules: {
					setTime: "required",
				},
				messages: {
					setTime: "Please select hours.",
				}
			});

		});


		// $('select[name="setTime"]').change(function(e) {
		// 	var id = $(this).val();
		// 	if(id==4){
		// 		var str1 ="1)04:00:00 AM  2)08:00:00 AM 3)12:00:00 PM 4)16:00:00 PM 5)20:00:00 PM 6)00:00:00 AM";

		// 	}else if(id==8){
		// 		var str1="1)08:00:00 AM 2)16:00:00 PM 3)00:00:00 AM";
		// 	}else{
		// 		var str1="1)12:00:00 PM 2)00:00:00 AM";
		// 	}

		// 	$("#spanWhatever").text(str1);
		// });

		$(function () {
			var today = new Date();
			$('#datetimepicker').datetimepicker({
				ignoreReadonly: true,
				format: 'YYYY-MM-DD',
				minDate: today
			});
		});
	</script>



