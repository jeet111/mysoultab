<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Tabfeatures extends MX_Controller {

	private $staff_permission = array();
	private $access_permission = true;
	public function __construct(){

		parent:: __construct();

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model('Common_model');
		$this->load->model('login_model');
		$this->load->model('Feature_model');
		//$this->load->library('upload');
		$this->load->helper(array('url'));
		$this->load->helper(array('form'));

		$user_id = $this->session->userdata('logged_ins')['id'];
		if(empty($user_id)) {
			redirect('admin/login');
		}

		if ($this->session->userdata('logged_ins')['user_role'] == 5) {
			$uid = $this->session->userdata('logged_ins')['id'];
			$permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
			if (!empty($permission_list)) 
			{
				foreach ($permission_list as $k => $v) 
				{
					$this->staff_permission[$v['permission_id']] = $v;
				}
			}
		}

	}

	public function permission_access($permission_id=6)
	{
		if ($this->session->userdata('logged_ins')['user_role'] != 1) 
		{
			if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
			{
				$this->access_permission = false;
			}
		}
	}

	public function feature_category_list()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$data['featurescat_list'] = $this->Common_model->getAllorderby('cp_tabfeature_category','id','desc');
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'Tab features');
			$this->template->load('admin_dashboard_layout', 'contents', 'feature_cate_list', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}




	public function feature_list()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$data['featurescat_list'] = $this->Common_model->getAllorderby('cp_tabfeatures','id','desc');
		//$data['featurescat_list'] = $this->Common_model->getAllorderby('cp_tabfeatures','id','desc');
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'Tab features');
			$this->template->load('admin_dashboard_layout', 'contents', 'features_list.php', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}


	public function add_feature_cate()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$this->form_validation->set_rules('feature_cat_name', 'Feature Category Name', 'required');
		$this->form_validation->set_rules('feature_cat_desc', 'Feature Category Description', 'required');

		$feature_name = $this->input->post('feature_cat_name');
		$feature_cat_desc = $this->input->post('feature_cat_desc');

		if ($this->form_validation->run() == TRUE)
		{


			

			// echo "<pre>";
			// print_r($_POST);
			// die;


			$data['upload_path'] = 'uploads/featureImages/cat_images';
			$data['allowed_types'] = 'jpg|png|gif|jpeg';    
			$data['max_size'] = '5000';
			$data['encrypt_name'] = true;
			$this->load->library('upload', $data);

			if ($this->upload->do_upload('feature_icon')) {
				$attachment_data = array('upload_data' => $this->upload->data());
				$uploadfile = $attachment_data['upload_data']['file_name'];
				
				$array = array(
					'feture_category	' => $feature_name,
					'cat_desc'=> $feature_cat_desc,
					'cat_img'=>$uploadfile,
					'create_date' => date('Y-m-d H:i:s')
				);
				//echo '<pre>';print_r($array );die;
				$test_type_id = $this->Feature_model->insertData('cp_tabfeature_category',$array);

			}
			
			$this->session->set_flashdata('success', 'Feature Category Successfully Added.');
			redirect('admin/feature_category_list');

		}
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'Tab Features');
			$this->template->load('admin_dashboard_layout', 'contents', 'add_feature_cate', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}


	public function add_feature()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$this->form_validation->set_rules('feature_name', 'Feature Name', 'required');
		$this->form_validation->set_rules('feature_desc', 'Feature Description.', 'required');
		$this->form_validation->set_rules('feature_Cat', 'Feature Category.', 'required');

		$feature_name = $this->input->post('feature_name');
		$feature_Cat = $this->input->post('feature_Cat');
		$feature_desc = $this->input->post('feature_desc');

		if ($this->form_validation->run() == TRUE)
		{


			$data['upload_path'] = 'uploads/featureImages/feature_icons';
			$data['allowed_types'] = 'jpg|png|gif|jpeg';    
			$data['max_size'] = '5000';
			$data['encrypt_name'] = true;
			$this->load->library('upload', $data);

			if ($this->upload->do_upload('feature_icon')) {
				$attachment_data = array('upload_data' => $this->upload->data());
				$uploadfile = $attachment_data['upload_data']['file_name'];
				
				$array = array(
					'feture_name' => $feature_name,
					'cat_id'=>$feature_Cat,
					'feature_icon'=>$uploadfile,
					'feature_desc'=>$feature_desc,
					'create_date' => date('Y-m-d H:i:s')
				);
			 //echo '<pre>';print_r($array );die;
				$test_type_id = $this->Feature_model->insertData('cp_tabfeatures',$array);

			}


			$this->session->set_flashdata('success', 'Feature Successfully Added.');
			redirect('admin/feature_list');

		}

		$data['staff_permission'] = $this->staff_permission;

		$this->permission_access(6);
		if ($this->access_permission) {
			$data['featurescat_list'] = $this->Common_model->getAllorderby('cp_tabfeature_category','id','desc');
			$this->template->set('title', 'Tab Features');
			$this->template->load('admin_dashboard_layout', 'contents', 'add_feature', $data);
		} else {
			$data['featurescat_list'] = $this->Common_model->getAllorderby('cp_tabfeature_category','id','desc');
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}



	// edit feature

	public function edit_feature()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$this->form_validation->set_rules('feature_name', 'Feature Name', 'required');
		$this->form_validation->set_rules('feature_desc', 'Feature Description.', 'required');
		$this->form_validation->set_rules('feature_Cat', 'Feature Category.', 'required');


		$feature_id = $this->uri->segment('3');
		$data['singleData'] = $this->Common_model->getsingle('cp_tabfeatures',array('id' => $feature_id));

		$feature_name = $this->input->post('feature_name');
		$feature_Cat = $this->input->post('feature_Cat');
		$feature_desc = $this->input->post('feature_desc');

		if ($this->form_validation->run() == TRUE)
		{


			$data['upload_path'] = 'uploads/featureImages/feature_icons';
			$data['allowed_types'] = 'jpg|png|gif|jpeg';    
			$data['max_size'] = '5000';
			$data['encrypt_name'] = true;
			$this->load->library('upload', $data);

			if ($this->upload->do_upload('feature_icon')) {
				$attachment_data = array('upload_data' => $this->upload->data());
				$uploadfile = $attachment_data['upload_data']['file_name'];
				
				$array = array(
					'feture_name' => $feature_name,
					'cat_id'=>$feature_Cat,
					'feature_icon'=>$uploadfile,
					'feature_desc'=>$feature_desc,
					'create_date' => date('Y-m-d H:i:s')
				);
			 //echo '<pre>';print_r($array );die;

				$this->Common_model->updateData('cp_tabfeatures',$array,array('id' => $feature_id));
				//$test_type_id = $this->Feature_model->insertData('cp_tabfeatures',$array);

			}else{

				$array = array(
					'feture_name' => $feature_name,
					'cat_id'=>$feature_Cat,
					'feature_icon'=>$data['singleData']->feature_icon,
					'feature_desc'=>$feature_desc,
					'create_date' => date('Y-m-d H:i:s')
				);
			 //echo '<pre>';print_r($array );die;

				$this->Common_model->updateData('cp_tabfeatures',$array,array('id' => $feature_id));


			}


			$this->session->set_flashdata('success', 'Feature Successfully Updated.');
			redirect('admin/feature_list');

		}

		$data['staff_permission'] = $this->staff_permission;

		$this->permission_access(6);
		if ($this->access_permission) {
			$data['featurescat_list'] = $this->Common_model->getAllorderby('cp_tabfeature_category','id','desc');
			$this->template->set('title', 'Tab Features');
			$this->template->load('admin_dashboard_layout', 'contents', 'edit_feature', $data);
		} else {
			$data['featurescat_list'] = $this->Common_model->getAllorderby('cp_tabfeature_category','id','desc');
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}

	// End
	// Edit feature cate

	public function edit_feature_category()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$this->form_validation->set_rules('feature_cat_name', 'Feature Category Name', 'required');
		$this->form_validation->set_rules('feature_cat_desc', 'Feature Category Description', 'required');

		$cate_id = $this->uri->segment('3');
		// echo $cate_id;
		// die;
		$data['singleData'] = $this->Common_model->getsingle('cp_tabfeature_category',array('id' => $cate_id));
		// echo "<pre>";
		// print_r($data['singleData']->cat_img);
		// die;
		$feature_name = $this->input->post('feature_cat_name');
		$feature_cat_desc = $this->input->post('feature_cat_desc');

		
		if ($this->form_validation->run() == TRUE)
		{
			// echo "<pre>";
			// print_r($_FILES);
			// die;

			$data['upload_path'] = 'uploads/featureImages/cat_images';
			$data['allowed_types'] = 'jpg|png|gif|jpeg';    
			$data['max_size'] = '5000';
			$data['encrypt_name'] = true;
			$this->load->library('upload',$data);
			if ($this->upload->do_upload('feature_icon')) {
				$attachment_data = array('upload_data' => $this->upload->data());
				$uploadfile = $attachment_data['upload_data']['file_name'];

				$array = array(
					'feture_category'=> $feature_name,
					'cat_desc'=> $feature_cat_desc,
					'cat_img'=>$uploadfile,
					'create_date' => date('Y-m-d H:i:s')
				);

				//echo '<pre>';print_r($array);die('hgkhjgk');  
				$this->Common_model->updateData('cp_tabfeature_category',$array,array('id' => $cate_id));
				//$test_type_id = $this->Feature_model->insertData('cp_tabfeature_category',$array);

			}else{

				//$attachment_data = array('upload_data' => $this->upload->data());
				//$uploadfile = $attachment_data['upload_data']['file_name'];
				$array = array(
					'feture_category'=> $feature_name,
					'cat_desc'=> $feature_cat_desc,
					'cat_img'=>$data['singleData']->cat_img,
					'create_date' => date('Y-m-d H:i:s')
				);
				//echo '<pre>';print_r($array );die('cnmvcnmv');
				$this->Common_model->updateData('cp_tabfeature_category',$array,array('id' => $cate_id));
				//$test_type_id = $this->Feature_model->insertData('cp_tabfeature_category',$array);


			}
			
			$this->session->set_flashdata('success', 'Feature Category Successfully Updated.');
			redirect('admin/feature_category_list');

		}



		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'Tab Features');
			$this->template->load('admin_dashboard_layout', 'contents', 'edit_feature_cate', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}

	// End

	public function edit_vital_sign()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$this->form_validation->set_rules('test_type', 'Test type', 'required');
		$this->form_validation->set_rules('test_unit', 'Test Unit', 'required');

		$segment = $this->uri->segment('3');

		$test_type = $this->input->post('test_type');
		$test_unit = $this->input->post('test_unit');

		$data['testData'] = $this->Common_model->getsingle('cp_vital_test_type',array('id' => $segment));

		// echo $data['singleData']->id;
		// die;

		$data['unitData'] = $this->Common_model->getsingle('cp_vital_test_type_unit',array('test_type_id' => $segment));



		$data['singleData']= (object)array_merge((array)$data['testData'],(array)$data['unitData']);


		if ($this->form_validation->run() == TRUE)
		{
			$array = array(
				'test_type' => $test_type,
				'update_date' => date('Y-m-d H:i:s'),
			);

			$Unitarray = array(
				'unit' => $test_unit,
				'update_date' => date('Y-m-d H:i:s')
			);


			// echo '<pre>';print_r($array );die;
			$this->Common_model->updateData('cp_vital_test_type',$array,array('id' => $segment));

			$this->Common_model->updateData('cp_vital_test_type_unit',$Unitarray,array('test_type_id' => $segment));

			$this->session->set_flashdata('success', 'Successfully updated');
			redirect('admin/vital_sign_list');

		}

		$data['staff_permission'] = $this->staff_permission;

		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'Vital Sign Test Type Management');
			$this->template->load('admin_dashboard_layout', 'contents' , 'add_vital_sign', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}

	public function delete_feature_category()
	{
		$segment = $this->uri->segment('3');
		$this->Feature_model->delete('cp_tabfeature_category',array('id' => $segment));
		//$this->Common_model->delete('cp_vital_test_type_unit',array('test_type_id' => $segment));
		$this->session->set_flashdata('success', 'Successfully deleted');
		redirect('admin/feature_category_list');
	}



	public function delete_feature()
	{
		$segment = $this->uri->segment('3');
		$this->Feature_model->delete('cp_tabfeatures',array('id' => $segment));
		//$this->Common_model->delete('cp_vital_test_type_unit',array('test_type_id' => $segment));
		$this->session->set_flashdata('success', 'Successfully deleted');
		redirect('admin/feature_list');
	}



	public function getFeture()
	{
		$fetr_id = $this->input->post('feature_id');
		$fetrData= $this->Common_model->getsingle('cp_tabfeatures',array('id' => $fetr_id));


		echo json_encode($fetrData);
	}	

}