<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Footer_cms extends MX_Controller {

	private $staff_permission = array();
	private $access_permission = true;
	public function __construct(){

		parent:: __construct();

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model('Common_model');
		$this->load->model('login_model');
		$this->load->model('Feature_model');

		$this->load->helper(array('url'));
		$this->load->helper(array('form'));

		$user_id = $this->session->userdata('logged_ins')['id'];
		if(empty($user_id)) {
			redirect('admin/login');
		}

		if ($this->session->userdata('logged_ins')['user_role'] == 5) {
			$uid = $this->session->userdata('logged_ins')['id'];
			$permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
			if (!empty($permission_list)) 
			{
				foreach ($permission_list as $k => $v) 
				{
					$this->staff_permission[$v['permission_id']] = $v;
				}
			}
		}

	}

	public function permission_access($permission_id=6)
	{
		if ($this->session->userdata('logged_ins')['user_role'] != 1) 
		{
			if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
			{
				$this->access_permission = false;
			}
		}
	}

	public function footer_content_list()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$data['FooterData'] = $this->Common_model->getAllorderby('cp_footer_content','id ','desc');
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'Pages CMS');
			$this->template->load('admin_dashboard_layout', 'contents', 'footer_content_list', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}


	public function edit_footer_content(){

		$data['menuactive'] = $this->uri->segment(2);
    //$this->form_validation->set_rules('template_id','Template Id','required');
    //$this->form_validation->set_rules('subject','Subject','required');
		$page_id = $this->uri->segment('3');

		$data['singleData'] = $this->Common_model->getsingle('cp_footer_content',array('id' => $page_id));

		$this->form_validation->set_rules('desc_heading','Description Heading','required');
		$this->form_validation->set_rules('footer_desc','Description','required');
		$this->form_validation->set_rules('contact_email','Contact email','required');
		$this->form_validation->set_rules('contact_phone','Contact phone','required');
		$this->form_validation->set_rules('right_reserved','Rights reserved','required');
		

		if($this->form_validation->run()==TRUE)
		{
			$desc_heading = $_POST['desc_heading'];
			$footer_desc = $_POST['footer_desc'];
			$contact_email = $_POST['contact_email'];
			$contact_phone = $_POST['contact_phone'];
			$right_reserved = $_POST['right_reserved'];
			$arrayData = array(
				'description_heading'=>$desc_heading,
				'footer_desc'=>$footer_desc,
				'contact_email'=>$contact_email,
				'contact_phone'=>$contact_phone,
				'rights_reserved'=>$right_reserved,
				'update_date'=>date('Y-m-d H:i:s')
			);
			$this->Common_model->updateData('cp_footer_content',$arrayData,array('id' =>$page_id));     

      //$this->Common_model->updateData('about_page',$array,array('template_id' => 1));
			$this->session->set_flashdata('success', 'Successfully updated.');
			redirect('admin/footer_content_list');
		}
		$data['staff_permission'] = $this->staff_permission;

		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'CMS');
			$this->template->load('admin_dashboard_layout', 'contents', 'edit_footer_content', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}


	public function delete_feature_category()
	{
		$segment = $this->uri->segment('3');
		$this->Feature_model->delete('cp_tabfeature_category',array('id' => $segment));
		//$this->Common_model->delete('cp_vital_test_type_unit',array('test_type_id' => $segment));
		$this->session->set_flashdata('success', 'Successfully deleted');
		redirect('admin/feature_category_list');
	}


	public function getFeture()
	{
		$fetr_id = $this->input->post('feature_id');
		$fetrData= $this->Common_model->getsingle('cp_tabfeatures',array('id' => $fetr_id));


		echo json_encode($fetrData);
	}	

}