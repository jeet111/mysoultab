<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Acknowledge extends MX_Controller {

  private $staff_permission = array();
  private $access_permission = true;
  public function __construct(){   

    parent:: __construct();

    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->model('acknowledge_model');
    $this->load->model('login_model');
    $this->load->helper(array('url'));
    $this->load->helper(array('form'));
    $user_id = $this->session->userdata('logged_ins')['id'];
    if(empty($user_id)) {
      redirect('admin/login');
    }

    if ($this->session->userdata('logged_ins')['user_role'] == 5) {
      $uid = $this->session->userdata('logged_ins')['id'];
      $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
      if (!empty($permission_list)) 
      {
         foreach ($permission_list as $k => $v) 
         {
            $this->staff_permission[$v['permission_id']] = $v;
         }
      }
    }

  }

  public function permission_access($permission_id=8)
  {
    if ($this->session->userdata('logged_ins')['user_role'] != 1) 
    {
       if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
       {
          $this->access_permission = false;
       }
    }
  }
  
  /*Created by 95 for show all acknowledges*/
  public function acknowledge_list()
  {
     $data['menuactive'] = $this->uri->segment(2);
	 $data['acknowledgeData'] = $this->acknowledge_model->getAllAcknowledge('acknowledge','acknowledge_id','desc');
	 
   $data['staff_permission'] = $this->staff_permission;
    
    $this->permission_access(8);
    if ($this->access_permission) {
      $this->template->set('title', 'Acknowledge Management');
      $this->template->load('admin_dashboard_layout', 'contents', 'acknowledge_list', $data);
    } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
    }
  }
 
  /*Created by 95 for delete acknowledge*/
  public function delete_acknowledge($acknowledge_id='')
  {
	  
	  $this->acknowledge_model->delete('acknowledge',array('acknowledge_id' =>$acknowledge_id));
	   $this->session->set_flashdata('success', 'Deleted successfully.');
			 redirect('admin/acknowledge_list');
  }



  
  
  public function view_acknowledge($acknowledge_id='')
  {
     $data['menuactive'] = $this->uri->segment(2);
     $data['viewAcknowledge']=$this->acknowledge_model->getsingle('acknowledge',array('acknowledge_id'=>$acknowledge_id));
     
     $data['staff_permission'] = $this->staff_permission;
     
    $this->permission_access(8);
    if ($this->access_permission) {
      $this->template->set('title', 'Acknowledge Management');
      $this->template->load('admin_dashboard_layout', 'contents', 'view_acknowledge', $data);
    } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
    }
  }  



}