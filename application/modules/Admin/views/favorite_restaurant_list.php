<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="row">

			<div class="col-md-12">

				<div class="">

					<?php //echo "<pre>"; print_r($MusicData); echo "</pre>";?>

					<div class="card-body">

						<div class="status-cng"></div>

						<?php if ($this->session->flashdata('success')) { ?>

							<div class="alert alert-success message">

								<button type="button" class="close" data-dismiss="alert">x</button>

								<?php echo $this->session->flashdata('success'); ?></div>

							<?php } ?>

							<table class="table table-hover tab_comn" id="sampleTable">

								<thead>

									<tr>

										<th>S no.</th>

										<th>Username</th>

										<th>Restaurant Name</th>

										<th>Address</th>

										<th>Image</th>						

										<th>Action</th>

									</tr>

								</thead>

								<tbody>

									<?php 

									$i = 1;

									foreach ($favorite_restaurant_data as $restaurant) {

										?>

										<tr>

											<td><?php echo $i++; ?></td>

											<td><?php 

											foreach ($users as $user) {

												if($user->id==$restaurant->user_id){



													echo $user->username;

												}		

											}

											?></td>

											<td><?php echo $restaurant->fav_rest_name; ?></td>

											<td><?php echo $restaurant->fav_rest_address; ?></td>

											<td>	

												<img src="<?php echo $restaurant->fav_rest_image; ?>"  >





											</td>

											

											

											

											<td>

												<div class="link-del-view">

													<?php

													if (isset($staff_permission[11]['view_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/view_restaurant/<?php echo $restaurant->fav_rest_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

															<span class="tooltiptext">View</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/view_restaurant/<?php echo $restaurant->fav_rest_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

															<span class="tooltiptext">View</span>

														</div>

														<?php

													}

													?>

													

													<?php

													if (isset($staff_permission[11]['delete_permission']) == 1) {

														?>

														<div class="tooltip-2">  

															<a href="<?php echo base_url(); ?>admin/delete_fav_restaurant/<?php echo $restaurant->fav_rest_id; ?>" onclick="return confirm('Delete this favorite restaurant ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">  

															<a href="<?php echo base_url(); ?>admin/delete_fav_restaurant/<?php echo $restaurant->fav_rest_id; ?>" onclick="return confirm('Delete this favorite restaurant ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

														</div>

														<?php

													}

													?>

													

													

													

												</div>

												

											</div>

										</td>

									</tr>

								<?php } ?>

							</tbody>

						</table>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>