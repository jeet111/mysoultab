<!--==========================

how it works Section

============================-->

<section id="order-detail">



<div class="paym_main_ss">



  <div class="container">

    <div class="row">



       <div id="section2" class="bigHeading wow fadeInDown animated animated" style="visibility: visible; animation-name: fadeInDown;">

        <h2 class="margin-bottom-xxl text-center">Payment <strong>Detail</strong></h2>

      </div>



      <div class="col-lg-12 ">

        <div class="web-heading prod-dtl wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">



          <h2><?php echo $this->lang->line('label_421'); ?></h2>

          <p><?php echo $this->lang->line('label_413'); ?></p>

        </div>

      </div>

      <div class="col-lg-12 col-sm-12 col-xs-8 offset-lg-2">

        <!-- main slider carousel -->

        <div class="row">

          <div class="col-lg-8">

            <div class="profile-basic background-white p20 payment-mode pay_sss">

              <div class="status-cng">



       



             

              </div>

              <h3 class="page-title"><?php echo $this->lang->line('label_414'); ?></h3>

              <div class="make-paymnt-sec">

                <div class="bx_tripsBgRight">

                  <table class="table table-bordered">

                    <thead>

                      <tr class="pl_ss">

                        <th>Plan name</th>

                        <th>Amount</th>

                        <th>Days</th>

                        

                      </tr>

                    </thead>



                    <tbody>

                      <tr class="pllll_txt">

                        <td><?php echo $subsplanDetails['plan_name'];  ?></td> 

                        <td>$ <?php echo $subsplanDetails['amount'];  ?></td>

                        <td><?php echo $subsplanDetails['plan_days'];  ?></td>

                      </tr> 

                      <tr class="tot_txtt"><td colspan="2">Total</td><td>Total</td></tr> 

                      

                    </tbody>





                  </table>

                </div>

              </div>

              <!-- <h3 class="page-title">

              Payment Method

              </h3>

              <div class="method-list method-box">

                <label class="control control--radio">

                <input type="radio" name="radio">

                <div class="control__indicator"></div>

              </label>

              <span><img src="assets/img/pay-7.png"></span>

            </div>

              <div class="method-list method-box">

                <label class="control control--radio">

                <input type="radio" name="radio">

                <div class="control__indicator"></div>

              </label>

              <span><img src="assets/img/pay-1.png"></span>

              <span><img src="assets/img/pay-2.png"></span>

              <span><img src="assets/img/pay-3.png"></span>

              <span><img src="assets/img/pay-4.png"></span>

            </div> -->

            <div class="full-width pay-ment">

              <div class="">

                <!-- <button class="btn btn-primary color-btn">Make Payment</button> -->

                <form id="payment-form" action="<?php echo base_url(); ?>payment-process" method="post">  

                  <div id="dropin-container"></div>

                  <button type="submit" id="paymentBtn" class="btn btn-primary color-btn">Pay now</button>

                  <input type="hidden" name="order_id" value="<?php echo $subsplanDetails['id'];  ?>">



                  <input type="hidden" name="user_id" value="<?php echo $users['user_id'];  ?>">



                  

                  

                  <input type="hidden" name="order_total" id="order_total" value="<?php echo $subsplanDetails['amount'];  ?>">

                  <input type="hidden" id="nonce" name="payment_method_nonce">

                </form>

              </div>

            </div>

          </div>

          </div>



 <div class="col-lg-4"> 

 <div class="profile-basic pay_sss_bott_s"> 

<img src="<?php echo base_url('assets/images/payment_im.jpg') ?>" alt="" />

 </div>

 </div>





      </div>

      <!--/main slider carousel-->

    </div>

  </div>

</div>



</div>



</section>



<script src="<?php echo base_url(); ?>assets/js/dropin.min.js"></script>

<script>

  var form = document.querySelector('#payment-form');

  var nonceInput = document.querySelector('#nonce');

  var generatedToken = '<?php echo Braintree_ClientToken::generate(); ?>';

  var ordertotal = document.querySelector('input[name="order_total"]').value;

  

  braintree.dropin.create({

    authorization: generatedToken,

    container: '#dropin-container',

    paypal: {

      flow: 'checkout',

      amount: ordertotal,

      currency: 'USD'

    },

  }, function (err, dropinInstance) {

    if (err) {

      // Handle any errors that might've occurred when creating Drop-in

      console.error(err);

    //  alert(err);

    return;

  }

  form.addEventListener('submit', function (event) {

    event.preventDefault();



    dropinInstance.requestPaymentMethod(function (err, payload) {

      if (err) {

         //  alert(err);

          // Handle errors in requesting payment method

          return;

        }



        // Send payload.nonce to your server

        nonceInput.value = payload.nonce;

        form.submit();

      });

  });

});





</script>