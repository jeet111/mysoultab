<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vitals extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('table');
		$this->load->library('pagination');
		$this->load->model('common_model');
		$this->load->model('common_model_new');
		$this->load->helper(array('common_helper'));
		$this->load->helper(array('url'));
		$this->load->helper(array('form'));
		not_login();
	}

	public function vital_signs_dashboard()
	{
		$checkLogin = $this->session->userdata('logged_in');
		$data['vitalArray'] = $this->common_model_new->getAllwhere("setting_butns", array("btnname" => 'Vital Signs', "user_id" => $checkLogin['id']));
		$this->template->set('title', 'vital signs');
		$this->template->load('new_user_dashboard_layout', 'contents', 'vital_signs', $data);
	}
	public function getall_vitals_list()
	{

		$checkLogin = $this->session->userdata('logged_in');

		if (!empty($checkLogin) && $checkLogin != '') {

			$user_id = isset($checkLogin['id']) ? $checkLogin['id'] : "";
			//_dx($user_id);
			if (!empty($user_id) && $user_id != '') {
				//$result_list = $this->common_model_new->getAllwhere_group_by_orderby("cp_vital_sign", array("user_id" => $user_id), 'date', 'date', 'DESC');
				$result_list = $this->common_model_new->getAllwhere("cp_vital_sign", array("user_id" => $user_id));
				$result_list = obj_to_array($result_list);
				$result['vital_setting_btn'] = $this->common_model_new->getAllwhere("setting_butns", array("btnname" => 'Vital Signs', "user_id" => $user_id));
				
				if (!empty($result_list) && $result_list != '') {

					foreach ($result_list as $k) {
						$final_result[] = array(
							'vital_sign_id' => $k['vital_sign_id'],
							'user_id'		=> $k['user_id'],
							'test_type'		=> $k['test_type'],
							'unit'			=> $k['unit'],
							'date'			=> $k['date'],
							'doctor'		=> $k['doctor'],
							'type'			=> $k['type'],
						);
					}
					//_dx($final_result);
					$result['vitals_array'] 	= $final_result;
					// print_r($final_result);
				}
			}
		

			$this->template->set('title', 'Vitals');
			//$this->template->load('new_user_dashboard_layout', 'contents', 'vital_signs', $result);
			if (!empty($result) && $result != '') {
				$this->template->load('new_user_dashboard_layout', 'contents', 'vital_signs', $result);
			} else {
				$result = '';
				$this->template->load('new_user_dashboard_layout', 'contents', 'vital_signs', $result);
			}
		} else {
			redirect('login');
		}
	}

	public function vital_add()
	{

		$checkLogin = $this->session->userdata('logged_in');

		if (!empty($checkLogin) && $checkLogin != '') {
			$this->form_validation->set_rules('vital_add_date', 'Select Vital Date', 'required');
		//	$this->form_validation->set_rules('test_type', 'Checkup Type', 'required');
		//	$this->form_validation->set_rules('doctor', 'Doctor', 'required');
			$this->form_validation->set_rules('unit', 'Unit', 'required');

			$submit = $this->input->post('submit');
			$submit = isset($submit) ? $submit : "";

			$user_id = isset($checkLogin['id']) ? $checkLogin['id'] : "";

			if ($this->form_validation->run()) {

				if (!empty($user_id) && $user_id != '' && !empty($submit) && $submit != "") {

					$vital_add_date = $this->input->post('vital_add_date');
					$vital_add_date = isset($vital_add_date) ? $vital_add_date : '';

					$test_type = $this->input->post('test_type');
					$test_type = isset($test_type) ? $test_type : "";

					$doctor = $this->input->post('doctor');
					$doctor = isset($doctor) ? $doctor : "";

					$unit = $this->input->post('unit');
					$unit = isset($unit) ? $unit : "";
					$other_test_type = $this->input->post('other_test_report');
					$other_doctor_name = $this->input->post('other_doctor_name');
					$other_doctor_email_address=$this->input->post('other_doctor_email_address');
					$other_doctor_fax_number=$this->input->post('other_doctor_fax_number');
					$other_doctor_address=$this->input->post('other_doctor_address');
					$other_doctor_phone_number=$this->input->post('other_doctor_phone_number');

					$type = "1";
						$insert1 = $this->common_model_new->addRecords('cp_doctor',array('other_doctor_name'=>$other_doctor_name,'doctor_email'=>$other_doctor_email_address,'fax_num'=>$other_doctor_fax_number,'doctor_address'=>$other_doctor_address,'doctor_mob_no'=>$other_doctor_phone_number));
						// _dx($insert1);
						$doctor_id = $this->db->insert_id();
						$insert2 = $this->common_model_new->addRecords('cp_test_type',array('test_type'=>$other_test_type));
						$report_id = $this->db->insert_id();
						$data=array(
							'date'			=> $vital_add_date,
							'doctor'		=> $other_doctor_name,
							'unit'			=> $unit,
							'user_id'		=> $user_id,
							'test_type'		=> $other_test_type,
							'create_date'	=> date('m-d-Y'),
							'type'			=> $type,
							'update_date' 	=> date('m-d-Y'),
							'create_at'	  	=> date("Y-m-d H:i:s"),
							'update_at'	  	=> date("Y-m-d H:i:s"),
						);
						// _dx($data);
						$sql_query = $this->common_model_new->addRecords('cp_vital_sign', $data);
						// _dx($sql_query);
					if (!empty($sql_query) && $sql_query != '') {
						$this->session->set_flashdata('success_msg', "Vital Add Sucessfully");
						redirect('vital_signs');
					} else {
						$this->session->set_flashdata('faild_msg', "Something was Wrong");
						redirect('vital_signs');
					}
					  
		
				
				}
			}
		}

		$id = "`id` = `id`";
		$doctor_id = "`doctor_id`=`doctor_id`";
		$doctor_arr 	 = $this->common_model_new->getallwhere_group_by_orderby('cp_doctor', $doctor_id, 'doctor_name', 'doctor_name', 'ASC');
		$type_unit_arr 	 = $this->common_model_new->getallwhere_group_by_orderby('cp_vital_test_type_unit', $id, 'test_type_id', 'unit', 'ASC');
		$test_type_arr	 = $this->common_model_new->getallwhere_group_by_orderby("cp_test_type", $id, 'test_type', 'test_type', 'ASC');

		$this->template->set('title', 'Vitals');
		$this->template->load('new_user_dashboard_layout', 'contents', 'vital_add', array('doctor_arr' => $doctor_arr, 'test_type_arr' => $test_type_arr, 'type_unit_arr' => $type_unit_arr));
		//$this->template->load('new_user_dashboard_layout','contents','vital_add',array('doctor_arr'=> $doctor_arr,'test_type_arr'=> $test_type_arr,'type_unit_arr'=> $type_unit_arr));

	}

	public function vital_edit($vital_sign_id = '')
	{


		$checkLogin = $this->session->userdata('logged_in');

		if (!empty($checkLogin) && $checkLogin != '') {

			$user_id = isset($checkLogin['id']) ? $checkLogin['id'] : "";
			$vital_sign_id = isset($vital_sign_id) ? $vital_sign_id : "";
			$data = $this->common_model_new->getsingle("cp_vital_sign", array("vital_sign_id" => $vital_sign_id));

			$id = "`id` = `id`";
			$doctor_id = "`doctor_id`=`doctor_id`";
			$doctor_arr 	 = $this->common_model_new->getallwhere_group_by_orderby('cp_doctor', $doctor_id, 'doctor_name', 'doctor_name', 'ASC');
			$type_unit_arr 	 = $this->common_model_new->getallwhere_group_by_orderby('cp_vital_test_type_unit', $id, 'test_type_id', 'unit', 'ASC');
			$test_type_arr	 = $this->common_model_new->getallwhere_group_by_orderby("cp_test_type", $id, 'test_type', 'test_type', 'ASC');
			$date = date('Y-m-d', strtotime($data->date));
			$vital_edit_arr = array(
				'vital_sign_id'	=> $data->vital_sign_id,
				'user_id'       => $data->user_id,
				'date' 			=> $date,
				'test_type' 	=> $data->test_type,
				'doctor'		=> $data->doctor,
				'unit'  		=> $data->unit,
			);

			$submit = isset($_POST['submit']) ? $_POST['submit'] : "";

			if (!empty($submit) && $submit != '' && $submit == "update") {

				$date = $this->input->post('date');
				$date = isset($date) ? $date : "";

				$date = date('Y-m-d', strtotime($date));

				$test_type = $this->input->post('test_type');
				$test_type = isset($test_type) ? $test_type : "";

				$doctor = $this->input->post('doctor');
				$doctor = isset($doctor) ? $doctor : "";

				$unit = $this->input->post('unit');
				$unit = isset($unit) ? $unit : "";

				$vital_sign_id = $this->input->post('vital_sign_id');
				$vital_sign_id = isset($vital_sign_id) ? $vital_sign_id : "";

				$vital_update_arr = array(
					'date' 			=> $date,
					'test_type' 	=> $test_type,
					'doctor'		=> $doctor,
					'unit'  		=> $unit,
				);

				$where = array('vital_sign_id' => $vital_sign_id);

				$update_login = $this->common_model->updateRecords('cp_vital_sign', $vital_update_arr, $where);

				if (!empty($checkLogin) && $update_login != '') {
					$this->session->set_flashdata('success_msg', 'Vital updated successfully');
					redirect('vital_signs');
				}
			}
		}

		$this->template->set('title', 'Vitals');
		$this->template->load('new_user_dashboard_layout', 'contents', 'vital_edit', array('doctor_arr' => $doctor_arr, 'test_type_arr' => $test_type_arr, 'type_unit_arr' => $type_unit_arr, 'vital_edit_arr' => $vital_edit_arr));
	}

	// public function update_status()
	// {

	// 	$checkLogin = $this->session->userdata('logged_in');
	// 	if (!empty($checkLogin) && $checkLogin != '') {

	// 		$user_id = isset($checkLogin['id']) ? $checkLogin['id'] : "";
	// 		$data = $this->common_model_new->getsingle("cp_vital_sign", array("user_id" => $user_id));

	// 		$type = ($data->type == '1' ? '0' : '1');
	// 		$condition	= array('user_id' => $data->user_id);
	// 		$array 		= array('type' => $type);

	// 		$update_login = $this->common_model->updateRecords('cp_vital_sign', $array, $condition);

	// 		if ($update_login) {
	// 			echo 1;
	// 		} else {
	// 			echo 0;
	// 		}
	// 	} else {
	// 		redirect('login');
	// 	}
	// }

	public function vital_delete($vital_sign_id = '')
	{
		$checkLogin = $this->session->userdata('logged_in');
		if (!empty($checkLogin) && $checkLogin != '') {
			$vital_sign_id = isset($_POST['id']) ? $_POST['id'] : "";

			if (!empty($vital_sign_id) && $vital_sign_id != '') {

				$delete_vital = $this->common_model->deleteRecords("cp_vital_sign", array("vital_sign_id" => $vital_sign_id));

				if (!empty($delete_vital) && $delete_vital != '') {
					$this->session->set_flashdata('success_msg', 'Successfully deleted');
				} else {
					$this->session->set_flashdata('faild_msg', 'Some thing was wrong');
				}
			}
		} else {
			redirect('login');
		}
	}
}
?>