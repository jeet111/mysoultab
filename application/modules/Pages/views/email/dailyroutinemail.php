<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<!--[if gte mso 9]>
	<xml>
		<o:OfficeDocumentSettings>
		<o:AllowPNG/>
		<o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="format-detection" content="date=no" />
	<meta name="format-detection" content="address=no" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="x-apple-disable-message-reformatting" />
    <!--[if !mso]><!-->
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/mail.css" rel="stylesheet">
    <!--<![endif]-->
	<title>Daily routine</title>
	
	

	
</head>
<body class="body">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f4f4f4">
		<tr>
			<td align="center" valign="top">
				<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
					<tr>
						<td class="td container">
							<!-- Header -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="sec1">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="p30-15">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<th class="column-top" width="145">
																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bottom-line">
																	<tr>
																		<td class="img m-center"><img src="<?php echo base_url(); ?>/assets/images/logo.png" width="100" height="50" border="0" alt="" class="img m-center"/></td>
																	</tr>
																</table>
															</th>
															
														</tr>
												<!--------main body----->
													</table>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="h3 pb20">Welcome to SoulTab</td>
														</tr>
														<tr>
															<td class="text pb20">Hello,
															 
															</td>
														</tr>
														<tr>
															<td class="text pb201">This is an alert email as you have missed your alarm for your morning dose of medicine.You have received this email because you are a subscribed user.
															 
															</td>
														</tr>
														<tr>
															<td class="text2 pb20">Here is profile information:
															 
															</td>
														</tr>
													</table>
								<?php
													if (!empty($check_dailyroutine)) {
													$morning = explode(",", $check_dailyroutine->morning);
													$noon  = explode(",", $check_dailyroutine->noon);
													$evening = explode(",", $check_dailyroutine->evening);
													$notes = explode(",", $check_dailyroutine->notes);
													$dinner = explode(",", $check_dailyroutine->dinner);
													$bedtime = explode(",", $check_dailyroutine->bedtime);
													} else {
													$morning = [];
													$noon  = [];
													$evening = [];
													$notes = [];
													$dinner = [];
													$bedtime = [];
													}
													?>
													<table width="100%" border="0" cellspacing="0" cellpadding="0" class="daily">
    														<thead>
    															<tr>
    															<th class="data pb20">Time</th>
    															<th class="data pb20">Meal</th>
    															<th class="data pb20">Snack</th>
    															<th class="data pb20">Medicine</th>
    															<th class="data pb20">Walk</th>
    															<th class="data pb20">Yoga</th>
    															<th class="data pb20">Meditation</th>
    															<th class="data pb201">Nap</th>
    															</tr>
    														</thead>
														<tbody>
														<tr>
															<td class="data pb20">Morning</td>
															<td class="cb"><input name="morning[]" type="checkbox" value="Meal" <?php if (in_array("Meal", $morning)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="morning[]" type="checkbox" value="Snack" <?php if (in_array("Snack", $morning)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="morning[]" type="checkbox" value="Medicine" <?php if (in_array("Medicine", $morning)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="morning[]" type="checkbox" value="Walk" <?php if (in_array("Walk", $morning)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="morning[]" type="checkbox" value="Yoga" <?php if (in_array("Yoga", $morning)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="morning[]" type="checkbox" value="Meditation" <?php if (in_array("Meditation", $morning)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="morning[]" type="checkbox" value="Nap" <?php if (in_array("Nap", $morning)) {
																	echo "checked";
																} ?>></td>
														</tr>
														<tr>
															<td class="data pb20">Noon</td>
															<td class="cb"><input name="noon[]" type="checkbox" value="Meal" <?php if (in_array("Meal", $noon)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="noon[]" type="checkbox" value="Snack" <?php if (in_array("Snack", $noon)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="noon[]" type="checkbox" value="Medicine" <?php if (in_array("Medicine", $noon)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="noon[]" type="checkbox" value="Walk" <?php if (in_array("Walk", $noon)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="noon[]" type="checkbox" value="Yoga" <?php if (in_array("Yoga", $noon)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="noon[]" type="checkbox" value="Meditation" <?php if (in_array("Meditation", $noon)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="noon[]" type="checkbox" value="Nap" <?php if (in_array("Nap", $noon)) {
																	echo "checked";
																} ?>></td>
														</tr>
														<tr>
															<td class="data pb20">Evening</td>
															<td class="cb"><input name="evening[]" type="checkbox" value="Meal" <?php if (in_array("Meal", $evening)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="evening[]" type="checkbox" value="Snack" <?php if (in_array("Snack", $evening)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="evening[]" type="checkbox" value="Medicine" <?php if (in_array("Medicine", $evening)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="evening[]" type="checkbox" value="Walk" <?php if (in_array("Walk", $evening)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="evening[]" type="checkbox" value="Yoga" <?php if (in_array("Yoga", $evening)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="evening[]" type="checkbox" value="Meditation" <?php if (in_array("Meditation", $evening)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="evening[]" type="checkbox" value="Nap" <?php if (in_array("Nap", $evening)) {
																	echo "checked";
																} ?>></td>
														</tr>
														<tr>
															<td class="data pb20">Dinner</td>
															<td class="cb"><input name="dinner[]" type="checkbox" value="Meal" <?php if (in_array("Meal", $dinner)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="dinner[]" type="checkbox" value="Snack" <?php if (in_array("Snack", $dinner)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="dinner[]" type="checkbox" value="Medicine" <?php if (in_array("Medicine", $dinner)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="dinner[]" type="checkbox" value="Walk" <?php if (in_array("Walk", $dinner)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="dinner[]" type="checkbox" value="Yoga" <?php if (in_array("Yoga", $dinner)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="dinner[]" type="checkbox" value="Meditation" <?php if (in_array("Meditation", $dinner)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="dinner[]" type="checkbox" value="Nap" <?php if (in_array("Nap", $dinner)) {
																	echo "checked";
																} ?>></td>
														</tr>
														<tr>
															<td class="data pb20">Bedtime</td>
															<td class="cb"><input name="bedtime[]" type="checkbox" value="Meal" <?php if (in_array("Meal", $bedtime)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="bedtime[]" type="checkbox" value="Snack" <?php if (in_array("Snack", $bedtime)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="bedtime[]" type="checkbox" value="Medicine" <?php if (in_array("Medicine", $bedtime)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="bedtime[]" type="checkbox" value="Walk" <?php if (in_array("Walk", $bedtime)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="bedtime[]" type="checkbox" value="Yoga" <?php if (in_array("Yoga", $bedtime)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="bedtime[]" type="checkbox" value="Meditation" <?php if (in_array("Meditation", $bedtime)) {
																	echo "checked";
																} ?>></td>
															<td class="cb"><input name="bedtime[]" type="checkbox" value="Nap" <?php if (in_array("Nap", $bedtime)) {
																	echo "checked";
																} ?>></td>
														</tr>


														</tbody>
														</table>
 							 					
  													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														
														<tr>
															<td class="text pb202" style="padding-top: 30px">What's next? 
															 
															</td>
														</tr>
														
														<tr>
															<td class="text pb201">To sign into your account,please visit <a href="<?php echo base_url(); ?>/login">www.mysoultab.com/login</a>
															</td>
														</tr>
														<tr>
															<td class="text pb201">Feel free to contact us at any minute,We would love to help you.Please visit <a href="#">www.mysoultab.com/contactus</a>
															</td>
														</tr>
													</table>

                                            <!------- end main body---------->


                                            <!-----------regards section---->
													<section class="one1">
    													<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td class="p30-155" bgcolor="#ffffff">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td align="center" valign="top" class="sec2"><img border="0" vspace="0" hspace="0" class="sec2" src="<?php echo base_url(); ?>/assets/images/team_two.jpg"alt="D" width="100%">
																			</td>
																		</tr>
											
																	</table>
																</td>
															</tr>
														</table>
 							 						</section>

  													<section class="two2">
    							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="p30-151" bgcolor="#ffffff">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											
											<tr>
												<td class="text-footer1 pb10">Best Regards</td>
											</tr>
											<tr>
												<td class="text-footer1 pb10">ABC</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20">Position</td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table>
  							</section>
							<!-- END Article / Title + Copy + Button -->

							
							<!-- Footer -->
							 <section class="one">
    							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="p30-152" bgcolor="#ffffff">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="left" class="sec3">
													<table>
														<tr>
															<td class="text12 pb20">Technical Support</td>
															
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20">Mail us at:</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20" >info@mysoultab.com</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20">Web :<a href="#">mysoultab.com</a></td>
											</tr>
											<tr>
												<td class="text-footer1 pb20">Tel no : 847 4501055</td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table>
 							 </section>

  							<section class="two">
    							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="p30-152" bgcolor="#ffffff">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="left" class="sec3">
													<table>
														<tr>
															<td class="text12 pb20">Information</td>
															
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20">Contact us</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20">About us </td>
											</tr>
											<tr>
												<td class="text-footer1 pb20">Privacy policy</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20">Terms of Use</td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table>
  							</section>


  							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="p30-152"  bgcolor="#ffffff">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="img" width="55" class="sec4"><a href="#" target="_blank"><img src="<?php echo base_url(); ?>/assets/images/t8_ico_facebook.jpg" width="38" height="38" border="0" alt="" /></a></td>
															<td class="img" width="55" class="sec4"><a href="#" target="_blank"><img src="<?php echo base_url(); ?>/assets/images/t8_ico_twitter.jpg" width="38" height="38" border="0" alt="" /></a></td>
															<td class="img" width="55" class="sec4"><a href="#" target="_blank"><img src="<?php echo base_url(); ?>/assets/images/t8_ico_instagram.jpg" width="38" height="38" border="0" alt="" /></a></td>
															<td class="img" width="38" class="sec4"><a href="#" target="_blank"><img src="<?php echo base_url(); ?>/assets/images/t8_ico_linkedin.jpg" width="38" height="38" border="0" alt="" /></a></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<!-- END footer -->

						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>




</body>
</html>
