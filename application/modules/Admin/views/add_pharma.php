<div class="container-fluid">
	<div class="dash-counter">
		<div class="Schedule_main_one">
			<div class="users-main">
				<?php
				echo "<h2>Add Pharma Company</h2>";
				?>

				<div class="btn_topBack">
					<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">
						Back
					</a>
				</div>

				<form class="form-horizontal" action="" method="post" enctype="multipart/form-data" name="AddUsers" id="pharma-form" >
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Name:<span class="field_req">*</span></label>
							<div class="col-md-8">
								<input type="text" placeholder="Company Name." class="form-control" name="company_name" id="username" value="<?php echo set_value('company_name'); ?>">
								<?php echo form_error('company_name'); ?>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Email:<span class="field_req">*</span></label>
							<div class="col-md-8">

								<input name="company_email" type="text" placeholder="Company Email." class="form-control"  id="" value="<?php echo set_value('company_email'); ?>">
								<?php echo form_error('company_email'); ?>
							</div>
						</div>
					</div>


					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Contact:<span class="field_req">*</span></label>
							<div class="col-md-8">

								<input name="company_contact" type="text" placeholder="Company Contact." class="form-control"  id="" value="<?php echo set_value('company_contact'); ?>">
								<?php echo form_error('company_contact'); ?>
							</div>
						</div>
					</div>


					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Address:<span class="field_req">*</span></label>
							<div class="col-md-8">

								<input name="company_address" type="text" placeholder="Company Address." class="form-control"  id="" value="<?php echo set_value('company_address'); ?>">
								<?php echo form_error('company_address'); ?>
							</div>
						</div>
					</div>

					<div class="col-md-8">
						<div class="bnt_sub">
							<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit" onclick="showFileSize()">

							<!-- <a class="cancel-btn btn" href="javascript:window.history.back()">
                        Back
                    </a> -->
                </div>
            </div>
        </div>
    </div>
</form>
</div>
</div>
</div>
</div>
<!--Added by 95 on 5-2-2019 For Doctor category for validation-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#pharma-form').validate({
			rules: {
				company_name: 'required',

				company_email:{
					required: true,
					email: true
				},

				company_contact: {
					required: true,
					number:true,
					minlength:10,
					maxlength:12,
					
				},
				company_address: 'required',

			},



			messages: {
				company_name: 'Please enter company name.',
				company_email:{
					required:'Please enter company email.',
					email: 'Please enter a valid email.'
				},
				
				company_contact: {
					required: 'Please enter contact number.',
					number:'Please enter only numbers.',
					minlength:"Please enter at least 10 digits.",
					maxlength:"Please do not enter more than 12 digits.",
				},
				company_address: 'Please enter address.',


				//company_email:'Please enter company email.',
			}
		});
	});
</script>