<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Yoga_video extends MX_Controller
{
  private $staff_permission = array();
  private $access_permission = true;
  public function __construct()
  {
    parent::__construct();
    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->model('movie_model');
    $this->load->model('yoga_video_model');
    $this->load->model('Common_model');
    $this->load->model('login_model');
    $this->load->helper(array('url'));
    $this->load->helper(array('form'));
    $user_id = $this->session->userdata('logged_ins')['id'];
    // print_r($user_id);
    if (empty($user_id)) {
      redirect('admin/login');
    }

    if ($this->session->userdata('logged_ins')['user_role'] == 5) {
      $uid = $this->session->userdata('logged_ins')['id'];
      $permission_list = $this->login_model->getAllRecordsById('staff_permission', array('user_id' => $uid));
      if (!empty($permission_list)) {
        foreach ($permission_list as $k => $v) {
          $this->staff_permission[$v['permission_id']] = $v;
        }
      }
    }
  }

  public function permission_access($permission_id = 14)
  {
    if ($this->session->userdata('logged_ins')['user_role'] != 1) {
      if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) {
        $this->access_permission = false;
      }
    }
  }

  /*Created by 95 for show all movie*/
  public function yoga_video_list()
  {
    $user_id = $this->session->userdata('logged_ins')['id'];
    $data['menuactive'] = $this->uri->segment(2);
    $data['YogaData'] = $this->yoga_video_model->getAllyogavideos();

    $data['staff_permission'] = $this->staff_permission;

    $this->permission_access(14);
    if ($this->access_permission) {
      $this->template->set('title', 'yoga Video');
      $this->template->load('admin_dashboard_layout', 'contents', 'yoga_video_list', $data);
    } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents', 'no_access', $data);
    }
  }


  /*Created by 95 for delete movie*/
  public function delete_yoga_video($yoga_id = '')
  {
    $image_name = $this->Common_model->getsingle("yoga_videos", array("yoga_id" => $yoga_id));
    // print_R($image_name);
    unlink("uploads/yoga_videos/" . $image_name->yoga_file);
    unlink("uploads/yoga_videos/image/" . $image_name->yoga_image);
    $this->yoga_video_model->delete('yoga_videos', array('yoga_id' => $yoga_id));
    // $this->movie_model->delete('cp_yoga_videos_favorite',array('yoga_id' =>$yoga_id));
    $this->session->set_flashdata('success', 'Deleted successfully.');
    redirect('admin/yoga_video_list');
  }


  /*Created by 95 for add movie*/
  public function add_yoga_video()
  {
    $user_id = $this->session->userdata('logged_ins')['id'];
    // _dx($user_id);
    $data['menuactive'] = $this->uri->segment(2);
    $this->form_validation->set_rules('yoga_title', 'Yoga Video Title', 'required');
    $this->form_validation->set_rules('yoga_desc', 'Yoga Video Description', 'required');
    $this->form_validation->set_rules('yoga_level', 'Yoga Video Level', 'required');
    $this->form_validation->set_rules('yoga_type', 'Yoga Video Type', 'required');

    if ($this->form_validation->run() == TRUE) {
      if ($_POST['submit']) {
        /*for audio file*/


        /*for image file*/
        $random1 = $this->generateRandomString(10);
        $ext = pathinfo($_FILES['pictureFile']['name'], PATHINFO_EXTENSION);
        $file_name1 = $random1 . "." . $ext;
        $dirpath = './././uploads/yoga_videos/image/' . $file_name1;
        if (move_uploaded_file($_FILES['pictureFile']['tmp_name'], $dirpath)) {
          $data['movie_pic'] = $file_name1;
        }
        if (!empty($data['movie_pic'])) {
          $pictureFile = $data['movie_pic'];

          /*For image resize*/
          $this->load->library('image_lib');
          $configer =  array(
            'image_library'   => 'gd2',
            'source_image'    =>  $dirpath,
            'maintain_ratio'  =>  TRUE,
            'width'           =>  150,
            'height'          =>  150,
          );
          $this->image_lib->clear();
          $this->image_lib->initialize($configer);
          $this->image_lib->resize();
        } else {
          $pictureFile = '';
        }
        if (!empty($data['movie_pic'])) {
          $pictureFile = $data['movie_pic'];
        } else {
          $pictureFile = '';
        }
        $random = $this->generateRandomString(10);
        $ext_video = pathinfo($_FILES['video_upload']['name'], PATHINFO_EXTENSION);
        $file_name_video = $random . "." . $ext_video;
        $dirpath_video = './././uploads/yoga_videos/' . $file_name_video;
        if (move_uploaded_file($_FILES['video_upload']['tmp_name'], $dirpath_video)) {
          $data['video_upload'] = $file_name_video;
        }
        if (!empty($data['video_upload'])) {
          $video = $data['video_upload'];
        } else {
          $video = '';
        }
        $data = array(
          'user_id' =>  $user_id,
          'yoga_title' => $this->input->post('yoga_title'),

          'help_with' => $this->input->post('help_with'),
          'body_part' => $this->input->post('body_part'),
          'yoga_style' => $this->input->post('yoga_style'),
          'yoga_pose' => $this->input->post('yoga_pose'),
          'yoga_desc' => $this->input->post('yoga_desc'),
          'yoga_duration' => $this->input->post('yoga_duration'),
          'yoga_url' => $this->input->post('yoga_url'),
          'yoga_type' => $this->input->post('yoga_type'),
          'yoga_image' =>  $pictureFile,
          'yoga_level' => $this->input->post('yoga_level'),

          'yoga_video' => $video,
          'active' => '1',
          'yoga_created' => date('Y-m-d H:i:s')
        );

        // print_r($data);
        // die;
        $this->yoga_video_model->insertData('yoga_videos', $data);
        $this->session->set_flashdata('success', 'Yoga video added successfully.');
        redirect('admin/yoga_video_list');
      }
    }


    $data['staff_permission'] = $this->staff_permission;

    $this->permission_access(14);
    if ($this->access_permission) {
      $this->template->set('title', 'Yoga Video');
      $this->template->load('admin_dashboard_layout', 'contents', 'add_yoga_video', $data);
    } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents', 'no_access', $data);
    }
  }



  public function generateRandomString($length = 12)
  {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }


  /*Created by 95 for edit movie*/
  public function edit_yoga_video($yoga_id = '')
  {
    $data['menuactive'] = $this->uri->segment(2);
    $this->form_validation->set_rules('yoga_title', 'Yoga Video Title', 'required');
    $this->form_validation->set_rules('yoga_desc', 'Yoga Video Description', 'required');
    $this->form_validation->set_rules('yoga_level', 'Yoga Video Level', 'required');
    $this->form_validation->set_rules('yoga_type', 'Yoga Video Type', 'required');

    //  $data['category_name'] = $this->Common_model->getAllwhereorderby("cp_meditation_videos_category",array("movie_category_status" => 1),"movie_category_name","asc");
    if ($this->form_validation->run() == TRUE) {

      if ($_POST['submit']) {

        /*for image file*/
        $random1 = $this->generateRandomString(10);
        $random = $this->generateRandomString(10);
        $ext_video = pathinfo($_FILES['video_upload']['name'], PATHINFO_EXTENSION);
        $file_name = $random . "." . $ext_video;
        $dirpath = './././uploads/yoga_videos/' . $file_name;
        if (move_uploaded_file($_FILES['video_upload']['tmp_name'], $dirpath)) {
          $data['new_pic'] = $file_name;
        }
        if (!empty($data['new_pic'])) {
          $picture = $data['new_pic'];
        } else {
          $picture = '';
        }

        $ext = pathinfo($_FILES['pictureFile']['name'], PATHINFO_EXTENSION);
        $file_name1 = $random1 . "." . $ext;
        $dirpath = './././uploads/yoga_videos/image/' . $file_name1;
        if (move_uploaded_file($_FILES['pictureFile']['tmp_name'], $dirpath)) {
          $data['music_pic'] = $file_name1;
        }
        if (!empty($data['music_pic'])) {
          $pictureFile = $data['music_pic'];

          /*For image resize*/
          $this->load->library('image_lib');
          $configer =  array(
            'image_library'   => 'gd2',
            'source_image'    =>  $dirpath,
            'maintain_ratio'  =>  TRUE,
            'width'           =>  150,
            'height'          =>  150,
          );
          $this->image_lib->clear();
          $this->image_lib->initialize($configer);
          $this->image_lib->resize();
        } else {
          $pictureFile = '';
        }


        if (!empty($pictureFile)) {

          $data = array(
            'user_id' =>  $user_id,
            'yoga_title' => $this->input->post('yoga_title'),
            'help_with' => $this->input->post('help_with'),
            'body_part' => $this->input->post('body_part'),
            'yoga_style' => $this->input->post('yoga_style'),
            'yoga_pose' => $this->input->post('yoga_pose'),

            'yoga_desc' => $this->input->post('yoga_desc'),
            'yoga_duration' => $this->input->post('yoga_duration'),
            'yoga_url' => $this->input->post('yoga_url'),
            'yoga_type' => $this->input->post('yoga_type'),

            'yoga_image' =>  $picturefile,
            'yoga_level' => $this->input->post('yoga_level'),


            'yoga_video' => $picture,
            'active' => '1',
            'yoga_created' => date('Y-m-d H:i:s')
          );

          $this->yoga_video_model->updateData('yoga_videos', $data, array('yoga_id' => $yoga_id));
          $this->session->set_flashdata('success', 'Yoga Video updated successfully.');
          redirect('admin/yoga_video_list');
        } else if (!empty($picture)) {

          $data = array(
            'user_id' =>  $user_id,
            'yoga_title' => $this->input->post('yoga_title'),
            'help_with' => $this->input->post('help_with'),
            'body_part' => $this->input->post('body_part'),
            'yoga_style' => $this->input->post('yoga_style'),
            'yoga_pose' => $this->input->post('yoga_pose'),

            'yoga_desc' => $this->input->post('yoga_desc'),
            'yoga_duration' => $this->input->post('yoga_duration'),
            'yoga_url' => $this->input->post('yoga_url'),
            'yoga_type' => $this->input->post('yoga_type'),

            'yoga_image' =>  $picturefile,
            'yoga_level' => $this->input->post('yoga_level'),


            //  'yoga_video'=>$picture,
            'active' => '1',
            'yoga_created' => date('Y-m-d H:i:s')
          );

          $this->yoga_video_model->updateData('yoga_videos', $data, array('yoga_id' => $yoga_id));

          $this->session->set_flashdata('success', 'Yoga Video updated successfully.');
          redirect('admin/yoga_video_list');
        } else if (empty($pictureFile) || $pictureFile == '' && empty($picture) || $picture == '') {

          $data = array(
            'user_id' =>  $user_id,
            'yoga_title' => $this->input->post('yoga_title'),
            'help_with' => $this->input->post('help_with'),
            'body_part' => $this->input->post('body_part'),
            'yoga_style' => $this->input->post('yoga_style'),
            'yoga_pose' => $this->input->post('yoga_pose'),

            'yoga_desc' => $this->input->post('yoga_desc'),
            'yoga_duration' => $this->input->post('yoga_duration'),
            'yoga_url' => $this->input->post('yoga_url'),
            'yoga_type' => $this->input->post('yoga_type'),

            'yoga_image' =>  $picturefile,
            'yoga_level' => $this->input->post('yoga_level'),


            'yoga_video' => $picture,
            'active' => '1',
            'yoga_created' => date('Y-m-d H:i:s')
          );

          $this->yoga_video_model->updateData('yoga_videos', $data, array('yoga_id' => $yoga_id));

          $this->session->set_flashdata('success', 'Yoga Video updated successfully.');
          redirect('admin/yoga_video_list');
        }
      }
    }

    $data['editYoga'] = $this->yoga_video_model->getsingle('yoga_videos', array('yoga_id' => $yoga_id));

    $data['staff_permission'] = $this->staff_permission;

    $this->permission_access(14);
    if ($this->access_permission) {
      $this->template->set('title', 'Yoga Viedo');
      $this->template->load('admin_dashboard_layout', 'contents', 'edit_yoga_video', $data);
    } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents', 'no_access', $data);
    }
  }

  public function view_yoga_video($yoga_id = '')
  {
    $data['menuactive'] = $this->uri->segment(2);
    $data['viewYoga'] = $this->yoga_video_model->getsingle('yoga_videos', array('yoga_id' => $yoga_id));

    $data['staff_permission'] = $this->staff_permission;

    $this->permission_access(14);
    if ($this->access_permission) {
      $this->template->set('title', 'Yoga Viedo');
      $this->template->load('admin_dashboard_layout', 'contents', 'view_yoga_video', $data);
    } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents', 'no_access', $data);
    }
  }


  public function ajax_yoga_add()
  {

    $user_id = $this->session->userdata('logged_ins')['id'];
    if ($_FILES["pictureFile"]["error"] > 0) {
    } else {

      $random = $this->generateRandomString(10);
      $ext_video = pathinfo($_FILES['video_upload']['name'], PATHINFO_EXTENSION);
      $file_name_video = $random . "." . $ext_video;
      $dirpath_video = './././uploads/yoga_videos/' . $file_name_video;
      if (move_uploaded_file($_FILES['video_upload']['tmp_name'], $dirpath_video)) {
        $data['video_upload'] = $file_name_video;
      }
      if (!empty($data['video_upload'])) {
        $video = $data['video_upload'];
      } else {
        $video = '';
      }

      $config2['upload_path'] = 'uploads/yoga_videos/image/';
      $config2['allowed_types'] = 'jpg|png|gif|jpeg';
      //$data['max_size'] = '5000';
      $config2['encrypt_name'] = true;

      $this->upload->initialize($config2);


      if ($this->upload->do_upload('pictureFile')) {
        $attachment_data_image = array('upload_data' => $this->upload->data());
        $picturefile = $attachment_data_image['upload_data']['file_name'];
        $return = array('code' => 1, 'message' => 'Successfully added');


        /*For image resize*/
        $this->load->library('image_lib');
        $configer =  array(
          'image_library'   => 'gd2',
          'source_image'    =>  $attachment_data_image['upload_data']['full_path'],
          'maintain_ratio'  =>  TRUE,
          'width'           =>  150,
          'height'          =>  150,
        );
        $this->image_lib->clear();
        $this->image_lib->initialize($configer);
        $this->image_lib->resize();
      } else {
        $return = array('code' => 2, 'message' => strip_tags($this->upload->display_errors()));
      }
      //  if($return['code'] != 2){ 
      $array = array(
        'user_id' =>  $user_id,
        'yoga_title' => $this->input->post('yoga_title'),
        'body_part' => $this->input->post('body_part'),
        'yoga_style' => $this->input->post('yoga_style'),
        'yoga_pose' => $this->input->post('yoga_pose'),

        'yoga_desc' => $this->input->post('yoga_desc'),
        'yoga_duration' => $this->input->post('yoga_duration'),
        'yoga_url' => $this->input->post('yoga_url'),
        'yoga_type' => $this->input->post('yoga_type'),
        'yoga_level' => $this->input->post('yoga_level'),
        'yoga_image' =>  $picturefile,
        'yoga_video' => $video,
        //  'category_id' => $this->input->post('movie_type'),
        'active' => '1',
        'yoga_created' => date('Y-m-d H:i:s')
      );
      //  _dx($array);
      $this->Common_model->insertData("yoga_videos", $array);
    }

    echo json_encode($return);
  }

  public function image_upload()
  {
    $config1['upload_path'] = 'uploads/movie/';
    $config1['allowed_types'] = 'mp4|flv|wmv|webm|vob|ogg|mpeg|mpg|3gp';

    $config1['encrypt_name'] = true;
    $this->load->library('upload', $config1);


    if ($this->upload->do_upload('picture')) {
      $attachment_data = array('upload_data' => $this->upload->data());
      $uploadfile = $attachment_data['upload_data']['file_name'];
      $return = array("code" => 1);
    }
    echo json_encode($return);
  }

  public function ajax_yoga_edit()
  {
    $yoga_id = $this->input->post('yoga_id');

    $singleData = $this->yoga_video_model->getsingle("yoga_videos", array("yoga_id" => $yoga_id));

    $config1['upload_path'] = 'uploads/yoga_videos/';
    $config1['allowed_types'] = 'mp4|flv|wmv|webm|vob|ogg|mpeg|mpg|3gp';

    //$data['max_size'] = '5000';
    $config1['encrypt_name'] = true;
    $this->upload->initialize($config1);
    //  _d($config1);
    $this->load->library('upload', $config1);
    if ($this->upload->do_upload('video_upload')) {
      $attachment_data_video = array('upload_data' => $this->upload->data());
      $video = $attachment_data_video['upload_data']['file_name'];
      // _d($video);
    }
    $return = array('code' => 1, 'message' => 'Successfully added');
    $config2['upload_path'] = 'uploads/yoga_videos/image/';
    $config2['allowed_types'] = 'jpg|png|gif|jpeg';
    //$data['max_size'] = '5000';
    $config2['encrypt_name'] = true;

    $this->upload->initialize($config2);

    if (!empty($_FILES["pictureFile"]["name"])) {
      if ($this->upload->do_upload('pictureFile')) {
        $attachment_data_image = array('upload_data' => $this->upload->data());
        $picturefile = $attachment_data_image['upload_data']['file_name'];
        $return = array('code' => 1, 'message' => 'Successfully added');
        /*For image resize*/
        $this->load->library('image_lib');
        $configer =  array(
          'image_library'   => 'gd2',
          'source_image'    =>  $attachment_data_image['upload_data']['full_path'],
          'maintain_ratio'  =>  TRUE,
          'width'           =>  150,
          'height'          =>  150,
        );
        $this->image_lib->clear();
        $this->image_lib->initialize($configer);
        $this->image_lib->resize();
      } else {
        $return = array('code' => 2, 'message' => strip_tags($this->upload->display_errors()));
      }
    }
    if (!empty($video)) {
      $video = $video;
    } else {
      $video = $singleData->yoga_video;
    }
    if (!empty($picturefile)) {
      $picfile = $picturefile;
    } else {
      $picfile = $singleData->yoga_image;
    }
    if ($return['code'] != 2) {
      $array = array(
        'yoga_title' => $this->input->post('yoga_title'),
        'body_part' => $this->input->post('body_part'),
        'yoga_style' => $this->input->post('yoga_style'),
        'yoga_pose' => $this->input->post('yoga_pose'),
        'yoga_desc' => $this->input->post('yoga_desc'),
        'yoga_duration' => $this->input->post('yoga_duration'),
        'yoga_url' => $this->input->post('yoga_url'),
        'yoga_type' => $this->input->post('yoga_type'),
        'yoga_image' =>  $picfile,
        'yoga_level' => $this->input->post('yoga_level'),
        'yoga_video' =>  $video,
        //  'category_id' => $this->input->post('movie_type'),
        'active' => '1',
        'yoga_created' => date('Y-m-d H:i:s')
      );
      $this->yoga_video_model->updateData("yoga_videos", $array, array('yoga_id' => $this->input->post("yoga_id")));

      $return = array('code' => 1, 'message' => 'Successfully updated.');
    } else {
      echo "outer";
      die;
    }
    $this->output->set_content_type('application/json');


    echo json_encode($return);
  }
}
