<?php

if (isset($staff_permission[14]['add_permission']) == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn">

			<a href="<?php echo base_url(); ?>admin/add_meditation_video_banner">

				Add Banner

			</a>

		</div>

	</div>

	<?php

} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn">

			<a href="<?php echo base_url(); ?>admin/add_meditation_video_banner">

				Add Banner

			</a>

		</div>

	</div>

	<?php

}

?>



<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="row">

			<div class="col-md-12">

				<div class="">



					<div id="deletemsg" style="text-align: center;"></div>

					

					<?php //echo "<pre>"; print_r($MovieBannerData); echo "</pre>";?>

					<div class="card-body">

						<div class="status-cng"></div>

						<?php if ($this->session->flashdata('success')) { ?>

							<div class="alert alert-success message">

								<button type="button" class="close" data-dismiss="alert">x</button>

								<?php echo $this->session->flashdata('success'); ?></div>

							<?php } ?>





							<!-- <div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_movie_banner">Add Banner</a></div> -->





							<table class="table table-hover tab_comn" id="sampleTable">

								<thead>

									<tr>

										<th>S no.</th>

										<th>Banner Title</th>



										<th>Banner Image</th>

										<th>Banner Status</th>							

										<th>Action</th>

									</tr>

								</thead>

								<tbody>

									<?php 

									$i = 1;

									foreach ($MovieBannerData as $MovieBanner) {

										?>

										<tr>

											<td><?php echo $i++; ?></td>

											<td><?php echo $MovieBanner->movie_banner_text; ?></td>



											<td><img src="<?php echo base_url('uploads/meditation_videos/banners/'.$MovieBanner->movie_banner_image) ?>" height="50px" width="100px">    </td>





											<td>

												<?php if($MovieBanner->movie_banner_status==1){ ?>



													<span class="testing_<?php echo $MovieBanner->movie_banner_id; ?>"><button type="" class="btn ybt btn-xs"  disabled="">Active</button></span>



												<?php }else{ ?>



													<span class="testing_<?php echo $MovieBanner->movie_banner_id; ?>"><button type="" class="btn xbt btn-xs"  disabled="">Inactive</button></span>





												<?php } ?>



											</td>



											<td>

												<div class="link-del-view">

													<?php

													if (isset($staff_permission[14]['view_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/view_meditation_video_banner/<?php echo $MovieBanner->movie_banner_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

															<span class="tooltiptext">View</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/view_meditation_video_banner/<?php echo $MovieBanner->movie_banner_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

															<span class="tooltiptext">View</span>

														</div>

														<?php

													}

													?>

													

													<?php

													if (isset($staff_permission[14]['edit_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/edit_meditation_video_banner/<?php echo $MovieBanner->movie_banner_id; ?>">

																<i class="fa fa-edit" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">

																Edit

															</span>

														</div>



														<?php if($MovieBanner->movie_banner_status == 1) { ?>

															<div class="tooltip-2"><a href="javascript:void(0);" onclick="return updateStatusBanner(<?php echo $MovieBanner->movie_banner_id; ?>);"><span class="test_<?php echo $MovieBanner->movie_banner_id; ?>"><i class="fa fa-toggle-on" aria-hidden="true"></i></span></a>

																<span class="tooltiptext">Active</span>

															</div>

														<?php }else { ?>

															<div class="tooltip-2"><a href="javascript:void(0);" onclick="return updateStatusBanner(<?php echo $MovieBanner->movie_banner_id; ?>);"><span class="test_<?php echo $MovieBanner->movie_banner_id; ?>"><i class="fa fa-toggle-off" aria-hidden="true"></i></span></a>

																<span class="tooltiptext">Deactive</span>

															</div>

														<?php } ?>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/edit_meditation_video_banner/<?php echo $MovieBanner->movie_banner_id; ?>">

																<i class="fa fa-edit" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">

																Edit

															</span>

														</div>



														<?php if($MovieBanner->movie_banner_status == 1) { ?>

															<div class="tooltip-2"><a href="javascript:void(0);" onclick="return updateStatusBanner(<?php echo $MovieBanner->movie_banner_id; ?>);"><span class="test_<?php echo $MovieBanner->movie_banner_id; ?>"><i class="fa fa-toggle-on" aria-hidden="true"></i></span></a>

																<span class="tooltiptext">Active</span>

															</div>

														<?php }else { ?>

															<div class="tooltip-2"><a href="javascript:void(0);" onclick="return updateStatusBanner(<?php echo $MovieBanner->movie_banner_id; ?>);"><span class="test_<?php echo $MovieBanner->movie_banner_id; ?>"><i class="fa fa-toggle-off" aria-hidden="true"></i></span></a>

																<span class="tooltiptext">Deactive</span>

															</div>

														<?php } ?>

														<?php

													}

													?>

													



													



													<?php

													if (isset($staff_permission[14]['delete_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/delete_meditation_video_banner/<?php echo $MovieBanner->movie_banner_id; ?>" onclick="return confirm('Are you sure, you want to delete this banner ?')">

																<i class="fa fa-trash-o" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">

																Delete

															</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/delete_meditation_video_banner/<?php echo $MovieBanner->movie_banner_id; ?>" onclick="return confirm('Are you sure, you want to delete this banner ?')">

																<i class="fa fa-trash-o" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">

																Delete

															</span>

														</div>

														<?php

													}

													?>

													





												</div>



											</div>

										</td>

									</tr>

								<?php } ?>

							</tbody>

						</table>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>



<script type="text/javascript">

	function updateStatusBanner(banner_id = '')

	{

		$.ajax({

			url:"<?php echo base_url()?>Admin/MeditationvideoBanner/updateBanner",

			method:"POST",

			data:{'banner_id':banner_id},

  //dataType:"json",

  success:function(data)

  {

  	if(data == 1){

  		$(".test_"+banner_id).html('<i class="fa fa-toggle-on" aria-hidden="true"></i>');

  		$(".testing_"+banner_id).html('<button type="" class="btn ybt btn-xs"  disabled="">Active</button>');

					//$("#deletemsg").html("<div class='alert alert-success'>Category active successfully</div>");

					$('#deletemsg').html("<div class='alert alert-success'>Banner activate successfully</div>").fadeIn().delay(1000).fadeOut();	

				}else{

					$(".test_"+banner_id).html('<i class="fa fa-toggle-off" aria-hidden="true"></i>');

					$(".testing_"+banner_id).html('<button type="" class="btn xbt btn-xs"  disabled="">Inactive</button>');

                   // $("#deletemsg").html("<div class='alert alert-success'>Category deactive successfully</div>");

                   $('#deletemsg').html("<div class='alert alert-success'>Banner inactivate successfully</div>").fadeIn().delay(1000).fadeOut();				

               }  	

           }

       });

	}

</script>