<aside class="right-side">

    <section class="content-header no-margin">

        <h1 class="text-center"><i class="fa fa-list"></i> Contact List <div class="rem_add"><a href="<?php echo base_url();?>add_contact" class="btn btn-primary" >Add</a></div></h1>

    </section>

<?php //echo "<pre>"; print_r($TestReportData); echo "</pre>"; ?>

<div id="expire_msg"></div>

            <?php if ($this->session->flashdata('success')) { ?>

            <div class="alert alert-success message">

                <button type="button" class="close" data-dismiss="alert">x</button>

            <?php echo $this->session->flashdata('success'); ?></div>

            <?php } ?>

<section class="content photo-list">

    <div class="photo-listMain reminder_listMain">

        <div class="row swt_cor">

            <div class="col-sm-6">

                <label style="margin-right: 10px;">

                    <input type="checkbox" id="checkallContacts">

                </label>

                <div class="delete-email">

                    <a href="#" name="contact_all_del" id="contact_all_del" class="contact_all_del" data-base="<?php echo base_url();?>"><i aria-hidden="true" class="fa fa-trash-o"></i></a>

                </div>

            </div>

        </div>

        <div class="table-responsive tb_swt">

            

            <table class="table table-hover table-bordered table-mailbox send-user-mail tab_medi" id="sampleTable">

                <thead>

                    <tr>

                        <th>#</th>

                        <th>Contact Name</th>

                        <th>Contact Number</th>

                        <!-- <th>Description</th> -->

                        <th>Action</th>

                    </tr>

                </thead>

                <tbody >

                    <?php foreach ($ContactData as $contact) {

                    ?>

                    <tr>

                        <td class="small-col" >

                            <input type="checkbox" name="contact_chk[]"  class="contact_chk" value="<?php echo $contact->contact_id;?>" data-id="<?php echo $report->report_id;  ?>"/>

                        </td>

                        <td><?php echo $contact->contact_name; ?></td>

                        <td><?php echo $contact->contact_mobile_number; ?></td>

                        <!-- <td><?php echo $report->description; ?></td> -->

                        <td>







                            <a href="<?php echo base_url();?>view_contact/<?php echo $contact->contact_id;?>" class="edit_btn">View</a>



                            <a href="<?php echo base_url();?>contact_edit/<?php echo $contact->contact_id;?>" class="edit_btn">Edit</a>

                            <a href="<?php echo base_url();?>delete_contact/<?php echo $contact->contact_id;?>" onclick="return confirm('Are you sure, you want to remove contact?')" class="edit_btn">Delete</a>

                        </td>

                    </tr>

                    <?php } ?>

                </tbody>

            </table>

        </div>

    </div>



    

</section>

</aside>

<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>















<!-- <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->



