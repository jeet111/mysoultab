<?php defined('BASEPATH') or exit('No direct script access allowed');
class Reminder extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->load->model('Common_model_new');
		$this->load->helper(array('common_helper'));
		$this->load->helper(array('url'));
		$this->load->helper(array('form'));
	}
	public function reminder_list()
	{
		$user_id = $this->session->userdata('logged_in')['id'];

		if ($this->session->userdata('logged_in')) {
			$data = array();
			$data['menuactive'] = $this->uri->segment(1);
			$data['reminder_list'] = $this->Common_model_new->getAllwhereorderby("cp_reminder", array("user_id" => $this->session->userdata('logged_in')['id']), 'reminder_id', 'desc');

			$data['btnsetingArray'] = $this->Common_model_new->getAllwhere("setting_butns", array("btnname" => 'Reminder', "user_id" => $user_id));
			$this->template->set('title', 'Reminder');
			$this->template->load('new_user_dashboard_layout', 'contents', 'reminder_list', $data);
		} else {
			redirect('login');
		}
	}
	public function add_reminder()
	{
		if ($this->session->userdata('logged_in')) {
			$data = array();
			$data['menuactive'] = $this->uri->segment(1);
			$this->template->set('title', 'Reminder');
			$this->template->load('new_user_dashboard_layout', 'contents', 'add_reminder', $data);
		} else {
			redirect('login');
		}
	}
	public function add_reminder1()
	{
		$data = array();
		$userId = $this->session->userdata('logged_in')['id'];
		$data['menuactive'] = $this->uri->segment(1);
		$reminder_title = $this->input->post('reminder_title');

		$reminder_description = $this->input->post('reminder_description');
		$reminder_date = $this->input->post('reminder_date');
		$before_time = $this->input->post('before_time');
		$repeat_time = $this->input->post('repeat_time');
		$data['singleData'] =  array();

		$reminder_time = $this->input->post('reminder_time');
		$array = array(
			'user_id' => $this->session->userdata('logged_in')['id'],
			'reminder_title' => $reminder_title,
			'reminder_description' => $reminder_description,
			'reminder_date' => $reminder_date,
			'reminder_time' => $reminder_time,
			'reminder_before' => $before_time,
			'repeat' => $repeat_time,
			'reminder_create' => date('Y-m-d H:i:s')
		);
		$result = $this->Common_model_new->insertData('cp_reminder', $array);
		if ($result) {
			echo 1;
		} else {
			echo 0;
		}
	}
	public function edit_reminder()
	{
		if ($this->session->userdata('logged_in')) {
			$data = array();
			$data['menuactive'] = $this->uri->segment(1);
			$data['singleData'] = $this->Common_model_new->getsingle("cp_reminder", array('reminder_id' => $this->uri->segment('2')));
			$this->template->set('title', 'Reminder');
			$this->template->load('new_user_dashboard_layout', 'contents', 'edit_reminder', $data);
		} else {
			redirect('login');
		}
	}
	public function edit_reminder1()
	{
		$data = array();
		$data['menuactive'] = $this->uri->segment(1);
		$reminder_id = $this->input->post('reminder_id');
		$reminder_title = $this->input->post('reminder_title');
		$reminder_description = $this->input->post('reminder_description');
		$reminder_date = $this->input->post('reminder_date');
		$before_time = $this->input->post('before_time');
		$repeat_time = $this->input->post('repeat_time');
		$reminder_time = $this->input->post('reminder_time');
		$explode = explode('/', $reminder_date);
		$new_explode = $explode['2'] . '-' . $explode['0'] . '-' . $explode['1'];
		$array = array(
			'user_id' => $this->session->userdata('logged_in')['id'],
			'reminder_title' => $reminder_title,
			'reminder_description' => $reminder_description,
			'reminder_date' => $reminder_date,
			'reminder_time' => $reminder_time,
			'reminder_before' => $before_time,
			'repeat' => $repeat_time,
			'reminder_create' => date('Y-m-d H:i:s')
		);
		$result = $this->Common_model_new->updateData('cp_reminder', $array, array('reminder_id' => $reminder_id));
		if ($result) {
			echo 1;
		} else {
			echo 0;
		}
	}
	public function delete_checkboxes()
	{
		$data['menuactive'] = $this->uri->segment(1);
		$checkLogin = $this->session->userdata('logged_in');
		$explode = explode(',', $_POST["bodytype"]);
		foreach ($explode as $id) {
			$where_condition = array('reminder_id' => $id);
			$upd = $this->Common_model_new->delete('cp_reminder', $where_condition);
			//if($upd){
			echo 'success';
			// }
		}
	}
	public function delete_reminder()
	{
		$delete_id = $this->uri->segment('2');
		$this->Common_model_new->delete("cp_reminder", array("reminder_id" => $delete_id));
		$this->session->set_flashdata('success', 'Successfully deleted');
		redirect('reminder_list');
	}
	public function reminder_all_del()
	{
		$bodytype = $this->input->post("bodytype");
		$explode = explode(',', $bodytype);
		foreach ($explode as $del) {
			$this->Common_model_new->delete("cp_reminder", array("reminder_id" => $del));
		}
		echo '1';
	}
	public function reminder_notification()
	{
		$data['reminers'] = $this->Common_model_new->getAllor1("cp_reminder", 'reminder_id', 'DESC');
		foreach ($data['reminers'] as $reminder) {
			$reminder_date = date('d-m-Y', strtotime($reminder->reminder_date));
			if (strtotime($reminder->reminder_date) >= strtotime(date('d-m-Y'))) {
				$minuts = $reminder->reminder_before;
				$endTime = strtotime("-$minuts minutes", strtotime($reminder->reminder_time));
				$notifyTime = date('h:i A', $endTime);
				$current_time =	date('h:i A');
				if ($notifyTime == $current_time) {
					// echo "sent<br>";
					// echo $notifyTime."=".$current_time."<br><br><br>";
					$data['UserData'] = $this->Common_model_new->getsingle("cp_users", array('id' => $reminder->user_id));
					if (isset($data['UserData']->email)) {
						$user_email = $data['UserData']->email;
					}
					$message = $reminder->reminder_description;
					$subject = "Notification Email.";
					echo $check = sendEmail($user_email, $subject, $message);
					if ($check) {
						$NotificationData = array(
							'common_id' => $reminder->reminder_id,
							'user_id' => $reminder->user_id,
							'notification_date' => date("Y-md H:i:s"),
							'type' => '2',
						);
						$this->Common_model_new->insertData('cp_common_notification', $NotificationData);
						if ($reminder->repeat == 'Daily') {
							$next_date_daily = date('Y-m-d', strtotime('+1 day', strtotime($reminder->reminder_date)));
							$dailydata = array(
								'reminder_date' => $next_date_daily
							);
							$this->Common_model_new->updateData('cp_reminder', $dailydata, array('reminder_id' => $reminder->reminder_id));
						} else if ($reminder->repeat == 'Weekly') {
							$next_date_weekly = date('Y-m-d', strtotime('+7 day', strtotime($reminder->reminder_date)));
							$weeklydata = array(
								'reminder_date' => $next_date_weekly
							);
							$this->Common_model_new->updateData('cp_reminder', $weeklydata, array('reminder_id' => $reminder->reminder_id));
						} else if ($reminder->repeat == 'Monthly') {
							$next_date_monthly = date('Y-m-d', strtotime('+1 month', strtotime($reminder->reminder_date)));
							$monthlydata = array(
								'reminder_date' => $next_date_monthly
							);
							$this->Common_model_new->updateData('cp_reminder', $monthlydata, array('reminder_id' => $reminder->reminder_id));
						} else if ($reminder->repeat == 'Yearly') {
							$next_date_yearly = date('Y-m-d', strtotime('+1 year', strtotime($reminder->reminder_date)));
							$yearlydata = array(
								'reminder_date' => $next_date_yearly
							);
							$this->Common_model_new->updateData('cp_reminder', $yearlydata, array('reminder_id' => $reminder->reminder_id));
						}
					}
				}
				// else {
				// 	echo "not sent<br>";      
				// 	echo $notifyTime."=".$current_time."<br><br><br>";
				// }                  
			}
		}
	}
	public function reminder_notification1()
	{
		$data['reminers'] = $this->Common_model_new->getAllor1("cp_reminder", 'reminder_id', 'DESC');
		foreach ($data['reminers'] as $reminder) {
			$reminder_date = date('d-m-Y', strtotime($reminder->reminder_date));
			if (strtotime($reminder->reminder_date) >= strtotime(date('d-m-Y'))) {
				$minuts = $reminder->reminder_before;
				$endTime = strtotime("-$minuts minutes", strtotime($reminder->reminder_time));
				$notifyTime = date('h:i A', $endTime);
				$current_time =	date('h:i A');
				if ($notifyTime == $current_time) {
					// echo "sent<br>";
					// echo $notifyTime."=".$current_time."<br><br><br>";
					$data['UserData'] = $this->Common_model_new->getsingle("cp_users", array('id' => $reminder->user_id));
					if (isset($data['UserData']->email)) {
						$user_email = $data['UserData']->email;
					}
					$message = $reminder->reminder_description;
					$subject = "Notification Email.";
					echo $check = sendEmail($user_email, $subject, $message);
					if ($check) {
						$NotificationData = array(
							'notification_title' => $reminder->reminder_title,
							'notification_description' => $reminder->reminder_description,
							'notification_date' => $reminder->reminder_date,
							'notification_time' => $reminder->reminder_time,
							'notification_user_id' => $reminder->user_id,
							'notification_before' => $reminder->reminder_before,
							'notification_repeat' => $reminder->repeat,
						);
						$this->Common_model_new->insertData('user_reminder_notifications', $NotificationData);
						if ($reminder->repeat == 'Daily') {
							$next_date_daily = date('Y-m-d', strtotime('+1 day', strtotime($reminder->reminder_date)));
							$dailydata = array(
								'reminder_date' => $next_date_daily
							);
							$this->Common_model_new->updateData('cp_reminder', $dailydata, array('reminder_id' => $reminder->reminder_id));
						} else if ($reminder->repeat == 'Weekly') {
							$next_date_weekly = date('Y-m-d', strtotime('+7 day', strtotime($reminder->reminder_date)));
							$weeklydata = array(
								'reminder_date' => $next_date_weekly
							);
							$this->Common_model_new->updateData('cp_reminder', $weeklydata, array('reminder_id' => $reminder->reminder_id));
						} else if ($reminder->repeat == 'Monthly') {
							$next_date_monthly = date('Y-m-d', strtotime('+1 month', strtotime($reminder->reminder_date)));
							$monthlydata = array(
								'reminder_date' => $next_date_monthly
							);
							$this->Common_model_new->updateData('cp_reminder', $monthlydata, array('reminder_id' => $reminder->reminder_id));
						} else if ($reminder->repeat == 'Yearly') {
							$next_date_yearly = date('Y-m-d', strtotime('+1 year', strtotime($reminder->reminder_date)));
							$yearlydata = array(
								'reminder_date' => $next_date_yearly
							);
							$this->Common_model_new->updateData('cp_reminder', $yearlydata, array('reminder_id' => $reminder->reminder_id));
						}
					}
				}
				// else {
				// 	echo "not sent<br>";      
				// 	echo $notifyTime."=".$current_time."<br><br><br>";
				// }                  
			}
		}
	}
	public function view_notification()
	{
		$notification_id = $this->uri->segment(2);
		$data['menuactive'] = $this->uri->segment(1);
		//$data['notification_detail']	= $this->Common_model_new->getsingle('cp_common_notification',array('notification_id'=>$notification_id));
		$data['notification_detail'] = $this->common_model->newjointwotable("cp_common_notification", "common_id", "cp_reminder", "reminder_id", array("cp_common_notification.notification_id" => $notification_id, "cp_common_notification.user_id" => $this->session->userdata('logged_in')['id']), "*", "notification_id", "desc");
		$notiData = array(
			'is_read' => 1
		);
		$this->Common_model_new->updateData('cp_common_notification', $notiData, array('notification_id' => $notification_id));
		$this->template->set('title', 'Notification');
		$this->template->load('new_user_dashboard_layout', 'contents', 'view_notification', $data);
	}
	public function view_reminder()
	{
		$reminder_id = $this->uri->segment(2);
		$data['menuactive'] = $this->uri->segment(1);
		$data['notification_detail']	= $this->Common_model_new->getsingle('cp_reminder', array('reminder_id' => $reminder_id));
		$this->template->set('title', 'Reminder');
		$this->template->load('new_user_dashboard_layout', 'contents', 'view_reminder', $data);
	}
	/*For Now there is no use of view all notifications*/
	public function view_all_Notification()
	{
		$this->template->set('title', 'Notification');
		$this->template->load('new_user_dashboard_layout', 'contents', 'all_notifications', $data);
	}
	public function show_activities()
	{
		$checkLogin = $this->session->userdata('logged_in');

		if ($this->session->userdata('logged_in')) {
			$data = array();
			$data['btnsetingArray'] = $this->Common_model_new->getAllwhere("setting_butns", array("btnname" => 'Calendar', "user_id" => $checkLogin['id']));
			$data['reminderArray'] = $this->Common_model_new->getAllwhere("cp_reminder", array("user_id" => $checkLogin['id']));

			$this->template->set('title', 'All Activities');
			$this->template->load('new_user_dashboard_layout', 'contents', 'show_activities', $data);
		} else {
			redirect('login');
		}
	}

	public function add_event()
	{
		if ($this->session->userdata('logged_in')) {
			$data = array();
			$this->template->set('title', 'Add Event');
			$this->template->load('new_user_dashboard_layout', 'contents', 'add_event', $data);
		} else {
			redirect('login');
		}
	}


	public function add_event1()
	{
		$data = array();
		$data['menuactive'] = $this->uri->segment(1);
		$event_title = $this->input->post('event_title');
		$event_description = $this->input->post('event_description');
		$event_date = $this->input->post('event_date');
		$data['singleData'] =  array();
		$array = array(
			'user_id' => $this->session->userdata('logged_in')['id'],
			'event_title' => $event_title,
			'event_description' => $event_description,
			'event_date' => $event_date,
			'event_create' => date('Y-m-d H:i:s')
		);
		$result = $this->Common_model_new->insertData('cp_events', $array);
		if ($result) {
			echo 1;
		} else {
			echo 0;
		}
	}
	/***********/
	public function yearwisedata()
	{

		$selecteddate = $this->input->post('year_dropdown');
		// _dx($selecteddate);
		$data_new = $this->Common_model_new->get_yearwise_result($selecteddate);
		if (!empty($data_new)) {

			$data = '';
			$data .= "<table border=1>" . "<tr>" .

				"<td>" . 'Date' . "</td>" .
				"<td>" . 'Event' . "</td>" .
				"<td>" . 'Description' . "</td>" .
				"</tr>";
			foreach ($data_new as $key => $event) {
				$data .= "<tr>" .
					"<td data-label='Date'>" . $event['reminder_date'] . "</td>" .
					"<td data-label='Event'>" . $event['reminder_title'] . "</td>" .
					"<td data-label='Description'>" . $event['reminder_description'] . "</td>" .
					"</tr>";
			}
			$data .= "</table>";
		} else {
			$data = '';
			$data .= "<table border=1>" . "<thead><tr>" .
				"<td>" . 'Date' . "</td>" .
				"<td>" . 'Event' . "</td>" .
				"<td>" . 'Description' . "</td>" .
				"</tr></thead>";

			$data .= "<tr>" .
				"<td colspan=3>" . 'Not Events' . "</td>" .

				"</tr>";

			$data .= "</table>";
		}
		echo $data;
	}
	/******Onclick Date wise */
	public function search_datewise_result()
	{

		$selecteddate = $this->input->post('dateon');

		$data_new = $this->Common_model_new->get_searched_dateresult($selecteddate);
		if (!empty($data_new)) {

			$data = '';
			$data .= "<table border=1>" . "<tr>" .

				"<td>" . 'Date' . "</td>" .
				"<td>" . 'Event' . "</td>" .
				"<td>" . 'Description' . "</td>" .
				"</tr>";
			foreach ($data_new as $key => $event) {
				$data .= "<tr>" .
					"<td data-label='Date'>" . $event['reminder_date'] . "</td>" .
					"<td data-label='Event'>" . $event['reminder_title'] . "</td>" .
					"<td data-label='Description'>" . $event['reminder_description'] . "</td>" .
					"</tr>";
			}
			$data .= "</table>";
		} else {
			$data = '';
			$data .= "<table border=1>" . "<thead><tr>" .
				"<td>" . 'Date' . "</td>" .
				"<td>" . 'Event' . "</td>" .
				"<td>" . 'Description' . "</td>" .
				"</tr></thead>";

			$data .= "<tr>" .
				"<td colspan=3>" . 'Not Events' . "</td>" .

				"</tr>";

			$data .= "</table>";
		}
		echo $data;
	}
	/*********Show month wise data */
	public function get_monthly_events()
	{

		$selectemonth = $this->input->post('monthValue');
		// _dx($selectemonth);
		$data_new = $this->Common_model_new->get_searched_monthwise_result($selectemonth);
		if (!empty($data_new)) {

			$data = '';
			$data .= "<table border=1>" . "<tr>" .

				"<td>" . 'Date' . "</td>" .
				"<td>" . 'Event' . "</td>" .
				"<td>" . 'Description' . "</td>" .
				"</tr>";
			foreach ($data_new as $key => $event) {
				$data .= "<tr>" .
					"<td data-label='Date'>" . $event['reminder_date'] . "</td>" .
					"<td data-label='Event'>" . $event['reminder_title'] . "</td>" .
					"<td data-label='Description'>" . $event['reminder_description'] . "</td>" .
					"</tr>";
			}
			$data .= "</table>";
		} else {
			$data = '';
			$data .= "<table border=1>" . "<thead><tr>" .
				"<td>" . 'Date' . "</td>" .
				"<td>" . 'Event' . "</td>" .
				"<td>" . 'Description' . "</td>" .
				"</tr></thead>";

			$data .= "<tr>" .
				"<td colspan=3>" . 'Not Events' . "</td>" .

				"</tr>";

			$data .= "</table>";
		}
		echo $data;
	}
	/******************/
	public function get_weekly()
	{
		$calenderDataBy = $this->input->post('getdata');
		// print_r(	$calenderDataBy);
		if ($calenderDataBy == 'daily') {
			$curentDate = date('Y-m-d');
			$dailyDate = date('Y-m-d');
			$user = $this->session->userdata('logged_in')['id'];
			$EvntDataDaily = $this->Common_model_new->getcalenderDataBy($curentDate, $dailyDate, $user);
			// _dx($EvntDataDaily);
			if (!empty($EvntDataDaily)) {
				$data = '';
				$data .= "<table border=1>" . "<thead><tr>" .

					"<td>" . 'Date' . "</td>" .
					"<td>" . 'Event' . "</td>" .
					"<td>" . 'Description' . "</td>" .
					"</tr></thead>";
				foreach ($EvntDataDaily as $key => $event) {
					$data .= "<tr>" .
						"<td data-label='Date'>" . $event['reminder_date'] . "</td>" .
						"<td data-label='Event'>" . $event['reminder_title'] . "</td>" .
						"<td data-label='Description'>" . $event['reminder_description'] . "</td>" .
						"</tr>";
				}
				$data .= "</table>";
			} else {
				$data = '';
				$data .= "<table border=1>" . "<thead><tr>" .
					"<td>" . 'Date' . "</td>" .
					"<td>" . 'Event' . "</td>" .
					"<td>" . 'Description' . "</td>" .
					"</tr></thead>";

				$data .= "<tr>" .
					"<td colspan=3>" . 'Not Events' . "</td>" .

					"</tr>";

				$data .= "</table>";
			}
			echo $data;

		} else if ($calenderDataBy == 'weekly') {

			$curentDate = date('Y-m-d');
			$weeklyDate = date('Y-m-d', strtotime('+7 day', strtotime(date('Y-m-d'))));
			$user = $this->session->userdata('logged_in')['id'];

			$EvntDataWeekly = $this->Common_model_new->getcalenderDataByWeekly($curentDate, $weeklyDate, $user);
// _dx($EvntDataWeekly);
			if (!empty($EvntDataWeekly)) {

				$data = '';
				$data .= "<table border=1>" . "<thead><tr>" .
					"<td>" . 'Date' . "</td>" .
					"<td>" . 'Event' . "</td>" .
					"<td>" . 'Description' . "</td>" .
					"</tr></thead>";
				foreach ($EvntDataWeekly as $key => $event) {
					$data .= "<tr>" .
						"<td data-label='Date'>" . $event['reminder_date'] . "</td>" .
						"<td data-label='Event'>" . $event['reminder_title'] . "</td>" .
						"<td data-label='Description'>" . $event['reminder_description'] . "</td>" .
						"</tr>";
				}
				$data .= "</table>";
			} else {
				$data = '';
				$data .= "<table border=1>" . "<thead><tr>" .

					"<td>" . 'Date' . "</td>" .
					"<td>" . 'Event' . "</td>" .
					"<td>" . 'Description' . "</td>" .
					"</tr></thead>";

				$data .= "<tr>" .
					"<td data-label='No Event' colspan=3>" . 'Not Events' . "</td>" .

					"</tr>";

				$data .= "</table>";
			}

			echo $data;

			//echo json_encode($EvntData);
		} else {

			$curentDate = date('Y-m-d');
			$monthlyDate = date('Y-m-d', strtotime('+30 day', strtotime(date('Y-m-d'))));
			$user = $this->session->userdata('logged_in')['id'];

			$monthlyDate = date('Y-m-d', strtotime('+7 day', strtotime(date('Y-m-d'))));
			$EvntData = $this->Common_model_new->getcalenderDataMonthly($curentDate, $monthlyDate, $user);

			if (!empty($EvntData)) {

				$data = '';
				$data .= "<table border=1>" . "<thead><tr>" .

					"<td>" . 'Date' . "</td>" .
					"<td>" . 'Event' . "</td>" .
					"<td>" . 'Description' . "</td>" .
					"</tr></thead>";
				foreach ($EvntData as $key => $event) {
					$data .= "<tr>" .
						"<td data-label='Date'>" . $event['reminder_date'] . "</td>" .
						"<td data-label='Event'>" . $event['reminder_title'] . "</td>" .
						"<td data-label='Description'>" . $event['reminder_description'] . "</td>" .
						"</tr>";
				}
				$data .= "</table>";
			} else {
				$data = '';
				$data .= "<table border=1>" . "<thead><tr>" .
					"<td>" . 'Date' . "</td>" .
					"<td>" . 'Event' . "</td>" .
					"<td>" . 'Description' . "</td>" .
					"</tr></thead>";

				$data .= "<tr>" .
					"<td colspan=3>" . 'Not Events' . "</td>" .

					"</tr>";

				$data .= "</table>";
			}
			echo $data;
		}
	}
}
