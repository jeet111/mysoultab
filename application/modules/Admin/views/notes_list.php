<style>
.text_heading {
    border-bottom: none !important;
    margin-bottom: 0;
}
</style>
<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="row">

			<div class="col-md-12">

				<div class="">

					<?php //echo "<pre>"; print_r($care_giver); echo "</pre>";?>

					<div class="card-body">

						<div class="status-cng"></div>

						<?php if ($this->session->flashdata('success')) { ?>

							<div class="alert alert-success message">

								<button type="button" class="close" data-dismiss="alert">x</button>

								<?php echo $this->session->flashdata('success'); ?></div>

							<?php } ?>

							<table class="table table-hover tab_comn" id="sampleTable">

								<thead>

									<tr>

										<th>S no.</th>

										<th>Username</th>

										<th>From caregiver</th>

										<th>To caregiver</th>

										<th>Description</th>

										<th>Action</th>

									</tr>

								</thead>

								<tbody>

									<?php 

									$i = 1;

									foreach ($note_list as $note) {

										?>

										<tr>

											<td><?php echo $i++; ?></td>

											<td><?php echo $note->username; ?></td>

											<td><?php 

											foreach ($care_giver as $value) {

												if($note->from_caregiver==$value->id){

													echo $value->username; 	

												}

											}

											?></td>

											<td><?php 

											foreach ($care_giver as $value) {

												if($note->to_caregiver==$value->id){

													echo $value->username; 	

												}

											}

											?></td>

											<td>



												<span class="ttt">

													<?php echo $note->description; ?>

												</span>

											</td>

											<td>

												<div class="link-del-view">

													<?php

													if (isset($staff_permission[2]['view_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/view_note/<?php echo $note->note_id; ?>">

																<i class="fa fa-eye" aria-hidden="true"></i></a>

																<span class="tooltiptext">View</span>

															</div>

															<?php

														} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

															?>

															<div class="tooltip-2">

																<a href="<?php echo base_url(); ?>admin/view_note/<?php echo $note->note_id; ?>">

																	<i class="fa fa-eye" aria-hidden="true"></i></a>

																	<span class="tooltiptext">View</span>

																</div>

																<?php

															}

															?>





															<?php

															if (isset($staff_permission[2]['delete_permission']) == 1) {

																?>

																<div class="tooltip-2">

																	<a href="<?php echo base_url(); ?>admin/delete_note/<?php echo $note->note_id; ?>" onclick="return confirm('Delete this Note ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

																</div>

																<?php

															} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

																?>

																<div class="tooltip-2">

																	<a href="<?php echo base_url(); ?>admin/delete_note/<?php echo $note->note_id; ?>" onclick="return confirm('Delete this Note ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

																</div>

																<?php

															}

															?>

														</div>

													</div>

												</td>

											</tr>

										<?php } ?>

									</tbody>

								</table>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>







		<style type="text/css">

			.ttt{

				display:inline-block;

				width:180px;

				white-space: nowrap;

				overflow:hidden !important;

				text-overflow: ellipsis;

			}

		</style>