

<div class="signup-page-new log_hhh_new" id="style-4">

  <section class="signup_new login-s_n_new">

    <!-- <img src="images/signup-bg.jpg" alt=""> -->

    <div class="container">

     <div class="row">



      <div class="all_log_in_new">



        <h1 class="left_logo_login"><img src="https://www.mysoultab.com/assets/images/logo.png"></h1>

        <h2 class="form-title rig_tx">Register Information</h2> 

        <?php

        if($this->session->flashdata('success')) {

         $message = $this->session->flashdata('success');

         ?>

         <div style="color:green;text-align: center;"><?php echo $message['message']; ?></div>

       <?php } ?>





       <form method="POST" id="signupform" class="signup-form" action="<?php echo base_url();?>signup/<?php echo base64_encode($plan_id); ?>" enctype="multipart/form-data">

        <input type="hidden" name="plan_id" value="<?php echo $plan_id; ?>">

        

        <?php if(!empty($tempUserDetail)){ ?>  

          <input type="hidden" name="tempUsr" value="<?php echo $tempUserDetail['id'] ?>">



        <?php }else{ ?>

          <input type="hidden" name="tempUsr" value="">

        <?php } ?>

        <div class="form-group sig_fm">



         <i class="fa fa-user-o" aria-hidden="true"></i>

         <label for="fname" class="new_fs">First name:</label>



         <input type="text" class="form-input fm_ctl" name="firstname" id="firstname" placeholder="Enter your first name" <?php if(!empty($tempUserDetail)){ ?> value="<?php echo $tempUserDetail['name']; ?>"  <?php }else{ ?>value="<?php echo set_value('firstname'); ?>" <?php } ?> onkeypress="return onlyAlphabets(event,this);"  data-toggle="tooltip" data-placement="top" title="Please enter first name."/>



         <?php echo form_error('firstname'); ?>

         <!-- <span id="errnamemsg"></span> -->

       </div>

       



       <div class="form-group sig_fm">

        <i class="fa fa-user-o" aria-hidden="true"></i>



        <label for="fname" class="new_fs">Last name:</label>

        <input type="text" class="form-input fm_ctl" name="lastname" id="lastname" placeholder="Enter your last name" <?php if(!empty($tempUserDetail)){ ?> value="<?php echo $tempUserDetail['lastname']; ?>"  <?php }else{ ?> value="<?php echo set_value('lastname'); ?>"  <?php } ?> onkeypress="return onlyAlphabets(event,this);" data-toggle="tooltip" data-placement="top" title="Please enter last name."/>



        <?php echo form_error('lastname'); ?>



      </div>







      <div class="form-group sig_fm">

        <i class="fa fa-road" aria-hidden="true"></i>

        <label for="fname" class="new_fs">Street name:</label>

        <input type="text" class="form-input fm_ctl" name="street_name" id="street_name" placeholder="Enter your street name" <?php if(!empty($tempUserDetail)){ ?> value="<?php echo $tempUserDetail['street_name']; ?>"  <?php }else{ ?> value="<?php echo set_value('street_name'); ?>" <?php } ?> data-toggle="tooltip" data-placement="top" title="Please enter your street name.."/>



        <?php echo form_error('street_name'); ?>



  <!-- <label for="fname">Street name:</label>

  <input type="text" class="form-input" name="address" id="address" placeholder="Enter your address" value="<?php echo set_value('address'); ?>" />



  <?php echo form_error('address'); ?> -->



</div>



<div class="form-group sig_fm">

  <i class="fa fa-building-o" aria-hidden="true"></i>

  <label for="fname" class="new_fs">Apartment:</label>

  <input type="text" class="form-input fm_ctl" name="apartment" id="apartment" placeholder="Enter your apartment name." <?php if(!empty($tempUserDetail)){ ?> value="<?php echo $tempUserDetail['apartment']; ?>"  <?php }else{ ?> value="<?php echo set_value('apartment'); ?>" <?php } ?> data-toggle="tooltip" data-placement="top" title="Please enter your apartment name."/>

  

  <?php echo form_error('apartment'); ?>

</div>



<div class="form-group sig_fm">

  <i class="fa fa-map-signs" aria-hidden="true"></i>

  <label for="fname" class="new_fs">City:</label>

  <input type="text" class="form-input fm_ctl" name="city" id="city" placeholder="City" <?php if(!empty($tempUserDetail)){ ?> value="<?php echo $tempUserDetail['city']; ?>"  <?php }else{ ?> value="<?php echo set_value('city'); ?>" <?php } ?> data-toggle="tooltip" data-placement="top" title="Please enter city."/>

  

  <?php echo form_error('city'); ?>



</div>





<div class="form-group sig_fm">

  <i class="fa fa-building-o" aria-hidden="true"></i>

  <label for="fname" class="new_fs">State:</label>

  <input type="text" class="form-input fm_ctl" name="state" id="state" placeholder="State" <?php if(!empty($tempUserDetail)){ ?> value="<?php echo $tempUserDetail['state']; ?>"  <?php }else{ ?> value="<?php echo set_value('state'); ?>" <?php } ?> data-toggle="tooltip" data-placement="top" title="Please enter state."/>

  

  <?php echo form_error('state'); ?>

</div>













<div class="form-group sig_fm">

  <i class="fa fa-map-o" aria-hidden="true"></i>

  <label for="fname" class="new_fs">Zipcode:</label>

  <input type="text" class="form-input fm_ctl" name="zipcode" id="zipcode" placeholder="Zip code" <?php if(!empty($tempUserDetail)){ ?> value="<?php echo $tempUserDetail['zipcode']; ?>"  <?php }else{ ?> value="<?php echo set_value('zipcode'); ?>" <?php } ?> data-toggle="tooltip" data-placement="top" title="Please enter zipcode."/>

  

  <?php echo form_error('zipcode'); ?>

</div>







<div class="form-group sig_fm">

  <i class="fa fa-envelope-o" aria-hidden="true"></i>

  <label for="fname" class="new_fs">Email:</label>

  <input type="email" class="form-input fm_ctl" name="email" id="email" placeholder="Enter your email address."  <?php if(!empty($tempUserDetail)){ ?> value="<?php echo $tempUserDetail['email']; ?>"  <?php }else{ ?> value="<?php echo set_value('email'); ?>" <?php } ?> data-toggle="tooltip" data-placement="top" title="Please enter email address."/>



  <?php echo form_error('email'); ?>

</div>





<div class="form-group sig_fm">

  <i class="fa fa-sign-in" aria-hidden="true"></i>

  <label for="fname" class="new_fs">Login Id:</label>

  <input type="text" class="form-input fm_ctl" name="login_id" id="login_id" placeholder="Enter login Id you will use to login into Tablet. " <?php if(!empty($tempUserDetail)){ ?> value="<?php echo $tempUserDetail['username']; ?>"  <?php }else{ ?> value="<?php echo set_value('login_id'); ?>"  <?php } ?> data-toggle="tooltip" data-placement="top" title="Plese enter login Id you will use to login into Tablet."/>



  <?php echo form_error('login_id'); ?>

</div>









<div class="form-group sig_fm">

  <i class="fa fa-mobile" aria-hidden="true"></i>

  <label for="fname" class="new_fs">Mobile:</label>

  <input type="text" class="form-input fm_ctl" name="mobile" id="mobile" placeholder="xxx-xxx-xxxx" <?php if(!empty($tempUserDetail)){ ?> value="<?php echo $tempUserDetail['mobile']; ?>"  <?php }else{ ?> value="<?php echo set_value('mobile'); ?>" <?php } ?> data-toggle="tooltip" data-placement="top" title="Please enter your phone number."/>



  <?php echo form_error('mobile'); ?>



  <span id="errmsg"></span>

</div>









  <!-- <label for="fname">Company / School:</label>

  <input type="text" class="form-input" name="company_school" id="company_school" placeholder="Enter login Id you will use to login into Tablet. " value="<?php echo set_value('company_school'); ?>" />



  <?php echo form_error('company_school'); ?>

  <br> -->



  <div class="form-group sig_fm">

    <i class="fa fa-unlock-alt" aria-hidden="true"></i>

    <label for="fname" class="new_fs">Password:</label>

    <input type="password" class="form-input fm_ctl" name="password" id="password" placeholder="Enter your password" <?php if(!empty($tempUserDetail)){ ?> value="<?php echo $tempUserDetail['pass']; ?>"  <?php }else{ ?> value="<?php echo set_value('password'); ?>" <?php } ?>  data-toggle="tooltip" data-placement="top" title="Must contain at least one number and one uppercase and lowercase and  punctuation mark letter, and at least 8 or more characters."/>

    <span toggle="#password" class="toggle-password"></span>

    

    <?php echo form_error('password'); ?>

  </div>

  



  <div class="form-group sig_fm">

    <i class="fa fa-unlock-alt" aria-hidden="true"></i>

    <label for="fname" class="new_fs">Confirm password:</label>

    <input type="password" class="form-input fm_ctl" name="re_password" id="re_password" placeholder="Repeat your password" <?php if(!empty($tempUserDetail)){ ?> value="<?php echo $tempUserDetail['pass']; ?>"  <?php }else{ ?> value="<?php echo set_value('re_password'); ?>" <?php } ?> data-toggle="tooltip" data-placement="top" title="Please enter confirm password."/>

    <span toggle="#password" class="toggle-password"></span>

    

    

    <?php echo form_error('re_password'); ?>

  </div>

  

  <!-- <label for="fname">First name:</label> -->



  <div class="form-group sig_fm_l">

    <input type="checkbox" name="agreeterm" id="agreeterm" class="agree-term"  value="1" data-toggle="tooltip" data-placement="top" title="Please agree."/>

    <label for="agree-term" class="label-agree-term"><span>I agree all statements in 

      <a href="<?php base_url() ?>/term-of-use" class="term-service" target="_blank">Terms and conditions</a>

    </span></label>



    

    <?php echo form_error('agree-term'); ?>

  </div>

  

  <!-- <label for="fname">First name:</label> -->



  <div class="loader-image" id="loading2" style="display:none;height: auto;width: auto;">

    <img src="<?php echo base_url('assets/images/ajax-loader-blue.gif') ?>" alt="" />

  </div>

  

  <!-- <label for="fname">First name:</label> -->



  <div class="form-group sig_fm_rr">

    <input type="submit" name="nnsubmit" id="btnsubmit" class="form-submit" value="Continue"/>

  </div>







  <!-- <input type="submit" value="Submit"> -->

</form>



<div class="form-group sig_fm_r">

  <p class="loginhere">

    Already have an account ? <a href="<?php base_url()?>/login" class="loginhere-link">Login here</a>

  </p></div>



</div>

</div></div></section></div>





<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>

<script type="text/javascript">



  $.validator.addMethod("pwcheck", function(value) {

   return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these

        && /[A-Z]/.test(value) // has a lowercase letter

       && /[a-z]/.test(value) // has a lowercase letter

       && /\d/.test(value) // has a digit

     });



  $("#signupform").validate({



    rules: {



      firstname:{

        required: true

      },

      

      email:{

        required: true,

        email: true,

        remote: "<?php echo base_url(); ?>validatedata"

      },



      lastname:{

        required: true

      },



      address:{

        required: false

      },



      street_name:{

        required: true

      },



      city:{

        required: true

      },



      apartment:{

        required: false

      },



      login_id:{

        required: true,

        minlength: 6,

        remote: "<?php echo base_url(); ?>validatedata"

      },



      state:{

        required: true

      },



      zipcode:{

        required: true

      },





      area_code:{

        required: true

      },





      company_school:{

        required: false

      },



      mobile:{

       required:true,

       number:false,

       minlength: 10,

       maxlength: 14,

     },



     password: {

       required: true,

       minlength: 8,

       maxlength: 30,

       pwcheck: true

     },

     re_password : {

       required:true,

       minlength : 6,

       maxlength: 30,

       equalTo : "#password"

     },

     agreeterm : {

      required:true

    },



    //  email: {

    //    required: true,

    //    email: true,

    //    maxlength: 40,

    //    remote: {

    //     url: "<?php base_url(); ?>validatedata",

    //     type: "post",



    //   }

    // },

  },



  messages: {



    city:{

      required: "Please enter city.",

    },





    state:{

      required: "Please enter state.",

    },



    zipcode:{

      required: "Please enter zipcode.",

    },





    area_code:{

      required: "Please enter area code.",

    },



    apartment:{

      required: "Please enter your apartment name.",

    },



    login_id:{

      required: "Please enter your login id.",

      minlength: "User name must be at least 6 characters",

      remote: "This login id is already exist.",

    },



    street_name:{

      required: "Please enter your street name.",

    },



    company_school:{

      required: "Please enter company/school name.",

    },





    firstname:{

      required: "Please enter first name.",

    },

    lastname:{

      required: "Please enter last name.",

    },

    mobile:{

      required:"Please enter your phone number.",

      number:"Please enter only number",

      minlength: "Please enter at least 10 number.",

      maxlength: "Please enter maximum 14 number.",

    },



    password: {

     required: "Please enter your password.",

     minlength: "Password must be at least 8 characters.",

     maxlength: "Password at maximum 30 characters.",

     pwcheck:"Password must contain at least one uppercase, lowercase, number, punctuation mark, and minimum of 8 characters."

   },

   re_password: {

     required: "Please enter your confirm password.", 

     equalTo: "Confirm password does not match with password.",

   },

   agreeterm: {

    required: "Please check terms and conditions.",

  },



  email: {

    required: "Please enter your email.",

    email: "Please enter valid email.",

    remote: "Email id is already exist, please login.",

    //maxlength: "Please enter maximum 40 characters.",

  },

},

submitHandler: function(form) {

    // do other things for a valid form

    form.submit();

  }

});



  $(document).ready(function () {

        //called when key is pressed in textbox

        $("#mobilesdf").keypress(function (e) {

           //if the letter is not digit then display error and don't type anything

           if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {

              //display error message

              //alert('sdfsdf');

              $("#errmsg").html("Please enter only number").show().fadeOut("slow");

              return false;

            }

          });

        // $(function(){

        //   $("#username").lettersOnly();

        // });

      });







  function phone_formatting(ele,restore) {

    var new_number,

    selection_start = ele.selectionStart,

    selection_end = ele.selectionEnd,

    number = ele.value.replace(/\D/g,'');



  // automatically add dashes

  if (number.length > 2) {

    // matches: 123 || 123-4 || 123-45

    new_number = number.substring(0,3) + '-';

    if (number.length === 4 || number.length === 5) {

      // matches: 123-4 || 123-45

      new_number += number.substr(3);

    }

    else if (number.length > 5) {

      // matches: 123-456 || 123-456-7 || 123-456-789

      new_number += number.substring(3,6) + '-';

    }

    if (number.length > 6) {

      // matches: 123-456-7 || 123-456-789 || 123-456-7890

      new_number += number.substring(6);

    }

  }

  else {

    new_number = number;

  }

  

  // if value is heigher than 12, last number is dropped

  // if inserting a number before the last character, numbers

  // are shifted right, only 12 characters will show

  ele.value =  (new_number.length > 12) ? new_number.substring(12,0) : new_number;

  

  // restore cursor selection,

  // prevent it from going to the end

  // UNLESS

  // cursor was at the end AND a dash was added

  document.getElementById('msg').innerHTML='<p>Selection is: ' + selection_end + ' and length is: ' + new_number.length + '</p>';

  

  if (new_number.slice(-1) === '-' && restore === false

    && (new_number.length === 8 && selection_end === 7)

    || (new_number.length === 4 && selection_end === 3)) {

    selection_start = new_number.length;

  selection_end = new_number.length;

}

else if (restore === 'revert') {

  selection_start--;

  selection_end--;

}

ele.setSelectionRange(selection_start, selection_end);



}



function phone_number_check(field,e) {

  var key_code = e.keyCode,

  key_string = String.fromCharCode(key_code),

  press_delete = false,

  dash_key = 189,

  delete_key = [8,46],

  direction_key = [33,34,35,36,37,38,39,40],

  selection_end = field.selectionEnd;

  

  // delete key was pressed

  if (delete_key.indexOf(key_code) > -1) {

    press_delete = true;

  }

  

  // only force formatting is a number or delete key was pressed

  if (key_string.match(/^\d+$/) || press_delete) {

    phone_formatting(field,press_delete);

  }

  // do nothing for direction keys, keep their default actions

  else if(direction_key.indexOf(key_code) > -1) {

    // do nothing

  }

  else if(dash_key === key_code) {

    if (selection_end === field.value.length) {

      field.value = field.value.slice(0,-1)

    }

    else {

      field.value = field.value.substring(0,(selection_end - 1)) + field.value.substr(selection_end)

      field.selectionEnd = selection_end - 1;

    }

  }

  // all other non numerical key presses, remove their value

  else {

    e.preventDefault();

//    field.value = field.value.replace(/[^0-9\-]/g,'')

phone_formatting(field,'revert');

}



}



document.getElementById('mobile').onkeyup = function(e) {

  phone_number_check(this,e);

}









$(document).ready(function () {

        //called when key is pressed in textbox

        $("#area_code").keypress(function (e) {

           //if the letter is not digit then display error and don't type anything

           if (e.which != 8 && e.which != 43 && e.which != 0 && (e.which < 48 || e.which > 57)) {

              //display error message

              //alert('sdfsdf');

              $("#errmsgarea").html("Please enter valid area code").show().fadeOut("slow");

              return false;

            }

          });

        // $(function(){

        //   $("#username").lettersOnly();

        // });

      });

    </script>

    <script language="Javascript" type="text/javascript">

     //function onlyAlphabets(e, t) {



      $('#firstname').keypress(function (e) {

        var regex = new RegExp("^[a-zA-Z ]+$");

        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);

        if (regex.test(str)) {

          return true;

        }

        else

        {

          e.preventDefault();

          alert('Please Enter Alphabate');

          return false;

        }

      });

    //   try {

    //    if (window.event) {

    //     var charCode = window.event.keyCode;

    //   }

    //   else if (e) {

    //     var charCode = e.which;

    //   }

    //   else { return true; }

    //   if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode ==32))

    //     return true;

    //   else

    //     return false;

    // }

    // catch (err) {

    //   alert('sdf');

    //   //alert(err.Description);

    // }

  //}

</script>

<!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Hooray!">Hover</a> -->

<script>

  $(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip();

  });

</script>