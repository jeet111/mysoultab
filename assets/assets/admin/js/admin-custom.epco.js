// This example displays an address form, using the autocomplete feature

// of the Google Places API to help users fill in the information.



// This example requires the Places library. Include the libraries=places

// parameter when you first load the API. For example:

// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">



var placeSearch, autocomplete;

var componentForm = {

  street_number: 'short_name',

  route: 'long_name',

  locality: 'long_name',

  administrative_area_level_1: 'short_name',

  country: 'long_name',

  postal_code: 'short_name'

};



function initAutocomplete() {

  // Create the autocomplete object, restricting the search to geographical

  // location types.

  autocomplete = new google.maps.places.Autocomplete(

      /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),

      {types: ['geocode']});



  // When the user selects an address from the dropdown, populate the address

  // fields in the form.

}



// Bias the autocomplete object to the user's geographical location,

// as supplied by the browser's 'navigator.geolocation' object.

function geolocate() {

  if (navigator.geolocation) {

    navigator.geolocation.getCurrentPosition(function(position) {

      var geolocation = {

        lat: position.coords.latitude,

        lng: position.coords.longitude

      };

      var circle = new google.maps.Circle({

        center: geolocation,

        radius: position.coords.accuracy

      });

      autocomplete.setBounds(circle.getBounds());

    });

  }

}



jQuery(document).ready(function($) {



  $('#sampleTable').DataTable();



  $('#userTable').DataTable( {

      "order": [[ 4, "desc" ]]

  } );



  $('#roleTable').DataTable( {

      "order": [[ 1, "desc" ]]

  } );



  $('#orderTable').DataTable( {

      "order": [[ 0, "desc" ]]

  } );  



  /*--------- Datepicker ----------*/

  $(".date").datepicker({

    format: 'yyyy-mm-dd',

    endDate: '+0d',

    autoclose: true

  });



  $(".appdate").datepicker({

    format: 'yyyy-mm-dd',

    startDate: '-0d',

    autoclose: true

  });



  $(".loaddate").datepicker({

    format: 'yyyy-mm-dd',

    autoclose: true

  });



  /*--------- Timepicker ----------*/

  $('#apptime').timepicker();



  var reurl = $('.baseurl').val();



  /*--------- Change user password ----------*/

  $("#updatepassword").validate({

  rules: {

    oldpass: "required",

    newpass:  {

      required: true,

      minlength: 6

    },

    newcpass: {

      required: true,

      equalTo: "#newpass",

      minlength: 6

    }

  },

  // Specify validation error messages

  messages: {

    oldpass: "Please enter your password",

    newpass: {

      required: "Please provide new password",

      minlength: "Your password must be at least 6 characters long"

    },

    newcpass: {

      required: "Please provide new password again",

      minlength: "Your password must be at least 6 characters long",

      equalTo: "Password must be same"

    },

  },

  submitHandler: function(form) {

      var dashurl = reurl+'dashboard';

      var formData = new FormData($("#updatepassword")[0]); 

      $.ajax({

          type: 'POST',

          url: reurl+'adminupdatePassword',

          dataType: 'json',

          data: formData,

          success: function(result){

            $('#results').html(result.msg);

            if (result.status==200) {

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = dashurl;

              }, 2000);

            }

          },

          cache: false,

          contentType: false,

          processData: false

      });

  }

  });



  /*--------- Edit user profile ----------*/

  $("#update_Profile").validate({

  rules: {

    user_firstname: "required",

    user_lastname: "required",

    user_dob: "required",

    user_phone: "required",

    country_details: "required",

    state_details: "required",

    city_details: "required",

    user_address: "required",

    user_email: {

      required: true,

      email: true

    }

  },

  // Specify validation error messages

  messages: {

    user_firstname: "The first name is required",

    user_lastname: "The last name is required",

    user_email: "The email is required",

    user_dob: "The date of birth is required",

    user_phone: "The phone number is required",

    country_details: "The country is required",

    state_details: "The state is required",

    city_details: "The city is required",

    user_address: "The address is required"

  },

  submitHandler: function(form) {

        var vieurl = reurl+'adminprofile';

        var formData = new FormData($("#update_Profile")[0]); 

      $.ajax({

          type: 'POST',

          url: reurl+'adminupdate',

          dataType: 'json',

          data: formData,

          success: function(result){

            $('#results').html(result.msg);

            if (result.status==200) {

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = vieurl;

              }, 2000);

            }

          },

          cache: false,

          contentType: false,

          processData: false

      });

  }

  });



  /*--------- Delete complaint ----------*/

  $('.dele_comp').click(function() {

    var id = $(this).attr('token');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this complaint?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deletecomplaint',

          type: 'POST',

          data: {

              tid: id

          },

          dataType: 'json',

          success: function(result) {

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'allcomplaintadmin';

              }, 2000);

          }

      });

      return false;

    }

  });



  /*--------- Delete doc info knowledge base  ----------*/

  $('.deletekbdoc').click(function() {


   var dockbid = $(this).attr('dockbid');

   var doctype = $(this).attr('doctype');

   // var cfid = $(this).attr('cfid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this document info?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deletedocinfo/'+dockbid,

          type: 'POST',

          data: {

              dockbid: dockbid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {


                if (doctype='General') 
                {

                  window.location.href = baseurl+'document-information-list';

                }else{

                  window.location.href = baseurl+'document-information-empspec-list';

                }

              }, 2000);

          }

      });

      return false;

    }

  });


    


  /*--------- Delete users ----------*/

  $('.delete-user').click(function() {

    var id = $(this).attr('uid');

    var usertype = $(this).attr('usertype');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this user?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deleteuser/'+id+'/'+usertype,

          type: 'POST',

          data: {

              tid: id

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'userslist';

              }, 2000);

          }

      });

      return false;

    }

  });



  /*--------- Delete certificate ----------*/

  $('.deletecerti').click(function() {

    var cfid = $(this).attr('cfid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this certificate?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deletecertificate/'+cfid,

          type: 'POST',

          data: {

              cfid: cfid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'allcertificate';

              }, 2000);

          }

      });

      return false;

    }

  });



  /*--------- Delete feedback ----------*/

  $('.deletefeed').click(function() {

    var fid = $(this).attr('fid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this feedback?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deletefeedback/'+fid,

          type: 'POST',

          data: {

              fid: fid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'allfeedback';

              }, 2000);

          }

      });

      return false;

    }

  });



  /*--------- Delete Role ----------*/

  $('.deleterole').click(function() {

    var roid = $(this).attr('roid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this role?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deleterole/'+roid,

          type: 'POST',

          data: {

              roid: roid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'roleslist';

              }, 2000);

          }

      });

      return false;

    }

  });



  /*--------- Delete Bag ----------*/

  $('.deletebag').click(function() {

    var bgid = $(this).attr('bgid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this container type?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deletebag/'+bgid,

          type: 'POST',

          data: {

              bgid: bgid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'baglist';

              }, 2000);

          }

      });

      return false;

    }

  });



  /*--------- Delete Document Type ----------*/

  $('.delete_doctyp').click(function() {

    var doctid = $(this).attr('doctid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this document type?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deletedoctyp/'+doctid,

          type: 'POST',

          data: {

              doctid: doctid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'document-type-list';

              }, 2000);

          }

      });

      return false;

    }

  });


   /*--------- Delete Assets ----------*/

  $('.deleteempassets').click(function() {

    var sid = $(this).attr('sid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this assets?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deleteempassets/'+sid,

          type: 'POST',

          data: {

              sid: sid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'employee-assets-list';

              }, 2000);

          }

      });

      return false;

    }

  });




  /*--------- Delete Assets Type ----------*/

  $('.delassetstype').click(function() {

    var doctid = $(this).attr('doctid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this assets type?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deleteassetstype/'+doctid,

          type: 'POST',

          data: {

              doctid: doctid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'assets-type-list';

              }, 2000);

          }

      });

      return false;

    }

  });




  /*--------- Delete Unit ----------*/

  $('.deleteunit').click(function() {

    var uid = $(this).attr('uid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this unit?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deleteunit/'+uid,

          type: 'POST',

          data: {

              uid: uid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'unitlist';

              }, 2000);

          }

      });

      return false;

    }

  });



  /*--------- Delete QR Code ----------*/

  $('.deleteqrcode').click(function() {

    var brid = $(this).attr('brid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this QR code?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deleteqrcode/'+brid,

          type: 'POST',

          data: {

              brid: brid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'barcodelist';

              }, 2000);

          }

      });

      return false;

    }

  });



  /*--------- Move Page to trash ----------*/

  $('.trash-page').click(function() {

    var pgtr = $(this).attr('pgtr');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this page?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'trashPage/'+pgtr,

          type: 'POST',

          data: {

              pgtr: pgtr

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'pageslist';

              }, 2000);

          }

      });

      return false;

    }

  });



  /*--------- Restore Page from trash  ----------*/

  $('.trashback-page').click(function() {

    var pid = $(this).attr('pid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to restore this page?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'trashbackPage/'+pid,

          type: 'POST',

          data: {

              pid: pid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'pageslist';

              }, 2000);

          }

      });

      return false;

    }

  });



  /*--------- Restore & Publish Page from trash  ----------*/

  $('.trashback-publishpage').click(function() {

    var pid = $(this).attr('pid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to restore & publish this page?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'trashbackAndPublishPage/'+pid,

          type: 'POST',

          data: {

              pid: pid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'pageslist';

              }, 2000);

          }

      });

      return false;

    }

  });



  /*--------- Upload home banner  ----------*/

  $("#uploadbanner").validate({

  rules: {

    ban_file: "required",

  },

  // Specify validation error messages

  messages: {

    ban_file: "Please select a valid image to upload",

  },

  submitHandler: function(form) {

    var bannerurl = reurl+'homeBanner';

    var formData = new FormData($("#uploadbanner")[0]); 

  $.ajax({

      type: 'POST',

      url: reurl+'uploadbanner',

      dataType: 'json',

      data: formData,

      success: function(result){

        $('#results').html(result.msg);

        if (result.status==200) {

          // redirect to google after 5 seconds

          window.setTimeout(function() {

              window.location.href = bannerurl;

          }, 2000);

        }

      },

      cache: false,

      contentType: false,

      processData: false

  });

  }

  });







  /*--------- chkpassword   ----------*/

  // $("#chkpassword").validate({

  // rules: {

  //   password: "required",

  // },

  // // Specify validation error messages

  // messages: {

  //   submit: "Please enter password",

  // },

  // submitHandler: function(form) {

  //   var bannerurl = reurl+'distpendinglisting';

  //   var formData = new FormData($("#chkpassword")[0]); 

  // $.ajax({

  //     type: 'POST',

  //     url: reurl+'chkpassword',

  //     dataType: 'json',

  //     data: formData,

  //     success: function(result){

  //       $('#results').html(result.msg);

  //       if (result.status==200) {

  //         // redirect to google after 5 seconds

  //         window.setTimeout(function() {

  //             window.location.href = bannerurl;

  //         }, 2000);

  //       }

  //     },

  //     cache: false,

  //     contentType: false,

  //     processData: false

  // });

  // }

  // });















  /*--------- Move Home banner to trash ----------*/

  $('.trash-banner').click(function() {

    var hbid = $(this).attr('hbid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this banner?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'trashBanner/'+hbid,

          type: 'POST',

          data: {

              hbid: hbid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'homeBanner';

              }, 2000);

          }

      });

      return false;

    }

  });



    /*--------- Restore Banner from trash  ----------*/

  $('.trashback-banner').click(function() {

    var bnid = $(this).attr('bnid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to restore this banner?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'trashbackBanner/'+bnid,

          type: 'POST',

          data: {

              bnid: bnid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'homeBanner';

              }, 2000);

          }

      });

      return false;

    }

  });



  /*--------- Restore & Publish Banner from trash  ----------*/

  $('.trashback-publishbanner').click(function() {

    var bnid = $(this).attr('bnid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to restore & publish this banner?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'trashbackAndPublishBanner/'+bnid,

          type: 'POST',

          data: {

              bnid: bnid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'homeBanner';

              }, 2000);

          }

      });

      return false;

    }

  });



  //Home section 6 Update

  $("#section6-1").validate({

    rules:{

      title:"required",

      template1:{

        required:true,

        maxlength:300,

      },

      title_ar:"required",

      template1_ar:{

        required:true,

        maxlength:300,

      }

    },

    messages:{

      title:"Please enter title",

      template1:{

        required:"Please enter description",

        maxlength:"More than 300 characters not allowed"

      },

      title_ar:"يرجى إدخال العنوان",

      template1_ar:{

        required:"يرجى إدخال الوصف",

        maxlength:"لا يُسمح بأكثر من 300 حرف"

      }

    },

    submitHandler:function(e){

      var formData = new FormData($("#section6-1")[0]);        

          $("#sub1").prop("disabled", true);

          $.ajax({

              type:'POST',

              url: reurl+"UpdateSecton6/1",

              dataType: 'json',

              data:formData,

              cache:false,

              contentType: false,

              processData: false,

              dataType:"json",

              success:function(data){

                $("#sub1").prop("disabled", false);                

                  $("#sec1msg").html(data.msg);

                  setTimeout(function(){

                      $("#sec1msg").html("");

                  }, 2000);

              }            

          });

      return false;

    }

  });

  $("#section6-2").validate({

    rules:{

      title:"required",

      template1:{

        required:true,

        maxlength:300,

      },

      title_ar:"required",

      template1_ar:{

        required:true,

        maxlength:300,

      }

    },

    messages:{

      title:"Please enter title",

      template1:{

        required:"Please enter description",

        maxlength:"More than 300 characters not allowed"

      },

      title_ar:"يرجى إدخال العنوان",

      template1_ar:{

        required:"يرجى إدخال الوصف",

        maxlength:"لا يُسمح بأكثر من 300 حرف"

      }

    },

    submitHandler:function(e){

      var formData = new FormData($("#section6-2")[0]);        

          $("#sub2").prop("disabled", true);

          $.ajax({

              type:'POST',

              url: reurl+"UpdateSecton6/2",

              dataType: 'json',

              data:formData,

              cache:false,

              contentType: false,

              processData: false,

              dataType:"json",

              success:function(data){                

                  $("#sub2").prop("disabled", false);                

                  $("#sec2msg").html(data.msg);

                  setTimeout(function(){

                      $("#sec2msg").html("");

                  }, 2000);

              }            

          });

      return false;

    }

  });

  $("#section6-3").validate({

    rules:{

      title:"required",

      template1:{

        required:true,

        maxlength:300,

      },

      title_ar:"required",

      template1_ar:{

        required:true,

        maxlength:300,

      }

    },

    messages:{

      title:"Please enter title",

      template1:{

        required:"Please enter description",

        maxlength:"More than 300 characters not allowed"

      },

      title_ar:"يرجى إدخال العنوان",

      template1_ar:{

        required:"يرجى إدخال الوصف",

        maxlength:"لا يُسمح بأكثر من 300 حرف"

      }

    },

    submitHandler:function(e){

      var formData = new FormData($("#section6-3")[0]);        

          $("#sub3").prop("disabled", true);

          $.ajax({

              type:'POST',

              url: reurl+"UpdateSecton6/3",

              dataType: 'json',

              data:formData,

              cache:false,

              contentType: false,

              processData: false,

              dataType:"json",

              success:function(data){                

                  $("#sub3").prop("disabled", false);                

                  $("#sec3msg").html(data.msg);

                  setTimeout(function(){

                      $("#sec3msg").html("");

                  }, 2000);

              }            

          });

      return false;

    }

  });

  $("#section6-4").validate({

    rules:{

      title:"required",

      template1:{

        required:true,

        maxlength:300,

      },

      title_ar:"required",

      template1_ar:{

        required:true,

        maxlength:300,

      }

    },

    messages:{

      title:"Please enter title",

      template1:{

        required:"Please enter description",

        maxlength:"More than 300 characters not allowed"

      },

      title_ar:"يرجى إدخال العنوان",

      template1_ar:{

        required:"يرجى إدخال الوصف",

        maxlength:"لا يُسمح بأكثر من 300 حرف"

      }

    },

    submitHandler:function(e){

      var formData = new FormData($("#section6-4")[0]);        

          $("#sub4").prop("disabled", true);

          $.ajax({

              type:'POST',

              url: reurl+"UpdateSecton6/4",

              dataType: 'json',

              data:formData,

              cache:false,

              contentType: false,

              processData: false,

              dataType:"json",

              success:function(data){                

                  $("#sub4").prop("disabled", false);                

                  $("#sec4msg").html(data.msg);

                  setTimeout(function(){

                      $("#sec4msg").html("");

                  }, 2000);

              }

          });

      return false;

    }

  });

  $("#section6-5").validate({

    rules:{

      title:"required",

      title_ar:"required",

    },

    messages:{

      title:"Please enter title",

      title_ar:"يرجى إدخال العنوان",

    },

    submitHandler:function(e){

      var formData = new FormData($("#section6-5")[0]);        

          $("#sub5").prop("disabled", true);

          $.ajax({

              type:'POST',

              url: reurl+"UpdateSecton6/5",

              dataType: 'json',

              data:formData,

              cache:false,

              contentType: false,

              processData: false,

              dataType:"json",

              success:function(data){                

                  $("#sub5").prop("disabled", false);                

                  $("#sec5msg").html(data.msg);

                  setTimeout(function(){

                      $("#sec5msg").html("");

                  }, 2000);

              }            

          });

      return false;

    }

  });

  //Home section 6 Update End



  //Home Promotional section 3 Update

  $("#section3-1").validate({

    rules:{

      title:"required",

      template1:{

        required:true,

        maxlength:300,

      },

      title_ar:"required",

      template1_ar:{

        required:true,

        maxlength:300,

      }

    },

    messages:{

      title:"Please enter title",

      template1:{

        required:"Please enter description",

        maxlength:"More than 300 characters not allowed"

      },

      title_ar:"يرجى إدخال العنوان",

      template1_ar:{

        required:"يرجى إدخال الوصف",

        maxlength:"لا يُسمح بأكثر من 300 حرف"

      }

    },

    submitHandler:function(e){

      var formData = new FormData($("#section3-1")[0]);        

          $("#sub31").prop("disabled", true);

          $.ajax({

              type:'POST',

              url: reurl+"UpdateSecton3/1",

              dataType: 'json',

              data:formData,

              cache:false,

              contentType: false,

              processData: false,

              dataType:"json",

              success:function(data){

                $("#sub31").prop("disabled", false);                

                  $("#sec31msg").html(data.msg);

                  setTimeout(function(){

                      $("#sec31msg").html("");

                  }, 2000);

              }            

          });

      return false;

    }

  });

  $("#section3-2").validate({

    rules:{

      title:"required",

      template1:{

        required:true,

        maxlength:300,

      },

      title_ar:"required",

      template1_ar:{

        required:true,

        maxlength:300,

      }

    },

    messages:{

      title:"Please enter title",

      template1:{

        required:"Please enter description",

        maxlength:"More than 300 characters not allowed"

      },

      title_ar:"يرجى إدخال العنوان",

      template1_ar:{

        required:"يرجى إدخال الوصف",

        maxlength:"لا يُسمح بأكثر من 300 حرف"

      }

    },

    submitHandler:function(e){

      var formData = new FormData($("#section3-2")[0]);        

          $("#sub32").prop("disabled", true);

          $.ajax({

              type:'POST',

              url: reurl+"UpdateSecton3/2",

              dataType: 'json',

              data:formData,

              cache:false,

              contentType: false,

              processData: false,

              dataType:"json",

              success:function(data){                

                  $("#sub32").prop("disabled", false);                

                  $("#sec32msg").html(data.msg);

                  setTimeout(function(){

                      $("#sec32msg").html("");

                  }, 2000);

              }            

          });

      return false;

    }

  });

  $("#section3-3").validate({

    rules:{

      title:"required",

      template1:{

        required:true,

        maxlength:300,

      },

      title_ar:"required",

      template1_ar:{

        required:true,

        maxlength:300,

      }

    },

    messages:{

      title:"Please enter title",

      template1:{

        required:"Please enter description",

        maxlength:"More than 300 characters not allowed"

      },

      title_ar:"يرجى إدخال العنوان",

      template1_ar:{

        required:"يرجى إدخال الوصف",

        maxlength:"لا يُسمح بأكثر من 300 حرف"

      }

    },

    submitHandler:function(e){

      var formData = new FormData($("#section3-3")[0]);        

          $("#sub33").prop("disabled", true);

          $.ajax({

              type:'POST',

              url: reurl+"UpdateSecton3/3",

              dataType: 'json',

              data:formData,

              cache:false,

              contentType: false,

              processData: false,

              dataType:"json",

              success:function(data){                

                  $("#sub33").prop("disabled", false);                

                  $("#sec33msg").html(data.msg);

                  setTimeout(function(){

                      $("#sec33msg").html("");

                  }, 2000);

              }            

          });

      return false;

    }

  });

  //Home Promotional section 3 Update End



  // Update social links

   $("#updateSociallinks").validate({

    rules:{

      facebook: "required",

      twitter: "required",

      instagram: "required",

      gplus: "required",

      copyright: "required",

      copyright_ar: "required"

    },

    messages:{

      facebook:"Please enter facebook link",

      twitter: "Please enter twitter link",

      instagram: "Please enter instagram link",

      gplus: "Please enter google+ link",

      copyright: "Please enter copyright",

      copyright_ar: "Please enter copyright for arabic"

    },

    submitHandler:function(e){

      $("#socialsub").prop("disabled", true);

      $(".loading").show();

      $.ajax({

      url:reurl+"updateSocialLinks",

      data:$("#updateSociallinks").serialize(),

      type:"POST",

      dataType:"json",      

      success: function(data){        

        $("#socialsub").prop("disabled", false);

          $(".loading").hide();

          $("#facebookmsg").html(data.msg);

                setTimeout(function(){

                    $("#facebookmsg").html("");                    

                }, 2000);

      }

    });

      return false;       

    }

   })

// Update social links



   /*--------- Delete appointment ----------*/

  $('.deleteapp').click(function() {

    var cfid = $(this).attr('cfid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this appointment?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deleteappointment/'+cfid,

          type: 'POST',

          data: {

              cfid: cfid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'appointmentlisting';

              }, 2000);

          }

      });

      return false;

    }

  });



  /*--------- Delete order ----------*/

  $('.deleteorder').click(function() {

    var oid = $(this).attr('oid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this order?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deleteorder/'+oid,

          type: 'POST',

          data: {

              oid: oid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'orderslist';

              }, 2000);

          }

      });

      return false;

    }

  });



     /*--------- Delete drop ----------*/

  $('.deletedrop').click(function() {

    var cfid = $(this).attr('cfid');

  //alert("hello");

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this drop?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deletedrops/'+cfid,

          type: 'POST',

          data: {

              cfid: cfid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'droplist';

              }, 2000);

          }

      });

      return false;

    }

  });

  

  /*--------- Delete services ----------*/

  $('.deleteservices').click(function() {

    var sid = $(this).attr('sid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this services?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deleteservice/'+sid,

          type: 'POST',

          data: {

              sid: sid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'serviceslisting';

              }, 2000);

          }

      });

      return false;

    }

  });





  /*--------- Delete testimonial ----------*/

  $('.deletetest').click(function() {

    var sid = $(this).attr('sid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this testimonial?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deletetestimonial/'+sid,

          type: 'POST',

          data: {

              sid: sid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'testimoniallisting';

              }, 2000);

          }

      });

      return false;

    }

  });





  /*--------- Delete news ----------*/

  $('.deletenw').click(function() {

    var sid = $(this).attr('sid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this news?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deletenews/'+sid,

          type: 'POST',

          data: {

              sid: sid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'newslisting';

              }, 2000);

          }

      });

      return false;

    }

  });



  /*--------- Delete people ----------*/

  $('.deletepeo').click(function() {

    var sid = $(this).attr('sid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this people?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deletepeople/'+sid,

          type: 'POST',

          data: {

              sid: sid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'peoplelisting';

              }, 2000);

          }

      });

      return false;

    }

  });



  /*--------- Delete Visitor Contact ----------*/

  $('.deletevcontact').click(function() {

    var sid = $(this).attr('sid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this visitor contact?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deletevisitorcontact/'+sid,

          type: 'POST',

          data: {

              sid: sid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'visitorcontact';

              }, 2000);

          }

      });

      return false;

    }

  });



    /*--------- Delete Job Seeker Contact ----------*/

  $('.deletejobcontact').click(function() {

    var sid = $(this).attr('sid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this job seeker contact?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deletejobseeker/'+sid,

          type: 'POST',

          data: {

              sid: sid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'jobseeker';

              }, 2000);

          }

      });

      return false;

    }

  });



  /*--------- Delete employee document ----------*/

  $('.deleteempdoc').click(function() {

    var sid = $(this).attr('sid');

    var baseurl = $('.baseurl').val();

    var confirmation = confirm("are you sure you want to delete this employee document?");

    if (confirmation) {

      $.ajax({

          url: baseurl+'deleteempdoc/'+sid,

          type: 'POST',

          data: {

              sid: sid

          },

          dataType: 'json',

          success: function(result) {

            //alert(result);

            $('#results').html(result.msg);

              // redirect to google after 5 seconds

              window.setTimeout(function() {

                  window.location.href = baseurl+'employee-document-list';

              }, 2000);

          }

      });

      return false;

    }

  });



  /*--------- send otp user  ----------*/

  // $('.sendotp').click(function() {

  //   var dcid = $(this).attr('dcid');

    

  //   var password = $('.password').val();

  //   var baseurl = $('.baseurl').val();

  //   var confirmation = confirm("are you sure you want to send otp?");

  //   if (confirmation) {

  //     $.ajax({

  //         url: baseurl+'sendotpuser/'+dcid,

  //         type: 'POST',

  //         data: {

  //             dcid: dcid

  //         },

  //         dataType: 'json',

  //         success: function(result) {

  //           alert(result);





  //           // $('#results').html(result.msg);

  //           //   // redirect to google after 5 seconds

  //           //   window.setTimeout(function() {

  //           //       window.location.href = baseurl+'peoplelisting';

  //           //   }, 2000);

  //         }

  //     });

  //     return false;

  //   }

  // });













  /*--------- Distraction Completed ----------*/

  // $('.distlink').click(function() {

  //   var dc_id = $(this).attr('dcid');

  //   $('.dcid').val(dc_id);      

  // });

  // $('.distractionModal').click(function() {

  //   var dcid = $('.dcid').val();

  //   var baseurl = $('.baseurl').val();

  //     $.ajax({

  //         url: baseurl+'distractioncompleted/'+dcid,

  //         type: 'POST',

  //         data: {

  //             dcid: dcid

  //         },

  //         dataType: 'json',

  //         success: function(result) {

  //           //alert(result);

  //           $('#results').html(result.msg);

  //             // redirect to google after 5 seconds

  //             window.setTimeout(function() {

  //                 window.location.href = baseurl+'distcompletedlisting';

  //             }, 2000);

  //         }

  //     });

  //     return false;

  // });









  //Going Green work picture Update

  $("#picture5-1").validate({

    submitHandler:function(e){

      var formData = new FormData($("#picture5-1")[0]);        

          $("#sub1").prop("disabled", true);

          $.ajax({

              type:'POST',

              url: reurl+"GoingUpdateSecton5/1",

              dataType: 'json',

              data:formData,

              cache:false,

              contentType: false,

              processData: false,

              success:function(data){

                $("#sub1").prop("disabled", false);                

                  $("#sec1msg").html(data.msg);

                  setTimeout(function(){

                      $("#sec1msg").html("");

                  }, 2000);

              }            

          });

      return false;

    }

  });

  $("#picture5-2").validate({

    submitHandler:function(e){

      var formData = new FormData($("#picture5-2")[0]);        

          $("#sub2").prop("disabled", true);

          $.ajax({

              type:'POST',

              url: reurl+"GoingUpdateSecton5/2",

              dataType: 'json',

              data:formData,

              cache:false,

              contentType: false,

              processData: false,

              success:function(data){                

                  $("#sub2").prop("disabled", false);                

                  $("#sec2msg").html(data.msg);

                  setTimeout(function(){

                      $("#sec2msg").html("");

                  }, 2000);

              }            

          });

      return false;

    }

  });

  $("#picture5-3").validate({

    submitHandler:function(e){

      var formData = new FormData($("#picture5-3")[0]);        

          $("#sub3").prop("disabled", true);

          $.ajax({

              type:'POST',

              url: reurl+"GoingUpdateSecton5/3",

              dataType: 'json',

              data:formData,

              cache:false,

              contentType: false,

              processData: false,

              dataType:"json",

              success:function(data){                

                  $("#sub3").prop("disabled", false);                

                  $("#sec3msg").html(data.msg);

                  setTimeout(function(){

                      $("#sec3msg").html("");

                  }, 2000);

              }            

          });

      return false;

    }

  });

  $("#picture5-4").validate({

    submitHandler:function(e){

      var formData = new FormData($("#picture5-4")[0]);        

          $("#sub4").prop("disabled", true);

          $.ajax({

              type:'POST',

              url: reurl+"GoingUpdateSecton5/4",

              dataType: 'json',

              data:formData,

              cache:false,

              contentType: false,

              processData: false,

              dataType:"json",

              success:function(data){                

                  $("#sub4").prop("disabled", false);                

                  $("#sec4msg").html(data.msg);

                  setTimeout(function(){

                      $("#sec4msg").html("");

                  }, 2000);

              }

          });

      return false;

    }

  });

  $("#picture5-5").validate({

    submitHandler:function(e){

      var formData = new FormData($("#picture5-5")[0]);        

          $("#sub5").prop("disabled", true);

          $.ajax({

              type:'POST',

              url: reurl+"GoingUpdateSecton5/5",

              dataType: 'json',

              data:formData,

              cache:false,

              contentType: false,

              processData: false,

              dataType:"json",

              success:function(data){                

                  $("#sub5").prop("disabled", false);                

                  $("#sec5msg").html(data.msg);

                  setTimeout(function(){

                      $("#sec5msg").html("");

                  }, 2000);

              }            

          });

      return false;

    }

  });

  //Going Green work picture Update End  



  /*--------- Email template ----------*/

  var baseurl = $('.baseurl').val();

  $("#saveTemplate").on("submit", function(){

    if(CKEDITOR.instances.template.getData() == ''){

      $("#ermsg").html("<label class='text-danger'>Please enter email description</label>");

      return false;

    }else if($("input[name='subject']", "#saveTemplate").val() == ""){

      $("#ersbmsg").html("<label class='text-danger'>Please enter email subject.</label>");

      $("input[name='subject']", "#saveTemplate").focus();

      return false;

    }

    else{

      $(".loading").show();

        $("#catsub").prop("disabled", true);

        $.ajax({

            url: $("#saveTemplate").attr("action"),

            data:{"template":CKEDITOR.instances.template.getData(), "subject":$("input[name='subject']", "#saveTemplate").val(), "variables":$("textarea[name='variables']", "#saveTemplate").val()},

            type: "POST",

            dataType: "json",

            success:function(data){

                $(".loading").hide();

                $("#catsub").prop("disabled", false);

                $("#catupmsg").html(data.msg);

                setTimeout(function(){

                    $("#catupmsg").html("");

                }, 2000);

                if(data.status == 200){

                  setTimeout(function(){

                      window.location.href = baseurl+"email-template";

                  }, 1000);

                }

            }

        });

    }

    return false;

  });



  /** After windod Load */

  $(window).bind("load", function() {

    window.setTimeout(function() {

      $(".alert").fadeTo(500, 0).slideUp(500, function(){

          $(this).remove(); 

      });

  }, 4000);

  });



  $("input[name$='recurr_order']").click(function() {

        var recu = $(this).val();

        if (recu=='weekly') {

          $("div#pre_day").show();

          $("div#pre_date").hide();

          $("div#pre_date input").val('');

          $("#Submitorder").attr('disabled','disabled');

          $("select#preferred_day").change(function(){

          var selecteday = $("#preferred_day option:selected").val();

          //alert(selecteday);

          if (selecteday!='') {

            $("#Submitorder").removeAttr('disabled');

          } else {

            $("#Submitorder").attr('disabled','disabled');

          }

        });

      } else if(recu=='monthly') {

          $("div#pre_day").hide();

          $("div#pre_date").show();

          $("div#pre_day select").val('');

          $("#Submitorder").attr('disabled','disabled');

        } else{

          $("div#pre_day").hide();

          $("div#pre_date").hide();

          $("div#pre_date input").val('');

          $("div#pre_day select").val('');

          $("#Submitorder").removeAttr('disabled');

        }

    });



  // This function is called from the pop-up menus to transfer to

  // a different page. Ignore if the value returned is a null string:

  function goPage (newURL) {



    // if url is empty, skip the menu dividers and reset the menu selection to default

    if (newURL != "") {

    

        // if url is "-", it is this page -- reset the menu:

        if (newURL == "-" ) {

            resetMenu();            

        } 

        // else, send page to designated URL            

        else {  

          document.location.href = newURL;

        }

    }

  }



  // resets the menu selection upon entry to this page:

  function resetMenu() {

    document.gomenu.selector.selectedIndex = 2;

  }



});

