  <?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  class Meditation_vedio extends MX_Controller {
    private $staff_permission = array();
    private $access_permission = true;
    public function __construct(){   
      parent:: __construct();
      $this->load->library('session');
      $this->load->library('form_validation');
      $this->load->model('movie_model');
      $this->load->model('meditation_vedio_model');
      $this->load->model('Common_model');
      $this->load->model('login_model');
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));
      $user_id = $this->session->userdata('logged_ins')['id'];
      if(empty($user_id)) {
        redirect('admin/login');
      }

      if ($this->session->userdata('logged_ins')['user_role'] == 5) {
        $uid = $this->session->userdata('logged_ins')['id'];
        $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
        if (!empty($permission_list)) 
        {
         foreach ($permission_list as $k => $v) 
         {
          $this->staff_permission[$v['permission_id']] = $v;
        }
      }
    }
  }

  public function permission_access($permission_id=14)
  {
    if ($this->session->userdata('logged_ins')['user_role'] != 1) 
    {
     if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
     {
      $this->access_permission = false;
    }
  }
}

/*Created by 95 for show all movie*/
public function meditation_vedio_list()
{
 $data['menuactive'] = $this->uri->segment(2);
 $data['MovieData'] = $this->meditation_vedio_model->getAllmeditationvedios();

 $data['staff_permission'] = $this->staff_permission;

 $this->permission_access(14);
 if ($this->access_permission) {
  $this->template->set('title', 'Meditation Viedo');
  $this->template->load('admin_dashboard_layout', 'contents', 'meditation_vedio_list', $data);
} else {
  $this->template->set('title', 'Admin Panel - Staff');
  $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
}
}


/*Created by 95 for delete movie*/
public function delete_meditation_vedio($movie_id='')
{
  $image_name = $this->Common_model->getsingle("meditation_videos",array("movie_id" => $movie_id));  
  unlink("uploads/meditation_videos/".$image_name->movie_file);
  unlink("uploads/meditation_videos/image/".$image_name->movie_image);
  $this->movie_model->delete('meditation_videos',array('movie_id' =>$movie_id));
  $this->movie_model->delete('cp_meditaion_videos_favorite',array('movie_id' =>$movie_id));
  $this->session->set_flashdata('success', 'Deleted successfully.');
  redirect('admin/meditation_video_list');
}


/*Created by 95 for add movie*/
public function add_meditation_vedio()
{
  $user_id = $this->session->userdata('logged_ins')['id'];

 $data['menuactive'] = $this->uri->segment(2);
 $this->form_validation->set_rules('movie_title','Meditation Vedio Title','required');
 $this->form_validation->set_rules('movie_description','Meditation Vedio Description','required');
 $this->form_validation->set_rules('movie_artist','Meditation Vedio Artist','required');
 $this->form_validation->set_rules('movie_type','Meditation Vedio Type','required');

 $data['category_name'] = $this->Common_model->getAllwhereorderby("cp_meditation_videos_category",array("movie_category_status" => 1),"movie_category_name","asc");
 
 
 




 if ($this->form_validation->run() == TRUE) 
 {  
  if($_POST['submit']){



  //   /*for vedio file*/
  //   $random = $this->generateRandomString(10);
  //   $ext = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
  //   $file_name = $random.".".$ext;
  //   $dirpath = './././uploads/meditation_videos/'.$file_name;
  //   if(move_uploaded_file($_FILES['picture']['tmp_name'], $dirpath)){
  //    $data['new_pic'] = $file_name;
  //  }
  //  if(!empty($data['new_pic'])){
  //   $picture =$data['new_pic'];
  // }else{
  //   $picture ='';
  // }

    /*for image file*/
    $random1 = $this->generateRandomString(10);
    $ext = pathinfo($_FILES['pictureFile']['name'], PATHINFO_EXTENSION);
    $file_name1 = $random1.".".$ext;
    $dirpath = './././uploads/meditation_videos/image/'.$file_name1;
    if(move_uploaded_file($_FILES['pictureFile']['tmp_name'], $dirpath)){
     $data['movie_pic'] = $file_name1;
   }
   if(!empty($data['movie_pic'])){
    $pictureFile =$data['movie_pic'];

    /*For image resize*/
    $this->load->library('image_lib');
    $configer =  array(
      'image_library'   => 'gd2',
      'source_image'    =>  $dirpath,
      'maintain_ratio'  =>  TRUE,
      'width'           =>  150,
      'height'          =>  150,
    );
    $this->image_lib->clear();
    $this->image_lib->initialize($configer);
    $this->image_lib->resize();



  }else{
    $pictureFile ='';
  }

// echo $picture;
// die;


  $data = array(
    'movie_title' => $this->input->post('movie_title'),
    'movie_desc' => $this->input->post('movie_description'),
    'movie_artist' => $this->input->post('movie_artist'),
    'movie_type' => $this->input->post('movie_type'),
    'category_id' => $this->input->post('movie_type'),
    'movie_file' => '',
    'movie_url' => $this->input->post('movie_url'),
    'movie_image' => $pictureFile,
    'user_id'=> $user_id,
    'movie_created' =>date('Y-m-d H:i:s')
  );


  $this->movie_model->insertData('meditation_videos',$data);
  $this->session->set_flashdata('success', 'Meditation vedio added successfully.'); 
  redirect('admin/meditation_vedio_list');
}
}

$data['staff_permission'] = $this->staff_permission;

$this->permission_access(14);
if ($this->access_permission) {
  $this->template->set('title', 'Meditation Viedo');
  $this->template->load('admin_dashboard_layout', 'contents', 'add_meditation_vedio', $data);
} else {
  $this->template->set('title', 'Admin Panel - Staff');
  $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
}
}



public function generateRandomString($length = 12) {
 $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
 $charactersLength = strlen($characters);
 $randomString = '';
 for ($i = 0; $i < $length; $i++) {
   $randomString .= $characters[rand(0, $charactersLength - 1)];
 }
 return $randomString;
}


/*Created by 95 for edit movie*/
public function edit_meditation_vedio($movie_id='')
{

  $user_id = $this->session->userdata('logged_ins')['id'];

 $data['menuactive'] = $this->uri->segment(2);
 $this->form_validation->set_rules('movie_title','Meditation Video Title','required');
 $this->form_validation->set_rules('movie_description','Meditation Video Description','required');
 $this->form_validation->set_rules('movie_artist','Meditation Video Artist','required');
 $this->form_validation->set_rules('movie_type','Meditation Video Type','required');

 $data['category_name'] = $this->Common_model->getAllwhereorderby("cp_meditation_videos_category",array("movie_category_status" => 1),"movie_category_name","asc");
 if ($this->form_validation->run() == TRUE) 
 {  

   if($_POST['submit']){
  //   /*for audio file*/
  //   $random = $this->generateRandomString(10);
  //   $ext = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
  //   $file_name = $random.".".$ext;
  //   $dirpath = './././uploads/meditation_videos/'.$file_name;
  //   if(move_uploaded_file($_FILES['picture']['tmp_name'], $dirpath)){
  //    $data['new_pic'] = $file_name;
  //  }
  //  if(!empty($data['new_pic'])){
  //   $picture =$data['new_pic'];
  // }else{
  //   $picture ='';
  // }

    /*for image file*/
    $random1 = $this->generateRandomString(10);
    $ext = pathinfo($_FILES['pictureFile']['name'], PATHINFO_EXTENSION);
    $file_name1 = $random1.".".$ext;
    $dirpath = './././uploads/meditation_videos/image/'.$file_name1;
    if(move_uploaded_file($_FILES['pictureFile']['tmp_name'], $dirpath)){
     $data['music_pic'] = $file_name1;
   }
   if(!empty($data['music_pic'])){
    $pictureFile =$data['music_pic'];
    /*For image resize*/
    $this->load->library('image_lib');
    $configer =  array(
      'image_library'   => 'gd2',
      'source_image'    =>  $dirpath,
      'maintain_ratio'  =>  TRUE,
      'width'           =>  150,
      'height'          =>  150,
    );
    $this->image_lib->clear();
    $this->image_lib->initialize($configer);
    $this->image_lib->resize();
  }else{
    $pictureFile ='';
  }


  if(!empty($pictureFile)){

    $data = array(
      'movie_title' => $this->input->post('movie_title'),
      'movie_desc' => $this->input->post('movie_description'),
      'movie_artist' => $this->input->post('movie_artist'),
      'movie_type' => $this->input->post('movie_type'),
        //'music_file' => $picture,
      'category_id' => $this->input->post('movie_type'),
      'movie_image' => $pictureFile,
      'user_id'=>  $user_id,
      'movie_created' =>date('Y-m-d H:i:s')
    );

    $this->movie_model->updateData('meditation_videos',$data,array('movie_id'=>$movie_id));
    $this->session->set_flashdata('success', 'Meditation Video updated successfully.');
    redirect('admin/meditation_vedio_list');

  }else if(!empty($picture)){

    $data = array(
      'movie_title' => $this->input->post('movie_title'),
      'movie_desc' => $this->input->post('movie_description'),
      'movie_artist' => $this->input->post('movie_artist'),
      'movie_type' => $this->input->post('movie_type'),
      'movie_file' => '',
      'category_id' => $this->input->post('movie_type'),
        // 'music_image' => $pictureFile,
      'movie_created' =>date('Y-m-d H:i:s')
    );

    $this->movie_model->updateData('meditation_videos',$data,array('movie_id'=>$movie_id));

    $this->session->set_flashdata('success', 'Meditation Video updated successfully.');
    redirect('admin/meditation_vedio_list');




  }else if(empty($pictureFile)|| $pictureFile=='' && empty($picture)|| $picture==''){

    $data = array(
      'movie_title' => $this->input->post('movie_title'),
      'movie_desc' => $this->input->post('movie_description'),
      'movie_artist' => $this->input->post('movie_artist'),
      'movie_type' => $this->input->post('movie_type'),
      'category_id' => $this->input->post('movie_type'),
        //'music_file' => $picture,
        // 'music_image' => $pictureFile,
      'movie_created' =>date('Y-m-d H:i:s')
    );

    $this->movie_model->updateData('meditation_videos',$data,array('movie_id'=>$movie_id));

    $this->session->set_flashdata('success', 'Meditation Video updated successfully.');
    redirect('admin/meditation_vedio_list');



  }
}


}

$data['editMovie']=$this->movie_model->getsingle('meditation_videos',array('movie_id'=>$movie_id));

$data['staff_permission'] = $this->staff_permission;

$this->permission_access(14);
if ($this->access_permission) {
  $this->template->set('title', 'Meditation Viedo');
  $this->template->load('admin_dashboard_layout','contents','edit_meditation_video',$data);
} else {
  $this->template->set('title', 'Admin Panel - Staff');
  $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
}
}

public function view_meditation_video($movie_id='')
{
 $data['menuactive'] = $this->uri->segment(2);
 $data['viewMovie']=$this->movie_model->getsingle('meditation_videos',array('movie_id'=>$movie_id));

 $data['staff_permission'] = $this->staff_permission;
 
 $this->permission_access(14);
 if ($this->access_permission) {
  $this->template->set('title', 'Meditation Viedo');
  $this->template->load('admin_dashboard_layout', 'contents', 'view_meditation_video', $data);
} else {
  $this->template->set('title', 'Admin Panel - Staff');
  $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
}
}

public function add_meditation_vedio_category()
{
  $data['menuactive'] = $this->uri->segment(2);
  $this->form_validation->set_rules('category_name','Category Name','required');

  $data['staff_permission'] = $this->staff_permission;
  
  $this->permission_access(14);
  if ($this->access_permission) {
    $this->template->set('title', 'Meditation Viedo');
    $this->template->load('admin_dashboard_layout', 'contents' , 'add_meditation_vedio_category', $data);
  } else {
    $this->template->set('title', 'Admin Panel - Staff');
    $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
  }
}

public function ajax_meditation_vedio_category()
{
	
 if ($_FILES["category_icon"]["error"] > 0)
 {

 }else{

   $data['upload_path'] = 'uploads/meditation_videos/category_icon/';
   $data['allowed_types'] = 'jpg|png|gif|jpeg';
   $data['max_size'] = '5000';
   $data['encrypt_name'] = true;

   $this->load->library('upload', $data);


   if ($this->upload->do_upload('category_icon')) {
    $attachment_data = array('upload_data' => $this->upload->data());
    $uploadfile = $attachment_data['upload_data']['file_name'];

    $array = array(
     'movie_category_name' => $_POST['category_name'],
     'movie_category_icon' => $uploadfile,
     'movie_category_desc' => $_POST['category_desc'],
     'movie_category_status' => 1,
     'movie_category_create_date' => date("Y-m-d H:i:s")
   );
    $this->Common_model->insertData("cp_meditation_videos_category",$array);

    $return = array('code' => 1,'message' => 'Category have been added successfully.');


  }else{
   $return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
 }

}
echo json_encode($return);
} 

public function meditation_vedio_category_list()	
{
 $data['menuactive'] = $this->uri->segment(2);

 $data['category_list'] = $this->Common_model->getAllorderby("cp_meditation_videos_category","movie_category_id","desc");

		 //echo '<pre>';print_r($data['category_list']);die;

 
 $data['staff_permission'] = $this->staff_permission;
 
 $this->permission_access(14);
 if ($this->access_permission) {
  $this->template->set('title', 'Meditation Viedo category');
  $this->template->load('admin_dashboard_layout', 'contents' , 'meditation_vedio_category_list', $data);
} else {
  $this->template->set('title', 'Admin Panel - Staff');
  $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
}
}

public function category_status()
{
 $movie_id = $this->input->post("movie_id");
 $check_category = $this->Common_model->getsingle("cp_meditation_videos_category",array("movie_category_id" => $movie_id));
 if($check_category->movie_category_status == 1){
  $this->Common_model->updateData("cp_meditation_videos_category",array("movie_category_status" => 0),array('movie_category_id' => $movie_id));
  echo '2';
}else{
 $this->Common_model->updateData("cp_meditation_videos_category",array("movie_category_status" => 1),array('movie_category_id' => $movie_id));
 echo '1';			
}
}

public function edit_meditation_vedio_category()
{
  $data['menuactive'] = $this->uri->segment(2);

  $segment = $this->uri->segment("3");
  $data['segment'] = $segment;

  $data['singleData'] = $this->Common_model->getsingle("cp_meditation_videos_category",array("movie_category_id" => $segment));

  
  $data['staff_permission'] = $this->staff_permission;
  
  $this->permission_access(14);
  if ($this->access_permission) {
    $this->template->set('title', 'Meditation Viedo category');
    $this->template->load('admin_dashboard_layout', 'contents' , 'edit_meditation_vedio_category', $data);
  } else {
    $this->template->set('title', 'Admin Panel - Staff');
    $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
  }
}

public function ajax_edit_meditation_vedio_category()
{
	
 $singledata = $this->Common_model->getsingle("cp_meditation_videos_category",array("movie_category_id" => $_POST['cat_id']));

 $data['upload_path'] = 'uploads/meditation_videos/category_icon/';
 $data['allowed_types'] = 'jpg|png|gif|jpeg';
 $data['max_size'] = '5000';
 $data['encrypt_name'] = true;

 $this->load->library('upload', $data);


 if ($this->upload->do_upload('category_icon')) {
  $attachment_data = array('upload_data' => $this->upload->data());
  $uploadfile = $attachment_data['upload_data']['file_name'];

}

if(!empty($uploadfile)){
  $files = $uploadfile;
}else{
  $files = $singledata->movie_category_icon;
}

$array = array(
 'movie_category_name' => $_POST['category_name'],
 'movie_category_icon' => $files,
 'movie_category_desc' => $_POST['category_desc'],
 'movie_category_create_date' => date("Y-m-d H:i:s")
);
$this->Common_model->updateData("cp_meditation_videos_category",$array,array("movie_category_id" => $_POST['cat_id']));

$return = array('code' => 1,'message' => 'Successfully updated');
echo json_encode($return);	 


}

public function view_meditation_vedio_category()
{
  $data['menuactive'] = $this->uri->segment(2);

  $data['segment'] = $this->uri->segment(3);

  $data['singledata'] = $this->Common_model->getsingle("cp_meditation_videos_category",array("movie_category_id" => $data['segment']));

  
  $data['staff_permission'] = $this->staff_permission;
  
  $this->permission_access(14);
  if ($this->access_permission) {
    $this->template->set('title', 'Meditation Viedo category');
    $this->template->load('admin_dashboard_layout', 'contents' , 'view_meditation_vedio_category', $data);
  } else {
    $this->template->set('title', 'Admin Panel - Staff');
    $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
  }
}

public function delete_meditation_vedio_category()
{

  $delete_movie = $this->input->post("delete_movie");
  $image_name = $this->Common_model->getsingle("cp_meditation_videos_category",array("movie_category_id" => $delete_movie));

  $movie_name = $this->Common_model->getAllwhere("meditation_videos",array("category_id" => $delete_movie));


  unlink("uploads/meditation_videos/category_icon/".$image_name->movie_category_icon);
  foreach($movie_name as $del){
   unlink("uploads/meditation_videos/".$image_name->movie_file);
   unlink("uploads/meditation_videos/image/".$image_name->movie_image);
   $this->Common_model->delete('meditation_videos',array('category_id' =>$delete_movie));

 }
 $this->Common_model->delete('cp_meditation_videos_category',array('movie_category_id' =>$delete_movie));


}

public function ajax_meditation_add()
{
  if ($_FILES["pictureFile"]["error"] > 0)
  //if ($_FILES["picture"]["error"] > 0 && $_FILES["pictureFile"]["error"] > 0)
  {

  }else{

   $config1['upload_path'] = 'uploads/meditation_videos/';
   $config1['allowed_types'] = 'mp4|flv|wmv|webm|vob|ogg|mpeg|mpg|3gp';

           //$data['max_size'] = '5000';
   $config1['encrypt_name'] = true;

   $this->load->library('upload', $config1);




   $random = $this->generateRandomString(10);
   $ext_video = pathinfo($_FILES['video_upload']['name'], PATHINFO_EXTENSION);
   $file_name_video = $random.".".$ext_video;
   $dirpath_video = './././uploads/meditation_videos/'.$file_name_video;
   if(move_uploaded_file($_FILES['video_upload']['tmp_name'], $dirpath_video)){
         $data['new_pic'] = $file_name_video;
    }
    if(!empty($data['new_pic'])){
      $video =$data['new_pic'];
    }else{
      $video ='';
    }



   $config2['upload_path'] = 'uploads/meditation_videos/image/';
   $config2['allowed_types'] = 'jpg|png|gif|jpeg';
           //$data['max_size'] = '5000';
   $config2['encrypt_name'] = true;

   $this->upload->initialize($config2);


   if ($this->upload->do_upload('pictureFile')) {
    $attachment_data_image = array('upload_data' => $this->upload->data());
    $picturefile = $attachment_data_image['upload_data']['file_name'];
    $return = array('code' => 1,'message' => 'Successfully added');	


    /*For image resize*/
    $this->load->library('image_lib');
    $configer =  array(
      'image_library'   => 'gd2',
      'source_image'    =>  $attachment_data_image['upload_data']['full_path'],
      'maintain_ratio'  =>  TRUE,
      'width'           =>  150,
      'height'          =>  150,
    );
    $this->image_lib->clear();
    $this->image_lib->initialize($configer);
    $this->image_lib->resize();

  }else{ 
   $return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));



 }
 

 if($return['code'] != 2){ 
   $array = array(
     'movie_title' => $this->input->post('movie_title'),
     'movie_desc' => $this->input->post('movie_description'),
     'movie_artist' => $this->input->post('movie_artist'),
     'movie_image' =>  $picturefile,
     'movie_url' => $this->input->post('movie_url'),
     'movie_type' => $this->input->post('movie_type'),
     'visit_provider' => $this->input->post('visit_provider'),
     'movie_file' => '',  

     'meditation_duration' => $this->input->post('meditation_duration'),
       

     'category_id' => $this->input->post('movie_type'),
          // 'music_image' => $pictureFile,
     'movie_created' =>date('Y-m-d H:i:s')
   );
   $this->Common_model->insertData("meditation_videos",$array);

   $return = array('code' => 1,'message' => 'Successfully added');
 }
}

echo json_encode($return);	
}

public function image_upload()
{
  $config1['upload_path'] = 'uploads/movie/';
  $config1['allowed_types'] = 'mp4|flv|wmv|webm|vob|ogg|mpeg|mpg|3gp';

           //$data['max_size'] = '5000';
  $config1['encrypt_name'] = true;

  $this->load->library('upload', $config1);


  if ($this->upload->do_upload('picture')) {
    $attachment_data = array('upload_data' => $this->upload->data());
    $uploadfile = $attachment_data['upload_data']['file_name'];
    $return = array("code" => 1);
  }
  echo json_encode($return);
}

public function ajax_meditation_edit()
{
  $movie_id = $this->input->post('movie_id');

  $singleData = $this->Common_model->getsingle("meditation_videos",array("movie_id" => $movie_id));


  $config1['upload_path'] = 'uploads/meditation_videos/';
  $config1['allowed_types'] = 'mp4|flv|wmv|webm|vob|ogg|mpeg|mpg|3gp';
  $config1['encrypt_name'] = true;

  $this->load->library('upload', $config1);
  $return = array('code' => 1,'message' => 'Successfully added');	

  $config2['upload_path'] = 'uploads/meditation_videos/image/';
  $config2['allowed_types'] = 'jpg|png|gif|jpeg';
  $config2['encrypt_name'] = true;

  $this->upload->initialize($config2);

  if(!empty($_FILES["pictureFile"]["name"])){ 
    if ($this->upload->do_upload('pictureFile')) {
      $attachment_data_image = array('upload_data' => $this->upload->data());
      $picturefile = $attachment_data_image['upload_data']['file_name'];
      $return = array('code' => 1,'message' => 'Successfully added');	
      
      

      /*For image resize*/
      $this->load->library('image_lib');
      $configer =  array(
        'image_library'   => 'gd2',
        'source_image'    =>  $attachment_data_image['upload_data']['full_path'],
        'maintain_ratio'  =>  TRUE,
        'width'           =>  150,
        'height'          =>  150,
      );
      $this->image_lib->clear();
      $this->image_lib->initialize($configer);
      $this->image_lib->resize();
      
    }else{ 
     $return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));

   }
 }

 if(!empty($picturefile)){
  $picfile = $picturefile;
}else{
  $picfile = $singleData->movie_image;
}


if($return['code'] != 2){ 
 $array = array(
   'movie_title' => $this->input->post('movie_title'),
   'movie_desc' => $this->input->post('movie_description'),
   'movie_artist' => $this->input->post('movie_artist'),
   'movie_type' => $this->input->post('movie_type'),
   'movie_file' => '',
   'movie_url' => $this->input->post('movie_url'),
   'meditation_duration' => $this->input->post('meditation_duration'),
   'visit_provider' => $this->input->post('visit_provider'),

   'movie_image' =>  $picfile,
   'category_id' => $this->input->post('movie_type'),
   'movie_created' =>date('Y-m-d H:i:s')
 );
 $this->Common_model->updateData("meditation_videos",$array,array('movie_id' => $this->input->post("movie_id")));

 $return = array('code' => 1,'message' => 'Successfully updated.!');
}else{
  echo "outer";
  die;
}


echo json_encode($return);	
}

}