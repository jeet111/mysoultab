<div class="container-fluid">
	<div class="dash-counter users-main my-sts-table user-list">
		<div class="">
			<div class="col-md-12">
				<div class="">
					<div class="card-body">
						<div class="my_info_one">
							<div class="Schedule_main_one">
								<div class=" users-main">
									<h2>View Ticket</h2>
									<div class="btn_topBack">
										<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">

											<?php $ticket_id = $this->uri->segment('3'); ?>

											Back
										</a>
									</div>
									<div class="row ">
										<?php //echo "<pre>"; print_r($singledata); echo "</pre>"; ?>
										<div class="col-md-12">
											<div class="general-info">
												<div class="card-header">
													<h5 class="card-header-text">Ticket Detail</h5>
												</div>
												<div class="rrow ">
													<div class="col-md-12">
														<?php if(!empty($singleData)){ ?>
															<?php foreach ($singleData as $key => $value) { ?>
																
																<?php if($value->by_admin!=''){ ?>
																	<div class="row sep">
																		<div class="col-md-12 col-sm-12">
																			<div class="row">
																				<div class="col-md-5 col-sm-5">
																					<div class="f_name"><b>


																						<?php foreach ($userData as $usr) { ?>

																							<?php if($usr->id==$value->by_admin){ ?>


																								<?php echo $usr->name.' '.$usr->lastname; echo  '&nbsp('.$usr->email.')' ?> 
																							<?php } ?>
																						<?php } ?>



																					</b></div>
																				</div>
																				<div class="col-md-7 col-sm-7">
																					<div class="f_nameOne">
																						<span class="msg">
																							<?php echo $value->reply_desc; ?>
																						</span> 
																						<span class="dtime">
																							Date time
																							<?php echo $value->created; ?>
																						</span> 
																					</div>
																				</div>

																			</div>
																		</div>

																	</div>    

																<?php }else{ ?>
																	<div class="row sep">
																		<div class="col-md-12 col-sm-12">
																			<div class="row">
																				<div class="col-md-5 col-sm-5">
																					<div class="f_name"><b>

																						<?php foreach ($userData as $usr) { ?>

																							<?php if($usr->id==$value->user_id){ ?>


																								<?php echo $usr->name.' '.$usr->lastname; echo  '&nbsp('.$usr->email.')' ?> 
																							<?php } ?>
																						<?php } ?>

																					</b></div>
																				</div>
																				<div class="col-md-7 col-sm-7">
																					<div class="f_nameOne">
																						<span class="msg">
																							<?php echo $value->reply_desc; ?>
																						</span>
																						<span class="dtime">
																							<span class="dtime">
																								Date time
																								<?php echo $value->created; ?>
																							</span>
																						</div>
																					</div>


																				</div>
																			</div>
																		</div>
																	<?php } ?>
																<?php } ?>
															<?php }else{ ?>
																No conversations
															<?php } ?>

														</div>
														<div class="row">


															<div class="col-md-12 col-sm-12">
																<div class="row">
																	<div class="col-md-5 col-sm-5">
																		<div class="f_name"><b>



																			<a href="<?php echo base_url(); ?>admin/reply_to_ticket/<?php echo$ticket_id; ?>" class="rply_cls"><i class="fa fa-reply" aria-hidden="true"></i>Reply</a>

																		</b></div>
																	</div>
																	<div class="col-md-7 col-sm-7">
																		<div class="f_nameOne">
																		</div>
																	</div>
																</div>
															</div>



														</div>
													</div>
												</div>
											</div>
											<?php $admin_url = $this->uri->segment(2);
											$url = 'admin/email_list/list';
											if ($admin_url == 'adminprofile') {
												$url = 'admin/dashboard';
											}?>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>