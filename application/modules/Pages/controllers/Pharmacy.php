<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Pharmacy extends MX_Controller {



	public function __construct() {

		parent:: __construct();

		$this->load->library('session');

		$this->load->library('form_validation');

		$this->load->model('activities_model');

		$this->load->model('Common_model_new');

		$this->load->model('common_model_new');

		$this->load->model('movie_model');

		$this->load->helper(array('common_helper'));

		$this->load->helper(array('url'));

		$this->load->helper(array('form'));

		not_login();

	}



   
	public function pharmacy_dashboard(){
    $data['menuactive'] = '';
    $checkLogin = $this->session->userdata('logged_in');
	
    if(!empty($checkLogin))
    {
      $data['menuactive'] = $this->uri->segment(1);
     
      unset($_POST);

			$user_id = $this->session->userdata('logged_in')['id'];
      
      $data['app_list'] = $this->common_model_new->getAllwhere("app_list",array('status' => 1,'use_for'=>"pharmacy"));
      $data['pharmacy_list'] = $this->common_model_new->getAllwhere("cp_pharmacy",array('user_id'=>$this->session->userdata('logged_in')['id']));
			if(!empty($user_id ) && $user_id  !=''){
				$data['pharmacytArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'Pharmacy Pickup',"user_id"=>$user_id));
			}
			$this->template->set('title', 'Dashboard Game');

      $this->template->load('new_user_dashboard_layout', 'contents', 'pharmacy_dashboard', $data);
    } 
    else{

      redirect('login');
  
    } 
  }

  public function add_pharmacy()
  {
    if(count($_POST)>0){
        if($_POST['type']==1){
      
        
        $array =array(
           'user_id' => $this->session->userdata('logged_in')['id'],
           'pharmacy_type' => $this->input->post('pharmacy_type'),
           'button_name' => $this->input->post('button_name'),
           'url'=>$this->input->post('url'),
           'app_name' => $this->input->post('app_name'),
           'created' => date('Y-m-d H:i:s'));
       }

      $check_pharmacy = $this->Common_model_new->getsingle("cp_pharmacy",array("button_name" => $this->input->post('button_name'),"user_id" => $this->session->userdata('logged_in')['id']));
      if(!empty($check_pharmacy)){
        unset($_POST);
        $this->session->set_flashdata('success', "Pharmacy already added");
        echo json_encode(array("statusCode"=>200,"msg"=>"pharmacy already added"));
        die();
      }
       $insert = $this->Common_model_new->insertData('cp_pharmacy',$array);
       if($insert){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        $this->session->set_flashdata('success', "Pharmacy added successfully");
        echo json_encode(array("statusCode"=>200,"msg"=>"pharmacy added successfully"));
        die();
      }

    }
  }

  public function edit_pharmacy()
  {
    if(count($_POST)>0){
        if($_POST['type']==2){
        $pharmacy_id = $this->input->post('id');
        
        $array =array(
           'user_id' => $this->session->userdata('logged_in')['id'],
           'pharmacy_type' => $this->input->post('pharmacy_type'),
           'button_name' => $this->input->post('button_name'),
           'url'=>$this->input->post('url'),
           'app_name' => $this->input->post('app_name'),
           'created' => date('Y-m-d H:i:s'));
       }

       $check_pharmacy = $this->Common_model_new->allexcludeone("cp_pharmacy",array("button_name" => $this->input->post('button_name'),"user_id" => $this->session->userdata('logged_in')['id']),$pharmacy_id);
      if(!empty($check_pharmacy)){
        unset($_POST);
        $this->session->set_flashdata('success', "Pharmacy already added");
        echo json_encode(array("statusCode"=>200,"msg"=>"pharmacy already added"));
        die();
      }
       $update = $this->Common_model_new->updateData('cp_pharmacy',$array,array('id' => $pharmacy_id));
       if($update){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        $this->session->set_flashdata('success', "Pharmacy updated successfully");
        echo json_encode(array("statusCode"=>200,"msg"=>"pharmacy updated successfully"));
        die();
      }

    }
  }

  public function delete_pharmacy()
  {
    if(count($_POST)>0){
        if($_POST['type']==3){
        $pharmacy_id = $this->input->post('id');

       $update = $this->Common_model_new->delete('cp_pharmacy',array('id' => $pharmacy_id));
       if($update){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        echo json_encode(array("statusCode"=>200,"msg"=>"pharmacy deleted successfully"));
        die();
      }

    }
  }
}
 
	



}
