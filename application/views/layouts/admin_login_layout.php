<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Pradeep Verma">
    <link rel="icon" href="<?php echo base_url(); ?>assets_new/images/favicon.ico" type="image/ico" sizes="16x16">
    <title>Care Pro - <?php echo $title;?></title>
    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url();?>assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/vendor/metisMenu/new_admin_login.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>assets/dist/css/sb-admin-2.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url();?>assets/vendor/morrisjs/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="icon" href="<?php echo base_url(); ?>/favicon.ico" type="image/gif">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">
<style>
section.admin-log {
    height: 100vh;
    padding: 25vh 0;
    font-family: 'Source Sans Pro', sans-serif !important
}
.login {
    background: #fff;
    padding: 30px;
    max-width: 450px;
    margin: 0 auto;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.17);
}
.login h2 {
    margin: 0 0 22px;
    font-size: 25px;
    position: relative;
    text-transform: uppercase;
    font-weight: 600;
    letter-spacing: 1.2px;
    padding: 0 0 10px;
    text-align: center;
}
.login input, .login input:focus {
    height: 40px;
    padding: 8px 15px;
    margin-bottom: 20px;
    color: #333;
    border: 1px solid rgba(0, 0, 0, 0.15) !important;
    box-shadow: none;
    outline: none;
    border-radius: 30px;
}
.login input.btn-primary {
    color: #fff;
    padding: 5px 20px;
    text-transform: uppercase;
    text-align: center;
    background:linear-gradient(to bottom, #2aa3ef 0%, #0466a9 100%) !important;
    border:none !important;
    margin:0;
    border-radius:30px;
    }
.login .form-group {
    margin:0px !important;
    }
.login h2:after {
    background:linear-gradient(to bottom, #2aa3ef 0%, #0466a9 100%);
    height: 2px;
    width: 50px;
    position: absolute;
    left: 0;
    right: 0;
    margin: 0 auto;
    bottom: -6px;
    content: '';
}
.login input.btn-primary:focus, .login input.btn-primary:hover {
    background: #000000 !important;
}
.login input:-webkit-autofill {
    border: 1px solid #ccc;
    -webkit-box-shadow: inset 0 0 0px 9999px white;
    outline: none;
}
.login input:-webkit-autofill:focus {
    border-color: #66afe9;
    -webkit-box-shadow: inset 0 0 0px 9999px white,
                        0 0 8px rgba(102, 175, 233, 0);
                        outline: none;
}
</style>
</head>
<body>
    <section class="admin-log">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="login"">
                         <?php echo $contents;?>
                    </div>                    
                </div>
            </div>
        </div>
    </section>

<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url();?>assets/vendor/metisMenu/metisMenu.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/raphael/raphael.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/morrisjs/morris.min.js"></script>
<script src="<?php echo base_url();?>assets/data/morris-data.js"></script>
<script src="<?php echo base_url();?>assets/dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_url("assets/lib/jquery/jquery.validate.min.js"); ?>"></script>

<script type="text/javascript">

    /*--------- Admin Login ----------*/
    $("#AdminLogin").validate({
        rules: {
          user_email:  {
            required: true,
            email: true
          },
          user_password: {
            required: true,
            minlength: 6
          }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

    /*--------- Reset password ----------*/
    $("#resetPass").validate({
        rules: {
          new_password:  {
            required: true,
            minlength: 6
          },
          confirm_password: {
            required: true,
            equalTo: "#new_password",
            minlength: 6
          }
        },
        // Specify validation error messages
        messages: {
          newpass: {
            required: "Please provide new password",
            minlength: "Your password must be at least 6 characters long"
          },
          newcpass: {
            required: "Please provide new password again",
            minlength: "Your password must be at least 6 characters long",
            equalTo: "Password must be same"
          },
        },
        submitHandler: function(form) {
          var formData = new FormData($("#resetPass")[0]);
          $.ajax({
            url: '<?php echo base_url(); ?>'+'reset/admin/password',
            type: 'POST',
            dataType: 'json',
            data: formData,
            success: function(result){
              $('#results').html(result.msg);
                if (result.status==200 && result.role==1) {
                    // redirect to google after 5 seconds
                    window.setTimeout(function() {
                        window.location.href = '<?php echo base_url(); ?>'+'admin/login';
                    }, 2000);
                } else {
                    // redirect to google after 5 seconds
                    window.setTimeout(function() {
                        window.location.href = '<?php echo base_url(); ?>';
                    }, 2000);
                }
            },
            cache: false,
            contentType: false,
            processData: false
          });
        }
    });
</script>
</body>
</html>