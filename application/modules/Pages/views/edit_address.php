<style>
    .btn.btn-primary {
        margin: 0px 0 15px !important;
    }

    .text_heading {
        border-bottom: 0;
    }
</style>

<div class=" text_heading">

    <h3><i class="fa fa-pencil-square-o"></i>Edit Address</h3>

    <div class="back_reminder"><a href="<?php echo base_url(); ?>address_book" class="btn btn-primary">Back</a></div>



</div>

<section class="content photo-list">

    <div class="photo-listMain">

        <form id="editaddressform">



            <div id="success_message"></div>

            <input type="hidden" name="address_id" id="address" value="<?php echo $singleData->address_id; ?>">
            <div class="row">

                <div class="col-md-12">

                    <div class="form-group">

                        <label for="">Address:<span class="red_star">*</span></label>

                        <textarea class="form-control" id="custaddress" name="address" value="" placeholder="Enter Address"><?php echo $singleData->address; ?></textarea>
                        <td class="error"><?php echo form_error('address'); ?></td>

                    </div>

                </div>

            </div>

            <div class="box-footerOne">

                <input type="submit" name="submit" class="btn btn-primary" value="Submit">
                 <input type="reset" name="cancel" class="btn btn-primary" value="Cancel" onclick="window.location='<?php echo base_url(); ?>address_book';">
                

            </div>

        </form>

    </div>

</section>

</aside>



<!--Added by 95 on 5-2-2019 For Doctor category for validation-->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDNUQSuu50z139ahVbm7vq9nnz5WwoL3OM&libraries=places"></script>
<script>
            var autocomplete = new google.maps.places.Autocomplete($("#custaddress")[0], {
                componentRestrictions: {
        country: "USA"
    }
            });

            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = autocomplete.getPlace();
                console.log(place.address_components);
            });


    $('#editaddressform').validate({

        rules: {

            address: "required",

            // reminder_description:"required",

            // reminder_date:"required",

            // reminder_time:"required",

            // before_time:"required",

            // repeat_time:"required",

        },

        messages: {

            //reminder_title: "Please enter reminder title.",

            address: "Please enter address.",

            // reminder_date:"Please select reminder date.",

            // reminder_time:"Please select reminder time.",

            // before_time:"Please select remind before time.",

            // repeat_time:"Please select repeat reminder time.",

        },

        submitHandler: function(form) {

            $.ajax({

                url: '<?php echo base_url(); ?>Pages/edit_address1',

                type: 'post',

                data: $(form).serialize(),

                success: function(response) {

                    if (response == 1) {

                        $('#success_message').fadeIn().html('<div class="alert alert-success message"><button type="button" class="close" data-dismiss="alert">x</button>Address updated successfully.</div>');

                        setTimeout(function() {

                            $('#success_message').fadeOut("milliseconds");

                        }, 500);

                    }

                    location.href = '<?php echo base_url(); ?>address_book';



                }

            });

        }

    });
</script>





<script type="text/javascript">
    $(function() {

        var today = new Date();

        $('#datetimepicker').datetimepicker({

            ignoreReadonly: true,

            format: 'YYYY-MM-DD',

            minDate: today

        });

    });
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>
