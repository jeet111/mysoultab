<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Caregiver_login extends MX_Controller {
  public function __construct() {
    parent:: __construct();
    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->library('table');
    $this->load->library('pagination');   
    $this->load->model('common_model_new');
	$this->load->model('common_model');
    $this->load->helper(array('common_helper'));
    $this->load->helper(array('url'));
    $this->load->helper(array('form'));  
   
  }
   public function index(){ 
         
          $data = array();
          $data['menuactive'] = $this->uri->segment(1);
          if(isset($_POST['submit'])){
            $email = $this->input->post('email');
            if (preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$email))
            { 
              $userdata = $this->common_model->getSingleRecordById('cp_users',array('email'=>$email,'user_role' => '4'));
			 // echo $this->db->last_query();die;
			  //echo '<pre>';print_r($userdata);die;
              if($userdata){
                $con = array(
                 'email' => $this->input->post('email'),
                 'password' =>  md5($this->input->post('password')));
                $checkLogin = $this->common_model->checkRow($con);
                if($checkLogin){
                  if($checkLogin['user_role'] != 1){
                    if($checkLogin['status'] == 0){
                      $this->session->set_flashdata('error', 'User inactive or deleted by admin.');
                      $this->template->load('home_layout', 'contents', 'login', $data);
                    }
                    else{
                      if(!empty($_POST["remember"])) {
                        setcookie ("email",$_POST["email"],time()+ 3600);
          //setcookie ("password",$_POST["password"],time()+ 3600);
                        setcookie ("remember",$_POST["remember"],time()+ 3600);
          //echo "Cookies Set Successfuly";
                      } else {
                        setcookie("email","");
          //setcookie("password","");
                        setcookie("remember","");
          //echo "Cookies Not Set";
                      }
                      $this->load->library('session');
                      $this->session->set_userdata('isUserLoggedIn',TRUE);
                      $this->session->set_userdata('logged_in', $checkLogin);
                      if($checkLogin['user_role'] == 4){
					   redirect('caregiver_dashboard');
					  }
                      echo "Logged in successfully";    
                    }
                  }else{
              // $error = base64_encode('The inserted Email is not associated with any account
              // ');
              // redirect('login?error='.$error);
                    $this->session->set_flashdata('error', 'The inserted email is not associated with any account.');
                    $this->template->load('home_layout', 'contents', 'login', $data);
                  }
                }else{
                  $this->session->set_flashdata('error', 'The inserted password is not correct.');
                  $this->template->load('home_layout', 'contents', 'login', $data);
              // $error = base64_encode('The inserted Email is not associated with any account
              // ');
              // redirect('login?error='.$error);
                }
              }else{
                $this->session->set_flashdata('error', 'The inserted email is not associated with any account.');
                $this->template->load('home_layout', 'contents', 'login', $data);
              } 
            }else{
             $userdata = $this->common_model->getSingleRecordById('cp_users',array('username'=>$email,'user_role' => '4'));
    //echo '<pre>';print_r($userdata);die;
             if($userdata){
              $con = array(
               'username' => $this->input->post('email'),
               'password' =>  md5($this->input->post('password')));
              $checkLogin = $this->common_model->checkRow($con);
              if($checkLogin){
                if($checkLogin['user_role'] != 1){
                  if($checkLogin['status'] == 0){
                    $this->session->set_flashdata('error', 'User inactive or deleted by admin.');
                    $this->template->load('home_layout', 'contents', 'login', $data);
                  }
                  else{
                    if(!empty($_POST["remember"])) {
                      setcookie ("email",$_POST["email"],time()+ 3600);
                      setcookie ("password",$_POST["password"],time()+ 3600);
                      setcookie ("remember",$_POST["remember"],time()+ 3600);
          //echo "Cookies Set Successfuly";
                    } else {
                      setcookie("email","");
                      setcookie("password","");
                      setcookie("remember","");
          //echo "Cookies Not Set";
                    }
                    $this->load->library('session');
                    $this->session->set_userdata('isUserLoggedIn',TRUE);
                    $this->session->set_userdata('logged_in', $checkLogin);
                    //redirect('dashboard');
					 if($checkLogin['user_role'] == 4){
						 redirect('caregiver_dashboard'); 
					  }
                    echo "Logged in successfully";
                  }
                }else{
              // $error = base64_encode('The inserted Email is not associated with any account
              // ');
              // redirect('login?error='.$error);
                  $this->session->set_flashdata('error', 'The inserted username is not associated with any account.');
                  $this->template->load('home_layout', 'contents', 'login', $data);
                }
              }else{
                $this->session->set_flashdata('error', 'The inserted password is not correct.');
                $this->template->load('home_layout', 'contents', 'login', $data);
              // $error = base64_encode('The inserted Email is not associated with any account
              // ');
              // redirect('login?error='.$error);
              }
            }else{ 
              $this->session->set_flashdata('error', 'The inserted username is not associated with any account.');
              $this->template->load('home_layout', 'contents', 'login', $data);
            } 
          }   
        }else{
          $this->template->set('title', 'Login');
          $this->template->load('home_layout', 'contents', 'login', $data);
        }
      }
}     
?>
