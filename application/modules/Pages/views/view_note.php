<style>
.text_heading {
    border-bottom: none;
    margin-bottom: 20px;
}
  form#detail_acknowledge {
    background: #fff;
    padding: 20px;
  }

  .main_section h1 {
    color: #000;
    padding-top: 30px;
    text-align: center;
    margin-top: 30px;
    margin-bottom: 27px;
  }

  section.content.photo-list {
    box-shadow: rgba(100, 100, 111, 0.4) 0px 7px 29px 0px;
  }

  .bx_med {
    color: #000;
    font-size: 17px;
    margin-bottom: 10px;
    border-bottom: 1px solid #ddd;
    padding-bottom: 6px;
  }
</style>

<div class=" text_heading">

  <h3><i class="fa fa-file-text" aria-hidden="true"></i> Note Detail</h3>

    <div class="rem_add"><a href="<?php echo base_url(); ?>note_list" class="btn btn-primary">Back</a></div>

 

</div>



<section class="content photo-list">

  <div class="photo-listMain">

    <form id="detail_acknowledge" method="post" class="frm_add">

      <div class="row">

        <div class="col-md-6">

          <div class="form-group">

            <div class="bx_med">Username:</div>

            <div class="bx_meSec">
              <?php $user = $this->session->userdata('logged_in');
              // print_r($user_id);
              ?>
              <label name="from_caregiver" id="from_caregiver" data-value="<?php echo $user['id']; ?>"><?php echo $username = $user['name'] . ' ' . $user['lastname']; ?></label>

              <?php echo form_error('from_caregiver'); ?>
            </div>

          </div>

        </div>

        <div class="col-md-6">

          <div class="form-group">

            <div class="bx_med">To Caregiver:</div>

            <div class="bx_meSec"><?php echo $tocaregiver->name . ' ' . $tocaregiver->lastname; ?></div>

          </div>

        </div>





      </div>





      <div class="row">



        <div class="col-md-12">

          <div class="form-group">

            <div class="bx_med">Description:</div>

            <div class="bx_meSec note_desc"><?php echo $singledata->description; ?></div>

          </div>

        </div>









      </div>





      <div class="box-footer">

        <a href="<?php echo base_url(); ?>edit_note/<?php echo $singledata->note_id; ?>" class="btn btn-primary">Edit</a>



      </div>

    </form>

  </div>

</section>

</aside>