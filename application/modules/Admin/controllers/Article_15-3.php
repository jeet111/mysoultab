  <?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  class Article extends MX_Controller {
    public function __construct(){   
      parent:: __construct();
      $this->load->library('session');
      $this->load->library('form_validation');
      $this->load->model('article_model');
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));
      $user_id = $this->session->userdata('logged_in')['id'];
      if(empty($user_id)) {
        redirect('admin/login');
      }
    }
    /*Created by 95 for show all articles*/
    public function articles_list()
    {
     $data['ArticlesData'] = $this->article_model->getAllArticles();
     $this->template->set('title', 'Articles');
     $this->template->load('admin_dashboard_layout', 'contents' , 'artilcles_list', $data);
   }
   /*Created by 95 for delete article*/
   public function delete_article($article_id='')
   {
    $this->article_model->delete('cp_articles',array('article_id' =>$article_id));
    $this->session->set_flashdata('success', 'Deleted successfully.');
    redirect('admin/articles_list');
  }
  /*Created by 95 for add article*/
  public function add_article()
  {

  $this->form_validation->set_rules('article_title','Article Title','required');
   
    $this->form_validation->set_rules('article_description','Article Description','required');
    
    
if ($this->form_validation->run() == TRUE) 
     {  


    if($_POST['submit']){
              //Check whether user upload picture
      if(!empty($_FILES['picture']['name'])){
        $config['upload_path'] = 'uploads/articles/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['picture']['name'];
                  //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('picture')){
          $uploadData = $this->upload->data();
          $picture = $uploadData['file_name'];
        }else{
          $picture = '';
        }
      }else{
        $picture = '';
      }
      $data = array(
        'article_title' => $this->input->post('article_title'),
        'article_desc' => $this->input->post('article_description'),
        'article_image' => $picture,
        'article_created' =>date('Y-m-d H:i:s')
      );
      $this->article_model->insertData('cp_articles',$data);
      $this->session->set_flashdata('success', 'Article Added Successfully.');
      redirect('admin/articles_list');
    }


  }

    $this->template->set('title', 'Articles');
    $this->template->load('admin_dashboard_layout', 'contents' , 'add_article');
  }
  /*Created by 95 for edit article*/
  public function edit_article($article_id='')
  {

    $this->form_validation->set_rules('article_title','Article Title','required');
   
    $this->form_validation->set_rules('article_description','Article Description','required');
    
    
if ($this->form_validation->run() == TRUE) 
     {  


    if($_POST['submit']){
              //Check whether user upload picture
      if(!empty($_FILES['picture']['name'])){
        $config['upload_path'] = 'uploads/articles/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['picture']['name'];
                  //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('picture')){
          $uploadData = $this->upload->data();
          $picture = $uploadData['file_name'];
        }else{
          $picture = '';
        }
        $data = array(
          'article_title' => $this->input->post('article_title'),
          'article_desc' => $this->input->post('article_description'),
          'article_image' => $picture,
          'article_created' =>date('Y-m-d H:i:s')
        );
        $this->article_model->updateData('cp_articles',$data,array('article_id'=>$article_id));
      }else{
        $picture = '';
      }
      $data = array(
        'article_title' => $this->input->post('article_title'),
        'article_desc' => $this->input->post('article_description'),
        'article_created' =>date('Y-m-d H:i:s')
      );
      $this->article_model->updateData('cp_articles',$data,array('article_id'=>$article_id));
      $this->session->set_flashdata('success', 'Article Updated Successfully.');
      redirect('admin/articles_list');
    }

  }  
    $data['article']=$this->article_model->getsingle('cp_articles',array('article_id'=>$article_id));
    $this->template->set('title', 'Articles');
    $this->template->load('admin_dashboard_layout', 'contents' , 'edit_article',$data);
  }
  }