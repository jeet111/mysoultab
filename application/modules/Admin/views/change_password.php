<div class="container-fluid">
  <div class="dash-counter">
    <div class="Schedule_main_one">
      <div class="users-main">
        <h2>Change Password</h2>
        <div id="results"></div>
        <form class="form-horizontal" id="updatepassword" method="POST">
          <div class="row">
            <div class="col-md-offset-1 col-md-10">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label col-md-4" for="">Old Password:<span class="field_req">*</span></label>
                  <div class="col-md-8">
                    <input type="Password" name="oldpass" placeholder="Enter old password" class="form-control">
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label col-md-4" for="">New Password:<span class="field_req">*</span></label>
                  <div class="col-md-8">
                    <input type="Password" name="newpass" id="newpass" placeholder="Enter new password" class="form-control">
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label col-md-4" for="">Confirm New Password:<span class="field_req">*</span></label>
                  <div class="col-md-8">
                    <input type="Password" name="newcpass" placeholder="Enter new password again" class="form-control cnfm-pass">
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group change_pass">
                  <div class="col-sm-offset-4 col-sm-4">
                    <button type="submit" class="chnge-mail btn btn-default upps">Change Password</button>
                  </div>
                  <div class="col-sm-4">
                    <a href="admin/dashboard">Cancel</a>
                  </div>
                </div>
              </div>


            </div>
          </div>
          <div class="row">
            <!-- <div class="col-md-12">
              <div class="form-group change_pass">
                <div class="col-sm-offset-4 col-sm-4">
                  <button type="submit" class="chnge-mail btn btn-default upps">Change Password</button>
                </div>
                <div class="col-sm-4">
                  <a href="admin/dashboard">Cancel</a>
                </div>
              </div>
            </div> -->
          </div>
        </form>
      </div>
    </div>
  </div>
</div>