<!-- jQuery UI 1.10.3 -->

<style>

div#video_loader {

  position: fixed;

  top: 0;

  bottom: 0;

  left: 0;

  right: 0;

  background-color: #0a0a0aa6;

  z-index: 999999;

  text-align: center;

  padding-top: 23%;

}

div#video_loader img#loading_img {

  width: 70px !important;

}

#flupmsgs{

  color:red

}

</style>

<aside class="right-side">

  <section class="content-header no-margin">

    <h1 class="text-center">

      <!-- <i class="fa fa-paper-plane"></i>  -->

      <i class="fa fa-phone-square" aria-hidden="true"></i>

      Add Contact

      <div class="back_reminder"><a href="<?php echo base_url(); ?>contact_list" class="btn btn-danger">Back</a></div>

    </h1>

  </section>

  <div id="image_msg"></div>

  <div id="loders"></div>

  <?php //echo "<pre>"; print_r($Testtypes); echo "<pre>"; ?>

  <section class="content photo-list">

    <div class="photo-listMain">

      <form id="add_contact" enctype="multipart/form-data" method="post" class="frm_add">

        <div class="row">

          <div class="col-sm-6 col-md-6">

            <div class="form-group">

              <label>Contact Name<font style="color:red;">*</font>:</label>

              <input type="text" name="contact_name" class="form-control valid" id="" placeholder="Contact Name" value="">

              <?php echo form_error('report_title'); ?>

            </div>

          </div>

          <input type="hidden" name="test_file" id="test_file" value="" >

          <div class="col-sm-6 col-md-6">

            <div class="form-group">

              <label>Contact Number<font style="color:red;">*</font>:</label>

              <input type="text" name="contact_number" class="form-control valid" id="contact_number" placeholder="Contact Number" value="">

              <?php echo form_error('contact_number'); ?>

            </div>

          </div>

        </div>

        <div class="row">

          <div class="col-sm-6 col-md-6">

            <div class="form-group">

              <label>Contact Image:</label>

              <input type="file" name="picture" class="form-control valid" id="reportFile" value="" onchange="readUrl(event)">

              <div id="flupmsgs"></div>

              <div class="profile-up">

                <?php if($reportData[0]->image==''){ ?>

                  <!-- <i class="fa fa-file-image-o" aria-hidden="true"></i> -->

                  <img  id='output' src="<?php echo base_url().'uploads/profile_images/user.png' ?>">

                <?php }else{ ?>

                  <img  id='output' src="<?php echo base_url().'uploads/test_report/'.$reportData[0]->image; ?>">

                <?php } ?>

              </div>

            </div>

          </div>

        </div>

        <div class="">

          <input type="submit" name="submit" class="btn btn-primary" value="Add Contact">

        </div>

      </form>

    </div>

  </section>

</aside>

<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

<script type="text/javascript">

  $("#add_contact").on('submit',(function(e)

  {

    e.preventDefault();

    var isvalidate=$("#add_contact").valid();

    var test_file = $("#test_file").val();

    if(isvalidate)

    {

      if(test_file == 1){

       $("#loders").append('<div id="video_loader" class="loading_class"><img id="loading_img" style="width:20px" src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif" /></div>');

     }

     $.ajax({

      url : "<?php echo base_url(); ?>Pages/UserContact/ajax_contact_add",

      type: "POST",

      data: new FormData(this),

      dataType: "json",

      contentType: false,

      cache: false,

      processData:false,

      success: function(data) {

        $("#loading_img").hide();

        $("#video_loader").hide();

        if(data.code == 1){

          $("#loading_img").hide();

          $("#video_loader").hide();

          $("#sub").prop('disabled', true);

          $("#image_msg").html('<div class="alert alert-success">'+data.message+'<div>');

          setTimeout(function(){

            $("#image_msg").html("");

            //location.reload();

            window.location.href = '<?php echo base_url(); ?>contact_list';

          }, 1000);

        }else{

          $("#loading_img").hide();

          $("#video_loader").hide();

          $("#image_msg").html('<div class="alert alert-danger">'+data.message+'<div>');

        }

      }

    });

   }

 }));

  $(document).ready(function() {

    $("#add_contact").validate({

      rules: {

        contact_name: "required",

        //picture: "required",

        contact_number: {

          required:true,

          number:true,

          minlength:10,

          maxlength:14,

        },

      },

      messages: {

        contact_name: "Please enter contact name.",

        //picture: "Please upload contact picture",

        contact_number:{

          required:"Please enter mobile number.",

          number:"Please enter only number.",

          minlength:"Please enter at least 10 digits.",

          maxlength:"Please do not enter more than 14 digits.",

        },

      },

    });

  });

  function readUrl (event) {

   var pcFile = $('#reportFile').val().split('\\').pop();

   var pcExt     = pcFile.split('.').pop();

   var output = document.getElementById('output');

   if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"

     || pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){

    $("#up_but").prop('disabled', false);

  $('#flupmsgs').html('');

  output.src = URL.createObjectURL(event.target.files[0]);

  $('#output').show();

  $("#test_file").val(1);

  $('#flupmsgs').html('');

}else{

 $("#test_file").val(2);

 $('#flupmsgs').html('Please select only Image file');

 $('#output').hide();

 $("#up_but").prop('disabled', true);

}

};

</script>

<script type="text/javascript">

  /*Created by 95 for disable alphabets in mobile field*/

  $('#contact_number').keypress(function (e) {

    var regex = new RegExp("^[0-9]+$");

    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);

    if (regex.test(str)) {

      return true;

    }

    e.preventDefault();

    return false;

  });

//   function loadFile (event) {

//      var pcFile = $('#profile_images').val().split('\\').pop();

//   var pcExt     = pcFile.split('.').pop();

//   var output = document.getElementById('img-upload');

//   if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"

//              || pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){

//     //$(".up_but").prop('disabled', false);

//         $('#flupmsg').html('');

//        output.src = URL.createObjectURL(event.target.files[0]);

//      $('#img-upload').show();

//       $('#flupmsg').html('');

//   }else{

//       $('#flupmsg').html('Please select only Image file');

//       $('#img-upload').hide();

//     //$(".up_but").prop('disabled', true);

//   }

// };

</script>

<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>

