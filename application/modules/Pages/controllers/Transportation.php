<?php defined('BASEPATH') or exit('No direct script access allowed');



class Transportation extends MX_Controller
{



  public function __construct()
  {

    parent::__construct();

    $this->load->library('session');

    $this->load->library('form_validation');

    $this->load->model('activities_model');

    $this->load->model('Common_model_new');

    $this->load->model('common_model_new');

    $this->load->model('movie_model');

    $this->load->helper(array('common_helper'));

    $this->load->helper(array('url'));

    $this->load->helper(array('form'));

    not_login();
  }


  public function transportation_dashboard()
  {
    $data['menuactive'] = '';
    $checkLogin = $this->session->userdata('logged_in');

    if (!empty($checkLogin)) {
      $data['menuactive'] = $this->uri->segment(1);

      unset($_POST);
      $user_id = $this->session->userdata('logged_in')['id'];
      $data['app_list'] = $this->common_model_new->getAllwhere("app_list", array('status' => 1, 'use_for' => "transportation"));
      $data['transportation_list'] = $this->common_model_new->getAllwhere("cp_transportation", array('user_id' => $this->session->userdata('logged_in')['id']));

      if (!empty($user_id) && $user_id  != '') {
        $data['transportation_array'] = $this->common_model_new->getAllwhere("setting_butns", array("btnname" => 'Transportation', "user_id" => $user_id));
      }
      $this->template->set('title', 'Transportation');

      $this->template->load('new_user_dashboard_layout', 'contents', 'transportation_dashboard', $data);
    } else {

      redirect('login');
    }
  }

  public function add_transportation()
  {
    if (count($_POST) > 0) {
      if ($_POST['type'] == 1) {


        $array = array(
          'user_id' => $this->session->userdata('logged_in')['id'],
          'transportation_type' => $this->input->post('transportation_type'),
          'button_name' => $this->input->post('button_name'),
          'url' => $this->input->post('url'),
          'app_name' => $this->input->post('app_name'),
          'created' => date('Y-m-d H:i:s')
        );
      }

      $check_transportation = $this->Common_model_new->getsingle("cp_transportation", array("button_name" => $this->input->post('button_name'), "user_id" => $this->session->userdata('logged_in')['id']));
      if (!empty($check_transportation)) {
        unset($_POST);
        $this->session->set_flashdata('success', "Transportation already added");
        echo json_encode(array("statusCode" => 200, "msg" => "Transportation already added"));
        die();
      }
      $insert = $this->Common_model_new->insertData('cp_transportation', $array);
      if ($insert) {
        //$this->session->set_flashdata('success','');
        unset($_POST);
        $this->session->set_flashdata('success', "Transportation added successfully");
        echo json_encode(array("statusCode" => 200, "msg" => "Transportation added successfully"));
        die();
      }
    }
  }

  public function edit_transportation()
  {
    if (count($_POST) > 0) {
      if ($_POST['type'] == 2) {
        $transportation_id = $this->input->post('id');

        $array = array(
          'user_id' => $this->session->userdata('logged_in')['id'],
          'transportation_type' => $this->input->post('transportation_type'),
          'button_name' => $this->input->post('button_name'),
          'url' => $this->input->post('url'),
          'app_name' => $this->input->post('app_name'),
          'created' => date('Y-m-d H:i:s')
        );
      }

      $check_transportation = $this->Common_model_new->allexcludeone("cp_transportation", array("button_name" => $this->input->post('button_name'), "user_id" => $this->session->userdata('logged_in')['id']), $transportation_id);
      if (!empty($check_transportation)) {
        unset($_POST);
        $this->session->set_flashdata('success', "Transportation already added");
        echo json_encode(array("statusCode" => 200, "msg" => "transportation already added"));
        die();
      }
      $update = $this->Common_model_new->updateData('cp_transportation', $array, array('id' => $transportation_id));
      if ($update) {
        //$this->session->set_flashdata('success','');
        unset($_POST);
        $this->session->set_flashdata('success', "Transportation updated successfully");
        echo json_encode(array("statusCode" => 200, "msg" => "transportation updated successfully"));
        die();
      }
    }
  }

  public function delete_transportation()
  {
    if (count($_POST) > 0) {
      if ($_POST['type'] == 3) {
        $transportation_id = $this->input->post('id');

        $update = $this->Common_model_new->delete('cp_transportation', array('id' => $transportation_id));
        if ($update) {
          //$this->session->set_flashdata('success','');
          unset($_POST);
          echo json_encode(array("statusCode" => 200, "msg" => "transportation deleted successfully"));
          die();
        }
      }
    }
  }
}
?>