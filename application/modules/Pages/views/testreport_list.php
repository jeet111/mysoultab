<style>
  .text_heading {
    border-bottom: 0;
  }

  .table-responsive {
    overflow-x: hidden;
  }

  th.sorting_asc {
    width: 13% !important;
    padding-left: 11px !important;
  }

  table#sampleTable th {
    border-bottom: none;
    padding: 4px 8px;
  }

  table#sampleTable {
    table-layout: inherit;
  }

  .btn.btn-primary {
    margin: 0px 0 14px !important;
  }

  .tab_medi td a.edit_btn {
    background: #1f569e;
    color: #fff;
    max-width: 69px;
    width: 100%;
    border-radius: 41px;
    text-align: center;
  }

  tbody tr td:last-child {
    text-align: left;
  }
</style>
<div class="text_heading">

  <div id="msg_show"></div>
  <h3><i class="fa fa-list"></i> Test Report List</h3>
  <div class="tooltip-2">

    <h2>Display Test Report button on Mobile App

      <?php
      if (is_array($btnsetingArray)) {

        foreach ($btnsetingArray as $res) {
          if ($res->settings == 1) { ?>
            <label class="switch ">
              <input type="checkbox" checked data-btn="<?php echo 'Test Report' ?>" class="updateStatus">
              <span class="slider round"></span>
            </label>

          <?php } else { ?>
            <label class="switch space">
              <input type="checkbox" data-btn="<?php echo 'Test Report' ?>" class="updateStatus">
              <span class="slider round"></span>
            </label>


          <?php } ?>
      <?php }
      }
      ?>
    </h2>

  </div>
  <div class="rem_add"><a href="<?php echo base_url(); ?>add_testreport" class="btn btn-primary">Add</a></h3>

  </div>

</div>



<?php //echo "<pre>"; print_r($TestReportData); echo "</pre>"; 
?>

<section class="content photo-list">
  <div class="photo-listMain reminder_listMain">
    <div class="table-responsive tb_swt">
      <table class="table table-hover table-bordered table-mailbox send-user-mail tab_medi" id="sampleTable">

        <thead>

          <tr>

            <th> <input type="checkbox" id="checkall" name="">

              <a href="javascript:void(0);" name="btn_delete" id="btn_delete" class="btn_delete"> <i aria-hidden="true" class="fa fa-trash-o"></i></a> Delete All

            </th>


            <th>Doctor Name</th>

            <th>Test Type</th>

            <th>Description</th>

            <th>Action</th>

          </tr>

        </thead>



        <tbody>

          <?php foreach ($TestReportData as $report) {



          ?>



            <tr>

              <td class="small-col" data-label="Delete All">
                <input type="checkbox" name="sub_chk[]" class="sub_chk" value="<?php echo $report->report_id; ?>" data-id="<?php echo $report->report_id;  ?>" />
              </td>
              <td data-label="Doctor Name">
                <?php if (!empty($report->other_doctor_name)) { ?>
                  <?php echo $report->other_doctor_name; ?>

                <?php } else { ?>
                  <?php echo $report->doctor_name; ?>

                <?php } ?>
              </td>
              <td data-label="Test Type">
                <?php if (!empty($report->other_testreport_name)) { ?>
                  <?php echo $report->other_testreport_name; ?>

                <?php } else { ?>
                  <?php echo $report->test_type; ?>
                <?php } ?>
              </td>



              <td data-label="Description"><?php

                  echo mb_strimwidth($report->description, 0, 10, "..."); ?></td>

              <td data-label="Action">

                <a href="<?php echo base_url(); ?>view_testreport/<?php echo $report->report_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

                <a href="<?php echo base_url(); ?>edit_testreport/<?php echo $report->report_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                <a href="<?php echo base_url(); ?>delete_testreport/<?php echo $report->report_id; ?>" onclick="return confirm('Are you sure, you want to delete this test report ?')"><i class="fa fa-trash" aria-hidden="true"></i></a>

              </td>

            </tr>

          <?php } ?>



        </tbody>

      </table>

    </div>

  </div>

</section>
<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    jQuery('#checkall').on('click', function(e) {

      if ($(this).is(':checked', true))

      {

        $(".sub_chk").prop('checked', true);

      } else

      {

        $(".sub_chk").prop('checked', false);

      }

    });
    $('#sampleTable').DataTable();
  });
  $(document).on('click', '.btn_delete', function(e) {





    var bodytype = $('input[name="sub_chk[]"]:checked').map(function() {

      return this.value;

    }).get().join(",");



    if (bodytype != '') {

      if (confirm("Are you sure you want to remove this note ?")) {

        $.ajax({

          url: '<?php echo base_url(); ?>Pages/Testreport/delete_testreport',

          type: 'POST',

          data: {
            bodytype: bodytype
          },

          success: function(data) {
            setTimeout(function() {
              $("#msg_show").html("<div class='alert alert-success message'>Successfully deleted !</div>");
              location.reload();

            }, 1300);

          }

        });



      }

    } else {

      alert("Please select atleast one note.");

    }

  });
  $(document).on('click', '.updateStatus', function() {
    var update_status = $(this).attr('data-btn');
    var update_st = $(this).attr('data-st');
    //alert(update_status+'-'+update_st);
    $.ajax({
      url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
      type: "POST",
      data: {
        'btn': update_status
      },
      dataType: "json",
      success: function(data) {
        $("#msg_show").html("<div class='alert alert-success'>" + data.status + "</div>");

      }
    });
  });
</script>