<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Admin_plan extends MX_Controller {



   private $staff_permission = array();
   private $access_permission = true;
   public function __construct(){



      parent:: __construct();



      $this->load->library('session');

      $this->load->library('form_validation');

      $this->load->model('login_model');

      $this->load->model('Common_model');

      $this->load->helper(array('url'));

      $this->load->helper(array('form'));



      $user_id = $this->session->userdata('logged_ins')['id'];

      if(empty($user_id)) {

         redirect('admin/login');

      }



      if ($this->session->userdata('logged_ins')['user_role'] == 5) {

         $uid = $this->session->userdata('logged_ins')['id'];

         $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   

         if (!empty($permission_list)) 

         {

            foreach ($permission_list as $k => $v) 

            {

               $this->staff_permission[$v['permission_id']] = $v;

            }

         }

      }



   }


   public function permission_access($permission_id=19)
   {
      if ($this->session->userdata('logged_ins')['user_role'] != 1) 
      {
         if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
         {
            $this->access_permission = false;
         }
      }
   }


   public function plan_list() {

      $data['menuactive'] = $this->uri->segment(2);

      

      $data['plan_list'] = $this->login_model->getRows('subscription_plan');

      $data['staff_permission'] = $this->staff_permission;

      $this->permission_access(19);
      if ($this->access_permission) {
         $this->template->set('title', 'Admin Panel');
         $this->template->load('admin_dashboard_layout', 'contents' , 'plan_listing', $data);
      } else {
         $this->template->set('title', 'Admin Panel - Staff');
         $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
      }
   }



   public function add_plan(){

    

      $data['menuactive'] = $this->uri->segment(2);
      if (isset($_POST['btnSubmit'])) {
        // print_r($_POST);
        // die;
       
        $plan_name = $this->input->post('plan_name');

        $amount = $this->input->post('amount');

        $plan_days = $this->input->post('plan_days');

        $description = $this->input->post('description');

        $plan_id = $this->input->post('plan_id');



        if (!empty($plan_id)) {

         $post_data = array(

            'plan_name' => $plan_name,

            'description' => $description,

            'amount' => $amount,

            'plan_days' => $plan_days,

            'updated_at' => date('Y-m-d H:i:s')

         );

         $where_condition = array('id'=>$plan_id);

         $resp = $this->login_model->updateRecords('subscription_plan', $post_data, $where_condition);



         if ($resp) {

            $data['success'] = 'plan updated successfully!';

         } else {

            $data['error'] = 'Some internal issue occured.';

         }



      } else {

         $post_data = array(

            'plan_name' => $plan_name,

            'description' => $description,

            'amount' => $amount,

            'plan_days' => $plan_days,

            'status' => 1,

            'created_at' => date('Y-m-d H:i:s'),

            'updated_at' => date('Y-m-d H:i:s')

         );

         $resp = $this->login_model->insertrecords('subscription_plan',$post_data);



         if ($resp) {

            $data['success'] = 'plan added successfully!';

         } else {

            $data['error'] = 'Some internal issue occured.';

         }



      }

   }



   $plan_id = $this->uri->segment(3);

   $data['plan_detail'] = array();

   if (!empty($plan_id)) {

      $data['plan_detail'] = $this->login_model->getSingleRecordById('subscription_plan',array('id'=>$plan_id));

   }

   

   $data['staff_permission'] = $this->staff_permission;

   $this->permission_access(19);
   if ($this->access_permission) {
      $this->template->set('title', 'Admin Panel');
      $this->template->load('admin_dashboard_layout', 'contents' , 'add_plan', $data);
   } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
   }
}



public function delete_plan(){

   $plan_id = $this->input->post('plan_id');

   $resp = $this->login_model->deleteRecords('subscription_plan',array('id'=>$plan_id));

   if ($resp) {

      echo 1;

   } else {

      echo 0;

   }

}

}