<div class="container-fluid">
	<div class="dash-counter">
		<div class="Schedule_main_one">
			<div class="users-main">
				<?php
				echo "<h2>" ?><?php if($this->uri->segment('3')){ ?>Edit About-us<?php }else{ ?>Edit About-us<?php } ?></h2>

        <div class="btn_topBack">
          <a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">
            Back
          </a>
        </div>

        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" name="test_type_add" id="test_type_add">
          <?php if ($this->session->flashdata('success')) { ?>
            <div class="alert alert-success message">
              <button type="button" class="close" data-dismiss="alert">x</button>
              <?php echo $this->session->flashdata('success'); ?></div>
            <?php } ?>
            <div class="row">
              <div class="col-md-12">
               <div class="form-group">
                <!-- <label class="control-label col-md-4" for="">Test type:<span class="field_req">*</span></label> -->
                <div class="col-md-12">
                  <textarea  id="editor1" name="editor1" data-sample-short>
                    <?php echo $singleData->description; ?>
                  </textarea>
                  <?php echo form_error('editor1'); ?>

                </div>
              </div>
            </div>


          </div>



          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
             <input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit">


              <!-- <a class="cancel-btn btn" href="javascript:window.history.back()">
                        Back
                      </a> -->
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        <!--Added by 95 on 5-2-2019 For Doctor category for validation-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>
        <script type="text/javascript">

         $(document).ready(function() {
           $("#test_type_add").validate({
             rules: {
              test_type: "required",

            },
            messages:{
              test_type:"Plesase enter a test type."
            }

          });

         });



       </script>

       <script src="https://cdn.ckeditor.com/4.14.0/standard-all/ckeditor.js"></script>
       <script>

   //  CKEDITOR.replace('post_content', {
   //   allowedContent:true,
   // });



   var editor1 = CKEDITOR.replace('editor1', {
    allowedContent:true,
    extraAllowedContent: 'div',
    height: 460
  });
   editor1.on('instanceReady', function() {
      // Output self-closing tags the HTML4 way, like <br>.
      this.dataProcessor.writer.selfClosingEnd = '>';

      // Use line breaks for block elements, tables, and lists.
      var dtd = CKEDITOR.dtd;
      for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
        this.dataProcessor.writer.setRules(e, {
          indent: true,
          breakBeforeOpen: true,
          breakAfterOpen: true,
          breakBeforeClose: true,
          breakAfterClose: true
        });
      }
      // Start in source mode.
      this.setMode('source');
    });
  </script>

