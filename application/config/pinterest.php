<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
|  LinkedIn API Configuration
| -------------------------------------------------------------------
|
| To get an facebook app details you have to create a Facebook app
| at Facebook developers panel (https://developers.facebook.com)
|
|  linkedin_api_key        string   Your LinkedIn App Client ID.
|  linkedin_api_secret     string   Your LinkedIn App Client Secret.
|  linkedin_redirect_url   string   URL to redirect back to after login. (do not include base URL)
|  linkedin_scope          array    Your required permissions.
*/
$root = str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

$config['pinterest_app_id']     = '5004245584018699998';
$config['pinterest_api_secret']    = 'd1a972b79ea3d7ad0150e6d9d5dfeaad31fa900c53323793a9dc94b2debdab29';
$config['pinterest_redirect_url']  = 'https://'.$_SERVER['HTTP_HOST'].$root.'pinterest/login';
$config['pinterest_scope']         = 'read_public,write_public';