<div class="container-fluid">
  <div class="dash-counter">
    <div class="Schedule_main_one">
      <div class="users-main">
        <h2><?php echo (!empty($testimonial_detail) ? 'Edit' : 'Add'); ?> Testimonial</h2>
        <div class="btn_topBack">
          <a class="cancel-btn btn bk_btn" href="<?php echo base_url().'admin/testimonial_list'; ?>">
            Back
          </a>
        </div>

         <?php
         if (!empty($success)) {
            ?>
            <div class="alert alert-success alert-dismissible">
               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
               <strong>Success!</strong> <?php echo $success; ?>
            </div>
            <?php
         }

         if (!empty($error)) {
            ?>
            <div class="alert alert-danger alert-dismissible">
               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
               <strong>Error!</strong> <?php echo $error; ?>
            </div>
            <?php
         }
         ?>
         


        <form class="form-horizontal" action="<?php echo base_url(); ?>admin/add_testimonial<?php echo (!empty($testimonial_detail['id']) ? '/'.$testimonial_detail['id'] : ''); ?>" method="post" enctype="multipart/form-data" name="AddTestimonial" id="AddTestimonial">
            <input type="hidden" name="testimonial_id" value="<?php echo (!empty($testimonial_detail['id']) ? $testimonial_detail['id'] : ''); ?>">
            <div class="form-group">
              <label class="control-label col-sm-2" for="name">Name:</label>
              <div class="col-sm-10">
                <input type="text" required value="<?php echo (!empty($testimonial_detail['name']) ? $testimonial_detail['name'] : ''); ?>" class="form-control" name="name" id="name" placeholder="Enter name">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="designation">Designation:</label>
              <div class="col-sm-10">
                <input type="text" required value="<?php echo (!empty($testimonial_detail['designation']) ? $testimonial_detail['designation'] : ''); ?>" class="form-control" name="designation" id="designation" placeholder="Enter designation">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="description">Description:</label>
              <div class="col-sm-10">
                <textarea class="form-control" required name="description" id="description" placeholder="Enter description"><?php echo (!empty($testimonial_detail['description']) ? $testimonial_detail['description'] : ''); ?></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="image">Image:</label>
              <div class="col-sm-10">
                <input type="file" <?php echo (!empty($testimonial_detail) ? '' : 'required'); ?> class="form-control" name="image" id="image" >
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="btnSubmit" value="Submit" class="btn btn-default"><?php echo (!empty($testimonial_detail) ? 'Update' : 'Submit'); ?></button>
              </div>
            </div>

        </form>
      </div>
    </div>
  </div>
</div>
      
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

      <?php
      if (!empty($testimonial_detail)) {
        ?>
        <script>
          $(document).ready(function () {

              $("#AddTestimonial").validate({
                  rules: {
                      name: {
                          required: true
                      },
                      designation: {
                          required: true
                      },
                      description: {
                          required: true
                      }
                  },
                  messages: {
                      name: {
                          required: "This field is required."
                      },
                      designation: {
                          required: "This field is required."
                      },
                      description: {
                          required: "This field is required."
                      }
                  },
                  submitHandler: function (form) { // for demo
                      form.submit();
                  }
              });

          });
        </script>
        <?php
      } else {
        ?>
        <script>
          $(document).ready(function () {

              $("#AddTestimonial").validate({
                  rules: {
                      name: {
                          required: true
                      },
                      designation: {
                          required: true
                      },
                      description: {
                          required: true
                      },
                      image: {
                          required: true
                      }
                  },
                  messages: {
                      name: {
                          required: "This field is required."
                      },
                      designation: {
                          required: "This field is required."
                      },
                      description: {
                          required: "This field is required."
                      },
                      image: {
                          required: "This field is required."
                      }
                  },
                  submitHandler: function (form) { // for demo
                      form.submit();
                  }
              });

          });
        </script>
        <?php
      }
      ?>
      