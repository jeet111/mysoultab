<!-- jQuery UI 1.10.3 -->
        
<aside class="right-side">    
    <section class="content-header no-margin">
        <h1 class="text-center">
          <!-- <i class="fa fa-paper-plane"></i>  -->
          <i class="fa fa-phone-square" aria-hidden="true"></i>
          Edit Contact
    <div class="back_reminder"><a href="<?php echo base_url(); ?>family_member/contact_list" class="btn btn-danger">Back</a></div>
    </h1>
    </section>
    <?php //echo "<pre>"; print_r($ContactData); echo "</pre>"; ?>    
    <section class="content photo-list">
        <div class="photo-listMain">            
          <form id="add_contact" enctype="multipart/form-data"  method="post" class="frm_add">
            <div class="row">
                    <div class="col-md-6">                        
                      <div class="form-group">
                          <label>Contact Name<font style="color:red;">*</font>:</label>
                          <input type="text" name="contact_name" class="form-control valid" id="" placeholder="Contact Name" value="<?php echo $ContactData[0]->contact_name; ?>">
                          <?php echo form_error('report_title'); ?>
                      </div>                                  
                    </div>


                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Contact Number<font style="color:red;">*</font>:</label>
                          <input type="text" name="contact_number" class="form-control valid" id="contact_number" placeholder="Contact Number" value="<?php echo $ContactData[0]->contact_mobile_number; ?>">
                          <?php echo form_error('contact_number'); ?>
                      </div>               
                    </div>
                    
                </div>
                <div class="row">
                    
                  <div class="col-md-6">
                      <div class="form-group">
                          <label>Contact Image:</label>
                          <input type="file" name="picture" class="form-control valid" id="reportFile" value="" onchange="readUrl(event)">
                          <div id="flupmsgs"></div>

                          <div class="profile-up">
                          <?php if($ContactData[0]->user_contact_image==''){ ?>

                          <!-- <i class="fa fa-file-image-o" aria-hidden="true"></i> -->


        <img  id='output' src="<?php echo base_url().'uploads/profile_images/user.png' ?>">

      <?php }else{ ?>

        <img  id='output' src="<?php echo base_url().'uploads/contact/'.$ContactData[0]->user_contact_image; ?>">

      <?php } ?> 
    </div>
                      </div> 




                    </div>


                </div>
                
                <div class="box-footer">
                    <input type="submit" name="submit" class="btn btn-primary" value="Update Contact">
                </div>               
            </form>              
        </div>
    </section>
</aside>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script> 
  <script type="text/javascript">
    $(document).ready(function() {
    $("#add_contact").validate({
            rules: {
                contact_name: "required",
                contact_number: {
                    required:true,
                    number:true,
                    minlength:10,
                    maxlength:10,
                  },
             },
        
            messages: {
                contact_name: "Please enter contact name.",
                contact_number:{
                        required:"Please enter mobile number.",
                        number:"Please enter only number.",
                        minlength:"Please enter at least 10 digits.",
                        maxlength:"Please do not enter more than 10 digits.",
                      },
                
            },
        });

    });



    function readUrl (event) {
     var pcFile = $('#reportFile').val().split('\\').pop();
  var pcExt     = pcFile.split('.').pop();
  var output = document.getElementById('output');
  if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"
             || pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){
    $("#up_but").prop('disabled', false);
        $('#flupmsgs').html('');       
      
       output.src = URL.createObjectURL(event.target.files[0]);
     $('#output').show();
      $('#flupmsgs').html('');
  }else{
      $('#flupmsgs').html('Please select only Image file');
      $('#output').hide();
    $("#up_but").prop('disabled', true);
  }
};
</script>


<script type="text/javascript">
  /*Created by 95 for disable alphabets in mobile field*/
$('#contact_number').keypress(function (e) {
    var regex = new RegExp("^[0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }
    e.preventDefault();
    return false;
});
</script>





<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>



    


    

                