<div class="container-fluid">
	<div class="dash-counter">
		<div class="Schedule_main_one">
			<div class="users-main">

				<?php //echo "<pre>"; print_r($unitData); echo "</pre>"; ?>

				<?php
				echo "<h2>" ?><?php if($this->uri->segment('3')){ ?>Edit Test type<?php }else{ ?>Set Unit <?php } ?></h2>

				<div class="btn_topBack">
					<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">
						Back
					</a>
				</div>

				<form class="form-horizontal" action="" method="post" enctype="multipart/form-data" name="set_units" id="set_unit">
					<?php if ($this->session->flashdata('success')) { ?>
						<div class="alert alert-success message">
							<button type="button" class="close" data-dismiss="alert">x</button>
							<?php echo $this->session->flashdata('success'); ?></div>
						<?php } ?>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Units:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<select name="unit" type="text" class="form-control" >
											<option value="">Select</option>
											<option value="Miles" <?php if($unitData[0]->place_unit=="Miles"){echo "selected";} ?> >Miles</option>
											<option value="KM" <?php if($unitData[0]->place_unit=="KM"){echo "selected";} ?>>KM</option>
										</select>

									</div>
								</div>
							</div>


						</div>



						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit">

							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!--Added by 95 on 5-2-2019 For Doctor category for validation-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>
	<script type="text/javascript">

		$(document).ready(function(){
			$("#set_unit").validate({
				rules: {
					unit: "required",
				},
				messages: {
					unit: "Please select a unit.",
				}
			});

		});
	</script>



