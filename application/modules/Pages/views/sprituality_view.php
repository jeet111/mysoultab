<aside class="right-side">
  <section class="content-header no-margin">
    <h1 class="text-center"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Sprituality 
      <div class="rem_add">
        <!-- <a href="<?php echo base_url(); ?>add_social" class="btn btn-primary">Add New</a> -->
      </div>
    </h1>
  </section>

  <section class="content weather-bg news_section">
    <!-- setting start -->
    <div class="col-md-12">

      <div id="msg_show"></div>

      <div class="col-md-10">
        <h3>Sprituality</h3>

        <?php $current_user_id = $_SESSION['logged_in']['id'];
         foreach($btnsetingArray as $res){
          $userid = $res->user_id;
          if($current_user_id == $userid){
            if($res->settings == 1) { ?>
            <div class="tooltip-2"><a data-btn="<?php echo 'sprituality'?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
              <span class="tooltiptext">Active</span>
            </div>
          <?php }else { ?>
            <div class="tooltip-2 "><a data-btn="<?php echo 'sprituality'?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
              <span class="tooltiptext">Deactive</span>
            </div>
          <?php } ?>
        <?php }
        } ?>
      </div>
    </section>


  <?php /* ?>
  <section class="content weather-bg news_section">
    <!-- setting start -->
    <div class="col-md-12">
      <div class="col-md-10">
        <h3>Sprituality</h3>
        <?php if(!empty($btnsetingArray)){ ?>
          <?php if($btnsetingArray[0]->settings=='1'){ ?>
            <label><input type="radio" name="btnshow" value='1' checked="checked">Now Sprituality showing on the mysoultab APP<br></label>
          <?php }else{ ?>
           <label> <input type="radio" name="btnshow" value='1' checked="checked">Now Sprituality not showing on the mysoultab APP </label>
         <?php } ?>
       <?php } ?> 
     </div>
     <div class="col-md-2 delete-email">
      <button class="btn btn-primary chk">Settings</button>
    </div> 
    <div id="para" style="display: none "> 
      <form id="submit_form" method="post" action="<?php echo base_url()?>savsettings "> 
        <div class="col-md-10">
          <label> <input type="checkbox" name="butn_seting" id="butn_seting" value="">Please check if want to show the Yoga in mysoultab App </label>
          <input type="hidden" name="btn" value="sprituality">
        </div>
        <div class="col-md-2">
         <input class="btn btn-primary chkdd" type="submit" name="submit" value="submit" >
       </div>
     </form>
     <div id="response"></div>
   </div>
   <!-- </div> -->
   <!-- End -->
 </section>

 <?php */ ?>

 <div id="expire_msg"></div>
 <?php if ($this->session->flashdata('success')) { ?>
  <div class="alert alert-success message">
   <button type="button" class="close" data-dismiss="alert">x</button>
   <?php echo $this->session->flashdata('success'); ?></div>
 <?php } ?>
 <style type="text/css">  
   table {
    width: 100%;
  }
  td {
    width: 50%;
  }
  tr {
    /*width: 100%;*/
    /** what should go here? **/
  }
</style>
<section class="content photo-list">
 <div class="photo-listMain reminder_listMain">
  <div class="row swt_cor">
   <div class="col-sm-6">
    <label style="margin-right: 10px;">
    </label>
  </div>
</div>
<table class="soc" border="0">
  <tr>
    <!-- <td>
      <center><img width="200" height="200" src="<?php echo base_url()?>uploads/calling/skype-512.png"></center>  
    </td> -->
    <td>
      <center><img width="200" height="200" src="<?php echo base_url()?>uploads/sprituality/icon_wellbeing_spiritual.png"></center> 
    </td>
  </tr>
  <table>
  </div>
</section>
</aside>
<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>
<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).on('click','.updateStatus',function(){
    var update_status = $(this).attr('data-btn');
    var update_st = $(this).attr('data-st');
    //alert(update_status+'-'+update_st);
    $.ajax({
      url: '<?php echo base_url();?>Pages/pages/change_button_status',
      type: "POST",
      data: {'btn':update_status},
      dataType: "json",
      success:function(data){
        setTimeout(function(){
          $("#msg_show").html("<div class='alert alert-success'>"+data.status+"</div>");
          location.reload();

        }, 1500);
      }
    }); 
  });
</script>