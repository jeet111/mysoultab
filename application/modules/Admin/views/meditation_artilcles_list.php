<style>

.article_wrap{

display: block; /* or inline-block */

text-overflow: ellipsis;

word-wrap: break-word;

overflow: hidden;

max-height: 2.6em;

line-height: 1.8em;

}



</style>



<?php

if (isset($staff_permission[14]['add_permission']) == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn">

			<a href="<?php echo base_url(); ?>admin/add_meditation_article">

				Add Meditation Article

			</a>

		</div>

	</div>

	<?php

} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn">

			<a href="<?php echo base_url(); ?>admin/add_meditation_article">

				Add Meditation Article

			</a>

		</div>

	</div>

	<?php

}

?>





<div class="container-fluid">

<div class="dash-counter users-main my-sts-table">

<div class="row">

<div class="col-md-12">

<div class="">

<?php //echo "<pre>"; print_r($acknowledgeData); echo "</pre>";?>

<div class="card-body">

<div class="status-cng"></div>

<?php if ($this->session->flashdata('success')) { ?>

<div class="alert alert-success message">

<button type="button" class="close" data-dismiss="alert">x</button>

<?php echo $this->session->flashdata('success'); ?></div>

<?php } ?>



<!-- <div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_article">Add Article</a></div> -->

<table class="table table-hover tab_comn" id="sampleTable">

<thead>

<tr>

	<th>S no.</th>

	<th>Article Title</th>

	<th>Article Description</th>

	<th>Article Image</th>

	<th>Action</th>

</tr>

</thead>

<tbody>

<?php

$i = 1;

foreach ($ArticlesData as $Article) {



	?>

	<tr>

		<td><?php echo $i++; ?></td>

		<td><?php echo $Article->article_title; ?></td>

		<td class="article_wrap"><?php echo $Article->article_desc; ?></td>

		<td><img src="<?php echo base_url('uploads/meditation_articles/'.$Article->article_image) ?>" height="50px" width="100px">    </td>

		



		<td>

			<div class="link-del-view">



				<?php

				if (isset($staff_permission[14]['view_permission']) == 1) {

					?>

					<div class="tooltip-2">

						<a href="admin/view_meditation_article/<?php echo $Article->article_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

						<span class="tooltiptext">View</span>

					</div>

					<?php

				} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

					?>

					<div class="tooltip-2">

						<a href="admin/view_meditation_article/<?php echo $Article->article_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

						<span class="tooltiptext">View</span>

					</div>

					<?php

				}

				?>

				

				<?php

				if (isset($staff_permission[14]['edit_permission']) == 1) {

					?>

					<div class="tooltip-2">

						<a href="<?php echo base_url(); ?>admin/edit_meditation_article/<?php echo $Article->article_id; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>

						<span class="tooltiptext">Edit</span>

					</div>

					<?php

				} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

					?>

					<div class="tooltip-2">

						<a href="<?php echo base_url(); ?>admin/edit_meditation_article/<?php echo $Article->article_id; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>

						<span class="tooltiptext">Edit</span>

					</div>

					<?php

				}

				?>

				

				<?php

				if (isset($staff_permission[14]['delete_permission']) == 1) {

					?>

					<div class="tooltip-2">

						<a href="<?php echo base_url(); ?>admin/delete_meditation_article/<?php echo $Article->article_id; ?>" onclick="return confirm('Are you sure, you want to delete this article ?')">

							<i class="fa fa-trash-o" aria-hidden="true"></i>

						</a>

						<span class="tooltiptext">

							Delete

						</span>

					</div>

					<?php

				} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

					?>

					<div class="tooltip-2">

						<a href="<?php echo base_url(); ?>admin/delete_meditation_article/<?php echo $Article->article_id; ?>" onclick="return confirm('Are you sure, you want to delete this article ?')">

							<i class="fa fa-trash-o" aria-hidden="true"></i>

						</a>

						<span class="tooltiptext">

							Delete

						</span>

					</div>

					<?php

				}

				?>

				





			<!-- </div> -->



		</div>

	</td>

</tr>

<?php } ?>

</tbody>

</table>

</div>

</div>

</div>

</div>

</div>

</div>