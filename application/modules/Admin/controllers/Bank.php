  <?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  class Bank extends MX_Controller {
    private $staff_permission = array();
    private $access_permission = true;
    public function __construct(){   
      parent:: __construct();
      $this->load->library('session');
      $this->load->library('form_validation');
      $this->load->model('bank_model');
      $this->load->model('login_model');
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));
      $user_id = $this->session->userdata('logged_ins')['id'];
      if(empty($user_id)) {
        redirect('admin/login');
      }

      if ($this->session->userdata('logged_ins')['user_role'] == 5) {
        $uid = $this->session->userdata('logged_ins')['id'];
        $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
        if (!empty($permission_list)) 
        {
           foreach ($permission_list as $k => $v) 
           {
              $this->staff_permission[$v['permission_id']] = $v;
           }
        }
      }
    }

    public function permission_access($permission_id=10)
    {
      if ($this->session->userdata('logged_ins')['user_role'] != 1) 
      {
         if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
         {
            $this->access_permission = false;
         }
      }
    }

    /*BANK SECTON*/

    /*Created by 95 for show all banks*/
    public function bank_list()
    {

     $data['menuactive'] = $this->uri->segment(2);
     $data['bankData'] = $this->bank_model->getAllbanks();
     
     $data['staff_permission'] = $this->staff_permission;
     

     $this->permission_access(10);
     if ($this->access_permission) {
        $this->template->set('title', 'Banks');
        $this->template->load('admin_dashboard_layout', 'contents', 'banks_list', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }


   /*Created by 95 for delete bank*/
   public function delete_bank($bank_id='')
   {
    $this->bank_model->delete('cp_bank',array('bank_id' =>$bank_id));

    $this->bank_model->delete('cp_fav_banks',array('bank_id' =>$bank_id));
    $this->session->set_flashdata('success', 'Deleted successfully.');
    redirect('admin/bank_list');
  }

  /*View single bank*/
  public function view_bank($bank_id='')
    {
     $data['viewBank']=$this->bank_model->getBank($bank_id);
     
     $data['staff_permission'] = $this->staff_permission;
     
     $this->permission_access(10);
     if ($this->access_permission) {
        $this->template->set('title', 'Banks');
        $this->template->load('admin_dashboard_layout', 'contents', 'view_bank', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }




/*FAVOURITE BANK SECTON*/

  /*Created by 95 for show all favourite banks*/
    public function favourite_bank_list()
    {

     $data['menuactive'] = $this->uri->segment(2);
     $data['favbankData'] = $this->bank_model->getAllfavourite_banks_list();
     
     $data['staff_permission'] = $this->staff_permission;
     
     $this->permission_access(10);
     if ($this->access_permission) {
        $this->template->set('title', 'Banks');
        $this->template->load('admin_dashboard_layout', 'contents', 'favourite_banks_list', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }


   /*Created by 95 for delete favourite bank*/
   public function delete_favourite_bank($fav_bank_id='')
   {
    $this->bank_model->delete('cp_fav_banks',array('fav_bank_id' =>$fav_bank_id));
    $this->session->set_flashdata('success', 'Deleted successfully.');
    redirect('admin/favourite_banks');
  }

  /*View single favourite bank*/
  public function view_favourite_bank($fav_bank_id='')
    {
     $data['menuactive'] = $this->uri->segment(2);
     $data['viewfavBank']=$this->bank_model->getFavourite_bank($fav_bank_id);
     
     $data['staff_permission'] = $this->staff_permission;
     
     $this->permission_access(10);
     if ($this->access_permission) {
        $this->template->set('title', 'Banks');
        $this->template->load('admin_dashboard_layout', 'contents', 'view_favourite_bank', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }

/*show the current set unit*/
  public function unit_unit()
  {
    
     $data['menuactive'] = $this->uri->segment(2);
     $data['unitData']=$this->bank_model->getAll('place_unit');
     
     $data['staff_permission'] = $this->staff_permission;
     
     $this->permission_access(15);
     if ($this->access_permission) {
        $this->template->set('title', 'Banks');
        $this->template->load('admin_dashboard_layout', 'contents', 'unit_list', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
  }

  /*Update the place unit*/
  public function set_unit()
  {
     $data['menuactive'] = $this->uri->segment(2);

     $data['unitData']=$this->bank_model->getAll('place_unit');

     if($this->input->post('submit')){
        $data=array(
                    'place_unit'=>$this->input->post('unit'),
                    );
        $this->bank_model->updateData1('place_unit',$data);
        redirect('admin/unit_list');
     }
     
     $data['staff_permission'] = $this->staff_permission;
     

     $this->permission_access(10);
     if ($this->access_permission) {
        $this->template->set('title', 'Banks');
        $this->template->load('admin_dashboard_layout', 'contents', 'set_unit', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
  }

  }