<?php defined('BASEPATH') OR exit('No direct script access allowed');

class UserContact extends MX_Controller {

  public function __construct() {

    parent:: __construct();

    $this->load->library('session');
    $this->load->library('form_validation');
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->load->model('contact_model');
		$this->load->model('login_model');
		$this->load->model('common_model');
		$this->load->model('common_model_new');
		$this->load->model('Common_model_new');
    	$this->load->helper(array('common_helper'));
    	$this->load->helper(array('url'));
		$this->load->helper(array('form'));
		$this->load->helper('array');
		not_login();
  }

  /*Created by 95 for show all user's contact*/

  public function contact_list()

  {

    $data['menuactive'] = $this->uri->segment(1);

    $checkLogin = $this->session->userdata('logged_in');

    if(!empty($checkLogin))

    {

      $data['ContactData']  = $this->contact_model->getAllwhereorderby('cp_user_contact',array('contact_user_id'=> $this->session->userdata('logged_in')['id']),'contact_id','desc');

      $this->template->set('title', 'Contact');

      $this->template->load('user_dashboard_layout', 'contents', 'contact_list',$data);

    }else{

     redirect('login');

   }

 }

 /*Created by 95 for delete multiple contacts*/

 public function contact_all_del()

 {

  $bodytype = $this->input->post("bodytype");

  $explode = explode(',',$bodytype);

  foreach($explode as $del){

    $this->contact_model->delete("cp_user_contact",array("contact_id" => $del));

  }

  echo '1';

}

/*Created by 95 for single contact delete*/

public function delete_contact()

{

  $delete_id = $this->uri->segment('2');

  $this->contact_model->delete("cp_user_contact",array("contact_id" => $delete_id));

  $this->session->set_flashdata('success', 'Successfully deleted');

  redirect('contact_list');

}

/*Created by 95 for add contact*/

public function add_contact()

{

  $data['menuactive'] = $this->uri->segment(1);

  $checkLogin = $this->session->userdata('logged_in');

  if(!empty($checkLogin))

  {

    $this->form_validation->set_rules('contact_name', 'Contact Name', 'required');

    $this->form_validation->set_rules('contact_number', 'Contact Number', 'required');

    if ($this->form_validation->run() == TRUE)

    {

      if($_POST['submit']){

            //Check whether user upload picture

        if(!empty($_FILES['picture']['name'])){

          $config['upload_path'] = 'uploads/contact/';

          $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|txt|doc';

          $config['file_name'] = $_FILES['picture']['name'];

                //Load upload library and initialize configuration

          $this->load->library('upload',$config);

          $this->upload->initialize($config);

          if($this->upload->do_upload('picture')){

            $uploadData = $this->upload->data();

            $picture = $uploadData['file_name'];

          }else{

            $picture = '';

          }

        }else{

          $picture = '';

        }

        $data=array(

          'contact_name'=>$this->input->post('contact_name'),

          'contact_mobile_number'=>$this->input->post('contact_number'),

          'contact_user_id'=>$this->session->userdata('logged_in')['id'],

          'user_contact_image'=>$picture

        );

        $insert = $this->contact_model->insertData('cp_user_contact',$data);

        if($insert){

          $this->session->set_flashdata('success','Contact added successfully');

          redirect('contact_list');

        }

      }

    }

    $this->template->set('title', 'Contact');

    $this->template->load('user_dashboard_layout', 'contents', 'add_contact', $data);

  }else{

   redirect('login');

 }

}

public function ajax_contact_add()

{

 $checkLogin = $this->session->userdata('logged_in');

 // if ($_FILES["picture"]["error"] > 0)

 // {

 //   $return = array('code' => 2,'message' => 'File loading error!');

 //}else{

 $config1['upload_path'] = 'uploads/contact/';

 $config1['allowed_types'] = 'jpg|jpeg|png|gif|pdf|txt|doc';

           //$data['max_size'] = '5000';

 $config1['encrypt_name'] = true;

 $this->load->library('upload', $config1);

 if ($this->upload->do_upload('picture')) {

  $attachment_data = array('upload_data' => $this->upload->data());

  $uploadfile = $attachment_data['upload_data']['file_name'];

      // $return = array('code' => 1,'message' => 'Successfully added');

}else{

      //$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));

}

if($uploadfile ){

  $array = array(

   'contact_name'=>$this->input->post('contact_name'),

   'contact_mobile_number'=>$this->input->post('contact_number'),

   'contact_user_id'=>$this->session->userdata('logged_in')['id'],

   'user_contact_image'=>$uploadfile

 );

  $this->contact_model->insertData("cp_user_contact",$array);

  $return = array('code' => 1,'message' => 'Successfully added');

}else{



  $uploadfile='';





  $array = array(

   'contact_name'=>$this->input->post('contact_name'),

   'contact_mobile_number'=>$this->input->post('contact_number'),

   'contact_user_id'=>$this->session->userdata('logged_in')['id'],

   'user_contact_image'=>$uploadfile

 );

  $this->contact_model->insertData("cp_user_contact",$array);

  $return = array('code' => 1,'message' => 'Successfully added');







}

//}

echo json_encode($return);

}

/*Created by 95 for edit contact*/

public function contact_edit($contact_id)

{

  $data['menuactive'] = $this->uri->segment(1);

  $checkLogin = $this->session->userdata('logged_in');

  $data['contact_id'] = $contact_id;

  if(!empty($checkLogin))

  {

    if($_POST['submit']){

      $this->form_validation->set_rules('contact_name', 'Contact Name', 'required');

      $this->form_validation->set_rules('contact_number','Contact Number', 'required');

      if ($this->form_validation->run() == TRUE)

      {

          //Check whether user upload picture

        if(!empty($_FILES['picture']['name'])){

          $config['upload_path'] = 'uploads/contact/';

          $config['allowed_types'] = 'jpg|jpeg|png|gif';

          $config['file_name'] = $_FILES['picture']['name'];

                //Load upload library and initialize configuration

          $this->load->library('upload',$config);

          $this->upload->initialize($config);

          if($this->upload->do_upload('picture')){

            $uploadData = $this->upload->data();

            $picture = $uploadData['file_name'];

          }else{

            $picture = '';

          }

          $data=array(

            'contact_name'=>$this->input->post('contact_name'),

            'contact_mobile_number'=>$this->input->post('contact_number'),

            'contact_user_id'=>$this->session->userdata('logged_in')['id'],

            'user_contact_image'=>$picture

          );

          $this->contact_model->updateData('cp_user_contact',$data,array('contact_id' => $contact_id));

          $this->session->set_flashdata('success','Contact updated successfully');

          redirect('contact_list');

        }else{

          $picture = '';

        }

        $data=array(

          'contact_name'=>$this->input->post('contact_name'),

          'contact_mobile_number'=>$this->input->post('contact_number'),

          'contact_user_id'=>$this->session->userdata('logged_in')['id'],

        );

        $this->contact_model->updateData('cp_user_contact',$data,array('contact_id' => $contact_id));

        $this->session->set_flashdata('success','Contact updated successfully');

        redirect('contact_list');

      }

    }

    $data['ContactData'] = $this->contact_model->getContact($contact_id);

    $this->template->set('title', 'Contact');

    $this->template->load('user_dashboard_layout', 'contents', 'edit_contact',$data);

  }else{

   redirect('login');

 }

}

public function ajax_contact_edit()

{

 $checkLogin = $this->session->userdata('logged_in');

 $contact_id = $this->input->post("contact_id");

 $config1['upload_path'] = 'uploads/contact/';

 $config1['allowed_types'] = 'jpg|jpeg|png|gif';

           //$data['max_size'] = '5000';

 $config1['encrypt_name'] = true;

 $this->load->library('upload', $config1);

 if ($this->upload->do_upload('picture')) {

  $attachment_data = array('upload_data' => $this->upload->data());

  $uploadfile = $attachment_data['upload_data']['file_name'];

      // $return = array('code' => 1,'message' => 'Successfully added');

}else{

      //$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));

}

$ContactData = $this->contact_model->getContact($contact_id);

if($uploadfile){

 $image =  $uploadfile;

}else{

  $image = $ContactData[0]->user_contact_image;

}

$explode = explode(".",$_FILES['picture']['name']);

if($explode[1] == 'jpg' || $explode[1] == 'jpeg'|| $explode[1] == 'png' || $explode[1] == 'gif' || $explode[1] == ''){

 $array = array(

   'contact_name'=>$this->input->post('contact_name'),

   'contact_mobile_number'=>$this->input->post('contact_number'),

   'contact_user_id'=>$this->session->userdata('logged_in')['id'],

   'user_contact_image'=>$image

 );

 $this->contact_model->updateData("cp_user_contact",$array,array("contact_id" => $contact_id));

 $return = array('code' => 1,'message' => 'Successfully updated');

}else{

//$return = array('code' => 2,'message' => 'Successfully updated');

}

echo json_encode($return);

}

public function view_contact($contact_id='')

{

  $data['menuactive'] = $this->uri->segment(1);

  $checkLogin = $this->session->userdata('logged_in');

  if(!empty($checkLogin))

  {

    $data['UserContact']=$this->contact_model->getsingle('cp_user_contact',array('contact_id'=>$contact_id,'contact_user_id'=>$this->session->userdata('logged_in')['id']));

    $this->template->set('title', 'Contacts');

    $this->template->load('user_dashboard_layout_contact', 'contents', 'usercontact_detail',$data);

  }else{

    redirect('login');

  }

}

public function user_profile($cp_users_id=''){

	$data = array();

	$checkLogin = $this->session->userdata('logged_in');
	//_dx($checkLogin);
	$data['menuactive_add'] = $this->uri->segment(3);
	$exting_image = $this->input->post('exting_profile_image');

	/*Time zone call selected*/
	$where="`cp_ct_id`=`cp_ct_id`";
	$zone_data =  $this->common_model_new->getAllwhere('cp_country_timezone',$where);
	$data['zone'] = obj_to_array($zone_data);

	/*start prefresnce array here*/

	$data['snt_txt_msg_alrt'] = array(
		'1'=>'Right Away',
		'2'=>'Evry Two hr',
		'3'=>'Once In day',
		'4'=>'Never'
	);
	$data['snt_eml_alrt'] = array(
		'1'=>'Right Away',
		'2'=>'Evry Two hr',
		'3'=>'Once In day',
		'4'=>'Never'
	);
	$data['call_me_alrt']= array(
		'1'=>'immediately',
		'2'=>'Never'
	);
	$data['snt_daily_eml_daily_rutin_alrt']= array(
		'1'=>'immediately',
		'2'=>'Never'
	);
	$data['snt_eml_no_activity_in_app_alrt']= array(
		'0'=>'Never',
		'2'=>'2 Hours',
		'4'=>'4 Hours',
		'6'=>'6 Hours',
		'8'=>'8 Hours',
		'10'=>'10 Hours',
		'12'=>'12 Hours',
		'24'=>'24 Hours'
	);

	$data['snt_eml_alrm_is_missed']= array(
		'0'=>'Never',
		'1'=>'immediately'
	);
	/*end prefresnce array here*/

	if(isset($_POST['submit']) && $_POST['submit'] !=''){
		
		$checkLogin = $this->session->userdata('logged_in');
		if(!empty($checkLogin)){
			$current_user_id = $checkLogin['id'];
		}

		$name = $this->input->post('name');
		$lastname = $this->input->post('lastname');
		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$mobile = $this->input->post('mobile');
		$street_name = $this->input->post('street_name');
		$city = $this->input->post('city');
		$state = $this->input->post('state');
		$country = $this->input->post('country');
		$language = $this->input->post('language');
		$zipcode = $this->input->post('zipcode');
		$password = $this->input->post('password');
		$password =	trim($password);
		//Insert array for userdetail
		$data = array(
			'cp_users_id'  	=> $current_user_id,
			'name'			=> trim($name),
			'lastname' 		=> trim($lastname),
			'username' 		=> trim($username),
			'email' 		=> trim($email),
			'mobile' 		=> trim($mobile),
			'street_name'	=> trim($street_name),
			'city' 			=> trim($city),
			'state' 		=> trim($state),
			'country' 		=> trim($country),
			'zipcode' 		=> trim($zipcode),
			'language' 		=> trim($language),
			'gender' 		=> $this->input->post('gender'),
			'timezone' 		=> $this->input->post('timezone'),
			'password' 		=> md5($password),
			'user_role' 	=>4,
			'create_user' 	=> date('Y-m-d H:i:s'),
			'status' 		=>1,
			'snt_txt_msg_alrt'	=> $this->input->post('snt_txt_msg_alrt'),
			'snt_eml_alrt'			=> $this->input->post('snt_eml_alrt'),
			'snt_daily_eml_daily_rutin_alrt' => $this->input->post('snt_daily_eml_daily_rutin_alrt'),
			'snt_eml_no_activity_in_app_alrt'=> $this->input->post('snt_eml_no_activity_in_app_alrt'),
			'snt_eml_alrm_is_missed'	=> $this->input->post('snt_eml_alrm_is_missed'),
		);


		if(!empty($_FILES['profile_image']['name']) && $_FILES['profile_image']['name'] !=''){
			
			$config['file_name']     = time().$_FILES['profile_image']['name'];
			$config['upload_path']   = 'uploads/profile_images/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['max_size']      = '10000';
			$config['max_width']     = '0';
			$config['max_height']    = '0';
			$config['remove_spaces'] = true;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if (!$this->upload->do_upload('profile_image')) {
				$error = array('error' => $this->upload->display_errors());
			}else{
				
				$img = array('upload_data' => $this->upload->data());
				$data['profile_image'] = $img['upload_data']['file_name'];
			}
		}
		
		//_dx($data);

		/*New user insert here*/
		$insertId = $this->login_model->insertrecords('cp_users', $data);
		/*Start add user setting btn*/
		if(!empty($insertId) && $insertId !=''){

			/*Start add user setting btn*/
			$setting_btn_arr = setting_btn_arr($insertId);
						
			if(!empty($setting_btn_arr) && $setting_btn_arr !=''){
				$insert_activities_record = $this->db->insert_batch('setting_butns', $setting_btn_arr);
			}
			/*End add user setting btn*/

			$base_url = base_url();
			$css_path = $base_url .'assets/styles/caregivermail.css';
			$img_path = base_url() . 'assets/images/';
			/*start email sent code*/
			$subject ="Caregiver Profile Registration";
			$message ="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
			<html xmlns='http://www.w3.org/1999/xhtml'>
				<head>
					<link href='https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i' rel='stylesheet' />
					<link rel='stylesheet' type='text/css' href='https://www.mysoultab.com/assets/styles/caregivermail.css'>
					<title>Caregiver</title>
					<style>
					section {
						width: 40%;
						min-height: 200px;
						height: 200px;
						color: black;
						
						padding: 5%;
						padding-left: 0px;
						float: left;
						background-color: white;
				  }
				  section.two {
						width: 40%;
						min-height: 200px;
						height: 200px;
						color: black;
						
						padding: 5%;
						
						float: left;
						background-color: white;
				  }
				  section.one1{
					  width: 20%;
						min-height: 100px;
						height: 100px;
						color: black;
						
						padding: 5%;
						padding-left:0px;
						float: left;
						background-color: white;
				  }
				  section.two2{
					  width: 60%;
						min-height: 100px;
						height: 100px;
						color: black;
						
						padding: 5%;
						padding-left: 0px;
						float: left;
						background-color: white;
				  }
				  .bottom-line {
			
						border-bottom: 1px solid #000;
						padding-bottom: 20px;
				  }
				  .body {
		  
					  padding:0 !important;
					  margin:0 !important; 
					  display:block !important; 
					  min-width:100% !important; 
					  width:100% !important; 
					  background:#f4f4f4; 
					  -webkit-text-size-adjust:none;
				  }
		  
				  .td.container {
					  width:650px; 
					  min-width:650px; 
					  font-size:0pt; 
					  line-height:0pt; 
					  margin:0; 
					  font-weight:normal; 
					  padding:55px 0px;
				  }
		  
				  td.sec1{
					  padding-bottom: 20px;
				  }
		  
				  td.p30-15 {
					  padding: 25px 30px 25px 30px; 
					  background: #fff;
				  }
				  th.column-top {
					  font-size:0pt; 
					  line-height:0pt; 
					  padding:0; 
					  margin:0; 
					  font-weight:normal; 
					  vertical-align:top;
				  }
				  td.img.m-center {
					  font-size:0pt; 
					  line-height:0pt; 
					  text-align:left;
				  }
				  img.img.m-center {
					  padding-left: 230px;
				  }
				  td.h3.pb20{
					  color:#114490; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:24px; 
					  line-height:32px; 
					  text-align:center; 
					  padding-bottom:0px;
					  padding-top: 10px
				  }
				  td.text.pb20{
					  color:#000; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:14px; 
					  line-height:26px; 
					  text-align:left; 
					  padding-bottom:10px;
					  font-weight: 700;
				  }
				  td.text.pb201{
					  color:#777777; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:14px; 
					  line-height:26px; 
					  text-align:left; 
					  padding-bottom:10px;
				  }
				  td.text.pb202{
					  color:#000; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:24px; 
					  line-height:26px; 
					  text-align:left; 
					  padding-bottom:10px;
				  }
				  td.text1.pb201{
					  color:#000; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:14px; 
					  line-height:26px; 
					  text-align:center; 
					  padding-top: 5px; 
					  font-weight: 600;
				  }
				  td.text1.pb2012{
					  color:#000; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:14px; 
					  line-height:26px; 
					  text-align:center; 
					  padding-top: 5px; 
					  font-weight: 600;
					  padding-bottom: 15px;
				  }
				  td.p30-155{
					  padding: 5px 5px ;
				  }
				  td.sec2{
					  border-collapse: collapse; 
					  border-spacing: 0;
		  
				  }
				  img.sec2{
					  padding: 0px; 
					  margin: 0; 
					  outline: none; 
					  text-decoration: none; 
					  -ms-interpolation-mode: bicubic; 
					  border: 0px solid orange; 
					  border-radius:8px; 
					  display: block;
					  float:left;
					  color: #000000;
				  }
				  td.p30-151{
					  padding: 70px 0px;
				  }
				  td.text-footer1.pb10{
					  color:#999999; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:16px; 
					  line-height:20px; 
					  text-align:left; 
					  padding-bottom:0px;
				  }
				  td.text-footer1.pb20{
					  color:#999999; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:16px; 
					  line-height:20px; 
					  text-align:left; 
					  padding-bottom:10px;
				  }
				  td.p30-152{
					  padding: 0px 0px;
				  }
				  td.sec3{
					  padding-bottom: 30px;
				  }
				  td.text1.pb20{
					  color:#000; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:24px; 
					  line-height:26px; 
					  text-align:left;
				  }
				  td.sec4{
					  font-size:0pt; 
					  line-height:0pt; 
					  text-align:left;
				  }
					</style>
				</head>
   
				<body class='body'>
					<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#f4f4f4'>
						<tr>
							<td align='center' valign='top'>
							<table width='650' border='0' cellspacing='0' cellpadding='0' class='mobile-shell'>
								<tr>
									<td class='td container'>
										<!-- Header -->
										<table width='100%' border='0' cellspacing='0' cellpadding='0'>
										<tr>
											<td class='sec1'>
												<table width='100%' border='0' cellspacing='0' cellpadding='0'>
													<tr>
													<td class='p30-15'>
														<table width='100%' border='0' cellspacing='0' cellpadding='0'>
															<tr>
																<th class='column-top' width='145'>
																<table width='100%' border='0' cellspacing='0' cellpadding='0 ' class='bottom-line'>
																	<tr>
																		<td class='img m-center'><img src='https://www.mysoultab.com/assets/images/logo.png' width='100' height='50' border='0' alt='' class='img m-center'/></td>
																	</tr>
																</table>
																</th>
															</tr>
														</table>
														<table width='100%' border='0' cellspacing='0' cellpadding='0'>
															<tr>
																<td class='h3 pb20'>Welcome to SoulTab</td>
															</tr>
															<tr>
																<td class='text pb20'>Hello,
																</td>
															</tr>
															<tr>
																<td class='text pb201'>We wanted to let you know that we have received your profile information.To view your profile : <a href='#'>mysoultab.com</a>
																</td>
															</tr>
															<tr>
																<td class='text pb202'>What's next? 
																</td>
															</tr>
															<tr>
																<td class='text pb201'>Thankyou for filling out your information.Please know that your information is safe and secure with us.
																</td>
															</tr>
															<tr>
																<td class='text pb201'>Here is your caregiver information:</a>
																</td>
															</tr>
															<tr>
																<td class='text1 pb201'>User Name:  $username
																</td>
															</tr>
															<tr>
																<td class='text1 pb201'>User Password:  $password
																</td>
															</tr>
															<tr>
																<td class='text1 pb2012'>User Email: $email
																</td>
															</tr>
															<tr>
																<td class='text pb201'>To sign into your account,please visit <a href='#'>www.mysoultab.com/login</a>
																</td>
															</tr>
															<tr>
																<td class='text pb201'>Call us at 847-450-1055 if you need any help.
																</td>
															</tr>
														</table>
														<section class='one1'>
															<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																<tr>
																<td class='p30-155' bgcolor='#ffffff'>
																	<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																		<tr>
																			<td align='center' valign='top' class='sec2' style=''><img border='0' vspace='0' hspace='0' class='sec2' src='https://www.mysoultab.com/assets/images/team_two.jpg'alt='D' width='100%'>
																			</td>
																		</tr>
																	</table>
																</td>
																</tr>
															</table>
														</section>
														<section class='two2'>
															<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																<tr>
																<td class='p30-151' bgcolor='#ffffff'>
																	<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																		<tr>
																			<td class='text-footer1 pb10'>Best Regards</td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb10'>ABC</td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20'>Position</td>
																		</tr>
																	</table>
																</td>
																</tr>
															</table>
														</section>
														<!-- END Article / Title + Copy + Button -->
														<!-- Footer -->
														<section class='one'>
															<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																<tr>
																<td class='p30-152' bgcolor='#ffffff'>
																	<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																		<tr>
																			<td align='left' class='sec3'>
																			<table>
																				<tr>
																					<td class='text1 pb20'>Technical Support</td>
																				</tr>
																			</table>
																			</td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20'>Mail us at:</td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20' >info@mysoultab.com</td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20'>Web :<a href='#'>mysoultab.com</a></td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20'>Tel no : 847 4501055</td>
																		</tr>
																	</table>
																</td>
																</tr>
															</table>
														</section>
														<section class='two'>
															<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																<tr>
																<td class='p30-152' bgcolor='#ffffff'>
																	<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																		<tr>
																			<td align='left' class='sec3'>
																			<table>
																				<tr>
																					<td class='text1 pb20'>Information</td>
																				</tr>
																			</table>
																			</td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20'>Contact us</td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20'>About us </td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20'>Privacy policy</td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20'>Terms of Use</td>
																		</tr>
																	</table>
																</td>
																</tr>
															</table>
														</section>
														<table width='100%' border='0' cellspacing='0' cellpadding='0'>
															<tr>
																<td class='p30-152'  bgcolor='#ffffff'>
																<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																	<tr>
																		<td align='center'>
																			<table border='0' cellspacing='0' cellpadding='0'>
																			<tr>
																				<td class='img' width='55' class='sec4'><a href='#' target='_blank'><img src='https://www.mysoultab.com/assets/images/t8_ico_facebook.jpg' width='38' height='38' border='0' alt='' /></a></td>
																				<td class='img' width='55' class='sec4'><a href='#' target='_blank'><img src='https://www.mysoultab.com/assets/images/t8_ico_twitter.jpg' width='38' height='38' border='0' alt='' /></a></td>
																				<td class='img' width='55' class='sec4'><a href='#' target='_blank'><img src='https://www.mysoultab.com/assets/images/t8_ico_instagram.jpg' width='38' height='38' border='0' alt='' /></a></td>
																				<td class='img' width='38' class='sec4'><a href='#' target='_blank'><img src='https://www.mysoultab.com/assets/images/t8_ico_linkedin.jpg' width='38' height='38' border='0' alt='' /></a></td>
																			</tr>
																			</table>
																		</td>
																	</tr>
																</table>
																</td>
															</tr>
														</table>
													</td>
													</tr>
												</table>
											</td>
										</tr>
										</table>
										<!-- END Header -->
										<!-- Intro -->
										<!-- END Intro -->
										<!-- Article / Title + Copy + Button -->
										<!---end footer--->
									</td>
								</tr>
							</table>
							</td>
						</tr>
					</table>
				</body>
				</html>";
			/*end email sent code*/

			//$chk_mail2 = new_send_mail($message, $subject,$email,$imageArray='');
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

			// Additional headers
			//$headers .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
			$headers .= 'From: MySoultab <no-reply@example.com>' . "\r\n";
			// Mail it
			mail($email, $subject, $message, $headers);
			
		}
//echo $insertId;exit();
		if (!empty($insertId) || $insertId  !='') {
			$messge = "User has been added successfully";
			$this->session->set_flashdata('success_msg', $messge);
			redirect('user_profile');
		}
	}

	if(empty($cp_users_id) && $cp_users_id ==''){
		$data['users_data'] ='';
		$this->template->set('title', 'User Profile');
		$this->template->load('new_user_dashboard_layout', 'contents' , 'user_profile', $data);	
	}else{
		$checkLogin = $this->session->userdata('logged_in');
		if(!empty($checkLogin)){
			//$current_user_id = $checkLogin['id'];
			$current_user_id = $checkLogin['cp_users_id'];
		}

		//_d($current_user_id);

		$data['users_data'] = $this->login_model->getAllRecordsById('cp_users', array('cp_users_id' => $current_user_id));
		
		//_dx($data['users_data']);
		$this->template->set('title', 'User Profile');
		$this->template->load('new_user_dashboard_layout', 'contents' , 'user_profile', $data);	
	}
	
	
}

public function user_profile_edit($user_id=''){

	/*Time zone call selected*/
	$where="`cp_ct_id`=`cp_ct_id`";
	$zone_data =  $this->common_model_new->getAllwhere('cp_country_timezone',$where);
	$data['zone'] = obj_to_array($zone_data);

	/*start prefresnce array here*/

	$data['snt_txt_msg_alrt'] = array(
		'1'=>'Right Away',
		'2'=>'Evry Two hr',
		'3'=>'Once In day',
		'4'=>'Never'
	);
	$data['snt_eml_alrt'] = array(
		'1'=>'Right Away',
		'2'=>'Evry Two hr',
		'3'=>'Once In day',
		'4'=>'Never'
	);
	$data['call_me_alrt']= array(
		'1'=>'immediately',
		'2'=>'Never'
	);
	$data['snt_daily_eml_daily_rutin_alrt']= array(
		'1'=>'immediately',
		'2'=>'Never'
	);
	$data['snt_eml_no_activity_in_app_alrt']= array(
		'0'=>'Never',
		'2'=>'2 Hours',
		'4'=>'4 Hours',
		'6'=>'6 Hours',
		'8'=>'8 Hours',
		'10'=>'10 Hours',
		'12'=>'12 Hours',
		'24'=>'24 Hours'
	);

	$data['snt_eml_alrm_is_missed']= array(
		'0'=>'Never',
		'1'=>'immediately'
	);
	/*end prefresnce array here*/

		if(!empty($user_id) && $user_id !=''){
				
			if(isset($_POST['update']) && $_POST['update'] !=''){
					
					$name = $this->input->post('name');
					$lastname = $this->input->post('lastname');
					$mobile = $this->input->post('mobile');
					$street_name = $this->input->post('street_name');
					$city = $this->input->post('city');
					$state = $this->input->post('state');
					$country = $this->input->post('country');
					$language = $this->input->post('language');
					$password = $this->input->post('password');
					
					//Update array for userdetail
					$data = array(
						'name' 		  		=> trim($name),
						'lastname' 			=>  trim($lastname),
						//'email' 			=> $this->input->post('email'),
						'mobile' 			=>  trim($mobile),
						'street_name'		=>  trim($street_name),
						'city' 				=>  trim($city),
						'state' 			=>  trim($state),
						'country' 			=>  trim($country),
						'language' 			=>  trim($language),
						'gender' 			=> $this->input->post('gender'),
						'timezone'			=> $this->input->post('timezone'),
						'snt_txt_msg_alrt' 	=> $this->input->post('snt_txt_msg_alrt'),
						'snt_eml_alrt' 		=> $this->input->post('snt_eml_alrt'),
						'snt_daily_eml_daily_rutin_alrt'	=> $this->input->post('snt_daily_eml_daily_rutin_alrt'),
						'snt_eml_no_activity_in_app_alrt'	=> $this->input->post('snt_eml_no_activity_in_app_alrt'),
						'snt_eml_alrm_is_missed'	=> $this->input->post('snt_eml_alrm_is_missed'),
					);
				
				if(!empty($password) ||  $password !=''){
					$password =	trim($password);
					$data['password'] = md5($password);
				}
				
				if(!empty($_FILES['profile_image']['name']) && $_FILES['profile_image']['name'] !=''){
					
					$config['file_name']     = $_FILES['profile_image']['name'];
					$config['upload_path']   = 'uploads/profile_images/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['max_size']      = '10000';
					$config['max_width']     = '0';
					$config['max_height']    = '0';
					$config['remove_spaces'] = true;
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					
					if (!$this->upload->do_upload('profile_image')) {
						$error = array('error' => $this->upload->display_errors());
						print_r($error);
						
					}else{
						
						if(!empty($exting_image) && $exting_image !=''){
							unlink('./uploads/profile_images/'.$exting_image);
						}
						
						$img = array('upload_data' => $this->upload->data());
						$data['profile_image'] = $img['upload_data']['file_name'];
					}
				}
				
				$user_id = isset($user_id) ? $user_id : '';

				$current_user_id = $user_id;
				$where_condition = ['id' => $current_user_id];

				//_dx($data);
				$update = $this->login_model->updateRecords('cp_users',$data,$where_condition);
				
				if (!empty($update) || $update  !='') {
					$messge = "User has been update successfully";
					$this->session->set_flashdata('success_msg', $messge);
					redirect('user_profile');
				}
			}
		}

		$data['users_data'] = $this->common_model_new->getsingle("cp_users", array("id" => $user_id));
		$data = obj_to_array($data);
		//_dx($data);
		$this->template->set('title', 'User edit profile');
		$this->template->load('new_user_dashboard_layout', 'contents' , 'user_profile_edit', $data);	
		
	}

	public function get_all_userprofile_list(){

		$checkLogin = $this->session->userdata('logged_in');
		if(!empty($checkLogin) && $checkLogin !=''){

			if(!empty($checkLogin)){
				$current_user_id = $checkLogin['id'];
				//_dx($current_user_id);
				//$current_user_id = 878;
			}

			if(!empty($current_user_id) && $current_user_id !=''){
				$data['userprofile_list']  = $this->common_model_new->getallwhere_group_by_orderby('cp_users',array('cp_users_id'=> $current_user_id),'id','id','desc');
			}
				
			if(!empty($data['userprofile_list']) && $data['userprofile_list'] !=''){
				//_dx($data['userprofile_list']);
				$this->template->set('title', 'User Profile list');
				$this->template->load('new_user_dashboard_layout', 'contents' , 'user_profile_list', $data);	
			}else{
				$data ='';
				$this->template->set('title', 'User Profile list');
				$this->template->load('new_user_dashboard_layout', 'contents' , 'user_profile_list', $data);	
			}
		}else{
			redirect('login');
		}
	}

	public function delete_user()
	{
		$data['menuactive'] = $this->uri->segment(1);
		$segment = $this->uri->segment("2");

		if(!empty($segment) && $segment !=''){

			$result =$this->Common_model_new->delete("cp_users",array("id" => $segment));
			if(!empty($result) && $result !=''){
				$messge = "User has been deleted successfully";
				$this->session->set_flashdata('success_msg', $messge);
				redirect('user_profile');
			}
		}
		
	}

	public function validateData() {

		if ($this->input->post("email") != '') {
		  $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[cp_users.email]');
		  if ($this->form_validation->run() == true) {
			die('true');
		  } else {
			die('false');
		  }
		}
	  
		if ($this->input->post("mobile") != '') {
		  $this->form_validation->set_rules('mobile', 'Mobile', 'required|is_unique[cp_users.mobile]');
		  if ($this->form_validation->run() == true) {
			die('true');
		  } else {
			die('false');
		  }
		}
	  
		if ($this->input->post("username") != '') {
		  $this->form_validation->set_rules('username', 'Username', 'required|is_unique[cp_users.username]');
		  if ($this->form_validation->run() == true) {
			die('true');
		  } else {
			die('false');
		  }
		}
	  
	  }

	/**
	 * send Notification daily of daily routine
	 */
	public function sendNotificationDailyRoutine() {
		$users = $this->common_model->getRecordOfSelewctedColumn('cp_users', 'id' , array( "user_role" => 2));
		$usersList = array_unique(array_column($users, 'id'));
		foreach ($usersList as $user) {
			$caregiverList = $this->common_model->getRecordOfSelewctedColumn('cp_users', 'id,email,snt_daily_eml_daily_rutin_alrt' , array( "cp_users_id" => $user,'snt_daily_eml_daily_rutin_alrt' => 1));
			$check_dailyroutine = $this->Common_model_new->getsingle("cp_dailyroutine",array("created" => date('Y-m-d'),"user_id" => $user));
			if(!empty($caregiverList) && !empty($check_dailyroutine)){
			foreach ($caregiverList as $caregiver){
				$message = $this->load->view('/email/dailyroutinemail',array('check_dailyroutine'=>$check_dailyroutine), TRUE);
				// To send HTML mail, the Content-type header must be set
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

				// Additional headers
				//$headers .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
				$headers .= 'From: MySoultab <no-reply@example.com>' . "\r\n";
				$to = $caregiver['email'];

				$subject = 'Dailyroutine Report';
				// Mail it
				mail($to, $subject, $message, $headers);
			}
			}

		}

	}

	/**
	 * sent user no activity mail
	 * @param minutes
	 * @return bool
	 */
	public function sentEmailNoActivity()
	{
		$interval =  $product_id = $this->uri->segment(2);
		$usersList = $this->common_model->getRecordOfSelewctedColumn('cp_users', 'id,email', array("user_role" => 2));
		//$usersList = array_unique(array_column($users, 'id'));
		if (!empty($usersList)) {
			foreach ($usersList as $users) {
				$caregiverList = $this->common_model->getRecordOfSelewctedColumn('cp_users', 'id,email,snt_daily_eml_daily_rutin_alrt', array("cp_users_id" => $users, 'snt_daily_eml_daily_rutin_alrt' => 1));
				foreach ($caregiverList as $user) {
				try {
					$device_info = $this->common_model->getSingleRecordById('device_details', array('device_user_id' => $user['id']));
					$loginTime = isset($device_info['device_last_online']) ? $device_info['device_last_online'] : '';
					$notifyTime = isset($device_info['notify_time']) ? $device_info['notify_time'] : '';
					if ($loginTime != '') {
						$startdateObj = new \DateTime($loginTime);
						$currentTime = new \DateTime(date('Y-m-d H:i:s'));
						if ($notifyTime != '') {
							$enddateObj = new \DateTime($notifyTime);
						} else {
							$enddateObj = $currentTime;
						}

						$diff = $startdateObj->diff($enddateObj);
						$duration = ($diff->days * 24 * 60 * 60) + ($diff->h * 60) + $diff->i;
//						echo $duration;
//						echo 'interval'.$interval;exit();

						if ($duration > $interval) {
							$message = $this->load->view('/email/noactivity', array('hours' => round($interval / 60)), TRUE);
							// To send HTML mail, the Content-type header must be set
							$headers = 'MIME-Version: 1.0' . "\r\n";
							$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

							// Additional headers
							//$headers .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
							$headers .= 'From: MySoultab <no-reply@example.com>' . "\r\n";
							$to = $user['email'];

							$subject = 'No Activity in App';
							// Mail it
							mail($to, $subject, $message, $headers);

							$where_condition = array('id' => $user['id']);
							$upd = $this->common_model->updateRecords('cp_users', array('notify_time' => $currentTime), $where_condition);

						}
					}
				} catch (Exception $ex) {
					echo $ex->getMessage();
				}
			}
			}
		}
		return true;
	}


}
?>
