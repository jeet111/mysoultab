<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Payments extends MX_Controller {   



  public function __construct() {





    parent:: __construct();

    //$this->load->model("Payment");

    //$this->load->model("login_model");

    //$this->load->library("pagination");

    $this->load->model('common_model');

    $this->load->model('Common_model_new');

    //$this->load->library('form_validation','session');

    $this->load->helper(array('common_helper'));

    $this->load->helper(array('url'));

    $this->load->helper(array('form'));

    $this->load->library("braintree_lib");

    $this->config->load('braintree');

		$this->load->helper('array');
    //$this->no_cache();

  }





    /*

    * Payment view

    */

    public function ViewPayment(){



      //die('this is die');



      $data = array();

      $data['menuactive'] = $this->uri->segment(1);



      $getparam = $this->uri->segment(2);

      $subsplan_id = base64_decode($getparam);  





      $user = $this->uri->segment(3);

      $user_id = base64_decode($user);



      $data['users']=array('user_id'=>$user_id);





      $data['subsplanDetails'] = $this->common_model->getSingleRecordById('subscription_plan',array('id'=> $subsplan_id));



        /*$data['user_data'] = $this->login_model->getRows('gb_users',array('id'=>$this->session_uid));



        $con['conditions'] = array(

            'payment_info_user_id'=>$this->session_uid,

        );

        $data['payment_methods'] = $this->login_model->getRows('gb_payment_info', $con);*/



        $this->template->set('title', 'View Payment');

        $this->template->load('home_layout', 'contents' , 'payment_view', $data);

      }



    /*

    * Payment Process

    */

    public function ProcessPayment() {

      $data = array();

      //echo "<pre>"; print_r($_POST);die();

      if(isset($_POST['payment_method_nonce'])) {



        $sale = array(

          'amount'   => $_POST['order_total'],

          'orderId'  => $_POST['order_id'],

                'paymentMethodNonce' => $_POST['payment_method_nonce'],   // Autogenerated field from braintree

                'options' => array(

                  'submitForSettlement'   => true,

                    //'storeInVaultOnSuccess' => true

                )

              );



        $result = Braintree_Transaction::sale($sale);





        // echo "<pre>";

        // print_r($result);

        // die;







        $user_id =  $_POST['user_id'];

        $order_id = $_POST['order_id'];



        if ($result->success) {







          $PlanDetail = $this->common_model->getSingleRecordById('subscription_plan',array('id'=> $_POST['order_id']));



          





            // PHP program to add 10 days in date 

          $startDate = date('Y-m-d');

          // Declare a date 

          $date = date_create($startDate); 

          $number=$PlanDetail['plan_days'];

          $days=$number.'days';

          // Use date_add() function to add date object 

          date_add($date, date_interval_create_from_date_string($days));       



            // Display the added date 

          $endtDate = date_format($date, "Y-m-d");   



          //get user detail from teprory table
          $TempUserDetail = $this->common_model->getSingleRecordById('cp_users_temprory',array('id'=> $user_id));

					$data = array(

            'username' =>$TempUserDetail['username'],
            //'ip_address'=>$ip_addr,  
            'name' => $TempUserDetail['name'],
            'lastname' => $TempUserDetail['lastname'],
            'city'=>$TempUserDetail['city'],
            'state'=>$TempUserDetail['state'],
            'zipcode'=>$TempUserDetail['zipcode'],
            'country_code'=>'',
            'company_school'=>'',
            'apartment'=>$TempUserDetail['apartment'],
            'dob' => '',
            'street_name'=>$TempUserDetail['street_name'],
            //'dob' => (!empty($dob) ? date('Y-m-d',strtotime($dob)) : null),
            'email' =>  $TempUserDetail['email'],
            'mobile' => $TempUserDetail['mobile'],
            'password' => md5($TempUserDetail['pass']),
            'user_role' => 2,
            'create_user' => date('Y-m-d H:i:s'),
            'status' => $TempUserDetail['status'],
            'otp' => $TempUserDetail['otp'],
            'gender' => '',
            //'gender' => (!empty($gender) ? $gender : 1),
            'address' => '',
            //'user_id' => '',
            'user_id' => (!empty($TempUserDetail['user_id']) ? $TempUserDetail['user_id'] : null)
          );

					//$insertId = $this->common_model->addRecords('cp_users_temprory', $data);

          $insertId = $this->common_model->addRecords('cp_users', $data);

					/*Start add user setting btn*/
					if(!empty($insertId) && $insertId !=''){
						
						$setting_btn_arr = setting_btn_arr($insertId);
								
						if(!empty($setting_btn_arr) && $setting_btn_arr !=''){
							$insert_activities_record = $this->db->insert_batch('setting_butns', $setting_btn_arr);
						}
					}
					/*End add user setting btn*/

          //End

          $user_id= $insertId;
          $tempId = $TempUserDetail['id'];

          $this->Common_model_new->delete("cp_users_temprory",array("id" => $tempId));

          $trnsactData = array(
            //'user_id' => $insertId,

            'user_id' =>$user_id,

            'plan_id' =>$result->transaction->orderId,

            'txn_id'   =>  $result->transaction->id,

            'txn_date' => $startDate, 

            'amount'  =>$result->transaction->amount,

            'cc'     =>$result->transaction->currencyIsoCode,

            'startDate'=>$startDate,

            'endDate'=>$endtDate,

            'itemname' =>    '',

            'qauntity'   =>  '',

            'payment_method'=>  $result->transaction->paymentInstrumentType,  

            'payment_status'  => 'success',

            'created_date'=>date('d-m-y h:i:s'),

          );



          $insertId = $this->common_model->addRecords('subscription_payment_transactions', $trnsactData);





          $UserDetail = $this->common_model->getSingleRecordById('cp_users',array('id'=> $user_id));



          // echo "<pre>";

          // print_r($UserDetail);

          // die;



          



          

          $data['email'] = $UserDetail['email'];

          

          $firstname = $UserDetail['name'];

          $lastname = $UserDetail['lastname'];



          $email = $UserDetail['email'];

          $mobile = $UserDetail['mobile'];

          $user_id = $UserDetail['user_id'];

          $dob = $UserDetail['dob'];

          $address = $UserDetail['address'];

          $gender = $UserDetail['gender'];



          

          //User email start

          



          $email = $data['email'];

          $name = $firstname.' '.$lastname;

          $TemplateData = $this->common_model->getSingleRecordById('mail_template',array('template_id'=> 3));





          $findArray=array("%name%","%email%");

          $replaaceArray=array($name,$email);



          $description= str_replace($findArray,$replaaceArray,$TemplateData['description']);

          $message = $description;



          //$message = 'Dear '.$firstname.' '.$lastname.',<br><p>Welcome to our carepro app.</p><br><p>Please open tablet and please do further process and enjoy our services.</p><br><p>Best Wishes,</p><br><p>CarePro Team</p>';

          $subject = $TemplateData['subject'];



          $check = new_send_mail($message, $subject, $email, '');



          

          // User email end



          // Admin mail start

          

          $message = '<html>

          <head>

          <title></title>

          </head>

          <body>

          <h2>Dear Admin,</h2>

          <p>Please see user information.</p>

          <table>

          <thead>

          <tr>

          <th>User Name</th>

          <th>Email</th>

          <th>Contact No</th>

          <th>User ID</th>

          <th>Gender</th>

          <th>Date Of Birth</th>

          <th>Address</th>

          </tr>

          </thead>

          <tbody>

          <tr>

          <td>'.$firstname.' '.$lastname.'</td>

          <td>'.$email.'</td>

          <td>'.$mobile.'</td>

          <td>'.$user_id.'</td>

          <td>'.($gender == 1 ? 'Male' : 'Female').'</td>

          <td>'.(!empty($dob) ? date('Y-m-d',strtotime($dob)) : null).'</td>

          <td>'.$address.'</td>

          </tr>

          </tbody>

          </table>

          </body>

          </html>';

          $subject = "CarePro - New Registration";

          $email = "info@carepro.com";

          $check = new_send_mail($message, $subject, $email, '');



          



          // Admin mail end



          // email end

          $orderId = base64_encode($insertId);

          redirect('success/'.$orderId);

        } else {









         //$tempodid = base64_encode($order_id);

         $tempID = base64_encode($user_id);

         redirect('failed/'.$tempID);

          ///echo "Sorry somthing went wrong";



       }

     }

     else { 



        //echo "Redirect";

      redirect('pricing');

        //redirect('my-order-view/'.base64_encode($_POST['order_id']));

                     // print_r($messge);



                    //  print_r($result);

                    //die;

    }



            //echo "<pre>";print_r($result); exit;

  } 





  public function paymentsuccess()

  {



    $getparam = $this->uri->segment(2);

    $order_id = base64_decode($getparam);

    $TransDetail = $this->common_model->getSingleRecordById('subscription_payment_transactions',array('payment_transaction_id'=> $order_id));



    $PlanDetail = $this->common_model->getSingleRecordById('subscription_plan',array('id'=> $TransDetail['plan_id']));















    $UserDetail = $this->common_model->getSingleRecordById('cp_users',array('id'=> $TransDetail['user_id']));



    $TemplateData = $this->common_model->getSingleRecordById('mail_template',array('template_id'=> 5));



            // Subscription email start

    $name= $UserDetail['name'];

    $plan_name=$PlanDetail['plan_name'];

    $desc= $PlanDetail['description'];

    $amount=$PlanDetail['amount'];

    $plan_days=$PlanDetail['plan_days'];





    $findArray=array("%name%","%planname%","%description%","%amount%","%plan_days%");

    $replaaceArray=array($name,$plan_name,$desc,$amount,$plan_days);



    $description= str_replace($findArray,$replaaceArray,$TemplateData['description']);





    $message =  $description;



      // $message = '<html>

      // <head>

      // <title></title>

      // </head>

      // <body>

      // <h2>Dear '.$UserDetail['name'].'</h2>

      // <p>Transaction is successfull. Below is the plan detail.</p>

      // <table>

      // <thead>

      // <tr>

      // <th>Plan Name</th>

      // <th>Description</th>

      // <th>Amount</th>

      // <th>Days</th>



      // </tr>

      // </thead>

      // <tbody>

      // <tr>

      // <td>'.$PlanDetail['plan_name'].'</td>

      // <td>'.$PlanDetail['description'].'</td>

      // <td>'.$PlanDetail['amount'].'</td>

      // <td>'.$PlanDetail['plan_days'].'</td>

      // </tr>

      // </tbody>

      // </table>

      // </body>

      // </html>';

    $subject = $TemplateData['subject'];

    $email = $UserDetail['email'];

      //$email = "info@carepro.com";

    $check = new_send_mail($message, $subject, $email, '');





          // Subscription email end





    $TemplateData = $this->common_model->getSingleRecordById('mail_template',array('template_id'=> 4));





    $plan_name= $PlanDetail['plan_name'];

    $txn_id= $TransDetail['txn_id'];

    $amount= $TransDetail['amount'];

    $plan_days= $PlanDetail['plan_days'];



    $findArray=array("%name%","%planname%","%txn_id%","%amount%","%plan_days%");

    $replaaceArray=array($name,$plan_name,$txn_id,$amount,$plan_days);



    $description= str_replace($findArray,$replaaceArray,$TemplateData['description']);







          // Payment email start

    $message =  $description;

      // $message = '<html>

      // <head>

      // <title></title>

      // </head>

      // <body>

      // <h2>Dear '.$UserDetail['name'].'</h2>

      // <p>Payment confirmation.</p>

      // <table>

      // <thead>

      // <tr>

      // <th>Plan Name</th>

      // <th>Transaction Id</th>

      // <th>Amount</th>

      // <th>Days</th>



      // </tr>

      // </thead>

      // <tbody>

      // <tr>

      // <td>'.$PlanDetail['plan_name'].'</td>

      // <td>'.$TransDetail['txn_id'].'</td>

      // <td>'.$TransDetail['amount'].'</td>

      // <td>'.$PlanDetail['plan_days'].'</td>

      // </tr>

      // </tbody>

      // </table>

      // </body>

      // </html>';

    $subject = $TemplateData['subject'];

      //$subject = "Transaction details.";

    $email = $UserDetail['email'];

      //$email = "info@carepro.com";

    $check = new_send_mail($message, $subject, $email, '');





          // Payment email end



      // Invoice mail start

    $TemplateData = $this->common_model->getSingleRecordById('mail_template',array('template_id'=> 1));





    $plan_name= $PlanDetail['plan_name'];

    $txn_id= $TransDetail['txn_id'];

    $amount= $TransDetail['amount'];

    $plan_days= $PlanDetail['plan_days'];



    $findArray=array("%name%","%planname%","%txn_id%","%amount%","%plan_days%");

    $replaaceArray=array($name,$plan_name,$txn_id,$amount,$plan_days);



    $description= str_replace($findArray,$replaaceArray,$TemplateData['description']);







          // Payment email start

    $message =  $description;

      // $message = '<html>

      // <head>

      // <title></title>

      // </head>

      // <body>

      // <h2>Dear '.$UserDetail['name'].'</h2>

      // <p>Payment confirmation.</p>

      // <table>

      // <thead>

      // <tr>

      // <th>Plan Name</th>

      // <th>Transaction Id</th>

      // <th>Amount</th>

      // <th>Days</th>



      // </tr>

      // </thead>

      // <tbody>

      // <tr>

      // <td>'.$PlanDetail['plan_name'].'</td>

      // <td>'.$TransDetail['txn_id'].'</td>

      // <td>'.$TransDetail['amount'].'</td>

      // <td>'.$PlanDetail['plan_days'].'</td>

      // </tr>

      // </tbody>

      // </table>

      // </body>

      // </html>';

    $subject = $TemplateData['subject'];

      //$subject = "Transaction details.";

    $email = $UserDetail['email'];

      //$email = "info@carepro.com";

    $check = new_send_mail($message, $subject, $email, '');



      // Invoice mail end



    $data['transactionData']= array('user_id'=>

      $UserDetail['user_id'],'transaction_id'=>$TransDetail['txn_id'],'amount'=>$TransDetail['amount'],'plan_name'=>$PlanDetail['plan_name']);



    $this->template->set('title', 'Thank you');

    $this->template->load('home_layout', 'contents', 'payment_thank_you', $data);

  }





  public function paymentfailed()

  {



    $getparam = $this->uri->segment(2);

    $tempU_id = base64_decode($getparam);



    $data['tempData'] = $this->common_model->getSingleRecordById('cp_users_temprory',array('id'=> $tempU_id));

    //$data['temorder'] = base64_decode($getparam);



    $this->template->set('title', 'Failed');

    $this->template->load('home_layout', 'contents', 'payment_failed',$data);

  }





    // protected function no_cache()

    // {

    //   header('Cache-Control: no-store, no-cache, must-revalidate');

    //   header('Cache-Control: post-check=0, pre-check=0',false);

    //   header('Pragma: no-cache'); 

    // }





}
