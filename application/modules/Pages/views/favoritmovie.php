<aside class="right-side new-music">

	<section class="content-header no-margin">

		<h1 class="text-center"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Favorite Movie List 

			<a href="javascript:void(0)" class="search-open">

				<i class="fa fa-search"></i>

			</a>

		</h1>		

		<div class="searching">			

			<div class="search-inline">

				<form method="post" action="" name="movie_search" id="movie_search">

					<input type="text" class="form-control" placeholder="Movie Title" name="movie_title" id="movie_title">

					<input type="hidden" name="hidden_movie_id" id="hidden_movie_id" value="<?php echo $this->uri->segment(2); ?>" />

					<button type="submit" id="search_sub"><i class="fa fa-search"></i></button>

					<a href="javascript:void(0)" class="search-close">

						<i class="fa fa-times"></i>

					</a>

				</form>

			</div>

		</div>                  		

	</section>

	<section class="doc_cate">

	<?php if ($this->session->flashdata('success')) { ?>

                <div class="alert alert-success message">

                    <button type="button" class="close" data-dismiss="alert">x</button>

        <?php echo $this->session->flashdata('success'); ?></div>

        <?php } ?>	

		<div class="doc_cateMidd doc_listMidd">

			<div class="row" id="main-cntdiv">

			

			<?php 

           $checkLogin = $this->session->userdata('logged_in');

			$i= 1;

				   if(!empty($music_list)){

                     foreach($music_list as $music){

						

			$singleData = $this->Common_model_new->getsingle('cp_movie_trash',array('user_id' => $checkLogin['id'],'movie_id' => $music['movie_id']));

			if(empty($singleData)){

				 ?>

				<div class="col-lg-3 col-sm-6 col-md-3 col-xs-12">

					<article class="themetechmount-box themetechmount-box-team themetechmount-teambox-view-overlay">

						<div class="themetechmount-post-item loadData" id="myData_<?php echo $i;?>">

							<div class="themetechmount-team-image-box">

								<span class="themetechmount-item-thumbnail">

									<span class="themetechmount-item-thumbnail-inner">

									<?php if(!empty($music['movie_image'])){  ?>

										

										

										

										

										

										

								

										<img src="<?php echo base_url();?>uploads/movie/image/<?php echo $music['movie_image']; ?>" class="" alt="">

									<?php }else{ ?>

									

									

						

<img src="<?php echo base_url();?>assets/images/no-image-available.png" class="" alt="">

									<?php } ?>									

									</span>

								</span>

								<div class="btn-group new-music-btn">

										

										<a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url()?>favoritemovie_detail_page/<?php echo $music['movie_id'];  ?>"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>								

  

									  <div class="dropdown-menu">

										<a class="dropdown-item" href="<?php echo base_url()?>favoritemovie_detail_page/<?php echo $music['movie_id'];  ?>"><i class="fa fa-eye" aria-hidden="true"></i>View</a>

										<?php /* ?><a class="dropdown-item" href="<?php echo base_url(); ?>delete_favorite/<?php echo $music['music_id']; ?>" onclick="return confirm('Delete this record?')"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a><?php */ ?>

										

									  </div>

									</div>

								<div class="themetechmount-overlay">

									<div class="themetechmount-icon-box themetechmount-box-link">

									</div>

								</div>

							</div>

							<div class="themetechmount-box-content">

								<div class="themetechmount-box-title">

									<h4>

										<a href="<?php echo base_url()?>favoritemovie_detail_page/<?php echo $music['movie_id'];  ?>"><?php echo $music['movie_title']; ?></a>

									</h4>

								</div>

								<div class="themetechmount-team-position">

								<?php echo $music['movie_artist']; ?>

								</div>

							</div>

						</div>

					</article>

				</div>

				

				<!-- Example single danger button -->



				   <?php 	} ?>

				   

				 <?php  } ?>

				 <div class="load-more" lastID="<?php echo $music['movie_id']; ?>" style="display: none;">

        <img src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif"/>

    </div>

				 <?php }else{ ?>

				 No record found

				 <?php } ?>

			



			

			

			</div>

		</div>

	</section>

</aside>



<script src="<?php echo base_url(); ?>assets/js/jquery.min2.js"></script>

<script>

$(document).on('keyup','#movie_title',function(e){ 

 // $( "#music_title" ).keypress(function( e ) {

		 e.preventDefault();

		var movie_title = $("#movie_title").val();

		

	   $.ajax

		({

			url: "<?php echo base_url(); ?>Pages/FavoritMovie/search_movie_list",

			type: "POST",             

			data: 'movie_title='+movie_title, 

			//dataType: "json",			        

			success: function(response)   

			{

				$('#main-cntdiv').html(response);

			}

			

	   });

	   

	}); 



$(document).ready(function (e) 

  {

	$(window).scroll(function()

	{

		scrollMore();

	});

	

  });



function scrollMore()

{

  if($(window).scrollTop() == ($(document).height() - $(window).height()))

  { 

	//$(window).unbind("scroll");

	var records = '<?php echo $count_total;?>';

	

	var offset = $('[id^="myData_"]').length;

	//alert(offset);

	  if(records != offset)

	  { 

		$('#main-cntdiv').append('<img id="loader_img" style="width:50px" src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif" />');

		loadMoreData(offset);

	  }

  } 

}



  function loadMoreData(offset)

  {

	

	//var cat = $("#segmentid").val();

	var like = $("#movie_title").val();

	if(like){

		var like_val = like

	}else{

		var like_val = '';

	}

	

	$.ajax({

		type: 'post',

		async:false,

		url: '<?php echo base_url(); ?>Pages/FavoritMovie/get_offset/',

		data: 'offset='+offset+'&like_val='+like_val,

		success: function(data)

		{	

			

			$('#main-cntdiv').append(data);

			$('#loader_img').remove();

		},

		error: function(data)

		{

		  alert("ajax error occured�"+data);

		}

	}).done(function()

	{	

	   // getliveurl();

		$(window).bind("scroll",function()

		{

		

		   scrollMore();	

	    });

	});

}

</script>



<script>

       var sp = document.querySelector('.search-open');

  var searchbar = document.querySelector('.search-inline');

  var shclose = document.querySelector('.search-close');

  function changeClass() {

      searchbar.classList.add('search-visible');

  }

  function closesearch() {

      searchbar.classList.remove('search-visible');

  }

  sp.addEventListener('click', changeClass);

  shclose.addEventListener('click', closesearch);

      

  </script>

  

<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script src="<?php echo base_url();?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>

