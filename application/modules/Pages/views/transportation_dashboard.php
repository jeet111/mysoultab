<style>
    .btn.btn-primary {
        margin: 0 0 14px 0 !important;
        max-width: 171px !important;
    }
</style>
<div class=" text_heading">

	<div id="msg_show"></div>
    <h3 class="text-center"><i class="fa fa-truck" aria-hidden="true"></i> Transportation</h3>
    <div class="tooltip-2">
        <h2>Display Transportation button on Mobile App

<?php
 if(!empty($transportation_array)){

foreach ($transportation_array as $res) {
  if ($res->settings == 1) { ?>
    <label class="switch ">
      <input type="checkbox" checked data-btn="<?php echo 'Transportation' ?>" class="updateStatus">
      <span class="slider round"></span>
    </label>

<?php } else { ?>
<label class="switch space">
<input type="checkbox" data-btn="<?php echo 'Transportation' ?>" class="updateStatus">
<span class="slider round"></span>
</label>


<?php } ?>
<?php }
 } else {
?>
<label class="switch space">
<input type="checkbox" data-btn="<?php echo 'Transportation' ?>" class="updateStatus">
<span class="slider round"></span>
</label>
<?php } ?>
</h2>

	</div>

    <div class="rem_add">
        <a href="#addtransportationmyModal" class="btn button btn-primary" data-toggle="modal">Add Transportation</i> </a>

    </div>
</div>
<div class="row websiteAppClass">

<?php
if (!empty($transportation_list)) {
    foreach ($transportation_list as $key => $value) {
?>

        <div class="serv col-md-3 bg-info">

            <h4><?php
                echo $value->button_name; ?></h4>
                <?php
                if ($value->transportation_type == "Website") {
                ?>

                    <img class="game" src="https://www.google.com/s2/favicons?domain=<?php echo $value->url; ?>" data-url="<?php echo $value->url; ?>">
                <?php } else {
                    $app_list_data = $this->common_model_new->getAllwhere("app_list", array('id' => $value->app_name));
                ?>
                    <img class="game app" src="<?php echo $app_list_data[0]->icon; ?>">

                <?php } ?>
                <div class="rem_add_button">

                    <a href="#edittransportationmyModal" class="model-content edit edit_btn" data-toggle="modal">
                        <i class="material-icons update" data-toggle="tooltip" data-transportationid="<?php echo $value->id; ?>" data-transportationtype="<?php echo $value->transportation_type; ?>" data-url="<?php echo $value->url; ?>" data-app_name="<?php echo $value->app_name; ?>" data-button_name="<?php echo $value->button_name; ?>" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></i>
                    </a>

                    <a href="#deletetransportationmyModal" class="model-content delete edit_btn" data-id="<?php echo $value->id; ?>
                                            " data-toggle="modal"><i class="fa fa-trash" aria-hidden="true"></i></a>
                </div>
        </div>

    <?php }
} else { ?>
        <div class="nodataContainer"><h4 class="nodataFound">No Data Found!</h4></div>
<?php } ?>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		setTimeout(function() {
		$('#success').fadeOut('fast');
		}, 2500);
	});

	 $(document).on('click', '.updateStatus', function() {
        var update_status = $(this).attr('data-btn');
        var update_st = $(this).attr('data-st');
        //alert(update_status+'-'+update_st);
        $.ajax({
            url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
            type: "POST",
            data: {
                'btn': update_status
            },
            dataType: "json",
            success: function(data) {
                    $("#msg_show").html("<div class='alert alert-success' id='hide_msg'>" + data.status + "</div>");
					setTimeout(function() {
					$('#hide_msg').fadeOut('fast');
				}, 2500);   
            }
        });
    });
</script>
