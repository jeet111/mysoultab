<aside class="right-side">    

    <section class="content-header no-margin">

        <h1 class="text-center"><i class="fa fa-plus-square-o" aria-hidden="true"></i> <?php if($this->uri->segment('2')){ ?>Edit <?php }else{ ?>Add <?php } ?>Acknowledge



            <div class="back_reminder"><a href="<?php echo base_url(); ?>acknowledge_list" class="btn btn-danger">Back</a></div>

        </h1>

    </section>    

    <section class="content photo-list">

        <div class="photo-listMain">

           <form id="add_know" enctype="multipart/form-data" method="post" class="frm_add">

              <?php echo validation_errors(); ?>	



              <div class="row"> 



                <div class="col-sm-6 col-md-6">

                    <div class="form-group">

                        <label for="">Title<font style="color: red;">*</font></label>

                        <input type="text" class="form-control required" id="acknowledge_desc" name="acknowledge_title" placeholder="Title" value="<?php if(!empty($singleData->acknowledge_title)){ echo $singleData->acknowledge_title; } ?>">

                    </div>

                </div>







                <div class="col-sm-6 col-md-6">

                    <div class="form-group">

                        <label for="">Description<font style="color: red;">*</font></label>

                        <input type="text" class="form-control required" id="acknowledge_desc" name="acknowledge_desc" placeholder="Description" value="<?php if(!empty($singleData->acknowledge_desc)){ echo $singleData->acknowledge_desc; } ?>">

                    </div>

                </div>





            </div>



            <div class="row"> 

                <div class="col-sm-6 col-md-6">

                    <div class="form-group">

                        <label for="">Pharma Company<font style="color: red;">*</font></label>

                        <div class="" data-provide="datepicker">

                          <select name="pharma_company_id" id="pharma_company_id" class="form-control required">

                              <option value="">Select Pharma Company</option>

                              <?php foreach($pharma_company as $pharma){ ?>

                                <option value="<?php echo $pharma->id; ?>" <?php if(!empty($singleData->pharma_company_id) == $pharma->id){echo 'selected'; } ?>><?php if(!empty($pharma->name)){ echo $pharma->name; }?></option>

                            <?php } ?>

                        </select>  

                    </div>

                </div>

            </div>

            <div class="col-sm-6 col-md-6">

                <div class="form-group">

                    <label for="">Medicine<font style="color: red;">*</font></label>

                    <div class="" data-provide="datepicker">

                      <select name="medicine_id" id="medicine_id" class="form-control required">

                       <option value="">Select Medicine</option>

                       <?php foreach($medicine_list as $medicine){ ?>

                        <option value="<?php echo $medicine->medicine_id; ?>" <?php if(!empty($singleData->medicine_id) == $medicine->medicine_id){echo 'selected'; } ?>><?php if(!empty($medicine->medicine_name)){ echo $medicine->medicine_name; } ?></option>

                    <?php } ?>

                </select>  

            </div>

        </div>

    </div>



</div>



<div class="box-footer">

    <button type="submit" class="btn btn-primary">Submit</button>

    <!-- <a href="<?php echo base_url(); ?>acknowledge_list"  class="btn btn-danger">Back</a> -->

</div>

</form>              

</div>

</section>

</aside>



<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script> 



<script>

    $(document).ready(function() {

        $("#add_know").validate({

            rules: {

                acknowledge_title:"required",

                acknowledge_desc: "required",

                pharma_company_id: "required",

                medicine_id: "required",                

            },



            messages: {

                acknowledge_title:"Please enter acknowledge title.",

                acknowledge_desc: "Please enter acknowledge description.",

                pharma_company_id: "Please select a pharma company.",

                medicine_id: "Please select a medicine.",                 

            },

        });



    });

</script>

<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>







