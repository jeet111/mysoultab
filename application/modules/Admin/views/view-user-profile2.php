<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table user-list">

		<div class="card-body">

			<div class="my_info_one">

				<div class="Schedule_main_one">

					<h2>View Profile</h2>

					<div class="btn_topBack">

						<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">

							Back

						</a>

					</div>



					<ul class="nav nav-tabs">

						<li class="active">

							<a data-toggle="tab" href="#userinfo">

								User Detail

							</a>

						</li>

						<li>

							<a data-toggle="tab" href="#paymentinfo">

								Payment Detail

							</a>

						</li>

						<li>

							<a data-toggle="tab" href="#tabletinfo">

								Tablet Detail

							</a>

						</li>

						<li>

							<a data-toggle="tab" href="#tabletverification">

								Admin Verification

							</a>

						</li>



						<li>

							<a data-toggle="tab" href="#verificationlog">

								Verification Logs

							</a>

						</li>

					</ul>



					<div class="tab-content">

						<div id="userinfo" class="tab-pane fade in active">

							<div class="row">

								<div class="col-md-3">

									<div class="card faq-left">

										<div class="social-profile">

											<?php if (!empty($users_data['profile_image'])){ ?>

												<img class="img-fluid" src="<?php echo base_url().'uploads/profile_images/'.$users_data['profile_image'];?>" alt="">

											<?php } else { ?>

												<img class="img-fluid" src="<?php echo base_url().'uploads/profile_images/user.png' ?>">

											<?php } ?>

										</div>

										<div class="card-block">

											<h4><?php //echo $users_data['username'];?></h4>

											<ul class="list_prof">

												<li>

													<a href="tel:<?php echo $users_data['country_code'].$users_data['mobile'];?>"><i class="fa fa-phone"></i>

														<?php echo $users_data['mobile'];?>

													</a>

												</li>

												<li>

													<a href="mailto:<?php echo $users_data['email'];?>">

														<i class="fa fa-envelope"></i>

														<?php echo $users_data['email'];?></a>

													</li>

												</ul>

											</div>

										</div>

									</div>

									<div class="col-md-9">

										<div class="general-info">

											<div class="card-header">

												<h5 class="card-header-text">About Me</h5>

												<p><?php echo $users_data['name'].' '.$users_data['lastname'];?></p>

											</div>

											<div class="rrow">

												<div class="col-md-6">

													<div class="row">

														<div class="col-md-5 col-sm-5">

															<div class="f_name">Login Id:</div>

														</div>

														<div class="col-md-7 col-sm-7">

															<div class="f_nameOne"><?php echo $users_data['username'];?></div>

														</div>

													</div>

												</div>

												<div class="col-md-6">

													<div class="row">

														<div class="col-md-5 col-sm-5">

															<div class="f_name">Contact:</div>

														</div>

														<div class="col-md-7 col-sm-7">

															<div class="f_nameOne eml">

																<?php echo '+'.$users_data['country_code'].$users_data['mobile'];?>

															</div>

														</div>

													</div>

												</div>

											</div>

											<div class="rrow">

												<div class="col-md-6">

													<div class="row">

														<div class="col-md-5 col-sm-5">

															<div class="f_name">Email :</div>

														</div>

														<div class="col-md-7 col-sm-7">

															<div class="f_nameOne">

																<a href="tel:<?php echo $users_data['country_code'].$users_data['mobile'];?>">

																	<?php echo $users_data['email'];?>

																</a>

															</div>

														</div>

													</div>

												</div>

											</div>

										</div>

										<?php $admin_url = $this->uri->segment(2);

										$url = 'admin/users/list';

										if ($admin_url == 'adminprofile') {

											$url = 'admin/dashboard';

										}elseif($users_data['user_role'] == '3'){

											$url = 'admin/family_member/list';

										}elseif($users_data['user_role'] == '4'){

											$url = 'admin/caregiver/list';

										}?>



									</div>

								</div>

							</div>



							<?php //echo "<pre>"; print_r($userstransdata); ?>



							<?php if(!empty($userstransdata)){ ?>

								<div id="paymentinfo" class="tab-pane fade">

									<div class="row">

										<div class="col-md-6">

											<div class="col-md-5 col-sm-5">

												<div class="f_name">Plan:</div>

											</div>

											<div class="col-md-7 col-sm-7">

												<div class="f_nameOne"><?php echo $userstransdata['plan_name']; ?></div>

											</div>

										</div>



										<div class="col-md-6">

											<div class="col-md-5 col-sm-5">

												<div class="f_name">Purchase Date:</div>

											</div>

											<div class="col-md-7 col-sm-7"> 

												<?php $date = $userstransdata['transaction_date'];

												$date = str_replace('-', '/', $date);

												$dt= date('d/m/Y', strtotime($date)); 

												?>

												<div class="f_nameOne"><?php    echo $dt; ?></div>

											</div>

										</div>



										<div class="col-md-6">

											<div class="col-md-5 col-sm-5">

												<div class="f_name">Start Date:</div>

											</div>

											<div class="col-md-7 col-sm-7">

												<div class="f_nameOne"><?php echo $userstransdata['start_date']; ?></div>

											</div>

										</div>



										<div class="col-md-6">

											<div class="col-md-5 col-sm-5">

												<div class="f_name">End Date:</div>

											</div>

											<div class="col-md-7 col-sm-7">

												<div class="f_nameOne"><?php echo $userstransdata['end_date']; ?></div>

											</div>

										</div>



										<div class="col-md-6">

											<div class="col-md-5 col-sm-5">

												<div class="f_name">Amount:</div>

											</div>

											<div class="col-md-7 col-sm-7">

												<div class="f_nameOne"><?php echo $userstransdata['amount']; ?> $</div>

											</div>

										</div>



										<div class="col-md-6">

											<div class="col-md-5 col-sm-5">

												<div class="f_name">Transaction Status:</div>

											</div>

											<div class="col-md-7 col-sm-7">

												<div class="f_nameOne"><?php echo $userstransdata['transaction_status']; ?></div>

											</div>

										</div>



										<div class="col-md-6">

											<div class="col-md-5 col-sm-5">

												<div class="f_name">Transaction ID:</div>

											</div>

											<div class="col-md-7 col-sm-7">

												<div class="f_nameOne"><?php echo $userstransdata['transaction_id']; ?></div>

											</div>

										</div>



									</div>

								</div>

							<?php }else{ ?>

								<div id="paymentinfo" class="tab-pane fade">

									<div class="row">

										<div class="col-md-6">

											<div class="col-md-5 col-sm-5">

												<div class="f_name">Plan:</div>

											</div>

											<div class="col-md-7 col-sm-7">

												<div class="f_nameOne">N/A</div>

											</div>

										</div>



										<div class="col-md-6">

											<div class="col-md-5 col-sm-5">

												<div class="f_name">Purchase Date:</div>

											</div>

											<div class="col-md-7 col-sm-7">

												<div class="f_nameOne">N/A</div>

											</div>

										</div>



										<div class="col-md-6">

											<div class="col-md-5 col-sm-5">

												<div class="f_name">Start Date:</div>

											</div>

											<div class="col-md-7 col-sm-7">

												<div class="f_nameOne">N/A</div>

											</div>

										</div>



										<div class="col-md-6">

											<div class="col-md-5 col-sm-5">

												<div class="f_name">End Date:</div>

											</div>

											<div class="col-md-7 col-sm-7">

												<div class="f_nameOne">N/A</div>

											</div>

										</div>



										<div class="col-md-6">

											<div class="col-md-5 col-sm-5">

												<div class="f_name">Amount:</div>

											</div>

											<div class="col-md-7 col-sm-7">

												<div class="f_nameOne">N/A</div>

											</div>

										</div>



										<div class="col-md-6">

											<div class="col-md-5 col-sm-5">

												<div class="f_name">Transaction Status:</div>

											</div>

											<div class="col-md-7 col-sm-7">

												<div class="f_nameOne">N/A</div>

											</div>

										</div>



										<div class="col-md-6">

											<div class="col-md-5 col-sm-5">

												<div class="f_name">Transaction ID:</div>

											</div>

											<div class="col-md-7 col-sm-7">

												<div class="f_nameOne">N/A</div>

											</div>

										</div>



									</div>

								</div>



							<?php } ?>

							<div id="tabletinfo" class="tab-pane fade">



								<?php if (!empty($token_info)) { ?>



									<button data-toggle="modal" data-target="#tabletModal" class="btn btn-default center-block" style="margin-top: 20px;">

										View Token Detils

									</button>



								<?php }else{ ?>

									<button data-toggle="modal" data-target="#tabletModal" class="btn btn-default center-block" style="margin-top: 20px;">

										Assign Tablet

									</button>

								<?php } ?>

								<!-- Modal -->

								<div id="tabletModal" class="modal fade" role="dialog">

									<div class="modal-dialog">



										<!-- Modal content-->

										<div class="modal-content">

											<div class="modal-header">

												<button type="button" class="close" data-dismiss="modal">&times;</button>

												<h4 class="modal-title">Assign Tablet</h4>

											</div>

											<div class="modal-body">

												<div id="responseMsg<?php echo $this->session->userdata('logged_ins')['id']; ?>"></div>



												<?php

												if (!empty($token_info)) {



													?>

													<div>

														<label style="width: 150px;">Token: </label><?php echo $token_info['token']; ?>

													</div>

													<br>



													<!--<div>

														<label style="width: 150px;">Device ID: </label><?php //echo $token_info['device_id']; ?>

													</div>

													<br>-->



													<div>

														<label style="width: 150px;">Tablet Name: </label><?php echo $token_info['tablet_name']; ?>

													</div>

													<br>



													<div>

														<label style="width: 150px;">Status: </label><?php echo ($token_info['status'] == 1 ? 'Active' : 'Inactive'); ?>

													</div>

													<br>



													<div>

														<label style="width: 150px;">Create Date: </label><?php echo (!empty($token_info['created_at']) ? date('d-m-Y H:i A', strtotime($token_info['created_at'])) : 'N/A'); ?>

													</div>

													<br>

													<?php

												} else {

													?>

													<form class="form-horizontal" onsubmit="return false;" id="tokenForm" action="#" method="post">

														<input type="hidden" name="user_id" value="<?php echo $users_data['id']; ?>">

														<div class="form-group">

															<label class="control-label col-sm-4" for="tablet_name">Tablet Name:</label>

															<div class="col-sm-8">

																<input type="text" required class="form-control" id="tablet_name" name="tablet_name" placeholder="Enter tablet name">

															</div>

														</div>



														<!--<div class="form-group">
															<label class="control-label col-sm-4" for="device_id">Device ID:</label>
															<div class="col-sm-8">
																<input type="text" required class="form-control" id="device_id" name="device_id" placeholder="Enter device id">
															</div>
														</div>-->

														<div class="form-group"></div>



														<div class="form-group">

															<label class="control-label col-sm-4" for="token">Token:</label>

															<div class="col-sm-8">

																<input type="text" required class="form-control" id="token" name="token" placeholder="Enter token">

																<button type="button" class="btn btn-default col-sm-offset-6" onclick="genrateToken();" id="generateToken">Generate New Token</button>

															</div>

														</div>



														<div class="form-group">

															<div class="col-sm-offset-4 col-sm-8">

																<?php 

																$form_id = 'tokenForm'; 

																$user_id = $this->session->userdata('logged_ins')['id'];

																?>

																<button type="submit" onclick="submit_token('<?php echo $form_id; ?>','<?php echo $user_id; ?>');" class="btn btn-default">Submit</button>

															</div>

														</div>

													</form>

													<?php

												}

												?>





											</div>

											<div class="modal-footer">

												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

											</div>

										</div>



									</div>

								</div>



							</div>







							<!-- verificatio section -->

							

							<div id="tabletverification" class="tab-pane fade">

								<div class="row">



									<table class="ver_full_tab"  border =1>

										<tr>

											<th class="action_sss">Verification</th>

											<th class="status_sss">Status</th>

											<th class="action_sss">Action</th>



										</tr>



										<tr>

											<!-- <td><div class="f_name">Device Verification:</div></td>-->

											<!--<td><div class="f_nameOne"><?php if($users_data['device_verify']==1){ ?> 
												<!--<button type='' class='btn ybt btn-xs'   disabled>Verified</button>--> <?php }else{ ?>

											<!-- <button type='' class='btn xbt btn-xs'  disabled>Not Verified</button>-->

												<!-- <button type='button' data-to='device_verify' data-activation='1' data-id='<?php //echo $users_data['id']; ?>' class='btn xbt btn-xs activation_cls'>Verify</button> -->

												<?php } ?></div></td>

												<!-- <td><button type='button' data-to='device_verify' data-activation='1' data-id='<?php //echo $users_data['id']; ?>' class='btn-info activation_cls'>Click to verify</button></td>-->

											</tr>



											<tr>

												<td><div class="f_name">Token Verification:</div></td>

												<td><div class="f_nameOne"><?php if($users_data['device_verify_by_token']==1){ ?> 

													<span id="btton_change_<?php echo $users_data['id']; ?>">

														<button type='' class='btn ybt btn-xs'  disabled>Verified</button> </span>

													<?php }else{ ?>

														<button type='' class='btn xbt btn-xs'  disabled>Not Verified</button>

														<!-- <button type='button' data-to='device_verify_by_token' data-activation='1' data-id='<?php echo $users_data['id']; ?>' class='btn xbt btn-xs activation_cls' >Verify</button> -->

													<?php } ?>

												</div></td>

												<td><button type='button' data-to='device_verify_by_token' data-activation='1' data-id='<?php echo $users_data['id']; ?>' class='btn-info activation_cls' >Click to verify</button></td>



											</tr>

											<tr>

												<td><div class="f_name">Registration Verification:</div></td>

												<td><div class="f_nameOne"><?php if($users_data['user_verify_by_app']==1){ ?> <button type='' class='btn ybt btn-xs'  disabled>Verified</button> <?php }else{ ?>



													<button type='' class='btn xbt btn-xs'  disabled>Not Verified</button>



													<!-- <button type='button' data-to='user_verify_by_app' data-activation='1' data-id='<?php echo $users_data['id']; ?>' class='btn xbt btn-xs activation_cls'>Verify</button> -->

													<?php } ?></div></td>

													<td><button type='button' data-to='user_verify_by_app' data-activation='1' data-id='<?php echo $users_data['id']; ?>' class='btn-info activation_cls'>Click to verify</button></td>

												</tr>

												<tr>

													<td><div class="f_name">OTP Verification:</div></td>

													<td><div class="f_nameOne"><?php if($users_data['status']==1){ ?> <button type='' class='btn ybt btn-xs'  disabled>Verified</button> <?php }else{ ?>

														<button type='' class='btn xbt btn-xs'  disabled>Not Verified</button>

														<!-- <button type='button' data-to='status' data-activation='1' data-id='<?php echo $users_data['id']; ?>' class='btn xbt btn-xs activation_cls'>Verify</button> -->

														<?php } ?></div></td>

														<td><button type='button' data-to='status' data-activation='1' data-id='<?php echo $users_data['id']; ?>' class='btn-info activation_cls'>Click to verify</button></td>

													</tr>



													



												</table>





												





												







											</div>

										</div>

										<!-- End -->





										<!-- verification log -->





										<div id="verificationlog" class="tab-pane fade">

											<div class="row">



												<table class="ver_full_tab" border =1>

													<tr>

														<th class="action_sss">Verification</th>

														<th class="action_sss">Status</th>



													</tr>



													<tr>
														<!-- <td><div class="f_name">Device Verification:</div></td>-->
														<td>
															<!--<div class="f_nameOne"><?php if($users_data['device_verify']==1){ ?> 
															<!--<button type='' class='btn ybt btn-xs'   disabled>Verified</button>--> 
															<?php }else{ ?>
															<!--<button type='' class='btn xbt btn-xs'  disabled>Not verified</button>-->
															<?php } ?></div>
														</td>
													</tr>



														<tr>

															<td><div class="f_name">Token Verification:</div></td>

															<td><div class="f_nameOne"><?php if($users_data['device_verify_by_token']==1){ ?> 

																<span id="btton_change_<?php echo $users_data['id']; ?>">

																	<button type='' class='btn ybt btn-xs'  disabled>Verified</button> </span>

																<?php }else{ ?>



																	<button type='' class='btn xbt btn-xs'  disabled>Not verified</button>



																<?php } ?>

															</div></td>



														</tr>

														<tr>

															<td><div class="f_name">Registration Verification:</div></td>

															<td><div class="f_nameOne"><?php if($users_data['user_verify_by_app']==1){ ?> <button type='' class='btn ybt btn-xs'  disabled>Verified</button> <?php }else{ ?>

																<button type='' class='btn xbt btn-xs'  disabled>Not verified</button>

																<?php } ?></div></td>



															</tr>

															<tr>

																<td><div class="f_name">OTP Verification:</div></td>

																<td><div class="f_nameOne"><?php if($users_data['status']==1){ ?> <button type='' class='btn ybt btn-xs'  disabled>Verified</button> <?php }else{ ?>

																	<button type='' class='btn xbt btn-xs'  disabled>Not verified</button>

																	<?php } ?></div></td>



																</tr>

																

																

															</table>









														</div>

													</div>



													<!-- log end -->

												</div>









											</div>

										</div>

									</div>



								</div>

							</div>



							<script type="text/javascript">

								function submit_token(form_id,user_id) {

	       //var formData = new FormData($("#"+form_id)[0]);

	       var form = $('#'+form_id)[0];

	       var data = new FormData(form);

	       $.ajax({

	       	type: 'POST',

	       	url: "<?php echo base_url().'admin/submit_token'; ?>",

	       	data: data,

	           //dataType: "text",

	           processData: false,

	           contentType: false,

	           cache: false,

	           success: function(resultData) { 

	           	console.log(resultData);

	           	if (parseInt(resultData) === 1) {

	           		$("#responseMsg"+user_id).html('<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> Token has been submitted successfully!</div>');

	           		setTimeout(function() {

	           			location.reload();

	           		}, 5000);

	           	} else {

	           		$("#responseMsg"+user_id).html('<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Some internal issue occured.</div>');

	           	}



	           },

	           error: function (errorData) {

	           	console.log(errorData);

	           	$("#responseMsg"+user_id).html('<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Some internal issue occured.</div>');

	           }

	       });

	   }

	</script>	



	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>



	<script type="text/javascript">

		$(document).on('click','.activation_cls',function(){



			var activation_id = $(this).attr('data-activation');

			var user_id = $(this).attr('data-id');	

			var column_id = $(this).attr('data-to');

			//alert(activation_id+'-'+user_id+'-'+column_id);

			$.ajax({

				type: "POST",

				url: '<?php echo base_url(); ?>Admin/Admin_user/verify_status/',

				data: 'activation_id='+activation_id+'&user_id='+user_id+'&column_id='+column_id,

            //dataType: "json",

            success: function(data)

            {



            	console.log(data);

            	if(data == 1){

            		alert("verified successfully.");

            		location.reload();



            		//$("#btton_change_"+user_id).html("<button type='' class='btn ybt btn-xs'  disabled>Verified</button>");

            	}

            }

        });

		});	

	</script>
pe: "POST",
				url: '<?php echo base_url(); ?>Admin/Admin_user/verify_status/',
				data: 'activation_id='+activation_id+'&user_id='+user_id+'&column_id='+column_id,
            //dataType: "json",
            success: function(data)
            {

            	console.log(data);
            	if(data == 1){
            		alert("verified successfully.");
            		location.reload();

            		//$("#btton_change_"+user_id).html("<button type='' class='btn ybt btn-xs'  disabled>Verified</button>");
            	}
            }
        });
		});	
	</script>
