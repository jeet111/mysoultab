<div class="container-fluid">
	<div class="dash-counter users-main my-sts-table">
		<div class="row">
			<div class="col-md-12">
				<div class="">
					<?php //echo "<pre>"; print_r($groceryData); echo "</pre>";?>
					<div class="card-body">
						<div class="status-cng"></div>
						<?php if ($this->session->flashdata('success')) { ?>
                    <div class="alert alert-success message">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <?php echo $this->session->flashdata('success'); ?></div>
                <?php } ?>
					<table class="table table-hover tab_comn" id="sampleTable">
						<thead>
							<tr>
								<th>S no.</th>
								<th>Username</th>
								<th>Grocery Name</th>
								<th>Grocery Address</th>							
								<th>Grocery Image</th>							
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i = 1;
						foreach ($groceryData as $grocery) {
								?>
							<tr>
								<td><?php echo $i++; ?></td>
								<td><?php echo $grocery->username; ?></td>
								<td><?php echo $grocery->grocery_name; ?></td>
								<td><?php echo $grocery->grocery_address; ?></td>
								<td>
									<?php if(!empty($grocery->grocery_image)){ ?>
									<img src="<?php echo $grocery->grocery_image; ?>" class="" alt="">
								<?php }else{ ?>
									<img src="<?php echo base_url();?>assets/images/bank_img.jpg" class="" alt="">
								<?php } ?>
								</td>
								<td>
									<div class="link-del-view">
										<div class="tooltip-2">
											<a href="<?php echo base_url(); ?>admin/view_grocery/<?php echo $grocery->grocery_id; ?>">
												<i class="fa fa-eye" aria-hidden="true"></i></a>
											<span class="tooltiptext">View</span>
										</div>

										<div class="tooltip-2">
											<a href="<?php echo base_url(); ?>admin/delete_grocery/<?php echo $grocery->grocery_id; ?>" onclick="return confirm('Delete this grocery ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

											
										</div>
										
										
									</div>
									
							</div>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>
</div>
</div>