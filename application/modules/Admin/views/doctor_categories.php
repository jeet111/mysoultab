<div class="container-fluid">
	<div class="dash-counter users-main my-sts-table">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<div class="status-cng"></div>
						<?php
					      if($this->session->flashdata('success')) {
					      $message = $this->session->flashdata('success');
					    ?>
					      <div class="<?php echo $message['class'] ?>"><?php echo $message['message']; ?></div>
					    <?php } ?>
						<!-- <div class="add-btn"><a href="admin/add/user">Add User</a></div> -->
						<table class="table table-hover table-bordered" id="sampleTable" width="100%">
							<thead>
								<tr>
									<th>S no.</th>
									<th>Doctor Category</th>
									<th>Icon</th>
									<th>Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php  foreach ($dc_categories_data as $key => $category) {
									?>
									<tr>
										<td><?php echo ($key+1); ?></td>
										<td><?php echo $category['dc_name']; ?></td>
										<td><img src="<?php echo base_url().'uploads/doctor_category/'.$category['dc_icon']; ?>" style="background: #333;"></td>
										<td><?php echo $category['dc_created']; ?></td>
										<td>
											<div class="link-del-view">
												<div class="tooltip-2">
													<a href="admin/view/dc_cat/<?php echo $category['dc_id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
													<span class="tooltiptext">View</span>
												</div>
												<div class="tooltip-2">
													<a href="javascript:void(0)" onclick="deleteStatus(<?php echo $category['dc_id']; ?>,'admin/delete/dc_cat')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>
												</div>
											</div>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>