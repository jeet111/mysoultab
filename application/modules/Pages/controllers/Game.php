<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Game extends MX_Controller {



	public function __construct() {

		parent:: __construct();

		$this->load->library('session');

		$this->load->library('form_validation');

		$this->load->model('activities_model');

		$this->load->model('Common_model_new');

		$this->load->model('common_model_new');

		$this->load->model('movie_model');

		$this->load->helper(array('common_helper'));

		$this->load->helper(array('url'));

		$this->load->helper(array('form'));

		not_login();

	}


	public function add_game()
  {
    if(count($_POST)>0){
        if($_POST['type']==1){
      
        
        $array =array(
           'user_id' => $this->session->userdata('logged_in')['id'],
           'game_type' => $this->input->post('game_type'),
           'button_name' => $this->input->post('button_name'),
           'url'=>$this->input->post('url'),
           'app_name' => $this->input->post('app_name'),
           'created' => date('Y-m-d H:i:s'));
       }

      $check_game = $this->Common_model_new->getsingle("cp_game",array("button_name" => $this->input->post('button_name'),"user_id" => $this->session->userdata('logged_in')['id']));
      if(!empty($check_game)){
        unset($_POST);
        $this->session->set_flashdata('success', "Game already added");
        echo json_encode(array("statusCode"=>200,"msg"=>"game already added"));
        die();
      }
       $insert = $this->Common_model_new->insertData('cp_game',$array);
       if($insert){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        $this->session->set_flashdata('success', "Game added successfully");
        echo json_encode(array("statusCode"=>200,"msg"=>"Game added successfully"));
        die();
      }

    }
  }

  public function edit_game()
  {
    if(count($_POST)>0){
        if($_POST['type']==2){
        $game_id = $this->input->post('id');
        
        $array =array(
           'user_id' => $this->session->userdata('logged_in')['id'],
           'game_type' => $this->input->post('game_type'),
           'button_name' => $this->input->post('button_name'),
           'url'=>$this->input->post('url'),
           'app_name' => $this->input->post('app_name'),
           'created' => date('Y-m-d H:i:s'));
       }

      $check_game = $this->Common_model_new->allexcludeone("cp_game",array("button_name" => $this->input->post('button_name'),"user_id" => $this->session->userdata('logged_in')['id']),$game_id);
      if(!empty($check_game)){
        unset($_POST);
        $this->session->set_flashdata('success', "Game already added");
        echo json_encode(array("statusCode"=>200,"msg"=>"game already added"));
        die();
      }
       $update = $this->Common_model_new->updateData('cp_game',$array,array('id' => $game_id));
       if($update){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        $this->session->set_flashdata('success', "Game updated successfully");
        echo json_encode(array("statusCode"=>200,"msg"=>"Game updated successfully"));
        die();
      }

    }
  }

  public function delete_game()
  {
    if(count($_POST)>0){
        if($_POST['type']==3){
        $game_id = $this->input->post('id');

       $update = $this->Common_model_new->delete('cp_game',array('id' => $game_id));
       if($update){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        echo json_encode(array("statusCode"=>200,"msg"=>"Game deleted successfully"));
        die();
      }

    }
  }
}

public function dashboard_game(){
    $data['menuactive'] = '';
    $checkLogin = $this->session->userdata('logged_in');
  $user_id=$this->session->userdata('logged_in')['id'];
    if(!empty($checkLogin))
    {
      $data['menuactive'] = $this->uri->segment(1);
     
      unset($_POST);
      
      $data['app_list'] = $this->common_model_new->getAllwhere("app_list",array('status' => 1,'use_for'=>"Game"));
      $data['gameArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'Game',"user_id"=>$user_id));

      $data['game_list'] = $this->common_model_new->getAllwhere("cp_game",array('user_id'=>$user_id));
      $this->template->set('title', 'Dashboard Game');

      $this->template->load('new_user_dashboard_layout', 'contents', 'dashboard_game', $data);
    } 
    else{

      redirect('login');
  
    } 
  }



   

 
	



}