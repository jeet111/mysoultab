<style>

.cat_icons {

background-color: gainsboro;

}

</style>



<?php

if (isset($staff_permission[14]['add_permission']) == 1) {

	?>

	<div class="bx_user">

	<div class="add-btn">

		<a href="<?php echo base_url(); ?>admin/add_meditation_article_category">

		Add Category</a></div>

	</div>

	<?php

} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

	?>

	<div class="bx_user">

	<div class="add-btn">

		<a href="<?php echo base_url(); ?>admin/add_meditation_article_category">

		Add Category</a></div>

	</div>

	<?php

}

?>





<div class="container-fluid">

<div class="dash-counter users-main my-sts-table">

<div class="row">

<div class="col-md-12">

<div class="">

<?php //echo "<pre>"; print_r($category_list); echo "</pre>";?>

<div class="card-body">

<div class="status-cng"></div>

<?php if ($this->session->flashdata('success')) { ?>

<div class="alert alert-success message">

<button type="button" class="close" data-dismiss="alert">x</button>

<?php echo $this->session->flashdata('success'); ?></div>

<?php } ?>

<div id="deletemsg"></div>

<!-- <div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_article_category">Add Category</a></div> -->

<table class="table table-hover tab_comn" id="sampleTable">

<thead>

	<tr>

		<th>S no.</th>

		<th>Category Name</th>

		<th>Icon</th>

		<th>Status</th>

		<th>Action</th>

	</tr>

</thead>

<tbody>

	<?php

	$i = 1;

	foreach ($category_list as $list) {

		?>

		<tr class="card_one">

			<td><?php echo $i++; ?></td>

			<td><?php echo $list->article_category_name; ?></td>

			<td><img class="cat_icons" src="<?php echo base_url('uploads/meditation_articles/category_icon/'.$list->article_category_icon) ?>" height="50px" width="100px">    </td>

			<td>

				<?php if($list->article_category_status==1){ ?>



					<span class="testing_<?php echo $list->article_category_id; ?>"><button type="" class="btn btn-xs ybt" disabled="">Active</button></span>



				<?php }else{ ?>



					<span class="testing_<?php echo $list->article_category_id; ?>"><button type="" class="btn btn-xs xbt" disabled="">Inactive</button></span>





				<?php } ?>



			</td>



			<td>

				<div class="link-del-view">



					<?php

					if (isset($staff_permission[14]['view_permission']) == 1) {

						?>

						<div class="tooltip-2">

							<a href="<?php echo base_url(); ?>admin/view_meditation_article_category/<?php echo $list->article_category_id; ?>">

								<i class="fa fa-eye" aria-hidden="true"></i>

							</a>

							<span class="tooltiptext">View</span>

						</div>

						<?php

					} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

						?>

						<div class="tooltip-2">

							<a href="<?php echo base_url(); ?>admin/view_meditation_article_category/<?php echo $list->article_category_id; ?>">

								<i class="fa fa-eye" aria-hidden="true"></i>

							</a>

							<span class="tooltiptext">View</span>

						</div>

						<?php

					}

					?>

					

					<?php

					if (isset($staff_permission[14]['edit_permission']) == 1) {

						?>

						<div class="tooltip-2">

							<a href="<?php echo base_url(); ?>admin/edit_meditation_article_category/<?php echo $list->article_category_id; ?>">

								<i class="fa fa-edit" aria-hidden="true"></i>

							</a>

							<span class="tooltiptext">Edit</span>

						</div>



						<?php if($list->article_category_status == 1) { ?>

							<div class="tooltip-2"><a href="javascript:void(0);" id="statusarticlecat_<?php echo $list->article_category_id; ?>"  data-cat_id="<?php echo $list->article_category_id; ?>"><span class="test_<?php echo $list->article_category_id; ?>"><i class="fa fa-toggle-on" aria-hidden="true"></i></span></a>

								<span class="tooltiptext">Active</span>

							</div>

						<?php }else { ?>



							<div class="tooltip-2"><a href="javascript:void(0);" id="statusarticlecat_<?php echo $list->article_category_id; ?>" data-cat_id="<?php echo $list->article_category_id; ?>"><span class="test_<?php echo $list->article_category_id; ?>"><i class="fa fa-toggle-off" aria-hidden="true"></i></span></a>

								<span class="tooltiptext">Deactive</span>

							</div>

						<?php } ?>

						<?php

					} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

						?>

						<div class="tooltip-2">

							<a href="<?php echo base_url(); ?>admin/edit_meditation_article_category/<?php echo $list->article_category_id; ?>">

								<i class="fa fa-edit" aria-hidden="true"></i>

							</a>

							<span class="tooltiptext">Edit</span>

						</div>



						<?php if($list->article_category_status == 1) { ?>

							<div class="tooltip-2"><a href="javascript:void(0);" id="statusarticlecat_<?php echo $list->article_category_id; ?>"  data-cat_id="<?php echo $list->article_category_id; ?>"><span class="test_<?php echo $list->article_category_id; ?>"><i class="fa fa-toggle-on" aria-hidden="true"></i></span></a>

								<span class="tooltiptext">Active</span>

							</div>

						<?php }else { ?>



							<div class="tooltip-2"><a href="javascript:void(0);" id="statusarticlecat_<?php echo $list->article_category_id; ?>" data-cat_id="<?php echo $list->article_category_id; ?>"><span class="test_<?php echo $list->article_category_id; ?>"><i class="fa fa-toggle-off" aria-hidden="true"></i></span></a>

								<span class="tooltiptext">Deactive</span>

							</div>

						<?php } ?>

						<?php

					}

					?>

					

					<?php

					if (isset($staff_permission[14]['delete_permission']) == 1) {

						?>

						<div class="tooltip-2">

							<a href="javascript:void(0);" data-id="<?php echo $list->article_category_id; ?>" id="deletearticlecat_<?php echo $list->article_category_id; ?>">

								<i class="fa fa-trash-o" aria-hidden="true"></i>

							</a>

							<span class="tooltiptext">

								Delete

							</span>

						</div>

						<?php

					} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

						?>

						<div class="tooltip-2">

							<a href="javascript:void(0);" data-id="<?php echo $list->article_category_id; ?>" id="deletearticlecat_<?php echo $list->article_category_id; ?>">

								<i class="fa fa-trash-o" aria-hidden="true"></i>

							</a>

							<span class="tooltiptext">

								Delete

							</span>

						</div>

						<?php

					}

					?>

					

					



				</div>



			</div>

		</td>

	</tr>

<?php } ?>

</tbody>

</table>

</div>

</div>

</div>

</div>

</div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<script>

$(document).on('click','[id^="deletearticlecat_"]',function(){

var delete_article_cat = $(this).attr('data-id');



//alert(delete_article_cat);





if(confirm("Are you sure, you want to delete this category ?")){

$.ajax({

type: "POST",

url: '<?php echo base_url(); ?>Admin/Meditation_article/delete_meditation_article_category/',

data: 'delete_article_cat='+delete_article_cat,

//dataType: "json",

success: function(data)

{

console.log(data);



setTimeout(function(){

$("#deletemsg").html("<div class='alert alert-success'>Deleted Successfully</div>");

location.reload();



}, 1000);



}

});

$(this).parents(".card_one").animate({ backgroundColor: "#fbc7c7" }, "fast")

.animate({ opacity: "hide" }, "slow");

}

});





$(document).on('click','[id^="statusarticlecat_"]',function(){

var cat_id = $(this).attr('data-cat_id');

$.ajax({

type: "POST",

url: '<?php echo base_url(); ?>Admin/Meditation_article/meditation_category_status',

data: 'cat_id='+cat_id,

// dataType: "json",

success: function(data)

{

if(data == 1){

$(".test_"+cat_id).html('<i class="fa fa-toggle-on" aria-hidden="true"></i>');

$(".testing_"+cat_id).html('<button type="" class="btn ybt btn-xs"  disabled="">Active</button>');

//$("#deletemsg").html("<div class='alert alert-success'>Category active successfully</div>");

$('#deletemsg').html("<div class='alert alert-success'>Category active successfully</div>").fadeIn().delay(1000).fadeOut();

}else{

$(".test_"+cat_id).html('<i class="fa fa-toggle-off" aria-hidden="true"></i>');

$(".testing_"+cat_id).html('<button type="" class="btn xbt btn-xs"  disabled="">Inactive</button>');

// $("#deletemsg").html("<div class='alert alert-success'>Category deactive successfully</div>");

$('#deletemsg').html("<div class='alert alert-success'>Category deactive successfully</div>").fadeIn().delay(1000).fadeOut();

}

}

});



});

</script>