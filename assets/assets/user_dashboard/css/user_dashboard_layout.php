<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Care Pro | <?php echo !empty($title) ? $title : "User Dashboard"; ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

         <link rel="stylesheet" href="<?php echo base_url();?>assets/css/multiselect/bootstrap-multiselect.css">

        <link href="<?php echo base_url();?>assets/user_dashboard/img/favicon.ico" rel="icon">

        <link href="<?php echo base_url(); ?>assets/user_dashboard/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/user_dashboard/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/user_dashboard/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/user_dashboard/css/morris/morris.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/user_dashboard/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/user_dashboard/css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/user_dashboard/css/timepicker/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
        
        <link href="<?php echo base_url(); ?>assets/user_dashboard/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/user_dashboard/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        
        <link href="<?php echo base_url(); ?>assets/user_dashboard/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/user_dashboard/css/user_dashboard.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/user_dashboard/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        
        <link href="<?php echo base_url(); ?>assets/user_dashboard/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        
        <link href="<?php echo base_url(); ?>assets/user_dashboard/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        
        <link href="<?php echo base_url(); ?>assets/user_dashboard/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
       
        <link href="<?php echo base_url(); ?>assets/user_dashboard/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/user_dashboard/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user_dashboard/css/jquery.fancybox.css" type="text/css" media="screen" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">

    </head>
    <body class="skin-blue">        
        <header class="header">
            <a href="<?php echo base_url(); ?>" class="logo">Care Pro</a>            
            <nav class="navbar navbar-static-top" role="navigation">            
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <?php 
                $checkLogin = $this->session->userdata('logged_in');
                ?>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">                        
                        
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo $checkLogin['username']; ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="<?php echo base_url(); ?>assets/user_dashboard/img/avatar3.png" class="img-circle" alt="User Image" />
                                    <p>
                                        <?php echo $checkLogin['username']; ?> 
                                        <small>Member since <?php echo date("F, Y", strtotime($checkLogin['create_user'])); ?></small>
                                    </p>
                                </li>
                               
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?php echo base_url(); ?>user/update_profile" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="logout" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
	<?php $total_email = $this->common_model->jointwotablenm('cp_emails', 'user_id', 'cp_users', 'id',array('to_email ' => $checkLogin['email'],'email_status' => 1,'cp_emails.inbox_status' => 0),'','cp_emails.email_id','desc'); 
	
	$profile_image = $this->common_model->getsingle('cp_users',array('id' => $checkLogin['id']));
	
	//echo $this->db->last_query();die;?>	
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
						<?php if($profile_image->profile_image){ ?>
						<img src="<?php echo base_url(); ?>uploads/profile_images/<?php echo $profile_image->profile_image; ?>" class="img-circle" alt="User Image" />
						<?php }else{ ?>
                            <img src="<?php echo base_url(); ?>assets/user_dashboard/img/avatar3.png" class="img-circle" alt="User Image" />
						<?php } ?>
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?php echo $checkLogin['username']; ?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <ul class="sidebar-menu">
                        <li <?php if($menuactive == ''){
                            echo 'class="active"';}?>>
                            <a href="<?php echo base_url().'dashboard'; ?>">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li <?php if($menuactive == 'email_list'){
                            echo 'class="active"';}?>>
                            <a href="<?php echo base_url();?>email_list">
                                <i class="fa fa-envelope"></i> <span>Email Screen</span>
                                <small class="badge pull-right bg-yellow"><?php echo count($total_email); ?></small>
                            </a>
                        </li>
                        <li <?php if($menuactive == 'user_photos'){
                            echo 'class="active"';}?>>
                            <a href="<?php echo base_url();?>user_photos">
                                <i class="fa fa-picture-o" aria-hidden="true"></i> <span>Photo Screen</span>
                            </a>
                        </li>
                        <li <?php if($menuactive == 'photos_favourite'){
                            echo 'class="active"';}?>>
                            <a href="<?php echo base_url();?>photos_favourite">
                                <i class="fa fa-heart-o" aria-hidden="true"></i> <span>Favorite Screen</span>
                            </a>
                        </li>


                        <li <?php if($menuactive == 'doctor_speciality'){
                            echo 'class="active"';}?>>
                            <a href="<?php echo base_url();?>doctor_speciality">
                                <i class="fa fa-user-md" aria-hidden="true"></i> <span>Doctor Specialties</span>
                            </a>
                        </li>
                        
						<li <?php if($menuactive == 'reminder_list'){       echo 'class="active"';}?>>
                            <a href="<?php echo base_url(); ?>reminder_list">
                                <i class="fa fa-bell-o" aria-hidden="true"></i> <span>Reminder</span>
                            </a>
                        </li>



                        <li <?php if($menuactive == 'show_activities'){       echo 'class="active"';}?>>
                            <a href="<?php echo base_url(); ?>show_activities">
                                <i class="fa fa-tasks" aria-hidden="true"></i> <span>
                                Show Activities
                            </span>
                            </a>
                        </li>

                        <li <?php if($menuactive == 'medicine_list'){       echo 'class="active"';}?>>
                            <a href="<?php echo base_url(); ?>medicine_list">
                                <i class="fa fa-medkit" aria-hidden="true"></i> <span>
                             Medicine Schedule
                            </span>
                            </a>
                        </li>                        

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-bell-o" aria-hidden="true"></i> <span>Appointment</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="#"><i class="fa fa-angle-double-right"></i> SWT</a>
                                    <ul class="frd_swtChild">
                                        <li>
                                            <a href="#"><i class="fa fa-angle-double-right"></i> SWT child</a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-angle-double-right"></i> SWT child</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-double-right"></i> SWT</a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"></i> SWT</a></li>
                            </ul>
                        </li>
                       
                        <li <?php if($menuactive == 'testreport_list'){       echo 'class="active"';}?>>
                            <a href="<?php echo base_url(); ?>testreport_list">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i> <span>Test Report List</span>
                            </a>
                        </li>
						
						<li <?php if($menuactive == 'acknowledge_list'){       echo 'class="active"';}?>>
                            <a href="<?php echo base_url(); ?>acknowledge_list">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i> <span>Acknowledge List</span>
                            </a>
                        </li>
						
						<li <?php if($menuactive == 'article_list'){       echo 'class="active"';}?>>
                            <a href="<?php echo base_url(); ?>article_list">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i> <span>Article List</span>
                            </a>
                        </li>
						<li <?php if($menuactive == 'music_list'){       echo 'class="active"';}?>>
                            <a href="<?php echo base_url(); ?>music_list">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i> <span>Music List</span>
                            </a>
                        </li>


                        <li <?php if($menuactive == 'movie_list'){       echo 'class="active"';}?>>
                            <a href="<?php echo base_url(); ?>movie_list">
                                <i class="fa fa-film" aria-hidden="true"></i> <span>Movie List</span>
                            </a>
                        </li>

                        <li <?php if($menuactive == 'favoritemusic_list'){       echo 'class="active"';}?>>
                            <a href="<?php echo base_url(); ?>favoritemusic_list">
                                <i class="fa fa-music" aria-hidden="true"></i> <span>Favorite Music List</span>
                            </a>
                        </li>
                        
                    </ul>
                </section>            
            </aside>

            <?php echo $contents;?>

        </div>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>        

        <script src="<?php echo base_url(); ?>assets/user_dashboard/js/jquery.min.js"></script>
        
        <script src="<?php echo base_url(); ?>assets/user_dashboard/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>		
        
        <script src="<?php echo base_url(); ?>assets/user_dashboard/js/bootstrap.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url(); ?>assets/user_dashboard/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
              
		<link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css">

		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

		<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/src/js/bootstrap-datetimepicker.js"></script>
		
		<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

        <script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/demo.js" type="text/javascript"></script>

		<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/custom.js" type="text/javascript"></script>

        <script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/dashboard.js" type="text/javascript"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.dataTables.min.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/plugins/dataTables.bootstrap.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/css/multiselect/bootstrap-multiselect.js"></script>
        
        <script type="text/javascript">$('#sampleTable').DataTable();</script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app.js" type="text/javascript"></script>

        <script>
            baguetteBox.run('.tz-gallery');
        </script>
       
        <script type="text/javascript">
            $(function() {
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                //Money Euro
                $("[data-mask]").inputmask();

                //Date range picker
                $('#reservation').daterangepicker();
                //Date range picker with time picker
                $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                        {
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                                'Last 7 Days': [moment().subtract('days', 6), moment()],
                                'Last 30 Days': [moment().subtract('days', 29), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                            },
                            startDate: moment().subtract('days', 29),
                            endDate: moment()
                        },
                function(start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
                );

                //iCheck for checkbox and radio inputs
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal',
                    radioClass: 'iradio_minimal'
                });
                //Red color scheme for iCheck
                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                    checkboxClass: 'icheckbox_minimal-red',
                    radioClass: 'iradio_minimal-red'
                });
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-red',
                    radioClass: 'iradio_flat-red'
                });

                //Colorpicker
                $(".my-colorpicker1").colorpicker();
                //color picker with addon
                $(".my-colorpicker2").colorpicker();

                //Timepicker
                $(".timepicker").timepicker({
                    showInputs: false
                });
            });
        </script>

        </div>
            </html>
    </body>
</html>