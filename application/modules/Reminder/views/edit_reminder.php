<style>

.btn.btn-primary {
    margin: 6px 0 16px !important;}
  .text_heading {
    border-bottom: 0;
  }
</style>
<div class=" text_heading">


        <h3><i class="fa fa-pencil-square-o"></i>Edit Reminder   </h3>

            <div class="back_reminder ">

                <a href="javascript:window.history.back()" class="btn btn-primary">Back</a></div>

         

        </div>

        <section class="content photo-list">

            <div class="photo-listMain">

                <form id="reminder" >

                    <input type="hidden" name="reminder_id" value="<?php echo $singleData->reminder_id; ?>">

                    <div id="success_message"></div>

                <?php /* ?>

                <?php if ($this->session->flashdata('success')) { ?>

                <div class="alert alert-success message">

                    <button type="button" class="close" data-dismiss="alert">x</button>

                <?php echo $this->session->flashdata('success'); ?></div>

                <?php } ?>

                <?php */ ?>

                <div class="row">

                    <div class="col-sm-4 col-md-4">

                        <div class="form-group">

                            <label for="">Title:<span class="red_star">*</span></label>

                            <input type="text" class="form-control" id="reminder_title" name="reminder_title" value="<?php if(!empty($singleData)){ echo $singleData->reminder_title; } ?>" placeholder="Reminder Title">

                            <td class="error"><?php echo form_error('reminder_title'); ?></td>

                        </div>

                    </div>

                    <div class="col-sm-4 col-md-4">

                        <div class="bootstrap-timepicker">

                            <div class="form-group">

                                <label>Date:<span class="red_star">*</span></label>

                                <div class="input-group date costom_err" id="datetimepicker">

                                    <input type='text' class="form-control" readonly name="reminder_date" value="<?php if($singleData->reminder_date){

                                        echo $singleData->reminder_date; } ?>" placeholder="Reminder Date" />

                                        <span class="input-group-addon">

                                            <span class="glyphicon glyphicon-calendar"></span>

                                        </span>

                                        <td class="error"><?php echo form_error('reminder_date'); ?></td>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="col-sm-4 col-md-4">

                            <div class="bootstrap-timepicker">

                                <div class="form-group">

                                    <label>Time:<span class="red_star">*</span></label>

                                    <div class="input-group" id='datetimepicker3'>

                                        <input type='text' data-format="hh:mm:ss" name="reminder_time"  class="form-control" value="<?php if(!empty($singleData)){ echo $singleData->reminder_time; } ?>" placeholder="Reminder Time" />

                                        <span class="input-group-addon add-on">

                                            <span class="glyphicon glyphicon-time"></span>

                                        </span>

                                    </div>

                                    <td class="error"><?php echo form_error('reminder_time'); ?></td>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-sm-4 col-md-6">

                            <div class="form-group">

                                <label>Remind Before:<span class="red_star">*</span></label>

                                <select name="before_time" class="form-control  ">

                                    <option value="">Select Before Time</option>

                                    <option value="5" <?php if($singleData->reminder_before=='5'){echo "Selected";} ?>>5 Minutes</option>

                                    <option value="10" <?php if($singleData->reminder_before=='10'){echo "Selected";} ?>>10 Minutes</option>

                                    <option value="15" <?php if($singleData->reminder_before=='15'){echo "Selected";} ?>>15 Minutes</option>

                                    <option value="20" <?php if($singleData->reminder_before=='20'){echo "Selected";} ?>>20 Minutes</option>

                                </select>

                                <td class="error"><?php echo form_error('reminder_date'); ?></td>

                            </div>

                        </div>

                        <div class="col-sm-4 col-md-6">

                            <div class="form-group">

                                <label>Repeat Reminder:<span class="red_star">*</span></label>

                                <select name="repeat_time" class="form-control  ">

                                    <option value="">Select Repeat Time</option>
                                 

<option value="Once" <?php if($singleData->repeat=='Once'){echo "Selected";} ?>>Once</option>

<option value="Every Day" <?php if($singleData->repeat=='Every Day'){echo "Selected";} ?>>Every Day</option>

<option value="Every Week" <?php if($singleData->repeat=='Every Week'){echo "Selected";} ?>>Every Week</option>

<option value="Every Year" <?php if($singleData->repeat=='Every Year'){echo "Selected";} ?>>Every Year</option>



                                </select>

                                <td class="error"><?php echo form_error('reminder_date'); ?></td>

                            </div>

                        </div>



                    </div>

                    <div class="row">

                        <div class="col-md-12">

                            <div class="form-group">

                                <label for="">Description:<span class="red_star">*</span></label>

                                <textarea class="form-control" id="reminder_description" name="reminder_description"  placeholder="Reminder Description"><?php echo $singleData->reminder_description ?></textarea>

                                <td class="error"><?php echo form_error('reminder_description'); ?></td>

                            </div>

                        </div>

                    </div>

                    <div class="box-footerOne">

                        <input type="submit" name="submit" class="btn btn-primary" value="Submit">

                    </div>

                </form>

            </div>

        </section>


    <!--Added by 95 on 5-2-2019 For Doctor category for validation-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

    <script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>







    <script type="text/javascript">

        $('#reminder').validate({

            rules: {

                reminder_title: "required",

                reminder_description:"required",

                reminder_date:"required",

                reminder_time:"required",

                before_time:"required",

                repeat_time:"required",

            },

            messages: {

                reminder_title: "Please enter reminder title.",

                reminder_description:"Please enter reminder descripiton.",

                reminder_date:"Please select reminder date.",

                reminder_time:"Please select reminder time.",

                before_time:"Please select remind before time.",

                repeat_time:"Please select repeat reminder time.",

            },

            submitHandler: function(form){

                $.ajax({

                    url: '<?php echo base_url();?>Reminder/edit_reminder1',

                    type: 'post',

                    data: $(form).serialize(),

                    success: function(response) {

                        if(response==1){

                            $('#success_message').fadeIn().html('<div class="alert alert-success message"><button type="button" class="close" data-dismiss="alert">x</button>Reminder Updated successfully.</div>');

                            setTimeout(function() {

                                $('#success_message').fadeOut("milliseconds");

                            }, 500);

                        }

                        location.href = '<?php echo base_url();?>reminder_list';



                    }            

                });

            }

        });



    </script>

























    <script type="text/javascript">

        $(function () {

            var today = new Date();

            $('#datetimepicker').datetimepicker({

                ignoreReadonly: true,

                format: 'YYYY-MM-DD',

                minDate: today

//minDate: today

});

        });



    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>