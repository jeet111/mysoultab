<!-- <style>
/*input#profile_image {
    width: 347px;
}*/
</style> -->
<div class="container-fluid">
	<div class="dash-counter">
		<div class="Schedule_main_one">
			<div class="users-main">
				<?php
				echo "<h2>Edit Doctor Category</h2>";
				?>

				<div class="btn_topBack">
					<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">
						Back
					</a>
				</div>

				<form class="form-horizontal" action="<?php echo site_url('admin/doctor-category/edit/'.$editData['dc_id']); ?>" method="post" enctype="multipart/form-data" name="AddUsers" id="edit-doctor-category" >
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for="">Category Name:<span class="field_req">*</span></label>
							<div class="col-md-8">
								<input type="text" placeholder="Enter Category name" class="form-control" name="category_name" id="username" value="<?php echo $editData['dc_name']?>">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="">Category Icon:<span class="field_req">*</span></label>
							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-btn">
										<!-- <span class="btn btn-default btn-files"> -->
											<input type="file" id="profile_image" class="form-control " name="picture" onchange="readURL(event)" >
											<!-- </span> -->
										</span>
										<div id="flupmsg" style="color: red;"></div>
									</div>

									<?php
									if($editData['dc_icon']==''){
										?>
										<img class="cat" id='img-upload' src="<?php echo base_url().'uploads/doctor_category/default_icon.png' ?>">

										<?php
									}
									?>
									<img class="cat" id='img-upload' src="<?php echo base_url().'uploads/doctor_category/'.$editData['dc_icon'] ?>">

								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4" for=""></label>
								<div class="col-sm-8">
									<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit">

							<!-- <a class="cancel-btn btn" href="javascript:window.history.back()">
												Back
											</a> -->
										</div>
									</div>
								</div>




							</form>
						</div>
					</div>
				</div>
			</div>

			<!-- Added by 95 for form validation -->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

			<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>
			<script>
				function readURL (event) {
					var pcFile = $('input[type=file]').val().split('\\').pop();
					var pcExt     = pcFile.split('.').pop();
					var output = document.getElementById('img-upload');
					if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"
						|| pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){
						$(".up_but").prop('disabled', false);

					output.src = URL.createObjectURL(event.target.files[0]);
					$('#img-upload').show();
					$('#flupmsg').html('');
				}else{

					$('#flupmsg').html('Please select only Image file.');
					$('#img-upload').hide();

					$(".up_but").prop('disabled', true);
				}
			}
		</script>
		<script type="text/javascript">


			$("#edit-doctor-category").validate({
				rules: {
					category_name: "required",
			// 	picture:{
			// required:true,
		 //        regx: /\.(jpe?g|png|gif|bmp)$/i,
			//    },
                //"shedules[]": "required",
            },
            messages: {
            	category_name: "Please enter category name",
                //shedules: "Please select at least on time",

            }
        });

    </script>