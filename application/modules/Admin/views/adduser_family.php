<div class="container-fluid">
	<div class="dash-counter">
		<div class="Schedule_main_one">
			<div class="users-main">
				<?php
				$act = 'Add';
				if ($this->uri->segment(2) == 'editfamily') {
					$act = 'Edit';
				}
				echo '<h2>'.$act.' User</h2>';
				?>
				<div class="btn_topBack">
					<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">
						Back
					</a>
				</div>
				<?php //echo "<pre>"; print_r($menuactive_add); echo "</pre>"?>
				<form class="form-horizontal" action="<?php echo base_url(); ?>Admin/Admin_user/adduser_family" method="post" enctype="multipart/form-data" name="AddUsers" id="AddUsersss">
					<?php if(empty($users_data)) { ?>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Full Name:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<input type="text" placeholder="Enter Full name" class="form-control" name="full_name" id="full_name" >
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Email:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<input type="text" placeholder="Enter email address" class="form-control" name="email" id="email" >
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">User Id:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<input type="text" placeholder="Enter user name" class="form-control" name="username" id="username">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Password:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<input type="password" placeholder="Enter password" class="form-control" name="password" id="password" >
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Confirm Password:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<input type="password" placeholder="Enter confirm password" class="form-control" name="cpassword" id="cpassword" >
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Mobile No:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<input type="text" placeholder="Enter mobile number" class="form-control" name="mobile" id="mobile" >
									</div>
								</div>
							</div>
						</div>
						<div  class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Profile Image:<span class="field_req"></span></label>
									<div class="col-md-8">
										<div class="input-group">
											<span class="input-group-btn">
												<span class="btn btn-default btn-file">
													Browse… <input type="file" id="profile_image" class="form-control" name="profile_image">
												</span>
											</span>
											<input type="hidden" class="form-control" name="user_profile_image" id="user_profile_image">
										</div>
										<img id='img-upload' src="<?php echo base_url().'uploads/profile_images/user.png' ?>">
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit">
								<!-- <a class="cancel-btn btn" href="javascript:window.history.back()">
												Back
											</a> -->
										</div>
									</div>
								<?php } else {
									foreach ($users_data as $user) {}
										?>
									<input type="hidden" class="form-control" value="<?php echo $user['id']?>" class="form-control" name="id" data-required="true">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4" for="">Full Name:<span class="field_req">*</span></label>
												<div class="col-md-8">
													<input type="text" placeholder="Enter Full name" class="form-control" name="full_name" id="full_name" value="<?php echo $user['name']; ?>">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4" for="">Email:<span class="field_req">*</span></label>
												<div class="col-md-8">
													<input type="text" placeholder="Enter email address" class="form-control" name="email" id="email" disabled value="<?php echo $user['email']; ?>">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4" for="">Mobile No:<span class="field_req">*</span></label>
												<div class="col-md-8">
													<input type="text" placeholder="Enter mobile number" class="form-control" name="mobile" id="mobile" value="<?php echo $user['mobile']; ?>">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4" for="">User Id:<span class="field_req">*</span></label>
												<div class="col-md-8">
													<input type="text" placeholder="Enter user name" class="form-control" name="username" id="username" disabled value="<?php echo $user['username']; ?>">
												</div>
											</div>
										</div>
									</div>



									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4" for="">New password:<span class="field_req"></span></label>
												<div class="col-md-8">
													<input type="text" placeholder="New Password" class="form-control" name="new_password" id="new_password">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4" for="">Confirm password:<span class="field_req"></span></label>
												<div class="col-md-8">
													<input type="text" placeholder="confirm password" class="form-control" name="confirm_pass" id="confirm_pass">
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4" for="">Profile Image:<span class="field_req"></span></label>
												<div class="col-md-8">
													<div class="input-group">
														<input type="file" id="profile_image" class="form-control" name="profile_image" onchange="loadFile(event)">
								            <!-- <span class="input-group-btn">
								                <span class="btn btn-default btn-file">
								                </span>
								            </span> -->
											<!-- <input type="hidden" class="form-control" name="user_profile_image" id="user_profile_image" value="<?php echo $user['profile_image']; ?>">
												<input type="hidden" class="form-control" name="exting_profile_image" id="exting_profile_image" value="<?php echo $user['profile_image']; ?>"> -->
											</div>
											<?php if (!empty($user['profile_image'])){ ?>
												<img id='img-upload' src="<?php echo base_url().'uploads/profile_images/'.$user['profile_image'] ?>">
											<?php } else { ?>
												<img id='img-upload' src="<?php echo base_url().'uploads/profile_images/user.png' ?>">
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<input type="submit" class="btn btn-default up_but" class="form-control" name="update" value="Submit">
									<?php $url = 'admin/users/list'; ?>
								<!-- <a class="cancel-btn btn" href="javascript:window.history.back()">
												Back
											</a> -->
										</div>
									</div>
								<?php } ?>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!--Added by 95 on 5-2-2019 For Doctor category for validation-->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
			<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>
			<script>
				function loadFile (event) {
					var pcFile = $('input[type=file]').val().split('\\').pop();
					var pcExt     = pcFile.split('.').pop();
					var output = document.getElementById('img-upload');
					if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"
						|| pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){
						$('#img-upload').show();
					output.src = URL.createObjectURL(event.target.files[0]);
					$('#flupmsg').html('');
				}else{
					$('#flupmsg').html('Please select only Image file');
					$('#img-upload').hide();
				}
			};
		</script>
		<script type="text/javascript">
			<?php if(empty($users_data)) { ?>
				$(document).ready(function() {
					$.validator.addMethod("regx", function(value, element, regexpr) {
						return regexpr.test(value);
					}, "Please upload valid extension jpg,jpeg,png file.");
					$("#AddUsersss").validate({
						rules: {
							username: {
								required: true,
        //maxlength: 40,
        remote: {
        	url: "<?php echo base_url(); ?>Admin/Admin_user/register_username_exists",
        	type: "post",
        }
    },
    email: {
    	required: true,
    	email: true,
      //maxlength: 40,
      remote: {
      	url: '<?php base_url(); ?>'+"Admin/Admin_user/validateData",
      	type: "post",
      }
  },
  full_name:{
  	required:true,
  },
  mobile:{
  	required:true,
  	number:true,
  	minlength: 10,
  	maxlength: 14,
  },
  password: {
  	required: true,
  	minlength: 6,
  	maxlength: 30,
  },
  cpassword: {
  	required:true,
  	minlength : 6,
  	maxlength: 30,
  	equalTo : "#password"
  },
},
messages: {
	username: {
		required: 'please enter username.',
         //maxlength: 40,
         remote: 'Username already used.',
     },
     email: {
     	required: "Please enter a email.",
     	email: "Please enter a valid email.",
      //maxlength: 40,
      remote: 'Email already used. Log in to your existing account.',
  },
  full_name:{
  	required: "Please enter a full name.",
  },
  mobile:{
  	required: "Please enter a mobile number.",
  	number:"Please enter a valid number.",
  	minlength: "Please enter at least 10 characters.",
  	maxlength: "Please enter no more than 14 characters.",
  },
  password: {
  	required: "Please enter a password.",
  	minlength: "Please enter at least 6 characters.",
  	maxlength: "Please enter no more than 30 characters.",
  },
  cpassword: {
  	required:"Please enter a confirm password.",
  	minlength : "Please enter at least 6 characters.",
  	maxlength: "Please enter no more than 30 characters.",
  	equalTo : "Confirm password not matched.",
  },
},
});
				});
			<?php }else{ ?>
				$(document).ready(function() {
					$.validator.addMethod("regx", function(value, element, regexpr) {
						return regexpr.test(value);
					}, "Please upload valid extension jpg,jpeg,png file.");
					$("#AddUsersss").validate({
						rules: {
							full_name: "required",
				//email: "required",
				//mobile:"required",
				// profile_image:{
				// 	required:true,
				// 	regx:/\.(jpe?g|png|gif|bmp)$/i,
				// },
				mobile: {
					required:true,
					number:true,
					minlength:10,
					maxlength:14,
				},

				new_password: {
					required: false,
					minlength: 6,
					maxlength: 30,
				},
				confirm_pass: {
					required:false,
					minlength : 6,
					maxlength: 30,
					equalTo : "#new_password"
				}
			},
			messages: {
				full_name:"Please enter full name.",
				mobile:{
					required:"Please enter mobile number.",
					number:"Please enter only number.",
					minlength:"Please enter at least 10 digits.",
					maxlength:"Please do not enter more than 14 digits.",
				},


				new_password: {
					//required: "Please enter a password",
					minlength: "Please enter at least 6 characters.",
					maxlength: "Please enter no more than 30 characters.",
				},
				confirm_pass: {
					//required:"Please enter a confirm password",
					minlength : "Please enter at least 6 characters.",
					maxlength: "Please enter no more than 30 characters.",
					equalTo : "Confirm password not matched.",
				}

				
				//email: "Please enter email.",
				//mobile:"Please enter mobile number.",
				// profile_image:{
				// 	required:"Please select an image.",
				// 	regx: "Allow formates jpeg,png,gif,bmp",
				// },
			}
		});
				});
			<?php } ?>
	// /*Created by 95 on 6-2-2019 For Image validation*/
	// function showFileSize(){
	// 	var img  = document.getElementById('profile_images').value;
	// 	if(img==='' || img===null){
	// 		document.getElementById("demo").innerHTML="Please choose an image.";
	// 	}else{
	// 		document.getElementById("demo").innerHTML=" ";
	// 	}
	// }
</script>