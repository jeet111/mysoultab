<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Social extends MX_Controller {



	public function __construct() {

		parent:: __construct();

		$this->load->library('session');

		$this->load->library('form_validation');

		$this->load->model('activities_model');

		$this->load->model('Common_model_new');

		$this->load->model('common_model_new');

		$this->load->model('movie_model');

		$this->load->helper(array('common_helper'));

		$this->load->helper(array('url'));

		$this->load->helper(array('form'));

		not_login();

	}



   
public function social_dashboard(){
    $data['menuactive'] = '';
    $checkLogin = $this->session->userdata('logged_in');

    if(!empty($checkLogin))
    {
      $data['menuactive'] = $this->uri->segment(1);
     
      unset($_POST);
			$user_id = $this->session->userdata('logged_in')['id'];
    
      $data['app_list'] = $this->common_model_new->getAllwhere("app_list",array('status' => 1,'use_for'=>"social"));
      $data['social_list'] = $this->common_model_new->getAllwhere("cp_social",array('user_id'=>$this->session->userdata('logged_in')['id']));
     	
			if(!empty($user_id ) && $user_id  !=''){
				$data['SocialArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'Social',"user_id"=>$user_id));
			}
			$this->template->set('title', 'Dashboard Game');
			$this->template->load('new_user_dashboard_layout', 'contents', 'social_dashboard', $data);
    } 
    else{

      redirect('login');
  
    } 
  }

  public function add_new_social()
  {
    if(count($_POST)>0){

				$postype = isset($_POST['type']) ? $_POST['type'] : '';

        if($postype==1 && $postype !=''){
						$array =array(
								'user_id' => $this->session->userdata('logged_in')['id'],
								'social_type' => $this->input->post('social_type'),
								'button_name' => $this->input->post('button_name'),
								'url'=>$this->input->post('url'),
								'app_name' => $this->input->post('app_name'),
								'created' => date('Y-m-d H:i:s')
						);
				}

       $check_social = $this->Common_model_new->getsingle("cp_social",array("button_name" => $this->input->post('button_name'),"user_id" => $this->session->userdata('logged_in')['id']));
      
			 $insrtmsg = "Social added successfully";
				if(!empty($check_social)){
						unset($_POST);
						$this->session->set_flashdata('success', $insrtmsg);
						echo json_encode(array("statusCode"=>200,"msg"=>$insrtmsg));
						die();
				}
      
				$insert = $this->Common_model_new->insertData('cp_social',$array);
      
				if(!empty($insert) && $insert !=''){
					unset($_POST);
					$this->session->set_flashdata('success', $insrtmsg);
					echo json_encode(array("statusCode"=>200,"msg"=>$insrtmsg));
					die();
				}
    }
  }

  public function edit_social()
  {
    if(count($_POST)>0){

			$postype = isset($_POST['type']) ? $_POST['type'] : '';

			if($postype==2 && $postype !=''){
        	$social_id = $this->input->post('id');
        	$array = array(
						'user_id' => $this->session->userdata('logged_in')['id'],
						'social_type' => $this->input->post('social_type'),
						'button_name' => $this->input->post('button_name'),
						'url'=>$this->input->post('url'),
						'app_name' => $this->input->post('app_name'),
						'created' => date('Y-m-d H:i:s')
					);
      }

       $check_social = $this->Common_model_new->allexcludeone("cp_social",array("button_name" => $this->input->post('button_name'),"user_id" => $this->session->userdata('logged_in')['id']),$social_id);
      
			 $updmsg = "Updated was successfull";

			 if(!empty($check_social)){
        unset($_POST);
				$this->session->set_flashdata('success', $updmsg);
        echo json_encode(array("statusCode"=>200,"msg"=> $updmsg));
        die();
      }

       $update = $this->Common_model_new->updateData('cp_social',$array,array('id' => $social_id));
       if(!empty($update) && $update !=''){
        //$this->session->set_flashdata('success','');
        unset($_POST);
				$updmsg = "Updated was successfull";
        $this->session->set_flashdata('success',$updmsg);
        echo json_encode(array("statusCode"=>200,"msg"=>$updmsg));
        die();
      }

    }
  }

  public function delete_social()
  {
    if(count($_POST)>0){
			
			$postype = isset($_POST['type']) ? $_POST['type'] : '';
			if($postype==3 && $postype !=''){
        
					$social_id = $this->input->post('id');

         $update = $this->Common_model_new->delete('cp_social',array('id' => $social_id));
         if($update){
          //$this->session->set_flashdata('success','');
          unset($_POST);
          echo json_encode(array("statusCode"=>200,"msg"=>"social deleted successfully"));
          die();
        }
      }
    }
  }
 
	



}
