<style>
    .text_heading {
        border-bottom: 0;
    }
    .addNote a, .btn.btn-primary {
    margin: 7px 0 0 !important;
}
</style>
<div class=" text_heading">
	<h3 class="text-center"><i class="" aria-hidden="true"></i> Alerts </h3>
	
	<div id="msg_show"></div>
	<div class="tooltip-2">  </div>
     <section class="content photo-list">
        <div class="photo-listMain reminder_listMain">

                <table class="table table-hover table-bordered table-mailbox send-user-mail tab_medi" id="sampleTable">

                    <thead>

                        <tr>

                            <th> Created Date</th>
							<th> Message</th>
							<th> Read Status</th>

                        </tr>

                    </thead>

				
					<tbody>
                     <?php 
                        if(!empty($alerts_array) && $alerts_array !=''){
                        foreach ($alerts_array as $k) {
                        ?>
                     <tr id="rowID">
					 
					 <?php //echo $k['user_id']; echo $k['category']; ?>
					 
                       <!-- <td data-btn="" id="user_id"></td>-->
                       <!-- <td data-btn="" id="category"></td>-->
                        <td data-btn="<?php echo $k['created_at']; ?>" id="created_at"><?php echo date("d-m-Y  H:i A", strtotime($k['created_at'])); ?></td>
                        <td data-btn="<?php echo $k['message']; ?>" id="message"><?php echo $k['message']; ?></td>
                        <?php $id = $k['id'];?>
                        <input type="hidden" name="alert_id" value="<?php echo $id; ?>" > 
                        <form class="form-horizontal" onsubmit="return false;" action="#" method="POST">
                           <td data-btn="<?php echo $k['read_flag']; ?>">
                              <a href="javascript:void(0)" onclick="update_status('<?php echo $id; ?>');">
                              <?php if($k['read_flag'] == 1) { ?>
                              		<i class="fa fa-toggle-on" aria-hidden="true"></i>  Already Read<?php
                                 }else{
                                 ?>
                             			<i class="fa fa-toggle-off" aria-hidden="true"></i>  Unread
                              <?php } ?>
                              </a>
                           </td>
                        </form>
                     </tr>
                     <?php }
                        } else { ?>
                    <h4>No Data Found!</h4>
                     <?php } ?>
                  </tbody>

                </table>

            </div>

     </section>
	 
<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>


<script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable();
	} );

	$('#alerts_page').click(function() {
		var url = '<?php echo base_url()."alerts" ?>';
		window.location.href = url;
		return false;
	});

	$('#dashboard_page').click(function() {
		var url = '<?php echo base_url()."dashboard_new" ?>';
		window.location.href = url;
		return false;
	});
	
   function update_status(alert_id) {
			  
   	/* if (confirm('Are you sure you want to update this?')) { */
   	  
   		var id = alert_id ;
		var url = "<?php echo base_url().'Pages/alerts/update_status'; ?>";
   
   		$.ajax({
   			type:'POST',
   			url:url,
   			data:{id:id},
   			
   			success: function(resultData) {
				console.log(resultData);
				var msg ="Alert Update Sucessfully";
				if (parseInt(resultData) === 1) {
					setTimeout(function() {
							$("#msg_show").html("<div class='alert alert-success'>" + msg + "</div>");
						location.reload();
					}, 2000);
	
				} else {
					alert("Some internal issue occured!");
   			  	}
   			},
   			error: function (errorData) {
   			  console.log(errorData);
   			  alert("Some internal issue occured!");
   			}
   		});
   	/* } */
   }
   
</script>
