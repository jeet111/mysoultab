<style>
.text_heading {
    border-bottom: none;
}
input#name,input#lastname {
    text-transform: capitalize !important;
}
img#img-upload {
    height: 70px;
    width: 70px;
    margin-bottom: 10px;
    float: left;
}
.row .col-md-8,.col-sm-10 {
    margin-bottom: 10px !important;
}
input.btn.btn-primary.up_but {
    margin: 0 !important;
}
.form-horizontal h3 {
    color: #000;
    text-align: center;
    margin: 0 0 17px 0px;
    /* font-weight: 600; */
}

.row.uf{
	padding-left: 15px;
}
.col-sm-10.uf{
	padding-left:8px;
}
.col-sm-2.uf{
	padding-left: 30px;
}
.col-md-4.uf{
	padding-left: 21px;
}
.form-control.uf{
	width:95%;
}
.col-sm-2.uf1{
	padding-left: 21px;
	margin-top: 10px;
}
.col-md-4.uf1{
	padding-left: 21px;
}
.form-control.uf1{

	width: 95%;
	margin-top: 11px;
}
.row.uf1{
	margin-top: 20px;
}

</style>

<div class=" text_heading">
  <div id="msg_show"></div>
  <h3><i class="fa fa-list"></i> User Profile<div class="rem_add"></h3>
</div>
<section class="content photo-list">
  <div class="photo-listMain reminder_listMain">
    <div class="swt_cor">
      <div class="col-sm-12">
				<form class="form-horizontal" action="<?php echo base_url(); ?>user_profile_add" method="POST" enctype="multipart/form-data" name="AddUsers" id="ADD_USER">
					<div class="row uf">
								<div class="col-sm-2">Profile Image:<span class="field_req"></span></div>
								<div class="col-sm-10 uf">
									<img id='img-upload' src="<?php echo base_url().'uploads/profile_images/user.png' ?>">
									<input type="file" id="profile_image" class="form-control" name="profile_image">
									<p id="error1" style="display:none; color:#FF0000;">
Invalid Image Format! Image Format Must Be JPG, JPEG, PNG or GIF.
</p>
<p id="error2" style="display:none; color:#FF0000;">
Maximum File Size Limit is 5MB.
</p>
									<input type="hidden" class="form-control" name="user_profile_image" id="user_profile_image">
								</div>
								</div>
								<div class="row">
								<div class="col-sm-6">
									<div class="col-sm-4">First Name<font style="color: red;">*</font></label></div>
									<div class="col-md-8">
										<input type="text" class="txtOnly form-control required" id="name" name="name" placeholder="Enter first name">
										<span class=""><?php echo form_error('name'); ?></span>
									</div>
									<div class="col-sm-4">Last Name:<font style="color: red;">*</font></label></div>
									<div class="col-md-8">
										<input type="text" class="txtOnly form-control required" id="lastname" name="lastname" placeholder="Enter last name" >
										<span class=""><?php echo form_error('lastname'); ?></span>

									</div>
									<div class="col-sm-4">Gender <font style="color: red;">*</font></label></div>
									<div class="col-md-8">
										<select name="gender" type="text" class="form-control" id="gender">
											<option value="">Select Gender</option>
											<option value="1" id="male"> Male</option>
											<option value="2" id="female"> Female</option>
											<option value="3" id="other"> Other</option>
										</select>
										<span class=""><?php echo form_error('gender'); ?></span>
									</div>
									<div class="col-sm-4">Email:<font style="color: red;">*</font></label></div>
									<div class="col-md-8">
										<input type="text" placeholder="Enter email address" class="form-control" name="email" id="email" >
										<span class=""><?php echo form_error('email'); ?></span>
									</div>
									<div class="col-sm-4">Street Address<font style="color: red;">*</font></label></div>
									<div class="col-md-8">
										<input type="text" class="form-control required" id="street_name" name="street_name" placeholder="Enter Street Address">
										<span class=""><?php echo form_error('street_name'); ?></span>

									</div>
									<div class="col-sm-4">City <font style="color: red;">*</font></label></div>
										<div class="col-md-8">
											<input type="text" class="form-control required" id="city" name="city" placeholder="Enter city">
											<span class=""><?php echo form_error('city'); ?></span>
										</div>
									</div>

							<div class="col-sm-6">
								<div class="col-sm-4">State <font style="color: red;">*</font></label></div>
								<div class="col-md-8">
									<input type="text" class="form-control required" id="state" name="state" placeholder="Enter state">
									<span class=""><?php echo form_error('state'); ?></span>

								</div>
								
								<div class="col-sm-4">Zipcode <font style="color: red;">*</font></label></div>
								<div class="col-md-8">
									<input type="text" class="form-control required" id="zipcode" name="zipcode" placeholder="Enter zipcode" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
									<span class=""><?php echo form_error('zipcode'); ?></span>

								</div>
								
								<div class="col-sm-4">Country <font style="color: red;">*</font></label></div>
								<div class="col-md-8">
									<input type="text" class="txtOnly form-control required" id="country" name="country" placeholder="Enter country">
									<span class=""><?php echo form_error('country'); ?></span>

								</div>
								
								<div class="col-sm-4">Phone/Cell No.<font style="color: red;">*</font></label></div>
								<div class="col-md-8">
									<input type="text" class="form-control required" id="mobile" name="mobile" placeholder="Enter mobile" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
									<span class=""><?php echo form_error('mobile'); ?></span> 

								</div>
								
								<div class="col-sm-4">language <font style="color: red;">*</font></label></div>
								<div class="col-md-8">
									<input type="text" class="form-control required" id="language" name="language" placeholder="Enter language">
									<span class=""><?php echo form_error('language'); ?></span> 

								</div>
							  	<div class="col-sm-4">Timezone<font style="color:red;">*</font>:</label></div>
									<div class="col-md-8">
										<select name="timezone" type="text" class="form-control" id="timezone">
											<?php 
												if(!empty($zone) && $zone !=''){ ?>
												<option value="">Select Time Zone</option>
												<?php foreach($zone as $k => $v){ ?>
													<option value="<?php echo $v['cp_ct_id']; ?>"> <?php echo $v['cp_ct_name']; ?>
													</option>
												<?php }  ?>
											<?php }  ?>
										</select>
										<span class=""><?php echo form_error('timezone'); ?></span> 
									</div> 
								</div>

							  	<div class="col-sm-2 uf">User Name:<font style="color:red;">*</font>:</label></div>
							  	<div class="col-md-4 uf">
									<input type="text" placeholder="Enter user name" class="form-control uf" name="username" id="username">
									<span class=""><?php echo form_error('username'); ?></span> 
								</div>

								<div class="col-sm-2 uf">Password:<font style="color:red;">*</font>:</label></div>
								<div class="col-md-4 uf">
									<input type="password" placeholder="Password" class="form-control uf" name="password" id="password">
									<span class=""><?php echo form_error('password'); ?></span> 
								</div>

								<div class="col-sm-2 uf1">Confirm password:<font style="color:red;">*</font></label></div>
								<div class="col-md-4 uf1">
									<input type="password" placeholder="confirm password" class="form-control uf1" name="cpassword" id="cpassword">
									<span class=""><?php echo form_error('cpassword'); ?></span> 
								</div>
							</div>

							<div class="row uf1">								
							<h3>Notification Preference Panel</h3>

							<div class="col-md-6">
								
								<div class="col-sm-4">Sent me text msg for alert <font style="color: red;">*</font></div>
								<div class="col-md-8">
									<select name="snt_txt_msg_alrt" class="form-control" id="snt_txt_msg_alrt">
										<option value="">Select text message alert</option>
										<?php 
											if(!empty($snt_txt_msg_alrt) && $snt_txt_msg_alrt !=''){
													foreach($snt_txt_msg_alrt as $key => $val){
										?>
										<option value="<?php echo $key; ?>"> <?php echo $val; ?></option>
										<?php } } ?>
									</select>
									<span class=""><?php echo form_error('snt_txt_msg_alrt'); ?></span> 
								</div>

								<div class="col-sm-4">Sent me email for daily routine acivites <font style="color: red;">*</font></div>
										<div class="col-md-8">
										<select name="snt_daily_eml_daily_rutin_alrt" class="form-control" id="snt_daily_eml_daily_rutin_alrt">
											<option value="">Select email for daily routine</option>
											<?php 
												if(!empty($snt_daily_eml_daily_rutin_alrt) && $snt_daily_eml_daily_rutin_alrt !=''){
														foreach($snt_daily_eml_daily_rutin_alrt as $key => $val){
											?>
											<option value="<?php echo $key; ?>"> <?php echo $val; ?></option>
											<?php } } ?>
										</select>
										<span class=""><?php echo form_error('snt_daily_eml_daily_rutin_alrt'); ?></span> 
									</div>
							</div>
							
							<div class="col-md-6">

								<div class="col-sm-4">Sent me email for alert <font style="color: red;">*</font></div>
									<div class="col-md-8">
										<select name="snt_eml_alrt" class="form-control" id="snt_eml_alrt">
											<option value="">Select email alert</option>
											<?php 
												if(!empty($snt_eml_alrt) && $snt_eml_alrt !=''){
														foreach($snt_eml_alrt as $key => $val){
											?>
											<option value="<?php echo $key; ?>"> <?php echo $val; ?></option>
											<?php } } ?>
										</select>
										<span class=""><?php echo form_error('snt_eml_alrt'); ?></span> 
									</div>
								


								<div class="col-sm-4">Sent me email Their is no activity in app <font style="color: red;">*</font></div>
										<div class="col-md-8">
											<select name="snt_eml_no_activity_in_app_alrt" class="form-control" id="snt_eml_no_activity_in_app_alrt">
												<option value="">Select email app activity</option>
												<?php 
													if(!empty($snt_eml_no_activity_in_app_alrt) && $snt_eml_no_activity_in_app_alrt !=''){
															foreach($snt_eml_no_activity_in_app_alrt as $key => $val){
												?>
												<option value="<?php echo $key; ?>"> <?php echo $val; ?></option>
												<?php } } ?>
											</select>
											<span class=""><?php echo form_error('snt_eml_no_activity_in_app_alrt'); ?></span> 
										</div>
									</div>


									<div class="col-md-6">
										<div class="col-sm-4">Sent me email when medicine alram missed<font style="color: red;">*</font></div>
											<div class="col-md-8">
												<select name="snt_eml_alrm_is_missed" class="form-control" id="snt_eml_alrm_is_missed">
													<option value="">Select medicine alram status</option>
													<?php 
														if(!empty($snt_eml_alrm_is_missed) && $snt_eml_alrm_is_missed !=''){
																foreach($snt_eml_alrm_is_missed as $key => $val){
													?>
													<option value="<?php echo $key; ?>"> <?php echo $val; ?></option>
													<?php } } ?>
												</select>
												<span class=""><?php echo form_error('snt_eml_alrm_is_missed'); ?></span> 
											</div>
										</div>
									
									</div>

									<div class="col-md-6">
										<input type="submit" class="btn btn-primary up_but" class="form-control" name="submit" value="Submit">
										 <input type="reset" name="cancel" class="btn btn-primary" value="Cancel" onclick="window.location='<?php echo base_url(); ?>user_profile';">
									</div>
					</div>		
				</form>				
			</div>
			</div>
			</div>
			</div>
			</section>
		<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>
		<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>

		<script>
			$(document).ready(function () {
				setTimeout(function() {
						$('#success_msg').fadeOut('fast');
					}, 2500);
				}
			);

			function loadFile (event) {

					var pcFile = $('input[type=file]').val().split('\\').pop();

					var pcExt     = pcFile.split('.').pop();

					var output = document.getElementById('img-upload');

					if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"

						|| pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){

						$('#img-upload').show();

					output.src = URL.createObjectURL(event.target.files[0]);

					$('#flupmsg').html('');

				}else{

					$('#flupmsg').html('Please select only Image file');

					$('#img-upload').hide();

				}

			};

		</script>

	<script type="text/javascript">
			
				$(document).ready(function () {

									    $('input[type="submit"]').prop("disabled", true);
var a=0;
//binds to onchange event of your input field
$('#profile_image').bind('change', function() {
if ($('input:submit').attr('disabled',false)){
 $('input:submit').attr('disabled',true);
 }
var ext = $('#profile_image').val().split('.').pop().toLowerCase();
if ($.inArray(ext, ['gif','png','jpg','jpeg']) == -1){
 $('#error1').slideDown("slow");
 $('#error2').slideUp("slow");
 a=0;
 }else{
 var picsize = (this.files[0].size);
 if (picsize > 5000000){
 $('#error2').slideDown("slow");
 a=0;
 }else{
 a=1;
 $('#error2').slideUp("slow");
 }
 $('#error1').slideUp("slow");
 if (a==1){
 $('input:submit').attr('disabled',false);
 }
}
});

					$( ".txtOnly" ).keypress(function(e) {
						var key = e.keyCode;
						if (key >= 48 && key <= 57) {
							e.preventDefault();
						}
					});

					jQuery.validator.addMethod("lettersonly", function(value, element) {
						return this.optional(element) || /^[a-z]+$/i.test(value);
					}, "Letters only please"); 

					$.validator.addMethod("regx", function (value, element, regexpr) {
						return regexpr.test(value);
					}, "Please upload valid extension jpg,jpeg,png file.");
					
					$("#ADD_USER").validate({

						rules: {

								name:{
									required: true,
									lettersonly: true
								},
								lastname:{
									required: true,
									lettersonly: true
								},
								gender:{
									required: true,
								},
								email:{
									required: true,
									email: true,
									maxlength: 40,
									remote: {
									url: '<?php base_url(); ?>'+"Pages/UserContact/validateData",
									type: "post",
									}
								},
								street_name: {
									required: true,
								},
								mobile: {
									required: true,
									number: true,
									minlength: 10,
									maxlength: 14,
								},
								city: {
									required: true,
								},
								state: {
									required: true,
								},
								zipcode: {
									required: true,
									minlength: 5,
									maxlength: 5,
									digits: true
								},
								country: {
									required: true,
									lettersonly: true
								},
								language: {
									required: true,
								},
								timezone:{
									required: true,
								},
								username: {
									required: true,
									maxlength: 40,
									remote: {
									url: '<?php base_url(); ?>'+"Pages/UserContact/validateData",
									type: "post",
									}
								},
								password: {
									required: true,
									minlength: 6,
									maxlength: 30,
								},
								cpassword: {
									required: true,
									minlength: 6,
									maxlength: 30,
									equalTo: "#password"
								},
								snt_txt_msg_alrt:{
									required: true,
								},
								snt_eml_alrt:{
									required: true,
								},
								snt_daily_eml_daily_rutin_alrt:{
									required: true,
								},
								snt_eml_no_activity_in_app_alrt:{
									required: true,
								},
								snt_eml_alrm_is_missed:{
									required: true,
								}
						},
						messages: {
							name:{
								required: 'Please enter first name.',
								lettersonly:'Only aphabet awllowed',
							},
							lastname:{
								required: 'Please enter last name.',
								lettersonly:'Only aphabet awllowed',
							},
							gender:{
								required:"Please select your gender.",
							},
							email:{
								required: "Please enter a email.",
								email: "Please enter a valid email.",
								maxlength: 40,
								remote: 'Email already used.',
							},
							street_name: {
								required: "Please enter street address.",
							},
							mobile: {
								required: "Please enter a mobile number.",
								number: "Please enter a valid number.",
								minlength: "Please enter at least 10 characters.",
								maxlength: "please enter max. 14 characters.",
							},
							city: {
								required: "Please enter city.",
							},
							state: {
								required: "Please enter state.",
							},
							zipcode: {
								required: "Please enter zipcode.",
								//required: "Please enter your Postal Code!",
								minlength: "Your Postal Code must be 5 numbers!",
								maxlength: "Your Postal Code must be 5 numbers!",
								digits: "Your Postal Code must be 5 numbers!"
								
							},
							country: {
								required: "Please enter country.",
								lettersonly:'Only aphabet awllowed',
							},
							language: {
								required: "Please enter language.",
							},
							timezone: {
								required: "Please Select timezone.",
							},
							username: {
								required: 'please enter username.',
								remote: 'Username already used.',
							},
							password: {
								required: "Please enter a password.",
								minlength: "Please enter at least 6 characters.",
								maxlength: "please enter max. 30 characters.",
							},
							cpassword: {
								required: "Please enter a confirm password.",
								minlength: "Please enter at least 6 characters.",
								maxlength: "Please enter no more than 30 characters.",
								equalTo: "Confirm password not matched.",
							},
							snt_txt_msg_alrt:{
								required: "Please select text message status.",
							},
							snt_eml_alrt:{
								required: "Please select email status.",
							},
							snt_daily_eml_daily_rutin_alrt:{
								required: "Please select email for daily routine acivites status.",
							},
							snt_eml_no_activity_in_app_alrt:{
								required: "Please select email thier is no activity in app status.",
							},
							snt_eml_alrm_is_missed:{
								required: "Please select medicine alram status.",
							}
						},
					});
				});
</script>
