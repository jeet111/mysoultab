<script>
	var userEmail = "<?php echo $this->session->userdata('logged_in')['email']; ?>";
</script>
<style>
button#appointmentSubmit {
    float: right;
    margin: 0 !important;
}


  .text_heading {
    border-bottom: none !important;
    margin: 0 0 14px 0 !important;
  }
  .alert.alert-success {
    text-align: center;
    margin: 0 0 10px 0;
    padding: 10px 0;
}
  tr#callFaxSection td {border:0 !important;
}
.dynamicButton:hover, .dynamicButton:focus, .dynamicButton:active {
    color: #fff;
}
tr#callFaxSection td {
    /* flex: 1 0 50%; */
    white-space: nowrap;
}
.dynamicButton {
    float: left;
    background: #337ab7;
    color: #fff;
    padding: 9px 45px;
    box-sizing: border-box;
    max-width: 100%;
    width: 100%;
    text-align: center;
    border: 0;
}
  table#doctorList {
    table-layout: fixed;

    border: 1px solid #ddd;
  }
  form#add_appointment td {
    text-align: left;
    margin-left: 5px !important;
  }
  .bootstrap-timepicker-widget.dropdown-menu.open {
    display: inline-block;
    z-index: 99999 !important;
  }

  #myTabContent table#sampleTable a {
    background: #1f569e !important;
    font-weight: bold;
    border: 1px solid #ddd;
    padding: 5px 4px;
    color: #fff;
  }

  #app_date {
    z-index: 99999 !important;
  }

  table#doctorList td,
  table#doctorList th {
    border: 1px solid #ddd !important;
  }

  .modal-footer {
    padding: 0 7px;
    text-align: right;
    border-top: none;
  }

  table#sampleTable {
    border: 1px solid #ddd;
  }

  table#sampleTable th {
    border-top: 1px solid #ddd;
  }

  #faxPopup h2 {
    margin: 0;
    color: #000;
    font-size: 20px;
    text-align: center;
  }

  #faxPopup .modal-header {
    padding: 15px 10px 0;
    border-bottom: none;
  }

  form#add_appointment input[type="text"] {
    padding-left: 12px;
    background: transparent;
    border: 0;
    margin: 0;
    padding: 0;
  }

  #doctorList th {
    color: #000;
    text-align: left !important;
  }

  div#scheduleAppointment th,
  #add_appointment th,
  #table#doctorList th {
    padding: 5px 0 5px 15px;
    color: #000;
    font-weight: bold;
    font-size: 16px;
    background: none !important;
  }

  div#scheduleAppointment td,
  #table#doctorList td #add_appointment td {
    padding: 3px 0 3px 10px;
    text-align: left;
  }

  div#scheduleAppointment td a {
    color: #000;
  }

  #scheduleAppointment .modal-header .close {
    margin-top: 1px;
  }

  #scheduleAppointment .modal-header {
    padding: 10px 10px 0 0;
    border-bottom: none !important;
  }

  #scheduleAppointment .modal-footer {
    padding: 10px 0 0 0;
    text-align: right;
    border-top: none !important;
  }

  #scheduleAppointment .table-bordered {
    margin: 0;
    table-layout: fixed;
  }

  label.error {
    color: #ff0000 !important;
  }

  .text_heading {
    border-bottom: 0;
  }

  ul#myTab li.active {
    background: #1f569e;
    color: #fff !important;
  }

  .modal-body label {
    font-weight: 400;
    color: #000;
  }

  .table-responsive {
    overflow-x: hidden;
  }

  ul#myTab li a {
    color: #000;
    font-weight: bold;
    padding: 12px 0;
    text-transform: uppercase;
  }

  .bootstrap-datetimepicker-widget {
    border: 0 !important;
    inset: 0 !important;
    height: 100px !important;
    width: 200px !important;
  }

  .nav-tabs>li.active>a,
  .nav-tabs>li.active>a:focus,
  .nav-tabs>li.active>a:hover {
    color: #555;
    cursor: default;
    background-color: #1f569e !important;
    border: none !important;
    border-bottom-color: transparent;
    color: #fff !important;
  }

  ul#myTab li {
    display: block;
    width: 50%;
    border: 1px solid #eee;
    text-align: center;

    font-size: 15px;
    color: #000 !important;
  }

  .modal-body.content.photo-list {
    float: left;
    width: 100%;
  }

  .active {
    background-color: #fff;
    color: #000;
  }
</style>
<div class=" text_heading">
  <div id="msg_show"></div>

  <h3><i class="fa fa-plus-square-o" aria-hidden="true"></i> Doctor List</h3>
  <form id="userForm" class="hide">
					<input id="username" placeholder="USERNAME" value="singh.bal83@gmail.com"><br>
					<input id="password" type="password" placeholder="PASSWORD" value="Jeet@123"><br>
					<button id="loginUser">Login</button>
					<button id="createUser">Create</button>
				</form>
				
  <?php if (!empty($btnsetingArray) && $btnsetingArray != '') { ?>
    <div class="tooltip-2">
      <h2>Display Doctor Appointemnt button on Mobile App
        <?php

        foreach ($btnsetingArray as $res) {
          if ($res->settings == 1 || $res->settings == '1') { ?>
            <label class="switch ">
              <input type="checkbox" checked data-btn="<?php echo 'Doctor Appointment' ?>" class="updateStatus">
              <span class="slider round"></span>
            </label>

          <?php } else { ?>
            <label class="switch space">
              <input type="checkbox" data-btn="<?php echo 'Doctor Appointment' ?>" class="updateStatus">
              <span class="slider round"></span>
            </label>
          <?php } ?>
        <?php }   ?>
      </h2>
    </div>
  <?php } ?>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#doctorModel">
    Add Doctor
  </button>
</div>
<!-- Modal -->
<div class="modal fade" id="doctorModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Docotor</h5>
        <button type="button" class="close" data-dismiss="modal" onclick="this.form.reset();" aria-label="Close" id="closeButton">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body content photo-list">
        <div class="photo-listMain">
          <form id="AddNewdoctor">
            <div id="success_message"></div>
            <div class="row">
              <div class="col-sm-6 col-md-6">
                <div class="form-group">
                  <label for="">Doctor Name:<span class="red_star">*</span></label>
                  <input type="text" class="form-control" id="doctor_name" name="doctor_name" value="" placeholder="Doctor Name" maxlength="100">
                  <td class="error"><?php echo form_error('doctor_name'); ?></td>
                </div>
              </div>
              <div class="col-sm-6 col-md-6">
                <div class="form-group">
                  <label>Doctor Address:<span class="red_star">*</span></label>
                  <input type="text" class="form-control" id="doctor_address" name="doctor_address" value="" placeholder="Doctor Address" maxlength="100">
                  <td class="error"><?php echo form_error('doctor_address'); ?></td>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6 col-md-6">
                <div class="form-group">
                  <label>Email:<span class="red_star">*</span></label>
                  <input type='text' name="doctor_email" class="form-control" value="" placeholder="Email Address" />
                </div>
                <td class="error"><?php echo form_error('doctor_email'); ?></td>
              </div>
              <div class="col-sm-6 col-md-6">
                <div class="form-group">
                  <label>Tel:<span class="red_star">*</span></label>
                  <input type="number" pattern="\d{3}[\-]\d{3}[\-]\d{4}" name="doctor_phone_number" class="form-control" value="" placeholder="Phone Number" maxlength="12" />
                  <td class="error"><?php echo form_error('doctor_phone_number'); ?></td>
                  <span class="error" style="color: red; display: none">* Input digits (0 - 9)</span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6 col-md-6">
                <div class="form-group">
                  <label>Fax:<span class="red_star">*</span></label>
                  <input type="fax" name="doctor_fax" id="doctor_fax" class="form-control" placeholder="Fax Number">
                  <td class="error"><?php echo form_error('doctor_fax'); ?></td>
                </div>
              </div>
              <div class="col-sm-6 col-md-6">
                <div class="form-group">
                  <label for="">Portal:<span class="red_star">*</span></label>
                  <input class="form-control" id="doctor_portal" name="doctor_portal" placeholder="Portal">
                  <td class="error"><?php echo form_error('doctor_portal'); ?></td>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 col-md-12">
                <div class="form-group">
                  <label for="">Speciality:<span class="red_star">*</span></label>
                  <select name="speciality" class="form-control" id="speciality">
                    <option value="">Select Doctor Speciality</option>
                    <?php foreach ($DoctorsSpeciality as $DoctorSep) { ?>
                      <option value="<?php echo $DoctorSep->dc_name; ?>"><?php echo ucwords($DoctorSep->dc_name); ?></option>
                    <?php } ?>
                  </select>
                  <td class="error"><?php echo form_error('speciality'); ?></td>
                </div>
              </div>
            </div>
            <div class="box-footerOne">
              <input type="submit" name="submit" class="btn btn-primary" value="Submit">
              <input type="reset" name="cancel" class="btn btn-primary" value="Cancel" onclick="window.location='<?php echo base_url();?>/appointment_list';">
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<!-- setting start -->
<section class="content photo-list">
  <div class="photo-listMain reminder_listMain">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item waves-effect waves-light active">
        <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Doctor Appointemnt</a>
      </li>
      <li class="nav-item waves-effect waves-light ">
        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Doctor List</a>
      </li>
    </ul>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane active " id="home" role="tabpanel" aria-labelledby="home-tab">
        <section class="content photo-list">
          <table class="table table-hover tab_comn" id="sampleTable">
            <thead>
              <tr>
                <!-- <th>S no.</th> -->
                <th> Doctor Name:</th>
                <th> Email</th>
                <th> Phone</th>
                <th> Fax</th>
                <th> Dcotor Address</th>
                <th> Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($appointmentResultNew)) {
                foreach ($appointmentResultNew as $app) {

              ?>

                  <tr id="rowID" data-btn="<?php echo $app['doctor_id']; ?>">
                    <?php if (!empty($app['other_doctor_name'])) { ?>
                      <td data-label="Doctor Name" data-btn="<?php echo  $app['other_doctor_name']; ?>" id="otheDoctorName"><?php echo $app['other_doctor_name']; ?></td>
                    <?php } else { ?>
                      <td data-label="Doctor Name"  data-btn="<?php echo  $app['doctor_name']; ?>" id="doctor_name"><?php echo $app['doctor_name']; ?></td>

                    <?php } ?>
                    <td data-label="Email"  data-btn="<?php echo  $app['doctor_email']; ?>" id="doctor_email"><?php echo $app['doctor_email']; ?></td>
                    <td data-label="Phone" data-btn="<?php echo  $app['doctor_mob_no']; ?>" id="doctor_mob_no"><?php echo $app['doctor_mob_no']; ?></td>
                    <td data-label="Fax" data-btn="<?php echo  $app['fax_num']; ?>" id="fax_num"><?php echo $app['fax_num']; ?></td>
                    <td data-label="Dcotor Address"  data-btn="<?php echo  $app['doctor_address']; ?>" id="doctor_address"><?php echo $app['doctor_address']; ?></td>
                    <td data-label="Action" ><a href="#" data-value="<?php echo $app['doctor_id']; ?>" id="cancelApp" class="cancelApp"> Cancel</a>
                      <a href="<?php echo base_url(); ?>edit_appointment/<?php echo  $app['dr_appointment_id']; ?>">Update</a>
                      <a href="<?php echo base_url(); ?>Appointment/delete_appointment/<?php echo $app['dr_appointment_id']; ?>" onclick="return confirm('Are you sure, you want to delete this Appointment ?')"> Delete</a>

                    </td>
                  </tr>
              <?php }
              } ?>
            </tbody>
          </table>
        </section>
      </div>
      <div class="tab-pane " id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <div class="table-responsive tb_swt">

          <table class="callApp" id="doctorList">
            <thead>
              <tr>
                <th>Name</th>
                <th>Address</th>
                <th>Email</th>
                <th>Tel</th>
                <th>Fax</th>
                <th>Schedule Appt</th>

              </tr>

            </thead>

            <tbody>

              <?php


              if (!empty($Doctors)) {

                foreach ($Doctors as $Doctorslist) { ?>

                  <tr>
                    <td data-label="Name">
                      <?php if (!empty($Doctorslist->other_doctor_name)) { ?>
                        <?php echo $Doctorslist->other_doctor_name; ?>

                      <?php } else { ?>
                        <?php echo $Doctorslist->doctor_name; ?>

                      <?php } ?>
                    </td>

                    <td data-label="Address"><?php echo $Doctorslist->doctor_address; ?></td>
                    <td data-label="Email"><?php if (!empty($Doctorslist->doctor_email)) {
                          echo $Doctorslist->doctor_email;
                        } ?></td>
                    <td data-label="Tel"><?php echo $Doctorslist->doctor_mob_no; ?></td>
                    <td data-label="Fax"><?php echo $Doctorslist->fax_num; ?></td>
                    <td data-label="Schedule Appt" class="act">
                      <a href="#" data-target="#scheduleAppointment" data-id="<?php echo $Doctorslist->doctor_id; ?>" data-value="<?php echo $Doctorslist->doctor_id; ?>" id="scheduleAppointmentOpen" class="btn btn-primary btn-single btn-sm fa-input scheduleAppointmentOpen">Schedule</i></a>
                    </td>
                  </tr>
              <?php }
              } ?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="scheduleAppointment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myModalLabel">Schedule Appointment</h4>
            </div>
            <script>
              	var userEmail = "<?php echo $this->session->userdata('logged_in')['email']; ?>";
            </script>


            
            <script>
              $("body").delegate("#app_date", "focusin", function() {

                $(this).datepicker({
                  minDate: 0
                });
              });
              $("body").delegate("#timepicker1", "focusin", function() {

                $('#timepicker1').timepicker({
                  showInputs: false
                });
              });
              $("body").delegate("#onFax", "click", function() {
                  $('#appDoctMob').val();
                    $.ajax({
                      url: '<?php echo base_url(); ?>Appointment/sendFax',
                      type: "POST",
                      data: {
                        'sendAppointmentId':$('#appointmentIDSaved').val(),
                        /***Appointment id ***/
                        'username':"<?php echo $this->session->userdata('logged_in')['name']; ?>",
                        'mobile' : $('#appDoctMob').val(),
                        'dr_appointment_date_id' : $('#app_date').val(),
                        'dr_appointment_time_id' : $('#timepicker1').val(),
                        'doctor_name' : $('#appDoctName').val(),
                        'fax_num'	: $('#appDoctFax').val(),
                      },
                      dataType: "json", 
                      success: function(data) {
                        $("#msg_show").html("<div class='alert alert-success'>" + data.status + "</div>");
                      }
                    });
                  });
            </script>
            <div class="modal-body">
              <div class="row">
                <table class="table table-hover table-bordered table-mailbox send-user-mail tab_medi ">
                  <tbody>

                  </tbody>
                </table>

              </div>
              <div class="modal-footer">
                <!-- <button type="button" class="btn btn-primary">Submit </button> -->
              </div>
            </div>
          </div>
        </div>
      </div>



      <div class="modal fade" id="faxPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h2>Select Appointment Slot</h2>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

            <script>
              jQuery.browser = {};
              (function() {
                jQuery.browser.msie = false;
                jQuery.browser.version = 0;
                if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                  jQuery.browser.msie = true;
                  jQuery.browser.version = RegExp.$1;
                }
              })();

              // Prevent jQuery from conflicting with ServiceNow
            </script>
            <div class="modal-body">

              <div class="row">
                <form id='add_appointment' enctype='multipart/form-data' method='post' class='add_appointment'>
                  <div id="success_msg"></div>
                  <table class="table table-hover table-bordered table-mailbox send-user-mail tab_medi ">
                    <tbody>
                      <!-- <a class="pull-right btn btn-warning btn-large" style="margin-right:40px" href="<?php echo site_url() ?>pdfs/save_pdf"><i class="fa fa-file-excel-o"></i> PDF Data</a> -->
                    </tbody>

                  </table>
                </form>
              </div>
              <div class="modal-footer">
              <button type='button' class='btn btn-primary' id='appointmentSubmit'>Submit </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
<!-- <script src="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script> -->
<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script src="https://cdn.jsdelivr.net/bootstrap.timepicker/0.2.6/js/bootstrap-timepicker.min.js"></script>


</script>
<script>
  $(document).ready(function() {
    $(".modal").on("hidden.bs.modal", function(){
          $("#appointmentSubmit").css('display','block');
    //     $('#faxPopup').on('shown.bs.modal', function (e) {
    //       var today = new Date();

    // $('#datetimepicker').datetimepicker({

    //   ignoreReadonly: true,

    //   format: 'YYYY-MM-DD',

    //   minDate: today

    // });
    // $("#datetimepicker3").datetimepicker({
    //   format: "LT",

    // });
    //       });




    $(".scheduleAppointmentOpen").click(function() {

      $('#faxPopup').modal('show');

      var id = $(this).attr('data-id');
      //  alert(id);
      $.ajax({
        url: '<?php echo base_url(); ?>Appointment/getResult',
        type: "POST",
        data: {
          'doctorID': id
        },
        dataType: "json",
        success: function(data) {
          // console.log(data);
          var len = data.length;
          for (var i = 0; i < len; i++) {
            var doctor_id = data[i].doctor_id;
            var doctor_name = data[i].doctor_name;
            var doctor_name= data[i].other_doctor_name;
            // alert(doctor_name);
            var doctor_address = data[i].doctor_address;
            var doctor_email = data[i].doctor_email;
            var doctor_mob_no = data[i].doctor_mob_no;
            var fax_num = data[i].fax_num;
            var portal = data[i].portal;
            if (doctor_mob_no != '' && fax_num != '' || portal != '') {
              var tr_str = "<tr><input type='hidden' value=" + doctor_id + " id='doctor_id'>" +
                "<td align='center'>Doctor Name:</td><td colspan='2'><input type='text' id='appDoctName' name='appDoctName' value=" + doctor_name + " readonly></td></tr>" +
                "<tr><td align='center'>Address:</td><td colspan='2'><input type='text' id='appDoctAdd' name='appDoctAdd' value=" + doctor_address + " readonly></td></tr>" +
                "<tr><td align='center'>Email:</td><td colspan='2'><input type='text' id='appDoctEmail' name='appDoctEmail' value=" + doctor_email + " readonly></td></tr>" +
                "<tr><td align='center'>Phone Number:</td><td colspan='2'><input type='text' id='appDoctMob' name='appDoctMob' value=" + doctor_mob_no + " readonly>";
                //"<input type='hidden' id='callUserName' value=" + doctor_mob_no +">"+
              
					      //<a href='#' id='onCall' class='dynamicButton'>Call</a>
                tr_str += "</td></tr>";

                tr_str += "<tr><td align='center'>Fax:</td><td colspan='2'><input type='text' id='appDoctFax' name='appDoctFax' value=" + fax_num + "></td>";
            
                tr_str += "</tr>";
                tr_str += "<tr><td align='center'>Portal:</td><td colspan='2'><input type='text' id='appDoctportal' name='appDoctportal' value=" + portal + "></td></tr>" +
                "<tr><td align='center'>Date</td><td colspan='2'>  <input type='text' class='form-control' name='app_date' id='app_date' data-toggle='datepicker' required></td></tr>" +
                "<tr><td align='center'>Time</td><td colspan='2'><div class='input-group bootstrap-timepicker timepicker'>  <input name='app_time' id='timepicker1' type='text' class='form-control input-small' required>  <span class='input-group-addon'><i class='glyphicon glyphicon-time'></i></span></div></td></tr><tr><td>Set reminder</td><td colspan='2'><label for='yes'> <input type='radio' value='1' name='set_reminder' id='set_reminder'> Yes</td></tr><tr><td></td><td colspan='2'><label for='no'><input type='radio' value='0' name='set_reminder' id='set_reminder'> No</label></td></tr>"+
                "<tr><td colspan='3' style='border:0;'><div class='clearfix'></div><div id='callLog'></div><input type='hidden' id='appointmentIDSaved'></td></tr>";
                tr_str += "<tr id='callFaxSection' class='callFaxSection' style='display:none;width:100%' >";

                if(fax_num != ''){
                  tr_str += "<td colspan='2'><a href='#' id='onFax' class='dynamicButton'>Send Fax</a></td>";
                }
 if(doctor_mob_no != ''){
                  tr_str += "<td><input type='hidden' id='phoneNumber' value='"+doctor_mob_no+"'>";
                  tr_str += "<button id='call' class='dynamicButton'>Call</button>";
                  tr_str += "<button id='hangup' class='dynamicButton hide'>Hangup</button></td>";
                }

 if(portal != ''){
                  tr_str += "<td><input type='text' id='appDoctportal' name='appDoctportal' value=" + portal + "></td>";
               
                }
                tr_str += "</tr></form>";
              $("#faxPopup tbody").html(tr_str);

            }
          }

        }
      });
    });


  });

  function goDoSomething(identifier) {
    var idOnclickFax = $(identifier).data('id');
    $("#dataid").val(idOnclickFax);
  }
  $(document).on("click", ".FaxContact", function() {
    var id = $(this).data('id');
    $.ajax({
      url: '<?php echo base_url(); ?>Appointment/getResult',
      type: "POST",
      data: {
        'doctorID': id
      },
      dataType: "json",
      success: function(data) {
        console.log(data);
        var len = data.length;
        for (var i = 0; i < len; i++) {
          var doctor_id = data[i].doctor_id;
          var doctor_name = data[i].doctor_name;
          var doctor_address = data[i].doctor_address;
          // alert(doctor_address);
          var doctor_email = data[i].doctor_email;
          var doctor_mob_no = data[i].doctor_mob_no;
          var fax_num = data[i].fax_num;
          var portal = data[i].portal;
          var tr_str = "<tr><input type='hidden' value=" + doctor_id + " id='doctor_id'>" +
            "<td align='center'>Doctor Name:</td><td><input type='text' id='appDoctName' name='appDoctName' value=" + doctor_name + " readonly></td></tr>" +
            "<tr><td align='center'>Address:</td><td><input type='text' id='appDoctAdd' name='appDoctAdd' value=" + doctor_address + " readonly></td></tr>" +
            "<tr><td align='center'>Email:</td><td><input type='text' id='appDoctEmail' name='appDoctEmail' value=" + doctor_email + " readonly></td></tr>" +
            "<tr><td align='center'>Phone Number:</td><td><input type='text' id='appDoctMob' name='appDoctMob' value=" + doctor_mob_no + " readonly></td></tr>" +
            "<tr><td align='center'>Fax:</td><td><input type='text' id='appDoctFax' name='appDoctFax' value=" + fax_num + " readonly></td></tr>" +
            "<tr><td align='center'>Portal:</td><td><input type='text' id='appDoctportal' name='appDoctportal' value=" + portal + " readonly></td></tr>" +
            "<tr><td align='center'>Date</td><td>  <input type='text' class='form-control' name='app_date' id='app_date' data-toggle='datepicker' required></td></tr>" +
            "<tr><td align='center'>Time</td><td><div class='input-group bootstrap-timepicker timepicker'>  <input name='app_time' id='timepicker1' type='text' class='form-control input-small' required>  <span class='input-group-addon'><i class='glyphicon glyphicon-time'></i></span></div></td></tr><tr><td>Set reminder</td><td><label for='yes'> <input type='radio' value='1' name='set_reminder' id='set_reminder'> Yes</td></tr><tr><td></td><td><label for='no'><input type='radio' value='0' name='set_reminder' id='set_reminder'> No</label></td></tr></form>";
          $("#faxPopup tbody").html(tr_str);


        }

      }
    });
  });
  $("#appointmentSubmit").click(function() {

    var doctor_id = $("#doctor_id").val();
    // alert(doctorID);
    var appDoctName = $("#appDoctName").val();
    var appDoctAdd = $("#appDoctAdd").val();
    var appDoctEmail = $("#appDoctEmail").val();
    var appDoctMob = $("#appDoctMob").attr('data-value');
    var appDoctportal = $("#appDoctportal").val();
    var app_date = $("#app_date").val();
    var app_time = $("#timepicker1").val();
    var set_reminder = $("#set_reminder:checked").val();
    // alert(set_reminder);
    var app_date=$('#app_date').val();
    if(app_date == ''){
      $('.errorDate').css('display','block');
    }
    else {
      $('.errorDate').css('display','none');
    }
    if(app_time == ''){
      $('.errorTime').css('display','block');
    }
    else {
      $('.errorTime').css('display','none');
    }
    if ($("#set_reminder").is(':checked')) {
      $('.errorRemind').css('display','none');
    } else {
      $('.errorRemind').css('display','block');
        return false;
    }

  $.ajax({
      url: "<?php echo base_url(); ?>Appointment/add_appointment",
      type: "POST",
      data: {
        'doctor_id': doctor_id,
        'appDoctName': appDoctName,
        'appDoctAdd': appDoctAdd,
        'appDoctEmail': appDoctEmail,
        'appDoctMob': appDoctMob,
        'appDoctportal': appDoctportal,
        'app_date': app_date,
        'app_time': app_time,
        'set_reminder': set_reminder



      },
      dataType: "json",
      success: function(data) {
        if (data ) {
          $("#success_msg").html("<div class='alert alert-success'>" + data.status + "</div>");
      
      $("#appointmentIDSaved").val(data.appointmentId);

          // jQuery("#callFaxSection").show();

          $("#callFaxSection").css("display", "block");
          $("#appointmentSubmit").css("display","none");
          
          // setTimeout(function() {
          //   $('#success_message').fadeOut("milliseconds");

          // }, 2000);
          // location.reload();

        } else if (response == 2) {
          $("#success_msg").html('<div class="alert alert-success">Appointment alredy schedule. Please select another date and time.</div>');


          // setTimeout(function() {
          //   $('#success_message').fadeOut("milliseconds");

          // }, 2000);
          // location.reload();

        }

      }
    });

    

  });
</script>

<script type="text/javascript">
  var switches = document.querySelectorAll('.switch');

  switches.forEach(function(item) {
    item.addEventListener('change', function() {
      var checkbox = this.querySelector('input'),
        data = checkbox.dataset;

      checkbox.value = checkbox.checked ? data.on : data.off;
    });
  });
  $(document).on('click', '.updateStatus', function() {
    var update_status = $(this).attr('data-btn');
    var update_st = $(this).attr('data-st');
    //alert(update_status+'-'+update_st);
    $.ajax({
      url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
      type: "POST",
      data: {
        'btn': update_status
      },
      dataType: "json",
      success: function(data) {
        $("#msg_show").html("<div class='alert alert-success'>" + data.status + "</div>");
      }
    });
  });
  $(document).on('click', '.cancelApp', function() {
    var cancelAppButton = $(this).attr('data-value');
    $.ajax({
      url: '<?php echo base_url(); ?>Appointment/change_status',
      type: "POST",
      data: {
        'cancelAppButton': cancelAppButton
      },
      dataType: "json",
      success: function(data) {
        $("#msg_show").html("<div class='alert alert-success'>" + data.status + "</div>");
      }
    });
  });
</script>
<script>
  $(document).ready(function() {

    $("#doctorList").DataTable();
    $('#sampleTable').DataTable();
    $("#AddNewdoctor").validate({
      rules: {
        doctor_name: "required",
        doctor_address: "required",
        doctor_email: "required",
        doctor_phone_number: "required",
        doctor_fax: "required",
        doctor_portal: "required",
        speciality: "required",



      },
      messages: {
        doctor_name: "Please enter Doctor name",
        doctor_address: "Please enter Doctor address.",
        doctor_email: "Please enter doctor email",
        doctor_phone_number: "Please enter Phone number.",
        doctor_fax: "Please enter Fax number",
        doctor_portal: "Please enter portal",
        speciality: "Select Speciality",


      },
      submitHandler: function(form) {
        $.ajax({
          url: '<?php echo base_url(); ?>Appointment/add_doctor',
          type: 'post',
          data: $(form).serialize(),
          success: function(response) {
            if (response == 1) {
              $('#success_message').fadeIn().html('<div class="alert alert-success message"><button type="button" class="close" data-dismiss="alert">x</button>Doctor added successfully.</div>');

            }
          }

        });

      }
    });
  });




  function scrollMore()

  {
    if ($(window).scrollTop() == ($(document).height() - $(window).height())) {

      //$(window).unbind("scroll");

      var records = '<?php echo $count_total; ?>';
      var offset = $('[id^="myData_"]').length;
      //alert(records);

      if (records != offset) {
        $('#main-cntdiv').append('<img id="loader_img" style="width:50px" src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif" />');

        loadMoreData(offset);
      }
    }
  }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/new_user_dashboard/js/sinch.min.js"></script>
<script src="<?php echo base_url(); ?>assets/new_user_dashboard/js/sinchaudio.js"></script>
