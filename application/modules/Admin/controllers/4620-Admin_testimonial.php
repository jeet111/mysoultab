<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_testimonial extends MX_Controller {

   private $staff_permission = array();
   private $access_permission = true;
   public function __construct(){

      parent:: __construct();

      $this->load->library('session');
      $this->load->library('form_validation');
      $this->load->model('login_model');
      $this->load->model('Common_model');
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));
      $this->load->library('upload');

      $user_id = $this->session->userdata('logged_ins')['id'];
      if(empty($user_id)) {
         redirect('admin/login');
      }

      if ($this->session->userdata('logged_ins')['user_role'] == 5) {
         $uid = $this->session->userdata('logged_ins')['id'];
         $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
         if (!empty($permission_list)) 
         {
            foreach ($permission_list as $k => $v) 
            {
               $this->staff_permission[$v['permission_id']] = $v;
            }
         }
      }

   }

   public function permission_access($permission_id=18)
   {
      if ($this->session->userdata('logged_ins')['user_role'] != 1) 
      {
         if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
         {
            $this->access_permission = false;
         }
      }
   }

   public function testimonial_list() {
      $data['menuactive'] = $this->uri->segment(2);
      
      $data['testimonial_listing'] = $this->login_model->getRows('testimonial');
      $data['staff_permission'] = $this->staff_permission;

      $this->permission_access(18);
      if ($this->access_permission) {
         $this->template->set('title', 'Admin Panel');
         $this->template->load('admin_dashboard_layout', 'contents', 'testimonial_listing', $data);
      } else {
         $this->template->set('title', 'Admin Panel - Staff');
         $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
      }
   }

   public function add_testimonial(){

      if (isset($_POST['btnSubmit'])) {
         $name = $this->input->post('name');
         $designation = $this->input->post('designation');
         $description = $this->input->post('description');
         $testimonial_id = $this->input->post('testimonial_id');

         $config['file_name'] = $_FILES['image']['name'];
         $config['upload_path'] = 'uploads/';
         $config['allowed_types'] = 'gif|jpg|png|jpeg';
         $config['max_size'] = "10000";
         $config['max_width'] = "0";
         $config['max_height'] = "0";
         $config['remove_spaces'] = true;

         $this->load->library('upload', $config);
         $this->upload->initialize($config);

         if (!$this->upload->do_upload('image')) 
         {
            $error = array('error' => $this->upload->display_errors());
            //echo "<pre>";print_r($this->upload->display_errors());
            $file_name = '';
         } 
         else 
         {
            $this->upload->data();
            $file_name = $this->upload->data()['file_name'];
         }

         if (!empty($testimonial_id)) {
            
            if (!empty($file_name)) {
               $post_data = array(
                              'name' => $name,
                              'designation' => $designation,
                              'image' => $file_name,
                              'description' => $description,
                              'updated_at' => date('Y-m-d H:i:s')
                           );
            } else {
               $post_data = array(
                              'name' => $name,
                              'designation' => $designation,
                              'description' => $description,
                              'updated_at' => date('Y-m-d H:i:s')
                           );
            }
            
            $where_condition = array('id'=>$testimonial_id);
            $resp = $this->login_model->updateRecords('testimonial', $post_data, $where_condition);

            if ($resp) {
               $data['success'] = 'Testimonial updated successfully!';
            } else {
               $data['error'] = 'Some internal issue occured.';
            }

         } else {
            if (!empty($file_name)) {
               $post_data = array(
                              'name' => $name,
                              'designation' => $designation,
                              'image' => $file_name,
                              'description' => $description,
                              'status' => 1,
                              'created_at' => date('Y-m-d H:i:s'),
                              'updated_at' => date('Y-m-d H:i:s')
                           );
            } else {
               $post_data = array(
                              'name' => $name,
                              'designation' => $designation,
                              'description' => $description,
                              'status' => 1,
                              'created_at' => date('Y-m-d H:i:s'),
                              'updated_at' => date('Y-m-d H:i:s')
                           );
            }
            
            $resp = $this->login_model->insertrecords('testimonial',$post_data);

            if ($resp) {
               $data['success'] = 'Testimonial added successfully!';
            } else {
               $data['error'] = 'Some internal issue occured.';
            }

         }
      }

      $testimonial_id = $this->uri->segment(3);
      $data['testimonial_detail'] = array();
      if (!empty($testimonial_id)) {
         $data['testimonial_detail'] = $this->login_model->getSingleRecordById('testimonial',array('id'=>$testimonial_id));
      }
      
      $data['staff_permission'] = $this->staff_permission;
      
      $this->permission_access(18);
      if ($this->access_permission) {
         $this->template->set('title', 'Admin Panel');
         $this->template->load('admin_dashboard_layout', 'contents' , 'add_testimonial', $data);
      } else {
         $this->template->set('title', 'Admin Panel - Staff');
         $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
      }
   }

   public function delete_testimonial(){
      $testimonial_id = $this->input->post('testimonial_id');
      $resp = $this->login_model->deleteRecords('testimonial',array('id'=>$testimonial_id));
      if ($resp) {
         echo 1;
      } else {
         echo 0;
      }
   }
}