<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_faq extends MX_Controller {

   private $staff_permission = array();
   private $access_permission = true;
   public function __construct(){

      parent:: __construct();

      $this->load->library('session');
      $this->load->library('form_validation');
      $this->load->model('login_model');
      $this->load->model('Common_model');
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));

      $user_id = $this->session->userdata('logged_ins')['id'];
      if(empty($user_id)) {
         redirect('admin/login');
      }

      if ($this->session->userdata('logged_ins')['user_role'] == 5) {
         $uid = $this->session->userdata('logged_ins')['id'];
         $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
         if (!empty($permission_list)) 
         {
            foreach ($permission_list as $k => $v) 
            {
               $this->staff_permission[$v['permission_id']] = $v;
            }
         }
      }

   }

   public function permission_access($permission_id=17)
   {
      if ($this->session->userdata('logged_ins')['user_role'] != 1) 
      {
         if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
         {
            $this->access_permission = false;
         }
      }
   }

   public function faq_list() {
      $data['menuactive'] = $this->uri->segment(2);
      
      $data['faq_list'] = $this->login_model->getRows('faq');
      $data['staff_permission'] = $this->staff_permission;

      $this->permission_access(17);
      if ($this->access_permission) {
         $this->template->set('title', 'Admin Panel');
         $this->template->load('admin_dashboard_layout', 'contents', 'faq_listing', $data);
      } else {
         $this->template->set('title', 'Admin Panel - Staff');
         $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
      }
   }

   public function add_faq(){
      $data['menuactive'] = $this->uri->segment(2);
      if (isset($_POST['btnSubmit'])) {
         $question = $this->input->post('question');
         $answer = $this->input->post('answer');
         $faq_id = $this->input->post('faq_id');

         if (!empty($faq_id)) {
            $post_data = array(
               'question' => $question,
               'answer' => $answer,
               'updated_at' => date('Y-m-d H:i:s')
            );
            $where_condition = array('id'=>$faq_id);
            $resp = $this->login_model->updateRecords('faq', $post_data, $where_condition);

            if ($resp) {
               $data['success'] = 'FAQ updated successfully!';
            } else {
               $data['error'] = 'Some internal issue occured.';
            }

         } else {
            $post_data = array(
               'question' => $question,
               'answer' => $answer,
               'status' => 1,
               'created_at' => date('Y-m-d H:i:s'),
               'updated_at' => date('Y-m-d H:i:s')
            );
            $resp = $this->login_model->insertrecords('faq',$post_data);

            if ($resp) {
               $data['success'] = 'FAQ added successfully!';
            } else {
               $data['error'] = 'Some internal issue occured.';
            }

         }
      }

      $faq_id = $this->uri->segment(3);
      $data['faq_detail'] = array();
      if (!empty($faq_id)) {
         $data['faq_detail'] = $this->login_model->getSingleRecordById('faq',array('id'=>$faq_id));
      }
      
      $data['staff_permission'] = $this->staff_permission;
      
      $this->permission_access(17);
      if ($this->access_permission) {
         $this->template->set('title', 'Admin Panel');
         $this->template->load('admin_dashboard_layout', 'contents', 'add_faq', $data);
      } else {
         $this->template->set('title', 'Admin Panel - Staff');
         $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
      }
   }

   public function delete_faq(){
      $faq_id = $this->input->post('faq_id');
      $resp = $this->login_model->deleteRecords('faq',array('id'=>$faq_id));
      if ($resp) {
         echo 1;
      } else {
         echo 0;
      }
   }
}