<style>
  table.soc {
    width: 100%;
  }
  .row.yoga_filter h3 {
    color: #000;
    margin: 0 0 19px 0;
    font-size: 24px;
    border-bottom: 1px solid #ddd;
    padding-bottom: 10px;
    padding-top: 0;
}
.table td {
  overflow: hidden; /* this is what fixes the expansion */
  text-overflow: ellipsis; /* not supported in all browsers, but I accepted the tradeoff */
  white-space: nowrap;
}
  body.skin-blue.pace-done.overflow {
    overflow-x: hidden !important;
    position: fixed;
    overflow-y: scroll;
}
.text_heading {margin-bottom: 20px;
    border-bottom: none;
}
  #fade {
    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: black;
    z-index: 1001;
    -moz-opacity: 0.8;
    opacity: .80;
    filter: alpha(opacity=80);
  }



#light {
    display: none;
    position: fixed;
    z-index: 1002;
    /* overflow: visible; */
    /* transform: translate(-50%, -16%); */
    /* overflow: hidden !important; */
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    margin: 0;
    bottom: 0;
    right: 0;

}
div#light iframe {
    position: absolute;
    top: 50%;
    left: 50%;  z-index: 1002;
    transform: translate(-50%,-50%);
}
  #boxclose {
    float: right;
    cursor: pointer;
    color: #fff;
    border: 1px solid #AEAEAE;
    border-radius: 3px;
    background: #222222;
    font-size: 31px;
    font-weight: bold;
    display: inline-block;
    line-height: 0px;
    padding: 11px 3px;
    position: absolute;
    right: 50px;
    top: 100px;
    z-index: 1002;
    opacity: 0.9;
  }

  .boxclose:before {
    content: "×";
  }

  #fade:hover~#boxclose {
    display: none;
  }

  .row.yoga_filter .col-md-3 li.active {
    background: #428bca;
    color: #fff;
  }

  div#video_container iframe {
    margin-right: 10px;
    margin-bottom: 20px;
  }
 
  .row.yoga_filter .col-md-3 ul {
    padding: 0;
    margin: 0;
    display: inline-block;
  }
  div#sampleTable_length {
    text-align: left;
}
.col-sm-5 {
    float: left;
    text-align: left;
}
.col-sm-7 {
    text-align: right;
    margin: 0;
}
.pagination {
    margin: 0 !important;
    border-radius: 4px;
}
table#sampleTable th {border-bottom: 1px solid #eee;
    border-top: 1px solid #eee;
    text-transform: capitalize;
    line-height: normal;
    font-size: 13px;
    margin: 0 !important;
    padding: 9px 0 7px 6px;
    font-weight: 600;
}
table#sampleTable td img {
    height: 50px;
    width: 100%;
}
.col-md-3 ul {
    text-align: left;
}
div#sampleTable_filter {
    text-align: right;
}
  div#video_container {
    float: left;
    width: 100%;
    margin: 20px 0 0 20px;
  }


  .row.yoga_filter h3 {
    padding-left: 13px;
    font-size: 22px;
    padding: 15px 20px;
    border-left: 4px solid #fff;
    text-align: center;
    border-right: 4px solid #fff;
  }

  input#filterserach {
    background: #428bca;
    border: none;
    color: #fff;
    font-size: 17px;
    padding: 10px 36px;
    float: right;
  }
  .btn.btn-primary {
    margin: 0px 0 !important;
    max-width: 139px !important;
}
td#yoga_url a {
    color: #000;
    border-bottom: 1px solid;
    padding-bottom: 1px !important;
}
.row.yoga_filter .col-md-3 li {
    display: inline-block;
    border: 1px solid #000;
    padding: 6px 0;
    box-sizing: border-box;
    max-width: 162px;
    width: 86px;
    font-size: 14px;
    text-align: center;
    color: #000;
    margin-bottom: 10px;
}

  .row.yoga_filter {
    margin-bottom: 20px;
  }

  .row.yoga_filter .col-md-3 li:hover {
    cursor: pointer;
    background: #428bca;
    color: #fff;
  }

  .row.yoga_filter label {
    float: left;
    width: 100%;color:#000;
  }
</style>
<div class="text_heading">


	<div id="msg_show"></div>

    <h3 class="text-center"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Yoga</h3>
    <div class="tooltip-2">
    <h2>Display call button on Mobile App

          <?php
			  if(!empty($btnsetingArray)){
          foreach ($btnsetingArray as $res) {
            if ($res->settings == 1) { ?>
              <label class="switch ">
                <input type="checkbox" checked data-btn="<?php echo 'Yoga' ?>" class="updateStatus">
                <span class="slider round"></span>
              </label>
    <?php } else { ?>
      <label class="switch space">
        <input type="checkbox" data-btn="<?php echo 'Yoga' ?>" class="updateStatus">
        <span class="slider round"></span>
      </label>


    <?php } ?>
  <?php } } ?>
  </h2>
<button id="serach_video" class="btn btn-primary">Search Video</button>
	</div>

	</div>

  <section class="content photo-list" id="section_search">
    <div class="photo-listMain reminder_listMain">
      <form name="search_video_result" method="post" id="search_video_result" class="search_video_result">



        <?php
        // $i = 1;

        if (!empty($YogaSearchUser)) {
        ?>
          <div class="row yoga_filter">
            <h3>Search Videos</h3>
          
            <div class="col-md-3">

            <label>Help With</label>
              <ul class="body_part1">
                <?php foreach ($getAllbodypart as $yoga) {    ?>
                  <?php if (!empty($yoga->body_part)) { ?>

                    <li data-value="<?php echo  $yoga->body_part; ?>" data-btn="<?php echo  $yoga->body_part; ?>" name="user_ids[]"><?php echo $yoga->body_part; ?></li>

                <?php }
                }      ?>
              </ul>
            </div>
            <div class="col-md-3">

              <label>Yoga Style</label>
              <ul class="yoga_style1">
                <?php foreach ($getAllyogastyle as $yoga) {

                ?>
                  <?php if (!empty($yoga->yoga_style)) { ?>

                    <li data-value="<?php echo  $yoga->yoga_style; ?>" data-btn="<?php echo  $yoga->yoga_style; ?>" name="user_ids[]"><?php echo $yoga->yoga_style; ?></li>

                <?php }
                }  ?>
              </ul>
            </div>
            <div class="col-md-3">
              <label>Yoga Duration</label>
              <ul class="yoga_duration1">
                <?php foreach ($getAllyogaduration as  $yoga) {
                  // _dx($yoga);
                ?>
                  <?php if (!empty($yoga->yoga_duration)) { ?>

                    <li data-value="<?php echo  $yoga->yoga_duration; ?>" data-btn="<?php echo $yoga->yoga_duration; ?>" name="user_ids[]"><?php echo $yoga->yoga_duration; ?></li>


                <?php }
                }    ?>
              </ul>
            </div>
            <div class="col-md-3">
              <label>Yoga Type</label>
              <ul class="yoga_type1">
                <?php foreach ($getAllyogatype as $yoga) {

                ?>
                  <?php if (!empty($yoga->yoga_type)) { ?>

                    <li data-value="<?php echo  $yoga->yoga_type; ?>" data-btn="<?php echo  $yoga->yoga_type; ?>" name="user_ids[]"><?php echo $yoga->yoga_type; ?></li>

                <?php }
                }    ?>
              </ul>
            </div>
          </div>
          <div class="row yoga_filter">
          
            <div class="col-md-3">
              <label>Yoga Pose</label>
              <ul class="yoga_pose1">
                <?php foreach ($getAllyogapose as $yoga) {

                ?>
                  <?php if (!empty($yoga->yoga_pose)) { ?>
                    <li data-value="<?php echo  $yoga->yoga_pose; ?>" data-btn="<?php echo  $yoga->yoga_pose; ?>" name="user_ids[]"><?php echo $yoga->yoga_pose; ?></li>

                <?php }
                } ?>
              </ul>
            </div>

            <div class="col-md-3">
              <label>Yoga Level</label>
              <ul class="yoga_lavel1">
                <?php foreach ($getAllyogalevel as $yoga) { ?>
                  <?php if (!empty($yoga->yoga_level)) { ?>

                    <li data-value="<?php echo  $yoga->yoga_level; ?>" data-btn="<?php echo  $yoga->yoga_level; ?>" name="user_ids[]"><?php echo $yoga->yoga_level; ?></li>

                <?php }
                } ?>
              </ul>
            </div>
          <?php                  } ?>
          <div class="col-md-12">
            <input type="submit" name="filterserach" id="filterserach" class="filterserach">
          </div>
          <div id="video_container">
            <!-- <img src="" id="loading-image">
          $('#loading-image').show(); -->
            <div id="loader">
              <img src="<?php echo base_url() ?>uploads/giphy.gif">
            </div>

          </div>
          </div>

      </form>
    </div>
  </section>

  <div id="expire_msg"></div>


  <section class="content photo-list">
    <div class="photo-listMain reminder_listMain">
      <table class="table table-hover tab_comn" id="sampleTable">
        <thead>
          <tr>
            <!-- <th>S no.</th> -->
            <th> Title</th>
            <th> Description</th>
            <th> Image</th>
            <th> Duration</th>
            <th>Help With</th>
            <th> Style</th>
            <th> Pose</th>
            <th>Video</th>
            <th> Type</th>
            <th> Level</th>
            <th> Action</th>
          </tr>
        </thead>
 
        <tbody>
          <?php
          $i = 1;
          if (!empty($YogaDataUser)) {
            foreach ($YogaDataUser as $yoga) {
              // _dx($yoga);
          ?>
              <!-- <tr id="rowID" data-btn="<?php echo $yoga->yoga_id; ?>"> -->
                <!-- <td><?php  echo $i++; ?></td> -->
              <tr id="rowID" data-btn="<?php echo $yoga->yoga_id; ?>">
                <td scope="row" data-label="Title" data-btn="<?php echo  $yoga->yoga_title; ?>" id="yoga_title"><?php echo $yoga->yoga_title; ?></td>
                <td data-label="Description" data-btn="<?php echo  $yoga->yoga_desc; ?>" id="yoga_desc"><?php echo $yoga->yoga_desc; ?></td>
                <td data-label="Image" data-btn="<?php echo base_url('uploads/yoga_videos/image/' . $yoga->yoga_image) ?>" id="yoga_img"> <img src="<?php echo base_url('uploads/yoga_videos/image/' . $yoga->yoga_image) ?>" height="50px" width="100px"> </td>
                <td data-label="Duration" data-btn="<?php echo  $yoga->yoga_duration; ?>" id="yoga_duration"><?php echo $yoga->yoga_duration; ?></td>
                <td data-label="Help With" data-btn="<?php echo  $yoga->body_part; ?>" id="body_part"><?php echo $yoga->body_part; ?></td>
                <td data-label="Style" data-btn="<?php echo  $yoga->yoga_style; ?>" id="yoga_style"><?php echo $yoga->yoga_style; ?></td>
                <td data-label="Pose" data-btn="<?php echo  $yoga->yoga_pose; ?>" id="yoga_pose"><?php echo $yoga->yoga_pose; ?></td>
                <td  data-label="Video" data-btn="<?php echo  $yoga->yoga_url; ?>" id="yoga_url">
            

<?php if(!empty( $yoga->yoga_url)){ ?>
   <div>

   <a href="<?php echo $yoga->yoga_url;   ?>" class="video-youtube">Watch video</a>
 </div>
<?php }else{ ?>
  <a href="<?php echo base_url('uploads/yoga_videos/'). $yoga->yoga_video;   ?>" class="video-youtube">Watch video</a>

<?php } ?>              

               
                </td>
                <!-- <td data-btn="<?php echo  $yoga->yoga_url; ?>" id="yoga_url"><iframe height="50" width="50" src="<?php echo $yoga->yoga_url; ?>"></iframe></td> -->
                <td data-label="Type" data-btn="<?php echo  $yoga->yoga_type; ?>" id="yoga_type"><?php echo $yoga->yoga_type; ?></td>
                <td data-label="Level" data-btn="<?php echo  $yoga->yoga_level; ?>" id="yoga_level"><?php echo $yoga->yoga_level; ?></td>
                <!-- <td><img src="<?php echo base_url() ?>uploads/yoga/fav_icon.png" class="add_fav_icon" data-btn="1"></td> -->
                <!--<td class="status"><i onclick="myFunction(this)" class="fa fa-heart-o" data-btn="1" id="<?php echo $yoga->yoga_id; ?>"></i> -->
             
                <td data-label="Action" class=""><i class="fa fa-heart feedbackIcon" data-feedbackValue="1" id="<?php echo $yoga->yoga_id; ?>"></i>
</td>
              </tr>
          <?php }
          } ?>
        </tbody>
      </table>
    </div>
  </section>
  
<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script> -->

<script type="text/javascript">
  $(document).ready(function() {
    $('.video-youtube').magnificPopup({
        type: 'iframe',
        iframe: {
            patterns: {
                youtube: {
                    index: 'youtube.com/',
                    id: function (url) { return url },
                    src: '%id%'
                },
                vimeo: {
                    index: 'vimeo.com/',
                    id: function (url) { return url },
                    src: '%id%'
                }
            }
        }
    });
});
$(document).ready( function () {

jQuery("#section_search").hide();
jQuery("#serach_video").click(function(){
jQuery("#section_search").slideToggle();
});
    $('#sampleTable').DataTable({

      responsive: true

    });
} );
  window.document.onkeydown = function(e) {
    if (!e) {
      e = event;
    }
    if (e.keyCode == 27) {
      lightbox_close();
    }
  }

  function lightbox_open() {
    $('body').addClass('overflow');
    var lightBoxVideo = document.getElementsByClassName("VisaChipCardVideo");
    window.scrollTo(0, 0);
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    // lightBoxVideo.play();
  }

  function lightbox_close() {
    $('body').removeClass('overflow');
    var lightBoxVideo = document.getElementsByClassName("VisaChipCardVideo");
    document.getElementById('light').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
    // lightBoxVideo.pause();
  }
  $("#loader").hide();
  $(document).on('click', '.updateStatus', function() {
    var update_status = $(this).attr('data-btn');
    var update_st = $(this).attr('data-st');
    $.ajax({
      url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
      type: "POST",
      data: {
        'btn': update_status
      },
      dataType: "json",
      success: function(data) {
          $("#msg_show").html("<div class='alert alert-success'>" + data.status + "</div>");
      
      }
    });
  });

  // $('.feedbackIcon').click(function (){

$(document).on('click', '.feedbackIcon', function(e) {
  var favorite = $(this).attr('data-feedbackValue');

  var yoga_id = $(this).attr('id');
  var test = $(this);
var favoriteResult = (favorite == "0") ? "1" : "0";

  $.ajax({
    url: '<?php echo base_url(); ?>Pages/pages/add_fav',
    type: "POST",
    data: {
      'favorite': favoriteResult,
      'yoga_id': yoga_id
    },
    
    dataType: "json",
    success: function(response) {
  console.log(response['list']['favorite']);
if(response['list']['favorite'] =='0'){
  test.addClass('fa-heart-o');
  test.attr('data-feedbackValue', '0');
 

}else{
  test.removeClass('fa-heart-o');
        test.addClass('fa-heart');
        test.attr('data-feedbackValue', '1');
}
$("#msg_show").html("<div class='alert alert-success'>Successfully updated</div>");
      // if (test.hasClass("fa-heart-o")){
      //   test.removeClass('fa-heart-o');
      //   test.addClass('fa-heart');
      //   test.attr('data-feedbackValue', '1');

      // }else{
      //   test.addClass('fa-heart-o');
      //   test.attr('data-feedbackValue', '0');
      // }
   }
    // }
  });
});



  $(".row.yoga_filter .col-md-3 li").click(function() {
    jQuery(this).toggleClass('active');



  });

 



  $('#filterserach').click(function(e) {
    e.preventDefault();
    var help_with_array = [];
    $('.help_with1 > li').each(function() {
      if ($(this).hasClass('active')) {
        help_with_array.push($(this).text());
      }
    });
    var body_parts_array = [];
    $('.body_part1 > li').each(function() {
      if ($(this).hasClass('active')) {
        body_parts_array.push($(this).text());
      }
      // return body_parts_array;
    });
    var yoga_style_array = [];
    $('.yoga_style1 > li').each(function() {
      if ($(this).hasClass('active')) {
        yoga_style_array.push($(this).text());
      }
      // return yoga_style_array;
    });

    var yoga_duration_array = [];
    $('.yoga_duration1 > li').each(function() {
      if ($(this).hasClass('active')) {
        yoga_duration_array.push($(this).text());
      }
      return yoga_duration_array;
    });

    var yoga_type_array = [];
    $('.yoga_type1 > li').each(function() {
      if ($(this).hasClass('active')) {
        yoga_type_array.push($(this).text());
      }
      // return yoga_type_array;
    });

    var yoga_pose_array = [];
    $('.yoga_pose1 > li').each(function() {
      if ($(this).hasClass('active')) {
        yoga_pose_array.push($(this).text());
      }
      // return yoga_pose_array;

    });

    var yoga_level_array = [];
    $('.yoga_lavel1 > li').each(function() {
      if ($(this).hasClass('active')) {
        yoga_level_array.push($(this).text());
      }
      //return yoga_level_array;

    });

    $.ajax({
      url: '<?php echo base_url(); ?>Pages/pages/search_result',
      type: "POST",
      dataType: "json",
      data: {
        help_with_array: help_with_array,
        body_parts_array: body_parts_array,
        yoga_style_array: yoga_style_array,
        yoga_duration_array: yoga_duration_array,
        yoga_type_array: yoga_type_array,
        yoga_pose_array: yoga_pose_array,
        yoga_level_array: yoga_level_array
      },
      beforeSend: function() {
        // Show image container
        $("#loader").show();
      },
      success: function(response) {
        var i;
        var len = response.length;
        // alert(len);    
        for (var i = 0; i < len; i++) {
          var yoga_url = response[i].yoga_url;
          var tr_str = "<iframe height='300px' width='300px' src=" + yoga_url + "></iframe>";
          $("#video_container").append(tr_str);
        }
      },
      complete: function(data) {
        // Hide image container
        $("#loader").hide();
      }
    });
  });
</script>
