<style>
.lgn label.error {
	font-size:12px;
}


</style>
<div class="container-fluid">
	<div class="dash-counter">
		<div class="Schedule_main_one">
			<div class="users-main">
				<?php
				echo "<h2>Add Doctor Specialty</h2>";
				?>

				<div class="btn_topBack">
					<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">
						Back
					</a>
				</div>

				<form class="form-horizontal" action="<?php echo site_url('admin/doctor-category/Add'); ?>" method="post" enctype="multipart/form-data" name="AddUsers" id="category-form" onsubmit="return check(this);">
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for="">Specialty Name:<span class="field_req">*</span></label>
							<div class="col-md-8">
								<input type="text" placeholder="Enter Category name" class="form-control" name="category_name" id="username" value="<?php echo set_value('category_name'); ?>">
								<?php echo form_error('category_name'); ?>

							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for="">Category Icon:<span class="field_req">*</span></label>
							<div class="col-md-8">
								<div class="input-group lgn">
									<span class="input-group-btn">
										<!-- <span class="btn btn-default btn-files"> -->
											<input type="file" id="profile_images" class="form-control" name="picture" onchange="loadFile(event)">
											<!-- </span> -->
										</span>
										<div id="flupmsg"></div>
									</div>
									<div>
										<span id="demo" style="color: red"></span>
									</div>
									<img id="img-upload" class="imgclas" src="<?php echo base_url().'uploads/articles/default_icon.png' ?>">

									<div class="bnt_sub">
										<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit" >

					<!-- <a class="cancel-btn btn" href="javascript:window.history.back()">
												Back
											</a> -->

										</div>
									</div>
								</div>
							</div>



						</form>
					</div>
				</div>
			</div>
		</div>

		<!--Added by 95 on 5-2-2019 For Doctor category for validation-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
		<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>
		<script>
			function loadFile (event) {
				var pcFile = $('input[type=file]').val().split('\\').pop();
				var pcExt     = pcFile.split('.').pop();
				var output = document.getElementById('img-upload');
				if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"
					|| pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){

					output.src = URL.createObjectURL(event.target.files[0]);
				$('#img-upload').show();
			}else{

				$('#img-upload').hide();
			}
		};
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$.validator.addMethod("regx", function(value, element, regexpr) {
				return regexpr.test(value);
			}, "<font size='2.9px'>Please upload valid extension jpg,jpeg,png file.</font>");
			$("#category-form").validate({
				rules: {
					category_name: "required",
					picture:{
						required:true,
						regx: /\.(jpe?g|png|gif|bmp)$/i,
					},
				},
				messages: {
					category_name: "Please enter category name.",
					picture:{
						required:"<font size='2.9px'>Please choose an icon.</font>",
						regx: "<font size='2.9px'>Please upload valid extension jpg,jpeg,png file.</font>",
					},
				}
			});
		});


	</script>