<style type="text/css">
    .text_heading {
        margin: 0 0 14px 0;
        border: 0;
    }
</style>


<div class=" text_heading">

    <h3><i class="fa fa-pencil-square-o"></i> Edit Appointment</h3>

    <div class="back_reminder"><a href="javascript:window.history.back()" class="btn btn-primary">Back</a></div>

</div>
<section class="content photo-list">

    <div class="photo-listMain reminder_listMain">
        <div id="image_msg"></div>
        <form class="appointmentEditForm" name="appointmentEditForm" method="post">
            <div class="col-xs-6 col-sm-6 col-md-6">

                <div class="bootstrap-timepicker">
                    <input type="hidden" value="<?php echo $EditAppointment[0]['dr_appointment_id']; ?>" id="appointment_id">
                    <div class="form-group new_selector">

                        <label>Doctor Name:<span class="red_star">*</span></label>

                        <select name="doctorName" id="doctorName" class="form-control">

                            <?php
                            foreach ($AllDoctorsList as $Dlist) {
                            ?>

                                <option value="<?php echo $Dlist->doctor_id; ?>" <?php if ($Dlist->doctor_id == $EditAppointment[0]['doctor_id']) {
                                                                                        echo 'selected';
                                                                                    } ?>><?php echo $Dlist->doctor_name; ?></option>

                            <?php } ?>

                        </select>

                    </div>

                </div>

            </div>

    <div class="col-sm-6 col-md-6">

        <div class="bootstrap-timepicker">

            <div class="form-group">

                <label>Date:<span class="red_star">*</span></label>

                <div class="input-group date costom_err" id="datetimepicker">



                    <input type='text' class="form-control" id="appointment_date" name="appointment_date" value="<?php if (isset($EditAppointment[0]['dr_appointment_date_id'])) {
                                                                                                                        echo $EditAppointment[0]['dr_appointment_date_id'];
                                                                                                                    } ?>" placeholder="Reminder Date" readonly />

                    <span class="input-group-addon">

                        <span class="glyphicon glyphicon-calendar"></span>

                    </span>


                </div>

            </div>

        </div>

    </div>
    <div class="col-sm-6 col-md-6">
        <div class="bootstrap-timepicker">
            <div class="form-group">
                <label>Time:<span class="red_star">*</span></label>

                <div class='input-group bootstrap-timepicker timepicker'> <input name='app_time' id='timepicker1' type='text' class='form-control input-small' value="<?php if (isset($EditAppointment[0]['dr_appointment_time_id'])) {
                                                                                                                                                                            echo $EditAppointment[0]['dr_appointment_time_id'];
                                                                                                                                                                        } ?>"> <span class='input-group-addon'><i class='glyphicon glyphicon-time'></i></span></div>
            </div>
            <td class="error"><?php echo form_error('reminder_time'); ?></td>
        </div>
    </div>
    <div class="col-sm-6 col-md-6">
        <div class="bootstrap-timepicker">
            <div class="form-group">
                <label>Reminder:<span class="red_star">*</span></label>

                <label for='yes'> <input type='radio' value='1' name='set_reminder' id='set_reminder'> Yes</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan='2'><label for='no'><input type='radio' value='0' name='set_reminder' id='set_reminder'> No</label>
            </div>

        </div>
    </div>
    <div class="col-sm-12 col-md-12">
        <input type="submit" name="submit" class="btn btn-primary" value="Submit" id="appointClick">

    </div>
    </form>
    </div>

</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

<script type="text/javascript">
    $(function() {

        var today = new Date();

        $('#datetimepicker').datetimepicker({

            ignoreReadonly: true,

            format: 'YYYY-MM-DD',

            minDate: today

        });

        $('#timepicker1').timepicker({
            showInputs: false
        });
    });
</script>

<script>
    $("#appointClick").click(function() {
 event.preventDefault();
        var appId = $("#appointment_id").val();
        var dName = $("#doctorName").val();
        var appDate = $("#appointment_date").val();
        var appTime = $("#timepicker1").val();
        var set_reminder = $("#set_reminder:checked").val();

        $.ajax({
            url: "<?php echo base_url(); ?>Appointment/edit_appointment1",
            type: "POST",
            data: {
                'appId':appId,
                'dName': dName,
                'appDate':appDate,
                'appTime':appTime,
                'set_reminder':set_reminder
                },

            dataType: "json",


            success: function(response) {


                if (response == 1) {

                    $("#loading_img").hide();

                    $("#sub").prop('disabled', true);

                    $("#image_msg").html('<div class="alert alert-success">Successfully Updated!<div>');

                    setTimeout(function() {

                        $("#image_msg").html("");

                        window.location.href = '<?php echo base_url(); ?>Appointment/appointment_list';

                    }, 1000);

                }
            }

        });
    });
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>