<style>
    .text_heading {
        border-bottom: 0;
    }

    .addNote a,
    input.btn.btn-primary {
        margin: 8px 0 !important;
    }
</style>
<div class=" text_heading">
    <h3 class="text-center"><i class="fa fa-pencil-square-o"></i> Add Reminder

        <div class="back_reminder addNote"><a href="<?php echo base_url(); ?>reminder_list" class="btn btn-danger">Back</a></div>

    </h3>
</div>


<section class="content photo-list">

    <div class="photo-listMain">

        <form id="reminder">



            <div id="success_message"></div>



            <?php /* ?>

                <?php if ($this->session->flashdata('success')) { ?>

                    <div class="alert alert-success message">

                        <button type="button" class="close" data-dismiss="alert">x</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                <?php } ?>			



                <?php */ ?>

            <div class="row">

                <div class="col-sm-4 col-md-4">

                    <div class="form-group">

                        <label for="">Title:<span class="red_star">*</span></label>

                        <input type="text" class="form-control" id="reminder_title" name="reminder_title" value="" placeholder="Reminder Title">

                        <td class="error"><?php echo form_error('reminder_title'); ?></td>

                    </div>

                </div>

                <div class="col-sm-4 col-md-4">

                    <div class="bootstrap-timepicker">

                        <div class="form-group">

                            <label>Date:<span class="red_star">*</span></label>

                            <div class="input-group date costom_err" id="datetimepicker">



                                <input type='text' class="form-control" readonly name="reminder_date" value="" placeholder="Reminder Date" />

                                <span class="input-group-addon">

                                    <span class="glyphicon glyphicon-calendar"></span>

                                </span>

                                <td class="error"><?php echo form_error('reminder_date'); ?></td>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-sm-4 col-md-4">

                    <div class="bootstrap-timepicker">

                        <div class="form-group">

                            <label>Time:<span class="red_star">*</span></label>

                            <div class='input-group bootstrap-timepicker timepicker'> <input name='reminder_time' id='timepicker1' type='text' class='form-control input-small' required> <span class='input-group-addon'><i class='glyphicon glyphicon-time'></i></span></div>

                            <td class="error"><?php echo form_error('reminder_time'); ?></td>

                        </div>

                    </div>

                </div>

            </div>



            <div class="row">

                <div class="col-sm-4 col-md-6">

                    <div class="form-group">

                        <label>Remind Before:<span class="red_star">*</span></label>

                        <select name="before_time" class="form-control  ">

                            <option value="">Select Before Time</option>

                            <option value="5">5 Minutes</option>

                            <option value="10">10 Minutes</option>

                            <option value="15">15 Minutes</option>

                            <option value="20">20 Minutes</option>

                        </select>

                        <td class="error"><?php echo form_error('reminder_date'); ?></td>

                    </div>

                </div>

                <div class="col-sm-4 col-md-6">

                    <div class="form-group">

                        <label>Repeat Reminder:<span class="red_star">*</span></label>

                        <select name="repeat_time" class="form-control  ">

                            <option value="">Select Repeat Time</option>

                            <option value="Daily">Daily Basis</option>

                            <option value="Weekly">Weekly Basis</option>

                            <option value="Monthly">Monthly Basis</option>

                            <option value="Yearly">Yearly Basis</option>

                            <option value="Never">Never</option>

                        </select>

                        <td class="error"><?php echo form_error('reminder_date'); ?></td>

                    </div>

                </div>



            </div>









            <div class="row">

                <div class="col-md-12">

                    <div class="form-group">

                        <label for="">Description:<span class="red_star">*</span></label>

                        <textarea class="form-control" id="reminder_description" name="reminder_description" placeholder="Reminder Description"></textarea>





                        <td class="error"><?php echo form_error('reminder_description'); ?></td>

                    </div>

                </div>

            </div>

            <div class="box-footerOne">

                <input type="submit" name="submit" class="btn btn-primary" value="Submit">

            </div>

        </form>

    </div>

</section>

</aside>



<!--Added by 95 on 5-2-2019 For Doctor category for validation-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>





<script type="text/javascript">
jQuery(document).ready(function(){
    $('#timepicker1').timepicker({
                  showInputs: false
                });
})
    $('#reminder').validate({

        rules: {

            reminder_title: "required",

            reminder_description: "required",

            reminder_date: "required",

            reminder_time: "required",

            before_time: "required",

            repeat_time: "required",

        },

        messages: {

            reminder_title: "Please enter reminder title.",

            reminder_description: "Please enter reminder descripiton.",

            reminder_date: "Please select reminder date.",

            reminder_time: "Please select reminder time.",

            before_time: "Please select remind before time.",

            repeat_time: "Please select repeat reminder time.",

        },

        submitHandler: function(form) {

            $.ajax({

                url: '<?php echo base_url(); ?>Reminder/add_reminder1',

                type: 'post',

                data: $(form).serialize(),

                success: function(response) {

                    if (response == 1) {

                        $('#success_message').fadeIn().html('<div class="alert alert-success message"><button type="button" class="close" data-dismiss="alert">x</button>Reminder added successfully.</div>');

                        setTimeout(function() {

                            $('#success_message').fadeOut("milliseconds");

                        }, 1500);

                    }

                    location.href = '<?php echo base_url(); ?>show_activities';



                }

            });

        }

    });
</script>





<script type="text/javascript">
  
    $(function() {

        var today = new Date();

        $('#datetimepicker').datetimepicker({

            ignoreReadonly: true,

            format: 'YYYY-MM-DD',

            minDate: today

        });

    });

    
</script>







<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>