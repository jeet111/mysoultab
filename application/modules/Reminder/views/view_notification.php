<aside class="right-side">
  <section class="content-header no-margin">
    <h1 class="text-center">
      <!-- <i class="fa fa-paper-plane"></i>-->

      <!-- <i class="fa fa-file-text" aria-hidden="true"></i> -->
      <i class="fa fa-bell-o" ></i>
      Reminder Notification
      <div class="back_reminder"><a href="javascript:window.history.back()" class="btn btn-danger">Back</a></div>
    </h1>
  </section>

  <?php //echo "<pre>"; print_r($notification_detail); echo "</pre>"; ?>
  <section class="content photo-list">
    <div class="photo-listMain">
	    <form id="detail_acknowledge" method="post" class="frm_add">
        <div class="row">
          <div class="col-md-6">                        
            <div class="form-group">
              <div class="bx_med">Title:</div>
              <div class="bx_meSec"><?php echo $notification_detail->reminder_title; ?></div>  
            </div>                                  
          </div>
          <div class="col-md-6">                        
            <div class="form-group">
              <div class="bx_med">Description:</div>
              <div class="bx_meSec"><?php echo $notification_detail->reminder_description; ?></div>  
            </div>                                  
          </div>


        </div>


        <div class="row">
          
          <div class="col-md-6">                        
            <div class="form-group">
              <div class="bx_med">Date:</div>
              <div class="bx_meSec"><?php echo $notification_detail->reminder_date; ?></div>
            </div>                                  
          </div>

          <div class="col-md-6">
            <div class="form-group">
                <div class="bx_med">Time:</div>
                <div class="bx_meSec"><?php echo $notification_detail->reminder_time; ?></div>
            </div>              
          </div>


        </div>
        <!-- <div class="row">
          
          <div class="col-md-6">
            <div class="form-group">
              <div class="bx_med">Report File:</div>
              <div class="bx_meSec">

                
                <img height="50px" width="50px"  src="">
              
                
                </div>
            </div>
          </div>
        </div> -->
       
        <!-- <div class="box-footer">
            <a href="<?php echo base_url();?>edit_testreport/<?php echo $TestReportView->report_id;?>" class="btn btn-primary" >Edit</a>            
			<a href="<?php echo base_url(); ?>send_testreport/<?php echo $TestReportView->report_id;?>" class="btn btn-primary"  class="edit_btn">Send To Caregiver</a>
        </div> -->
      </form>           
    </div>
  </section>
</aside>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>


