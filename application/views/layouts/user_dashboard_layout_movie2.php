<?php medicine_reminder(); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Care Pro | <?php echo !empty($title) ? $title : "User Dashboard"; ?></title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/multiselect/bootstrap-multiselect.css">
  <link href="<?php echo base_url();?>assets/user_dashboard/img/favicon.ico" rel="icon">
  <!-- bootstrap 3.0.2 -->
  <link href="<?php echo base_url(); ?>assets/user_dashboard/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- font Awesome -->
  <link href="<?php echo base_url(); ?>assets/user_dashboard/css/font-awesome.css" rel="stylesheet" type="text/css" />
  <link
  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"
  rel="stylesheet"  type='text/css'>
  <!-- Ionicons -->
  <link href="<?php echo base_url(); ?>assets/user_dashboard/css/ionicons.min.css" rel="stylesheet" type="text/css" />
  <!-- Morris chart -->
  <link href="<?php echo base_url(); ?>assets/user_dashboard/css/morris/morris.css" rel="stylesheet" type="text/css" />
  <!-- jvectormap -->
  <link href="<?php echo base_url(); ?>assets/user_dashboard/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/user_dashboard/css/notification.css" rel="stylesheet" type="text/css" />
  <!-- Date Picker -->
  <link href="<?php echo base_url(); ?>assets/user_dashboard/css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/user_dashboard/css/timepicker/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
  <!-- Daterange picker -->
  <link href="<?php echo base_url(); ?>assets/user_dashboard/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
  <!-- bootstrap wysihtml5 - text editor -->
  <link href="<?php echo base_url(); ?>assets/user_dashboard/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
  <!-- Theme style -->
  <link href="<?php echo base_url(); ?>assets/user_dashboard/css/AdminLTE.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/user_dashboard/css/user_dashboard.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/user_dashboard/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/user_dashboard/css/ionicons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/user_dashboard/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/user_dashboard/css/AdminLTE.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/user_dashboard/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user_dashboard/css/jquery.fancybox.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
  <link href="<?php echo base_url(); ?>assets/music_js/styles.css" rel="stylesheet" type="text/css" />
          <!-- <script type="text/javascript">
           $.noConflict();
         </script> -->
         <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
         <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
         <script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>
         <script type="text/javascript" src="<?php echo base_url(); ?>assets/music_js/musicplayer.js"></script>
         <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css" />
       </head>
       <body class="skin-blue" onload="getLocation();">
        <input type="hidden" name="get_lat" id="get_lats" value="" >
        <input type="hidden" name="get_long" id="get_longs" value="" >
        <header class="header">
          <a href="<?php echo base_url(); ?>dashboard" class="logo desk-logo">Care Pro </a>
          <nav class="navbar navbar-static-top" role="navigation">
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </a>
            <div class="mobile_logo"><a href="<?php echo base_url(); ?>" class="logo">Care Pro </a></div>
            <?php
            $checkLogin = $this->session->userdata('logged_in');
            $profile_image = $this->common_model->getsingle('cp_users',array('id' => $checkLogin['id']));
            $notification_data = $this->common_model->getAllwhereorderby("cp_notification",array("user_id" => $checkLogin['id']),"notification_id","desc");
            $unreadnotice = $this->common_model->countwhereuser('cp_notification',array('user_id' =>$checkLogin['id'],'is_read'=>0));
            /*=================*/
            $notification_data = $this->common_model->jointwotablenn("cp_common_notification", "common_id", "medicine_schedule", "medicine_id",array("cp_common_notification.type" => 1,"cp_common_notification.user_id" =>$checkLogin['id'] ),"*","notification_id","desc");
            $unreadnotice = $this->common_model->countwhereuser('cp_common_notification',array('cp_common_notification.user_id' =>$checkLogin['id'],'cp_common_notification.type' => 1,'is_read'=>0));
            $notification_data_reminder = $this->common_model->jointwotablenn("cp_common_notification", "common_id", "cp_reminder", "reminder_id",array("type" => 2,"cp_common_notification.user_id" =>$checkLogin['id'] ),"*","notification_id","desc");
            $unreadnotice_reminder = $this->common_model->countwhereuser('cp_common_notification',array('cp_common_notification.user_id' =>$checkLogin['id'],'cp_common_notification.type' => 2,'cp_common_notification.is_read'=>0));
                // $notification_data_appointment = $this->common_model->jointwotablenn("cp_common_notification", "common_id", "doctor_appointments", "dr_appointment_id",array("type" => 3,"cp_common_notification.user_id" =>$checkLogin['id'],"cp_common_notification.notification_date"=>date('Y-m-d')),"*","notification_id","desc");
            $notification_data_appointment = $this->common_model->getAppointmentNot();
            // $unreadnotice_appointment = $this->common_model->countwhereuser('cp_common_notification',array('cp_common_notification.user_id' =>$checkLogin['id'],'cp_common_notification.type' => 3,'cp_common_notification.is_read'=>0,'notification_date'=>date('Y-m-d')));
            $unreadnotice_appointment = $this->common_model->countAppointmentNot();
            /*=================*/
            /*For Reminder Notification*/
            $notifications = $this->common_model->getAllwhere("user_reminder_notifications",array("notification_user_id" => $checkLogin['id'],'read_status'=>0));
            $count = $this->common_model->countwhereuser('user_reminder_notifications',array('notification_user_id' =>$checkLogin['id'],'read_status'=>0));
            /*=============*/
            $totl_count = $unreadnotice+$unreadnotice_reminder+$unreadnotice_appointment;
            ?>
            <div class="navbar-right hm-menu">
              <ul class="nav navbar-nav">
                <!----------------------------swati--------- -->
                <li class="dropdown"><span class="msg-notfcsn" <?php if($totl_count == 0){ ?> style="display:none;" <?php } ?> id="unreadnotice"><?php echo $totl_count; ?></span><a href="javascript:void(0);" class="dropdown-toggle loadnot" id="menu2" data-toggle="dropdown"><i class="fa fa-bell"></i></a>
                 <ul class="dropdown-menu notifcsn-drop" role="menu" aria-labelledby="menu2" id="menu3">
                   <div id="noticescroll" class="for-scrl" onscroll="scrollnotice();">
                     <?php
                     if(!empty($notification_data) || !empty($notification_data_reminder) || !empty($notification_data_appointment)){
                       foreach($notification_data as $noti){
                         ?>
                         <li role="presentation" class="red-notifcsn">
                          <div class="notifcsn-usr-name">
                            <b><a class="title_sch title_schclick" data-segment="<?php echo $noti->notification_id; ?>" data-medicine="<?php echo $noti->common_id; ?>" href="javascript:void(0);" ><?php echo $noti->medicine_name; ?></a></b>
                            <span class="abut-ntfcsn-time">
                             <?php echo date('F j, Y'); ?>
                             <?php //echo date('F j, Y, g:i a',strtotime($noti->notification_date)); ?>
                           </span>
                         </div>
                       </li>
                     <?php }
                     foreach($notification_data_reminder as $noti_reminder){
                       ?>
                       <li role="presentation" class="red-notifcsn">
                        <div class="notifcsn-usr-name">
                          <b><a class="title_sch" data-medicine="<?php echo $noti->notification_id; ?>" href="<?php echo base_url()?>view_notification/<?php echo $noti_reminder->notification_id; ?>" ><?php echo $noti_reminder->reminder_title; ?></a></b>
                          <span class="abut-ntfcsn-time">
                            <?php echo date('F j, Y'); ?>
                            <?php //echo date('F j, Y, g:i a',strtotime($noti_reminder->notification_date)); ?>
                          </span>
                        </div>
                      </li>
                    <?php }
                    foreach($notification_data_appointment as $noti_appointment){
                      $appo_time = $this->common_model->getsingle("dr_available_time",array("avtime_id" => $noti_appointment->dr_appointment_time_id));
                      ?>
                      <li role="presentation" class="red-notifcsn">
                        <div class="notifcsn-usr-name">
                          <b><a class="title_sch" data-medicine="<?php echo $noti->notification_id; ?>" href="<?php echo base_url()?>view_appointment_notification/<?php echo $noti_appointment->notification_id; ?>" ><?php if(!empty($noti_appointment->dr_appointment_id)){echo "You have appointment.";} ?></a></b>
                          <span class="abut-ntfcsn-time">
                           <?php echo date('F j, Y'); ?>
                           <?php //echo date('F j, Y',strtotime($noti_appointment->notification_date)).', '.$appo_time->avtime_text; ?>
                         </span>
                       </div>
                     </li>
                   <?php }
                 }else{ ?>
                  <h5 class="no_record_noti">No record found</h5>
                <?php } ?>
              </div>
            </ul>
          </li>
                <!-- <li class="dropdown"><span class="msg-notfcsn" <?php if($unreadnotice == 0){ ?> style="display:none;" <?php } ?> id="unreadnotice"><?php echo $unreadnotice; ?></span><a href="javascript:void(0);" class="dropdown-toggle loadnot" id="menu2" data-toggle="dropdown"><i class="fa fa-bell"></i></a>
                 <ul class="dropdown-menu notifcsn-drop" role="menu" aria-labelledby="menu2" id="menu3">
                   <div id="noticescroll" class="for-scrl" onscroll="scrollnotice();">
                     <?php
                     if(!empty($notification_data)){
                       foreach($notification_data as $noti){
                         $medicine = $this->common_model->getsingle("medicine_schedule",array("medicine_id" => $noti->medicine_id));
                         ?>
                         <li role="presentation" class="red-notifcsn">
                          <div class="notifcsn-usr-name">
                            <b><a class="title_sch" data-medicine="<?php echo $noti->notification_id; ?>" href="javascript:void(0);" ><?php echo $medicine->medicine_name; ?></a></b>
                            <span class="abut-ntfcsn-time">
                             <?php //echo 'F j, Y, g:i a' ?>
                             <?php echo date('F j, Y, g:i a',strtotime($noti->notification_date)); ?>
                           </span>
                         </div>
                       </li>
                     <?php }}else{ ?>
                      No record found
                    <?php } ?>
                  </div>
                </ul>
              </li> -->
              <!------------------------------------------- -->
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="glyphicon glyphicon-user"></i>
                  <span><?php echo $checkLogin['username']; ?> <i class="caret"></i></span>
                </a>
                <ul class="dropdown-menu">
                  <?php //echo $menuactive; ?>
                  <!-- User image -->
                  <li class="user-header bg-light-blue">
                    <?php if($profile_image->profile_image){ ?>
                      <img src="<?php echo base_url(); ?>uploads/profile_images/<?php echo $profile_image->profile_image; ?>" class="img-circle" alt="User Image" />
                    <?php }else{ ?>
                      <img src="<?php echo base_url(); ?>assets/user_dashboard/img/avatar3.png" class="img-circle" alt="User Image" />
                    <?php } ?>
                    <p>
                      <?php echo $checkLogin['username']; ?>
                      <small>Member since <?php echo date("F, Y", strtotime($checkLogin['create_user'])); ?></small>
                    </p>
                  </li>
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo base_url(); ?>user/update_profile" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url(); ?>logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!----------------------------swati--------- -->
          <?php /* ?><li class="dropdown"><span class="msg-notfcsn" <?php if($unreadnotice == 0){ ?> style="display:none;" <?php } ?> id="unreadnotice"><?php echo $unreadnotice; ?></span><a href="javascript:void(0);" class="dropdown-toggle loadnot" id="menu2" data-toggle="dropdown"><i class="fa fa-bell"></i></a>
              <ul class="dropdown-menu notifcsn-drop" role="menu" aria-labelledby="menu2" id="menu3">
                  <div id="noticescroll" class="for-scrl" onscroll="scrollnotice();">
          <?php
          foreach($notification_data as $noti){
          $medicine = $this->common_model->getsingle("medicine_schedule",array("medicine_id" => $noti->medicine_id));
          ?>
                <li role="presentation" class="red-notifcsn">
                <div class="notifcsn-usr-name">
                <b><a class="title_sch" data-medicine="<?php echo $noti->medicine_id; ?>" href="javascript:void(0);" ><?php echo $medicine->medicine_name; ?></a></b>
                <span class="abut-ntfcsn-time">
                  <?php //echo 'F j, Y, g:i a' ?></span>
                </div>
                 </li>
          <?php } ?>
                </div>
                </ul>
                </li><?php */ ?>
                <!------------------------------------------- -->
              </ul>
            </div>
          </nav>
        </header>
        <?php $total_email = $this->common_model->jointwotablenm('cp_emails', 'user_id', 'cp_users', 'id',array('to_email ' => $checkLogin['email'],'email_status' => 1,'cp_emails.inbox_status' => 0),'','cp_emails.email_id','desc');
    //echo $this->db->last_query();die;?>
    <div class="wrapper row-offcanvas row-offcanvas-left">
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
           <div class="pull-left image">
            <?php if($profile_image->profile_image){ ?>
              <img src="<?php echo base_url(); ?>uploads/profile_images/<?php echo $profile_image->profile_image; ?>" class="img-circle" alt="User Image" />
            <?php }else{ ?>
              <img src="<?php echo base_url(); ?>assets/user_dashboard/img/avatar3.png" class="img-circle" alt="User Image" />
            <?php } ?>
          </div>
          <div class="pull-left info">
            <p>Hello, <?php echo $profile_image->name; ?></p>
            <i class="fa fa-circle text-success"></i> Online
          </div>
        </div>
        <ul class="sidebar-menu">
          <?php //if(empty($menuactive)){ $menuactive='';} ?>
          <li <?php if($menuactive == ''){
            echo 'class="active"';}?>>
            <a href="<?php echo base_url().'dashboard'; ?>">
              <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
          </li>
          <li class="treeview <?php if($menuactive == 'email_list'  || $menuactive == 'contact_list' || $menuactive == 'weekly_weather' || $menuactive == 'contact_edit' || $menuactive == 'add_contact' || $menuactive == 'reminder_list' || $menuactive == 'edit_reminder' || $menuactive == 'view_reminder' || $menuactive == 'show_activities'){
            ?>active <?php } ?>">
            <a href="#">
              <i class="fa fa-user-o" aria-hidden="true"></i><span>Personal</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php /* ?><li <?php if($menuactive == 'contact_list'){
                echo 'class="active"';}?>><a href="<?php echo base_url(); ?>contact_list"><i class="fa fa-angle-double-right"></i> Calls</a></li><?php */ ?>
                

                <li <?php if($menuactive == 'show_activities'){ echo 'class="active"';}?>><a href="<?php echo base_url();?>call"><i class="fa fa-angle-double-right"></i>Call</a></li>



                <li <?php if($menuactive == 'email_list'){
                  echo 'class="active"';}?>><a href="<?php echo base_url();?>email_list"><i class="fa fa-angle-double-right"></i> Emails</a></li>

                  <li <?php if($menuactive == 'show_activities'){ echo 'class="active"';}?>><a href="<?php echo base_url();?>yoga"><i class="fa fa-angle-double-right"></i>Yoga</a></li>


                  <!-- New menues added  -->
                  <li <?php if($menuactive == 'social'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>social"><i class="fa fa-angle-double-right"></i> Social </a></li>



                  <li <?php if($menuactive == 'transpotation'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>transpotation"><i class="fa fa-angle-double-right"></i> Transpotation </a></li>
                  <!-- end -->

                  <li <?php if($menuactive == 'show_activities'){ echo 'class="active"';}?>><a href="<?php echo base_url();?>sprituality"><i class="fa fa-angle-double-right"></i>Sprituality</a></li>


                  <li <?php if($menuactive == 'reminder_list' || $menuactive == 'add_reminder' || $menuactive == 'edit_reminder' || $menuactive == 'view_reminder'){
                    echo 'class="active"';}?>><a href="<?php echo base_url();?>reminder_list"><i class="fa fa-angle-double-right"></i> Reminder</a></li>


                    <li <?php if($menuactive == 'show_activities'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>show_activities"><i class="fa fa-angle-double-right"></i> Show Activities</a></li>



                <?php /* ?>



                <li <?php if($menuactive == 'email_list'){
                  echo 'class="active"';}?>><a href="<?php echo base_url();?>email_list"><i class="fa fa-angle-double-right"></i> Emails</a></li>
                  <li <?php if($menuactive == 'weekly_weather'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>weekly_weather"><i class="fa fa-angle-double-right"></i> Weather</a></li>


                  <li <?php if($menuactive == 'reminder_list' || $menuactive == 'add_reminder' || $menuactive == 'edit_reminder' || $menuactive == 'view_reminder'){
                    echo 'class="active"';}?>><a href="<?php echo base_url();?>reminder_list"><i class="fa fa-angle-double-right"></i> Reminder</a></li>



                    <li <?php if($menuactive == 'show_activities'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>show_activities"><i class="fa fa-angle-double-right"></i> Show Activities</a></li>

                    <?php */ ?>
                  </ul>
                </li>
                <li class="treeview <?php if($menuactive == 'doctor_speciality' || $menuactive == 'doctors' || $menuactive == 'view_doctor' || $menuactive == 'appointment_list' || $menuactive == 'edit_appointment' || $menuactive == 'view_appointment' || $menuactive == 'medicine_list' || $menuactive == 'thank_you' || $menuactive == 'view_schedule' || $menuactive == 'edit_schedule'|| $menuactive == 'testreport_list' || $menuactive == 'view_testreport' || $menuactive == 'edit_testreport' || $menuactive == 'add_testreport' || $menuactive == 'acknowledge_list' || $menuactive == 'acknowledge_detail' || $menuactive == 'edit_acknowledge' || $menuactive == 'add_acknowledge'  || $menuactive == 'pickupmedicine_list' || $menuactive == 'add_pickup_medicine' || $menuactive == 'nearby_restaurant' || $menuactive == 'view_restaurant' || $menuactive == 'favorite_restaurant' || $menuactive == 'view_favorite_restaurant' || $menuactive == 'all_banks' || $menuactive == 'view_bank' || $menuactive == 'favorite_banks' || $menuactive == 'view_favorite_bank' || $menuactive == 'grocery' || $menuactive == 'view_grocery' || $menuactive == 'favorite_grocery' || $menuactive == 'view_favorite_grocery' || $menuactive == 'note_list' || $menuactive == 'view_note' || $menuactive == 'edit_note' || $menuactive == 'add_note'){ ?> active <?php } ?>">
                  <a href="#">
                    <i class="fa fa-bell-o" aria-hidden="true"></i> <span>Concierge</span>
                    <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">


                    <li <?php if($menuactive == 'appointment_list' || $menuactive == 'edit_appointment' || $menuactive == 'view_appointment'){
                      echo 'class="active"';}?>><a href="<?php echo base_url(); ?>appointment_list"><i class="fa fa-angle-double-right"></i> Doctor Appointments</a></li>

                      <li <?php if($menuactive == 'medicine_list' || $menuactive == 'edit_schedule' || $menuactive == 'add_medicine' || $menuactive == 'view_schedule'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>medicine_list"><i class="fa fa-angle-double-right"></i> Medicine Schedule</a></li>


                      <li <?php if($menuactive == 'pickupmedicine_list' || $menuactive == 'add_pickup_medicine'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>pickupmedicine_list"><i class="fa fa-angle-double-right"></i> Request medicine</a></li>


                      <li <?php if($menuactive == 'pickupmedicine_list' || $menuactive == 'add_pickup_medicine'){ echo 'class="active"';}?>><a href="#"><i class="fa fa-angle-double-right"></i> Vital signs </a></li>

                      <li <?php if($menuactive == 'testreport_list' ||$menuactive == 'add_testreport' || $menuactive == 'edit_testreport' || $menuactive == 'view_testreport' || $menuactive == 'send_testreport' ){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>testreport_list"><i class="fa fa-angle-double-right"></i> Test Report </a></li>


                      <li <?php if($menuactive == 'note_list' || $menuactive == 'add_note' || $menuactive == 'edit_note' || $menuactive == 'view_note'){
                        echo 'class="active"';}?>><a href="<?php echo base_url();?>note_list"><i class="fa fa-angle-double-right"></i> Note</a></li>


                    <?php /* ?>


                    <li <?php if($menuactive == 'doctor_speciality' || $menuactive == 'doctors' || $menuactive == 'view_doctor' || $menuactive == 'thank_you'){
                      echo 'class="active"';}?>><a href="<?php echo base_url(); ?>doctor_speciality"><i class="fa fa-angle-double-right"></i> Doctor Specialties</a></li>
                      <li <?php if($menuactive == 'appointment_list' || $menuactive == 'edit_appointment' || $menuactive == 'view_appointment'){
                        echo 'class="active"';}?>><a href="<?php echo base_url(); ?>appointment_list"><i class="fa fa-angle-double-right"></i> Appointments</a></li>
                        <li <?php if($menuactive == 'medicine_list' || $menuactive == 'edit_schedule' || $menuactive == 'add_medicine' || $menuactive == 'view_schedule'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>medicine_list"><i class="fa fa-angle-double-right"></i> Medicine Schedule</a></li>
                        <li <?php if($menuactive == 'testreport_list' ||$menuactive == 'add_testreport' || $menuactive == 'edit_testreport' || $menuactive == 'view_testreport' || $menuactive == 'send_testreport'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>testreport_list"><i class="fa fa-angle-double-right"></i> Test Report List</a></li>
                        <li <?php if($menuactive == 'acknowledge_list' || $menuactive == 'acknowledge_detail' || $menuactive == 'edit_acknowledge' || $menuactive == 'add_acknowledge'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>acknowledge_list"><i class="fa fa-angle-double-right"></i> Acknowledge List</a></li>



                      <!-- <li <?php if($menuactive == 'reminder_list' || $menuactive == 'add_reminder' || $menuactive == 'edit_reminder' || $menuactive == 'view_reminder'){
                        echo 'class="active"';}?>><a href="<?php echo base_url();?>reminder_list"><i class="fa fa-angle-double-right"></i> Reminder</a></li>
                        


                        <li <?php if($menuactive == 'show_activities'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>show_activities"><i class="fa fa-angle-double-right"></i> Show Activities</a></li> -->


                        <li <?php if($menuactive == 'pickupmedicine_list' || $menuactive == 'add_pickup_medicine'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>pickupmedicine_list"><i class="fa fa-angle-double-right"></i> Pickup medicine</a></li>


                        <?php */ ?>

                        <?php /* ?>

                        <li>
                          <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Restaurants</span>
                          <ul class="frd_swtChild">
                            <li <?php if($menuactive == 'nearby_restaurant'|| $menuactive == 'view_restaurant'){ echo 'class="active"';}?>>
                              <a href="javascript:void(0);" id="near_test"><i class="fa fa-angle-double-right"></i> Restaurant List </a>
                            </li>
                            <li <?php if($menuactive == 'favorite_restaurant'|| $menuactive == 'view_favorite_restaurant'){ echo 'class="active"';}?>>
                              <a href="<?php echo base_url();?>favorite_restaurant"><i class="fa fa-angle-double-right"></i> Favorite Restaurant</a>
                            </li>
                          </ul>
                        </li>
                        <li>
                          <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Banks</span>
                          <ul class="frd_swtChild">
                            <li <?php if($menuactive == 'all_banks'||$menuactive == 'view_bank'){ echo 'class="active"';}?>>
                              <a href="javascript:void(0);" id="bank_test"><i class="fa fa-angle-double-right"></i> Bank List </a>
                            </li>
                            <li <?php if($menuactive == 'favorite_banks'||$menuactive == 'view_favorite_bank'){ echo 'class="active"';}?>>
                              <a href="<?php echo base_url();?>favorite_banks"><i class="fa fa-angle-double-right"></i> Favourite Banks</a>
                            </li>
                          </ul>
                        </li>
                        <li>
                          <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Grocery</span>
                          <ul class="frd_swtChild">
                            <li <?php if($menuactive == 'grocery'){ echo 'class="active"';}?>>
                              <a href="javascript:void(0);" id="grocery_test"><i class="fa fa-angle-double-right"></i> Grocery List </a>
                            </li>
                            <li <?php if($menuactive == 'favorite_grocery' || $menuactive == 'view_favorite_grocery'){ echo 'class="active"';}?>>
                              <a href="<?php echo base_url();?>favorite_grocery"><i class="fa fa-angle-double-right"></i> Favorite Grocery</a>
                            </li>

                            <?php */ ?>

                          </ul>
                        </li>
                        <li <?php if($menuactive == 'note_list' || $menuactive == 'add_note' || $menuactive == 'edit_note' || $menuactive == 'view_note'){
                          echo 'class="active"';}?>><a href="<?php echo base_url();?>note_list"><i class="fa fa-angle-double-right"></i> Note</a></li>
                        </ul>
                      </li>
                      <li class="treeview <?php if($menuactive == 'articles' || $menuactive == 'music_list' || $menuactive == 'movie_list' || $menuactive == 'music_detail' || $menuactive == 'movie_detail' || $menuactive == 'detail_article' || $menuactive == 'favoritemusic_list' || $menuactive == 'favorite_articles' || $menuactive == 'favoritemovie_list' || $menuactive == 'favoritemovie_detail_page' ||$menuactive == 'mymusic_list'||$menuactive =='mymusic_detail' ||$menuactive== 'view_allmovies' ||$menuactive== 'view_all_movies_categories' ||$menuactive== 'view_all_movies_category' || $menuactive == 'user_photos' || $menuactive == 'photos_favourite'){
                        ?>active <?php } ?>">
                        <a href="#">
                          <i class="fa fa-star-o" aria-hidden="true"></i>  <span>Entertainment</span>
                          <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">

                          <li>
                            <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Photos</span>
                            <ul class="frd_swtChild">
                              <li <?php if($menuactive == 'user_photos'){ echo 'class="active"';}?>>
                                <a href="<?php echo base_url();?>user_photos"><i class="fa fa-angle-double-right"></i> Photo </a>
                              </li>
                        <!-- <li <?php if($menuactive == 'photos_favourite'){ echo 'class="active"';}?>>
                          <a href="<?php echo base_url();?>photos_favourite"><i class="fa fa-angle-double-right"></i> Favorite Photo</a>
                        </li> -->
                      </ul>
                    </li>


                    <li>
                      <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Article</span>
                      <ul class="frd_swtChild">
                        <li <?php if($menuactive == 'articles' || $menuactive == 'detail_article' ||$menuactive== 'view_All_articles' || $menuactive =='read_article' || $menuactive == 'add_article' || $menuactive == 'edit_article'){ echo 'class="active"';}?>>
                          <a href="<?php echo base_url();?>articles"><i class="fa fa-angle-double-right"></i> Articles  </a>
                        </li>
                      </ul>
                    </li>


                    <li>
                      <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Music</span>
                      <ul class="frd_swtChild">
                        <li <?php if($menuactive == 'music_list' || $menuactive == 'add_music' || $menuactive == 'edit_music'){ echo 'class="active"';}?>>
                          <a href="<?php echo base_url();?>music_list"><i class="fa fa-angle-double-right"></i> Music </a>
                        </li>
                      </ul>
                    </li>

                    <li>
                      <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Music</span>
                      <ul class="frd_swtChild">
                        <li <?php if($menuactive == 'music_list' || $menuactive == 'add_music' || $menuactive == 'edit_music'){ echo 'class="active"';}?>>
                          <a href="#"><i class="fa fa-angle-double-right"></i> Camera </a>
                        </li>
                      </ul>
                    </li>


                    <li>
                      <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Movie</span>
                      <ul class="frd_swtChild">
                        <li <?php if($menuactive == 'movie_list' || $menuactive == 'movie_detail' || $menuactive == 'add_movie' || $menuactive == 'edit_movie'){ echo 'class="active"';}?>>
                          <a href="<?php echo base_url();?>movie_list"><i class="fa fa-angle-double-right"></i> Movies</a>
                        </li>
                      </ul>
                    </li>


                    <li>
                      <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Music</span>
                      <ul class="frd_swtChild">
                        <li <?php if($menuactive == 'music_list' || $menuactive == 'add_music' || $menuactive == 'edit_music'){ echo 'class="active"';}?>>
                          <a href="#"><i class="fa fa-angle-double-right"></i> Games </a>
                        </li>
                      </ul>
                    </li>

                    <li>
                      <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Music</span>
                      <ul class="frd_swtChild">
                        <li <?php if($menuactive == 'weekly_weather'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>weekly_weather"><i class="fa fa-angle-double-right"></i> Weather</a></li>
                      </ul>
                    </li>


                    <li>
                      <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Music</span>
                      <ul class="frd_swtChild">
                        <li <?php if($menuactive == 'weekly_weather'){ echo 'class="active"';}?>><a href="https://www.40plusmart.com/"><i class="fa fa-angle-double-right"></i> 40 Plus Mart</a></li>
                      </ul>
                    </li>

                          <?php /* ?>

                          <li>
                           <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Article</span>
                           <ul class="frd_swtChild">
                            <li <?php if($menuactive == 'articles' || $menuactive == 'detail_article'){ echo 'class="active"';}?>>
                              <a href="<?php echo base_url();?>articles"><i class="fa fa-angle-double-right"></i> Article List </a>
                            </li>
                                        <!-- <li <?php if($menuactive == 'favorite_articles'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>favorite_articles"><i class="fa fa-angle-double-right"></i> Favorite Article</a>
                                          </li> -->
                                        </ul>
                                      </li>
                                      <li>
                                       <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Music</span>
                                       <ul class="frd_swtChild">
                                        <li <?php if($menuactive == 'music_list'){ echo 'class="active"';}?>>
                                          <a href="<?php echo base_url();?>music_list"><i class="fa fa-angle-double-right"></i> Music List </a>
                                        </li>
                                      <!--  <li <?php if($menuactive == 'favoritemusic_list'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>favoritemusic_list"><i class="fa fa-angle-double-right"></i> Favorite Music</a>
                                        </li>
                                        <li <?php if($menuactive == 'mymusic_list'||$menuactive =='mymusic_detail'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>mymusic_list"><i class="fa fa-angle-double-right"></i> My Music</a>
                                          </li> -->
                                        </ul>
                                      </li>
                                      <li>
                                       <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Movie</span>
                                       <ul class="frd_swtChild">
                                        <li <?php if($menuactive == 'movie_list' || $menuactive == 'movie_detail' ||$menuactive== 'view_allmovies' ||$menuactive== 'view_all_movies_categories' ||$menuactive== 'view_all_movies_category'){ echo 'class="active"';}?>>
                                          <a href="<?php echo base_url();?>movie_list"><i class="fa fa-angle-double-right"></i> Movie List </a>
                                        </li>
                                        <!-- <li <?php if($menuactive == 'favoritemovie_list'  || $menuactive == 'favoritemovie_detail_page'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>favoritemovie_list"><i class="fa fa-angle-double-right"></i> Favorite Movie</a>
                                          </li> -->
                                        </ul>
                                      </li>
                                      <li>
                                        <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Photos</span>
                                        <ul class="frd_swtChild">
                                          <li <?php if($menuactive == 'user_photos'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>user_photos"><i class="fa fa-angle-double-right"></i> Photo </a>
                                          </li>
                                          <li <?php if($menuactive == 'photos_favourite'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>photos_favourite"><i class="fa fa-angle-double-right"></i> Favorite Photo</a>
                                          </li>


                                          <?php */ ?>


                                        </ul>
                                      </li>
                                    </ul>
                                  </li>
                                  
                                  <!-- <li <?php if($menuactive == 'transactions' ){
                                    echo 'class="active"';}?>><a href="<?php echo base_url();?>transactions"><i class="fa fa-angle-double-right"></i>Transactions</a></li> -->


                                    <li <?php if($menuactive == 'address_book' || 'view_address' || 'edit_address' || 'add_address'){
                                      echo 'class="active"';}?>><a href="<?php echo base_url();?>address_book"><i class="fa fa-angle-double-right"></i>Address Book</a></li>


                                      <!-- <li <?php if($menuactive == 'all_tickets' || 'genrate_ticket' || 'view_ticket'){
                                        echo 'class="active"';}?>><a href="<?php echo base_url();?>all_tickets"><i class="fa fa-angle-double-right"></i>Tickets</a></li> -->

                                        
                                        <!-- New tabs -->
                                    <?php /* ?>
                                    <li class="treeview <?php if($menuactive == 'email_list'  || $menuactive == 'contact_list' || $menuactive == 'weekly_weather' || $menuactive == 'contact_edit' || $menuactive == 'add_contact'){
                                      ?>active <?php } ?>">
                                      <a href="#">
                                        <!-- <i class="fa fa-user-o" aria-hidden="true"></i> -->
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                        <span>Personal</span>
                                        <i class="fa fa-angle-left pull-right"></i>
                                      </a>
                                      <ul class="treeview-menu">
             <li <?php if($menuactive == 'contact_list' || $menuactive == 'contact_edit' || $menuactive == 'add_contact'){
              echo 'class="active"';}?>><a href="<?php echo base_url(); ?>contact_list"><i class="fa fa-angle-double-right"></i> Calls</a></li>
              <li <?php if($menuactive == 'email_list'){
                echo 'class="active"';}?>><a href="<?php echo base_url();?>email_list"><i class="fa fa-angle-double-right"></i> Emails</a></li>
                <li <?php if($menuactive == 'weekly_weather'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>weekly_weather"><i class="fa fa-angle-double-right"></i> Weather</a></li>
                <?php */ ?>
              </ul>
            </li>
            <!-- end new tabs -->
          </ul>
        </section>
      </aside>
      <?php echo $contents;?>
    </div>
    <script src="<?php echo base_url(); ?>assets/user_dashboard/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/user_dashboard/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/src/js/bootstrap-datetimepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
    <script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/demo.js" type="text/javascript">
    </script>
    <script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/custom.js" type="text/javascript">
    </script>
    <script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/dashboard.js" type="text/javascript"></script>
    <!-- <script src="<?php echo base_url();?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script> -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/css/multiselect/bootstrap-multiselect.js"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>
    <!--  -->
    <script>
      $(document).on('click','.title_schclick',function(){
       var medicine = $(this).attr('data-medicine');
       var segment = $(this).attr('data-segment');
       $.post("<?php echo base_url().'Pages/Cron_test/readnotification'; ?>",
         { medicine : medicine,segment : segment},
         function(response){
           window.location.href = '<?php echo base_url(); ?>view_shedule/'+medicine;
//$(".msg-notfcsn").load(location.href + " .msg-notfcsn");
}, "json");
     });
      $(document).on('click','#near_test',function(e){
       var lat = $("#get_lats").val();
  //alert(lat);
  var longt = $("#get_longs").val();
  window.location = "<?php echo base_url(); ?>nearby_restaurant?lat="+lat+'&long='+longt;
});
      $(document).on('click','#bank_test',function(e){
       var lat = $("#get_lats").val();
  //alert(lat);
  var longt = $("#get_longs").val();
  window.location = "<?php echo base_url(); ?>all_banks?lat="+lat+'&long='+longt;
});
      $(document).on('click','#grocery_test',function(e){
       var lat = $("#get_lats").val();
  //alert(lat);
  var longt = $("#get_longs").val();
  window.location = "<?php echo base_url(); ?>grocery?lat="+lat+'&long='+longt;
});     
      function getLocation() {
       if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
      } else {
          //alert("Geolocation is not supported by this browser.");
          var lat = "22.7167";
          var lng = "75.8333";
          $("#get_lats").val(lat);
          $("#get_longs").val(lng);
        }
      }
      function geoSuccess(position) {
       var lat = position.coords.latitude;
       var lng = position.coords.longitude;
       $("#get_lats").val(lat);
       $("#get_longs").val(lng);      
     }
     function geoError() {
      //alert("Geocoder failed.");
      var lat = "22.7167";
      var lng = "75.8333";
      $("#get_lats").val(lat);
      $("#get_longs").val(lng); 
    }
    baguetteBox.run('.tz-gallery');
  </script>
  <!--  -->
  <script>
   $(document).on('click','.title_sch',function(){
     var medicine = $(this).attr('data-medicine');
     $.post("<?php echo base_url().'Pages/Cron_test/readnotification'; ?>",
       { medicine : medicine},
       function(response){
         window.location.href = '<?php echo base_url(); ?>view_shedule/'+medicine;
//$(".msg-notfcsn").load(location.href + " .msg-notfcsn");
}, "json");
   });
 </script>
</div>
</html>
</body>
</html>