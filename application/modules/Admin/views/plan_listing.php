<?php

if (isset($staff_permission[17]['add_permission']) == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn">

			<a href="<?php echo base_url(); ?>admin/add_plan">

				Add Plan

			</a>

		</div>

	</div>

	<?php

} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn">

			<a href="<?php echo base_url(); ?>admin/add_plan">

				Add Plan

			</a>

		</div>

	</div>

	<?php	

}

?>



<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="card-body">

			<div class="status-cng"></div>

			<?php

			if($this->session->flashdata('success')) {

				$message = $this->session->flashdata('success');

				?>

				<div class="<?php echo $message['class'] ?>"><?php echo $message['message']; ?></div>

			<?php } ?>

			 <!-- <div class="bx_user">

	<div class="add-btn"><a href="<?php echo base_url(); ?>admin/add/user">Add User</a></div>

</div> -->



<div id="deletemsg"></div>





<table class="table table-hover tab_comn " id="sampleTable">

	<thead>

		<tr>

			<th>Sno.</th>

			<th>Plan Name</th>

			<th>Amount</th>

			<th>Days</th>

			<th>Description</th>

			<th>Action</th>

		</tr>

	</thead>

	<tbody>

		<?php 

		foreach ($plan_list as $key => $user) 

		{

			?>

			<tr>

				<td><?php echo ($key+1); ?></td>

				<td><?php echo $user['plan_name']; ?></td>

				<td><?php echo $user['amount']; ?></td>

				<td><?php echo $user['plan_days']; ?></td>

				<td><?php echo $user['description']; ?></td>

				<td>

					<div class="link-del-view">

						<?php

						if (isset($staff_permission[17]['edit_permission']) == 1) {

							?>

							<div class="tooltip-2">

								<a href="admin/edit_plan/<?php echo $user['id']; ?>">

									<i class="fa fa-edit" aria-hidden="true"></i>

								</a>

								<span class="tooltiptext">Edit</span>

							</div>

							<?php

						} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

							?>

							<div class="tooltip-2">

								<a href="admin/edit_plan/<?php echo $user['id']; ?>">

									<i class="fa fa-edit" aria-hidden="true"></i>

								</a>

								<span class="tooltiptext">Edit</span>

							</div>

							<?php

						}

						?>

						

						<?php

						if (isset($staff_permission[17]['delete_permission']) == 1) {

							?>

							<div class="tooltip-2">

								<a href="javascript:void(0);" data-id="<?php echo $user['id']; ?>" id="deleteplan_<?php echo $user['id']; ?>">

									<i class="fa fa-trash-o" aria-hidden="true"></i>

								</a>

								<span class="tooltiptext">

									Delete

								</span>

								<!-- <a href="javascript:void(0)" onclick="deleteStatus(<?php echo $user['id']; ?>,'admin/delete/user')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span> -->

							</div>

							<?php

						} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

							?>

							<div class="tooltip-2">

								<a href="javascript:void(0);" data-id="<?php echo $user['id']; ?>" id="deleteplan_<?php echo $user['id']; ?>">

									<i class="fa fa-trash-o" aria-hidden="true"></i>

								</a>

								<span class="tooltiptext">

									Delete

								</span>

							</div>

							<?php

						}

						?>

						

						

					</div>

				</td>

			</tr>

		<?php } ?>

	</tbody>

</table>

</div>

</div>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>



<script type="text/javascript">







	$(document).on('click','[id^="deleteplan_"]',function(){

		var plan_id = $(this).attr('data-id');

		if(confirm("Are you sure, you want to delete this plan ?")){

			$.ajax({

				type: "POST",

				url: '<?php echo base_url(); ?>admin/delete_plan',

				data: 'plan_id='+plan_id,

            //dataType: "json",

            success: function(data)

            {

            	if(parseInt(data)==1){

            		alert('Plan has been deleted!');

            		setTimeout(function(){

            			location.reload();

            		}, 3000);

            	} else {

            		alert('Some internal issue occured!');

            	}

            }

        });

		}

	});

	

	

	

	$(document).on('click','.activation_cls',function(){

		

		var activation_id = $(this).attr('data-activation');

		var user_id = $(this).attr('data-id');	

		$.ajax({

			type: "POST",

			url: '<?php echo base_url(); ?>Admin/Admin_user/activation_status/',

			data: 'activation_id='+activation_id+'&user_id='+user_id,

            //dataType: "json",

            success: function(data)

            {

            	if(data == 1){

            		$("#btton_change_"+user_id).html("<button type='button' data-activation='1' data-id='"+user_id+"' class='btn ybt btn-xs activation_cls'  >Active</button>");

            	}else{

            		$("#btton_change_"+user_id).html("<button type='button' data-activation='0' data-id='"+user_id+"' class='btn xbt btn-xs activation_cls'  >Inactive</button>");

            	}

            }

        });

	});	

</script>