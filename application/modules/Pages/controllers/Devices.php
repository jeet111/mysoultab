<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Devices extends MX_Controller {
	
	public function __construct() {
		parent:: __construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('table');
		$this->load->library('pagination');
		$this->load->model('common_model');
		$this->load->model('common_model_new');
		$this->load->model('Common_model_new');
		$this->load->helper(array('common_helper'));
		$this->load->helper(array('url'));
		$this->load->helper(array('form'));
		not_login();
	}

	public function devices(){

		$checkLogin = $this->session->userdata('logged_in');

		if(!empty($checkLogin) && $checkLogin !=''){
	
		  $user_id	= $checkLogin['id'];
		  
		  $data		= $this->common_model_new->getsingle("device_details",array("device_user_id" => $user_id)); 
		  
			if(!empty($data)){
			  
				$device_data['btnsetingArray'] = array(
				  'device_id'				  =>$data->device_id,
				  'device_user_id'            =>$data->device_user_id,
				  'device_unique_id'          =>$data->device_unique_id,
				  'device_btnname'            =>$data->device_btnname,
				  'device_status'             =>$data->device_status,
				  'device_last_known_location'=>$data->device_last_known_location,
				  'device_battery_percentage' =>$data->device_battery_percentage,
				  'device_last_online'		  =>date("d-m-Y  H:i A", strtotime($data->device_last_online)),
				  'device_os_version'         =>$data->device_os_version,
				  'device_app_version'        =>$data->device_app_version,
				  'device_connection_type'    =>$data->device_connection_type,
				  'device_signal_strength'    =>$data->device_signal_strength
				);

				$this->template->load('new_user_dashboard_layout', 'contents', 'device_detail',$device_data);
			}else{
				$device_data='';
				$this->template->load('new_user_dashboard_layout', 'contents', 'device_detail',$device_data);
			}

		}else{
			redirect('login');
		}
	}

	
	public function edit_device_setting(){
		
		$checkLogin = $this->session->userdata('logged_in');
	
		if(!empty($checkLogin) && $checkLogin !=''){
			
			$user_id	= isset($checkLogin['id']) ? $checkLogin['id'] : "";
			$data		= $this->common_model_new->getsingle("device_details",array("device_user_id" => $user_id)); 
			
			if(!empty($data)){
			  
				$device_data['btnsetingArray'] = array(
				  'device_id'			=>$data->device_id,
				  'device_user_id'      =>$data->device_user_id,
				  'device_kiosk_mode'   =>$data->device_kiosk_mode
				);
				
				$this->template->load('new_user_dashboard_layout', 'contents', 'edit_device_setting',$device_data);	
			}
		}else{
			redirect('login');
		} 
   }
   
	public function update_status(){
   
		$checkLogin = $this->session->userdata('logged_in');
	
		if(!empty($checkLogin) && $checkLogin !=''){
			
			$user_id = isset($checkLogin['id']) ? $checkLogin['id'] : "";
			$data	 = $this->common_model_new->getsingle("device_details",array("device_user_id" => $user_id)); 
		
			$device_kiosk_mode = ($data->device_kiosk_mode == '1' ? '0' : '1');
				
			$condition	= array('device_id' =>$data->device_id);
			$array 		= array('device_kiosk_mode' => $device_kiosk_mode);
			
			$update_login = $this->common_model->updateRecords('device_details', $array, $condition);
		
			if ($update_login) {
			 	echo 1;
			}else{
				echo 0;
			}
		} 
	}
  
}
  

  


	
?>
