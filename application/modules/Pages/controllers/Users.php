<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions

require APPPATH . '/libraries/REST_Controller.php';

require APPPATH . 'libraries/twilio-php-master/Twilio/autoload.php';

use Twilio\Rest\Client;

use Twilio\Jwt\AccessToken;

use Twilio\Jwt\Grants\VoiceGrant;

use Twilio\Jwt\Grants\VideoGrant;

class Users extends REST_Controller

{

	public function __construct(){

		parent:: __construct();

		$this->load->library("security");

		/*For Language*/

		if ($_POST) { 

			///$this->param = $_POST;

			$this->param = $this->security->xss_clean($_POST);

		}else{ 

			//$this->param = json_decode(file_get_contents("php://input"),true); 

			$this->param = $this->security->xss_clean(json_decode(file_get_contents("php://input"),true));

		}

//$userData = $this->param['user_language'];

// $GLOBALS['user_lang'] = $userData;

	}

	/*___________________________________ -START FROM HEAR-_____________________________*/

	/* User Login */

	public function login_post() {

		//$pdata = file_get_contents("php://input");

		//$data = json_decode($pdata, true);

		//$pdata = file_get_contents("php://input");

		//$data = json_decode($pdata, true);

		$data = $this->param;

		//echo $data['password'];

		//print_r($data);

		//die;

		$required_parameter = array('email','password','device_id','device_type','user_roll');

		$chk_error = check_required_value($required_parameter, $data);

		if ($chk_error) {

			$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param'])));

			$this->response($resp);

		}else {

			$email = (!empty($data['email']) ? $data['email'] : '');

			$password = (!empty($data['password']) ? md5($data['password']) : '');

			$user_roll = (!empty($data['user_roll']) ? $data['user_roll'] : '');

			if($this->isValidEmail($email)){

				$check_email = $this->common_model->getRecordCount('cp_users', array('email' => $email));

				$check_email_validation = $this->common_model->getsingle('cp_users', array('email' => $email));

				if($check_email > 0){

					if($check_email_validation->email == $data['email']){

						if($check_email_validation->password == md5($data['password'])){

							$conditions = array('email' => $email,'password' => $password,'user_role'=>$user_roll);

							$conditions1 = array('email' => $email,'password' => $password,'status'=>1,'user_role'=>$user_roll);

							$users = $this->common_model->getSingleRecordById('cp_users',$conditions);

							if(!empty($users)){

								$condition = array('id' => $users['id']);

								$device_type = isset($data['device_type']) ? $data['device_type'] : '';

								$device_id   = isset($data['device_id']) ? $data['device_id'] : '';

								$updateArr = array('device_type'=>$device_type,'device_id'=>$device_id);

								$update_login = $this->common_model->updateRecords('cp_users', $updateArr, $condition);

								$users1 = $this->common_model->getSingleRecordById('cp_users',$conditions1);

								//if(!empty($users1)){

								$user_details = array(

									'id' => $users['id'],

									'name' => $users['name'],

									'lastname' => $users['lastname'],

									'dob' => $users['dob'],

									'email' => $users['email'],

									'Otp_verification' =>$users['status'],    

										//'email' => $users['email'],

									'mobile' => $users['mobile'],

									'device_id' => $users['device_id'],

									'device_type' => $users['device_type'],

									'profile_image'=>base_url().'uploads/profile_images/'.$users['profile_image'],

								);

								if($users['status']==1){

									$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Login successfully', 'response' => $user_details);

								}else{

									// OTP code start

									$mob= $users['mobile'];

									$datee=strtotime(date("h:i:sa"))+600;

									$datee=date("Y-m-d h:i:s",$datee);

									$user_name = $users['email'];

									$otp= rand(1000,9999);

									$uid = array('id'=>$users['id']);

									$data_otp= array('otp'=>$otp,'otp_expire' => $datee);

									//$response= $this->send_sms($otp,$mob);

									$response= $this->send_sms($otp,$mob);

									$subject = 'CarePro OTP Code';

									$message = 'Your user verify OTP code:'.$otp;

									$chk_mail= new_send_mail($message, $subject, $user_name,'');

									// echo $chk_mail;

									// die;

									$otp_id = $this->common_model->updateRecords('cp_users',$data_otp,$uid);

									// OTP code end

									$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Account not verified.', 'response' => $user_details);

								}

								// }else{

								// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => "User inactive or deleted by admin",'response' => array('message' => 'User inactive or deleted by admin'));

								// }

							}else{

								$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'The email or password inserted is wrong','response' => array('message' => 'The email or password inserted is wrong'));

							}

						}else{

							$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Password inserted is wrong','response' => array('message' => 'Password inserted is wrong'));

						}

					}else{

						$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Email inserted is wrong','response' => array('message' => 'Email inserted is wrong'));

					}

				}else {

// $resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'The inserted email is not associated with any account sdfsdf','response' => array('message' => 'The inserted Email is not associated with any account tst'));

					$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Please enter correct username and password.','response' => array('message' => 'Please enter correct username and password.'));

				}

			}else{

				$check_username = $this->common_model->getRecordCount('cp_users', array('username' => $email));

				if($check_username > 0){

					$conditions = array('username' => $email,'password' => $password,'user_role'=>$user_roll);

					$conditions1 = array('username' => $email,'password' => $password,'status'=>1,'user_role'=>$user_roll);

					$users = $this->common_model->getSingleRecordById('cp_users',$conditions);

					if(!empty($users)){

						$condition = array('id' => $users['id']);

						$device_type = isset($data['device_type']) ? $data['device_type'] : '';

						$device_id   = isset($data['device_id']) ? $data['device_id'] : '';

						$updateArr = array('device_type'=>$device_type,'device_id'=>$device_id);

						$update_login = $this->common_model->updateRecords('cp_users', $updateArr, $condition);

						$users1 = $this->common_model->getSingleRecordById('cp_users',$conditions1);

						//if(!empty($users1)){

						$user_details = array(

							'id' => $users['id'],

							'name' => $users['name'],

							'username' => $users['username'],

							'lastname' => $users['lastname'],

							'dob' => $users['dob'],

							'email' => $users['email'],

							'Otp_verification' =>$users['status'],

							'mobile' => $users['mobile'],

							'device_id' => $users['device_id'],

							'device_type' => $users['device_type'],

							'profile_image'=>base_url().'uploads/profile_images/'.$users['profile_image'],

						);

						if($users['status']==1){

							$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Login successfully', 'response' => $user_details);

						}else{

						// OTP code start

							$mob= $users['mobile'];

							$datee=strtotime(date("h:i:sa"))+600;

							$datee=date("Y-m-d h:i:s",$datee);

							$user_name = $users['email'];

							$otp= rand(1000,9999);

							$uid = array('id'=>$users['id']);

							$data_otp= array('otp'=>$otp,'otp_expire' => $datee);

									//$response= $this->send_sms($otp,$mob);

							$response= $this->send_sms($otp,$mob);

							$subject = 'CarePro OTP Code';

							$message = 'Your user verify OTP code:'.$otp;

							$chk_mail= new_send_mail($message, $subject, $user_name,'');

									// echo $chk_mail;

									// die;

							$otp_id = $this->common_model->updateRecords('cp_users',$data_otp,$uid);

									// OTP code end

							$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Account not verified.', 'response' => $user_details);

						}

						// }else{

						// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => "User inactive or deleted by admin",'response' => array('message' => 'User inactive or deleted by admin'));

						// }

					}else{

						$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'The username or password inserted is wrong','response' => array('message' => 'The username or password inserted is wrong'));

					}

				}else {

					$check_phone = $this->common_model->getRecordCount('cp_users', array('mobile_with_code' => $email));

					if($check_phone > 0){

						$conditions = array('mobile_with_code' => $email,'password' => $password);

						$conditions1 = array('mobile_with_code' => $email,'password' => $password,'status'=>1);

						$users = $this->common_model->getSingleRecordById('cp_users',$conditions);

						if(!empty($users)){

							$condition = array('id' => $users['id']);

							$device_type = isset($data['device_type']) ? $data['device_type'] : '';

							$device_id   = isset($data['device_id']) ? $data['device_id'] : '';

							$updateArr = array('device_type'=>$device_type,'device_id'=>$device_id);

							$update_login = $this->common_model->updateRecords('cp_users', $updateArr, $condition);

							$users1 = $this->common_model->getSingleRecordById('cp_users',$conditions1);

							//if(!empty($users1)){

							$user_details = array(

								'id' => $users['id'],

								'name' => $users['name'],

								'username' => $users['username'],

								'email' => $users['email'],

								'Otp_verification' =>$users['status'],

								'mobile' => $users['mobile'],

								'device_id' => $users['device_id'],

								'device_type' => $users['device_type'],

								'profile_image'=>base_url().'uploads/profile_images/'.$users['profile_image'],

							);

							if($users['status']==1){

								$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Login successfully', 'response' => $user_details);

							}else{

							// OTP code start

								$mob= $users['mobile'];

								$datee=strtotime(date("h:i:sa"))+600;

								$datee=date("Y-m-d h:i:s",$datee);

								$user_name = $users['email'];

								$otp= rand(1000,9999);

								$uid = array('id'=>$users['id']);

								$data_otp= array('otp'=>$otp,'otp_expire' => $datee);

									//$response= $this->send_sms($otp,$mob);

								$response= $this->send_sms($otp,$mob);

								$subject = 'CarePro OTP Code';

								$message = 'Your user verify OTP code:'.$otp;

								$chk_mail= new_send_mail($message, $subject, $user_name,'');

									// echo $chk_mail;

									// die;

								$otp_id = $this->common_model->updateRecords('cp_users',$data_otp,$uid);

									// OTP code end

								$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Account not verified.', 'response' => $user_details);		

							}

							// }else{

							// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => "User inactive or deleted by admin",'response' => array('message' => 'User inactive or deleted by admin'));

							// }

						}else{

							$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'The mobile number or password inserted is wrong','response' => array('message' => 'The mobile number or password inserted is wrong'));

						}

					}else{

//$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'The inserted mobile number is not associated with any account sfd','response' => array('message' => 'The inserted Mobile Number is not associated with any account'));

						$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Please enter correct username and password.','response' => array('message' => 'Please enter correct username and password.'));

					}

				}

			}

		}

		$this->response($resp);

	}

	function isValidEmail($email) {

		return filter_var($email, FILTER_VALIDATE_EMAIL)

		&& preg_match('/@.+\./', $email);

	}

	/*User Signup */

	public function signup_post() {

		// $pdata = file_get_contents("php://input");

		// $data = json_decode($pdata, true);

		$data = $this->param;

		$mob = $data['mobile_no'];

		$required_parameter = array("user_roll","name","lastname","dob","username","email","mobile_no","country_code","password","confirm_password","device_id","device_type");

		$chk_error = check_required_value($required_parameter, $data);

		if ($chk_error) {

			$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' =>  'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param'])));

			$this->response($resp);

		}

		if(!$this->isValidEmail($data['email'])){

			$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'input must have valid email in the text', 'response' => array('message' => 'input must have valid email in the text'));

			$this->response($resp);

		}

		if(!is_numeric($data['mobile_no'])){

			$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Input must have numbers only', 'response' => array('message' => 'Input must have numbers only'));

			$this->response($resp);

		}

		// security changes start

		$password = trim($data['password']);

		$regex_lowercase = '/[a-z]/';

		$regex_uppercase = '/[A-Z]/';

		$regex_number = '/[0-9]/';

		$regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';

		// if (preg_match_all($regex_lowercase, $password) < 1)

		// {

		// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => message(43), 'response' => array('message' => message(43)));

		// 	$this->response($resp);

		// }

		if(preg_match_all($regex_lowercase, $password) < 1) {

			$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must contains lowercase letter.', 'response' => array('message' => 'Your password must contains lowercase letter.'));

			$this->response($resp);

		}

		if(preg_match_all($regex_uppercase, $password) < 1) {

			$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must contains uppercase letter.', 'response' => array('message' => 'Your password must contains uppercase letter.'));

			$this->response($resp);

		}

		if(preg_match_all($regex_number, $password) < 1) {

			$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must contains number.', 'response' => array('message' => 'Your password must contains number.'));

			$this->response($resp);

		}

		if(preg_match_all($regex_special, $password) < 1) {

			$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must contains special character.', 'response' => array('message' => 'Your password must contains special character.'));

			$this->response($resp);

		}

		if(strlen($password) < 6) {

			$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must be at least 6 characters long.', 'response' => array('message' => 'Your password must be at least 6 characters long.'));

			$this->response($resp);

		}

		if(strlen($password) > 30) {

			$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must not exceed 30 characters long.', 'response' => array('message' => 'Your password must not exceed 30 characters long.'));

			$this->response($resp);

		}

		// security changes end

		if($data['password'] != $data['confirm_password']) {

			$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Input must be the same as the password', 'response' => array('message' => 'Input must be the same as the password'));

			$this->response($resp);

		}else{

			$check_email = $this->common_model->getRecordCount('cp_users', array('email' => $data['email'],'status!='=>3));

			// echo $check_email;

			// die;

			if($check_email >0){

			// if($check_email > 0){

			// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'This email is already exit, please login.', 'response' => array('message' =>'This email is already exit, please login.'));

			// 	$this->response($resp);

			// 	exit;

			// }

			// $check_username = $this->common_model->getRecordCount('cp_users', array('username' => $data['username'],'status!='=>3));

			// if($check_username > 0){

			// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'This userid is already exit, please login.', 'response' => array('message' =>'This userid is already exit, please login.'));

			// 	$this->response($resp);

			// 	exit;

			// }

			// echo $data['country_code'].$data['mobile_no'];

			// die;

				$enter_email = $data['email'];

				$enter_username = $data['username'];

				$enter_mobile_no = $data['mobile_no'];

				$enter_password = md5($data['password']);

				$ChechUser = $this->common_model->getSingleRecordById('cp_users', array('email' => $enter_email));

				$get_email=	$ChechUser['email'];

				$get_mobile=	$ChechUser['mobile'];

				$get_username=	$ChechUser['username'];

				$get_password=	$ChechUser['password'];

				// echo 'Enter email'.' '.$enter_email;

				// echo "<br>";

				// echo 'Get email'.' '.$get_email;

				// echo "<br>";

				// echo 'Enter username'.' '.$enter_username;

				// echo "<br>";

				// echo 'Get username'.' '.$get_username;

				// echo "<br>";

				// echo 'Enter mobile'.' '.$enter_mobile_no;

				// echo "<br>";

				// echo 'Get mobile'.' '.$get_mobile;

				// echo "<br>";

				// echo 'Enter pass'.' '.$enter_password;

				// echo "<br>";

				// echo 'Get pass'.' '.$get_password;

				// echo "<br>"; 

				// die;

				// echo "check2";

				// echo "<pre>";

				// print_r($ChechUser['email']);

				// die;

				// echo $enter_email;

				// echo "<br>";

				// echo $get_email;

				// die;

				$error_count = 0;

				if($enter_email != $get_email){

					$error_count++;

				}

				if($enter_username != $get_username){

					$error_count++;

				}

				if($enter_mobile_no != $get_mobile){

					$error_count++;

				}

				if($enter_password != $get_password){

					$error_count++;

				}

				if ($error_count > 0) {

					$resp = array('status_code' => ERROR,'status' => 'false','count' => $error_count, 'message' => 'Sorry you are not verified.','response' => array('message' =>'Sorry you are not verified.'));

				} else {

					$u_id=$ChechUser['id'];

					$where_condition=array('id'=>$u_id);

					$verificationData=array('user_verify_by_app'=>1);    

					$this->common_model->updateRecords('cp_users',$verificationData,$where_condition);

					// echo "if true";  

					// die;

					// echo "check2";

					// echo "<pre>";

					// print_r($enter_email);

					// die;

					$data = array(

						'username' => (!empty($ChechUser['username']) ? $ChechUser['username'] : ''),

						'name' => (!empty($ChechUser['name']) ? $ChechUser['name'] : ''),

						'lastname' => (!empty($ChechUser['lastname']) ? $ChechUser['lastname'] : ''), 

						'dob' => (!empty($ChechUser['dob']) ? $ChechUser['dob'] : ''),

						'email' => (!empty($ChechUser->email) ? $ChechUser['email'] : ''),

						'mobile' => (!empty($ChechUser->mobile) ? $ChechUser['mobile'] : ''),

						//'password' => (!empty($data['password']) ? md5($ChechUser->email) : ''),

						'device_id' => (!empty($ChechUser->device_id) ? $ChechUser['device_id'] : ''),

						'device_type' => (!empty($ChechUser->device_type) ? $ChechUser['device_type'] : ''),

						//'create_user' => date('Y-m-d h:i:s'),

						'Otp_verification' =>$ChechUser->status,  

						//'user_verification_status'=> $ChechUser['user_verify_by_app'],       

						//'status'      => 0,

						'user_role'      =>$ChechUser['user_role'],

						'country_code'      =>$ChechUser['country_code'],

						'mobile_with_code'    =>$ChechUser['country_code'].$ChechUser['mobile'],

					);

					// echo "<pre>";

					// print_r($data);

					// die;

					//$userId = $this->common_model->addRecords('cp_users', $data);

					$userId = $ChechUser['id'];

					// echo $userId;

					// die;   

					if($userId)

					{ 

						$mob=$get_mobile;

						$datee=strtotime(date("h:i:sa"))+600;

						$datee=date("Y-m-d h:i:s",$datee);

						$user_name = $ChechUser['email'];

						$otp= rand(1000,9999);

						$uid = array('id'=>$userId);

						$data_otp= array('otp'=>$otp,'otp_expire' => $datee);

						//$response= $this->send_sms($otp,$mob);

						$response= $this->send_sms($otp,$mob);

						$subject = 'CarePro OTP Code';

						$message = 'Your user verify OTP code:'.$otp; 

						$chk_mail= new_send_mail($message, $subject, $user_name,'');

						// echo $chk_mail;

						// die;

						$otp_id = $this->common_model->updateRecords('cp_users',$data_otp,$uid);

						//$userData = $data;

						$userData = $this->common_model->getSingleRecordById('cp_users', array('id' => $userId));

						// echo "<pre>";

						// print_r($userData);

						// die;

						$UsrData=array(

							'id'=>$userData['id'] ,

							'name'=>$userData['name'] ,

							'lastname'=>$userData['lastname'],

							'dob'=>	$userData['dob'] ,

							'username'=>$userData['username'] ,

							'city'=>$userData['city'] , 

							'state'=>$userData['state'] , 

							'zipcode'=>$userData['zipcode'] , 

							'company_school'=>$userData['company_school'] ,	 

							'email'	 =>$userData['email'] ,

							'mobile'=> $userData['mobile'] ,

							'password'	=>$userData['password'] ,

							'profile_image'	=>$userData['profile_image'] , 

							'device_id'	=>$userData['device_id'] ,

							'device_type'=>$userData['device_type'] , 

							'device_token'=>$userData['device_token'] , 

							'user_role'	=>$userData['user_role'] ,

							'act_link'=>$userData['act_link'] ,

							'status'=>$userData['status'] ,

							'Otp_verification' =>$userData['status'],

							'user_verification_status'=> $userData['user_verify_by_app'],

							'conversation_status'=>$userData['conversation_status'] ,

							'create_user'=>$userData['create_user'] ,

							'update_user'=>$userData['update_user'] ,

							'otp'=>$userData['otp'] ,

							'otp_expire'=>$userData['otp_expire'] ,

							'country_code'=>$userData['country_code'] ,

							'mobile_with_code'=>$userData['mobile_with_code'], 

							'gender'=>$userData['gender'] ,

							'address'=>$userData['address'] ,

							'user_id'=>$userData['user_id'] ,         

						);

						$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' =>'Verify successfully.', 'response' => $UsrData);

					}

					// else{

					// 	$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again','response' => array('message' =>'Some error occured, please try again'));

					// }

				}

			}else{

				$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Please check entered email address is wrong.','response' => array('message' =>'Please check entered email address is wrong.'));

				// echo "email not found";

				// die;

			}

		}

		$this->response($resp);

	}

	public function old_signup_post() {

		$pdata = file_get_contents("php://input");

		$data = json_decode($pdata, true);

		$mob = $data['mobile_no'];

		$required_parameter = array("user_roll","name","lastname","dob","username","email","mobile_no","country_code","password","confirm_password","device_id","device_type");

		$chk_error = check_required_value($required_parameter, $data);

		if ($chk_error) {

			$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' =>  'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param'])));

			$this->response($resp);

		}

		if(!$this->isValidEmail($data['email'])){

			$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'input must have valid email in the text', 'response' => array('message' => 'input must have valid email in the text'));

			$this->response($resp);

		}

		if(!is_numeric($data['mobile_no'])){

			$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Input must have numbers only', 'response' => array('message' => 'Input must have numbers only'));

			$this->response($resp);

		}

		if($data['password'] != $data['confirm_password']) {

			$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Input must be the same as the password', 'response' => array('message' => 'Input must be the same as the password'));

			$this->response($resp);

		}else{

			$check_email = $this->common_model->getRecordCount('cp_users', array('email' => $data['email'],'status!='=>3));

			if($check_email > 0){

// $resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'This email is already exit', 'response' => array('message' =>'This email is already exit'));

				$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'This email is already exit, please login.', 'response' => array('message' =>'This email is already exit, please login.'));

				$this->response($resp);

				exit;

			}

			$check_username = $this->common_model->getRecordCount('cp_users', array('username' => $data['username'],'status!='=>3));

			if($check_username > 0){

// $resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'This email is already exit', 'response' => array('message' =>'This email is already exit'));

				$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'This userid is already exit, please login.', 'response' => array('message' =>'This userid is already exit, please login.'));

				$this->response($resp);

				exit;

			}

			$data = array(

				'username' => (!empty($data['username']) ? $data['username'] : ''),

				'name' => (!empty($data['name']) ? $data['name'] : ''),

				'lastname' => (!empty($data['lastname']) ? $data['lastname'] : ''), 

				'dob' => (!empty($data['dob']) ? $data['dob'] : ''),

				'email' => (!empty($data['email']) ? $data['email'] : ''),

				'mobile' => (!empty($data['mobile_no']) ? $data['mobile_no'] : ''),

				'password' => (!empty($data['password']) ? md5($data['password']) : ''),

				'device_id' => (!empty($data['device_id']) ? $data['device_id'] : ''),

				'device_type' => (!empty($data['device_type']) ? $data['device_type'] : ''),

				'create_user' => date('Y-m-d h:i:s'),

				'status'      => 0,

				'user_role'      =>$data['user_roll'],

				'country_code'      =>$data['country_code'],

				'mobile_with_code'      =>$data['country_code'].$data['mobile_no'],

			);

			$userId = $this->common_model->addRecords('cp_users', $data);

			if($userId)

			{

				$datee=strtotime(date("h:i:sa"))+600;

				$datee=date("Y-m-d h:i:s",$datee);

				$user_name = $data['email'];

				$otp= rand(1000,9999);

				$uid = array('id'=>$userId);

				$data_otp= array('otp'=>$otp,'otp_expire' => $datee);

//echo $data['mobile_no'];die;

				$response= $this->send_sms($otp,$mob);

				$subject = 'CarePro OTP Code';

				$message = 'Your user verify OTP code:'.$otp;

// $chk_mail= sendEmail($user_name,$subject,$message);

				$chk_mail= new_send_mail($message, $subject, $user_name,'');

				$otp_id = $this->common_model->updateRecords('cp_users',$data_otp,$uid);

				$userData = $this->common_model->getSingleRecordById('cp_users', array('id' => $userId));

				$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' =>'Successfully registered', 'response' => $userData);

			}else{

				$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again','response' => array('message' =>'Some error occured, please try again'));

			}

		}

		$this->response($resp);

	}

	/*Verify Otp*/

	public function verifyotp_post() {

		/* Check for required parameter */

		// $pdata = file_get_contents("php://input");

		// $data = json_decode( $pdata,true );

		$data = $this->param;

		$object_info = $data;

		$required_parameter = array('email','otp');

		$chk_error = check_required_value($required_parameter, $object_info);

		if ($chk_error) {

			$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param'])));

			$this->response($resp);

		}

		$check_email = $this->common_model->getRecordCount('cp_users', array('email' => $data['email']));

//echo $this->db->last_query();die;

// echo '<pre>';print_r($check_email);die;

		if($check_email == 0){

			$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => "The inserted Email is not associated with any account",'response' => array('message' => "The inserted Email is not associated with any account"));

		}else{

			//$resultuser=$this->common_model->getSingleRecordById('cp_users',array('email' => $data['email'],'otp' => $data['otp']));

			//$data['otp']=1111;

			$resultuser=$this->common_model->getSingleRecordById('cp_users',array('email' => $data['email'],'otp' => $data['otp']));

			//echo '<pre>';print_r($resultuser);die;

//2019-07-03 04:11:22

//2019-07-03 03:45:00

			if(!empty($resultuser)) {

				$today_date = date("Y-m-d h:i:s");

//echo $resultuser['otp_expire'] .'>='. $today_date;die;

				if($resultuser['otp_expire'] >= $today_date){

					$updateArr = array('status' => 1);

$condition = array('email' => $data['email']);//die;

$this->common_model->updateRecords('cp_users', $updateArr, $condition);

// New changes

$user_details = array(

	'id'=>$resultuser['id'] ,

	'name'=>$resultuser['name'] ,

	'lastname'=>$resultuser['lastname'],

	'dob'=>	$resultuser['dob'] ,

	'username'=>$resultuser['username'] ,

	'city'=>$resultuser['city'] , 

	'state'=>$resultuser['state'] , 

	'zipcode'=>$resultuser['zipcode'] , 

	'company_school'=>$resultuser['company_school'] ,	 

	'email'	 =>$resultuser['email'] ,

	'mobile'=> $resultuser['mobile'] ,

	'password'	=>$resultuser['password'] ,

	'profile_image'	=>$resultuser['profile_image'] , 

	'device_id'	=>$resultuser['device_id'] ,

	'device_type'=>$resultuser['device_type'] , 

	'device_token'=>$resultuser['device_token'] , 

	'user_role'	=>$resultuser['user_role'] ,

	'act_link'=>$resultuser['act_link'] ,

	'status'=>$resultuser['status'] ,

	'Otp_verification' =>$resultuser['status'],

	'conversation_status'=>$resultuser['conversation_status'] ,

	'create_user'=>$resultuser['create_user'] ,

	'update_user'=>$resultuser['update_user'] ,

	'otp'=>$resultuser['otp'] ,

	'otp_expire'=>$resultuser['otp_expire'] ,

	'country_code'=>$resultuser['country_code'] ,

	'mobile_with_code'=>$resultuser['mobile_with_code'], 

	'gender'=>$resultuser['gender'] ,

	'address'=>$resultuser['address'] ,

	'user_id'=>$resultuser['user_id'] ,																			

);

// End    

$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => "Your OTP has been verified", 'response' => $user_details);

}else {

	$resp = array('status_code' => FAILURE,'status' => 'false','message' => "Your OTP has been expired",'response' => array('message' => "Your OTP has been expired"));

}

}

else {

	$resp = array('status_code' => FAILURE,'status' => 'false','message' => "The OTP entered is incorrect",'response' => array('message' => "The OTP entered is incorrect"));

}

}

$this->response($resp);

}

/*Forgot Password*/

public function forgot_password_post() {

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('email');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param'])));

		$this->response($resp);

	}

	$user_name = $data['email'];

	$type = $data['type'];

	$conditions = array('email' => $user_name);

	$users = $this->common_model->getSingleRecordById('cp_users',$conditions);

	if(!empty($users))

	{

		$otp= rand(1000,9999);

		$datee=strtotime(date("h:i:sa"))+600;

		$datee=date("Y-m-d h:i:s",$datee);

		$post_data= array('otp'=>$otp,'status'=>0,'otp_expire' => $datee);

		$where_condition = array('id' => $users['id']);

		$this->common_model->updateRecords('cp_users', $post_data, $where_condition);

		$subject = 'Care pro password code';

//$message = 'Your reset password OTP:'.$otp;

		$base_url = base_url().'assets/images/logo_img1.png';

		$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

		<html xmlns="http://www.w3.org/1999/xhtml">

		<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<title>Care Pro</title> <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">

		</head>

		<body style="margin: 0;font-family:"open sans";">

		<div style="max-width: 700px;margin: 30px auto 0; background:#f4f4f4;">

		<div style="border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;">

		<a href="#" target="_blank" style="display:inline-block;padding:20px 0;margin: 0 auto 40px;">

		<img style="max-width:200px;width:100%;margin: 0 auto;" src='.$base_url.'>

		</a>

		</div>

		<div style="border: none;padding:0; max-width: 580px; margin:-40px auto 0px; border-radius:4px;">

		<div class="temp_cont" style="width:100%; margin:0 auto;">

		<div style="padding:25px 30px 25px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px;     border: 1px solid #cccccc6e;">

		<h2 style="font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;">

		"Forgot Password"

		</h2>

		<p align="center" style="font-size: 16px;line-height: 25px;font-family: open sans;color: #515151;margin-top: 0;">

		<span style="color:#000;text-transform: uppercase;margin-bottom:15px;display:block;text-align: center;font-size: 15px;font-weight:600">Hi '.$users['username'].'</span>

		<span style="color:#000;font-weight:600;text-transform:uppercase;margin-bottom: 5px;display:block;text-align:center;font-size:21px;">One time reset OTP password:</span>

		</p>

		<p style="text-align:center;color:#000;">

		'.$otp.'

		</p>

		</div>

		<div style="background:#2f8dccdb;padding: 13px 10px 13px;color:#fff;text-align:center;     border-radius: 3px; margin-bottom:25px; display:inline-block; width: 100%;">

		<p style="font-size: 13px;margin: 0 0 8px;">Thank You for visit</p>

		<p style="font-size: 13px;margin:0;">

		<a style="color:#fff;font-weight:600;text-decoration:none;" href="#" target="_blank">https://mobivdigital.com/carepro/</a>

		</p>

		<div style="display:block;height: 1px;background:#fff;margin:10px 0;"></div>

		<p style="font-size: 13px;margin:0 0 8px;">Care Pro</p>

		<p style="font-size: 13px;margin:0;">

		<a style="color:#fff;font-weight:600;text-decoration:none;" href="javascript:void(0)">EMAIL: info@carepro.com/</a>

		</p>

		</div>

		</div>

		</div>

		</div>

		</body>

		</html>';

		$from_m = "info@carepro.com";

		$fromname = "Carepro";

//$chk_mail= sendEmail($user_name,$subject,$message);

//$chk_mail= new_send_mail($message, $subject, $user_name,'');

		$chk_mail=  socialEmail($user_name,$from_m,$fromname,$subject,$message);

		$response= $this->send_sms($otp,$users['mobile']);

// echo "<pre>";

// print_r($response);

// die;

		if($chk_mail)

		{

			if($type == 1){

				$msgs = "Your password OTP successfully sent please check your email.";

			}

			else{

				$msgs = "Your password reset OTP successfully sent please check your email.";

			}

			$user_info = array('email' => $users['email'],'mobile_no' => $users['mobile'],'message' => $msgs);

// echo "<pre>";

// print_r($user_info);

// die;

			echo json_encode(array('status_code' => SUCCESS,'status' => 'true','message'=>$msgs,'response'=> $user_info));

		}

		else

		{

			$error = show_error($this->email->print_debugger());

			echo json_encode(array('status_code' => FAILURE,'status' => 'false','message'=>"your password reset OTP not send",'response'=> array('message' => "your password reset OTP not send")));

		}

	}

	else

	{

		echo json_encode(array('status_code' => FAILURE,'status' => 'false','message'=>"The inserted Email is not associated with any account",'response'=> array('message' =>"The inserted Email is not associated with any account")));

	}

}

/*Reset Password*/

public function resetpassword_post()

{

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('email', 'password', 'confirm_password');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	// security changes start

	$password = trim($data['password']);

	$regex_lowercase = '/[a-z]/';

	$regex_uppercase = '/[A-Z]/';

	$regex_number = '/[0-9]/';

	$regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';

		// if (preg_match_all($regex_lowercase, $password) < 1)

		// {

		// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => message(43), 'response' => array('message' => message(43)));

		// 	$this->response($resp);

		// }

	if(preg_match_all($regex_lowercase, $password) < 1) {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must contains lowercase letter.', 'response' => array('message' => 'Your password must contains lowercase letter.'));

		$this->response($resp);

	}

	if(preg_match_all($regex_uppercase, $password) < 1) {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must contains uppercase letter.', 'response' => array('message' => 'Your password must contains uppercase letter.'));

		$this->response($resp);

	}

	if(preg_match_all($regex_number, $password) < 1) {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must contains number.', 'response' => array('message' => 'Your password must contains number.'));

		$this->response($resp);

	}

	if(preg_match_all($regex_special, $password) < 1) {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must contains special character.', 'response' => array('message' => 'Your password must contains special character.'));

		$this->response($resp);

	}

	if(strlen($password) < 6) {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must be at least 6 characters long.', 'response' => array('message' => 'Your password must be at least 6 characters long.'));

		$this->response($resp);

	}

	if(strlen($password) > 30) {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must not exceed 30 characters long.', 'response' => array('message' => 'Your password must not exceed 30 characters long.'));

		$this->response($resp);

	}

		// security changes end

	if($data['password'] != $data['confirm_password']) {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => "Input must be the same as the password", 'response' => array('message' => "Input must be the same as the password"));

		$this->response($resp);

	}

	$email = $data['email'];

	/* Check for user id */

	$check_key = $this->common_model->getRecordCount('cp_users', array('email' => $email));

	if($check_key == 1) {

		/* Change password */

		$condition = array('email' => $email);

		$updateArr = array('password' => md5($data['password']));

		$this->common_model->updateRecords('cp_users', $updateArr, $condition);

		/* Response array */

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => "Your password reset successfully", 'response' => array('message' => "Your password reset successfully"));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => "Invalid details, please try again", 'response' => array('message' => "Invalid details, please try again"));

	}

	$this->response($resp);

}

/*send mail */

public function send_mail_post() {

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('to_email','subject','message','user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$post_data = array

	(

		'to_email'=> $data['to_email'],

		'subject'=> $data['subject'],

		'message'=> $data['message'],

		'user_id'=> $data['user_id'],

		'create_email' => date('Y-m-d H:i:s'),

		//'create_email' => date('Y-m-d'),

		'email_status' => 1,

	);

	// New changes for email template start 

	$send_touserData = $this->common_model->getSingleRecordById('cp_users', array('email' => $data['to_email']));

	$send_tousername = $send_touserData['username'];

	$send_byuserData = $this->common_model->getSingleRecordById('cp_users', array('id' => $data['user_id']));

	$send_byname = $send_byuserData['name'];

	$emailId = $this->common_model->addRecords('cp_emails', $post_data);

	$getemail_Data = $this->common_model->getSingleRecordById('cp_emails', array('email_id' => $emailId));

	$create_email_date = $getemail_Data['create_email'];

	$currentDateTime = $create_email_date;

	$newDateTime = date('d-m-y, h:i A', strtotime($currentDateTime));

	// New changes for email template end

	if($emailId)

	{

		$user_name = $data['to_email'];

		$subject = $data['subject'];

		//$message = $data['message'];

		$message ="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

		<html xmlns='http://www.w3.org/1999/xhtml'>

		<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>

		<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>

		</head>

		<body style='margin: 0;font-family:'open sans';'>

		<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;'>

		<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>

		<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin: 0 auto 0px;'>

		<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

		</a>

		</div>

		<div style='border:none; padding:0px 60px; max-width:580px; margin:-40px auto 0px; border-radius:0px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>

		<div class='temp_cont' style='width:100%; margin:0 auto;'>

		<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>

		<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

		<span style='display:block;width:100%;'>

		<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>

		</span>

		Send mail

		</h2>

		<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

		Hi &nbsp;".$send_tousername.",

		</h3>

		<style>

						#customers td, #customers th {

		border:1px solid #ddd;

		padding:8px;

	}

						#customers th {

	padding-top:8px;

	padding-bottom:8px;

	text-align:left;

	background-color:#4CAF50;

	color:white;

}

</style>

<table id='customers' style='width:100%; border-collapse: collapse;'>

<tr>

<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$send_byname."</td>

</tr>						  

<tr>

<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Date and Time:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$newDateTime."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Description:</th>

<td style='font-size:13px; padding:8px;border:1px solid #ddd;'>".$data['message']."</td>

</tr>

</table>

</div>

<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%; box-sizing:border-box;'>

<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>

</p>						

<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>

<p style='font-size: 13px;margin:0 0 8px;'>Care Pro</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>EMAIL: info@carepro.com/</a>

</p>

</div>

</div>

</div>

</div>

</body>

</html>";

//$message = $data['message'];

// echo $message;

// die;

$chk_mail= sendEmail($user_name,$subject,$message);

// echo $chk_mail;

// die;

$emailData = $this->common_model->getSingleRecordById('cp_emails', array('email_id' => $emailId));

if($emailData['create_email'] == "0000-00-00"){

	$emails_create = $emailData['create_email'];

}else{

	$emails_create = date("M d, Y", strtotime($emailData['create_email']));

}

$emailData1 = array(

	'to_email' => $emailData['to_email'],

	'subject' => $emailData['subject'],

	'message' => $emailData['message'],

	'user_id' => $emailData['user_id'],

	'create_date' => $emails_create,

);

$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Message sent.', 'response' => $emailData1);

} else

{

	$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

}

$this->response($resp);

}

/*list mail */

public function emaillist1_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$limit = 5;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));

		$this->response($resp);

	}

	if($data['page_no']==''){

		$data['page_no']=1;

	}

	$emailCount =$this->common_model->getEmailCount($data['user_id']);

	$start  = ($data['page_no']-1)*$limit;

	$pages = ceil(count($emailCount) / $limit);

	$emailData =$this->common_model->getEmails($start,$limit,$data['user_id']);

	if(!empty($emailData)){

		foreach ( $emailData as $row ){

			if($emailData['create_email'] == "0000-00-00"){

				$emails_create = $row['create_email'];

			}else{

				$emails_create = date("M d, Y", strtotime($row['create_email']));

			}

			$attachment = $this->orderImage($row['email_id']);

			$email[] = array(

				'id' => $row['email_id'],

				'user_id' => $row['user_id'],

				'to_email' => $row['to_email'],

				'subject' => $row['subject'],

				'message' => $row['message'],

				'create_date' => $row['create_email'],

				'attachment' => $attachment,

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('email_data' => $email));

	}

	else {

		$email = array();

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Email not found','response' => array('message' => 'Email not found','email_data' => $email));

	}

	$this->response($resp);

}

public function  orderImage($email_id){

	$this->db->select('attachment_file');

	$this->db->from('cp_emails_attachment');

	$this->db->where('email_id',$email_id);

	$query = $this->db->get();

	$result = $query->result_array();

	foreach($result as $result_details){

		$imageArray[]  = base_url().'uploads/email/'.$result_details['attachment_file'];

	}

	$result_new = implode(",",$imageArray);

	return $result_new;

}

/*list mail */

public function inboxlist1_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$limit = 5;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));

		$this->response($resp);

	}

	if($data['page_no']==''){

		$data['page_no']=1;

	}

	$emailId = $this->common_model->getSingleRecordById('cp_users', array('id' => $data['user_id']));

	$emailId = $emailId['email'];

	$emailCount =$this->common_model->getEmailCount1($emailId);

//echo $emailCount;die;

	$start  = ($data['page_no']-1)*$limit;

	$pages = ceil(count($emailCount) / $limit);

	$emailData =$this->common_model->getEmails1($start,$limit,$emailId);

// print_r("<pre/>");

//print_r($emailData);

// die;

	if(!empty($emailData)){

		foreach ( $emailData as $row ){

			if($emailData['create_email'] == "0000-00-00"){

				$emails_create = $row['create_email'];

			}else{

				$emails_create = date("M d, Y", strtotime($row['create_email']));

			}

			$attachment = $this->orderImage($row['email_id']);

			$email[] = array(

				'id' => $row['email_id'],

				'user_id' => $row['user_id'],

				'to_email' => $row['to_email'],

				'subject' => $row['subject'],

				'message' => $row['message'],

				'create_date' => $emails_create,

				"attachment"=>$attachment

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('email_data' => $email));

	}

	else {

		$email = array();

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Email not found','response' => array('message' => 'Email not found','email_data' => $email));

	}

	$this->response($resp);

}

/*photo list  */

public function photolist_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$limit = 5;

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));

		$this->response($resp);

	}

	if($data['page_no']==''){

		$data['page_no']=1;

	}

	$emailCount =$this->common_model->getPhotoCount($data['user_id']);

	$start  = ($data['page_no']-1)*$limit;

	$pages = ceil(count($emailCount) / $limit);

	$emailData =$this->common_model->getPhoto($start,$limit,$data['user_id']);

//$emailData =$this->common_model->getAllRecordsById("cp_user_photo",array('u_photo_status'=>1,'user_id'=>$data['user_id']));

	if(!empty($emailData)){

		foreach ( $emailData as $row ){

			$fav  = $this->getfav($row['u_photo_id']);

			$email[] = array(

				'id' => $row['u_photo_id'],

				'user_id' => $row['user_id'],

				'photo' => base_url().'uploads/photos/'.$row['u_photo'],

				'fav' => $fav,

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('photo_data' => $email));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Email not found','response' => array('message' => 'Email not found'));

	}

	$this->response($resp);

}

public function  getfav($photo_id){

	$this->db->select('*');

	$this->db->from('cp_photo_favourite');

	$this->db->where('photo_id',$photo_id);

//$this->db->where('order_id',$order_id);

	$query = $this->db->get();

	if($query->num_rows() > 0){

		return "1";

	}

	return "0";

}

public function  getArticleFav($article_id){

	$this->db->select('*');

	$this->db->from('cp_article_favourite');

	$this->db->where('article_id',$article_id);

//$this->db->where('order_id',$order_id);

	$query = $this->db->get();

	if($query->num_rows() > 0){

		return "1";

	}

	return "0";

}

public function  getArticleFavuserby($article_id,$user_id){

	$this->db->select('*');

	$this->db->from('cp_article_favourite');

	$this->db->where('article_id',$article_id);

	$this->db->where('user_id',$user_id);

//$this->db->where('order_id',$order_id);

	$query = $this->db->get();

//echo $this->db->last_query();die;

	if($query->num_rows() > 0){

		return "1";

	}

	return "0";

}

public function  getMusicFav($music_id){

	$this->db->select('*');

	$this->db->from('cp_music_favorite');

	$this->db->where('music_id',$music_id);

//$this->db->where('order_id',$order_id);

	$query = $this->db->get();

	if($query->num_rows() > 0){

		return "1";

	}

	return "0";

}

public function  getMusicUserFav($music_id,$user_id){

	$this->db->select('*');

	$this->db->from('cp_music_favorite');

	$this->db->where('music_id',$music_id);

	$this->db->where('user_id',$user_id);

//$this->db->where('order_id',$order_id);

	$query = $this->db->get();

	if($query->num_rows() > 0){

		return "1";

	}

	return "0";

}

public function  getMovieFav($movie_id){

	$this->db->select('*');

	$this->db->from('cp_movie_favorite');

	$this->db->where('movie_id',$movie_id);

//$this->db->where('order_id',$order_id);

	$query = $this->db->get();

	if($query->num_rows() > 0){

		return "1";

	}

	return "0";

}

public function  getMovieUserFav($movie_id,$user_id){

	$this->db->select('*');

	$this->db->from('cp_movie_favorite');

	$this->db->where('movie_id',$movie_id);

	$this->db->where('user_id',$user_id);

//$this->db->where('order_id',$order_id);

	$query = $this->db->get();

	if($query->num_rows() > 0){

		return "1";

	}

	return "0";

}

public function  getRestFav($rest_id){

	$this->db->select('*');

	$this->db->from('cp_fav_restaurant');

	$this->db->where('restaurant_id',$rest_id);

//$this->db->where('order_id',$order_id);

	$query = $this->db->get();

	if($query->num_rows() > 0){

		return "1";

	}

	return "0";

}

public function  getBankFav($bank_id){

	$this->db->select('*');

	$this->db->from('cp_fav_banks');

	$this->db->where('bank_id',$bank_id);

//$this->db->where('order_id',$order_id);

	$query = $this->db->get();

	if($query->num_rows() > 0){

		return "1";

	}

	return "0";

}

public function  getGroceryFav($grocery_id){

	$this->db->select('*');

	$this->db->from('cp_fav_grocery');

	$this->db->where('grocery_id',$grocery_id);

//$this->db->where('order_id',$order_id);

	$query = $this->db->get();

	if($query->num_rows() > 0){

		return "1";

	}

	return "0";

}

/*delete mail */

public function emaildelete1_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('email_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$ids = explode(',',$data['email_id']);

	$emailid =$this->common_model->deleteEmails($ids);

	if($emailid) {

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('email_data' => "Message successfully deleted"));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Email not found','response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

/*delete mail */

public function emailInboxDelete_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('email_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$ids = explode(',',$data['email_id']);

	$emailid =$this->common_model->deleteEmails1($ids);

	if($emailid) {

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('email_data' => "Message successfully deleted"));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Email not found','response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

/* add photo */

public function addPhoto_post() {

	//$data = $_POST;

	// print_r($_POST);

	// die;

	$data = $this->param;  

	// print_r($data);

	// die;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	if(!empty($_FILES['photo_image']['name'])) {

		// echo $_FILES['photo_image']['name'];

		// die;

		// security chnages start

		$imgfile=$_FILES['photo_image']['name'];

		$extension = substr($imgfile,strlen($imgfile)-4,strlen($imgfile));

		// echo $imgfile;

		// die;

            // allowed extensions

		$allowed_extensions = array(".jpg",".jpeg",".png");

		//$allowed_extensions = array(".jpg",".jpeg");

		// security chnages start

		 if(in_array($extension,$allowed_extensions)) // security check start

		 {

		 	$random = $this->generateRandomString(10);

		 	$ext = pathinfo($_FILES['photo_image']['name'], PATHINFO_EXTENSION);

		 	$file_name = $random.".".$ext;

		 	$target_dir = "uploads/photos/";

		 	$target_file = $target_dir .$file_name;

		 	if (move_uploaded_file($_FILES["photo_image"]["tmp_name"], $target_file)){

		 		$photoFile = $file_name;

		 	}

		}else{  // security check end

			$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your image file is not valide. allow file types jpg, jpeg, png', 'response' => array('message' => 'Your image file is not valide. allow file types jpg, jpeg, png'));

			$this->response($resp); 

		}

	}

	$post_data = array

	(

		'user_id'=> $data['user_id'],

		'u_photo_created' => date('Y-m-d h:i:s'),

		'u_photo_status' => 1,

		'u_photo'        =>$photoFile,

	);

	$photoId = $this->common_model->addRecords('cp_user_photo', $post_data);

	if($photoId)

	{

		$photoData = $this->common_model->getSingleRecordById('cp_user_photo', array('u_photo_id' => $photoId));

		$photoData1 = array(

			'id' => $photoData['u_photo_id'],

			'user_id' => $photoData['user_id'],

			'image_url'=>base_url().'uploads/photos/'.$photoData['u_photo']

		);

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'photo added successfully', 'response' => $photoData1);

	}else{

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => message(18)));

	}

	$this->response($resp);

}

public function generateRandomString($length = 5) {

	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

	$charactersLength = strlen($characters);

	$randomString = '';

	for ($i = 0; $i < $length; $i++) {

		$randomString .= $characters[rand(0, $charactersLength - 1)];

	}

	return $randomString;

}

/* add fav photo */

public function addFavPhoto_post() {

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id','photo_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$FavPhotoData1 = $this->common_model->getSingleRecordById('cp_photo_favourite', array('user_id' => $data['user_id'],"photo_id"=>$data['photo_id']));

	if(empty($FavPhotoData1)){

		$post_data = array

		(

			'user_id'=> $data['user_id'],

			'photo_id'=> $data['photo_id'],

			'photo_fav_created' => date('Y-m-d h:i:s'),

			'favourite' => 1,

		);

		$FavPhotoId = $this->common_model->addRecords('cp_photo_favourite', $post_data);

		if($FavPhotoId) {

//$FavPhotoData = $this->common_model->getSingleRecordById('cp_photo_favourite', array('photo_fav_id' => $FavPhotoId));

			$resp = array('status_code' => SUCCESS,'status' => 'true','fav_status' =>'1', 'message' => 'Favorite successfully', 'response' => array('fav_data' => "Favorite successfully"));

		}else{

			$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

		}

	}else{

		$this->db->delete('cp_photo_favourite', array("user_id"=>$data['user_id'],"photo_id"=>$data['photo_id']));

		$FavPhotoId = $this->db->affected_rows();

		if($FavPhotoId) {

//$FavPhotoData = $this->common_model->getSingleRecordById('cp_photo_favourite', array('photo_fav_id' => $FavPhotoId));

			$resp = array('status_code' => SUCCESS,'status' => 'true','fav_status' =>'0', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));

		}else{

			$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

		}

	}

	$this->response($resp);

}

/* favourite photo list  */

public function favphotolist_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));

		$this->response($resp);

	}

	$favData = $this->common_model->jointwotable('cp_photo_favourite','photo_id','cp_user_photo','u_photo_id',array('cp_photo_favourite.user_id'=>$data['user_id']),'*');

	if(!empty($favData)){

		foreach ( $favData as $row ){

			$favphoto[] = array(

				'id' => $row->photo_fav_id,

				'user_id' => $row->user_id,

				'photo' => base_url().'uploads/photos/'.$row->u_photo,

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('favPhoto_data' => $favphoto));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Favourite Data not found','response' => array('message' => 'Favourite Data not found'));

	}

	$this->response($resp);

}

/*photo delete */

public function photodelete_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('photo_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$ids = explode(',',$data['photo_id']);

	$photoid =$this->common_model->deletePhotos($ids);

	if($photoid) {

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('photo_data' => "photo deleted succesfully"));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'photo not found','response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

/* fav photo delete */

public function favphotodelete_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('fav_photo_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$ids = explode(',',$data['fav_photo_id']);

	$photoid =$this->common_model->deletePhotosfav($ids);

	if($photoid) {

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('photo_data' => "Unfavorite succesfully"));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Some error occured, please try again','response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

/*photo list  */

public function allPhotoList_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));

		$this->response($resp);

	}

	$emailData =$this->common_model->getAllRecordsById("cp_user_photo",array('u_photo_status'=>1,'user_id'=>$data['user_id']));

	if(!empty($emailData)){

		foreach ( $emailData as $row ){

			$fav  = $this->getfav($row['u_photo_id']);

			$email[] = array(

				'id' => $row['u_photo_id'],

				'user_id' => $row['user_id'],

				'photo' => base_url().'uploads/photos/'.$row['u_photo'],

				'fav' => $fav,

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('photo_data' => $email));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Photo not found','response' => array('message' => 'Photo not found'));

	}

	$this->response($resp);

}

public function sharePhoto_post(){  

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('sender_id',"receiver_ids","photo_ids");

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	} else {

		$sender_id = $data['sender_id'];

		$receiver_ids = $data['receiver_ids'];

		$photo_ids = $data['photo_ids'];

		$description = $data['description'];

		$sender_info = $this->common_model->getSingleRecordById('cp_users',array('id'=>$sender_id));

		//$user_arr = $this->common_model->getWhereIn('cp_users','email',$receiver_ids);

		$photo_arr = $this->common_model->getWhereIn('cp_user_photo','u_photo_id',$photo_ids);

		$photoarr = array_chunk($photo_arr,2);

		if (!empty($receiver_ids)) {

			//foreach ($user_arr as $key => $value) {

				$user_name = $receiver_ids;//$value['email'];

				$subject = 'Carepro - Share Photos';

				$message = '<!DOCTYPE html>

				<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

				<head>

				<meta charset="utf-8">

				<!-- utf-8 works for most cases -->

				<meta name="viewport" content="width=device-width">

				<!-- Forcing initial-scale shouldnt be necessary -->

				<meta http-equiv="X-UA-Compatible" content="IE=edge">

				<!-- Use the latest (edge) version of IE rendering engine -->

				<meta name="x-apple-disable-message-reformatting">

				<!-- Disable auto-scale in iOS 10 Mail entirely -->

				<title></title>

				<!-- The title tag shows in email notifications, like Android 4.4. -->

				<link href="https://fonts.googleapis.com/css?family=Work+Sans:200,300,400,500,600,700" rel="stylesheet">

				<!-- CSS Reset : BEGIN -->

				<style>

				html,

				body {

					margin: 0 auto !important;

					padding: 0 !important;

					height: 100% !important;

					width: 100% !important;

					background: #f1f1f1;

				}

							         * {

				-ms-text-size-adjust: 100%;

				-webkit-text-size-adjust: 100%;

			}

			div[style*="margin: 16px 0"] {

				margin: 0 !important;

			}

			table,

			td {

				mso-table-lspace: 0pt !important;

				mso-table-rspace: 0pt !important;

			}

			table {

				border-spacing: 0 !important;

				border-collapse: collapse !important;

				table-layout: fixed !important;

				margin: 0 auto !important;

			}

			img {

				-ms-interpolation-mode:bicubic;

			}

			a {

				text-decoration: none;

			}

							         *[x-apple-data-detectors],

			.unstyle-auto-detected-links *,

			.aBn {

				border-bottom: 0 !important;

				cursor: default !important;

				color: inherit !important;

				text-decoration: none !important;

				font-size: inherit !important;

				font-family: inherit !important;

				font-weight: inherit !important;

				line-height: inherit !important;

			}

			.a6S {

				display: none !important;

				opacity: 0.01 !important;

			}

			.im {

				color: inherit !important;

			}

			img.g-img + div {

				display: none !important;

			}

			@media only screen and (min-device-width: 320px) and (max-device-width: 374px) {

				u ~ div .email-container {

					min-width: 320px !important;

				}

			}

			@media only screen and (min-device-width: 375px) and (max-device-width: 413px) {

				u ~ div .email-container {

					min-width: 375px !important;

				}

			}

			@media only screen and (min-device-width: 414px) {

				u ~ div .email-container {

					min-width: 414px !important;

				}

			}

			</style>

			<!-- CSS Reset : END -->

			<!-- Progressive Enhancements : BEGIN -->

			<style>

			.primary{

				background: #2f89fc;

			}

			.bg_white{

				background: #ffffff;

			}

			.bg_light{

				background: #fafafa;

			}

			.bg_black{

				background: #000000;

			}

			.bg_dark{

				background: rgba(0,0,0,.8);

			}

			.email-section{

				padding:2.5em;

			}

			.btn{

				padding: 5px 15px;

				display: inline-block;

			}

			.btn.btn-primary{

				border-radius: 5px;

				background: #2f89fc;

				color: #ffffff;

			}

			.btn.btn-white{

				border-radius: 5px;

				background: #ffffff;

				color: #000000;

			}

			.btn.btn-white-outline{

				border-radius: 5px;

				background: transparent;

				border: 1px solid #fff;

				color: #fff;

			}

			h1,h2,h3,h4,h5,h6{

				font-family: Work Sans, sans-serif;

				color: #000000;

				margin-top: 0;

				font-weight: 400;

			}

			body{

				font-family: Work Sans, sans-serif;

				font-weight: 400;

				font-size: 15px;

				line-height: 1.8;

				color: rgba(0,0,0,.4);

			}

			a{

				color: #2f89fc;

			}

			table{

			}

			.logo h1{

				margin: 0;

			}

			.logo h1 a{

				color: #000000;

				font-size: 20px;

				font-weight: 700;

				text-transform: uppercase;

				font-family: Poppins, sans-serif;

			}

			.navigation{

				padding: 0;

			}

			.navigation li{

				list-style: none;

				display: inline-block;;

				margin-left: 5px;

				font-size: 13px;

				font-weight: 500;

			}

			.navigation li a{

				color: rgba(0,0,0,.4);

			}

			.hero{

				position: relative;

				z-index: 0;

			}

			.hero .text{

				color: rgba(0,0,0,.3);

			}

			.hero .text h2{

				color: #000;

				font-size: 30px;

				margin-bottom: 0;

				font-weight: 300;

			}

			.hero .text h2 span{

				font-weight: 600;

				color: #2f89fc;

			}

			.heading-section{

			}

			.heading-section h2{

				color: #000000;

				font-size: 28px;

				margin-top: 0;

				line-height: 1.4;

				font-weight: 400;

			}

			.heading-section .subheading{

				margin-bottom: 20px !important;

				display: inline-block;

				font-size: 13px;

				text-transform: uppercase;

				letter-spacing: 2px;

				color: rgba(0,0,0,.4);

				position: relative;

			}

			.heading-section .subheading::after{

				position: absolute;

				left: 0;

				right: 0;

				bottom: -10px;

				width: 100%;

				height: 2px;

				background: #2f89fc;

				margin: 0 auto;

			}

			.heading-section-white{

				color: rgba(255,255,255,.8);

			}

			.heading-section-white h2{

				font-family: 

				line-height: 1;

				padding-bottom: 0;

			}

			.heading-section-white h2{

				color: #ffffff;

			}

			.heading-section-white .subheading{

				margin-bottom: 0;

				display: inline-block;

				font-size: 13px;

				text-transform: uppercase;

				letter-spacing: 2px;

				color: rgba(255,255,255,.4);

			}

			.text-project{

				padding-top: 10px;

			}

			.text-project h3{

				margin-bottom: 0;

			}

			.text-project h3 a{

				color: #000;

			}

			.footer{

				color: rgba(255,255,255,.5);

			}

			.footer .heading{

				color: #ffffff;

				font-size: 20px;

			}

			.footer ul{

				margin: 0;

				padding: 0;

			}

			.footer ul li{

				list-style: none;

				margin-bottom: 10px;

			}

			.footer ul li a{

				color: rgba(255,255,255,1);

			}

			@media screen and (max-width: 500px) {

			}

			</style>

			</head>

			<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #222222;">

			<center style="width: 100%; background-color: #f1f1f1;">

			<div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">

			&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;

			</div>

			<div style="max-width: 600px; margin: 0 auto;" class="email-container">

			<!-- BEGIN BODY -->

			<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">

			<tr>

			<td valign="top" class="bg_white" style="padding: 1em 2.5em;">

			<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">

			<tr>

			<td class="logo" style="text-align: center;">

			<h1><a href="#">Carepro</a></h1>

			</td>

			</tr>

			</table>

			</td>

			</tr>

			<!-- end tr -->

			<tr>

			<td valign="middle" class="hero hero-2 bg_white" style="padding: 4em 0;">

			<table>

			<tr>

			<td>

			<div class="text" style="padding: 0 3em; text-align: center;">

			<h2>'.(!empty($sender_info["name"]) ? $sender_info["name"]." ".$sender_info["lastname"] : "User").' share some photo with you. Please have a look.</h2>

			</div>

			</td>

			</tr>

			</table>

			</td>

			</tr>

			<!-- end tr -->

			<tr>

			<td class="bg_white">

			<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">

			<tr>

			<td class="bg_white email-section">

			<div class="heading-section" style="text-align: center; padding: 0 30px;">

			<h2>Photos</h2>

			<!-- <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p> -->

			</div>

			<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">';

			if (!empty($photoarr)) {

				foreach ($photoarr as $k => $v) {

					if (!empty($v)) {

						$message = $message.'<tr>';

						foreach ($v as $k1 => $v1) {

							$photo_url = base_url()."/uploads/photos/".$v1["u_photo"];

							$message = $message.'<td valign="top" width="50%">

							<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">

							<tr>

							<td style="padding-top: 20px; padding-right: 10px;">

							<a href="#"><img src="'.$photo_url.'" alt="" style="width: 100%; max-width: 600px; height: auto; margin: auto; display: block;"></a>

							</td>

							</tr>

							</table>

							</td>';

						}

						$message = $message.'</tr>';

					}

				}

			} 

			$message = $message.'</table>

			</td>

			</tr>

			<!-- end: tr -->

			</table>

			</td>

			</tr>

			<tr>

			<td valign="middle" class="hero hero-2 bg_white" style="padding: 4em 0;">

			<table>

			<tr>

			<td>

			<div class="text" style="text-align: center;">

			<h2>'.(!empty($description) ? $description : "").'</h2>

			</div>

			</td>

			</tr>

			</table>

			</td>

			</tr>

			<!-- end:tr -->

			<!-- 1 Column Text + Button : END -->

			</table>

			<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">

			<!-- end: tr -->

			<tr>

			<td valign="middle" class="bg_black footer email-section">

			<table style="width: 100%;">

			<tr>

			<td valign="top" colspan="2" width="33.333%">

			<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">

			<tr>

			<td style="text-align: left; padding-right: 10px;">

			<p>&copy; 2020 Carepro. All Rights Reserved</p>

			</td>

			</tr>

			</table>

			</td>

			</tr>

			</table>

			</td>

			</tr>

			</table>

			</div>

			</center>

			</body>

			</html>';

			$chk_mail= new_send_mail($message,$subject,$user_name,'');

		//}

		}



	//echo $user_name; die;

		/*print_r($user_arr);

		print_r($photo_arr);*/

		//$this->common_model->getAllRecordsById($table,$conditions);

		$receiverids = implode(',', $receiver_ids);

		$photoids = implode(',', $photo_ids);

		$record_data = array(

			'sender_id' => $sender_id,

			'receiver_ids' => $receiverids,

			'photo_ids' => $photoids,

			'description' => (!empty($description) ? $description : ''),

			'status' => 1,

			'created_at' => date('Y-m-d H:i:s'),

			'created_at' => date('Y-m-d H:i:s')

		);

		$record_id = $this->common_model->addRecords('share_gallery',$record_data);

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Photo has been shared!');

		$this->response($resp);

	}

}

public function shareArticla_post(){

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('sender_id',"receiver_ids","article_id");

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	} else {

		$sender_id = $data['sender_id'];

		$receiver_ids = $data['receiver_ids'];

		$article_id = $data['article_id'];

		$sender_info = $this->common_model->getSingleRecordById('cp_users',array('id'=>$sender_id));

		$user_arr = $this->common_model->getWhereIn('cp_users','id',$receiver_ids);

		$article_info = $this->common_model->getSingleRecordById('cp_articles',array('article_id'=>$article_id));

		if (!empty($user_arr)) {

			foreach ($user_arr as $key => $value) {

				$user_name = $value['email'];

				$subject = 'Carepro - Share Article';

				$message = '<!DOCTYPE html>

				<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

				<head>

				<meta charset="utf-8">

				<meta name="viewport" content="width=device-width">

				<meta http-equiv="X-UA-Compatible" content="IE=edge">

				<meta name="x-apple-disable-message-reformatting">

				<title></title>

				<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,600,700|Lato:300,400,700" rel="stylesheet">

				<style>

				html,

				body {

					margin: 0 auto !important;

					padding: 0 !important;

					height: 100% !important;

					width: 100% !important;

					background: #f1f1f1;

				}

								         * {

				-ms-text-size-adjust: 100%;

				-webkit-text-size-adjust: 100%;

			}

			div[style*="margin: 16px 0"] {

				margin: 0 !important;

			}

			table,

			td {

				mso-table-lspace: 0pt !important;

				mso-table-rspace: 0pt !important;

			}

			table {

				border-spacing: 0 !important;

				border-collapse: collapse !important;

				table-layout: fixed !important;

				margin: 0 auto !important;

			}

			img {

				-ms-interpolation-mode:bicubic;

			}

			a {

				text-decoration: none;

			}

								         *[x-apple-data-detectors],

			.unstyle-auto-detected-links *,

			.aBn {

				border-bottom: 0 !important;

				cursor: default !important;

				color: inherit !important;

				text-decoration: none !important;

				font-size: inherit !important;

				font-family: inherit !important;

				font-weight: inherit !important;

				line-height: inherit !important;

			}

			.a6S {

				display: none !important;

				opacity: 0.01 !important;

			}

			.im {

				color: inherit !important;

			}

			img.g-img + div {

				display: none !important;

			}

			@media only screen and (min-device-width: 320px) and (max-device-width: 374px) {

				u ~ div .email-container {

					min-width: 320px !important;

				}

			}

			@media only screen and (min-device-width: 375px) and (max-device-width: 413px) {

				u ~ div .email-container {

					min-width: 375px !important;

				}

			}

			@media only screen and (min-device-width: 414px) {

				u ~ div .email-container {

					min-width: 414px !important;

				}

			}

			</style>

			<style>

			.primary{

				background: #448ef6;

			}

			.bg_white{

				background: #ffffff;

			}

			.bg_light{

				background: #fafafa;

			}

			.bg_black{

				background: #000000;

			}

			.bg_dark{

				background: rgba(0,0,0,.8);

			}

			.email-section{

				padding:2.5em;

			}

			.btn{

				padding: 5px 15px;

				display: inline-block;

			}

			.btn.btn-primary{

				border-radius: 30px;

				background: #448ef6;

				color: #ffffff;

			}

			.btn.btn-white{

				border-radius: 30px;

				background: #ffffff;

				color: #000000;

			}

			.btn.btn-white-outline{

				border-radius: 30px;

				background: transparent;

				border: 1px solid #fff;

				color: #fff;

			}

			h1,h2,h3,h4,h5,h6{

				font-family: Josefin Sans, sans-serif;

				color: #000000;

				margin-top: 0;

				font-weight: 400;

			}

			body{

				font-family: Josefin Sans, sans-serif;

				font-weight: 400;

				font-size: 15px;

				line-height: 1.8;

				color: rgba(0,0,0,.4);

			}

			a{

				color: #448ef6;

			}

			table{

			}

			.logo{

				margin: 0;

				display: inline-block;

				position: absolute;

				top: 10px;

				left: 0;

				right: 0;

				margin-bottom: 0;

			}

			.logo a{

				color: #fff;

				font-size: 24px;

				font-weight: 700;

				text-transform: uppercase;

				font-family: Josefin Sans, sans-serif;

				display: inline-block;

				border: 2px solid #fff;

				line-height: 1.3;

				padding: 10px 15px 4px 15px;

				margin: 0;

			}

			.logo h1 a span{

				line-height: 1;

			}

			.navigation{

				padding: 0;

			}

			.navigation li{

				list-style: none;

				display: inline-block;;

				margin-left: 5px;

				font-size: 13px;

				font-weight: 500;

			}

			.navigation li a{

				color: rgba(0,0,0,.4);

			}

			.hero{

				position: relative;

				z-index: 0;

			}

			.hero .overlay{

				position: absolute;

				top: 0;

				left: 0;

				right: 0;

				bottom: 0;

				width: 100%;

				background: #000000;

				z-index: -1;

				opacity: .3;

			}

			.hero .text{

				color: rgba(255,255,255,.9);

			}

			.hero .text h2{

				color: #fff;

				font-size: 40px;

				margin-bottom: 0;

				font-weight: 600;

				line-height: 1;

				text-transform: uppercase;

			}

			.hero .text h2 span{

				font-weight: 600;

				color: #448ef6;

			}

			.heading-section{

			}

			.heading-section h2{

				color: #000000;

				font-size: 28px;

				margin-top: 0;

				line-height: 1.4;

				font-weight: 700;

				text-transform: uppercase;

				letter-spacing: 1px;

			}

			.heading-section .subheading{

				margin-bottom: 20px !important;

				display: inline-block;

				font-size: 13px;

				text-transform: uppercase;

				letter-spacing: 2px;

				color: rgba(0,0,0,.4);

				position: relative;

			}

			.heading-section .subheading::after{

				position: absolute;

				left: 0;

				right: 0;

				bottom: -10px;

				width: 100%;

				height: 2px;

				background: #448ef6;

				margin: 0 auto;

			}

			.heading-section-white{

				color: rgba(255,255,255,.8);

			}

			.heading-section-white h2{

				font-family: 

				line-height: 1;

				padding-bottom: 0;

			}

			.heading-section-white h2{

				color: #ffffff;

			}

			.heading-section-white .subheading{

				margin-bottom: 0;

				display: inline-block;

				font-size: 13px;

				text-transform: uppercase;

				letter-spacing: 2px;

				color: rgba(255,255,255,.4);

			}

			.blog-entry{

				border: 1px solid red;

				padding-bottom: 30px !important !important;

			}

			.text-blog .meta{

				text-transform: uppercase;

				font-size: 13px;

				margin-bottom: 0;

			}

			.footer{

				color: rgba(255,255,255,.5);

			}

			.footer .heading{

				color: #ffffff;

				font-size: 20px;

			}

			.footer ul{

				margin: 0;

				padding: 0;

			}

			.footer ul li{

				list-style: none;

				margin-bottom: 10px;

			}

			.footer ul li a{

				color: rgba(255,255,255,1);

			}

			@media screen and (max-width: 500px) {

			}

			</style>

			</head>

			<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #222222;">

			<center style="width: 100%; background-color: #f1f1f1;">

			<div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">

			&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;

			</div>

			<div style="max-width: 600px; margin: 0 auto;" class="email-container">

			<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">

			<tr>

			<td class="bg_white email-section">

			<div class="heading-section" style="text-align: center; padding: 0 30px;">

			<h2>'.(!empty($sender_info["name"]) ? $sender_info["name"]." ".$sender_info["lastname"] : "User").' share article with you. Please have a look.</h2>

			</div>

			<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">

			<tr>

			<td style="padding-bottom: 30px;">

			<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">

			<td valign="middle" width="100%">

			<img src="'.base_url().'/uploads/articles/'.$article_info["artist_image"].'" alt="" style="width: 100px;max-width: 600px;height: auto;margin: auto auto 20px;display: block;border-radius: 50%;">

			<h3 class="name" style="text-align: center;">'.$article_info["artist_name"].'</h3>

			</td>

			</table>

			</td>

			</tr>

			</table>

			</td>

			</tr>

			<tr>

			<td class="bg_white">

			<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">

			<tr>

			<td>

			<a href="#"><img src="'.base_url().'/uploads/articles/'.$article_info["article_image"].'" alt="" style="width: 100%; max-width: 600px; height: auto; margin: auto; display: block;"></a>

			</td>

			</tr>

			<tr>

			<td class="text-blog" style="text-align: center; padding: 2em 2.5em">

			<p class="meta"><span>Posted on '.(!empty($article_info["article_created"]) ? date("d M Y",strtotime($article_info["article_created"])) : "N/A").'</span> </p>

			<h3 style="font-size: 24px;">'.$article_info["article_title"].'</h3>

			<p>'.$article_info["article_desc"].'</p>

			</td>

			</tr>

			</table>

			</td>

			</tr>

			</table>

			<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">

			<tr>

			<td valign="middle" class="bg_black footer email-section">

			<table>

			<tr>

			<td valign="top" width="33.333%">

			<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">

			<tr>

			<td style="text-align: left; padding-right: 10px;">

			<p>&copy; 2020 Carepro. All Rights Reserved</p>

			</td>

			</tr>

			</table>

			</td>

			<td valign="top" width="33.333%">

			<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">

			<tr>

			<td style="text-align: right; padding-left: 5px; padding-right: 5px;">

			<p>

			</p>

			</td>

			</tr>

			</table>

			</td>

			</tr>

			</table>

			</td>

			</tr>

			</table>

			</div>

			</center>

			</body>

			</html>';

			$chk_mail= new_send_mail($message,$subject,$user_name,'');

		}

	}

		/*print_r($user_arr);

		print_r($photo_arr);*/

		//$this->common_model->getAllRecordsById($table,$conditions);

		$receiverids = implode(',', $receiver_ids);

		$record_data = array(

			'sender_id' => $sender_id,

			'receiver_ids' => $receiverids,

			'article_id' => $article_id,

			'status' => 1,

			'created_at' => date('Y-m-d H:i:s'),

			'created_at' => date('Y-m-d H:i:s')

		);

		$record_id = $this->common_model->addRecords('share_article',$record_data);

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Article has been shared!');

		$this->response($resp);

	}

}

/* add reminder */

public function reminder_add_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

//$required_parameter = array('title',"date","time","user_id","reminder_before","repeat");

	$required_parameter = array('title',"date","time","user_id","reminder_before","repeat","snooze",'after_time','for_time');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$post_data = array

	(

		'user_id'=> $data['user_id'],

		'reminder_title'=> $data['title'],

		'reminder_date'=> $data['date'],

		'reminder_time'=> $data['time'],

		'reminder_before'=> $data['reminder_before'],

		'repeat'=> $data['repeat'],

		'snooze'=> $data['snooze'],

		'after_time'=> $data['after_time'],

		'for_time'=> $data['for_time'],

		'reminder_create' => date('Y-m-d H:i:s'),

	);

	$reminderId = $this->common_model->addRecords('cp_reminder', $post_data);

	if($reminderId){

		$reminderData = $this->common_model->getSingleRecordById('cp_reminder', array('reminder_id' => $reminderId));

		$reminderData1 = array(

			'id' => $reminderData['reminder_id'],

			'title' => $reminderData['reminder_title'],

			'Date' => $reminderData['reminder_date'],

			'Time' => $reminderData['reminder_time'],

			'user_id' => $reminderData['user_id'],

			'reminder_before' => $reminderData['reminder_before'],

			'repeat' => $reminderData['repeat'],

			'snooze'=> $reminderData['snooze'],

			'after_time'=> $reminderData['after_time'],

			'for_time'=> $reminderData['for_time'],

		);

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Reminder added successfully', 'response' => $reminderData1);

	}

	else

	{

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function show_reminder_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));

		$this->response($resp);

	}

	$reminderData = $this->common_model->getAllRecordsById('cp_reminder', array('user_id' => $data['user_id']));

	if(!empty($reminderData)){

		foreach ( $reminderData as $row ){

			if($row['reminder_date'] == "0000-00-00"){

				$reminder_date = $row['reminder_date'];

			}else{

				$reminder_date = date("M d, Y", strtotime($row['reminder_date']));

			}

			$reminder[] = array(

				'id' => $row['reminder_id'],

				'title' => $row['reminder_title'],

//'date' => $reminder_date,

				'date' => date('m-d-Y',strtotime($row['reminder_date'])),

				'time' => $row['reminder_time'],

				'user_id' => $row['user_id'],

				'reminder_before' => $row['reminder_before'],

				'repeat' => $row['repeat'],

				'snooze'=> $row['snooze'],

				'after_time'=> $row['after_time'],

				'for_time'=> $row['for_time'],

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('reminder_data' => $reminder));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Reminder Data not found','response' => array('message' => 'Reminder Data not found'));

	}

	$this->response($resp);

}

/*delete reminder */

public function reminderdelete_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('reminder_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$ids = explode(',',$data['reminder_id']);

	$reminderid =$this->common_model->deleteReminders($ids);

	if($reminderid)

	{

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('reminder_data' => "Reminder data deleted succesfully"));

	}

	else

	{

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Some error occured, please try again','response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function updatereminder_post()

{

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('title',"date","time","reminder_id","reminder_before","repeat","snooze",'after_time','for_time');

//$required_parameter = array('title',"date","time","user_id","reminder_before","repeat","snooze",'after_time','for_time');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$post_data = array

	(

		'reminder_title'=> $data['title'],

		'reminder_date'=>  $data['date'],

		'reminder_time'=>  $data['time'],

		'reminder_before'=>  $data['reminder_before'],

		'repeat'=>  $data['repeat'],

		'snooze'=> $data['snooze'],

		'after_time'=> $data['after_time'],

		'for_time'=> $data['for_time'],

	);

	$reminderId = $this->common_model->updateRecords('cp_reminder', $post_data,array('reminder_id' => $data['reminder_id']));

	if($reminderId)

	{

		$reminderData = $this->common_model->getSingleRecordById('cp_reminder', array('reminder_id' => $data['reminder_id']));

		$users1 = array(

			'id'=>$reminderData['reminder_id'],

			'title'=>$reminderData['reminder_title'],

			'date'=>$reminderData['reminder_date'],

			'time'=>$reminderData['reminder_time'],

			'reminder_before'=>$reminderData['reminder_before'],

			'repeat'=>$reminderData['repeat'],

			'snooze'=> $reminderData['snooze'],

			'after_time'=> $reminderData['after_time'],

			'for_time'=> $reminderData['for_time'],

		);

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Reminder update successfully', 'response' => $users1);

	}

	else

	{

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function doctorCategory_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$limit = 4;

	// if(!isset($data['page_no'])){

	// 	$data['page_no']=1;

	// }

	$start  = (!empty($data['page_no'])-1)*$limit;

	$con1['returnType'] = 'count';

	$table = "cp_doctor_categories";

	$category_count  = $this->Api_model->getRows($table,$con1);

//echo $category_count;die;

	$pages = ceil($category_count / $limit);

//echo $pages;die;

	$con['sorting'] = array("dc_id"=>"DESC");

	if(isset($data['page_no'])){

		$con['limit'] = $limit;

		$con['start'] = $start;

	}

	$categoryDatas  = $this->Api_model->getRows($table,$con);

	if(!empty($categoryDatas)){

		foreach($categoryDatas as $categoryData_details){

			if(!empty($categoryData_details['dc_icon'])){

				$img=	$categoryData_details['dc_icon'];

			}else{

				$img='catDefault.png';

			}

			$categoryeData[] = array('id'=>$categoryData_details['dc_id'],

				'name'=>$categoryData_details['dc_name'],

				'icon'=>base_url().'uploads/doctor_category/'.$img, 

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('category_data' => $categoryeData));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Category not found','response' => array('message' => 'Category not found'));

	}

	$this->response($resp);

}

public function doctor_list_post1(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('category_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$limit = 5;

	if($data['page_no']==''){

		$data['page_no']=1;

	}

	$start  = ($data['page_no']-1)*$limit;

	$con1['returnType'] = 'count';

	$table = "cp_doctor";

	$con1['conditions'] = array("doctor_category_id"=>$data['category_id']);

	$category_count  = $this->Api_model->getRows($table,$con1);

	$pages = ceil(count($category_count) / $limit);

	$con['sorting'] = array("doctor_id"=>"DESC");

	$con['limit'] = $limit;

	$con['start'] = $start;

	$con['conditions'] = array("doctor_category_id"=>$data['category_id']);

	$doctorDatas  = $this->Api_model->getRows($table,$con);

	if(!empty($doctorDatas)){

		foreach($doctorDatas as $doctorData_details){

			$doctorData[] = array('id'=>$doctorData_details['doctor_id'],

				'name'=>$doctorData_details['doctor_name'],

				'fees'=>$doctorData_details['doctor_fees'],

				'address'=>$doctorData_details['doctor_address'],

				'contact'=>$doctorData_details['doctor_mob_no'],

				'image'=>base_url().'uploads/doctor/'.$doctorData_details['doctor_pic'],

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('doctor_data' => $doctorData));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Doctor not found','response' => array('message' => 'Doctor not found'));

	}

	$this->response($resp);

}

// Add Restaurant data

public function add_restaurnat_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id',"name","image","lat","long","address","place_id","rating",'icon');

	$chk_error = check_required_value($required_parameter, $data['rests'][0]);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	for($i=0;$i<count($data['rests']);$i++){

		$place_id = $this->common_model->getSingleRecordById('cp_restaurant', array('place_id' => $data['rests'][$i]['place_id'],"user_id"=>$data['rests'][$i]['user_id']));

		if($place_id){

			continue;

		}

		$post_data = array(

			'user_id' => (!empty($data['rests'][$i]['user_id']) ? $data['rests'][$i]['user_id'] : ''),

			'rest_name' => (!empty($data['rests'][$i]['name']) ? $data['rests'][$i]['name'] : ''),

			'rest_image' => (!empty($data['rests'][$i]['image']) ? $data['rests'][$i]['image'] : ''),

			'rest_lat' => (!empty($data['rests'][$i]['lat']) ? $data['rests'][$i]['lat'] : ''),

			'rest_long' => (!empty($data['rests'][$i]['long']) ? $data['rests'][$i]['long'] : ''),

			'place_id' => (!empty($data['rests'][$i]['place_id']) ? $data['rests'][$i]['place_id'] : ''),

			'rating' => (!empty($data['rests'][$i]['rating']) ? $data['rests'][$i]['rating'] : ''),

			'rest_icon' => (!empty($data['rests'][$i]['icon']) ? $data['rests'][$i]['icon'] : ''),

			'rest_address' => (!empty($data['rests'][$i]['address']) ? $data['rests'][$i]['address'] : ''),

			'rest_created' => date('Y-m-d H:i:s'),

			'rating'=>(!empty($data['rests'][$i]['rating']) ? $data['rests'][$i]['rating'] : '')

		);

		$restaurantId = $this->common_model->addRecords('cp_restaurant', $post_data);

	}

	$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Add Restaurant successfully', 'response' => array('message'=>'Add Restaurant successfully'));

	$this->response($resp);

}

// list restaurant data

public function rest_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$limit = 10;

	if($data['page_no']==''){

		$data['page_no']=1;

	}

	$start  = ($data['page_no']-1)*$limit;

	$con1['returnType'] = 'count';

	$table = "cp_restaurant";

	$con1['conditions'] = array("user_id"=>$data['user_id']);

	$category_count  = $this->Api_model->getRows($table,$con1);

	$pages = ceil(count($category_count) / $limit);

	$con['sorting'] = array("rest_id"=>"DESC");

	$con['limit'] = $limit;

	$con['start'] = $start;

	$con['conditions'] = array("user_id"=>$data['user_id']);

	$restaurantDatas  = $this->Api_model->getRows($table,$con);

	if(!empty($restaurantDatas)){

		foreach($restaurantDatas as $restaurantData_details){

			$fav  = $this->getRestFav($restaurantData_details['rest_id']);

			$restaurantData[] = array('id'=>$restaurantData_details['rest_id'],

				'name'=>$restaurantData_details['rest_name'],

				'image'=>$restaurantData_details['rest_image'],

				'address'=>$restaurantData_details['rest_address'],

				'user_id'=>$restaurantData_details['user_id'],

				'lat' => $restaurantData_details['rest_lat'],

				'long' => $restaurantData_details['rest_long'],

				'rating' => $restaurantData_details['rating'],

				'fav' => $fav,

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('restaurant_data' => $restaurantData));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Restaurant not found','response' => array('message' => 'Restaurant not found'));

	}

	$this->response($resp);

}

public function add_fav_restaurant_post(){

	$data = $this->param;

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$object_info = $data;

	$required_parameter = array('restaurant_name','restaurant_image','restaurant_address','rating','place_id','user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$address  =  $this->get_address($data['lat'],$data['long']);

	$post_data = array

	(

		'user_id'=> $data['user_id'],

		'fav_rest_name' => $data['restaurant_name'],

		'fav_rest_image' => $data['restaurant_image'],

		'fav_rest_address' => $data['restaurant_address'],	'fav_rest_rating' => $data['rating'],

		'fav_rest_place_id'=> $data['place_id'],

		'fav_rest_created' => date('Y-m-d H:i:s'),

	);

	$restaurantId = $this->common_model->addRecords('cp_fav_restaurant', $post_data);

	if($restaurantId){

		$restaurantData = $this->common_model->getSingleRecordById('cp_fav_restaurant', array('fav_rest_id' => $restaurantId));

		$restaurantData1 = array(

			'id' => $restaurantData['fav_rest_id'],

		);

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Favorite successfully', 'response' => array('fav_data' => "favorite successfully"));

	}

	else

	{

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function get_address($lat,$long){

	$geocode = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&sensor=false&key=AIzaSyCJyDp4TLGUigRfo4YN46dXcWOPRqLD0gQ";

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $geocode);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch, CURLOPT_PROXYPORT, 3128);

	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

	$response = curl_exec($ch);

	curl_close($ch);

	$output = json_decode($response);

	$dataarray = get_object_vars($output);

	$address = $dataarray['results'][0]->formatted_address;

	return $address;

}

public function unfav_restaurant_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('place_id','user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$restid =$this->common_model->deleteRecords('cp_fav_restaurant',array('fav_rest_place_id'=>$data['place_id'],'user_id' => $data['user_id']));

	if($restid){

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));

	}

	else {

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function restFav_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$limit = 5;

	if($data['page_no']==''){

		$data['page_no']=1;

	}

	$start  = ($data['page_no']-1)*$limit;

	$con1['returnType'] = 'count';

	$table = "cp_fav_restaurant";

	$con1['conditions'] = array("cp_fav_restaurant.user_id"=>$data['user_id']);

	$category_count  = $this->Api_model->getRowsRestaurantnew($table,$con1);

	$pages = ceil(count($category_count) / $limit);

	$con['sorting'] = array("fav_rest_id"=>"DESC");

	$con['limit'] = $limit;

	$con['start'] = $start;

	$con['conditions'] = array("cp_fav_restaurant.user_id"=>$data['user_id']);

	$restaurantDatas  = $this->Api_model->getRowsRestaurantnew($table,$con);

//print_r("<pre/>");

//print_r($restaurantDatas);

// die;

	if(!empty($restaurantDatas)){

		foreach($restaurantDatas as $restaurantData_details){

			$restaurantData[] = array('id'=>$restaurantData_details['fav_rest_id'],

				'name'=>$restaurantData_details['fav_rest_name'],

				'image'=>$restaurantData_details['fav_rest_image'],

				'address'=>$restaurantData_details['fav_rest_address'],

				'user_id'=>$restaurantData_details['user_id'],

				'rating' => $restaurantData_details['fav_rest_rating'],

				'place_id' => $restaurantData_details['fav_rest_place_id']

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('restaurant_data' => $restaurantData));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Favorite Restaurant not found','response' => array('message' => 'Favorite Restaurant not found'));

	}

	$this->response($resp);

}

public function add_bank_post111(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id',"name","image","lat","long","address","place_id","rating",'icon');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$address  =  $this->get_address($data['lat'],$data['long']);

	$post_data = array

	(

		'user_id'=> $data['user_id'],

		'bank_name'=> $data['name'],

		'bank_image'=> $data['image'],

		'bank_lat'=> $data['lat'],

		'bank_long'=> $data['long'],

		'place_id'=> $data['place_id'],

		'bank_rating'=> $data['rating'],

		'bank_icon'=> $data['icon'],

		'bank_address'=> (!empty($address) ? $address : ''),

		'bank_created' => date('Y-m-d H:i:s'),

	);

	$bankId = $this->common_model->addRecords('cp_bank', $post_data);

	if($bankId){

		$bankData = $this->common_model->getSingleRecordById('cp_bank', array('bank_id' => $bankId));

		$bankData1 = array(

			'id' => $bankData['bank_id'],

			'name' => $bankData['bank_name'],

			'image' => $bankData['bank_image'],

			'lat' => $bankData['bank_lat'],

			'long' => $bankData['bank_long'],

			'address' => $bankData['bank_address'],

		);

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Bank added successfully', 'response' => $bankData1);

	}

	else

	{

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function add_bank_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id',"name","image","lat","long","address","place_id","rating",'icon');

	$chk_error = check_required_value($required_parameter, $data['rests'][0]);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	for($i=0;$i<count($data['rests']);$i++){

		$place_id = $this->common_model->getSingleRecordById('cp_bank', array('place_id' => $data['rests'][$i]['place_id'],"user_id"=>$data['rests'][$i]['user_id']));

		if($place_id){

			continue;

		}

		$post_data = array(

			'user_id' => (!empty($data['rests'][$i]['user_id']) ? $data['rests'][$i]['user_id'] : ''),

			'rest_name' => (!empty($data['rests'][$i]['name']) ? $data['rests'][$i]['rest_name'] : ''),

			'rest_image' => (!empty($data['rests'][$i]['image']) ? $data['rests'][$i]['image'] : ''),

			'rest_lat' => (!empty($data['rests'][$i]['lat']) ? $data['rests'][$i]['lat'] : ''),

			'rest_long' => (!empty($data['rests'][$i]['long']) ? $data['rests'][$i]['long'] : ''),

			'place_id' => (!empty($data['rests'][$i]['place_id']) ? $data['rests'][$i]['place_id'] : ''),

			'rating' => (!empty($data['rests'][$i]['rating']) ? $data['rests'][$i]['rating'] : ''),

			'rest_icon' => (!empty($data['rests'][$i]['icon']) ? $data['rests'][$i]['icon'] : ''),

			'rest_address' => (!empty($data['rests'][$i]['address']) ? $data['rests'][$i]['address'] : ''),

			'rest_created' => date('Y-m-d H:i:s'),

			'rating'=>(!empty($data['rests'][$i]['rating']) ? $data['rests'][$i]['rating'] : '')

		);

		$restaurantId = $this->common_model->addRecords('cp_bank', $post_data);

	}

	$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Add Bank successfully', 'response' => array('message'=>'Add Bank successfully'));

	$this->response($resp);

}

public function add_fav_bank_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter =  array('bank_name','bank_image','bank_address','rating','place_id','user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

// $address  =  $this->get_address($data['lat'],$data['long']);

	$post_data = array

	(

		'user_id'=> $data['user_id'],

		'fav_bank_name' => $data['bank_name'],

		'fav_bank_image' => $data['bank_image'],

		'fav_bank_address' => $data['bank_address'],	'fav_bank_rating' => $data['rating'],

		'fav_place_id'=> $data['place_id'],

		'fav_bank_created' => date('Y-m-d H:i:s'),

	);

	$bankId = $this->common_model->addRecords('cp_fav_banks', $post_data);

	if($bankId){

		$bankData = $this->common_model->getSingleRecordById('cp_fav_banks', array('fav_bank_id' => $bankId));

		$bankData1 = array(

			'id' => $bankData['fav_bank_id'],

		);

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Favorite successfully', 'response' => array('message'=>'Favorite successfully'));

	}

	else

	{

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function bank_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$limit = 10;

	if($data['page_no']==''){

		$data['page_no']=1;

	}

	$start  = ($data['page_no']-1)*$limit;

	$con1['returnType'] = 'count';

	$table = "cp_bank";

	$con1['conditions'] = array("user_id"=>$data['user_id']);

	$bank_count  = $this->Api_model->getRows($table,$con1);

	$pages = ceil(count($bank_count) / $limit);

	$con['sorting'] = array("bank_id"=>"DESC");

	$con['limit'] = $limit;

	$con['start'] = $start;

	$con['conditions'] = array("user_id"=>$data['user_id']);

	$bankDatas  = $this->Api_model->getRows($table,$con);

	if(!empty($bankDatas)){

		foreach($bankDatas as $bankDatas_details){

			$fav  = $this->getBankFav($bankDatas_details['bank_id']);

			$bankData[] = array('id'=>$bankDatas_details['bank_id'],

				'name'=>$bankDatas_details['bank_name'],

				'image'=>$bankDatas_details['bank_image'],

				'address'=>$bankDatas_details['bank_address'],

				'user_id'=>$bankDatas_details['user_id'],

				'rating'=>$bankDatas_details['bank_rating'],

				'fav'=>$fav,

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('bank_data' => $bankData));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Bank not found','response' => array('message' => 'Bank not found'));

	}

	$this->response($resp);

}

public function fav_bank_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$limit = 10;

	if($data['page_no']==''){

		$data['page_no']=1;

	}

	$start  = ($data['page_no']-1)*$limit;

	$con1['returnType'] = 'count';

	$table = "cp_fav_banks";

	$con1['conditions'] = array("cp_fav_banks.user_id"=>$data['user_id']);

	$bank_count  = $this->Api_model->getRowsBanknew($table,$con1);

	$pages = ceil(count($bank_count) / $limit);

	$con['sorting'] = array("cp_fav_banks.bank_id"=>"DESC");

	$con['limit'] = $limit;

	$con['start'] = $start;

	$con['conditions'] = array("cp_fav_banks.user_id"=>$data['user_id']);

	$bankDatas  = $this->Api_model->getRowsBanknew($table,$con);

//  echo $this->db->last_query();die;

	if(!empty($bankDatas)){

		foreach($bankDatas as $bankDatas_details){

			$bankData[] = array('id'=>$bankDatas_details['fav_bank_id'],

				'name'=>$bankDatas_details['fav_bank_name'],

				'image'=>$bankDatas_details['fav_bank_image'],

				'address'=>$bankDatas_details['fav_bank_address'],

				'user_id'=>$bankDatas_details['user_id'],

				'rating'=>$bankDatas_details['fav_bank_rating'],

				'place_id' => $bankDatas_details['fav_place_id']

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('bank_data' => $bankData));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Fav Bank not found','response' => array('message' => 'Fav Bank not found'));

	}

	$this->response($resp);

}

public function unfav_bank_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('place_id','user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$restid =$this->common_model->deleteRecords('cp_fav_banks',array('fav_place_id'=>$data['place_id'],'user_id' => $data['user_id']));

	if($restid){

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));

	}

	else {

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function doctor_available_time_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('doctor_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$table = "dr_available_date";

	if($data['date']){

		$date = $data['date'];

	}else{

		$date = date('Y-m-d');

	}

	$con['conditions'] = array("avdate_date"=>$date,'avdate_dr_id'=>$data['doctor_id']);

	$doctorAvail  = $this->Api_model->getRows($table,$con);

	if(!empty($doctorAvail)){

		foreach($doctorAvail as $doctorAvail_details){

			$availTime  = $this->getTimeSchedule($doctorAvail_details['avdate_id']);

			$doctorAvail_details1[] = array('id'=>$doctorAvail_details['avdate_id'],

				'Available_data'=>$doctorAvail_details['avdate_date'],

				'Available_time'=>$availTime,

// 'Available_time_slot'=>$availTime,

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('doctor_avail_data' => $doctorAvail_details1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Doctor not Available','response' => array('message' => 'Doctor not Available'));

	}

	$this->response($resp);

}

public function  getTimeSchedule($avdate_id){

	$this->db->select('*');

	$this->db->from('dr_available_time');

	$this->db->where('avtime_date_id',$avdate_id);

	$query = $this->db->get();

	$result = $query->result_array();

	foreach($result as $result_details){

		$imageArray[] = array('id'=>$result_details['avtime_id'],'name'=>$result_details['avtime_text'],'time_slot'=>$result_details['avtime_day_slot']);

	}

	return $imageArray;

}

public function add_appointment_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('doctor_id',"user_id","date_id","time_id");

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$new_appint = $this->common_model->getsingle("doctor_appointments",array("doctor_id" => $data['doctor_id'],"dr_appointment_date_id" => $data['date_id'],"dr_appointment_time_id" => $data['time_id']));





	// echo "<pre>";

	// print_r($new_appint);

	// die;





	if(empty($new_appint)){

		$post_data = array

		(

			'doctor_id'=> $data['doctor_id'],

			'user_id'=> $data['user_id'],

			'dr_appointment_date_id'=> $data['date_id'],

			'dr_appointment_time_id'=> $data['time_id'],

			'appointments_reminder'=>(!empty($data['reminder']) ? $data['reminder'] : 0),

		);

		$drAppId = $this->common_model->addRecords('doctor_appointments', $post_data);

		// -------------------------Email functionality start-------------------------

		$doctor_id = $data['doctor_id'];

		$user_id = $data['user_id'];

		$appointment_id = $drAppId;

		// echo $doctor_id;

		// die; 

		$docDetail = $this->common_model->getSingleRecordById('cp_doctor',array('doctor_id'=>$doctor_id));

		//$docDetail = $this->common_model_new->getsingle('cp_doctor',array('doctor_id'=>$doctor_id));

		// echo "<pre>";

		// print_r($docDetail);

		// die;

	        // user deetail

		$userDetail = $this->common_model->getSingleRecordById('cp_users',array('id'=>$user_id));

		$name= $userDetail['name'];

		$lastname =$userDetail['lastname'];

		$usrfullname= $name.' '.$lastname;

		$usremail= $userDetail['email'];

		$usrmobile=$userDetail['mobile'];    

		// echo "<pre>";

		// print_r($userDetail['id']);

		// die;

        //Appointment detail

		$appointment_data = $this->common_model->getAppointmentDetail($appointment_id);

		// echo "<pre>";

		// print_r($appointment_data);

		// die;

        //$appointment_data[0]->doctor_id





		$apointDate= date('d-m-Y',strtotime($appointment_data[0]->avdate_date));



		$apointTime = $appointment_data[0]->avtime_text;

		$doctorEmail = $docDetail['doctor_email'];

		$doctorName = $docDetail['doctor_name'];

		$TemplateData = $this->common_model->getSingleRecordById('mail_template',array('template_id'=>6));

		//$TemplateData = $this->common_model_new->getsingle('mail_template',array('template_id'=>6));

		$findArray=array("%Drname%","%Date%","%Time%","%name%","%email%","%Mobile%");

		$replaaceArray=array($doctorName,$apointDate,$apointTime,$usrfullname,$usremail,$usrmobile);

		$description= str_replace($findArray,$replaaceArray,$TemplateData['description']);

		$message = $description;

          //$message = 'Dear '.$firstname.' '.$lastname.',<br><p>Welcome to our carepro app.</p><br><p>Please open tablet and please do further process and enjoy our services.</p><br><p>Best Wishes,</p><br><p>CarePro Team</p>';

		$subject = $TemplateData['subject'];



		// echo $message;

		// echo "<br>";



		// echo $subject;

		// echo "<br>";

		

		// echo $doctorEmail;

		// //echo "<br>";

		// die;

		$check = new_send_mail($message, $subject, $doctorEmail, '');

			// -------------------------End-------------------------



		



		if($drAppId){

			$drAppData = $this->Api_model->getAppointments($drAppId);

			$drAppData1 = array(

				'id' => $drAppData[0]['dr_appointment_id'],

				'selected_date' => $drAppData[0]['avdate_date'],

				'schedule_time' => $drAppData[0]['avtime_text'],

				'reminder' => $drAppData[0]['appointments_reminder'],

			);

			$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Appointment Added successfully', 'response' => $drAppData1);

		}else{

			$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

		}

	}else{

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'This appointment time is already scheduled by a user, please select any other.', 'response' => array('message' => 'This appointment time is already scheduled by a user, please select any other.'));

	}

	$this->response($resp);

}

public function doctor_list_post(){

//echo "Testing";die;

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('category_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$limit = 5;

	if($data['page_no']==''){

		$data['page_no']=1;

	}

	$start  = ($data['page_no']-1)*$limit;

	$category_count  = $this->Api_model->getDoctorListCount($data['category_id']);

	$pages = ceil(count($category_count) / $limit);

/*$con['sorting'] = array("doctor_id"=>"DESC");

$con['limit'] = $limit;

$con['start'] = $start;

$con['conditions'] = array("doctor_category_id"=>$data['category_id']);*/

// $doctorDatas  = $this->Api_model->getDoctorList($data['category_id'],$limit,$start);

$doctorDatas  = $this->Api_model->getDoctorList($data['category_id']);

$categoryImage = $this->getCategoryImage($data['category_id']);

if(!empty($categoryImage)){

	$catImg=base_url().'uploads/doctor_category/'.$categoryImage;

}else{

	$catImg=base_url().'uploads/doctor_category/catDefault.png';

}

if(!empty($doctorDatas)){

	foreach($doctorDatas as $doctorData_details){

		if(!empty($doctorData_details['doctor_pic'])){

			$docPic= base_url().'uploads/doctor/'.$doctorData_details['doctor_pic'];

		}else{

			$docPic= base_url().'uploads/doctor_category/def.png';

		}

		$doctorData[] = array('id'=>$doctorData_details['doctor_id'],

			'name'=>$doctorData_details['doctor_name'],

			'fees'=>$doctorData_details['doctor_fees'],

			'address'=>$doctorData_details['doctor_address'],

			'contact'=>$doctorData_details['doctor_mob_no'],

			'category_name'=>$this->getCategory($doctorData_details['doctor_category_id']),

			'category_id'=>$doctorData_details['doctor_category_id'],

			'image'=>$catImg,

			'doctor_image'=>$docPic,

			"doctor_email" => $doctorData_details['doctor_email'],

		);

	}

	$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('doctor_data' => $doctorData));

} else {

	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Doctor not found','response' => array('message' => 'Doctor not found'));

}

$this->response($resp);

}

public function getCategory($id){

	$id = explode(',',$id);

	$this->db->select('dc_name');

	$this->db->from('cp_doctor_categories');

	$this->db->where_in('dc_id',$id);

	$query = $this->db->get();

	$result = $query->result_array();

	$last_names1 = array_column($result, 'dc_name');

	$result_data = implode(',',$last_names1);

//print_r("<pre/>");

//print_r($last_names1);

//die;

	return $result_data;

}

public function getCategoryImage($category_id){

	$this->db->select('dc_icon');

	$this->db->from('cp_doctor_categories');

	$this->db->where('dc_id',$category_id);

	$query = $this->db->get();

	$result = $query->result_array();

	return $result[0]['dc_icon'];

}

public function activity_details_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));

		$this->response($resp);

	}

	//$reminderData = $this->common_model->getAllRecordsById('cp_reminder', array('user_id' => $data['user_id'],"reminder_date"=>$data['date']));



	if(!isset($data['date'])){

		$data['date']='';

	}



	$reminderData = $this->Api_model->getUserReminders($data['user_id'],$data['date']);

	//   $new_time_arr = array();

	//   foreach ($reminderData as $key => $val)

	//   {

	//     if ($val['snooze'] != 'false') {

	//      $val['reminder_time'];

	//      $after_time = $val['after_time'];

	//      $for_time = $val['for_time'];

	//      $reminder_id = $val['reminder_id'];

	//      $increase_time = $val['reminder_time'];

	//      $newtime = '';

	//              //$newtime = array();

	//      for ($i=0; $i < $for_time; $i++)

	//      {

	//       $new_time =  strtotime("+".$after_time."minutes", strtotime($increase_time));

	//       $new_time = date('h:i A',$new_time);

	//       $increase_time = $new_time;

	//       $newtime = $newtime.$new_time.',';

	//                 //$newtime[] = $new_time;

	//     }

	//     $new_time_arr[] = array($reminder_id=>$newtime);

	//   }

	// }

	// echo "<pre>";

	//       //print_r($reminderData);

	// print_r($new_time_arr);

	// die;

	// echo "<pre>";

	// print_r($reminderData);

	// die;

	$AppData = $this->Api_model->getUserAppointments($data['user_id'],$data['date']);

	// print_r("<pre/>");

	// print_r($AppData);

	// die;

	if(empty($AppData)){

		$reminder['Appointments'] = array();

	}

	if(empty($reminderData)){

		$reminder['reminders'] = array();

	}

	//$reminder['reminders']= array();

	if(!empty($reminderData) or !empty($AppData)){

		//$selected_time = '';

		$new_time_arr = array();

		foreach ( $reminderData as $row ){

			if($row['reminder_date'] == "0000-00-00"){

				$reminder_date = $row['reminder_date'];

			}else{

				$reminder_date = date("M d, Y", strtotime($row['reminder_date']));

			}

			if ($row['snooze'] != 'false') {

				$row['reminder_time'];

				$after_time = $row['after_time'];

				$for_time = $row['for_time'];

				$reminder_id = $row['reminder_id'];

				$increase_time = $row['reminder_time'];

				$newtime = '';

				//$newtime = array();

				for ($i=0; $i < $for_time; $i++)

				{

					$new_time =  strtotime("+".$after_time."minutes", strtotime($increase_time));

					$new_time = date('h:i A',$new_time);

					$increase_time = $new_time;

					$newtime = $newtime.$new_time.',';

					//$newtime[] = $new_time;

				}

				$new_time_arr = array($newtime);

			}

			// else{

			//   $new_time_arr[] = $row['reminder_time'];

			// }

			//  $times=array();

			//  for ($i=1; $i<=$row['for_time']; $i++) {

			//   if (empty($selected_time)) {

			//     $selectedTime =$row['reminder_time'];

			//     $selected_time = $row['reminder_time'];

			//   } else {

			//       //$selected_time = $selected_time+5;

			//     $selected_minutestime = strtotime("+".'5', strtotime($selected_time));

			//     $selectedTime =$selected_time;

			//   }

			//   $after_time = $row['after_time'];

			//   $endTime = strtotime("+".$after_time."minutes", strtotime($selectedTime));

			//   $time = date('h:i A', $endTime);

			//   $times=$time;

			// }

			//$reminder['reminders'][]='';

			$currentDate = date('Y-m-d');

			//if($row['reminder_date'] >= $currentDate){

			$reminder['reminders'][] = array(

				'id' => $row['reminder_id'],

				'title' => $row['reminder_title'],

				//'date' => $reminder_date,

				'date' => $row['reminder_date'],

				'actual_time' => $row['reminder_time'],

				'time' => $new_time_arr,

				'snooze' => $row['snooze'],

				'for_time' => $row['for_time'],

				'after_time' => $row['after_time'],

				'user_id' => $row['user_id'],

				'reminder_before' => $row['reminder_before'],

				'repeat' => $row['repeat'],

			);

				// if(empty($reminder['reminders'])){

				// 	echo "data";

				// }else{

				// 	echo "No data";

				// }

			//}

		}

		// echo "<pre>";

		// print_r($new_time_arr);

		// echo "</pre>";

		// die;

		//die('kfjdsk');

		foreach ( $AppData as $row_data ){

			$doctordata = $this->common_model->getsingle("cp_doctor",array("doctor_id" => $row_data['doctor_id']));

			$currentDate = date('Y-m-d');

			//if($row_data['avdate_date'] >= $currentDate){

			$reminder['Appointments'][] = array(

				'id' => $row_data['dr_appointment_id'],

				'selected_date' => $row_data['avdate_date'],

				'schedule_time' => $row_data['avtime_text'],

				'user_id' => $row_data['user_id'],

				'doctorName' => $doctordata->doctor_name,

				'doctoraddress' => $doctordata->doctor_address,

			);

			// if(empty($reminder['Appointments'])){

			// 	echo "data";

			// }else{

			// 	echo "No data";

			// }

			//}

		}

		//echo "<pre>";

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('Activities' => $reminder));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Activity Data not found','response' => array('message' => 'Activity Data not found'));

	}

	$this->response($resp);

}

public function update_appointment_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array("date_id","time_id","appointment_id","user_id");

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$singledata = $this->common_model->getsingle("doctor_appointments",array('dr_appointment_id' => $data['appointment_id']));

//$array_appoint = "doctor_id=".$singledata->doctor_id.' and dr_appointment_date_id='.$data['date_id'].' and dr_appointment_time_id='.$data['time_id'].' and '

	$new_appint = $this->common_model->getsingle("doctor_appointments",array("doctor_id" => $singledata->doctor_id,"dr_appointment_date_id" => $data['date_id'],"dr_appointment_time_id" => $data['time_id']));

	if(empty($new_appint) || $new_appint->user_id == $data['user_id']){

		$post_data = array

		(

//'doctor_id'=> $data['doctor_id'],

// 'user_id'=> $data['user_id'],

			'dr_appointment_date_id'=> $data['date_id'],

			'dr_appointment_time_id'=> $data['time_id'],

			'appointments_reminder'=>(!empty($data['reminder']) ? $data['reminder'] : 0),

		);

		$drAppId = $this->common_model->updateRecords('doctor_appointments', $post_data,array('dr_appointment_id' => $data['appointment_id']));

		if($drAppId){

			$drAppData = $this->Api_model->getAppointments($data['appointment_id']);

			$drAppData1 = array(

				'id' => $drAppData[0]['dr_appointment_id'],

				'selected_date' => $drAppData[0]['avdate_date'],

				'schedule_time' => $drAppData[0]['avtime_text'],

				'reminder' => $drAppData[0]['appointments_reminder'],

			);

			$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Appointment Updated successfully', 'response' => $drAppData1);

		}

		else

		{

			$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

		}

	}else{

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'This appointment time is already scheduled by a user, please select any other.', 'response' => array('message' => 'This appointment time is already scheduled by a user, please select any other.'));

	}

	$this->response($resp);

}

public function smsapi1($otp,$otp_resend_type)

{

	$apiKey = urlencode('AQGYL+5ECgs-8hDM6ND0expDH4NWQ5uydvdAnsUhsZ');

// Message details

	$numbers = array($otp_resend_type);

	$sender = urlencode('TXTLCL');

	$message = rawurlencode('This is your reset otp message:'.$otp);

	$numbers = implode(',', $numbers);

// Prepare data for POST request

	$data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);

// Send the POST request with cURL

	$ch = curl_init('https://api.textlocal.in/send/');

	curl_setopt($ch, CURLOPT_POST, true);

	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$response = curl_exec($ch);

	curl_close($ch);

// Process your response here

	return $response;

}

public function smsapi($otp,$mobile){

	$apikey = "hGS9pj6kqEOA5DggePuVqQ";

	$apisender = "SMSTST";

	$msg =$otp;

$num = $mobile;    // MULTIPLE NUMBER VARIABLE PUT HERE...!

$ms = rawurlencode($msg);   //This for encode your message content

$url = 'https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey='.$apikey.'&senderid='.$apisender.'&channel=2&DCS=0&flashsms=0&number='.$num.'&text='.$ms.'&route=1';

//echo $url;

$ch=curl_init($url);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

curl_setopt($ch,CURLOPT_POST,1);

curl_setopt($ch,CURLOPT_POSTFIELDS,"");

curl_setopt($ch, CURLOPT_RETURNTRANSFER,2);

$data = curl_exec($ch);

//print_r("<pre/>");

//print_r($data);die;

//echo '<br/> <br/>';

//print($data); /* result of API call*/

}

/* add medicine */

public function add_medicine_schedule_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('name',"days","time","user_id","doctor_id","reminder");

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	if(!empty($data['other_doctor_name'])){

		$array_otherdoc = array('other_doctor_name' => $data['other_doctor_name'],'doctor_name'=>$data['other_doctor_name'],'doctor_created' => date("Y-m-d H:i:s"));

		$new_doc = $this->common_model->addRecords('cp_doctor', $array_otherdoc);

	}

	if(!empty($data['other_doctor_name'])){

		$doct = $new_doc;

	}else{

		$doct = $data['doctor_id'];

	}

	$post_data = array

	(

		'medicine_name'=> $data['name'],

		'medicine_take_days'=> $data['days'],

		'medicine_time'=> $data['time'],

		'user_id'=> $data['user_id'],

		'doctor_id'=> $doct,

		'medicine_reminder'=> $data['reminder'],

		'end_date' => $data['end_date'],

		'description' => $data['description'],

		'medicine_create_date' => date('Y-m-d H:i:s'),

	);

	$medicineId = $this->common_model->addRecords('medicine_schedule', $post_data);

	if($medicineId){

		$medicineData = $this->common_model->getSingleRecordById('medicine_schedule', array('medicine_id' => $medicineId));

		$medicineData1 = array(

			'id' => $medicineData['medicine_id'],

			'user_id' => $medicineData['user_id'],

			'doctor_id' => $medicineData['doctor_id'],

			'name' => $medicineData['medicine_name'],

			'time' => $medicineData['medicine_time'],

			'end_date' => $medicineData['end_date'],

			'description' => $medicineData['description'],

			'reminder' => $medicineData['medicine_reminder'],

		);

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Medicine Schedule added successfully', 'response' => $medicineData1);

	}

	else

	{

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function update_medicine_schedule_post()

{

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('name',"days","time","medicine_id","doctor_id","reminder");

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$medicineData = $this->common_model->getSingleRecordById('medicine_schedule', array('medicine_id' => $data['medicine_id']));

	if(!empty($data['other_doctor_name'])){

		$this->common_model->updateRecords('cp_doctor', array("doctor_name" => $data['other_doctor_name'],'other_doctor_name'=>$data['other_doctor_name']), array("doctor_id"=>$medicineData['doctor_id']));

		$newd = $this->common_model->getsingle('cp_doctor', array('doctor_id' => $medicineData['doctor_id']));

		$newvaldoc = $medicineData['doctor_id'];

	}else{

		$newvaldoc = $data['doctor_id'];

	}

	$post_data = array

	(

		'medicine_name'=> $data['name'],

		'medicine_take_days'=> $data['days'],

		'doctor_id'=> $newvaldoc,

		'medicine_time'=> $data['time'],

		'medicine_create_date' => $data['start_date'],

		'end_date' => $data['end_date'],

		'description' => $data['description'],

		'medicine_reminder'=> $data['reminder'],

	);

	$medicineId = $this->common_model->updateRecords('medicine_schedule', $post_data,array('medicine_id' => $data['medicine_id']));

	if($medicineId)

	{

		$users1 = array(

			'id'=>$medicineData['medicine_id'],

			'user_id'=>$medicineData['user_id'],

			'doctor_id'=>$medicineData['doctor_id'],

			'name'=>$medicineData['medicine_name'],

			'days'=>$medicineData['medicine_take_days'],

			'start_date' => $medicineData['medicine_create_date'],

			'end_date' => $medicineData['end_date'],

			'description' => $medicineData['description'],

			'time'=>$medicineData['medicine_time'],

			'reminder'=>$medicineData['medicine_reminder'],

		);

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Medicine updated successfully', 'response' => $users1);

	}

	else

	{

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function show_medicine_schedule_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));

		$this->response($resp);

	}

	$data_post['start_date'] = $data['start_date'];

	$data_post['end_date'] = $data['end_date'];

//$medicineData = $this->common_model->getAllRecordsById('medicine_schedule', array('user_id' => $data['user_id']));

	$medicineData = $this->common_model->getDetailField(array("search" => $data_post),$data['user_id']);

	//echo '<pre>';print_r($this->db->last_query());die;

	//echo '<pre>';print_R($medicineData);die;

	if(!empty($medicineData)){

		foreach ( $medicineData as $row ){

			$currentDate=date('m-d-Y');

			$medicnDate = date('m-d-Y',strtotime($row->medicine_create_date));

			$end_date = date('m-d-Y',strtotime($row->end_date));



			//$Enteredend_date = date('m-d-Y',strtotime($data_post['end_date']));



			



			if($medicnDate >= $currentDate || $end_date >= $currentDate){



				//if($end_date <= $Enteredend_date){



				$medicine[] = array(

					'id' => $row->medicine_id,

					'user_id' => $row->user_id,

					'doctor_id' => $row->doctor_id,

					'name' => $row->medicine_name,

					'days' => $row->medicine_take_days,

					'time' => $row->medicine_time,

					'reminder'=>$row->medicine_reminder,

					'end_date' => date('m-d-Y',strtotime($row->end_date)),

					'description' => $row->description,

					'medicine_date'=>date('m-d-Y',strtotime($row->medicine_create_date)),

				);



				//}

			}

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('medicine_data' => $medicine));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Medicine Data not found','response' => array('message' => 'Medicine Data not found'));

	}

	$this->response($resp);

}

public function get_medicine_schedule_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('medicine_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));

		$this->response($resp);

	}

	$medicineData = $this->common_model->getSingleRecordById('medicine_schedule', array('medicine_id' => $data['medicine_id']));

//print_r("<pre/>");

//print_r($medicineData);

// die;

	if(!empty($medicineData)){

//foreach ( $medicineData as $row ){

		$medicine = array(

			'id' => $medicineData['medicine_id'],

			'user_id' => $medicineData['user_id'],

			'doctor_id' => $medicineData['doctor_id'],

			'name' => $medicineData['medicine_name'],

			'days' => $medicineData['medicine_take_days'],

			'time' => $medicineData['medicine_time'],

			'reminder'=>$medicineData['medicine_reminder'],

			'medicine_date'=>$medicineData['medicine_create_date'],

		);

// }

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('medicine_data' => $medicine));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Favourite Data not found','response' => array('message' => 'Favourite Data not found'));

	}

	$this->response($resp);

}

public function send_sms($otp,$mobile_number){

	//echo $mobile_number;die;

	//echo $mobile_number;die;

//$mobile_number = trim($mobile_number);

	$account_sid = 'AC6746ef6886924e71439cb1ef3dc7595a';

	$auth_token = '5bddd7cc3a5a59b3a3e46b96708c0e71';

	$twilio_number = "+12512379625";

	//$twilio_number = "+15005550006";

	$client = new Client($account_sid, $auth_token);

//echo $client;die;

	$client->messages->create(

// Where to send a text message (your cell phone?)

//"+1".trim($mobile_number),

		"+91".$mobile_number,   

		//"+1".$mobile_number,

		array(

			'from' => $twilio_number,

			'body' => 'This is your otp message:'.$otp

		)

	);

}

public function video_access_token_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('identity');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));

		$this->response($resp);

	}

	$sid    = "AC5ac3a5fd26d52942e039e626af48a4b4";

	$token  = "598a5da83008bb2c918fb84aa2195add";

	$twilio = new Client($sid, $token);

	$new_key = $twilio->newKeys

	->create();

	$twilioAccountSid = 'AC5ac3a5fd26d52942e039e626af48a4b4';

	$twilioApiKey = $new_key->sid;

	$twilioApiSecret = $new_key->secret;

	$identity = $data['identity'];

	$token = new AccessToken(

		$twilioAccountSid,

		$twilioApiKey,

		$twilioApiSecret,

		3600,

		$identity

	);

	$roomName = $data['roomName'];

	$videoGrant = new VideoGrant();

	$videoGrant->setRoom($roomName);

	$token->addGrant($videoGrant);

// render token to string

	echo $token->toJWT();

}

public function access_token_post(){

//$account_sid = 'SKaeb6d7b26adb79e1d74b7f1889afaad4';

//$auth_token = 'AIzaSyBZmM30jCgAaA4B1X-ND7L0fB8drAZaZZ0';

//$client = new Client($account_sid, $auth_token);

//$twilioAccountSid = 'AC5ac3a5fd26d52942e039e626af48a4b4';

// $twilioApiKey = 'SKaeb6d7b26adb79e1d74b7f1889afaad4';

// $twilioApiSecret = 'iR2Qaw0cg7PCwMLvtV8Oqxf6G03X1552';

//$sid    = "AC50352170c8ed9744b2c4ccb3cf42f4c4";

//$sid    = "AC5ac3a5fd26d52942e039e626af48a4b4";

	$sid    = "AC5ac3a5fd26d52942e039e626af48a4b4";

//$token  = "c8ead357ad788b270266ee1d4a82e7bc";

//$token  = "598a5da83008bb2c918fb84aa2195add";

	$token  = "598a5da83008bb2c918fb84aa2195add";

	$twilio = new Client($sid, $token);

	$new_key = $twilio->newKeys

	->create();

//print_r($new_key);die;

//$twilioAccountSid = 'AC50352170c8ed9744b2c4ccb3cf42f4c4';

// $twilioApiKey = 'c8ead357ad788b270266ee1d4a82e7bc';

//$twilioAccountSid = 'AC50352170c8ed9744b2c4ccb3cf42f4c4';

//$twilioAccountSid = 'AC5ac3a5fd26d52942e039e626af48a4b4';

	$twilioAccountSid = 'AC5ac3a5fd26d52942e039e626af48a4b4';

	$twilioApiKey = $new_key->sid;

//$twilioApiSecret = 'B8ZKYbl5XlbWS43ZRMGFFQ0JXQii2Pqp';

	$twilioApiSecret = $new_key->secret;

//$outgoingApplicationSid = 'APdbdbe2a3099a474f9b09d0ecaf82b5d7';

//$outgoingApplicationSid = 'AP13639ec86f321bfdf716757d7d45c056';

	$outgoingApplicationSid = 'AP13639ec86f321bfdf716757d7d45c056';

	$identity = "+12098782148";

	$token = new AccessToken(

		$twilioAccountSid,

		$twilioApiKey,

		$twilioApiSecret,

		3600,

		$identity

	);

	$voiceGrant = new VoiceGrant();

	$voiceGrant->setOutgoingApplicationSid($outgoingApplicationSid);

//$voiceGrant->setOutgoingApplicationSid($outgoingApplicationSid);

// $voiceGrant->setIncomingAllow(true);

//$voiceGrant->setIncomingAllow(true);

// Add grant to token

	$token->addGrant($voiceGrant);

// render token to string

	echo $token->toJWT();die;

//$client1 = new AccessToken("ACe2c924ce8254191edcea440ac8802b87","SKaeb6d7b26adb79e1d74b7f1889afaad4","iR2Qaw0cg7PCwMLvtV8Oqxf6G03X1552");

}

public function access_token1_post(){

	$sid    = "AC50352170c8ed9744b2c4ccb3cf42f4c4";

	$token  = "c8ead357ad788b270266ee1d4a82e7bc";

	$twilio = new Client($sid, $token);

	$new_key = $twilio->newKeys

	->create();

//print_r($new_key);die;

	$incoming_phone_number = $twilio->incomingPhoneNumbers

	->create(array(

		"phoneNumber" => "+15005550006",

		"voiceUrl" => "http://demo.twilio.com/docs/voice.xml"

	)

);

	$twilioAccountSid = 'AC50352170c8ed9744b2c4ccb3cf42f4c4';

//$twilioApiKey = 'c8ead357ad788b270266ee1d4a82e7bc';

	$twilioApiKey = 'SKc94350436e67ad70e8db146a590272dc';

	$twilioApiSecret = 'B8ZKYbl5XlbWS43ZRMGFFQ0JXQii2Pqp';

	$outgoingApplicationSid = 'APdbdbe2a3099a474f9b09d0ecaf82b5d7';

	$token = new AccessToken(

		$twilioAccountSid,

		$twilioApiKey,

		$twilioApiSecret

	);

	$voiceGrant = new VoiceGrant();

	$voiceGrant->setOutgoingApplicationSid($outgoingApplicationSid);

// $voiceGrant->setIncomingAllow(true);

	$token->addGrant($voiceGrant);

// render token to string

	echo $token->toJWT();die;

}

// view doctor appointment

public function show_appointments_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('appointment_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));

		$this->response($resp);

	}

	$AppData = $this->Api_model->viewAppointments($data['appointment_id']);

//print_r("<pre/>");

//print_r($AppData);

//die;

	if(!empty($AppData)){

		foreach ( $AppData as $row ){

			$appointment[] = array(

				'id' => $row['dr_appointment_id'],

				'doctor_name' => $row['doctor_name'],

				'doctor_id' => $row['doctor_id'],

				'doctor_fees' => $row['doctor_fees'],

				'doctor_address' => $row['doctor_address'],

				'contact' => $row['doctor_mob_no'],

				'date' => $row['avdate_date'],

				'time' => $row['avtime_text'],

				'category_name'=>$this->getCategory($row['doctor_category_id']),

				'category_id' => $row['doctor_category_id'],

				'image'=>base_url().'uploads/doctor/'.$row['doctor_pic'],

				'time_id'=>$row['dr_appointment_time_id'],

				'date_id'=>$row['dr_appointment_date_id'],

				'reminder' => $row['appointments_reminder'],

				'doctor_image'=>base_url().'uploads/doctor/'.$row['doctor_pic'],

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('appointment_data' => $appointment));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Favourite Data not found','response' => array('message' => 'Favourite Data not found'));

	}

	$this->response($resp);

}

// view doctor appointment

public function list_appointments_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));

		$this->response($resp);

	}

	$AppData = $this->Api_model->showAppointments($data['user_id']);

	// echo "<pre>";

	// print_r($AppData);

	// die;

	if(!empty($AppData)){

		foreach ( $AppData as $row ){

			$currentDate = date('m-d-Y');

			$appointDate= date('m-d-Y',strtotime($row['avdate_date']));

			if($appointDate >= $currentDate){

				// echo "yes";

				// die;

				// $appointment[] = array(

				// 	'test'=> $appointDate,

				// 	'tes'=>date('m-d-Y H:i:s')

				// );

				$appointment[] = array(

					'id' => $row['dr_appointment_id'],

					'doctor_name' => $row['doctor_name'],

					'date' => date('m-d-Y',strtotime($row['avdate_date'])),

					'time' => $row['avtime_text'],

				);

			}

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('appointment_data' => $appointment));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Appointments Data not found','response' => array('message' => 'Appointments Data not found'));

	}

	$this->response($resp);

}

/*delete appointment */

public function appointmentdelete_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('appointment_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$ids = explode(',',$data['appointment_id']);

	$emailid =$this->common_model->deleteAppointments($ids);

	if($emailid)

	{

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('appointment_data' => "Appointment deleted successfully"));

	}

	else

	{

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Email not found','response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

/*----Doctor list---------*/

public function get_all_doctor_list_post()

{

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$doctors_list  = $this->Api_model->getAllor('cp_doctor','doctor_id','desc');

	if(!empty($doctors_list)){

		foreach($doctors_list as $doctor_details){

			$doctorData[] = array('id'=> $doctor_details->doctor_id,'doctor_name'=>$doctor_details->doctor_name);

			$test_other = array('id'=> 0,'doctor_name'=>'Other');

			$merged_array = array_merge($doctorData, array($test_other));

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('doctor_data' => $merged_array));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Doctor not found','response' => array('message' => 'Doctor not found'));

	}

	$this->response($resp);

}

/*-------Test type list--------*/

public function get_all_test_type_post()

{

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$test_type_list  = $this->Api_model->getAllor('cp_test_type','test_type','asc');

	if(!empty($test_type_list)){

		foreach($test_type_list as $type_details){

			$typeData[] = array('id'=> $type_details->id,

				'test_type_name'=>$type_details->test_type);

			$test_other = array('id'=> 0,'test_type_name'=>'Other');

			$merged_array = array_merge($typeData, array($test_other));

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('doctor_data' => $merged_array));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Test type not found','response' => array('message' => 'Test type not found'));

	}

	$this->response($resp);

}

/*-------Delete medicine schedule--------*/

public function delete_medicine_schedule_post()

{

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('medicine_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$ids = explode(',',$data['medicine_id']);

	$pickup_medicine = $this->common_model->getsingle("picup_medicine",array("medicine_id" => $data['medicine_id']));

	$acknowledge_medicine = $this->common_model->getsingle("acknowledge",array("medicine_id" => $data['medicine_id']));

	if(empty($pickup_medicine) && empty($acknowledge_medicine)){

		$emailid =$this->common_model->deletemedicineschedule($ids);

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('medicine_data' => "Medicine deleted successfully"));

	}else{

		$resp = array('status_code' => SUCCESS,'status' => 'false', 'message' => 'Some error occured, please try again','response' => array('message' => 'Some error occured, please try again'));

	}

	if($emailid) {

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('medicine_data' => "Medicine deleted successfully"));

	}

	else {

		$resp = array('status_code' => SUCCESS,'status' => 'false', 'message' => 'Medicine not found','response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function delete_test_report_post()

{

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('report_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$ids = explode(',',$data['report_id']);

	$emailid =$this->common_model->deletetestreport($ids);

	if($emailid) {

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('email_data' => "email data deleted succesfully"));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Email not found','response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function show_all_test_report1_post()

{

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$limit = 5;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));

		$this->response($resp);

	}

	if($data['page_no']==''){

		$data['page_no']=1;

	}

	$emailCount =$this->common_model->gettestreportCount1($data['user_id']);

	$start  = ($data['page_no']-1)*$limit;

	$pages = ceil(count($emailCount) / $limit);

	$emailData =$this->common_model->getalltestreport($start,$limit,$data['user_id']);

// print_r("<pre/>");

//print_r($emailData);

// die;

	if(!empty($emailData)){

		foreach($emailData as $row){

			$doctor_name = $this->common_model->getsingle("cp_doctor",array("doctor_id" => $row['doctor_id']));

			$test_type = $this->common_model->getsingle("cp_test_type",array("id" => $row['test_type_id']));

			$username = $this->common_model->getsingle("cp_users",array("id" => $row['user_id']));

// print_r($test_type);

			$email[] = array(

				'id' => $row['id'],

				'user_id' => $row['user_id'],

				'doctor_name' => $doctor_name->doctor_name,

				'test_type' => $test_type->test_type,

				'image' => base_url().'uploads/test_report/'.$row['image'],

				'user_name' => $username->username,

			);

		}

//print_r($email);die;

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('email_data' => $email));

	}

	else {

		$email = array();

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Email not found','response' => array('message' => 'Email not found','email_data' => $email));

	}

//print_r($resp);die;

	$this->response($resp);

}

public function test_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$limit = 5;

	if(!isset($data['page_no'])==''){

		$data['page_no']=1;

	}

/* $start  = ($data['page_no']-1)*$limit;

$con1['returnType'] = 'count';

$table = "cp_test_report";

$category_count  = $this->Api_model->getRows($table,$con1);

$pages = ceil($category_count / $limit);

$con['sorting'] = array("id"=>"DESC");

if($data['page_no']){

$con['limit'] = $limit;

$con['start'] = $start;

}*/





$pages='';



$table = "cp_test_report";

$con['sorting'] = array("id"=>"DESC");

$categoryDatas  = $this->Api_model->testlist($data['user_id']);

if(!empty($categoryDatas)){

	foreach($categoryDatas as $categoryData_details){

		if($categoryData_details['test_type_id'] == '0'){

		}

		$testunit = $this->common_model->getsingle('cp_vital_test_type_unit', array('test_type_id' => $categoryData_details['type_id']));

		$categoryeData[] = array('id'=>$categoryData_details['id'],

			'name'=>$categoryData_details['test_report_title'],

			'user_id'=>$categoryData_details['user_id'],

			'description'=>$categoryData_details['description'],

			'image'=>base_url().'uploads/test_report/'.$categoryData_details['image'],

			'doctor_name'=>$categoryData_details['doctor_name'],

			'doctor_id'=>$categoryData_details['doctor_id'],

			'test_type_id'=>$categoryData_details['type_id'],

			'test_type_name'=>$categoryData_details['test_type_name'],

			'unit' => (!empty($testunit->unit) ? $testunit->unit : ''),

		);

	}

	$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('test_data' => $categoryeData));

} else {

	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Test Report not found','response' => array('message' => 'Test Report not found'));

}

$this->response($resp);

}

/*-------Acknowledge list--------*/

public function acknowledge_list_post(){

	//die('check');  

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array("user_id");

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$con['sorting'] = array("acknowledge_id"=>"DESC");

	$acknowledgeData  = $this->Api_model->acknowledgeList($data['user_id']);

	if(!empty($acknowledgeData)){

		foreach($acknowledgeData as $acknowledgeData_datails){

			$acknowledgeData1[] = array('id'=>$acknowledgeData_datails['acknowledge_id'],

				'description'=>$acknowledgeData_datails['descp'],

				'title'=>$acknowledgeData_datails['title'],

				'medicine_name'=>$acknowledgeData_datails['medicine'],

				'medicine_name2'=>$acknowledgeData_datails['medicine2'],

				'medicine_name3'=>$acknowledgeData_datails['medicine3'],

				'pharma_company'=>$acknowledgeData_datails['pharma_name'],

				'user_id'=>$data['user_id'],

				'name'=>$acknowledgeData_datails['users_name'],

				'lastname'=>$acknowledgeData_datails['lastname'],

				'dob'=>$acknowledgeData_datails['dob'],

				'selected_user'=>$acknowledgeData_datails['selected_user'],

				'send_date'=>$acknowledgeData_datails['send_date'],

				'send_time'=>$acknowledgeData_datails['send_time'],

				'date'=>$acknowledgeData_datails['acknowledge_created'],

				'rx_number' => $acknowledgeData_datails['rx_number'],

				'supply_days' => $acknowledgeData_datails['supply_days'],

				'supply_days2' => $acknowledgeData_datails['supply2_days'],

				'supply_days3' => $acknowledgeData_datails['supply3_days'],

				//'user_id'=>$acknowledgeData_datails['user_id'],

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('acknowledge_data' => $acknowledgeData1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Acknowledge not found','response' => array('message' => 'Acknowledge Data not found'));

	}

	$this->response($resp);

}

/*-------Send Acknowledge--------*/

public function acknowledge_send_post(){

//die('testing');

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array("pharma_company_id","medicine_id","user_id","select_user_id","date","time");

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	if(!empty($data['rx_number'])){

		$test_rx = $data['rx_number'];

	}else{

		$test_rx = '';

	}

	if(!empty($data['medicine2_id'])){

		$medicine2_id = $data['medicine2_id'];

	}else{

		$medicine2_id = '';

	}

	if(!empty($data['medicine3_id'])){

		$medicine3_id = $data['medicine3_id'];

	}else{

		$medicine3_id = '';

	}

	if(!empty($data['supply_days'])){

		$test_supply = $data['supply_days'];

	}else{

		$test_supply = '';

	}

	if(!empty($data['supply2_days'])){

		$supply2_days = $data['supply2_days'];

	}else{

		$supply2_days = '';

	}

	if(!empty($data['supply3_days'])){

		$supply3_days = $data['supply3_days'];

	}else{

		$supply3_days = '';

	}

	$post_data = array

	(

		'acknowledge_desc'=> $data['desc'],

//'acknowledge_title'=> $data['title'],

		'pharma_company_id '=> $data['pharma_company_id'],

		'medicine_id'=> $data['medicine_id'],

		'medicine2_id'=> $medicine2_id,

		'medicine3_id'=> $medicine3_id,

		'user_id'=> $data['user_id'],

		'name'=> $data['name'],

		'lastname'=> $data['lastname'],

		'dob'=> $data['dob'],

		'send_date'=>$data['date'],

		'send_time'=>$data['time'],

		'selected_user'=>$data['select_user_id'],

		'rx_number' => $test_rx,

		'supply_days' => $test_supply,

		'supply2_days' => $supply2_days,

		'supply3_days' => $supply3_days,

		'acknowledge_created' => date('Y-m-d H:i:s'),

	);

	$acknowledgeId = $this->common_model->addRecords('acknowledge', $post_data);

	$acknowledgeData = $this->common_model->getSingleRecordById('acknowledge', array('acknowledge_id' => $acknowledgeId));

	// $medicine_id = $acknowledgeData['medicine_id'];

	// $medicineData = $this->common_model->getSingleRecordById('medicine_schedule', array('medicine_id' => $medicine_id));

	// $medicine_name = $medicineData['medicine_name'];

	// echo "<pre>";

	// print_r($acknowledgeData['medicine_id']);

	// die;

	$email_pharma_company = $this->common_model->getsingle("pharma_company",array("id"=> $data['pharma_company_id']));

	$medicine_name_mail = $this->common_model->getsingle("medicine_schedule",array("medicine_id"=> $data['medicine_id']));

	if(!empty($medicine2_id)){

		$medicine_name_mail2 = $this->common_model->getsingle("medicine_schedule",array("medicine_id"=> $medicine2_id));

	}else{

		$medicine_name_mail2='';

	}

	if(!empty($medicine3_id)){

		$medicine_name_mail3 = $this->common_model->getsingle("medicine_schedule",array("medicine_id"=> $medicine3_id));

	}else{

		$medicine_name_mail3='';

	}

	$username_mail = $this->common_model->getsingle("cp_users",array("id"=> $data['user_id']));

	$selectUser_mail = $this->common_model->getsingle("cp_users",array("id"=> $data['select_user_id']));

	$from_m = "info@carepro.com";

	$fromname = "Carepro";

	if(empty($medicine_name_mail2) && empty($medicine_name_mail3)){

		$message ="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

		<html xmlns='http://www.w3.org/1999/xhtml'>

		<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>

		<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>

		</head>

		<body style=margin: 0;font-family:'open sans';'>

		<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;position:relative;'>

		<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>

		<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin:0 auto 0px;'>

		<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

		</a>

		</div>

		<div style='border: none;padding:0px 60px; margin:-40px auto 0px; border-radius:0px; position: absolute; max-width: 580px; top: 110px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>

		<div class='temp_cont' style='width:100%; margin:-40px auto 0px !important;'>

		<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>

		<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

		<span style='display:block;width:100%;'>

		<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>

		</span>

		Medicine Pick up request

		</h2>

		<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

		Hi &nbsp;".$username_mail->username.",

		</h3>

		<p style='text-align: center; font-size: 14px; margin-top: 0px;'>Patient Name has requested you to pick up medicines from pharmacy, please see medicine and pharmacy information below</p>

		<style>

							#customers td, #customers th {

		border:1px solid #ddd;

		padding:8px;

	}

							#customers th {

	padding-top:8px;

	padding-bottom:8px;

	text-align:left;

	background-color:#4CAF50;

	color:white;

}

</style>

<table id='customers' style='width:100%; border-collapse: collapse;'>

<tr>

<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>#1 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$medicine_name_mail->medicine_name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; text-align:left; background-color:#4CAF50; color:#fff;'>Description:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$data['desc']."</td>

</tr> 

<tr>

<th style='width:200px; font-size:15px; padding:8px; text-align:left; background-color:#4CAF50;color:#fff;border: 1px solid #ddd;'>Date and Time for pickup:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$acknowledgeData['send_date'].",".$acknowledgeData['send_time']."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; padding:8px; border: 1px solid #ddd; text-align:left; background-color:#4CAF50;color:#fff;'>Pharmacy Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Address:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->address."<isindex></isindex></td>

</tr>

<tr>

<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Number:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->contact_no."</td>

</tr> 

</table>

</div>

<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%;     box-sizing: border-box;'>

<p style='font-size: 13px;margin: 0 0 8px;'>Please call 'sender user name' if you are not able to pick up these medicines</p>						

<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>

<div style='width:50%; float:left;'>

<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>

</p>

</div>

<div style='width:50%; float:right;'>

<div style='border:none;'>

<a href='#' target='_blank' style='display:inline-block;'>

<img style='max-width:120px;width:100%;margin:0 auto 10px;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

</a>

</div>

<p style='font-size: 13px;margin:0 0 8px;'>Carepro customer service</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>9876543210</a>

</p>

</div>

</div>

</div>

</div>

</div>

</body>

</html>";

}else if(!empty($medicine_name_mail2) && empty($medicine_name_mail3)){

	$message ="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

	<html xmlns='http://www.w3.org/1999/xhtml'>

	<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>

	<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>

	</head>

	<body style='margin: 0;font-family:'open sans';'>

	<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;position:relative;'>

	<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>

	<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin:0 auto 0px;'>

	<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

	</a>

	</div>

	<div style='border: none;padding:0px 60px; margin:-40px auto 0px; border-radius:0px; position: absolute; max-width: 580px; top: 110px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>

	<div class='temp_cont' style='width:100%; margin:-40px auto 0px !important;'>

	<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>

	<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

	<span style='display:block;width:100%;'>

	<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>

	</span>

	Medicine Pick up request

	</h2>

	<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

	Hi &nbsp;".$username_mail->username.",

	</h3>

	<p style='text-align: center; font-size: 14px; margin-top: 0px;'>Patient Name has requested you to pick up medicines from pharmacy, please see medicine and pharmacy information below</p>

	<style>

							#customers td, #customers th {

	border:1px solid #ddd;

	padding:8px;

}

							#customers th {

padding-top:8px;

padding-bottom:8px;

text-align:left;

background-color:#4CAF50;

color:white;

}

</style>

<table id='customers' style='width:100%; border-collapse: collapse;'>

<tr>

<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#1 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$medicine_name_mail->medicine_name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#2 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$medicine_name_mail2->medicine_name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; text-align:left; background-color:#4CAF50; color:#fff;'>Description:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$data['desc']."</td>

</tr> 

<tr>

<th style='width:200px; font-size:15px; padding:8px; text-align:left; background-color:#4CAF50;color:#fff;border: 1px solid #ddd;'>Date and Time for pickup:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$acknowledgeData['send_date'].",".$acknowledgeData['send_time']."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; padding:8px; border: 1px solid #ddd; text-align:left; background-color:#4CAF50;color:#fff;'>Pharmacy Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Address:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->address."<isindex></isindex></td>

</tr>

<tr>

<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Number:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->contact_no."</td>

</tr> 

</table>

</div>

<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%;     box-sizing: border-box;'>

<p style='font-size: 13px;margin: 0 0 8px;'>Please call 'sender user name' if you are not able to pick up these medicines</p>						

<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>

<div style='width:50%; float:left;'>

<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>

</p>

</div>

<div style='width:50%; float:right;'>

<div style='border:none;'>

<a href='#' target='_blank' style='display:inline-block;'>

<img style='max-width:120px;width:100%;margin:0 auto 10px;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

</a>

</div>

<p style='font-size: 13px;margin:0 0 8px;'>Carepro customer service</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>9876543210</a>

</p>

</div>

</div>

</div>

</div>

</div>

</body>

</html>";

}else if(!empty($medicine_name_mail3) && empty($medicine_name_mail2)){

	$message ="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

	<html xmlns='http://www.w3.org/1999/xhtml'>

	<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>

	<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>

	</head>

	<body style='margin: 0;font-family:'open sans';'>

	<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;position:relative;'>

	<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>

	<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin:0 auto 0px;'>

	<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

	</a>

	</div>

	<div style='border: none;padding:0px 60px; margin:-40px auto 0px; border-radius:0px; position: absolute; max-width: 580px; top: 110px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>

	<div class='temp_cont' style='width:100%; margin:-40px auto 0px !important;'>

	<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>

	<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

	<span style='display:block;width:100%;'>

	<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>

	</span>

	Medicine Pick up request

	</h2>

	<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

	Hi &nbsp;".$username_mail->username.",

	</h3>

	<p style='text-align: center; font-size: 14px; margin-top: 0px;'>Patient Name has requested you to pick up medicines from pharmacy, please see medicine and pharmacy information below</p>

	<style>

							#customers td, #customers th {

	border:1px solid #ddd;

	padding:8px;

}

							#customers th {

padding-top:8px;

padding-bottom:8px;

text-align:left;

background-color:#4CAF50;

color:white;

}

</style>

<table id='customers' style='width:100%; border-collapse: collapse;'>

<tr>

<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#1 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$medicine_name_mail->medicine_name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#2 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$medicine_name_mail3->medicine_name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; text-align:left; background-color:#4CAF50; color:#fff;'>Description:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$data['desc']."</td>

</tr> 

<tr>

<th style='width:200px; font-size:15px; padding:8px; text-align:left; background-color:#4CAF50;color:#fff;border: 1px solid #ddd;'>Date and Time for pickup:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$acknowledgeData['send_date'].",".$acknowledgeData['send_time']."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; padding:8px; border: 1px solid #ddd; text-align:left; background-color:#4CAF50;color:#fff;'>Pharmacy Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Address:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->address."<isindex></isindex></td>

</tr>

<tr>

<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Number:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->contact_no."</td>

</tr> 

</table>

</div>

<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%;     box-sizing: border-box;'>

<p style='font-size: 13px;margin: 0 0 8px;'>Please call 'sender user name' if you are not able to pick up these medicines</p>						

<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>

<div style='width:50%; float:left;'>

<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>

</p>

</div>

<div style='width:50%; float:right;'>

<div style='border:none;'>

<a href='#' target='_blank' style='display:inline-block;'>

<img style='max-width:120px;width:100%;margin:0 auto 10px;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

</a>

</div>

<p style='font-size: 13px;margin:0 0 8px;'>Carepro customer service</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>9876543210</a>

</p>

</div>

</div>

</div>

</div>

</div>

</body>

</html>";

}else if(!empty($medicine_name_mail3) && !empty($medicine_name_mail2)){

	$message ="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

	<html xmlns='http://www.w3.org/1999/xhtml'>

	<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>

	<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>

	</head>

	<body style='margin: 0;font-family:'open sans';'>

	<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;position:relative;'>

	<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>

	<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin:0 auto 0px;'>

	<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

	</a>

	</div>

	<div style='border: none;padding:0px 60px; margin:-40px auto 0px; border-radius:0px; position: absolute; max-width: 580px; top: 110px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>

	<div class='temp_cont' style='width:100%; margin:-40px auto 0px !important;'>

	<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>

	<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

	<span style='display:block;width:100%;'>

	<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>

	</span>

	Medicine Pick up request

	</h2>

	<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

	Hi &nbsp;".$username_mail->username.",

	</h3>

	<p style='text-align: center; font-size: 14px; margin-top: 0px;'>Patient Name has requested you to pick up medicines from pharmacy, please see medicine and pharmacy information below</p>

	<style>

							#customers td, #customers th {

	border:1px solid #ddd;

	padding:8px;

}

							#customers th {

padding-top:8px;

padding-bottom:8px;

text-align:left;

background-color:#4CAF50;

color:white;

}

</style>

<table id='customers' style='width:100%; border-collapse: collapse;'>

<tr>

<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#1 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$medicine_name_mail->medicine_name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#2 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$medicine_name_mail2->medicine_name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#3 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$medicine_name_mail3->medicine_name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; text-align:left; background-color:#4CAF50; color:#fff;'>Description:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$data['desc']."</td>

</tr> 

<tr>

<th style='width:200px; font-size:15px; padding:8px; text-align:left; background-color:#4CAF50;color:#fff;border: 1px solid #ddd;'>Date and Time for pickup:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$acknowledgeData['send_date'].",".$acknowledgeData['send_time']."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; padding:8px; border: 1px solid #ddd; text-align:left; background-color:#4CAF50;color:#fff;'>Pharmacy Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Address:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->address."<isindex></isindex></td>

</tr>

<tr>

<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Number:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->contact_no."</td>

</tr> 

</table>

</div>

<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%;     box-sizing: border-box;'>

<p style='font-size: 13px;margin: 0 0 8px;'>Please call 'sender user name' if you are not able to pick up these medicines</p>						

<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>

<div style='width:50%; float:left;'>

<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>

</p>

</div>

<div style='width:50%; float:right;'>

<div style='border:none;'>

<a href='#' target='_blank' style='display:inline-block;'>

<img style='max-width:120px;width:100%;margin:0 auto 10px;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

</a>

</div>

<p style='font-size: 13px;margin:0 0 8px;'>Carepro customer service</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>9876543210</a>

</p>

</div>

</div>

</div>

</div>

</div>

</body>

</html>";

}

socialEmail($email_pharma_company->email,$from_m,$fromname,"Acknowledge",$message);

socialEmail($selectUser_mail->email,$from_m,$fromname,"Acknowledge",$message);

if($acknowledgeId){

	$acknowledgeData = $this->common_model->getSingleRecordById('acknowledge', array('acknowledge_id' => $acknowledgeId));

	$acknowledgeData1 = array(

		'id' => $acknowledgeData['acknowledge_id'],

		'desc' => $acknowledgeData['acknowledge_desc'],

        //'title' => $acknowledgeData['acknowledge_title'],

		'medicine_id' => $acknowledgeData['medicine_id'],

		'select_user_id' => $data['select_user_id'],

		'date' => $data['date'],

		'time' => $data['time'],

		'rx_number' => $data['rx_number'],

		'supply_days' => $data['supply_days'],

		'pharma_company_id' => $acknowledgeData['pharma_company_id'],

	);

	$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Acknowledge added successfully', 'response' => $acknowledgeData1);

}

else

{

	$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

}

$this->response($resp);

}

/*-------Send Acknowledge--------*/

public function send_test_mail_post(){

//die('testing');

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array("pharma_company_id","medicine_id","user_id","select_user_id","date","time");

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	if(!empty($data['rx_number'])){

		$test_rx = $data['rx_number'];

	}else{

		$test_rx = '';

	}

	if(!empty($data['supply_days'])){

		$test_supply = $data['supply_days'];

	}else{

		$test_supply = '';

	}

	$post_data = array

	(

		'acknowledge_desc'=> $data['desc'],

//'acknowledge_title'=> $data['title'],

		'pharma_company_id '=> $data['pharma_company_id'],

		'medicine_id'=> $data['medicine_id'],

		'user_id'=> $data['user_id'],

		'name'=> $data['name'],

		'lastname'=> $data['lastname'],

		'dob'=> $data['dob'],

		'send_date'=>$data['date'],

		'send_time'=>$data['time'],

		'selected_user'=>$data['select_user_id'],

		'rx_number' => $test_rx,

		'supply_days' => $test_supply,

		'acknowledge_created' => date('Y-m-d H:i:s'),

	);

	$acknowledgeId = $this->common_model->addRecords('acknowledge', $post_data);

	$acknowledgeData = $this->common_model->getSingleRecordById('acknowledge', array('acknowledge_id' => $acknowledgeId));

	// $medicine_id = $acknowledgeData['medicine_id'];

	// $medicineData = $this->common_model->getSingleRecordById('medicine_schedule', array('medicine_id' => $medicine_id));

	// $medicine_name = $medicineData['medicine_name'];

	// echo "<pre>";

	// print_r($acknowledgeData['medicine_id']);

	// die;

	$email_pharma_company = $this->common_model->getsingle("pharma_company",array("id"=> $data['pharma_company_id']));

	$medicine_name_mail = $this->common_model->getsingle("medicine_schedule",array("medicine_id"=> $data['medicine_id']));

	$username_mail = $this->common_model->getsingle("cp_users",array("id"=> $data['user_id']));

	$selectUser_mail = $this->common_model->getsingle("cp_users",array("id"=> $data['select_user_id']));

	$from_m = "info@carepro.com";

	$fromname = "Carepro";

	$message ="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

	<html xmlns='http://www.w3.org/1999/xhtml'>

	<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>

	<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>

	</head>

	<body style='margin: 0;font-family:'open sans';'>

	<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;'>

	<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>

	<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin: 0 auto 0px;'>

	<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

	</a>

	</div>

	<div style='border:none; padding:0px 60px; max-width:580px; margin:-40px auto 0px; border-radius:0px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>

	<div class='temp_cont' style='width:100%; margin:0 auto;'>

	<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>

	<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

	<span style='display:block;width:100%;'>

	<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>

	</span>

	Subject here

	</h2>

	<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

	Hi &nbsp;".$username_mail->username.",

	</h3>

	<style>

						#customers td, #customers th {

	border:1px solid #ddd;

	padding:8px;

}

						#customers th {

padding-top:8px;

padding-bottom:8px;

text-align:left;

background-color:#4CAF50;

color:white;

}

</style>

<table id='customers' style='width:100%; border-collapse: collapse;'>

<tr>

<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>Jhon</td>

</tr>						  

<tr>

<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Date and Time:</th>

<td style='padding:8px;border:1px solid #ddd;'>24-11-2019, 03:00 AM</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Description:</th>

<td style='font-size:13px; padding:8px;border:1px solid #ddd;'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</td>

</tr>

</table>

</div>

<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%; box-sizing:border-box;'>

<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>

</p>						

<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>

<p style='font-size: 13px;margin:0 0 8px;'>Care Pro</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>EMAIL: info@carepro.com/</a>

</p>

</div>

</div>

</div>

</div>

</body>

</html>";

socialEmail($email_pharma_company->email,$from_m,$fromname,"Acknowledge",$message);

socialEmail($selectUser_mail->email,$from_m,$fromname,"Acknowledge",$message);

if($acknowledgeId){

	$acknowledgeData = $this->common_model->getSingleRecordById('acknowledge', array('acknowledge_id' => $acknowledgeId));

	$acknowledgeData1 = array(

		'id' => $acknowledgeData['acknowledge_id'],

		'desc' => $acknowledgeData['acknowledge_desc'],

        //'title' => $acknowledgeData['acknowledge_title'],

		'medicine_id' => $acknowledgeData['medicine_id'],

		'select_user_id' => $data['select_user_id'],

		'date' => $data['date'],

		'time' => $data['time'],

		'rx_number' => $data['rx_number'],

		'supply_days' => $data['supply_days'],

		'pharma_company_id' => $acknowledgeData['pharma_company_id'],

	);

	$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Acknowledge added successfully', 'response' => $acknowledgeData1);

}

else

{

	$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

}

$this->response($resp);

}

public function update_acknowledge_post()

{

	//die('test');

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array("pharma_company_id","medicine_id","acknowledge_id","select_user_id","date","time");

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	if(!empty($data['rx_number'])){

		$test_rx = $data['rx_number'];

	}else{

		$test_rx = '';

	}

	if(!empty($data['medicine2_id'])){

		$medicine2_id = $data['medicine2_id'];

	}else{

		$medicine2_id = '';

	}

	if(!empty($data['medicine3_id'])){

		$medicine3_id = $data['medicine3_id'];

	}else{

		$medicine3_id = '';

	}

	if(!empty($data['supply_days'])){

		$test_supply = $data['supply_days'];

	}else{

		$test_supply = '';

	}

	if(!empty($data['supply2_days'])){

		$supply2_days = $data['supply2_days'];

	}else{

		$supply2_days = '';

	}

	if(!empty($data['supply3_days'])){

		$supply3_days = $data['supply3_days'];

	}else{

		$supply3_days = '';

	}

	$post_data = array

	(

		'acknowledge_desc'=> $data['desc'],

//'acknowledge_title'=> $data['title'],

		'name'=> $data['name'],

		'lastname'=> $data['lastname'],

		'dob'=> $data['dob'],

		'send_date'=>$data['date'],

		'send_time'=>$data['time'],

		'selected_user'=>$data['select_user_id'],

		'rx_number' => $test_rx,

		'supply_days' => $test_supply,

		'supply2_days' => $supply2_days,

		'supply3_days' => $supply3_days,

		'pharma_company_id'=> $data['pharma_company_id'],

		'medicine_id'=> $data['medicine_id'],

		'medicine2_id'=> $medicine2_id,

		'medicine3_id'=> $medicine3_id,

	);

	$acknowledgeId = $this->common_model->updateRecords('acknowledge', $post_data,array('acknowledge_id' => $data['acknowledge_id']));

	$email_pharma_company = $this->common_model->getsingle("pharma_company",array("id"=> $data['pharma_company_id']));

	if($acknowledgeId)

	{

		$acknowledgeData = $this->common_model->getSingleRecordById('acknowledge', array('acknowledge_id' => $data['acknowledge_id']));

		$email_pharma_company = $this->common_model->getsingle("pharma_company",array("id"=> $data['pharma_company_id']));

		$medicine_name_mail = $this->common_model->getsingle("medicine_schedule",array("medicine_id"=> $data['medicine_id']));

		if(!empty($medicine2_id)){

			$medicine_name_mail2 = $this->common_model->getsingle("medicine_schedule",array("medicine_id"=> $medicine2_id));

		}		

		if(!empty($medicine3_id)){

			$medicine_name_mail3 = $this->common_model->getsingle("medicine_schedule",array("medicine_id"=> $medicine3_id));

		}

		$username_mail = $this->common_model->getsingle("cp_users",array("id"=> $acknowledgeData['user_id']));

		$selectUser_mail = $this->common_model->getsingle("cp_users",array("id"=> $data['select_user_id']));

		$from_m = "info@carepro.com";

		$fromname = "Carepro";

		if(empty($medicine_name_mail2) && empty($medicine_name_mail3)){

			$message ="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

			<html xmlns='http://www.w3.org/1999/xhtml'>

			<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>

			<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>

			</head>

			<body style='margin: 0;font-family:'open sans';'>

			<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;position:relative;'>

			<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>

			<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin:0 auto 0px;'>

			<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

			</a>

			</div>

			<div style='border: none;padding:0px 60px; margin:-40px auto 0px; border-radius:0px; position: absolute; max-width: 580px; top: 110px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>

			<div class='temp_cont' style='width:100%; margin:-40px auto 0px !important;'>

			<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>

			<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

			<span style='display:block;width:100%;'>

			<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>

			</span>

			Medicine Pick up request

			</h2>

			<p style='text-align: center; font-size: 14px; margin-top: 0px;'>Patient Name has requested you to pick up medicines from pharmacy, please see medicine and pharmacy information below</p>

			<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

			Hi &nbsp;".$username_mail->username.",

			</h3>

			<style>

							#customers td, #customers th {

			border:1px solid #ddd;

			padding:8px;

		}

							#customers th {

		padding-top:8px;

		padding-bottom:8px;

		text-align:left;

		background-color:#4CAF50;

		color:white;

	}

	</style>

	<table id='customers' style='width:100%; border-collapse: collapse;'>

	<tr>

	<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#1 Medicine Name:</th>

	<td style='padding:8px;border:1px solid #ddd;'>".$medicine_name_mail->medicine_name."</td>

	</tr>

	<!-- <tr>

	<th style='width:200px; border: 1px solid #ddd; font-size:15px; padding:8px; background-color:#4CAF50; text-align:left; color:#fff;'>#2 Medicine Name:</th>

	<td style='padding:8px; border:1px solid #ddd;'>Ibuprofen</td>

	</tr>

	<tr>

	<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; background-color:#4CAF50; text-align:left;color:#fff;'>#3 Medicine Name:</th>

	<td style='padding:8px;border:1px solid #ddd;'>Tylenol</td>

	</tr>

	<tr>

	<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; background-color:#4CAF50; text-align:left;color:#fff;'>#4 Medicine Name:</th>

	<td style='padding:8px;border:1px solid #ddd;'>Ernst Handel</td>

	</tr>-->

	<tr>

	<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; text-align:left; background-color:#4CAF50; color:#fff;'>Description:</th>

	<td style='padding:8px;border:1px solid #ddd;'>".$data['desc']."</td>

	</tr> 

	<tr>

	<th style='width:200px; font-size:15px; padding:8px; text-align:left; background-color:#4CAF50;color:#fff;border: 1px solid #ddd;'>Date and Time for pickup:</th>

	<td style='padding:8px;border:1px solid #ddd;'>".$acknowledgeData['send_date'].",".$acknowledgeData['send_time']."</td>

	</tr>

	<tr>

	<th style='width:200px; font-size:15px; padding:8px; border: 1px solid #ddd; text-align:left; background-color:#4CAF50;color:#fff;'>Pharmacy Name:</th>

	<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->name."</td>

	</tr>

	<tr>

	<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Address:</th>

	<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->address."<isindex></isindex></td>

	</tr>

	<tr>

	<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Number:</th>

	<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->contact_no."</td>

	</tr> 

	</table>

	</div>

	<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%;     box-sizing: border-box;'>

	<p style='font-size: 13px;margin: 0 0 8px;'>Please call 'sender user name' if you are not able to pick up these medicines</p>						

	<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>

	<div style='width:50%; float:left;'>

	<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>

	<p style='font-size: 13px;margin:0;'>

	<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>

	</p>

	</div>

	<div style='width:50%; float:right;'>

	<div style='border:none;'>

	<a href='#' target='_blank' style='display:inline-block;'>

	<img style='max-width:120px;width:100%;margin:0 auto 10px;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

	</a>

	</div>

	<p style='font-size: 13px;margin:0 0 8px;'>Carepro customer service</p>

	<p style='font-size: 13px;margin:0;'>

	<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>9876543210</a>

	</p>

	</div>

	</div>

	</div>

	</div>

	</div>

	</body>

	</html>";

}else if(!empty($medicine_name_mail2) && empty($medicine_name_mail3)){

	$message ="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

	<html xmlns='http://www.w3.org/1999/xhtml'>

	<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>

	<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>

	</head>

	<body style='margin: 0;font-family:'open sans';'>

	<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;position:relative;'>

	<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>

	<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin:0 auto 0px;'>

	<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

	</a>

	</div>

	<div style='border: none;padding:0px 60px; margin:-40px auto 0px; border-radius:0px; position: absolute; max-width: 580px; top: 110px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>

	<div class='temp_cont' style='width:100%; margin:-40px auto 0px !important;'>

	<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>

	<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

	<span style='display:block;width:100%;'>

	<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>

	</span>

	Medicine Pick up request

	</h2>

	<p style='text-align: center; font-size: 14px; margin-top: 0px;'>Patient Name has requested you to pick up medicines from pharmacy, please see medicine and pharmacy information below</p>

	<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

	Hi &nbsp;".$username_mail->username.",

	</h3>

	<style>

							#customers td, #customers th {

	border:1px solid #ddd;

	padding:8px;

}

							#customers th {

padding-top:8px;

padding-bottom:8px;

text-align:left;

background-color:#4CAF50;

color:white;

}

</style>

<table id='customers' style='width:100%; border-collapse: collapse;'>

<tr>

<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#1 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$medicine_name_mail->medicine_name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#2 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$medicine_name_mail2->medicine_name."</td>

</tr>

<!-- <tr>

<th style='width:200px; border: 1px solid #ddd; font-size:15px; padding:8px; background-color:#4CAF50; text-align:left; color:#fff;'>#2 Medicine Name:</th>

<td style='padding:8px; border:1px solid #ddd;'>Ibuprofen</td>

</tr>

<tr>

<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; background-color:#4CAF50; text-align:left;color:#fff;'>#3 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>Tylenol</td>

</tr>

<tr>

<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; background-color:#4CAF50; text-align:left;color:#fff;'>#4 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>Ernst Handel</td>

</tr>-->

<tr>

<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; text-align:left; background-color:#4CAF50; color:#fff;'>Description:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$data['desc']."</td>

</tr> 

<tr>

<th style='width:200px; font-size:15px; padding:8px; text-align:left; background-color:#4CAF50;color:#fff;border: 1px solid #ddd;'>Date and Time for pickup:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$acknowledgeData['send_date'].",".$acknowledgeData['send_time']."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; padding:8px; border: 1px solid #ddd; text-align:left; background-color:#4CAF50;color:#fff;'>Pharmacy Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Address:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->address."<isindex></isindex></td>

</tr>

<tr>

<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Number:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->contact_no."</td>

</tr> 

</table>

</div>

<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%;     box-sizing: border-box;'>

<p style='font-size: 13px;margin: 0 0 8px;'>Please call 'sender user name' if you are not able to pick up these medicines</p>						

<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>

<div style='width:50%; float:left;'>

<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>

</p>

</div>

<div style='width:50%; float:right;'>

<div style='border:none;'>

<a href='#' target='_blank' style='display:inline-block;'>

<img style='max-width:120px;width:100%;margin:0 auto 10px;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

</a>

</div>

<p style='font-size: 13px;margin:0 0 8px;'>Carepro customer service</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>9876543210</a>

</p>

</div>

</div>

</div>

</div>

</div>

</body>

</html>";

}else if(!empty($medicine_name_mail3) && empty($medicine_name_mail2)){

	$message ="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

	<html xmlns='http://www.w3.org/1999/xhtml'>

	<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>

	<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>

	</head>

	<body style='margin: 0;font-family:'open sans';'>

	<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;position:relative;'>

	<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>

	<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin:0 auto 0px;'>

	<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

	</a>

	</div>

	<div style='border: none;padding:0px 60px; margin:-40px auto 0px; border-radius:0px; position: absolute; max-width: 580px; top: 110px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>

	<div class='temp_cont' style='width:100%; margin:-40px auto 0px !important;'>

	<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>

	<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

	<span style='display:block;width:100%;'>

	<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>

	</span>

	Medicine Pick up request

	</h2>

	<p style='text-align: center; font-size: 14px; margin-top: 0px;'>Patient Name has requested you to pick up medicines from pharmacy, please see medicine and pharmacy information below</p>

	<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

	Hi &nbsp;".$username_mail->username.",

	</h3>

	<style>

							#customers td, #customers th {

	border:1px solid #ddd;

	padding:8px;

}

							#customers th {

padding-top:8px;

padding-bottom:8px;

text-align:left;

background-color:#4CAF50;

color:white;

}

</style>

<table id='customers' style='width:100%; border-collapse: collapse;'>

<tr>

<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#1 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$medicine_name_mail->medicine_name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#2 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$medicine_name_mail3->medicine_name."</td>

</tr>

<!-- <tr>

<th style='width:200px; border: 1px solid #ddd; font-size:15px; padding:8px; background-color:#4CAF50; text-align:left; color:#fff;'>#2 Medicine Name:</th>

<td style='padding:8px; border:1px solid #ddd;'>Ibuprofen</td>

</tr>

<tr>

<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; background-color:#4CAF50; text-align:left;color:#fff;'>#3 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>Tylenol</td>

</tr>

<tr>

<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; background-color:#4CAF50; text-align:left;color:#fff;'>#4 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>Ernst Handel</td>

</tr>-->

<tr>

<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; text-align:left; background-color:#4CAF50; color:#fff;'>Description:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$data['desc']."</td>

</tr> 

<tr>

<th style='width:200px; font-size:15px; padding:8px; text-align:left; background-color:#4CAF50;color:#fff;border: 1px solid #ddd;'>Date and Time for pickup:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$acknowledgeData['send_date'].",".$acknowledgeData['send_time']."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; padding:8px; border: 1px solid #ddd; text-align:left; background-color:#4CAF50;color:#fff;'>Pharmacy Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Address:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->address."<isindex></isindex></td>

</tr>

<tr>

<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Number:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->contact_no."</td>

</tr> 

</table>

</div>

<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%;     box-sizing: border-box;'>

<p style='font-size: 13px;margin: 0 0 8px;'>Please call 'sender user name' if you are not able to pick up these medicines</p>						

<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>

<div style='width:50%; float:left;'>

<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>

</p>

</div>

<div style='width:50%; float:right;'>

<div style='border:none;'>

<a href='#' target='_blank' style='display:inline-block;'>

<img style='max-width:120px;width:100%;margin:0 auto 10px;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

</a>

</div>

<p style='font-size: 13px;margin:0 0 8px;'>Carepro customer service</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>9876543210</a>

</p>

</div>

</div>

</div>

</div>

</div>

</body>

</html>";

}else if(!empty($medicine_name_mail3) && !empty($medicine_name_mail2)){

	$message ="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

	<html xmlns='http://www.w3.org/1999/xhtml'>

	<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>

	<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>

	</head>

	<body style='margin: 0;font-family:'open sans';'>

	<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;position:relative;'>

	<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>

	<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin:0 auto 0px;'>

	<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

	</a>

	</div>

	<div style='border: none;padding:0px 60px; margin:-40px auto 0px; border-radius:0px; position: absolute; max-width: 580px; top: 110px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>

	<div class='temp_cont' style='width:100%; margin:-40px auto 0px !important;'>

	<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>

	<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

	<span style='display:block;width:100%;'>

	<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>

	</span>

	Medicine Pick up request

	</h2>

	<p style='text-align: center; font-size: 14px; margin-top: 0px;'>Patient Name has requested you to pick up medicines from pharmacy, please see medicine and pharmacy information below</p>

	<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

	Hi &nbsp;".$username_mail->username.",

	</h3>

	<style>

							#customers td, #customers th {

	border:1px solid #ddd;

	padding:8px;

}

							#customers th {

padding-top:8px;

padding-bottom:8px;

text-align:left;

background-color:#4CAF50;

color:white;

}

</style>

<table id='customers' style='width:100%; border-collapse: collapse;'>

<tr>

<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#1 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$medicine_name_mail->medicine_name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#2 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$medicine_name_mail2->medicine_name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#3 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$medicine_name_mail3->medicine_name."</td>

</tr>

<!-- <tr>

<th style='width:200px; border: 1px solid #ddd; font-size:15px; padding:8px; background-color:#4CAF50; text-align:left; color:#fff;'>#2 Medicine Name:</th>

<td style='padding:8px; border:1px solid #ddd;'>Ibuprofen</td>

</tr>

<tr>

<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; background-color:#4CAF50; text-align:left;color:#fff;'>#3 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>Tylenol</td>

</tr>

<tr>

<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; background-color:#4CAF50; text-align:left;color:#fff;'>#4 Medicine Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>Ernst Handel</td>

</tr>-->

<tr>

<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; text-align:left; background-color:#4CAF50; color:#fff;'>Description:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$data['desc']."</td>

</tr> 

<tr>

<th style='width:200px; font-size:15px; padding:8px; text-align:left; background-color:#4CAF50;color:#fff;border: 1px solid #ddd;'>Date and Time for pickup:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$acknowledgeData['send_date'].",".$acknowledgeData['send_time']."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; padding:8px; border: 1px solid #ddd; text-align:left; background-color:#4CAF50;color:#fff;'>Pharmacy Name:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->name."</td>

</tr>

<tr>

<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Address:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->address."<isindex></isindex></td>

</tr>

<tr>

<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Number:</th>

<td style='padding:8px;border:1px solid #ddd;'>".$email_pharma_company->contact_no."</td>

</tr> 

</table>

</div>

<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%;     box-sizing: border-box;'>

<p style='font-size: 13px;margin: 0 0 8px;'>Please call 'sender user name' if you are not able to pick up these medicines</p>						

<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>

<div style='width:50%; float:left;'>

<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>

</p>

</div>

<div style='width:50%; float:right;'>

<div style='border:none;'>

<a href='#' target='_blank' style='display:inline-block;'>

<img style='max-width:120px;width:100%;margin:0 auto 10px;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

</a>

</div>

<p style='font-size: 13px;margin:0 0 8px;'>Carepro customer service</p>

<p style='font-size: 13px;margin:0;'>

<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>9876543210</a>

</p>

</div>

</div>

</div>

</div>

</div>

</body>

</html>";

}

socialEmail($email_pharma_company->email,$from_m,$fromname,"Acknowledge",$message);

socialEmail($selectUser_mail->email,$from_m,$fromname,"Acknowledge",$message);

$users1 = array(

	'id' => $acknowledgeData['acknowledge_id'],

	'desc' => $acknowledgeData['acknowledge_desc'],

	'select_user_id' => $data['select_user_id'],

	'date' => $data['date'],

	'time' => $data['time'],

	'medicine_id' => $acknowledgeData['medicine_id'],

	'rx_number' => $data['rx_number'],

	'supply_days' => $data['supply_days'],

	'pharma_company_id' => $acknowledgeData['pharma_company_id'],

);

$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Acknowledge update successfully', 'response' => $users1);

}

else

{

	$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

}

$this->response($resp);

}

public function delete_acknowledge_post()

{

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('acknowledge_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$ids = explode(',',$data['acknowledge_id']);

	$emailid =$this->common_model->deleteacknowledge($ids);

	if($emailid) {

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('acknowledge_data' => "Acknowledgment deleted successfully"));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Email not found','response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

/*-------Pharma list--------*/

public function pharma_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$con['sorting'] = array("id"=>"DESC");

	$acknowledgeData  = $this->Api_model->newpharmaList();

	if(!empty($acknowledgeData)){

		foreach($acknowledgeData as $acknowledgeData_datails){

			$acknowledgeData1[] = array('id'=>$acknowledgeData_datails['id'],

				'name'=>$acknowledgeData_datails['name'],

				'email'=>$acknowledgeData_datails['email'],

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('pharma_data' => $acknowledgeData1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Doctor not found','response' => array('message' => 'Acknowledge Data not found'));

	}

	$this->response($resp);

}

/*-------Article list--------*/

public function article_list1_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$con['sorting'] = array("article_id"=>"DESC");

	$acknowledgeData  = $this->Api_model->articleList();

	if(!empty($acknowledgeData)){

		foreach($acknowledgeData as $acknowledgeData_datails){

			$FavPhotoData1 = $this->common_model->getSingleRecordById('cp_article_trash', array('user_id' => $data['user_id'],"article_id"=>$acknowledgeData_datails['article_id']));

			if($FavPhotoData1){

				continue;

			}

			$fav  = $this->getArticleFav($acknowledgeData_datails['article_id']);

			$acknowledgeData1[] = array('id'=>$acknowledgeData_datails['article_id'],

				'title'=>$acknowledgeData_datails['article_title'],

				'artist_name'=>$acknowledgeData_datails['artist_name'],

				'description'=>$acknowledgeData_datails['article_desc'],

				'article_image'=>base_url().'uploads/articles/'.$acknowledgeData_datails['article_image'],

				'artist_image'=>base_url().'uploads/articles/'.$acknowledgeData_datails['artist_image'],

				'article_icon'=>base_url().'uploads/articles/'.$acknowledgeData_datails['article_icon'],

				'date'=>$acknowledgeData_datails['article_created'],

				'fav'=>$fav,

				'user_id'=>$acknowledgeData_datails['user_id'],

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('article_data' => $acknowledgeData1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Article not found','response' => array('message' => 'Article not found'));

	}

	$this->response($resp);

}

/*-------Fav Article list--------*/

public function fav_article_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$con['sorting'] = array("article_fav_id"=>"DESC");

	$acknowledgeData  = $this->Api_model->favarticleList();

	$acknowledgeData1  = array();

	if(!empty($acknowledgeData)){

		foreach($acknowledgeData as $acknowledgeData_datails){

			$trashData = $this->common_model->getSingleRecordById('cp_article_trash', array('user_id' => $acknowledgeData_datails['user_id'],"article_id"=>$acknowledgeData_datails['article_id']));

			if($trashData){

				continue;

			}

			$acknowledgeData1[] = array('id'=>$acknowledgeData_datails['article_fav_id'],

				'title'=>$acknowledgeData_datails['article_title'],

				'artist_name'=>$acknowledgeData_datails['artist_name'],

				'description'=>$acknowledgeData_datails['article_desc'],

				'article_image'=>base_url().'uploads/articles/'.$acknowledgeData_datails['article_image'],

				'artist_image'=>base_url().'uploads/articles/'.$acknowledgeData_datails['artist_image'],

				'date'=>$acknowledgeData_datails['article_fav_created'],

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('articlefav_data' => $acknowledgeData1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Fav Article not found','response' => array('message' => 'Fav Article not found'));

	}

	$this->response($resp);

}

/*-------Fav Article list--------*/

public function unfav_article_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('article_fav_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$restid =$this->common_model->deleteRecords('cp_article_favourite',array('article_fav_id'=>$data['article_fav_id']));

	if($restid){

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));

	}

	else {

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

/*-------Fav Article list--------*/

public function article_delete1_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('article_id','user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$post_data = array

	(

		'user_id'=> $data['user_id'],

		'article_id'=> $data['article_id'],

		'article_trash_date' => date('Y-m-d h:i:s'),

	);

	$restid = $this->common_model->addRecords('cp_article_trash', $post_data);

// $restid =$this->common_model->deleteRecords('cp_articles',array('article_id'=>$data['article_id']));

	if($restid){

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Article Delete successfully', 'response' => array('article_data' => "Article Delete  successfully"));

	}

	else {

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

/* add fav photo */

public function addFavArticle_post() {

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id','article_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$FavPhotoData1 = $this->common_model->getSingleRecordById('cp_article_favourite', array('user_id' => $data['user_id'],"article_id"=>$data['article_id']));

	if(empty($FavPhotoData1)){

		$post_data = array

		(

			'user_id'=> $data['user_id'],

			'article_id'=> $data['article_id'],

			'article_fav_created ' => date('Y-m-d h:i:s'),

			'fav' => 1,

		);

		$FavPhotoId = $this->common_model->addRecords('cp_article_favourite', $post_data);

		if($FavPhotoId) {

//$FavPhotoData = $this->common_model->getSingleRecordById('cp_photo_favourite', array('photo_fav_id' => $FavPhotoId));

			$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Favorite successfully', 'response' => array('fav_data' => "Favorite successfully"));

		}else{

			$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

		}

	}else{

		$this->db->delete('cp_article_favourite', array("user_id"=>$data['user_id'],"article_id"=>$data['article_id']));

		$FavPhotoId = $this->db->affected_rows();

		if($FavPhotoId) {

//$FavPhotoData = $this->common_model->getSingleRecordById('cp_photo_favourite', array('photo_fav_id' => $FavPhotoId));

			$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));

		}else{

			$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

		}

	}

	$this->response($resp);

}

/*-------Music list--------*/

public function music_list1_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$con['sorting'] = array("music_id"=>"DESC");

	$acknowledgeData  = $this->Api_model->musicList();

	if(!empty($acknowledgeData)){

		foreach($acknowledgeData as $acknowledgeData_datails){

			$fav  = $this->getMusicFav($acknowledgeData_datails['music_id']);

			$acknowledgeData1[] = array('id'=>$acknowledgeData_datails['music_id'],

				'title'=>$acknowledgeData_datails['music_title'],

				'desc'=>$acknowledgeData_datails['music_desc'],

				'artist'=>$acknowledgeData_datails['music_artist'],

				'music_image'=>base_url().'uploads/music/image/'.$acknowledgeData_datails['music_image'],

				'music_file'=>base_url().'uploads/music/'.$acknowledgeData_datails['music_file'],

				'fav'=>$fav,

				'type'=>$acknowledgeData_datails['music_type']

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('music_data' => $acknowledgeData1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Music not found','response' => array('message' => 'Music not found'));

	}

	$this->response($resp);

}

/*-------fav Music list--------*/

public function fav_music_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$con['sorting'] = array("music_id"=>"DESC");

	$acknowledgeData  = $this->Api_model->favMusicList($data['user_id']);

	if(!empty($acknowledgeData)){

		foreach($acknowledgeData as $acknowledgeData_datails){

			$acknowledgeData1[] = array('id'=>$acknowledgeData_datails['music_fav_id'],

				'title'=>$acknowledgeData_datails['music_title'],

				'desc'=>$acknowledgeData_datails['music_desc'],

				'artist'=>$acknowledgeData_datails['music_artist'],

				'music_id'=>$acknowledgeData_datails['music_id'],

				'music_image'=>base_url().'uploads/music/image/'.$acknowledgeData_datails['music_image'],

				'music_file'=>base_url().'uploads/music/'.$acknowledgeData_datails['music_file'],

				'user_id'=>$acknowledgeData_datails['user_id'],

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('music_data' => $acknowledgeData1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Music not found','response' => array('message' => 'Music not found'));

	}

	$this->response($resp);

}

/* add fav photo */

public function addFavMusic_post() {

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id','music_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$FavPhotoData1 = $this->common_model->getSingleRecordById('cp_music_favorite', array('user_id' => $data['user_id'],"music_id"=>$data['music_id']));

	if(empty($FavPhotoData1)){

		$post_data = array

		(

			'user_id'=> $data['user_id'],

			'music_id'=> $data['music_id'],

			'music_fav_created ' => date('Y-m-d h:i:s'),

			'fav' => 1,

		);

		$FavPhotoId = $this->common_model->addRecords('cp_music_favorite', $post_data);

		if($FavPhotoId) {

//$FavPhotoData = $this->common_model->getSingleRecordById('cp_photo_favourite', array('photo_fav_id' => $FavPhotoId));

			$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Favorite successfully', 'response' => array('fav_data' => "Favorite successfully"));

		}else{

			$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

		}

	}else{

		$this->db->delete('cp_music_favorite', array("user_id"=>$data['user_id'],"music_id"=>$data['music_id']));

		$FavPhotoId = $this->db->affected_rows();

		if($FavPhotoId) {

//$FavPhotoData = $this->common_model->getSingleRecordById('cp_photo_favourite', array('photo_fav_id' => $FavPhotoId));

			$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));

		}else{

			$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

		}

	}

	$this->response($resp);

}

public function music_delete_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('music_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$restid =$this->common_model->deleteRecords('music',array('music_id'=>$data['music_id']));

	if($restid){

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Music Delete successfully', 'response' => array('music_data' => "Music Delete  successfully"));

	}

	else {

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

/*-------Movie list--------*/

public function movie_list1_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$con['sorting'] = array("movie_id"=>"DESC");

	$acknowledgeData  = $this->Api_model->movieList();

	if(!empty($acknowledgeData)){

		foreach($acknowledgeData as $acknowledgeData_datails){

			$fav  = $this->getMovieFav($acknowledgeData_datails['movie_id']);

			$acknowledgeData1[] = array('id'=>$acknowledgeData_datails['movie_id'],

				'title'=>$acknowledgeData_datails['movie_title'],

				'desc'=>$acknowledgeData_datails['movie_desc'],

				'artist'=>$acknowledgeData_datails['movie_artist'],

				'movie_image'=>base_url().'uploads/movie/image/'.$acknowledgeData_datails['movie_image'],

				'movie_file'=>base_url().'uploads/movie/'.$acknowledgeData_datails['movie_file'],

				'type'=>$acknowledgeData_datails['movie_type'],

				'fav'=>$fav

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('movie_data' => $acknowledgeData1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Music not found','response' => array('message' => 'Movie not found'));

	}

	$this->response($resp);

}

public function unFavMulRestaurant_post()

{

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('fav_rest_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$ids = explode(',',$data['fav_rest_id']);

	$emailid =$this->common_model->deleteRest($ids);

	if($emailid) {

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('fav_data' => "Unfavorite successfully"));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Email not found','response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

/* add fav photo */

public function addFavMovie_post() {

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id','movie_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$FavPhotoData1 = $this->common_model->getSingleRecordById('cp_movie_favorite', array('user_id' => $data['user_id'],"movie_id"=>$data['movie_id']));

	if(empty($FavPhotoData1)){

		$post_data = array

		(

			'user_id'=> $data['user_id'],

			'movie_id'=> $data['movie_id'],

			'movie_fav_created' => date('Y-m-d h:i:s'),

			'fav' => 1,

		);

		$FavPhotoId = $this->common_model->addRecords('cp_movie_favorite', $post_data);

		if($FavPhotoId) {

//$FavPhotoData = $this->common_model->getSingleRecordById('cp_photo_favourite', array('photo_fav_id' => $FavPhotoId));

			$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Favorite successfully', 'response' => array('fav_data' => "Favorite successfully"));

		}else{

			$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

		}

	}else{

		$this->db->delete('cp_movie_favorite', array("user_id"=>$data['user_id'],"movie_id"=>$data['movie_id']));

		$FavPhotoId = $this->db->affected_rows();

		if($FavPhotoId) {

//$FavPhotoData = $this->common_model->getSingleRecordById('cp_photo_favourite', array('photo_fav_id' => $FavPhotoId));

			$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));

		}else{

			$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

		}

	}

	$this->response($resp);

}

/*-------fav Music list--------*/

public function fav_movie_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$pdata = file_get_contents("php://input");

	$data = json_decode($pdata, true);

	$object_info = $data;

	$con['sorting'] = array("movie_id"=>"DESC");

	$acknowledgeData  = $this->Api_model->favMovieList($data['user_id']);

	if(!empty($acknowledgeData)){

		foreach($acknowledgeData as $acknowledgeData_datails){

			$acknowledgeData1[] = array('id'=>$acknowledgeData_datails['movie_fav_id'],

				'user_id'=>$acknowledgeData_datails['user_id'],

				'title'=>$acknowledgeData_datails['movie_title'],

				'desc'=>$acknowledgeData_datails['movie_desc'],

				'artist'=>$acknowledgeData_datails['movie_artist'],

				'movie_id'=>$acknowledgeData_datails['movie_id'],

				'movie_image'=>base_url().'uploads/movie/image/'.$acknowledgeData_datails['movie_image'],

				'movie_file'=>base_url().'uploads/music/'.$acknowledgeData_datails['movie_file'],

				'fav'=>1,

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('movie_data' => $acknowledgeData1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Movie not found','response' => array('message' => 'Movie not found'));

	}

	$this->response($resp);

}

/*-------Movie list--------*/

public function show_caregiver_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$acknowledgeData  = $this->Api_model->caregiverList();

	if(!empty($acknowledgeData)){

		foreach($acknowledgeData as $acknowledgeData_datails){

			$acknowledgeData1[] = array('id'=>$acknowledgeData_datails['id'],

				'username'=>$acknowledgeData_datails['username'],

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('caregiver_data' => $acknowledgeData1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Caregiver  not found','response' => array('message' => 'Caregiver not found'));

	}

	$this->response($resp);

}

/*-------caregiver list with self field--------*/

public function show_caregiver_self_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	if(!empty($object_info['user_id'])){

		$user_id = $object_info['user_id'];

	}

	$acknowledgeData  = $this->Api_model->caregiverList();

	if(!empty($acknowledgeData)){

		foreach($acknowledgeData as $acknowledgeData_datails){

			$acknowledgeData1[] = array('id'=>$acknowledgeData_datails['id'],

				'username'=>$acknowledgeData_datails['username'],

			);

			$test_self = array('id'=>$user_id,'username'=>'Self');

			$merged_array = array_merge($acknowledgeData1, array($test_self));

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('caregiver_data' => $merged_array));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Caregiver  not found','response' => array('message' => 'Caregiver not found'));

	}

	$this->response($resp);

}

public function unFavMulArticle_post()

{

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('fav_article_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$ids = explode(',',$data['fav_article_id']);

	$emailid =$this->common_model->deleteArticle($ids);

	if($emailid) {

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('fav_data' => "Unfavorite successfully"));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Email not found','response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function send_test_report_post() {

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);



	

	//$data = $this->param;

	//$object_info = $data;



	$data = $_POST;





	//echo "<pre>" ;print_r($data); die;



	$object_info = $data;

	// print_r($object_info);

	// die;

	$required_parameter = array('test_id','description','user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));



		$this->response($resp);

	}



	

	// echo "<pre>";print_r($_FILES);

	// die;







	if(!empty($_FILES['image']['name'])) {

		$random = $this->generateRandomString(10);

		$ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);

		$file_name = $random.".".$ext;

		$target_dir = "uploads/test_report/";

		$target_file = $target_dir .$file_name;

		if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)){

			$photoFile = $file_name;

		}

		if($photoFile){

			$image = $photoFile;

		}else{

			$image = '';

		}

		$test_report_data['image'] = $image;

		$condition = array('id' => $data['test_id']);

		$Id = $this->common_model->updateRecords('cp_test_report', $test_report_data, $condition);

	}







	$testReportFile = $this->common_model->getsingle("cp_test_report",array("id" => $data['test_id']));

	



	$this->db->select('image');

	$this->db->from('cp_test_report');

	$this->db->where('id',$data['test_id']);

	$query = $this->db->get();

	$result = $query->result_array();





	// echo "<pre>";

	// print_r($testReportFile);

	// echo "<br>";

	// echo "<pre>";

	// print_r($result);







	// die;

	//$result_details['image']="yW2q9AoyVZ.jpg";



	foreach($result as $result_details){

		$imageArray[]  = base_url().'uploads/test_report/'.$result_details['image'];

	}



	$caregiverData = $this->common_model->getsingle("cp_users",array("email" => $data['caregiver_id'],'user_role' => 4));

	

	//echo"<pre>"; print_r($data); die;



	//if(empty($caregiverData)){



	if(!$this->isValidEmail($data['caregiver_id'])){

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'input must have valid email in the text', 'response' => array('message' => 'input must have valid email in the text'));

		$this->response($resp);





		// $resp = array('status_code' => FAILURE,'status' => 'false', 'message' => "The inserted Email is not associated with any caregiver's account",'response' => array('message' => "The inserted Email is not associated with any caregiver's account"));



		// $this->response($resp);



	}else{



		$post_data = array

		(

			'test_id'=> $data['test_id'],

			'caregiver_id' => (!empty($data['caregiver_id']) ? $data['caregiver_id'] : ''),

			'doc_id' => (!empty($data['doc_id']) ? $data['doc_id'] : ''),

			'description'=> $data['description'],

			'user_id'=> $data['user_id'],

			'send_test_report_created' => date('Y-m-d H:i:s')

		);

		$currentDateTime = $post_data['send_test_report_created'];

		$newDateTime = date('d-m-y, h:i A', strtotime($currentDateTime));

		$emailId = $this->common_model->addRecords('send_test_report', $post_data);



	}

	// echo $emailId;

	// die;



	if($emailId) {



		$testType = $this->common_model->getsingle("cp_test_report",array("id" => $data['test_id']));

		// echo $this->db->last_query();

		// die;

		// print_r($testType);

		// die;



		$username = $this->common_model->getsingle("cp_users",array("id" => $data['user_id']));

		$subject = "Send test report";







// 		if($data['doc_id']){



// 			$doctormail = $this->common_model->getsingle("cp_doctor",array("doctor_id" => $data['doc_id']));



// 			$message ="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

// 			<html xmlns='http://www.w3.org/1999/xhtml'>

// 			<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>

// 			<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>

// 			</head>

// 			<body style='margin: 0;font-family:'open sans';'>

// 			<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;'>

// 			<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>

// 			<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin: 0 auto 0px;'>

// 			<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

// 			</a>

// 			</div>

// 			<div style='border:none; padding:0px 60px; max-width:580px; margin:-40px auto 0px; border-radius:0px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>

// 			<div class='temp_cont' style='width:100%; margin:0 auto;'>

// 			<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>

// 			<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

// 			<span style='display:block;width:100%;'>

// 			<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>

// 			</span>

// 			Test report

// 			</h2>

// 			<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

// 			Hi &nbsp;".$username->username."</h3>

// 			<p style='text-align: center; font-size: 14px; margin-top: 0px; padding:0px 15px;' >Patient Name has sent you a document to view, please click on attached file to view the document.</p>

// 			<style>

// 						#customers td, #customers th {

// 			border:1px solid #ddd;

// 			padding:8px;

// 		}

// 						#customers th {

// 		padding-top:8px;

// 		padding-bottom:8px;

// 		text-align:left;

// 		background-color:#4CAF50;

// 		color:white;

// 	}

// 	</style>

// 	<table id='customers' style='width:100%; border-collapse: collapse;'>

// 	<tr>

// 	<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Doctor Name:</th>

// 	<td style='padding:8px;border:1px solid #ddd;'>".$doctormail->doctor_name."</td>

// 	</tr>						  

// 	<tr>

// 	<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Date and Time:</th>

// 	<td style='padding:8px;border:1px solid #ddd;'>".$newDateTime."</td>

// 	</tr>

// 	<tr>

// 	<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Description:</th>

// 	<td style='font-size:13px; padding:8px;border:1px solid #ddd;'>".$data['description']."</td>

// 	</tr>

// 	</table>

// 	</div>

// 	<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%; box-sizing:border-box;'>

// 	<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>

// 	<p style='font-size: 13px;margin:0;'>

// 	<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>

// 	</p>						

// 	<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>

// 	<p style='font-size: 13px;margin:0 0 8px;'>Care Pro</p>

// 	<p style='font-size: 13px;margin:0;'>

// 	<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>EMAIL: info@carepro.com/</a>

// 	</p>

// 	</div>

// 	</div>

// 	</div>

// 	</div>

// 	</body>

// 	</html>";

// //$chk_mail1 = sendEmail1($doctormail->doctor_email,$subject,$message,$imageArray);

// 	$chk_mail1= new_send_mail($message, $subject, $doctormail->doctor_email,$imageArray);



// }





		if(!empty($testType->test_report_title)){

			$Type = $testType->test_report_title;

		}else{

			$Type ="N.A.";

		}









		if($data['caregiver_id']){

			// echo $emailId;

			// die;

			//$caregiver_data = $this->common_model->getsingle("cp_users",array("id" => $caregiverData->id));

			$message ="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

			<html xmlns='http://www.w3.org/1999/xhtml'>

			<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>

			<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>

			</head>

			<body style='margin: 0;font-family:'open sans';'>

			<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;'>

			<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>

			<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin: 0 auto 0px;'>

			<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>

			</a>

			</div>

			<div style='border:none; padding:0px 60px; max-width:580px; margin:-40px auto 0px; border-radius:0px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>

			<div class='temp_cont' style='width:100%; margin:0 auto;'>

			<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>

			<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

			<span style='display:block;width:100%;'>

			<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>

			</span>

			Test report

			</h2>

			<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>

			Hi &nbsp;".$username->username.",

			</h3>

			<p style='text-align: center; font-size: 14px; margin-top: 0px; padding:0px 15px;' >".$username->username." has sent you a document to view, please click on attached file to view the document.</p>

			<style>

						#customers td, #customers th {

			border:1px solid #ddd;

			padding:8px;

		}

						#customers th {

		padding-top:8px;

		padding-bottom:8px;

		text-align:left;

		background-color:#4CAF50;

		color:white;

	}

	</style>

	<table id='customers' style='width:100%; border-collapse: collapse;'>

	<tr>

	<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Caregiver Email:</th>

	<td style='padding:8px;border:1px solid #ddd;'>".$data['caregiver_id']."</td>

	</tr>



	<tr>

	<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Test Type:</th>

	<td style='padding:8px;border:1px solid #ddd;'>".$Type."</td>

	</tr>						  

	<tr>

	<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Date and Time:</th>

	<td style='padding:8px;border:1px solid #ddd;'>".$newDateTime."</td>

	</tr>

	<tr>

	<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Description:</th>

	<td style='font-size:13px; padding:8px;border:1px solid #ddd;'>".$data['description']."</td>

	</tr>

	</table>

	</div>

	<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%; box-sizing:border-box;'>

	<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>

	<p style='font-size: 13px;margin:0;'>

	<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>

	</p>						

	<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>

	<p style='font-size: 13px;margin:0 0 8px;'>Care Pro</p>

	<p style='font-size: 13px;margin:0;'>

	<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>EMAIL: info@carepro.com/</a>

	</p>

	</div>

	</div>

	</div>

	</div>

	</body>

	</html>";  

			// $message =

			// "<table>

			// <tr><th>Caregiver:</th>

			// <td>".$caregiver_data->username."</td></tr>

			// <tr><th>Username:</th>

			// <td>".$username->username."</td></tr>

			// <tr><th>Description:</th>

			// <td>".$data['description']."</td></tr>

			// </table>";

// $chk_mail2 = sendEmail1($caregiver_data->email,$subject,$message,$imageArray);

	$chk_mail2= new_send_mail($message, $subject, $data['caregiver_id'],$imageArray);

// echo $chk_mail2;

// die;

}

$emailData = $this->common_model->getSingleRecordById('send_test_report', array('send_test_report_id' => $emailId));

// echo "<pre>";

// print_r($emailData);

// die;

$orderData = array(

	'test_id' => $emailData['test_id'],

	'description' => $emailData['description'],

);







$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Send Test Report successfully', 'response' => $orderData);

} else { 

	$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

}

$this->response($resp);

}









public function add_pickup_medicine_post()

{

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('medicine_id','user_id','caregiver_id','pharmacy');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$post_data = array

	(

// 'doctor_id'=> $data['doctor_id'],

		'medicine_id'=> $data['medicine_id'],

		'user_id' => $data['user_id'],

		'pharmacy' => $data['pharmacy'],

		'caregiver_id'=> $data['caregiver_id'],

		'create_date' => date('Y-m-d H:i:s')

	);

	$emailId = $this->common_model->addRecords('picup_medicine', $post_data);

	if($emailId)

	{

		$caregiver_data = $this->common_model->getsingle("cp_users",array("id" => $data['caregiver_id']));

		$doctormail = $this->common_model->getsingle("cp_doctor",array("doctor_id" => $data['doctor_id']));

		$username = $this->common_model->getsingle("cp_users",array("id" => $data['user_id']));

		$medicine = $this->common_model->getsingle("medicine_schedule",array("medicine_id" => $data['medicine_id']));

		$subject = "Pickup Medicine";

		$message =

		"<table>

		<tr><th>Username:</th>

		<td>".$caregiver_data->username."</td></tr>

		<tr><th>Medicine:</th>

		<td>".$medicine->medicine_name."</td></tr>

		</table>";

//$chk_mail1= new_send_mail($message, $subject, $caregiver_data->email,'');

		$from_m = "info@carepro.com";

		$fromname = "Carepro";

		$chk_mail=  socialEmail($caregiver_data->email,$from_m,$fromname,$subject,$message);

		$orderData = array(

			'caregiver_name' => $caregiver_data->username,

//  'doctor_name' => $doctormail->doctor_name,

			'medicine_name' => $medicine->medicine_name

		);

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Pickup medicine inserted successfully', 'response' => $orderData);

	}else

	{

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

/*-------show pick up medicine--------*/

public function pickup_medicine_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$con['sorting'] = array("id"=>"DESC");

	$acknowledgeData  = $this->Api_model->pickupList();

	if(!empty($acknowledgeData)){

		foreach($acknowledgeData as $acknowledgeData_datails){

			$userData = $this->common_model->getsingle("cp_users",array("id" => $acknowledgeData_datails['caregiver_id']));

			$pharma_company = $this->common_model->getsingle("pharma_company",array("id" => $acknowledgeData_datails['pharmacy']));

			$acknowledgeData1[] = array('id'=>$acknowledgeData_datails['id'],

				'title'=>$acknowledgeData_datails['user_id'],

				'medicine_name'=>$acknowledgeData_datails['medicine_name'],

				'user_id' => $userData->id,

				'username' => $userData->username,

				'medicine_id' => $acknowledgeData_datails['medicine_id'],

				'pharmacy' => $pharma_company->name,

				'pharmacy_id' => $acknowledgeData_datails['pharmacy'],

// 'doctor_name'=>$acknowledgeData_datails['doctor_name'],

				'date'=>$acknowledgeData_datails['create_date'],

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('pickupList' => $acknowledgeData1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'pickup medicine not found','response' => array('message' => 'pickup medicine not found'));

	}

	$this->response($resp);

}

/*-------Delete pick up medicine--------*/

public function delete_pickup_medicine_post()

{

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('pickup_medicine_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$ids = explode(',',$data['pickup_medicine_id']);

	$emailid =$this->common_model->deletepickupmedicine($ids);

	if($emailid) {

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('pickup_medicine_data' => "Pickup medicine deleted successfully"));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'pickup medicine not found','response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

/*-------Update pick up medicine--------*/

public function update_pickup_medicine_post()

{

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array("medicine_id",'caregiver_id','user_id','pickup_medicine_id','pharmacy');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$post_data = array

	(

// 'doctor_id'=> $data['doctor_id'],

		'medicine_id'=> $data['medicine_id'],

		'pharmacy' => $data['pharmacy'],

		'caregiver_id'=> $data['caregiver_id'],

		'user_id'=> $data['user_id'],

	);

	$acknowledgeId = $this->common_model->updateRecords('picup_medicine', $post_data,array('id' => $data['pickup_medicine_id']));

	if($acknowledgeId)

	{

		$caregiver_data = $this->common_model->getsingle("cp_users",array("id" => $data['caregiver_id']));

		$username = $this->common_model->getsingle("cp_users",array("id" => $data['user_id']));

		$medicine = $this->common_model->getsingle("medicine_schedule",array("medicine_id" => $data['medicine_id']));

		$subject = "Pickup Medicine";

		$message =

		"<table>

		<tr><th>Username:</th>

		<td>".$caregiver_data->username."</td></tr>

		<tr><th>Medicine:</th>

		<td>".$medicine->medicine_name."</td></tr>

		</table>";

//$chk_mail1= new_send_mail($message, $subject, $caregiver_data->email,'');

		$from_m = "info@carepro.com";

		$fromname = "Carepro";

		$chk_mail=  socialEmail($caregiver_data->email,$from_m,$fromname,$subject,$message);

		$acknowledgeData = $this->common_model->getSingleRecordById('picup_medicine', array('id' => $data['pickup_medicine_id']));

		$users1 = array(

			'id' => $acknowledgeData['id'],

//'doctor_id' => $acknowledgeData['doctor_id'],

			'medicine_id' => $acknowledgeData['medicine_id'],

			'caregiver_id' => $acknowledgeData['caregiver_id'],

			'user_id' => $acknowledgeData['user_id'],

		);

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Pickup medicine update successfully', 'response' => $users1);

	}

	else

	{

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

/*-------show contact list--------*/

public function user_contact_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$con['sorting'] = array("contact_id"=>"DESC");

	$contactData  = $this->Api_model->contactList($data['user_id']);

	if(!empty($contactData)){

		foreach($contactData as $contactData_datails){

			$acknowledgeData1[] = array('id'=>$contactData_datails['contact_id'],

				'contact_name'=>$contactData_datails['contact_name'],

				'mobile_number'=>$contactData_datails['contact_mobile_number'],

				'user_id'=>$contactData_datails['contact_user_id'],

//'contact_image'=>$contactData_datails['user_contact_image'],

				'contact_image'=>base_url().'uploads/contact/'.$contactData_datails['user_contact_image'],

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('contactList' => $acknowledgeData1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Contact not found','response' => array('message' => 'Contact not found'));

	}

	$this->response($resp);

}

/*-------Delete user contact--------*/

public function delete_user_contact_post()

{

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_contact_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$ids = explode(',',$data['user_contact_id']);

	$contactid =$this->common_model->deleteContact($ids);

	if($contactid) {

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('pickup_medicine_data' => "contact data deleted succesfully"));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'contact not found','response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

/*-------show contact list--------*/

public function family_contact_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$con['sorting'] = array("contact_id"=>"DESC");

	$contactData  = $this->Api_model->contactList($data['user_id']);

	if(!empty($contactData)){

		foreach($contactData as $contactData_datails){

			$acknowledgeData1[] = array('id'=>$contactData_datails['contact_id'],

				'contact_name'=>$contactData_datails['contact_name'],

				'mobile_number'=>$contactData_datails['contact_mobile_number'],

				'user_id'=>$contactData_datails['contact_user_id'],

//'contact_image'=>$contactData_datails['user_contact_image'],

				'contact_image'=>base_url().'uploads/contact/'.$contactData_datails['user_contact_image'],

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('contactList' => $acknowledgeData1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Contact not found','response' => array('message' => 'Contact not found'));

	}

	$this->response($resp);

}

/*-------Delete  family contact--------*/

public function delete_family_contact_post()

{

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_contact_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$ids = explode(',',$data['user_contact_id']);

	$contactid =$this->common_model->deleteContact($ids);

	if($contactid) {

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('pickup_medicine_data' => "contact data deleted succesfully"));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'contact not found','response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function get_facebook_url_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('facebook_url' => "https://www.facebook.com/"));

	$this->response($resp);

}

public function get_twitter_url_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('twitter_url' => "https://twitter.com/"));

	$this->response($resp);

}

// Add Restaurant data

public function add_grocery_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id',"name","image","lat","long","address","place_id","rating",'icon');

	$chk_error = check_required_value($required_parameter, $data['rests'][0]);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	for($i=0;$i<count($data['rests']);$i++){

		$place_id = $this->common_model->getSingleRecordById('cp_grocery', array('place_id' => $data['rests'][$i]['place_id'],"user_id"=>$data['rests'][$i]['user_id']));

		if($place_id){

			continue;

		}

		$post_data = array(

			'user_id' => (!empty($data['rests'][$i]['user_id']) ? $data['rests'][$i]['user_id'] : ''),

			'grocery_name' => (!empty($data['rests'][$i]['name']) ? $data['rests'][$i]['name'] : ''),

			'grocery_image' => (!empty($data['rests'][$i]['image']) ? $data['rests'][$i]['image'] : ''),

			'grocery_lat' => (!empty($data['rests'][$i]['lat']) ? $data['rests'][$i]['lat'] : ''),

			'grocery_long' => (!empty($data['rests'][$i]['long']) ? $data['rests'][$i]['long'] : ''),

			'place_id' => (!empty($data['rests'][$i]['place_id']) ? $data['rests'][$i]['place_id'] : ''),

			'grocery_address' => (!empty($data['rests'][$i]['rating']) ? $data['rests'][$i]['rating'] : ''),

			'grocery_icon' => (!empty($data['rests'][$i]['icon']) ? $data['rests'][$i]['icon'] : ''),

			'grocery_address' => (!empty($data['rests'][$i]['address']) ? $data['rests'][$i]['address'] : ''),

			'grocery_created' => date('Y-m-d H:i:s'),

			'grocery_rating'=>(!empty($data['rests'][$i]['rating']) ? $data['rests'][$i]['rating'] : '')

		);

		$restaurantId = $this->common_model->addRecords('cp_grocery', $post_data);

	}

	$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Add Grocery successfully', 'response' => array('message'=>'Add Grocery successfully'));

	$this->response($resp);

}

// list restaurant data

public function grocery_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$limit = 10;

	if($data['page_no']==''){

		$data['page_no']=1;

	}

	$start  = ($data['page_no']-1)*$limit;

	$con1['returnType'] = 'count';

	$table = "cp_grocery";

	$con1['conditions'] = array("user_id"=>$data['user_id']);

	$category_count  = $this->Api_model->getRows($table,$con1);

	$pages = ceil(count($category_count) / $limit);

	$con['sorting'] = array("grocery_id"=>"DESC");

	$con['limit'] = $limit;

	$con['start'] = $start;

	$con['conditions'] = array("user_id"=>$data['user_id']);

	$restaurantDatas  = $this->Api_model->getRows($table,$con);

	if(!empty($restaurantDatas)){

		foreach($restaurantDatas as $restaurantData_details){

			$fav  = $this->getGroceryFav($restaurantData_details['grocery_id']);

			$restaurantData[] = array('id'=>$restaurantData_details['grocery_id'],

				'name'=>$restaurantData_details['grocery_name'],

				'image'=>$restaurantData_details['grocery_image'],

				'address'=>$restaurantData_details['grocery_address'],

				'user_id'=>$restaurantData_details['user_id'],

				'lat' => $restaurantData_details['grocery_lat'],

				'long' => $restaurantData_details['grocery_long'],

				'rating' => $restaurantData_details['grocery_rating'],

				'fav'=>$fav

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('grocery_data' => $restaurantData));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Grocery not found','response' => array('message' => 'Restaurant not found'));

	}

	$this->response($resp);

}

public function add_fav_grocery_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('grocery_name','grocery_address','grocery_image','rating','place_id','user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

//$address  =  $this->get_address($data['lat'],$data['long']);

	$post_data = array

	(

		'user_id'=> $data['user_id'],

		'fav_grocery_name' => $data['grocery_name'],

		'fav_grocery_image' => $data['grocery_image'],

		'fav_grocery_address' => $data['grocery_address'],	'fav_grocery_rating' => $data['rating'],

		'fav_grocery_place_id'=> $data['place_id'],

		'fav_grocery_created' => date('Y-m-d H:i:s'),

	);

	$restaurantId = $this->common_model->addRecords('cp_fav_grocery', $post_data);

	if($restaurantId){

		$restaurantData = $this->common_model->getSingleRecordById('cp_fav_grocery', array('fav_grocery_id' => $restaurantId));

		$restaurantData1 = array(

			'id' => $restaurantData['fav_grocery_id'],

		);

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Favorite successfully', 'response' => array('fav_data' => "favorite successfully"));

	}

	else

	{

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function unfav_grocery_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('place_id','user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$restid =$this->common_model->deleteRecords('cp_fav_grocery',array('fav_grocery_place_id'=>$data['place_id'],'user_id' => $data['user_id']));

	if($restid){

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));

	}

	else {

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function fav_grocery_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$limit = 10;

	if($data['page_no']==''){

		$data['page_no']=1;

	}

	$start  = ($data['page_no']-1)*$limit;

	$con1['returnType'] = 'count';

	$table = "cp_fav_grocery";

	$con1['conditions'] = array("cp_fav_grocery.user_id"=>$data['user_id']);

	$bank_count  = $this->Api_model->getRowsGrocerynew($table,$con1);

	$pages = ceil(count($bank_count) / $limit);

	$con['sorting'] = array("cp_fav_grocery.fav_grocery_id"=>"DESC");

	$con['limit'] = $limit;

	$con['start'] = $start;

	$con['conditions'] = array("cp_fav_grocery.user_id"=>$data['user_id']);

	$bankDatas  = $this->Api_model->getRowsGrocerynew($table,$con);

//echo '<pre>';print_r($bankDatas);die;

// echo $this->db->last_query();die;

	if(!empty($bankDatas)){

		foreach($bankDatas as $bankDatas_details){

			$bankData[] = array('id'=>$bankDatas_details['fav_grocery_id'],

				'name'=>$bankDatas_details['fav_grocery_name'],

				'image'=>$bankDatas_details['fav_grocery_image'],

				'address'=>$bankDatas_details['fav_grocery_address'],

				'user_id'=>$bankDatas_details['user_id'],

				'rating'=>$bankDatas_details['fav_grocery_rating'],

				'place_id'=>$bankDatas_details['fav_grocery_place_id'],

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('grocery_data' => $bankData));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Fav Grocery not found','response' => array('message' => 'Fav Grocery not found'));

	}

	$this->response($resp);

}

/*-------Pharma list--------*/

public function note_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

// $post_data = array

// (

// 	'user_id'=> $data['user_id'],

// );

	$con['sorting'] = array("note_id"=>"DESC");

	if($data['read_status'] == '0'){

		$user_id = $data['user_id'];

		$acknowledgeData  = $this->common_model->getAllordynamic($data['read_status'],$user_id);

	}else{

		$user_id = $data['user_id'];

		$acknowledgeData  = $this->common_model->getAllornew("cp_note",$user_id);

	}

// $acknowledgeData  = $this->Api_model->noteList();

	if(!empty($acknowledgeData)){

		foreach($acknowledgeData as $acknowledgeData_datails){

			$acknowledgeData1[] = array('id'=>$acknowledgeData_datails['note_id'],

				'from_caregiver'=>$this->getGiver($acknowledgeData_datails['from_caregiver']),

				'to_caregiver'=>$this->getGiver($acknowledgeData_datails['to_caregiver']),

				'description'=>$acknowledgeData_datails['description'],

				'read_status'=>$acknowledgeData_datails['read_status'],

				'date'=>$acknowledgeData_datails['create_date'],

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('note_data' => $acknowledgeData1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Note Data not found','response' => array('message' => 'Note Data not found'));

	}

	$this->response($resp);

}

function getGiver($user_id){

	$this->db->select('*');

	$this->db->from('cp_users');

	$this->db->where('id',$user_id);

	$query = $this->db->get();

	$result = $query->result_array();

	return $result[0]['username'];

}

public function add_note_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('from_caregiver',"to_caregiver","description","user_id");

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$post_data = array

	(

		'from_caregiver'=> $data['from_caregiver'],

		'to_caregiver'=> $data['to_caregiver'],

		'user_id'=> $data['user_id'],

		'description'=> $data['description'],

		'create_date' => date('Y-m-d h:i:s'),

	);

	$drAppId = $this->common_model->addRecords('cp_note', $post_data);

	if($drAppId){

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Note Added successfully', 'response' => array('read_status' => 0));

	}

	else

	{

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function update_note_post()

{

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('from_caregiver',"to_caregiver","description","note_id",'read_status');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$post_data = array

	(

		'from_caregiver'=> $data['from_caregiver'],

		'to_caregiver'=> $data['to_caregiver'],

		'user_id'=> $data['user_id'],

		'description'=> $data['description'],

		'note_id'=> $data['note_id'],

		'read_status' => $data['read_status'],

		'create_date' => date('Y-m-d h:i:s'),

	);

	$acknowledgeId = $this->common_model->updateRecords('cp_note', $post_data,array('note_id' => $data['note_id']));

	if($acknowledgeId)

	{

		$getdata = $this->common_model->getsingle("cp_note",array('note_id' => $data['note_id']));

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Note update successfully', 'response' => array("read_status"=>$getdata->read_status));

	}

	else

	{

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function delete_note_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('note_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$restid =$this->common_model->deleteRecords('cp_note',array('note_id'=>$data['note_id']));

	if($restid){

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Note deleted successfully', 'response' => array('note_data' => "Note deleted successfully"));

	}

	else {

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function add_website_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id',"name","image","lat","long","address","place_id","rating",'icon');

	$chk_error = check_required_value($required_parameter, $data['rests'][0]);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	for($i=0;$i<count($data['rests']);$i++){

		$place_id = $this->common_model->getSingleRecordById('cp_website', array('place_id' => $data['rests'][$i]['place_id'],"user_id"=>$data['rests'][$i]['user_id']));

		if($place_id){

			continue;

		}

		$post_data = array(

			'user_id' => (!empty($data['rests'][$i]['user_id']) ? $data['rests'][$i]['user_id'] : ''),

			'website_name' => (!empty($data['rests'][$i]['name']) ? $data['rests'][$i]['name'] : ''),

			'website_image' => (!empty($data['rests'][$i]['image']) ? $data['rests'][$i]['image'] : ''),

			'website_lat' => (!empty($data['rests'][$i]['lat']) ? $data['rests'][$i]['lat'] : ''),

			'website_long' => (!empty($data['rests'][$i]['long']) ? $data['rests'][$i]['long'] : ''),

			'place_id' => (!empty($data['rests'][$i]['place_id']) ? $data['rests'][$i]['place_id'] : ''),

			'website_icon' => (!empty($data['rests'][$i]['icon']) ? $data['rests'][$i]['icon'] : ''),

			'website_address' => (!empty($data['rests'][$i]['address']) ? $data['rests'][$i]['address'] : ''),

			'website_created' => date('Y-m-d H:i:s'),

			'website_rating'=>(!empty($data['rests'][$i]['rating']) ? $data['rests'][$i]['rating'] : '')

		);

		$restaurantId = $this->common_model->addRecords('cp_website', $post_data);

	}

	$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Add Website successfully', 'response' => array('message'=>'Add Website successfully'));

	$this->response($resp);

}

public function website_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$limit = 10;

	if($data['page_no']==''){

		$data['page_no']=1;

	}

	$start  = ($data['page_no']-1)*$limit;

	$con1['returnType'] = 'count';

	$table = "cp_website";

	$con1['conditions'] = array("user_id"=>$data['user_id']);

	$bank_count  = $this->Api_model->getRows($table,$con1);

	$pages = ceil(count($bank_count) / $limit);

	$con['sorting'] = array("website_id"=>"DESC");

	$con['limit'] = $limit;

	$con['start'] = $start;

	$con['conditions'] = array("user_id"=>$data['user_id']);

	$bankDatas  = $this->Api_model->getRows($table,$con);

	if(!empty($bankDatas)){

		foreach($bankDatas as $bankDatas_details){

//$fav  = $this->getBankFav($bankDatas_details['bank_id']);

			$bankData[] = array('id'=>$bankDatas_details['website_id'],

				'name'=>$bankDatas_details['website_name'],

				'image'=>$bankDatas_details['website_image'],

				'address'=>$bankDatas_details['website_address'],

				'user_id'=>$bankDatas_details['user_id'],

				'rating'=>$bankDatas_details['website_rating'],

//'fav'=>$fav,

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('website_data' => $bankData));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Bank not found','response' => array('message' => 'Bank not found'));

	}

	$this->response($resp);

}

public function add_fav_website_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('website_id','user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

//$address  =  $this->get_address($data['lat'],$data['long']);

	$post_data = array

	(

		'user_id'=> $data['user_id'],

		'website_id'=> $data['website_id'],

		'fav_website_created' => date('Y-m-d H:i:s'),

	);

	$restaurantId = $this->common_model->addRecords('cp_fav_website', $post_data);

	if($restaurantId){

		$restaurantData = $this->common_model->getSingleRecordById('cp_fav_website', array('fav_website_id' => $restaurantId));

		$restaurantData1 = array(

			'id' => $restaurantData['fav_website_id'],

		);

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Favorite successfully', 'response' => array('fav_data' => "favorite successfully"));

	}

	else

	{

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

public function unfav_website_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('website_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$restid =$this->common_model->deleteRecords('cp_fav_website',array('website_id'=>$data['website_id']));

	if($restid){

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));

	}

	else {

		$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

// all music list

public function music_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$limit = 10;

// get banner data

	$bannerCon['sorting'] = array("music_banner_id"=>"DESC");

	$bannerCon['conditions'] = array("music_banner_status"=>1);

	$bannerCon['limit'] = $limit;

	$banner_data  = $this->Api_model->getRows('cp_music_banner',$bannerCon);

	$bannerTotalCon['conditions'] = array("music_banner_status"=>1);

	$banner_Total_data  = $this->Api_model->getRows('cp_music_banner',$bannerTotalCon);

	if(empty($banner_data)){

		$musicList['Banners'] = array();}else{

			$musicList['Banners']['bannerCount'] = count($banner_Total_data);

			foreach ( $banner_data as $row_banner_data ){

				$musicList['Banners']['bannerList'][] = array(

					'id'=>$row_banner_data['music_banner_id'],

					'music_banner_path'=>base_url().'uploads/music/banners/'.$row_banner_data['music_banner_image'],

					'music_banner_file'=>base_url().'uploads/music/banners/files/'.$row_banner_data['music_banner_file']

				);

			}

		}

// get popular music data

		$popularCon['sorting'] = array("music_view"=>"DESC");

		$popularCon['limit'] = $limit;

		$popular_music_data  = $this->Api_model->getRows('music',$popularCon);

		$popular_Total_data  = $this->Api_model->getRows('music');

		if(empty($popular_music_data)){

			$musicList['Populars']['popularCount'] = "0";$musicList['Populars']['popularList']=array();

		}else{

			$musicList['Populars']['popularCount'] = count($popular_Total_data);

			foreach ( $popular_music_data as $row_popular_data ){

				$popularFav  = $this->getMusicUserFav($row_popular_data['music_id'],$data['user_id']);

				$popularCategoryName  = $this->getCategoryName($row_popular_data['category_id']);

				$musicList['Populars']['popularList'][] = array(

					'music_id'=>$row_popular_data['music_id'],

					'title'=>$row_popular_data['music_title'],

					'desc'=>$row_popular_data['music_desc'],

					'artist'=>$row_popular_data['music_artist'],

					'music_image'=>base_url().'uploads/music/image/'.$row_popular_data['music_image'],

					'music_file'=>base_url().'uploads/music/'.$row_popular_data['music_file'],

					'fav'=>$popularFav,

					'category_name'=>$popularCategoryName,

					'user_id'=>$row_popular_data['user_id'],

				);

			}

		}

// get recent music data

		$recentCon['sorting'] = array("music_id"=>"DESC");

		$recentCon['limit'] = $limit;

		$recent_music_data  = $this->Api_model->getRows('music',$recentCon);

		$recent_Total_data  = $this->Api_model->getRows('music');

		if(empty($recent_music_data)){

			$musicList['Recents']['recentCount'] = "0";$musicList['Recents']['recentList']=array();

		}else{

			$musicList['Recents']['recentCount'] = count($recent_Total_data);

			foreach ( $recent_music_data as $row_recent_data ){

				$recentFav  = $this->getMusicUserFav($row_recent_data['music_id'],$data['user_id']);

				$recentCategoryName  = $this->getCategoryName($row_recent_data['category_id']);

				$musicList['Recents']['recentList'][] = array(

					'music_id'=>$row_recent_data['music_id'],

					'title'=>$row_recent_data['music_title'],

					'desc'=>$row_recent_data['music_desc'],

					'artist'=>$row_recent_data['music_artist'],

					'music_image'=>base_url().'uploads/music/image/'.$row_recent_data['music_image'],

					'music_file'=>base_url().'uploads/music/'.$row_recent_data['music_file'],

					'fav'=>$recentFav,

					'category_name'=>$recentCategoryName,

					'user_id'=>$row_recent_data['user_id'],

				);

			}

		}

// get category data

		$categoryCon['sorting'] = array("music_category_id"=>"DESC");

		$categoryCon['limit'] = $limit;

		$categoryCon['conditions'] = array("music_category_status"=>1);

		$category_data  = $this->Api_model->getRows('cp_music_category',$categoryCon);

		$categoryTotalCon['conditions'] = array("music_category_status"=>1);

		$category_Total_data  = $this->Api_model->getRows('cp_music_category',$categoryTotalCon);

		if(empty($category_data)){

			$musicList['Categories'] = array();}else{

				$musicList['Categories']['categoryCount'] = count($category_Total_data);

				foreach ( $category_data as $row_category_data ){

					$musicList['Categories']['categoryList'][] = array(

						'id'=>$row_category_data['music_category_id'],

						'name'=>$row_category_data['music_category_name'],

						'category_icon'=>base_url().'uploads/music/category_icon/'.$row_category_data['music_category_icon'],

					);

				}

			}

// get my music data

			$myMusicCon['sorting'] = array("music_id"=>"DESC");

			$myMusicCon['conditions'] = array("user_id"=>$data['user_id']);

			$myMusicCon['limit'] = $limit;

			$my_music_data  = $this->Api_model->getRows('music',$myMusicCon);

			$myMusicTotalCon['conditions'] = array("user_id"=>$data['user_id']);

			$myMusic_Total_data  = $this->Api_model->getRows('music',$myMusicTotalCon);

			if(empty($my_music_data)){

				$musicList['myMusic']['myMusicCount'] = "0";$musicList['myMusic']['myMusicList']=array();}else{

					$musicList['myMusic']['myMusicCount'] = count($myMusic_Total_data);

					foreach ( $my_music_data as $row_mymusic_data ){

						$myMusicFav  = $this->getMusicUserFav($row_mymusic_data['music_id'],$data['user_id']);

						$myMusicCategoryName  = $this->getCategoryName($row_mymusic_data['category_id']);

						$musicList['myMusic']['myMusicList'][] = array(

							'music_id'=>$row_mymusic_data['music_id'],

							'title'=>$row_mymusic_data['music_title'],

							'desc'=>$row_mymusic_data['music_desc'],

							'artist'=>$row_mymusic_data['music_artist'],

							'music_image'=>base_url().'uploads/music/image/'.$row_mymusic_data['music_image'],

							'music_file'=>base_url().'uploads/music/'.$row_mymusic_data['music_file'],

							'fav'=>$myMusicFav,

							'category_name'=>$myMusicCategoryName,

							'user_id'=>$row_mymusic_data['user_id'],

						);

					}

				}

// get favourite music data

				$favMusicCon['sorting'] = array("music_fav_id"=>"DESC");

				$favMusicCon['limit'] = $limit;

				$favMusicCon['conditions'] = array("cp_music_favorite.user_id"=>$data['user_id']);

				$fav_music_data  = $this->Api_model->getRowsFavouriteMusic('cp_music_favorite',$favMusicCon);

// print_r("<pre/>");

//print_r($fav_music_data);

//  die;

				$favMusicTotalCon['conditions'] = array("cp_music_favorite.user_id"=>$data['user_id']);

				$favMusic_Total_data  = $this->Api_model->getRowsFavouriteMusic('cp_music_favorite',$favMusicTotalCon);

				if(empty($fav_music_data)){

					$musicList['Favourite']['favCount'] = "0";$musicList['Favourite']['favList']=array();}else{

						$musicList['Favourite']['favCount'] = count($favMusic_Total_data);

						foreach ( $fav_music_data as $row_favmusic_data ){

							$favMusicCategoryName  = $this->getCategoryName($row_favmusic_data['category_id']);

							$musicList['Favourite']['favList'][] = array(

								'id'=>$row_favmusic_data['music_fav_id'],

								'title'=>$row_favmusic_data['music_title'],

								'desc'=>$row_favmusic_data['music_desc'],

								'artist'=>$row_favmusic_data['music_artist'],

								'music_image'=>base_url().'uploads/music/image/'.$row_favmusic_data['music_image'],

								'music_file'=>base_url().'uploads/music/'.$row_favmusic_data['music_file'],

								'music_id'=>$row_favmusic_data['music_id'],

								'category_name'=>$favMusicCategoryName,

								'fav'=>1,

								'user_id'=>$data['user_id'],

							);

						}

					}

					if(!empty($banner_data) or !empty($popular_music_data) or !empty($recent_music_data) or !empty($category_data) or !empty($my_music_data) or !empty($fav_music_data)){

						$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('musicList' => $musicList));

					}else{

						$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Music Data not found','response' => array('message' => 'Music Data not found'));

					}

					$this->response($resp);

				}

// add popular data

				public function add_popular_music_post(){

					// $pdata = file_get_contents("php://input");

					// $data = json_decode($pdata, true);

					$data = $this->param;

					$object_info = $data;

					$required_parameter = array('music_id');

					$chk_error = check_required_value($required_parameter, $object_info);

					if ($chk_error) {

						$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

						$this->response($resp);

					}

					$this->db->select('music_view');

					$this->db->from('music');

					$this->db->where("music_id", $data['music_id']);

					$query = $this->db->get();

					$result = $query->row_array();

					$view = $result['music_view'];

					$count_where  = array('music_id'=>$data['music_id']);

					$count_array  = array('music_view'=>$view+1);

					$musicId = $this->common_model->updateRecords('music', $count_array, $count_where);

					if($musicId){

						$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Added successfully', 'response' => array('message'=>'Added successfully'));

					}

					else

					{

						$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

					}

					$this->response($resp);

				}

// get category name

				public function getCategoryName($music_category_id){

					$this->db->select('music_category_name');

					$this->db->from('cp_music_category');

					$this->db->where("music_category_id",$music_category_id);

					$query = $this->db->get();

					$result = $query->row_array();

					$categoryName = $result['music_category_name'];

					if($categoryName){

						return $categoryName;

					}else{

						return "NA";

					}

				}

				public function add_popular_movie_post(){

					// $pdata = file_get_contents("php://input");

					// $data = json_decode($pdata, true);

					$data = $this->param;

					$object_info = $data;

					$required_parameter = array('movie_id');

					$chk_error = check_required_value($required_parameter, $object_info);

					if ($chk_error) {

						$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

						$this->response($resp);

					}

					$this->db->select('movie_view');

					$this->db->from('movie');

					$this->db->where("movie_id", $data['movie_id']);

					$query = $this->db->get();

					$result = $query->row_array();

					$view = $result['movie_view'];

					$count_where  = array('movie_id'=>$data['movie_id']);

					$count_array  = array('movie_view'=>$view+1);

					$movieId = $this->common_model->updateRecords('movie', $count_array, $count_where);

					if($movieId){

						$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Added successfully', 'response' => array('message'=>'Added successfully'));

					}

					else

					{

						$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

					}

					$this->response($resp);

				}

// movie listing api

				public function movie_list_post(){

					// $pdata = file_get_contents("php://input");

					// $data = json_decode($pdata, true);

					$data = $this->param;

					$object_info = $data;

					$required_parameter = array('user_id');

					$chk_error = check_required_value($required_parameter, $object_info);

					if ($chk_error) {

						$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

						$this->response($resp);

					}

					$limit = 10;

// get banner data

					$bannerCon['sorting'] = array("movie_banner_id"=>"DESC");

					$bannerCon['conditions'] = array("movie_banner_status"=>1);

					$bannerCon['limit'] = $limit;

					$banner_data  = $this->Api_model->getRows('cp_movie_banner',$bannerCon);

					$bannerTotalCon['conditions'] = array("movie_banner_status"=>1);

					$banner_Total_data  = $this->Api_model->getRows('cp_movie_banner',$bannerTotalCon);

					if(empty($banner_data)){

						$movieList['Banners'] = array();}else{

							$movieList['Banners']['bannerCount'] = count($banner_Total_data);

							foreach ( $banner_data as $row_banner_data ){

								$movieList['Banners']['bannerList'][] = array(

									'id'=>$row_banner_data['movie_banner_id'],

									'movie_banner_path'=>base_url().'uploads/movie/banners/'.$row_banner_data['movie_banner_image'],

									'movie_banner_file'=>base_url().'uploads/movie/banners/files/'.$row_banner_data['movie_banner_file']

								);

							}

						}

// get popular music data

						$popularCon['sorting'] = array("movie_view"=>"DESC");

						$popularCon['limit'] = $limit;

						$popular_movie_data  = $this->Api_model->getRows('movie',$popularCon);

						$popular_Total_data  = $this->Api_model->getRows('movie');

						if(empty($popular_movie_data)){

							$movieList['Populars']['popularCount'] = 0;

							$movieList['Populars']['popularList'] = array();

						}else{

							$movieList['Populars']['popularCount'] = count($popular_Total_data);

							foreach ( $popular_movie_data as $row_popular_data ){

								$popularFav  = $this->getMovieUserFav($row_popular_data['movie_id'],$data['user_id']);

								$popularCategoryName  = $this->getMovieCategoryName($row_popular_data['category_id']);

								$mypopu = $this->common_model->getsingle("movie",array("user_id" => $row_popular_data['user_id']));

								if($mypopu->user_id == $data['user_id'])

								{

									$isedit_popular = "1";

								}

								else

								{

									$isedit_popular = "";

								}

								$movieList['Populars']['popularList'][] = array(

									'id'=>$row_popular_data['movie_id'],

									'title'=>$row_popular_data['movie_title'],

									'desc'=>$row_popular_data['movie_desc'],

									'artist'=>$row_popular_data['movie_artist'],

									'movie_image'=>base_url().'uploads/movie/image/'.$row_popular_data['movie_image'],

									'movie_file'=>base_url().'uploads/movie/'.$row_popular_data['movie_file'],

									'fav'=>$popularFav,

									'category_name'=>$popularCategoryName,

									'isEditable' => $isedit_popular

								);

							}

						}

// get recent music data

						$recentCon['sorting'] = array("movie_id"=>"DESC");

						$recentCon['limit'] = $limit;

						$recent_movie_data  = $this->Api_model->getRows('movie',$recentCon);

						$recent_Total_data  = $this->Api_model->getRows('movie');

						if(empty($recent_movie_data)){

							$movieList['Recents']['recentCount'] = 0;

							$movieList['Recents']['recentList'] = array();

						}else{

							$movieList['Recents']['recentCount'] = count($recent_Total_data);

							foreach ( $recent_movie_data as $row_recent_data ){

								$recentFav  = $this->getMovieFav($row_recent_data['movie_id']);

								$recentCategoryName  = $this->getMovieCategoryName($row_recent_data['category_id']);

								$myrece = $this->common_model->getsingle("movie",array("user_id" => $row_recent_data['user_id']));

								if($myrece->user_id == $data['user_id'])

								{

									$isedit_recent = "1";

								}

								else

								{

									$isedit_recent = "";

								}

								$movieList['Recents']['recentList'][] = array(

									'id'=>$row_recent_data['movie_id'],

									'title'=>$row_recent_data['movie_title'],

									'desc'=>$row_recent_data['movie_desc'],

									'artist'=>$row_recent_data['movie_artist'],

									'movie_image'=>base_url().'uploads/movie/image/'.$row_recent_data['movie_image'],

									'movie_file'=>base_url().'uploads/movie/'.$row_recent_data['movie_file'],

									'fav'=>$recentFav,

									'category_name'=>$recentCategoryName,

									'isEditable' => $isedit_recent

								);

							}

						}

// get category data

						$categoryCon['sorting'] = array("movie_category_id"=>"DESC");

						$categoryCon['limit'] = $limit;

						$category_data  = $this->Api_model->getRows('cp_movie_category',$categoryCon);

						$categoryTotalCon['conditions'] = array("movie_category_id"=>1);

						$category_Total_data  = $this->Api_model->getRows('cp_movie_category',$categoryTotalCon);

						if(empty($category_data)){

							$movieList['Categories'] = array();}else{

								$movieList['Categories']['categoryCount'] = count($category_Total_data);

								foreach ( $category_data as $row_category_data ){

									$movieList['Categories']['categoryList'][] = array(

										'id'=>$row_category_data['movie_category_id'],

										'name'=>$row_category_data['movie_category_name'],

										'category_icon'=>base_url().'uploads/movie/category_icon/'.$row_category_data['movie_category_icon'],

									);

								}

							}

// get my movie data

							$myMovieCon['sorting'] = array("movie_id"=>"DESC");

							$myMovieCon['conditions'] = array("user_id"=>$data['user_id']);

							$myMovieCon['limit'] = $limit;

							$my_movie_data  = $this->Api_model->getRows('movie',$myMovieCon);

							$myMovieTotalCon['conditions'] = array("user_id"=>$data['user_id']);

							$myMovie_Total_data  = $this->Api_model->getRows('movie',$myMovieTotalCon);

							if(empty($my_movie_data)){

								$movieList['myMovie']['myMovieCount'] = "0";$movieList['myMovie']['myMovieList']=array();}else{

									$movieList['myMovie']['myMovieCount'] = count($myMovie_Total_data);

									foreach ( $my_movie_data as $row_mymovie_data ){

										$myMovieFav  = $this->getMovieFav($row_mymovie_data['movie_id']);

										$myMovieCategoryName  = $this->getMovieCategoryName($row_mymovie_data['category_id']);

										$movieList['myMovie']['myMovieList'][] = array(

											'id'=>$row_mymovie_data['movie_id'],

											'title'=>$row_mymovie_data['movie_title'],

											'desc'=>$row_mymovie_data['movie_desc'],

											'artist'=>$row_mymovie_data['movie_artist'],

											'movie_image'=>base_url().'uploads/movie/image/'.$row_mymovie_data['movie_image'],

											'movie_file'=>base_url().'uploads/movie/'.$row_mymovie_data['movie_file'],

											'fav'=>$myMovieFav,

											'category_name'=>$myMovieCategoryName,

											'isEditable'=>1

										);

									}

								}

								$favMovieCon['sorting'] = array("movie_fav_id"=>"DESC");

								$favMovieCon['limit'] = $limit;

								$favMovieCon['conditions'] = array("cp_movie_favorite.user_id"=>$data['user_id']);

								$fav_movie_data  = $this->Api_model->getRowsFavouriteMovie('cp_movie_favorite',$favMovieCon);

								$favMovieTotalCon['conditions'] = array("cp_movie_favorite.user_id"=>$data['user_id']);

								$favMovie_Total_data  = $this->Api_model->getRowsFavouriteMovie('cp_movie_favorite',$favMovieTotalCon);

								if(empty($fav_movie_data)){

									$movieList['Favourite']['favCount'] = "0";$movieList['Favourite']['favList']=array();}else{

										$movieList['Favourite']['favCount'] = count($favMovie_Total_data);

										foreach ( $fav_movie_data as $row_favmovie_data ){

											$favMovieCategoryName  = $this->getCategoryName($row_favmovie_data['category_id']);

											$myfavmo = $this->common_model->getsingle("movie",array("user_id" => $row_favmovie_data['user_id']));

											if($myfavmo->user_id == $data['user_id'])

											{

												$isedit_fav = "1";

											}

											else

											{

												$isedit_fav = "";

											}

											$movieList['Favourite']['favList'][] = array(

												'id'=>$row_favmovie_data['movie_fav_id'],

												'title'=>$row_favmovie_data['movie_title'],

												'desc'=>$row_favmovie_data['movie_desc'],

												'artist'=>$row_favmovie_data['movie_artist'],

												'movie_image'=>base_url().'uploads/movie/image/'.$row_favmovie_data['movie_image'],

												'movie_file'=>base_url().'uploads/movie/'.$row_favmovie_data['movie_file'],

												'movie_id'=>$row_favmovie_data['movie_id'],

												'category_name'=>$favMovieCategoryName,

												'fav'=>1,

												"isEditable" => $isedit_fav

											);

										}

									}

									if(!empty($banner_data) or !empty($popular_movie_data) or !empty($recent_movie_data) or !empty($category_data) or !empty($my_movie_data) or !empty($fav_movie_data)){

										$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('movieList' => $movieList));

									}else{

										$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Moview Data not found','response' => array('message' => 'Movie Data not found'));

									}

									$this->response($resp);

								}

// get movie category name

								public function getMovieCategoryName($movie_category_id){

									$this->db->select('movie_category_name');

									$this->db->from('cp_movie_category');

									$this->db->where("movie_category_id",$movie_category_id);

									$query = $this->db->get();

									$result = $query->row_array();

									$categoryName = $result['movie_category_name'];

									if($categoryName){

										return $categoryName;

									}else{

										return "NA";

									}

								}

								public function changepassword1_post()

								{

									// $pdata = file_get_contents("php://input");

									// $data = json_decode($pdata, true);

									$data = $this->param;

									$object_info = $data;

									$required_parameter = array('email', 'password', 'confirm_password');

									$chk_error = check_required_value($required_parameter, $object_info);

									if ($chk_error) {

										$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

										$this->response($resp);

									}

									// security changes start

									$password = trim($data['password']);

									$regex_lowercase = '/[a-z]/';

									$regex_uppercase = '/[A-Z]/';

									$regex_number = '/[0-9]/';

									$regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';

		// if (preg_match_all($regex_lowercase, $password) < 1)

		// {

		// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => message(43), 'response' => array('message' => message(43)));

		// 	$this->response($resp);

		// }

									if(preg_match_all($regex_lowercase, $password) < 1) {

										$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must contains lowercase letter.', 'response' => array('message' => 'Your password must contains lowercase letter.'));

										$this->response($resp);

									}

									if(preg_match_all($regex_uppercase, $password) < 1) {

										$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must contains uppercase letter.', 'response' => array('message' => 'Your password must contains uppercase letter.'));

										$this->response($resp);

									}

									if(preg_match_all($regex_number, $password) < 1) {

										$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must contains number.', 'response' => array('message' => 'Your password must contains number.'));

										$this->response($resp);

									}

									if(preg_match_all($regex_special, $password) < 1) {

										$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must contains special character.', 'response' => array('message' => 'Your password must contains special character.'));

										$this->response($resp);

									}

									if(strlen($password) < 6) {

										$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must be at least 6 characters long.', 'response' => array('message' => 'Your password must be at least 6 characters long.'));

										$this->response($resp);

									}

									if(strlen($password) > 30) {

										$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must not exceed 30 characters long.', 'response' => array('message' => 'Your password must not exceed 30 characters long.'));

										$this->response($resp);

									}

		// security changes end

									if($data['password'] != $data['confirm_password']) {

										$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => "Input must be the same as the password", 'response' => array('message' => "Input must be the same as the password"));

										$this->response($resp);

									}

									$email = $data['email'];

									/* Check for user id */

									$check_key = $this->common_model->getRecordCount('cp_users', array('email' => $email));

									if($check_key == 1) {

										/* Change password */

										$condition = array('email' => $email);

										$updateArr = array('password' => md5($data['password']));

										$this->common_model->updateRecords('cp_users', $updateArr, $condition);

										/* Response array */

										$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => "Your password reset successfully", 'response' => array('message' => "Your password reset successfully"));

									} else {

										$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => "Invalid details, please try again", 'response' => array('message' => "Invalid details, please try again"));

									}

									$this->response($resp);

								}

								public function changepassword_post()

								{

									// $pdata = file_get_contents("php://input");

									// $data = json_decode($pdata, true);

									$data = $this->param;

									$object_info = $data;

									$required_parameter = array('user_id','old_password','password');

									$chk_error = check_required_value($required_parameter, $object_info);

									if ($chk_error) {

										$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

										$this->response($resp);

									}

									$user_pass = $this->common_model->getSingleRecordById('cp_users', array('id' => $data['user_id']));

									// security changes start

									$password = trim($data['password']);

									$regex_lowercase = '/[a-z]/';

									$regex_uppercase = '/[A-Z]/';

									$regex_number = '/[0-9]/';

									$regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';

		// if (preg_match_all($regex_lowercase, $password) < 1)

		// {

		// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => message(43), 'response' => array('message' => message(43)));

		// 	$this->response($resp);

		// }

									if(preg_match_all($regex_lowercase, $password) < 1) {

										$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must contains lowercase letter.', 'response' => array('message' => 'Your password must contains lowercase letter.'));

										$this->response($resp);

									}

									if(preg_match_all($regex_uppercase, $password) < 1) {

										$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must contains uppercase letter.', 'response' => array('message' => 'Your password must contains uppercase letter.'));

										$this->response($resp);

									}

									if(preg_match_all($regex_number, $password) < 1) {

										$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must contains number.', 'response' => array('message' => 'Your password must contains number.'));

										$this->response($resp);

									}

									if(preg_match_all($regex_special, $password) < 1) {

										$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must contains special character.', 'response' => array('message' => 'Your password must contains special character.'));

										$this->response($resp);

									}

									if(strlen($password) < 6) {

										$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must be at least 6 characters long.', 'response' => array('message' => 'Your password must be at least 6 characters long.'));

										$this->response($resp);

									}

									if(strlen($password) > 30) {

										$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your password must not exceed 30 characters long.', 'response' => array('message' => 'Your password must not exceed 30 characters long.'));

										$this->response($resp);

									}

		// security changes end

//print_r($user_pass); die;

									if(md5($data['old_password']) != $user_pass['password']) {

										$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => "Please old password must be correct", 'response' => array('message' => "Please old password must be correct"));

										$this->response($resp);

									}

/* if($data['password'] != $data['confirm_password']) {

$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => message(13), 'response' => array('message' => message(13)));

$this->response($resp);

}*/

$userId = $data['user_id'];

/* Check for user id */

$check_key = $this->common_model->getRecordCount('cp_users', array('id' => $userId));

if($check_key == 1) {

	/* Change password */

	$condition = array('id' => $userId);

	$updateArr = array('password' => md5($data['password']));

	$this->common_model->updateRecords('cp_users', $updateArr, $condition);

	/* Response array */

	$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => "Your password change successfully", 'response' => array('message' => "Your password change successfully"));

} else {

	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => "Invalid details, please try again", 'response' => array('message' => "Invalid details, please try again"));

}

$this->response($resp);

}

public function getUser_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array("user_id");

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$con['sorting'] = array("id"=>"DESC");

	$userData = $this->common_model->getSingleRecordById('cp_users', array('id' => $data['user_id']));

	if(!empty($userData)){

		$userData1 = array('id'=>$userData['id'],

			'name'=>$userData['name'],

			'username'=>$userData['username'],

			'email'=>$userData['email'],

			'phone'=>$userData['mobile'],

			'profile_image'=>base_url().'uploads/profile_images/'.$userData['profile_image']

		);

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('user_data' => $userData1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Doctor not found','response' => array('message' => 'Acknowledge Data not found'));

	}

	$this->response($resp);

}

/*list mail */

public function inboxlist_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$limit = 10;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));

		$this->response($resp);

	}

	if($data['page_no']==''){

		$data['page_no']=1;

	}

	$emailId = $this->common_model->getSingleRecordById('cp_users', array('id' => $data['user_id']));

	$emailId = $emailId['email'];

	$emailCount =$this->common_model->getEmailCount1($emailId,$data['user_id']);

	$start  = ($data['page_no']-1)*$limit;

	$pages = ceil(count($emailCount) / $limit);

	$emailData =$this->common_model->getEmails1($start,$limit,$emailId,$data['user_id']);

//print_r("<pre/>");

//print_r($emailCount);

//  die;

	if(!empty($emailData)){

		foreach ( $emailData as $row ){

			if($emailData['create_email'] == "0000-00-00"){

				$emails_create = $row['create_email'];

			}else{

				$emails_create = date("M d, Y H:i:s", strtotime($row['create_email']));

			}

			$attachment = $this->orderImage($row['email_id']);

			$toEmail = $this->toEmail($row['email_id']);

			$getMail = $this->getMail($row['user_id']);

			$email[] = array(

				'id' => $row['email_id'],

				'user_id' => $row['user_id'],

				'to_email' => $toEmail,

				'subject' => $row['subject'],

				'message' => $row['message'],

				'create_date' => $emails_create,

				"attachment"=>$attachment,

				"username"=>$row['username'],

				'profile_image'=>base_url().'uploads/profile_images/'.$row['profile_image'],

				'send_email'=>$getMail,

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'total'=>count($emailCount),'per_page_records'=>$limit, 'response' => array('email_data' => $email));

	}

	else {

		$email = array();

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Email not found','response' => array('message' => 'Email not found','email_data' => $email));

	}

	$this->response($resp);

}

public function emaillist_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$limit = 10;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));

		$this->response($resp);

	}

	if($data['page_no']==''){

		$data['page_no']=1;

	}

	$emailCount =$this->common_model->getEmailCount($data['user_id']);

	$start  = ($data['page_no']-1)*$limit;

	$pages = ceil(count($emailCount) / $limit);

	$emailData =$this->common_model->getEmails($start,$limit,$data['user_id']);

	if(!empty($emailData)){

		foreach ( $emailData as $row ){

			if($row['create_email'] == "0000-00-00"){

				$emails_create = $row['create_email'];

			}else{

				$emails_create = date("M d, Y H:i:s", strtotime($row['create_email']));

			}

			$attachment = $this->orderImage($row['email_id']);

			$toEmail = $this->toEmail($row['email_id']);

			$getMail = $this->getMail($row['user_id']);

			$email[] = array(

				'id' => $row['email_id'],

				'user_id' => $row['user_id'],

				'to_email' => $toEmail,

				'subject' => $row['subject'],

				'message' => $row['message'],

				'create_date' => $emails_create,

				'attachment' => $attachment,

				"username"=>$row['username'],

				'profile_image'=>base_url().'uploads/profile_images/'.$row['profile_image'],

				'send_email'=>$getMail,

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'total'=>count($emailCount),'per_page_records'=>$limit, 'response' => array('email_data' => $email));

	}

	else {

		$email = array();

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Email not found','response' => array('message' => 'Email not found','email_data' => $email));

	}

	$this->response($resp);

}

/*-------User list--------*/

public function user_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$con['sorting'] = array("id"=>"DESC");

	$userData  = $this->common_model->getAllRecordsById('cp_users',array('status'=>1));

	if(!empty($userData)){

		foreach($userData as $userData_details){

			$userData_details1[] = array('user_id'=>$userData_details['id'],

				'name'=>$userData_details['name'],

				'email'=>$userData_details['email'],

				'profile_image'=>base_url().'uploads/profile_images/'.$userData_details['profile_image'],

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('user_data' => $userData_details1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'User not found','response' => array('message' => 'User Data not found'));

	}

	$this->response($resp);

}

public function toEmail($email_id){

	$this->db->select('to_email');

	$this->db->from('cp_to_email');

	$this->db->where('email_id',$email_id);

	$query = $this->db->get();

	$result = $query->result_array();

	foreach($result as $result_details){

		$imageArray[] = $result_details['to_email'];

	}

	$result_new = implode(",",$imageArray);

	return $result_new;

}

public function getMail($user_id){

	$this->db->select('email');

	$this->db->from('cp_users');

	$this->db->where('id',$user_id);

	$query = $this->db->get();

	$result = $query->result_array();

	return $result[0]['email'];

}

/*public function emaildelete_post(){

$pdata = file_get_contents("php://input");

$data = json_decode($pdata, true);

$object_info = $data;

$required_parameter = array('email_id','user_id');

$chk_error = check_required_value($required_parameter, $object_info);

if ($chk_error) {

$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

$this->response($resp);

}

$ids = explode(',',$data['email_id']);

foreach($ids as $ids_details){

$post_data = array('email_id'=>$ids_details,'user_id'=>$data['user_id']);

$emailid = $this->common_model->addRecords('cp_email_trash',$post_data);

}

if($emailid) {

$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('email_data' => "email data deleted succesfully"));

}

else {

$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Email not found','response' => array('message' => 'Some error occured, please try again'));

}

$this->response($resp);

}*/

public function emaildelete_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$required_parameter = array('email_id','user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	$ids = explode(',',$data['email_id']);

	foreach($ids as $ids_details){

		$post_data = array('email_id'=>$ids_details,'user_id'=>$data['user_id'],'check_email' => $data['delete_mail'],'trash_created'=>date('Y-m-d H:i:s'));

		$emailid = $this->common_model->addRecords('cp_email_trash',$post_data);

	}

	if($emailid) {

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('email_data' => "Message successfully deleted"));

	}

	else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Email not found','response' => array('message' => 'Some error occured, please try again'));

	}

	$this->response($resp);

}

/*-------User list--------*/

public function trash_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$limit = 10;

	$required_parameter = array('user_id');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	if($data['page_no']==''){

		$data['page_no']=1;

	}

	$trashCount =$this->common_model->getCountTrash($data['user_id']);

	$start  = ($data['page_no']-1)*$limit;

	$pages = ceil(count($trashCount) / $limit);

	$userData  = $this->common_model->getTrash($start,$limit,$data['user_id']);

	if(!empty($userData)){

		foreach($userData as $userData_details){

			$toEmail = $this->toEmail($userData_details['email_id']);

			if($userData_details['trash_created'] == "0000-00-00 00:00:00"){

				$emails_create = $userData_details['trash_created'];

			}else{

				$emails_create = date("M d, Y H:i:s", strtotime($userData_details['trash_created']));

			}

			$userData_details1[] = array('id'=>$userData_details['trash_email_id'],

				'username'=>$userData_details['username'],

				'user_id'=>$userData_details['user_id'],

				'to_email' => $toEmail,

				'subject'=>$userData_details['subject'],

				'message'=>$userData_details['message'],

				'create_date'=>$emails_create,

				'profile_image'=>base_url().'uploads/profile_images/'.$userData_details['profile_image'],

				'mail_status' => $userData_details['check_email']

			);

		}

//$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('email_data' => $userData_details1));

//$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('email_data' => $userData_details1));

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'total'=>count($trashCount),'per_page_records'=>$limit, 'response' => array('email_data' => $userData_details1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Data not found','response' => array('message' => 'Data not found'));

	}

	$this->response($resp);

}

/*-------User list--------*/

public function musicCateogry_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

	$data = $this->param;

	$object_info = $data;

	$musicCategoryData  = $this->common_model->getAllRecordsById('cp_music_category',array('music_category_status'=>1));

	if(!empty($musicCategoryData)){

		foreach($musicCategoryData as $musicCategoryData_details){

			$musicCategoryData_details1[] = array('id'=>$musicCategoryData_details['music_category_id'],

				'name'=>$musicCategoryData_details['music_category_name'],

				'category_icon'=>base_url().'uploads/music/category_icon/'.$musicCategoryData_details['music_category_icon'],

			);

		}

		$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('category_data' => $musicCategoryData_details1));

	} else {

		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'User not found','response' => array('message' => 'User Data not found'));

	}

	$this->response($resp);

}

public function addUserMusic_post(){

	$data = $_POST;

	$object_info = $data;

	$required_parameter = array('category_id','user_id','music_title','music_desc','music_artist');

	$chk_error = check_required_value($required_parameter, $object_info);

	if ($chk_error) {

		$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

		$this->response($resp);

	}

	if(!empty($_FILES['music_image']['name'])) {

		$imgfile=$_FILES['music_image']['name'];

		$extension = substr($imgfile,strlen($imgfile)-4,strlen($imgfile));

		// echo $imgfile;

		// die;

            // allowed extensions

		$allowed_extensions = array(".jpg",".jpeg",".png",".gif");

		//$allowed_extensions = array(".jpg",".jpeg");

		// security chnages start

		 if(in_array($extension,$allowed_extensions)) // security check start

		 {

		 	$random = $this->generateRandomString(10);

		 	$ext = pathinfo($_FILES['music_image']['name'], PATHINFO_EXTENSION);

		 	$music_file_name = $random.".".$ext;

		 	$target_dir = "uploads/music/image/";

		 	$target_file = $target_dir .$music_file_name;

		 	if (move_uploaded_file($_FILES["music_image"]["tmp_name"], $target_file)){

		 		$musicImage = $music_file_name;

		 	}

		 	}else{  // security check end

		 		$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your image file is not valide. allow file types jpg, jpeg, png', 'response' => array('message' => 'Your image file is not valide. allow file types jpg, jpeg, png'));

		 		$this->response($resp); 

		 	}

		 }

		 if(!empty($_FILES['music_file']['name'])) {

		 	$random = $this->generateRandomString(10);

		 	$ext = pathinfo($_FILES['music_file']['name'], PATHINFO_EXTENSION);

		 	$file_name = $random.".".$ext;

		 	$target_dir = "uploads/music/";

		 	$target_file = $target_dir .$file_name;

		 	if (move_uploaded_file($_FILES["music_file"]["tmp_name"], $target_file)){

		 		$musicFile = $file_name;

		 	}

		 }

		 $post_data = array

		 (

		 	'category_id'=> $data['category_id'],

		 	'user_id'=> $data['user_id'],

		 	'music_title'=> $data['music_title'],

		 	'music_desc'=> $data['music_desc'],

		 	'music_artist'=> $data['music_artist'],

		 	'music_image'        =>$musicImage,

		 	'music_file'        =>$musicFile,

		 	'music_created' => date('Y-m-d h:i:s'),

		 );

		 $musicUserId = $this->common_model->addRecords('music', $post_data);

		 if($musicUserId)

		 {

		 	$musicData = $this->common_model->getSingleRecordById('music', array('music_id' => $musicUserId));

		 	$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'music added successfully', 'response' => array('music_data' => "music added successfully"));

		 }else{

		 	$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

		 }

		 $this->response($resp);

		}

		public function updateUserMusic_post(){

			// $data = $_POST;

			$data = $this->param;

			$object_info = $data;

			$required_parameter = array('category_id','music_title','music_desc','music_artist','music_id');

			$chk_error = check_required_value($required_parameter, $object_info);

			if ($chk_error) {

				$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

				$this->response($resp);

			}

			if(!empty($_FILES['music_image']['name'])) {

				$imgfile=$_FILES['music_image']['name'];

				$extension = substr($imgfile,strlen($imgfile)-4,strlen($imgfile));

				$allowed_extensions = array(".jpg",".jpeg",".png",".gif");

				//$allowed_extensions = array(".jpeg",".png",".gif");

				if(in_array($extension,$allowed_extensions)) // security check start

				{

					$random = $this->generateRandomString(10);

					$ext = pathinfo($_FILES['music_image']['name'], PATHINFO_EXTENSION);

					$music_file_name = $random.".".$ext;

					$target_dir = "uploads/music/image/";

					$target_file = $target_dir .$music_file_name;

					if (move_uploaded_file($_FILES["music_image"]["tmp_name"], $target_file)){

						$musicImage = $music_file_name;

						$post_data['music_image'] = $musicImage;

					}

				}else{  

					$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your image file is not valide. allow file types jpg, jpeg, png', 'response' => array('message' => 'Your image file is not valide. allow file types jpg, jpeg, png, gif'));

					$this->response($resp); 

				}

				// security check end

			}

			if(!empty($_FILES['music_file']['name'])) {

				$random = $this->generateRandomString(10);

				$ext = pathinfo($_FILES['music_file']['name'], PATHINFO_EXTENSION);

				$file_name = $random.".".$ext;

				$target_dir = "uploads/music/";

				$target_file = $target_dir .$file_name;

				if (move_uploaded_file($_FILES["music_file"]["tmp_name"], $target_file)){

					$musicFile = $file_name;

					$post_data['music_file'] = $musicFile;

				}

			}

			$post_data['category_id'] = $data['category_id'];

			$post_data['user_id'] = $data['user_id'];

			$post_data['music_title'] = $data['music_title'];

			$post_data['music_desc'] = $data['music_desc'];

			$post_data['music_artist'] = $data['music_artist'];

			$musicUserId = $this->common_model->updateRecords('music', $post_data,array('music_id' => $data['music_id']));

			$data_music = $this->common_model->getsingle("music",array('music_id' => $data['music_id']));

			$Fav  = $this->getMusicUserFav($data_music->music_id,$data_music->user_id);

			$CategoryName  = $this->getCategoryName($data_music->category_id);

			$music_data_array = array(

				"title" => $data_music->music_title,

				"desc" => $data_music->music_desc,

				"artist" => $data_music->music_artist,

				"music_image" => base_url().'uploads/music/image/'.$data_music->music_image,

				"music_file" => base_url().'uploads/music/'.$data_music->music_file,

				"fav" => $Fav,

				"category_name" => $CategoryName,

				"user_id" => $data_music->user_id,

				"music_id" => $data_music->music_id,

			);

			if($musicUserId)

			{

				$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'music update successfully', 'response' => array('music_data' => $music_data_array));

			}else{

				$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

			}

			$this->response($resp);

		}

		public function  get_musicCategory_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

			$data = $this->param;

			$object_info = $data;

			$required_parameter = array('category_id');

			$chk_error = check_required_value($required_parameter, $object_info);

			if ($chk_error) {

				$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

				$this->response($resp);

			}

			$pdata = file_get_contents("php://input");

			$data = json_decode($pdata, true);

			$object_info = $data;

			$musicCategoryData  = $this->common_model->getAllRecordsById('music',array('category_id'=>$data['category_id']));

			if(!empty($musicCategoryData)){

				foreach($musicCategoryData as $musicCategoryData_details){

					$Fav  = $this->getMusicFav($musicCategoryData_details['music_id']);

					$CategoryName  = $this->getCategoryName($musicCategoryData_details['category_id']);

					$musicCategoryData_details1[] = array(

						'music_id'=>$musicCategoryData_details['music_id'],

						'title'=>$musicCategoryData_details['music_title'],

						'desc'=>$musicCategoryData_details['music_desc'],

						'artist'=>$musicCategoryData_details['music_artist'],

						'music_image'=>base_url().'uploads/music/image/'.$musicCategoryData_details['music_image'],

						'music_file'=>base_url().'uploads/music/'.$musicCategoryData_details['music_file'],

						'fav'=>$Fav,

						'category_name'=>$CategoryName

					);

				}

				$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('music_data' => $musicCategoryData_details1));

			} else {

				$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Music not found','response' => array('message' => 'Music Data not found'));

			}

			$this->response($resp);

		}

		public function  my_music_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

			$data = $this->param;

			$object_info = $data;

			$required_parameter = array('user_id');

			$chk_error = check_required_value($required_parameter, $object_info);

			if ($chk_error) {

				$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

				$this->response($resp);

			}

			$pdata = file_get_contents("php://input");

			$data = json_decode($pdata, true);

			$object_info = $data;

			$musicCategoryData  = $this->common_model->getAllRecordsById('music',array('user_id'=>$data['user_id']));

			if(!empty($musicCategoryData)){

				foreach($musicCategoryData as $musicCategoryData_details){

					$Fav  = $this->getMusicFav($musicCategoryData_details['music_id']);

					$CategoryName  = $this->getCategoryName($musicCategoryData_details['category_id']);

					$musicCategoryData_details1[] = array(

						'music_id'=>$musicCategoryData_details['music_id'],

						'title'=>$musicCategoryData_details['music_title'],

						'desc'=>$musicCategoryData_details['music_desc'],

						'artist'=>$musicCategoryData_details['music_artist'],

						'music_image'=>base_url().'uploads/music/image/'.$musicCategoryData_details['music_image'],

						'music_file'=>base_url().'uploads/music/'.$musicCategoryData_details['music_file'],

						'fav'=>$Fav,

						'category_name'=>$CategoryName,

						'user_id'=>$musicCategoryData_details['user_id']

					);

				}

				$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('music_data' => $musicCategoryData_details1));

			} else {

				$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'User not found','response' => array('message' => 'User Data not found'));

			}

			$this->response($resp);

		}

		public function movieCateogry_list_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

			$data = $this->param;

			$object_info = $data;

//$musicCategoryData  = $this->common_model->getAllRecordsById('cp_movie_category',array('movie_category_status'=>1));

			$musicCategoryData  = $this->common_model->getAllwhereorderby('cp_movie_category',array('movie_category_status'=>1),'movie_category_name','ASC');

//print_r("<pre/>");

//print_r($musicCategoryData);

// die;

			if(!empty($musicCategoryData)){

				foreach($musicCategoryData as $musicCategoryData_details){

					$musicCategoryData_details1[] = array('id'=>$musicCategoryData_details->movie_category_id,

						'name'=>$musicCategoryData_details->movie_category_name,

						'category_icon'=>base_url().'uploads/movie/category_icon/'.$musicCategoryData_details->movie_category_icon,

					);

				}

				$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('category_data' => $musicCategoryData_details1));

			} else {

				$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'User not found','response' => array('message' => 'User Data not found'));

			}

			$this->response($resp);

		}

		public function  all_music_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

			$data = $this->param;

			$object_info = $data;

			$required_parameter = array('type');

			$chk_error = check_required_value($required_parameter, $object_info);

			if ($chk_error) {

				$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

				$this->response($resp);

			}

			$pdata = file_get_contents("php://input");

			$data = json_decode($pdata, true);

			$object_info = $data;

			$musicCategoryData  = $this->Api_model->view_all_music($data['type']);

			if(!empty($musicCategoryData)){

				foreach($musicCategoryData as $musicCategoryData_details){

					$Fav  = $this->getMusicFav($musicCategoryData_details['music_id']);

					$CategoryName  = $this->getCategoryName($musicCategoryData_details['category_id']);

					$musicCategoryData_details1[] = array(

						'music_id'=>$musicCategoryData_details['music_id'],

						'title'=>$musicCategoryData_details['music_title'],

						'desc'=>$musicCategoryData_details['music_desc'],

						'artist'=>$musicCategoryData_details['music_artist'],

						'music_image'=>base_url().'uploads/music/image/'.$musicCategoryData_details['music_image'],

						'music_file'=>base_url().'uploads/music/'.$musicCategoryData_details['music_file'],

						'fav'=>$Fav,

						'category_name'=>$CategoryName,

						'user_id'=>$musicCategoryData_details['user_id'],

					);

				}

				$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('music_data' => $musicCategoryData_details1));

			} else {

				$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'User not found','response' => array('message' => 'User Data not found'));

			}

			$this->response($resp);

		}

// add user movie

		public function addUserMovie_post(){

			$data = $_POST;

			$object_info = $data;

			$required_parameter = array('category_id','user_id','movie_title','movie_desc','movie_artist');

			$chk_error = check_required_value($required_parameter, $object_info);

			if ($chk_error) {

				$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

				$this->response($resp);

			}

			if(!empty($_FILES['movie_image']['name'])) {

				$imgfile=$_FILES['movie_image']['name'];

				$extension = substr($imgfile,strlen($imgfile)-4,strlen($imgfile));

				$allowed_extensions = array(".jpg",".jpeg",".png",".gif");

		//$allowed_extensions = array(".jpg",".jpeg");

		// security chnages start

		 if(in_array($extension,$allowed_extensions)) // security check start

		 {

		 	$random = $this->generateRandomString(10);

		 	$ext = pathinfo($_FILES['movie_image']['name'], PATHINFO_EXTENSION);

		 	$movie_file_name = $random.".".$ext;

		 	$target_dir = "uploads/movie/image/";

		 	$target_file = $target_dir .$movie_file_name;

		 	if (move_uploaded_file($_FILES["movie_image"]["tmp_name"], $target_file)){

		 		$movieImage = $movie_file_name;

		 	}

		 }else{  

		 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your image file is not valide. allow file types jpg, jpeg, png, gif', 'response' => array('message' => 'Your image file is not valide. allow file types jpg, jpeg, png, gif'));

		 	$this->response($resp); 

		 }

			// security check end

		}

		if(!empty($_FILES['movie_file']['name'])) {

			$random = $this->generateRandomString(10);

			$ext = pathinfo($_FILES['movie_file']['name'], PATHINFO_EXTENSION);

			$file_name = $random.".".$ext;

			$target_dir = "uploads/movie/";

			$target_file = $target_dir .$file_name;

			if (move_uploaded_file($_FILES["movie_file"]["tmp_name"], $target_file)){

				$movieFile = $file_name;

			}

		}

		$post_data = array

		(

			'category_id'=> $data['category_id'],

			'user_id'=> $data['user_id'],

			'movie_title'=> $data['movie_title'],

			'movie_desc'=> $data['movie_desc'],

			'movie_artist'=> $data['movie_artist'],

			'movie_image'=>$movieImage,

			'movie_file'=>$movieFile,

			'movie_created' => date('Y-m-d h:i:s'),

		);

// echo "<pre>";

// print_r($post_data);

// echo "</pre>";

// die;

		$movieUserId = $this->common_model->addRecords('movie', $post_data);

		if($movieUserId)

		{

			$movieData = $this->common_model->getSingleRecordById('movie', array('movie_id' => $movieUserId));

			$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'movie added successfully', 'response' => array('music_data' => "movie added successfully"));

		}else{

			$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

		}

		$this->response($resp);

	}

// update user movie

	public function updateUserMovie_post(){

		//$data = $_POST;

		$data = $this->param;

		$object_info = $data;

		$required_parameter = array('category_id','movie_title','movie_desc','movie_artist','movie_id');

		$chk_error = check_required_value($required_parameter, $object_info);

		if ($chk_error) {

			$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

			$this->response($resp);

		}

		if(!empty($_FILES['movie_image']['name'])) {

			$imgfile=$_FILES['movie_image']['name'];

			$extension = substr($imgfile,strlen($imgfile)-4,strlen($imgfile));

			$allowed_extensions = array(".jpg",".jpeg",".png",".gif");

			if(in_array($extension,$allowed_extensions)) // security check start

			{

				$random = $this->generateRandomString(10);

				$ext = pathinfo($_FILES['movie_image']['name'], PATHINFO_EXTENSION);

				$movie_file_name = $random.".".$ext;

				$target_dir = "uploads/movie/image/";

				$target_file = $target_dir .$movie_file_name;

				if (move_uploaded_file($_FILES["movie_image"]["tmp_name"], $target_file)){

					$movieImage = $movie_file_name;

					$post_data['movie_image'] = $movieImage;

				}

			}else{  

				$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Your image file is not valide. allow file types jpg, jpeg, png, gif', 'response' => array('message' => 'Your image file is not valide. allow file types jpg, jpeg, png, gif'));

				$this->response($resp); 

			}

			// security check end

		}

		if(!empty($_FILES['movie_file']['name'])) {

			$random = $this->generateRandomString(10);

			$ext = pathinfo($_FILES['movie_file']['name'], PATHINFO_EXTENSION);

			$file_name = $random.".".$ext;

			$target_dir = "uploads/movie/";

			$target_file = $target_dir .$file_name;

			if (move_uploaded_file($_FILES["movie_file"]["tmp_name"], $target_file)){

				$movieFile = $file_name;

				$post_data['movie_file'] = $movieFile;

			}

		}

		$post_data['category_id'] = $data['category_id'];

		$post_data['user_id'] = $data['user_id'];

		$post_data['movie_title'] = $data['movie_title'];

		$post_data['movie_desc'] = $data['movie_desc'];

		$post_data['movie_artist'] = $data['movie_artist'];

		$movieUserId = $this->common_model->updateRecords('movie', $post_data,array('movie_id' => $data['movie_id']));

		if($movieUserId)

		{

			$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'movie update successfully', 'response' => array('movie_data' => "movie update successfully"));

		}else{

			$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

		}

		$this->response($resp);

	}

	public function  get_movieCategory_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

		$data = $this->param;

		$object_info = $data;

		$required_parameter = array('category_id');

		$chk_error = check_required_value($required_parameter, $object_info);

		if ($chk_error) {

			$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

			$this->response($resp);

		}

		$pdata = file_get_contents("php://input");

		$data = json_decode($pdata, true);

		$object_info = $data;

		$musicCategoryData  = $this->common_model->getAllRecordsById('movie',array('category_id'=>$data['category_id']));

		if(!empty($musicCategoryData)){

			foreach($musicCategoryData as $musicCategoryData_details){

				$Fav  = $this->getMovieFav($musicCategoryData_details['movie_id']);

				$CategoryName  = $this->getMovieCategoryName($musicCategoryData_details['category_id']);

				$musicCategoryData_details1[] = array(

					'id'=>$musicCategoryData_details['movie_id'],

					'title'=>$musicCategoryData_details['movie_title'],

					'desc'=>$musicCategoryData_details['movie_desc'],

					'artist'=>$musicCategoryData_details['movie_artist'],

					'movie_image'=>base_url().'uploads/movie/image/'.$musicCategoryData_details['movie_image'],

					'movie_file'=>base_url().'uploads/movie/'.$musicCategoryData_details['movie_file'],

					'fav'=>$Fav,

					'category_name'=>$CategoryName

				);

			}

			$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('movie_data' => $musicCategoryData_details1));

		} else {

			$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Movie not found','response' => array('message' => 'Movie Data not found'));

		}

		$this->response($resp);

	}







	public function  get_meditationVideo_by_Category_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

		$data = $this->param;

		$object_info = $data;

		$required_parameter = array('category_id');

		$chk_error = check_required_value($required_parameter, $object_info);

		if ($chk_error) {

			$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

			$this->response($resp);

		}

		$pdata = file_get_contents("php://input");

		$data = json_decode($pdata, true);

		$object_info = $data;



		if(empty($data['search'])){



			$musicCategoryData  = $this->common_model->getAllRecordsById('meditation_videos',array('category_id'=>$data['category_id']));



			// echo $this->db->last_query();

			// die('if');





			// echo "<pre>";print_r($musicCategoryData);

			// echo "<br>";

			// die('if');



		}else{



			$conditions = array('category_id'=>$data['category_id'],'movie_title'=>$data['search']);

			$musicCategoryData  = $this->common_model->getRecordsByName('meditation_videos',$conditions);



			// echo $this->db->last_query();

			// echo "<br>";

			// die('elsepart');





			// echo "<pre>";

			// print_r($musicCategoryData);

			// die('elsepart');



		}

		if(!empty($musicCategoryData)){

			foreach($musicCategoryData as $musicCategoryData_details){

				$Fav  = $this->getMovieFav($musicCategoryData_details['movie_id']);

				$CategoryName  = $this->getMovieCategoryName($musicCategoryData_details['category_id']);

				$musicCategoryData_details1[] = array(

					'id'=>$musicCategoryData_details['movie_id'],

					'title'=>$musicCategoryData_details['movie_title'],

					'desc'=>$musicCategoryData_details['movie_desc'],

					'artist'=>$musicCategoryData_details['movie_artist'],

					'movie_image'=>base_url().'uploads/meditation_videos/image/'.$musicCategoryData_details['movie_image'],

					'movie_file'=>base_url().'uploads/meditation_videos/'.$musicCategoryData_details['movie_file'],

					'fav'=>$Fav,

					'category_name'=>$CategoryName

				);

			}

			$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('meditation_video_data' => $musicCategoryData_details1));

		} else {

			$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Meditation video not found.','response' => array('message' => 'Meditation video not found.'));

		}

		$this->response($resp);

	}



	public function  all_movie_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

		$data = $this->param;

		$object_info = $data;

		$required_parameter = array('type');

		$chk_error = check_required_value($required_parameter, $object_info);

		if ($chk_error) {

			$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

			$this->response($resp);

		}

		$pdata = file_get_contents("php://input");

		$data = json_decode($pdata, true);

		$object_info = $data;

		$musicCategoryData  = $this->Api_model->view_all_movie($data['type']);

		if(!empty($musicCategoryData)){

			foreach($musicCategoryData as $musicCategoryData_details){

				$Fav  = $this->getMovieUserFav($musicCategoryData_details['movie_id'],$data['user_id']);

				$CategoryName  = $this->getMovieCategoryName($musicCategoryData_details['category_id']);

				$musicCategoryData_details1[] = array(

					'id'=>$musicCategoryData_details['movie_id'],

					'title'=>$musicCategoryData_details['movie_title'],

					'desc'=>$musicCategoryData_details['movie_desc'],

					'artist'=>$musicCategoryData_details['movie_artist'],

					'movie_image'=>base_url().'uploads/movie/image/'.$musicCategoryData_details['movie_image'],

					'movie_file'=>base_url().'uploads/movie/'.$musicCategoryData_details['movie_file'],

					'fav'=>$Fav,

					'category_name'=>$CategoryName

				);

			}

			$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('movie_data' => $musicCategoryData_details1));

		} else {

			$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Movie not found','response' => array('message' => 'Movie Data not found'));

		}

		$this->response($resp);

	}

	public function  my_movie_post(){

	// $pdata = file_get_contents("php://input");

	// $data = json_decode($pdata, true);

		$data = $this->param;

		$object_info = $data;

		$required_parameter = array('user_id');

		$chk_error = check_required_value($required_parameter, $object_info);

		if ($chk_error) {

			$resp = array('status_code' => MISSING_PARAM,'status'