<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Article_model extends CI_Model{
	function __construct() {
		
	}
	
	public function insertData($table,$datainsert)
	{
		$this->db->insert($table,$datainsert);
		return $this->db->insert_id();
	}
	
	public function updateData($table,$data,$where)
	{
		$this->db->update($table,$data,$where);
		return $this->db->affected_rows();
	}
	
	public function delete($table,$where)
	{
		$this->db->delete($table,$where);
		return;
	}
	
	
	public function getsingle($table,$where)
	{
		$q = $this->db->get_where($table,$where);
		return $q->row();
	}

	
	
	public function getAllwhereorderby($table,$where,$order_id,$order_by)
	{
		$this->db->select('*');
		$this->db->order_by($order_id,$order_by);
		$q = $this->db->get_where($table,$where);	
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	
	public function getAll($table)
	{
		$this->db->select('*');		
		$q = $this->db->get($table);		
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	
	public function getAllorderby($table,$order_id,$order_by)
	{
		$this->db->select('*');	
		$this->db->order_by($order_id,$order_by);		
		$q = $this->db->get($table);		
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	
	
	
	public function getAllwhere($table,$where)
	{
		$this->db->select('*');
		$q = $this->db->get_where($table,$where);
		$num_rows = $q->num_rows();
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	
	function jointwotablenn($table, $field_first, $table1, $field_second,$where='',$field,$order_id,$order_by) {

		$this->db->select($field);
		$this->db->order_by( $order_id, $order_by);
		$this->db->from("$table");
		$this->db->join("$table1", "$table1.$field_second = $table.$field_first"); 
		if($where !=''){
			$this->db->where($where); 
		}
		$q = $this->db->get();
		if($q->num_rows() > 0) {
			foreach($q->result() as $rows) {
				$data[] = $rows;
			}
			$q->free_result();
			return $data;
		}
	}
	
	
	/*Created by 95 for show all Articles */
	public function getAllArticles()
	{
		$this->db->select('*');
		$this->db->from('cp_articles');
		$query = $this->db->get();
		return $query->result();
	}


	/*get single row data*/
	public function ToView($article_id='')
	{
		$this->db->select('*');
		$this->db->from('cp_articles');
		$this->db->join('cp_article_category','cp_article_category.article_category_id=cp_articles.category_id','left');
		$this->db->where('article_id',$article_id);
		$query = $this->db->get();
		return $query->row();
	}


	/*get single row data*/
	public function ToViewmeditation($article_id='')
	{
		$this->db->select('*');
		$this->db->from('cp_meditation_articles');
		$this->db->join('cp_meditation_art_category','cp_meditation_art_category.article_category_id=cp_meditation_articles.category_id','left');
		$this->db->where('article_id',$article_id);
		$query = $this->db->get();
		return $query->row();
	}
}