<div class="container-fluid">
<div class="dash-counter">
<div class="Schedule_main_one">
<div class="users-main">
<?php	
echo "<h2>Edit Article</h2>";
?>
<form class="form-horizontal" action="" method="post" enctype="multipart/form-data" name="AddUsers" id="edit-article" >
	<div class="col-md-8">
		<div class="form-group">
			<label class="control-label col-md-4" for="">Article Title:<span class="field_req">*</span></label>
			<div class="col-md-8">
				<input type="text" placeholder="Article Title" class="form-control" name="article_title" id="username" value="<?php echo $article->article_title; ?>">
				<?php echo form_error('article_title'); ?>
				
			</div>
		</div>
	</div>	

	<div class="col-md-8">
		<div class="form-group">
			<label class="control-label col-md-4" for="">Article Description:<span class="field_req">*</span></label>
			<div class="col-md-8">

				<textarea name="article_description" id="" class="form-control" placeholder="Article Title" value="<?php echo set_value('article_description'); ?>"><?php echo $article->article_desc; ?></textarea>
				<?php echo form_error('article_description'); ?>
				
			</div>
		</div>
	</div>

	<div class="col-md-8">
		<div class="form-group">
			<label class="control-label col-md-4" for="">Article Image:<span class="field_req">*</span></label>
			<div class="col-md-8">
				<div class="input-group">
					<span class="input-group-btn">
						<!-- <span class="btn btn-default btn-files"> -->
							<input type="file" id="profile_images" class="form-control" name="picture" onchange="loadFile(event)">
						<!-- </span> -->
					</span>
<div id="flupmsg"></div>
				</div>
				<div>
				<span id="demo" style="color: red"></span>
				</div>

				<?php if($article->article_image==''){ ?>
				<img id='img-upload' src="<?php echo base_url().'uploads/articles/default_icon.png' ?>">

			<?php }else{ ?>

				<img id='img-upload' src="<?php echo base_url().'uploads/articles/'.$article->article_image; ?>">

			<?php } ?>

				<div class="bnt_sub">
					<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit" onclick="showFileSize()">
				</div>
			</div>
		</div>
	</div>

	

</form>
</div>
</div>
</div>
</div>

<!--Added by 95 on 5-2-2019 For Doctor category for validation-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="assets/admin/js/jquery.validate.js"></script>
<script>
	function loadFile (event) {
		var pcFile = $('input[type=file]').val().split('\\').pop();
		var pcExt     = pcFile.split('.').pop();
		var output = document.getElementById('img-upload');
		if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"
			|| pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){
			$('#img-upload').show();
		output.src = URL.createObjectURL(event.target.files[0]);
		$('#flupmsg').html('');
	}else{
		$('#flupmsg').html('Please select only Image file');
		$('#img-upload').hide();
	}
};
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$.validator.addMethod("regx", function(value, element, regexpr) {          
			return regexpr.test(value);
		}, "Please upload valid extension jpg,jpeg,png file."); 
		$("#edit-article").validate({
			rules: {
				article_title: "required",
				article_description:"required",
				
			},
			messages: {
				article_title: "Please enter article title.",
				article_description:"Please enter article description."
				
			}
		});
	});

	// /*Created by 95 on 6-2-2019 For Image validation*/
	// function showFileSize(){
	// 	var img  = document.getElementById('profile_images').value;

	// 	if(img==='' || img===null){
	// 		document.getElementById("demo").innerHTML="Please choose an image.";
	// 	}else{
	// 		document.getElementById("demo").innerHTML=" ";	
	// 	}
	// }
</script>