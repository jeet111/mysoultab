<style>
	
	
    table#sampleTable th {
        border-bottom: none;
    }
	table#sampleTable {
    table-layout: inherit;
}
.addNote a, .btn.btn-primary {
    margin: 0 0 14px 0 !important;
    padding: 10px 9px !important;
    max-width: 107px !important;
    width: 100% !important;
    border-radius: 50px !important;
}

	.text_heading {
		border-bottom: none !important;
		margin-bottom: 0;
	}
</style>
<div class=" text_heading">

	<!--- Error Message --->
	<?php if ($this->session->flashdata('faild_msg')) { ?>

		<div class="alert alert-danger message" id="faild_msg">
			<button type="button" class="close" data-dismiss="alert">x</button>
			<p align="center"><?php echo $this->session->flashdata('faild_msg'); ?></p>
		</div>

	<?php } ?>
	<h3><i class="fa fa-plus-square-o" aria-hidden="true"></i> Vital Signs</h3>
	<div id="msg_show"></div>
	<?php if (!empty($vital_setting_btn) && $vital_setting_btn != '') { ?>
	<div class="tooltip-2">
		<h2>Display Vital Signs button on Mobile App
			<?php
			
				foreach($vital_setting_btn as $res){ 
					if ($res->settings == 1 || $res->settings == '1') { ?>
						<label class="switch ">
							<input type="checkbox" checked data-btn="<?php echo 'Vital Signs' ?>" class="updateStatus">
							<span class="slider round"></span>
						</label>
	
				<?php } else { ?>
					<label class="switch space">
						<input type="checkbox" data-btn="<?php echo 'Vital Signs' ?>" class="updateStatus">
						<span class="slider round"></span>
					</label>
				<?php } ?>
		<?php }   ?>
		</h2>
	</div>
	<?php } ?>
</div>

<div class="rem_add"><a href="<?php echo base_url() ?>vital_add" class="btn btn-primary">Add Vital</a></div>
</div>

<!-- /# row -->
<div class="row">
	<!-- setting start -->
	<section class="content photo-list">
		<div class="photo-listMain reminder_listMain">
			<table id="sampleTable" id="example" class="table table-hover table-bordered table-mailbox send-user-mail tab_medi">
				<thead style="white-space:nowrap;">
					<tr>
						<th> Test Type</th>
						<th> Reading</th>
						<th> Date Recorded</th>
						<th> Source</th>
						<th> Action</th>
						<!--<th> Share These Vital</th>-->
					</tr>
				</thead>
				<tbody style="white-space:nowrap;">
					<?php
					if (!empty($vitals_array) && $vitals_array != '') {
						foreach ($vitals_array as $k) {
					?>
							<tr>
								<td data-label="Test Type" data-btn="<?php echo $k['test_type']; ?>" id="test_type"><?php echo $k['test_type']; ?></td>
								<td data-label="Reading" data-btn="<?php echo $k['unit']; ?>" id="test_type"><?php echo $k['unit']; ?></td>
								<td data-label="Date Recorded" data-btn="<?php echo $k['date']; ?>" id="date"><?php echo $k['date']; ?></td>
								<td data-label="Source" data-btn="<?php echo $k['doctor']; ?>" id="doctor"><?php echo $k['doctor']; ?></td>
								<td data-label="Action">
									<?php $vital_sign_id = $k['vital_sign_id']; ?>
									<a href="<?php echo base_url() . "vital_edit/{$vital_sign_id}" ?>">
										<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
									</a> &nbsp;&nbsp;&nbsp;

									<a href="javascript:void(0)" onclick="vital_delete('<?php echo $vital_sign_id; ?>');">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</a>
									<input type="hidden" name="vital_sign_id" value="<?php echo $vital_sign_id; ?>">
								</td>
							</tr>
						<?php }
					} else { ?>
						<tr>
							<td align="center" colspan="5">Empty vital available</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
</div>
</div>

<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery-3.5.1.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
        $('#sampleTable').DataTable();
		setTimeout(function() {
			$('#err_msg').fadeOut('fast');
		}, 2500);

		$('#sent_email').click(function() {
			var vital_sign_id = '';
			var vital_sign_id = '<?php //echo $vital_sign_id ?>';
			var url = '<?php echo base_url() . "vital_edit/" ?>';
			window.location.href = url + vital_sign_id;
			return false;
		});
	});


	$('#dashboard_page').click(function() {
		var url = '<?php echo base_url() . "dashboard_new" ?>';
		window.location.href = url;
		return false;
	});

	$('#alerts_page').click(function() {
		var url = '<?php echo base_url() . "alerts" ?>';
		window.location.href = url;
		return false;
	});

	$('#vital_page').click(function() {
		var url = '<?php echo base_url() . "vital_signs" ?>';
		window.location.href = url;
		return false;
	});

	$(document).on('click', '.updateStatus', function() {
		var update_status = $(this).attr('data-btn');
		var update_st = $(this).attr('data-st');
		//alert(update_status+'-'+update_st);
		$.ajax({
			url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
			type: "POST",
			data: {
				'btn': update_status
			},
			dataType: "json",
			success: function(data) {
				$("#msg_show").html("<div class='alert alert-success' id='hide_msg'>" + data.status + "</div>");
				setTimeout(function() {
					$('#hide_msg').fadeOut('fast');
				}, 2500);
			}
		});
	});


	function vital_delete(vital_sign_id) {
		var id = '';
		var id = vital_sign_id;
		var url = "<?php echo base_url() . 'vital_delete/'; ?>";
		var url_mix = url + id;
		$.ajax({
			type: 'POST',
			url: url_mix,
			data: {
				id: id
			},

			success: function(resultData) {
				if (parseInt(resultData) != '') {
					var msg= "Vital deleted suscessfully";
					$("#msg_show").html("<div class='alert alert-success' id='hide_msg'>" + msg + "</div>");
					setTimeout(function() {
						$('#hide_msg').fadeOut('fast');
						location.reload();
					}, 2500);
				}
			},
			error: function(errorData) {}
		});
	}
</script>
