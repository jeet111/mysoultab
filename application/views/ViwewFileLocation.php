<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<!--[if gte mso 9]>
	<xml>
		<o:OfficeDocumentSettings>
		<o:AllowPNG/>
		<o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="format-detection" content="date=no" />
	<meta name="format-detection" content="address=no" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="x-apple-disable-message-reformatting" />
    <!--[if !mso]><!-->
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i" rel="stylesheet" />
	
    <!--<![endif]-->
	<title>Email Template</title>
	
	

	
</head>
<body class="body" style="padding:0 !important;margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#f4f4f4;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f4f4f4" style="overflow: wrap;" autosize="1">
		<tr>
			<td align="center" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="mobile-shell"  style="overflow: wrap;">
					<tr>
						<td class="td container" style="	width:100%; min-width:100%;font-size:0pt; line-height:0pt; margin:0; font-weight:normal; padding:0px 0px;">
							<!-- Header -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0"  style="overflow: wrap;">
								<tr>
									<td class="sec1" style="padding-bottom: 20px;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="p30-15" style="padding: 25px 30px 25px 30px; background: #fff;">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<th class="column-top" width="100%" style="	font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																<table width="100%" border="0" cellspacing="0"  style="overflow: wrap;" cellpadding="0" class="bottom-line" style="border-bottom: 1px solid #000;padding-bottom: 20px;">
																	<tr>
																		<td style="text-align:left;"><img src="https://www.mysoultab.com/assets/images/logo.png" width="100" height="50" border="0" alt="" class="img m-center" style="padding-left: 0px;"/></td>
																	</tr>
																</table>
															</th>
															
														</tr>
												<!--------main body----->
													</table>
													<table width="100%" border="0" cellspacing="0" cellpadding="0"  style="overflow: wrap;">
														<tr>
															<td style="	color:#114490;font-family:'Noto Sans', Arial,sans-serif; font-size:24pt !important; line-height:32pt; text-align:center; padding-bottom:0px;padding-top: 10pt">Welcome to SoulTab</td>
														</tr>
														<tr>
															<td style="color:#000; font-family:'Noto Sans', Arial,sans-serif; font-size:20pt !important; line-height:26pt; text-align:left; padding-bottom:10px;font-weight: 700;">Hello,
															 
															</td>
														</tr>
														<tr>
															<td style="color:#777777; font-family:'Noto Sans', Arial,sans-serif;font-size:14pt; line-height:26pt; text-align:left;padding-bottom:10px;"><?php echo $username;?> uses Soultab smart app to schedule doctor appoitments, we wanted to let you know that our client is trying to schedule a doctor appointment with your office, information below: 															 
															</td>
														</tr>
														<tr>
															<td style="color:#777777; font-family:'Noto Sans', Arial,sans-serif; font-size:14pt; line-height:26pt; text-align:center;padding-bottom: 20px;">Here is your information:
															 
															</td>
														</tr>
													</table>
														<section class="two22 dr" style="min-height: 100px;height: 100px;color: black;padding-top: 0px;padding-left: 0px;float: left;background-color: white;padding-left: 100px; padding-bottom: 30px">
    													<table width="100%" border="0" cellspacing="0" cellpadding="0"  style="overflow: wrap;">
															<tr>
																<td style="color: #000;font-family: 'Noto Sans', Arial,sans-serif;font-size: 14pt;line-height: 26pt;text-align: center;padding-top: 10px;font-weight: 600;">Patient name :  
															 
																<?php echo $username;?>
															 
															 </td>
															</tr>
															<tr>
																<td class="textt pb20" style="color: #000;font-family: 'Noto Sans', Arial,sans-serif;font-size: 14pt;line-height: 26pt;text-align: center;font-weight: 600">Doctor name:  
															 
																<?php echo $doctor_name;?> 
															 
																</td>
															</tr>
															<tr>
																<td class="textt pb20"  style="color: #000;font-family: 'Noto Sans', Arial,sans-serif;font-size: 14pt;line-height: 26pt;text-align: center;font-weight: 600">Date appointment requested  :  
															 
																<?php echo $dr_appointment_date_id;?> 
															 
																</td>
															</tr>
															<tr>
																<td class="textt pb20"  style="color: #000;font-family: 'Noto Sans', Arial,sans-serif;font-size: 14pt;line-height: 26pt;text-align: center;font-weight: 600">Time appointment requested :
															 
																<?php echo $dr_appointment_time_id;?> 
															 
															 </td>
															</tr>
															<tr>
																<td class="textt pb2012" style="color: #000;font-family: 'Noto Sans', Arial,sans-serif;font-size: 14pt;line-height: 26pt;text-align: center;padding-top: 0px;font-weight: 600;padding-bottom: 15px;">Patient telephone number:  
															 
																<?php echo $mobile; ?>   
															 
															 </td>
															</tr>
														</table>
 							 						</section>

  													<table width="100%" border="0" cellspacing="0" cellpadding="0"  style="overflow: wrap;">
														
														
														<tr>
															<td class="text pb202" style="padding-top: 30px;color:#000; font-family:'Noto Sans', Arial,sans-serif; font-size:24pt; line-height:26pt; text-align:left; padding-bottom:10px;">What's next? 
															 
															</td>
														</tr>
														
														<tr>
															<td class="text pb201" style="color:#777777; font-family:'Noto Sans', Arial,sans-serif; font-size:14pt; line-height:26pt; text-align:left; padding-bottom:10px;">Please call %username% at %number% to schedule the appointment</a>
															</td>
														</tr>
													</table>
														
                                            <!------- end main body---------->


                                            <!-----------regards section---->
													<section class="one1" style="width: 20%;min-height: 100px;height: 100px;color: black;padding: 5%;padding-left:0px;float: left;background-color: white;">
    													<table width="100%" border="0" cellspacing="0" cellpadding="0"  style="overflow: wrap;">
															<tr>
																<td class="p30-155" bgcolor="#ffffff" style="padding: 5px 5px ;">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td align="center" valign="top" class="sec2" style="border-collapse: collapse; border-spacing: 0;"><img border="0" vspace="0" hspace="0" class="sec2" src="https://www.mysoultab.com/assets/images/team_two.jpg"alt="D" width="100%" style="padding: 0px; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: 0px solid orange; border-radius:8px; display: block;float:left;color: #000000;">
																			</td>
																		</tr>
											
																	</table>
																</td>
															</tr>
														</table>
 							 						</section>

  													<section class="two2" style="width: 60%;min-height: 100px;height: 100px;color: black;padding: 5%;padding-left: 0px;float: left;background-color: white;">
    							<table width="100%" border="0" cellspacing="0" cellpadding="0"  style="overflow: wrap;">
								<tr>
									<td class="p30-151" bgcolor="#ffffff" style="padding: 70px 0px;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0"  style="overflow: wrap;">
											
											<tr>
												<td class="text-footer1 pb10" style="color: #999999;font-family: 'Noto Sans', Arial,sans-serif;font-size: 16px;line-height: 20px;text-align: left;padding-bottom: 0px;">Best Regards</td>
											</tr>
											<tr>
												<td class="text-footer1 pb10"  style="color: #999999;font-family: 'Noto Sans', Arial,sans-serif;font-size: 16px;line-height: 20px;text-align: left;padding-bottom: 0px;" >ABC</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20" style="color: #999999;font-family: 'Noto Sans', Arial,sans-serif;font-size: 16px;line-height: 20px;text-align: left;padding-bottom: 10px;">Position</td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table>
  							</section>
							<!-- END Article / Title + Copy + Button -->

							
							<!-- Footer -->
							 <section class="one" style="width: 40%;min-height: 200px;height: 200px;color: black;padding: 5%;padding-left: 0px;float: left;background-color: white;">
    							<table width="100%" border="0" cellspacing="0" cellpadding="0"  style="overflow: wrap;">
								<tr>
									<td class="p30-152" bgcolor="#ffffff" style="padding: 0px 0px;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="left" class="sec3" style="padding-bottom: 30px;">
													<table>
														<tr>
															<td class="text12 pb20" style="color: #000;font-family: 'Noto Sans', Arial,sans-serif;font-size: 24px;line-height: 26px;text-align: left;font-weight: 700;">Technical Support</td>
															
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20" style="color: #999999;font-family: 'Noto Sans', Arial,sans-serif;font-size: 16px;line-height: 20px;text-align: left;padding-bottom: 10px;">Mail us at:</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20" style="color: #999999;font-family: 'Noto Sans', Arial,sans-serif;font-size: 16px;line-height: 20px;text-align: left;padding-bottom: 10px;">info@mysoultab.com</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20" style="color: #999999;font-family: 'Noto Sans', Arial,sans-serif;font-size: 16px;line-height: 20px;text-align: left;padding-bottom: 10px;">Web :<a href="#">mysoultab.com</a></td>
											</tr>
											<tr>
												<td class="text-footer1 pb20" style="color: #999999;font-family: 'Noto Sans', Arial,sans-serif;font-size: 16px;line-height: 20px;text-align: left;padding-bottom: 10px;">Tel no : 847 4501055</td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table>
 							 </section>

  							<section class="two" style="width: 40%;min-height: 200px;height: 200px;color: black;padding: 5%;float: left;background-color: white;">
    							<table width="100%" border="0" cellspacing="0" cellpadding="0"  style="overflow: wrap;">
								<tr>
									<td class="p30-152" bgcolor="#ffffff">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="left" class="sec3" style="padding-bottom: 30px;">
													<table>
														<tr>
															<td class="text12 pb20" style="color: #000;font-family: 'Noto Sans', Arial,sans-serif;font-size: 24px;line-height: 26px;text-align: left;font-weight: 700;">Information</td>
															
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20" style="color: #999999;font-family: 'Noto Sans', Arial,sans-serif;font-size: 16px;line-height: 20px;text-align: left;padding-bottom: 10px;">Contact us</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20" style="color: #999999;font-family: 'Noto Sans', Arial,sans-serif;font-size: 16px;line-height: 20px;text-align: left;padding-bottom: 10px;">About us </td>
											</tr>
											<tr>
												<td class="text-footer1 pb20" style="color: #999999;font-family: 'Noto Sans', Arial,sans-serif;font-size: 16px;line-height: 20px;text-align: left;padding-bottom: 10px;">Privacy policy</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20" style="color: #999999;font-family: 'Noto Sans', Arial,sans-serif;font-size: 16px;line-height: 20px;text-align: left;padding-bottom: 10px;">Terms of Use</td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table>
  							</section>


  							<table width="100%" border="0" cellspacing="0" cellpadding="0"  style="overflow: wrap;">
								<tr>
									<td class="p30-152"  bgcolor="#ffffff" style="padding: 0px 0px;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="img" width="55" class="sec4"><a href="#" target="_blank"><img src="images/t8_ico_facebook.jpg" width="38" height="38" border="0" alt="" /></a></td>
															<td class="img" width="55" class="sec4"><a href="#" target="_blank"><img src="images/t8_ico_twitter.jpg" width="38" height="38" border="0" alt="" /></a></td>
															<td class="img" width="55" class="sec4"><a href="#" target="_blank"><img src="images/t8_ico_instagram.jpg" width="38" height="38" border="0" alt="" /></a></td>
															<td class="img" width="38" class="sec4"><a href="#" target="_blank"><img src="images/t8_ico_linkedin.jpg" width="38" height="38" border="0" alt="" /></a></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<!-- END footer -->

						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>




</body>
</html>
 