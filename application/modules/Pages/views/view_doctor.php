<style type="text/css">

.foo {

	float: left;

	width: 10px;

	height: 10px;

	margin: 12px;

	border: 1px solid rgba(0, 0, 0, .2);

}



.new_pos {

	word-break: break-all;

}



.blue {

	background: #42B373;

}



/*.purple {

	background: #ab3fdd;

}



.wine {

	background: #ae163e;

	}*/





</style>







<aside class="right-side">

	<section class="content-header no-margin">

		<h1 class="text-center"><i class="fa fa-user-md" aria-hidden="true"></i> Doctors Detail <div class="back_reminder"><a href="javascript:window.history.back()" class="btn btn-danger">Back</a></div></h1>

	</section>

	<section class="doc_cate">

		<?php if($this->session->flashdata('failed')){ ?>

			<div class="alert alert-danger">

				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

				<?php echo $this->session->flashdata('failed'); ?>

			</div>

		<?php } ?>

		<div class="doc_cateMidd">

			<div class="row">

				<div class="col-sm-4 col-md-4">

					<div class="pro_img">

						<?php if($dc_detail->doctor_pic==''){ ?>

							<img src="<?php echo base_url();?>uploads/doctor/default.png" class="" alt="">

						<?php }else{ ?>

							<img src="<?php echo base_url();?>uploads/doctor/<?php echo $dc_detail->doctor_pic; ?>" class="" alt="">

						<?php } ?>

					</div>

				</div>

				<div class="col-sm-8 col-md-8">

					<div class="bx_drDetail">

						<div class="row">

							<div class="col-md-12">

								<div class="bx_nam">

									<h2 class="tm_member"><?php echo $dc_detail->doctor_name;  ?></h2>

									<h5 class="tm_position new_pos">

										<?php $categories = explode(',',$dc_detail->doctor_category_id);

										$catArray=array();

										foreach ($categories as $category_id) {

											$catArray[]= $this->common_model_new->getCategory1($category_id);

										}

										echo implode(',',$catArray);

										?>

									</h5>

								</div>

							</div>

						</div>

						<form method="get"  id="make_appointment" action="<?php echo base_url();?>view_doctor/<?php echo $dc_detail->doctor_id; ?>">

							<ul class="tm-team-details-list">

								<li>

									<div class="tm-team-list-title"><i class="fa fa-phone"></i> Phone :</div>

									<div class="tm-team-list-value">

										<?php echo $dc_detail->doctor_mob_no; ?>

									</div>

								</li>

								<li>

									<div class="tm-team-list-title"><i class="fa fa-map-marker"></i> Address :</div>

									<div class="tm-team-list-value">

										<?php echo $dc_detail->doctor_address; ?>

									</div>

								</li>

								<li>

									<div class="tm-team-list-title"><i class="fa fa-usd"></i> Fee :</div>

									<div class="tm-team-list-value">

										<?php echo '$'.$dc_detail->doctor_fees; ?>

									</div>

								</li>

								<li>

									<div class="tm-team-list-title"><i class="fa fa-envelope"></i> Email :</div>

									<div class="tm-team-list-value">

										<?php echo $dc_detail->doctor_email; ?>

									</div>

								</li>

								<li>

									<div class="tm-team-list-title"><i class="fa fa-calendar"></i></i>Available Date <span style="color: red;">*</span> :</div>

									<div class="tm-team-list-value dat">

										<div class="bootstrap-timepicker">

											<div class="input-group date" id="datetimepickerhs">

												<input id="datetimepickerh" type="text" name="reminder_date" class="form-control" value="" style="cursor: pointer;" readonly="true">

												<span class="input-group-addon">

													<span class="glyphicon-calendar glyphicon"></span>

												</span>

											</div>

										</div>

										<div class="no-records"></div>

										<div class="foo blue"></div><h6>Available appointmets dates.</h6>



										<?php //if(empty($time_slot)){ ?>

											<!-- <div class="no-records">No date available on this time</div> -->

											<?php //} ?>

										</div>

									</li>

									<li id="slots_avail">

										<div class="newAvail">

											<?php if(!empty($time_slot)){ ?>

												<div class="tm-team-list-title"><i class="fa fa-clock-o"></i> Time<span style="color: red;">*</span>:</div>

												<div class="tm-team-list-value">

													<ul class="list_time">

														<?php if(!empty($class="new_selector")){

															foreach($time_slot as $slot){

																?>

																<li>

																	<input type="radio" class="timeslots" id="f-option_<?php echo $slot->avtime_id; ?>" data-slot="<?php echo $slot->avtime_day_slot; ?>" name="selector" value="<?php echo $slot->avtime_day_slot; ?>">

																	<!-- <input type="hidden" name="doctor_id"> -->

																	<label for="f-option_<?php echo $slot->avtime_id; ?>"><?php echo $slot->avtime_day_slot; ?></label>

																	<div class="check"></div>

																</li>

															<?php }} ?>

														</ul>

													</div>

												<?php } ?>

											</div>

										</li>

										<li >

											<div class="new_selector">

											</div>

										</li>

										<li>

											<div class="tm-team-list-title"><i class="fa fa-bell-o" aria-hidden="true"></i> Reminder :</div>

											<div class="tm-team-list-value">

												<input type="checkbox" name="remind" value="1" checked data-toggle="toggle" data-style="ios">

<!-- <input id="toggle-event" type="checkbox" data-toggle="toggle">

	<div id="console-event"></div> -->

</div>

</li>

<li>

	<div class="tm-team-list-title"></div>

	<div class="tm-team-list-value">

		<div class="bx_apoi">

			<button type="submit" class="btn_apoi" name="submit" id="appointment">

				<i class="fa fa-calendar-o" aria-hidden="true"></i> MAKE AN APPOINTMENT</button>

			</div>

		</div>

	</li>

</ul>

</form>

</div>

</div>

</div>

</section>

</aside>

<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>

<script type="text/javascript">

	$(document).ready(function() {

		$("#make_appointment").validate({

			rules: {

				selector: "required",

				reminder_date: "required",

			},

			messages: {

				selector:"Please select any one time slot.",

				reminder_date:"Please select date.",

			},

		});

	});

</script>

<script>

	jQuery(document).ready(function() {

	//$(function () {

		var SelectedDates = {};

		<?php

		foreach ($doctorDates as $eventDate) {

			$dat = date('m/d/Y',strtotime($eventDate['date']));

			?>SelectedDates[new Date('<?php echo $dat; ?>')] = new Date('<?php echo $dat; ?>').toString();<?php

		}

		?>

		var SeletedText = {};

		<?php

		foreach ($doctorDates as $eventDate) {

			$dat = date('m/d/Y',strtotime($eventDate['date']));

			?>SeletedText[new Date('<?php echo $dat; ?>')] = 'Available';<?php

		}

		?>

		var today = new Date();

		$('#datetimepickerh').datepicker({

			dateFormat: 'yy-mm-dd',

			minDate: 0,

			onSelect: function (date) {

				show_activities(date);

			},

			beforeShowDay: function(date) {

				var Highlight = SelectedDates[date];

				var HighlighText = SeletedText[date];

				if (Highlight) {

					return [true, "Highlighted", HighlighText];

				}

				else {

					return [true, '', ''];

				}

			}

		});

		$('#datetimepickerh').datepicker('setDate', today);

// var eventDates = {};

//         <?php

//         foreach ($doctorDates as $eventDate) {

//             $dat = date('m/d/Y',strtotime($eventDate['date']));

//             ?>eventDates[new Date('<?php echo $dat; ?>')] = new Date('<?php echo $dat; ?>').toString();<?php

//         }

//          ?>

//   var today = new Date();

//   $('#datetimepickerh').datepicker({

//    dateFormat: 'yy-mm-dd',

//       onSelect: function (date) {

// 	     show_activities(date);

//         },

//     beforeShowDay: function(date){

//                 var highlight = eventDates[date];

//                 if( highlight ) {

//                      return [true, "event", highlight];

//                 } else {

//                      return [true, '', ''];

//                 }

//         }

//   });

//   $('#datetimepickerh').datepicker('setDate', today);

});

	function show_activities(date){

		var doctor_id = "<?php echo $this->uri->segment('2'); ?>";

		$.ajax({

			type:'POST',

			url: "<?php echo base_url().'Pages/view_slots'; ?>",

			data: 'date='+date+'&doctor_id='+doctor_id,

			success:function(data){

				if(data == '<div class="no-record">No date available on this time</div>'){

					$(".no-records").show();

					$(".no-records").html('<div class="no-record">No date available on this time</div>');

					$('.newAv').hide();

					$("#slots_avail").hide();

				}else{

					$(".no-records").hide();

					$("#slots_avail").show();

					$("#slots_avail").html(data);

				}

			}

		});

	}

	$(document).on("click",".timeslots",function(){

		var slot_id = $(this).attr('data-slot');

		var doctor_id = "<?php echo $this->uri->segment('2'); ?>";

		var date_id = $("#datetimepickerh").val();

		$.ajax({

			type:'POST',

			url: "<?php echo base_url().'Pages/view_timeslot'; ?>",

			data: 'slot_id='+slot_id+'&doctor_id='+doctor_id+'&date_id='+date_id,

			success:function(data){

				$(".new_selector").html(data);

			}

		});

	});

</script>

<script type="text/javascript">

	jQuery(document).ready(function(){

		var time='<?php echo $time_slot ?>';

		if(time==''){

			$(".no-records").show();

			$(".no-records").html('<div class="no-record">No date available on this time</div>');

			$('.newAv').hide();

			$("#slots_avail").hide();

		}

	});

</script>

<style>

.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }

.toggle.ios .toggle-handle { border-radius: 20px; }

</style>

<link href="<?php echo base_url(); ?>assets/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="<?php echo base_url(); ?>assets/js/bootstrap-toggle.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script src="<?php echo base_url();?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>

<style type="text/css">

.Highlighted a{

	background-color : #42B373 !important;

	background-image :none !important;

	color: White !important;

	font-weight:bold !important;

	font-size: 12pt;

}

</style>