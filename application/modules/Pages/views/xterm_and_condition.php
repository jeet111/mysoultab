
	<!-- Home -->

	<div class="aboutMain">
		<div class="ban_box wow slideInDown animated">
			<div class="container">
				<div class="ben_hed">
					<h2>Term & Condition</h2>
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					  	<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
					    <li class="breadcrumb-item active" aria-current="page">Term & Condtition</li>
					  </ol>
					</nav>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="main">
                 <div class="article" id="intro">
                     
                    <h1>Terms and Conditions Sample Generator</h1>

                    <p>Help protect your website and its users with clear and fair website terms and conditions. These terms and conditions for a website set out key issues such as acceptable use, privacy, cookies, registration and passwords, intellectual property, links to other sites, termination and disclaimers of responsibility. Terms and conditions are used and necessary to protect a website owner from liability of a user relying on the information or the goods provided from the site then suffering a loss.</p>

                    <p>Making your own terms and conditions for your website is hard, not impossible, to do. It can take a few hours to few days for a person with no legal background to make. But worry no more; we are here to help you out.</p>

                    <p>All you need to do is fill up the blank spaces and then you will receive an email with your personalized terms and conditions.</p>

                    <p><strong>Also looking for a GDPR Privacy Policy? Check out <a href="https://gdprprivacynotice.com">gdprprivacynotice.com</a></strong></p>

                    <p><strong>The accuracy of the generated document on this website is not legally binding. Use at your own risk.</strong></p>

                 </div>
             </div>
	    </div>