<section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                         <div class="pull-left image">
                        <?php if($profile_image->profile_image){ ?>
                        <img src="<?php echo base_url(); ?>uploads/profile_images/<?php echo $profile_image->profile_image; ?>" class="img-circle" alt="User Image" />
                        <?php }else{ ?>
                            <img src="<?php echo base_url(); ?>assets/user_dashboard/img/avatar3.png" class="img-circle" alt="User Image" />
                        <?php } ?>
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?php echo $profile_image->name; ?></p>

                            <i class="fa fa-circle text-success"></i> Online
                        </div>
                    </div>
                   <ul class="sidebar-menu">
                        <li <?php if($menuactive == ''){
                            echo 'class="active"';}?>>
                            <a href="<?php echo base_url().'dashboard'; ?>">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="treeview <?php if($menuactive == 'email_list' || $menuactive == 'user_photos' || $menuactive == 'photos_favourite' || $menuactive == 'contact_list' || $menuactive == 'weekly_weather' || $menuactive == 'contact_edit' || $menuactive == 'add_contact'){
                            ?>active <?php } ?>">
                            <a href="#">
                                <i class="fa fa-user-o" aria-hidden="true"></i><span>Personal</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li <?php if($menuactive == 'contact_list'){
                            echo 'class="active"';}?>><a href="<?php echo base_url(); ?>contact_list"><i class="fa fa-angle-double-right"></i> Calls</a></li>
                                <li <?php if($menuactive == 'email_list'){
                            echo 'class="active"';}?>><a href="<?php echo base_url();?>email_list"><i class="fa fa-angle-double-right"></i> Emails</a></li>
                                <li>
                                    <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Photos</span>
                                    <ul class="frd_swtChild">
                                        <li <?php if($menuactive == 'user_photos'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>user_photos"><i class="fa fa-angle-double-right"></i> Photo </a>
                                        </li>
                                        <li <?php if($menuactive == 'photos_favourite'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>photos_favourite"><i class="fa fa-angle-double-right"></i> Favorite Photo</a>
                                        </li>
                                    </ul>
                                </li>   
                                <li <?php if($menuactive == 'weekly_weather'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>weekly_weather"><i class="fa fa-angle-double-right"></i> Weather</a></li>
                                
                            </ul>
                        </li>
                        
                        <li class="treeview <?php if($menuactive == 'doctor_speciality' || $menuactive == 'reminder_list' || $menuactive == 'show_activities' || $menuactive == 'medicine_list' || $menuactive == 'appointment_list' || $menuactive == 'testreport_list' || $menuactive == 'acknowledge_list' || $menuactive == 'acknowledge_detail' || $menuactive == 'edit_acknowledge' || $menuactive == 'add_acknowledge' || $menuactive == 'add_testreport' || $menuactive == 'edit_testreport' || $menuactive == 'edit_appointment' || $menuactive == 'edit_shedule' || $menuactive == 'add_medicine' || $menuactive == 'add_reminder' || $menuactive == 'edit_reminder' || $menuactive == 'doctors' || $menuactive == 'view_doctor' || $menuactive == 'thank_you' || $menuactive == 'pickupmedicine_list' || $menuactive == 'add_pickup_medicine' || $menuactive == 'nearby_restaurant' || $menuactive == 'favorite_restaurant' || $menuactive == 'view_shedule' || $menuactive == 'view_testreport' || $menuactive == 'send_testreport' || $menuactive == 'view_appointment'  || $menuactive == 'favorite_banks' || $menuactive == 'all_banks' || $menuactive == 'grocery' || $menuactive == 'favorite_grocery'  || $menuactive == 'add_note' || $menuactive == 'note_list' || $menuactive == 'edit_note' || $menuactive == 'view_note' ||$menuactive == 'view_bank' ||$menuactive == 'view_favorite_bank'|| $menuactive == 'view_restaurant' || $menuactive == 'view_favorite_restaurant'){
                            ?>active <?php } ?>">
                            <a href="#">
                                <i class="fa fa-bell-o" aria-hidden="true"></i> <span>Concierge</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li <?php if($menuactive == 'doctor_speciality' || $menuactive == 'doctors' || $menuactive == 'view_doctor' || $menuactive == 'thank_you'){
                            echo 'class="active"';}?>><a href="<?php echo base_url(); ?>doctor_speciality"><i class="fa fa-angle-double-right"></i> Doctor Specialties</a></li>
                            
                            <li <?php if($menuactive == 'appointment_list' || $menuactive == 'edit_appointment' || $menuactive == 'view_appointment'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>appointment_list"><i class="fa fa-angle-double-right"></i> Appointments</a></li>
                            
                            <li <?php if($menuactive == 'medicine_list' || $menuactive == 'edit_shedule' || $menuactive == 'add_medicine' || $menuactive == 'view_shedule'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>medicine_list"><i class="fa fa-angle-double-right"></i> Medicine Schedule</a></li>
                            
                            <li <?php if($menuactive == 'testreport_list' ||$menuactive == 'add_testreport' || $menuactive == 'edit_testreport' || $menuactive == 'view_testreport' || $menuactive == 'send_testreport'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>testreport_list"><i class="fa fa-angle-double-right"></i> Test Report List</a></li>
                            
                            <li <?php if($menuactive == 'acknowledge_list' || $menuactive == 'acknowledge_detail' || $menuactive == 'edit_acknowledge' || $menuactive == 'add_acknowledge'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>acknowledge_list"><i class="fa fa-angle-double-right"></i> Acknowledge List</a></li>
                            
                                <li <?php if($menuactive == 'reminder_list' || $menuactive == 'add_reminder' || $menuactive == 'edit_reminder'){
                            echo 'class="active"';}?>><a href="<?php echo base_url();?>reminder_list"><i class="fa fa-angle-double-right"></i> Reminder</a></li>
                                   
                                <li <?php if($menuactive == 'show_activities'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>show_activities"><i class="fa fa-angle-double-right"></i> Show Activities</a></li>
                                <li <?php if($menuactive == 'pickupmedicine_list' || $menuactive == 'add_pickup_medicine'){ echo 'class="active"';}?>><a href="<?php echo base_url(); ?>pickupmedicine_list"><i class="fa fa-angle-double-right"></i> Pickup medicine</a></li>
                                
                             <li>
                             <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Restaurants</span>
                                    <ul class="frd_swtChild">
                                        <li <?php if($menuactive == 'nearby_restaurant'|| $menuactive == 'view_restaurant'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url(); ?>nearby_restaurant"><i class="fa fa-angle-double-right"></i> Restaurant List </a>
                                        </li>
                                        <li <?php if($menuactive == 'favorite_restaurant'|| $menuactive == 'view_favorite_restaurant'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>favorite_restaurant"><i class="fa fa-angle-double-right"></i> Favorite Restaurant</a>
                                        </li>
                                    </ul>
                                </li>   
                                
                                
                                <li>
                             <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Banks</span>
                                    <ul class="frd_swtChild">
                                        <li <?php if($menuactive == 'all_banks' ||$menuactive == 'view_bank'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url(); ?>all_banks"><i class="fa fa-angle-double-right"></i> Bank List </a>
                                        </li>
                                        <li <?php if($menuactive == 'favorite_banks' ||$menuactive == 'view_favorite_bank'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>favorite_banks"><i class="fa fa-angle-double-right"></i> Favourite Banks</a>
                                        </li>
                                    </ul>
                                </li>   
                                
                                 <li>
                             <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Grocery</span>
                                    <ul class="frd_swtChild">
                                        <li <?php if($menuactive == 'grocery'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url(); ?>grocery"><i class="fa fa-angle-double-right"></i> Grocery List </a>
                                        </li>
                                        <li <?php if($menuactive == 'favorite_grocery'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>favorite_grocery"><i class="fa fa-angle-double-right"></i> Favorite Grocery</a>
                                        </li>
                                    </ul>
                                </li>
                                
                                <li <?php if($menuactive == 'note_list' || $menuactive == 'add_note' || $menuactive == 'edit_note' || $menuactive == 'view_note'){
                            echo 'class="active"';}?>><a href="<?php echo base_url();?>note_list"><i class="fa fa-angle-double-right"></i> Note</a></li>                                
     
                                
                            </ul>
                        </li>
                        
                        
                        <li class="treeview <?php if($menuactive == 'articles' || $menuactive == 'music_list' || $menuactive == 'movie_list' || $menuactive == 'music_detail' || $menuactive == 'movie_detail' || $menuactive == 'detail_article' || $menuactive == 'favoritemusic_list' || $menuactive == 'favorite_articles' || $menuactive == 'favoritemovie_list' || $menuactive == 'favoritemovie_detail_page' ||$menuactive == 'mymusic_list'||$menuactive =='mymusic_detail'){
                            ?>active <?php } ?>">
                            <a href="#">
                                <i class="fa fa-star-o" aria-hidden="true"></i>  <span>Entertainment</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li>
                             <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Article</span>
                                    <ul class="frd_swtChild">
                                        <li <?php if($menuactive == 'articles' || $menuactive == 'detail_article'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>articles"><i class="fa fa-angle-double-right"></i> Article List </a>
                                        </li>
                                        <li <?php if($menuactive == 'favorite_articles'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>favorite_articles"><i class="fa fa-angle-double-right"></i> Favorite Article</a>
                                        </li>
                                    </ul>
                                </li>
                                
                            
                             <li>
                             <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Music</span>
                                    <ul class="frd_swtChild">
                                        <li <?php if($menuactive == 'music_list'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>music_list"><i class="fa fa-angle-double-right"></i> Music List </a>
                                        </li>
                                        <li <?php if($menuactive == 'favoritemusic_list'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>favoritemusic_list"><i class="fa fa-angle-double-right"></i> Favorite Music</a>
                                        </li>

                                        <li <?php if($menuactive == 'mymusic_list'||$menuactive =='mymusic_detail'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>mymusic_list"><i class="fa fa-angle-double-right"></i> My Music</a>
                                        </li>
                                    </ul>
                                </li>  
                                 
                                <li>
                             <span class="submenu-cls"><i class="fa fa-angle-double-right"></i> Movie</span>
                                    <ul class="frd_swtChild">
                                        <li <?php if($menuactive == 'movie_list' || $menuactive == 'movie_detail'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>movie_list"><i class="fa fa-angle-double-right"></i> Movie List </a>
                                        </li>
                                        <li <?php if($menuactive == 'favoritemovie_list'  || $menuactive == 'favoritemovie_detail_page'){ echo 'class="active"';}?>>
                                            <a href="<?php echo base_url();?>favoritemovie_list"><i class="fa fa-angle-double-right"></i> Favorite Movie</a>
                                        </li>
                                    </ul>
                                </li>
                                
                            </ul>
                        </li>
                        
                    </ul>
                </section> 