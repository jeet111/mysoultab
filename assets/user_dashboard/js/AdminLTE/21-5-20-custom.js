

$(function() { 
$('#datetimepicker2').datetimepicker({
  format: 'h:mm A', // correctly shows time picker in 12 hour format
  sideBySide: true
});
var today = new Date();
 $('#datetimepicker8').datetimepicker({
                   format: 'YYYY-MM-DD',
				   minDate: today
                });


    $('#datetimepicker3').datetimepicker({
     // pickDate: false,
	  format: 'h:mm A',
  sideBySide: true
    });
  
   $('#datetimepicker51').datetimepicker({
   // pickDate: false
      endDate: '+0d',	  
	  startDate: today
   });
   
   $("img.lazy").lazyload();
  });
  
    jQuery('#checkall').on('click', function(e) {
        if($(this).is(':checked',true))  
        {
            $(".reminder_chk").prop('checked', true);  
        }  
        else  
        {  
            $(".reminder_chk").prop('checked',false);  
        }  
    });
    jQuery('.reminder_chk').on('click',function(){
        if($('.reminder_chk:checked').length == $('.reminder_chk').length){
            $('#checkall').prop('checked',true);
        }else{
            $('#checkall').prop('checked',false);
        }
    });
	
	$(document).on('click','.reminder_all_del', function(e){
		
		
		var data_base = $(this).attr('data-base');
		
		
            var bodytype = $('input[name="reminder_chk[]"]:checked').map(function () {  
        return this.value;
        }).get().join(",");
		
		if(bodytype != ''){
	if(confirm("Are you sure you want to remove this reminder?")){	
		$.ajax({
                url: data_base+'Reminder/reminder_all_del',
                type: 'POST',
                data: {bodytype:bodytype},
				dataType: "json",
                success: function(data) { //alert('swati');
                    
					
					 setTimeout(function(){
            $("#expire_msg").html("<div class='alert alert-success message'>Successfully deleted !</div>");
            location.reload();
//window.location.href = email_url+"email_list";
}, 1000);
                }
            });
			
			}
		}else{
			alert("Please select atleast one reminder.");
		}
  });  
    

  /* Created by 95 on 27-2-2019 for medicine shedule multiple delete */
  jQuery('#checkallmedicine').on('click', function(e) {
        if($(this).is(':checked',true))  
        {
            $(".medicine_chk").prop('checked', true);  
        }  
        else  
        {  
            $(".medicine_chk").prop('checked',false);  
        }  
    });
    jQuery('.medicine_chk').on('click',function(){
        if($('.medicine_chk:checked').length == $('.medicine_chk').length){
            $('#checkallmedicine').prop('checked',true);
        }else{
            $('#checkallmedicine').prop('checked',false);
        }
    });
  
  $(document).on('click','.medicine_all_del', function(e){
    
    
    var data_base = $(this).attr('data-base');
    
      var bodytype = $('input[name="medicine_chk[]"]:checked').map(function () {  
        return this.value;

        }).get().join(",");
    
    if(bodytype != ''){
  if(confirm("Are you sure you want to remove this medicine schedule?")){  
    $.ajax({
                url: data_base+'Pages/Medicine/medicine_all_del',
                type: 'POST',
                data: {bodytype:bodytype},
        dataType: "json",
                success: function(data) { 
                  console.log(data);
                    
            
           setTimeout(function(){
            $("#expire_msg").html("<div class='alert alert-success'>Successfully deleted !</div>");
            location.reload();
//window.location.href = email_url+"email_list";
}, 1000);
                }
            });
      
      }
    }else{
      alert("Please select atleast one medicine schedule.");
    }
  });



  /* Created by 95 for appointment delete */

  jQuery('#checkallappointment').on('click', function(e) {
        if($(this).is(':checked',true))  
        {
            $(".appointment_chk").prop('checked', true);  
        }  
        else  
        {  
            $(".appointment_chk").prop('checked',false);  
        }  
    });
    jQuery('.appointment_chk').on('click',function(){
        if($('.appointment_chk:checked').length == $('.appointment_chk').length){
            $('#checkallappointment').prop('checked',true);
        }else{
            $('#checkallappointment').prop('checked',false);
        }
    });
  
  $(document).on('click','.appointment_all_del', function(e){
    
    
    var data_base = $(this).attr('data-base');
    //alert(data_base);
    
            var bodytype = $('input[name="appointment_chk[]"]:checked').map(function () {  

              //alert(bodytype);
        return this.value;



        //alert(this.value);

        }).get().join(",");
    
    if(bodytype != ''){
  if(confirm("Are you sure you want to remove this appointment?")){  
    $.ajax({
                url: data_base+'Appointment/appointments_all_del',


                type: 'POST',
                data: {bodytype:bodytype},
        dataType: "json",
                success: function(data) { //alert('swati');
                    
          
           setTimeout(function(){
            $("#expire_msg").html("<div class='alert alert-success message'>Successfully deleted !</div>");
            location.reload();
//window.location.href = email_url+"email_list";
}, 1000);
                }
            });
      
      }
    }else{
      alert("Please select atleast one appointment.");
    }
  }); 



  /* Created by 95 for test report delete */

  jQuery('#checkallTestReports').on('click', function(e) {
        if($(this).is(':checked',true))  
        {
            $(".testreport_chk").prop('checked', true);  
        }  
        else  
        {  
            $(".testreport_chk").prop('checked',false);  
        }  
    });
    jQuery('.testreport_chk').on('click',function(){
        if($('.testreport_chk:checked').length == $('.testreport_chk').length){
            $('#checkallTestReports').prop('checked',true);
        }else{
            $('#checkallTestReports').prop('checked',false);
        }
    });
  
  $(document).on('click','.testreport_all_del', function(e){
    
    
    var data_base = $(this).attr('data-base');
    //alert(data_base);
    
            var bodytype = $('input[name="testreport_chk[]"]:checked').map(function () {  

              //alert(bodytype);
        return this.value;



        //alert(this.value);

        }).get().join(",");
    
    if(bodytype != ''){

      

  if(confirm("Are you sure you want to remove this test report?")){  
    $.ajax({
                url: data_base+'Pages/Testreport/testreports_all_del',


                type: 'POST',
                data: {bodytype:bodytype},
                dataType: "json",
                success: function(data) { //alert('swati');
                    
          
           setTimeout(function(){
            $("#expire_msg").html("<div class='alert alert-success message'>Successfully deleted !</div>");
            location.reload();
//window.location.href = email_url+"email_list";
}, 1000);
                }
            });
      
      }
    }else{
      alert("Please select atleast one test report.");
    }
  });



  /* Created by 95 for contact delete */

  jQuery('#checkallContacts').on('click', function(e) {
        if($(this).is(':checked',true))  
        {
            $(".contact_chk").prop('checked', true);  
        }  
        else  
        {  
            $(".contact_chk").prop('checked',false);  
        }  
    });
    jQuery('.contact_chk').on('click',function(){
        if($('.contact_chk:checked').length == $('.contact_chk').length){
            $('#checkallContacts').prop('checked',true);
        }else{
            $('#checkallContacts').prop('checked',false);
        }
    });
  
  $(document).on('click','.contact_all_del', function(e){
    
    
    var data_base = $(this).attr('data-base');
    //alert(data_base);
    
            var bodytype = $('input[name="contact_chk[]"]:checked').map(function () {  

              //alert(bodytype);
        return this.value;



        //alert(this.value);

        }).get().join(",");
    
    if(bodytype != ''){

      

  if(confirm("Are you sure you want to remove this test report?")){  
    $.ajax({
                url: data_base+'Pages/UserContact/contact_all_del',


                type: 'POST',
                data: {bodytype:bodytype},
                dataType: "json",
                success: function(data) { //alert('swati');
                    
          
           setTimeout(function(){
            $("#expire_msg").html("<div class='alert alert-success message'>Successfully deleted !</div>");
            location.reload();
//window.location.href = email_url+"email_list";
}, 1000);
                }
            });
      
      }
    }else{
      alert("Please select atleast one contact.");
    }
  });