(function ($) {
    "use strict";

    /*  Sales Chart
    --------------------*/

    var sales = {
        type: 'line',
        data: {
            labels: ["1w", "2w", "3w", "4w", "5w", "6w"],
            type: 'line',
            defaultFontFamily: 'Montserrat',
            datasets: [{
                label: "Medicine",
                data: [0, 42, 23, 14, 25, 15, 140],
                backgroundColor: 'transparent',
                borderColor: '#e6a1f2',
                borderWidth: 3,
                pointStyle: 'circle',
                pointRadius: 1,
                pointBorderColor: 'transparent',
                pointBackgroundColor: '#e6a1f2',

                    }, {
                label: "Yoga",
                data: [0, 30, 10, 60, 80, 63, 10],
                backgroundColor: 'transparent',
                borderColor: '#ed7f7e',
                borderWidth: 3,
                pointStyle: 'circle',
                pointRadius: 1,
                pointBorderColor: 'transparent',
                pointBackgroundColor: '#ed7f7e',
                    }, {
                label: "Spirituality",
                data: [0, 50, 40, 20, 40, 79, 20],
                backgroundColor: 'transparent',
                borderColor: '#87de75',
                borderWidth: 3,
                pointStyle: 'circle',
                pointRadius: 1,
                pointBorderColor: 'transparent',
                pointBackgroundColor: '#87de75',
                    }]
        },
        options: {
            responsive: true,

            tooltips: {
                mode: 'index',
                titleFontSize: 12,
                titleFontColor: '#000',
                bodyFontColor: '#000',
                backgroundColor: '#fff',
                titleFontFamily: 'Montserrat',
                bodyFontFamily: 'Montserrat',
                cornerRadius: 3,
                intersect: false,
            },
            legend: {
                labels: {
                    usePointStyle: true,
                    fontFamily: 'Montserrat',
                },
            },
            scales: {
                xAxes: [{
                    display: true,
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Week'
                    }
                        }],
                yAxes: [{
                    display: true,
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                        }]
            },
            title: {
                display: false,
                text: 'Normal Legend'
            }
        }
    };


    


            



   




    window.onload = function () {
        var ctx = document.getElementById("sales-chart").getContext("2d");
        window.myLine = new Chart(ctx, sales);

        
    }; 
    
})(jQuery);









