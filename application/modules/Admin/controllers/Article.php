  <?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  class Article extends MX_Controller {
    private $staff_permission = array();
    private $access_permission = true;
    public function __construct(){   
      parent:: __construct();
      $this->load->library('session');
      $this->load->library('form_validation');
      $this->load->model('article_model');
      $this->load->model('login_model');
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));
      $user_id = $this->session->userdata('logged_ins')['id'];
      if(empty($user_id)) {
        redirect('admin/login');
      }

      if ($this->session->userdata('logged_ins')['user_role'] == 5) {
        $uid = $this->session->userdata('logged_ins')['id'];
        $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
        if (!empty($permission_list)) 
        {
           foreach ($permission_list as $k => $v) 
           {
              $this->staff_permission[$v['permission_id']] = $v;
           }
        }
      }
    }

    public function permission_access($permission_id=13)
    {
      if ($this->session->userdata('logged_ins')['user_role'] != 1) 
      {
         if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
         {
            $this->access_permission = false;
         }
      }
    }

    /*Created by 95 for show all articles*/
    public function articles_list()
    {
       $data['menuactive'] = $this->uri->segment(2);
     $data['ArticlesData'] = $this->article_model->getAllorderby('cp_articles','article_id','desc');
     
     $data['staff_permission'] = $this->staff_permission;
     
     $this->permission_access(13);
     if ($this->access_permission) {
        $this->template->set('title', 'Articles');
        $this->template->load('admin_dashboard_layout', 'contents' , 'artilcles_list', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }
   /*Created by 95 for delete article*/
   public function delete_article($article_id='')
   {
    $this->article_model->delete('cp_articles',array('article_id' =>$article_id));
    $this->session->set_flashdata('success', 'Deleted successfully.');
    redirect('admin/articles_list');
  }
  /*Created by 95 for add article*/
  public function add_article()
  {
 $data['menuactive'] = $this->uri->segment(2);
  $this->form_validation->set_rules('article_title','Article Title','required');
   
    $this->form_validation->set_rules('article_description','Article Description','required');
    
    
if ($this->form_validation->run() == TRUE) 
     {  


    if($_POST['submit']){
              //Check whether user upload picture
      if(!empty($_FILES['picture']['name'])){
        $config['upload_path'] = 'uploads/articles/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['picture']['name'];
                  //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('picture')){
          $uploadData = $this->upload->data();
          $picture = $uploadData['file_name'];

          /*For image resize*/
          $dirpath = $uploadData['full_path'];

           $this->load->library('image_lib');
          $configer =  array(
              'image_library'   => 'gd2',
              'source_image'    =>  $dirpath,
              'maintain_ratio'  =>  TRUE,
              'width'           =>  205,
              'height'          =>  205,
            );
            $this->image_lib->clear();
            $this->image_lib->initialize($configer);
            $this->image_lib->resize();


        }else{
          $picture = '';
        }
      }else{
        $picture = '';
      }
	  
	  if(!empty($_FILES['artist_image']['name'])){ 
        $config['upload_path'] = 'uploads/articles/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
       
                  //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('artist_image')){ 
           $attachment_data = array('upload_data' => $this->upload->data());
           $uploadfile = $attachment_data['upload_data']['file_name'];

        }else{
          $uploadfile = '';
        }
      }else{
        $uploadfile = '';
      }
	  
	  if(!empty($_FILES['article_icon']['name'])){ 
        $config['upload_path'] = 'uploads/articles/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
       
                  //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('article_icon')){ 
           $attachment_data = array('upload_data' => $this->upload->data());
           $uploadfile_icon = $attachment_data['upload_data']['file_name'];

        }else{
          $uploadfile_icon = '';
        }
      }else{
        $uploadfile_icon = '';
      }
	  
	  $data = array(
    'article_title' => $this->input->post('article_title'),
    'article_desc' => $this->input->post('article_description'),
    'article_image' => $picture,
		'artist_image' => $uploadfile,
		'article_icon' => $uploadfile_icon,
		'artist_name' => $this->input->post('artist_name'),
    'user_id'=>$this->session->userdata('logged_ins')['id'],
    'category_id'=>$this->input->post('article_type'),
    'article_created' => date('Y-m-d h:i:s'),

      );
      $this->article_model->insertData('cp_articles',$data);
      $this->session->set_flashdata('success', 'Article Added Successfully.');
      redirect('admin/articles_list');
    }


  } 
  $data['categorys']= $this->article_model->getAllwhere("cp_article_category",array("article_category_status" =>1));

    $data['staff_permission'] = $this->staff_permission;
    
     $this->permission_access(13);
     if ($this->access_permission) {
        $this->template->set('title', 'Articles');
        $this->template->load('admin_dashboard_layout', 'contents' , 'add_article',$data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
  }
  public function ajax_article_add()
{
	if ($_FILES["artist_image"]["error"] > 0 && $_FILES["picture"]["error"] > 0)
  { 
   //$return = array('code' => 2,'message' => 'File loading error!');
  }else{ 

           $config1['upload_path'] = 'uploads/articles/';
		   $config1['allowed_types'] = 'jpg|jpeg|png|gif';
           
           //$data['max_size'] = '5000';
            $config1['encrypt_name'] = true;

            $this->load->library('upload', $config1);

            if ($this->upload->do_upload('artist_image')) {  
                $attachment_data = array('upload_data' => $this->upload->data());
                $uploadfile = $attachment_data['upload_data']['file_name'];
			// $return = array('code' => 1,'message' => 'Successfully added');	
		 }else{ 
			$return = array('code' => 2,'message' => ''/*strip_tags($this->upload->display_errors())*/);
		 }
		 
		 
		 $config2['upload_path'] = 'uploads/articles/';
         $config2['allowed_types'] = 'jpg|jpeg|png|gif';
           //$data['max_size'] = '5000';
         $config2['encrypt_name'] = true;

         $this->upload->initialize($config2);


          if ($this->upload->do_upload('picture')) {
          $attachment_data_image = array('upload_data' => $this->upload->data());
           $picturefile = $attachment_data_image['upload_data']['file_name'];
      //  $return = array('code' => 1,'message' => 'Successfully added');	


            /*For image resize*/
           $this->load->library('image_lib');
          $configer =  array(
              'image_library'   => 'gd2',
              'source_image'    =>  $attachment_data_image['upload_data']['full_path'],
              'maintain_ratio'  =>  TRUE,
              'width'           =>  150,
              'height'          =>  150,
            );
            $this->image_lib->clear();
            $this->image_lib->initialize($configer);
            $this->image_lib->resize();


		 }else{ 
			$return = array('code' => 2,'message' => ''/*strip_tags($this->upload->display_errors())*/);
			
		 }
		 
		
		 if($return['code'] != 2){ 
		 $array = array(
	      'article_title' => $this->input->post('article_title'),
          'article_desc' => $this->input->post('article_description'),
          'artist_image' =>$uploadfile,
          'article_image' => $picturefile,		  
          'artist_name' => $this->input->post('artist_name'),	  
		 // 'user_id'=>$this->session->userdata('logged_in')['id'],
		  'category_id' =>$this->input->post('article_type'),          
    'article_created' => date('Y-m-d h:i:s'),
	   );
	   $this->article_model->insertData("cp_articles",$array);
	   
	 
				
$return = array('code' => 1,'message' => 'Successfully added');
		 }
	}
	echo json_encode($return);
}
  
  /*Created by 95 for edit article*/
  public function edit_article($article_id='')
  {
     $data['menuactive'] = $this->uri->segment(2);
    $this->form_validation->set_rules('article_title','Article Title','required');
   
    $this->form_validation->set_rules('article_description','Article Description','required');
    
    $data['article']=$this->article_model->getsingle('cp_articles',array('article_id'=>$article_id));
	
	$data['articleid'] = $article_id;
	
if ($this->form_validation->run() == TRUE) 
     {  


    if($_POST['submit']){
              //Check whether user upload picture
      if(!empty($_FILES['picture']['name'])){
        $config['upload_path'] = 'uploads/articles/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['picture']['name'];
                  //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('picture')){
          $uploadData = $this->upload->data();
          $picture = $uploadData['file_name'];


          /*For image resize*/
          $dirpath = $uploadData['full_path'];

           $this->load->library('image_lib');
          $configer =  array(
              'image_library'   => 'gd2',
              'source_image'    =>  $dirpath,
              'maintain_ratio'  =>  TRUE,
              'width'           =>  205,
              'height'          =>  205,
            );
            $this->image_lib->clear();
            $this->image_lib->initialize($configer);
            $this->image_lib->resize();



        }else{
          $picture = $data['article']->article_image;
        }
	  }else{
          $picture = $data['article']->article_image;
        }
	  
	  if(!empty($_FILES['artist_image']['name'])){ 
        $config['upload_path'] = 'uploads/articles/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
       
                  //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('artist_image')){ 
           $attachment_data = array('upload_data' => $this->upload->data());
           $uploadfile = $attachment_data['upload_data']['file_name'];

        }else{
          $uploadfile = '';
        }
      }else{
        $uploadfile = $data['article']->artist_image;
      }
	  
	  if(!empty($_FILES['article_icon']['name'])){ 
        $config['upload_path'] = 'uploads/articles/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
       
                  //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('article_icon')){ 
           $attachment_data = array('upload_data' => $this->upload->data());
           $uploadfile_icon = $attachment_data['upload_data']['file_name'];

        }else{
          $uploadfile_icon = '';
        }
      }else{
        $uploadfile_icon = $data['article']->article_icon;
      }

        $data = array(
          'article_title' => $this->input->post('article_title'),
          'article_desc' => $this->input->post('article_description'),
          'article_image' => $picture,
		  'artist_image' => $uploadfile,
		  'article_icon' => $uploadfile_icon,
		  'artist_name' => $this->input->post('artist_name'),
      'user_id'=>$this->session->userdata('logged_ins')['id'],     
      'category_id'=>$this->input->post('article_type'),
          'article_created' =>date('Y-m-d H:i:s')
        );
        $this->article_model->updateData('cp_articles',$data,array('article_id'=>$article_id));
      
      
      $this->article_model->updateData('cp_articles',$data,array('article_id'=>$article_id));
      $this->session->set_flashdata('success', 'Article Updated Successfully.');
      redirect('admin/articles_list');
    }

  }  
    $data['categorys']= $this->article_model->getAllwhere("cp_article_category",array("article_category_status" =>1));
    
    $data['staff_permission'] = $this->staff_permission;
    
     $this->permission_access(13);
     if ($this->access_permission) {
        $this->template->set('title', 'Articles');
        $this->template->load('admin_dashboard_layout', 'contents', 'edit_article',$data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
  }
  
   public function ajax_article_edit()
  {
	  $article_id = $this->input->post('article_id');
		$checkLogin = $this->session->userdata('logged_in');
		
		$singleData = $this->article_model->getsingle("cp_articles",array("article_id" => $article_id));
		

           $config1['upload_path'] = 'uploads/articles/';
		   $config1['allowed_types'] = 'jpg|png|gif|jpeg';
           
           //$data['max_size'] = '5000';
            $config1['encrypt_name'] = true;

            $this->load->library('upload', $config1);

          if(!empty($_FILES["artist_image"]["name"])){ 
            if ($this->upload->do_upload('artist_image')) { 
                $attachment_data = array('upload_data' => $this->upload->data());
                $uploadfile = $attachment_data['upload_data']['file_name'];
			// $return = array('code' => 1,'message' => 'Successfully added');	
		 }else{ 
			$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
		 }
		  }
		 
		 $config2['upload_path'] = 'uploads/articles/';
         $config2['allowed_types'] = 'jpg|png|gif|jpeg';
           //$data['max_size'] = '5000';
         $config2['encrypt_name'] = true;

         $this->upload->initialize($config2);

        if(!empty($_FILES["profile_images"]["name"])){ 
          if ($this->upload->do_upload('profile_images')) {
          $attachment_data_image = array('upload_data' => $this->upload->data());
           $picturefile = $attachment_data_image['upload_data']['file_name'];
      //  $return = array('code' => 1,'message' => 'Successfully added');	

           
           /*For image resize*/
           $this->load->library('image_lib');
        	$configer =  array(
              'image_library'   => 'gd2',
              'source_image'    =>  $attachment_data_image['upload_data']['full_path'],
              'maintain_ratio'  =>  TRUE,
              'width'           =>  150,
              'height'          =>  150,
            );
            $this->image_lib->clear();
            $this->image_lib->initialize($configer);
            $this->image_lib->resize();

		 }else{ 
			$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
			
		 }
		}
		 //print_r($uploadfile);die;
		 if(!empty($picturefile)){ 
			 $picfile = $picturefile;
		 }else{ 
			 $picfile = $singleData->article_image;
		 }
		 
		 if(!empty($uploadfile)){ 
			 $upfile = $uploadfile;
		 }else{
			 $upfile = $singleData->artist_image;
		 }
		 
		if(!empty($upfile) && !empty($picfile)){
		 if($return['code'] != 2){ 
		 $array = array(
	      'article_title' => $this->input->post('article_title'),
          'article_desc' => $this->input->post('article_description'),
          'article_image' => $picfile,
          'artist_image' => $upfile,
          'artist_name' => $this->input->post('artist_name'),		 
		 // 'user_id' => $this->session->userdata('logged_in')['id'], 
		  'category_id' => $this->input->post('article_type'),
          // 'music_image' => $pictureFile,
        'article_created' =>date('Y-m-d H:i:s')
	   );
	   
	   $this->article_model->updateData('cp_articles',$array,array('article_id'=>$article_id));
				
$return = array('code' => 1,'message' => 'Successfully updated');
		 }
		}
	
	
	echo json_encode($return);
  }


  public function view_article($article_id='')
    {
       $data['menuactive'] = $this->uri->segment(2);

     $data['viewArticle']=$this->article_model->ToView($article_id);  
     // $data['viewArticle']=$this->article_model->getsingle('cp_articles',array('article_id'=>$article_id));
     
     $data['staff_permission'] = $this->staff_permission;
     
     $this->permission_access(13);
     if ($this->access_permission) {
        $this->template->set('title', 'Articles');
        $this->template->load('admin_dashboard_layout', 'contents', 'view_article', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }

   /*======Category Section Start=======*/

  /*Add article */
  public function add_article_category()
  {
   $data['menuactive'] = $this->uri->segment(2);
   $unique_id = substr(number_format(time() * rand(),0,'',''),0,4);
   $this->form_validation->set_rules('category_name','Category Name','required');

   $data['staff_permission'] = $this->staff_permission;
   
     $this->permission_access(13);
     if ($this->access_permission) {
        $this->template->set('title', 'Articles');
        $this->template->load('admin_dashboard_layout', 'contents', 'add_article_category', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
  }


  /*Add article ajax*/  
  public function ajax_article_category()
  {
   if ($_FILES["category_icon"]["error"] > 0)
   {
     $return = array('code' => 2,'message' => 'File loading error!');
   }else{
     $data['upload_path'] = 'uploads/articles/category_icon/';
     $data['allowed_types'] = 'jpg|png|gif|jpeg';    
     $data['max_size'] = '5000';
     $data['encrypt_name'] = true;
     $this->load->library('upload', $data);
     if ($this->upload->do_upload('category_icon')) {
      $attachment_data = array('upload_data' => $this->upload->data());
      $uploadfile = $attachment_data['upload_data']['file_name'];
      $array = array(
        'article_category_name' => $_POST['category_name'],
        'article_category_icon' => $uploadfile,
        'article_category_desc' => $_POST['category_desc'],
        'article_category_status' => 1,
        'article_category_create_date' => date("Y-m-d H:i:s")
      );
      $this->article_model->insertData("cp_article_category",$array);
      $return = array('code' => 1,'message' => 'Successfully added');
    }else{
      $return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
    }
  }
  echo json_encode($return);
  }


  /*Article list*/
  public function article_category_list() 
  {
   $data['menuactive'] = $this->uri->segment(2);
   $data['category_list'] = $this->article_model->getAllorderby("cp_article_category","article_category_id","desc");
   
   $data['staff_permission'] = $this->staff_permission;
   
     $this->permission_access(13);
     if ($this->access_permission) {
        $this->template->set('title', 'Articles');
        $this->template->load('admin_dashboard_layout', 'contents', 'article_category_list', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
  }

  /*Edit article for view*/
  public function edit_article_category()
  {
    $data['menuactive'] = $this->uri->segment(2);
    $segment = $this->uri->segment("3");
    $data['segment'] = $segment;
    $data['singleData'] = $this->article_model->getsingle("cp_article_category",array("article_category_id" => $segment));

    $data['staff_permission'] = $this->staff_permission;
    
     $this->permission_access(13);
     if ($this->access_permission) {
        $this->template->set('title', 'Articles');
        $this->template->load('admin_dashboard_layout', 'contents' , 'edit_article_category', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
  }

  /*Edit article for ajax*/
  public function ajax_edit_article_category()
  {
    $singledata = $this->article_model->getsingle("cp_article_category",array("article_category_id" => $_POST['cat_id']));
    $data['upload_path'] = 'uploads/articles/category_icon/';
    $data['allowed_types'] = 'jpg|png|gif|jpeg';
    $data['max_size'] = '5000';
    $data['encrypt_name'] = true;
    $this->load->library('upload', $data);
    if ($this->upload->do_upload('category_icon')) {
      $attachment_data = array('upload_data' => $this->upload->data());
      $uploadfile = $attachment_data['upload_data']['file_name'];
    }
    if(!empty($uploadfile)){
     $files = $uploadfile;
   }else{
     $files = $singledata->article_category_icon;
   }
   $array = array(
    'article_category_name' => $_POST['category_name'],
    'article_category_icon' => $files,
    'article_category_desc' => $_POST['category_desc'],
    'article_category_create_date' => date("Y-m-d H:i:s")
  );
   $this->article_model->updateData("cp_article_category",$array,array("article_category_id" => $_POST['cat_id']));
   $return = array('code' => 1,'message' => 'Successfully updated');
   echo json_encode($return);   
  }

  /*Delete article category*/
  public function delete_article_category()
  {
    $delete_article_cat = $this->input->post("delete_article_cat");
    $image_name = $this->article_model->getsingle("cp_article_category",array("article_category_id" => $delete_article_cat));

    $article_name = $this->article_model->getAllwhere("cp_articles",array("category_id" => $delete_article_cat));


    unlink("uploads/articles/category_icon/".$image_name->article_category_icon);
    foreach($article_name as $del){
     unlink("uploads/articles/".$del->article_image);
     $this->article_model->delete('cp_articles',array('category_id' =>$delete_article_cat));

     $fav_article = $this->article_model->getAllwhere("cp_article_favourite",array("article_id" => $del->article_id));
   foreach($fav_article as $deld){
     $this->article_model->delete('cp_article_favourite',array('article_id' =>$deld->article_id));
   }



   }
   
   $this->article_model->delete('cp_article_category',array('article_category_id' =>$delete_article_cat));     
  }

  /*Delete article category*/
  // public function delete_article_category()
  // {
  //   $delete_article_cat = $this->input->post("delete_article_cat");
  //   $image_name = $this->article_model->getsingle("cp_article_category",array("article_category_id" => $delete_article_cat));
  //   $article_name = $this->article_model->getAllwhere("cp_articles",array("category_id" => $delete_article_cat));
  //   unlink("uploads/articles/category_icon/".$image_name->article_category_icon);
  //   foreach($article_name as $del){
  //    unlink("uploads/articles/".$del->article_image);
  //    $this->article_model->delete('cp_articles',array('category_id' =>$delete_article_cat));
  //  }
  //  $fav_article = $this->article_model->getAllwhere("cp_article_favourite",array("category_id" => $delete_article_cat));
  //  foreach($fav_article as $del){
  //    $this->article_model->delete('cp_article_favourite',array('category_id' =>$delete_article_cat));
  //  }
  //  $this->article_model->delete('cp_article_category',array('article_category_id' =>$delete_article_cat));     
  // }

  /*View article*/
  public function view_article_category()
  {
    $data['menuactive'] = $this->uri->segment(2);
    $data['segment'] = $this->uri->segment(3);
    $data['singledata'] = $this->article_model->getsingle("cp_article_category",array("article_category_id" => $data['segment']));
    
    $data['staff_permission'] = $this->staff_permission;
    
     $this->permission_access(13);
     if ($this->access_permission) {
        $this->template->set('title', 'Articles');
        $this->template->load('admin_dashboard_layout', 'contents' , 'view_article_category', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
  }

  /*update category status*/
  public function category_status()
  {
   $cat_id = $this->input->post("cat_id");
   $check_category = $this->article_model->getsingle("cp_article_category",array("article_category_id" => $cat_id));
   if($check_category->article_category_status == 1){
     $this->article_model->updateData("cp_article_category",array("article_category_status" => 0),array('article_category_id' => $cat_id));
     echo '2';
   }else{
    $this->article_model->updateData("cp_article_category",array("article_category_status" => 1),array('article_category_id' => $cat_id));
    echo '1';     
     }
  }
  }