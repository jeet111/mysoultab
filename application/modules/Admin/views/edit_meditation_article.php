<style>

img#output {

	width: 85px;

	margin-top: 15px;

}



div#video_loader {

	position: fixed;

	top: 0;

	bottom: 0;

	left: 0;

	right: 0;

	background-color: #0a0a0aa6;

	z-index: 999999;

	text-align: center;

	padding-top: 23%;

}

div#video_loader img#loading_img {

	width: 70px !important;

}



#flupmsgs {

	color : red;

}

#flupmsg {

	color : red;

}

</style>

<div class="container-fluid">

	<div class="dash-counter">

		<div class="Schedule_main_one">

			<div class="users-main">

				<?php

				echo "<h2>Edit Meditation Article</h2>";

				?>



				<div class="btn_topBack">

					<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">

						Back

					</a>

				</div>

				<div id="image_msg"></div>

				<div id="loders"></div>

				<form class="form-horizontal" action="" method="post" enctype="multipart/form-data" name="AddUsers" id="add_article" >

					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for="">Article Title:<span class="field_req">*</span></label>

							<div class="col-md-8">

								<input type="text" placeholder="Article Title" class="form-control" name="article_title" id="username" value="<?php echo $article->article_title; ?>">

								<?php echo form_error('article_title'); ?>



							</div>

						</div>

					</div>



					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for="">Article Description:<span class="field_req">*</span></label>

							<div class="col-md-8">



								<textarea name="article_description" id="" class="form-control" placeholder="Article Description" value="<?php echo set_value('article_description'); ?>" rows="10" cols="50"><?php echo $article->article_desc; ?></textarea>

								<?php echo form_error('article_description'); ?>



							</div>

						</div>

					</div>



					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for="">Artist Name:<span class="field_req">*</span></label>

							<div class="col-md-8">



								<input type="text" name="artist_name" placeholder="Artist Name" class="form-control" id="artist_name" value="<?php echo $article->artist_name; ?>" >



							</div>

						</div>

					</div>



					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for="">Article Type:<span class="field_req">*</span></label>

							<div class="col-md-8">

								<select name="article_type" type="text" class="form-control" >

									<option value="">Select Type</option>

									<?php foreach($categorys as $category){ ?>

										<option value="<?php echo $category->article_category_id; ?>" <?php if($category->article_category_id==$article->category_id){ echo 'selected=selected';} ?>><?php echo $category->article_category_name; ?></option>

									<?php } ?>

								</select>

								<?php echo form_error('article_type'); ?>

							</div>

						</div>

					</div>

					<input type="hidden" id="article_id" name="article_id" value="<?php echo $articleid; ?>" >



					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for="">Artist Image:<span class="field_req">*</span></label>

							<div class="col-md-8">

								<div class="input-group">

									<span class="input-group-btn">

										<!-- <span class="btn btn-default btn-files"> -->

											<input type="file" id="artist_image" class="form-control showvalidation" name="artist_image" onchange="readUrl(event)">

											<!-- </span> -->

										</span>

										<div id="flupmsgs"></div>

									</div>

									<div>

										<span id="demos" style="color: red"></span>

									</div>

									<?php if($article->artist_image==''){ ?>

										<img id='output' src="<?php echo base_url().'uploads/meditation_articles/default_icon.png' ?>">



									<?php }else{ ?>



										<img id='output' src="<?php echo base_url().'uploads/meditation_articles/'.$article->artist_image; ?>">



									<?php } ?>

								</div>

							</div>

						</div>

	<?php /* ?>

	<div class="col-md-8">

		<div class="form-group">

			<label class="control-label col-md-4" for="">Article Icon:<span class="field_req">*</span></label>

			<div class="col-md-8">

<div class="input-group lgn">

					<span class="input-group-btn">



							<input type="file" id="article_icon" class="form-control showvalidation" name="article_icon" onchange="readicon(event)">



					</span>

<div id="flupicon"></div>

				</div>

<div>

				<span id="demos_icon" style="color: red"></span>

				</div>

<?php if($article->article_icon==''){ ?>

<img id='output_icon' src="<?php echo base_url().'uploads/articles/default_icon.png' ?>">

<?php }else{ ?>

<img id='output_icon' src="<?php echo base_url().'uploads/articles/'.$article->article_icon; ?>">

<?php } ?>

			</div>

		</div>

	</div>



	<?php */ ?>
					<div class="col-md-8">
<div class="form-group">
<label class="control-label col-md-4" for=""> Visit Provider</label>

<div class="col-md-8">

	<div class="input-group">

		<span class="input-group-btn">

			<input type="url" class="form-control" name="visit_provider" value="<?php echo $article->visit_provider; ?>" placeholder="https://example.com">


		</span>



	</div>

</div>

</div></div>
	<div class="col-md-8">

		<div class="form-group">

			<label class="control-label col-md-4" for="">Article Image:<span class="field_req">*</span></label>

			<div class="col-md-8">

				<div class="input-group">

					<span class="input-group-btn">

						<!-- <span class="btn btn-default btn-files"> -->

							<input type="file" id="profile_images" class="form-control showvalidation" name="picture" onchange="loadFile(event)">

							<!-- </span> -->

						</span>

						<div id="flupmsg"></div>

					</div>

					<div>

						<span id="demo" style="color: red"></span>

					</div>



					<?php if($article->article_image==''){ ?>

						<img id='img-upload' src="<?php echo base_url().'uploads/meditation_articles/default_icon.png' ?>">



					<?php }else{ ?>



						<img id='img-upload' src="<?php echo base_url().'uploads/meditation_articles/'.$article->article_image; ?>">



					<?php } ?>



					<div class="bnt_sub">

						<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit" >



					<!-- <a class="cancel-btn btn" href="javascript:window.history.back()">

                        Back

                    </a> -->

                </div>

            </div>

        </div>

    </div>







</form>

</div>

</div>

</div>

</div>



<!--Added by 95 on 5-2-2019 For Doctor category for validation-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>

<script>

	$("#add_article").on('submit',(function(e)

	{

		e.preventDefault();

		var isvalidate=$("#add_article").valid();

		if(isvalidate)

		{

			$("#loders").append('<div id="video_loader" class="loading_class"><img id="loading_img" class="loader_test" style="width:20px" src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif" /></div>');





			$.ajax({

				url : "<?php echo base_url(); ?>Admin/Meditation_article/ajax_meditation_article_edit",

				type: "POST",

				data: new FormData(this),

				dataType: "json",

				contentType: false,

				cache: false,

				processData:false,

				success: function(data) {



					$("#loading_img").hide();

					$("#video_loader").hide();

					if(data.code == 1){

						$("#loading_img").hide();

						$("#video_loader").hide();

						$("#sub").prop('disabled', true);



						$("#image_msg").html('<div class="alert alert-success">Update successfully!<div>');

						setTimeout(function(){





							$("#image_msg").html("");

            //location.reload();

            window.location.href = '<?php echo base_url(); ?>admin/meditation_articles_list';

        }, 2000);

					}else{

						$(".loader_test").hide();

						$(".loading_class").hide();



						//  $("#image_msg").html('<div class="alert alert-danger">'+data.message+'<div>');



					}

				}

			});

		}

	}));



	function loadFile (event) {

		var pcFile = $('#profile_images').val().split('\\').pop();

		var pcExt     = pcFile.split('.').pop();

		var output = document.getElementById('img-upload');

		if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"

			|| pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){

			$(".up_but").prop('disabled', false);

		$('#flupmsg').html('');



		output.src = URL.createObjectURL(event.target.files[0]);

		$('#img-upload').show();

		$('#flupmsg').html('');

	}else{

		$('#flupmsg').html('Please select only Image file');

		$('#img-upload').hide();

		$(".up_but").prop('disabled', true);

	}

};



function readUrl (event) {

	var pcFile = $('#artist_image').val().split('\\').pop();

	var pcExt     = pcFile.split('.').pop();

	var output = document.getElementById('output');

	if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"

		|| pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){

		$(".up_but").prop('disabled', false);

	$('#flupmsgs').html('');



	output.src = URL.createObjectURL(event.target.files[0]);

	$('#output').show();

	$('#flupmsgs').html('');

}else{

	$('#flupmsgs').html('Please select only Image file');

	$('#output').hide();

	$(".up_but").prop('disabled', true);

}

};



</script>

<script type="text/javascript">

	$(document).ready(function() {



		$("#add_article").validate({

			rules: {

				article_title: "required",

				article_type:"required",

				article_description:"required",



			},

			messages: {

				article_title: "Please enter article title.",

				article_type:"Please select a article type.",

				article_description:"Please enter article description."



			}

		});

	});



	function showFileSize(){



		var img1  = document.getElementById('artist_image').value;



		var img2  = document.getElementById('profile_images').value;



		var pcFile = $('#profile_images').val().split('\\').pop();

		var pcExt     = pcFile.split('.').pop();



		var pcFile1 = $('#artist_image').val().split('\\').pop();

		var pcExt1     = pcFile1.split('.').pop();







		if(pcExt1 == "png" || pcExt1 == "PNG" || pcExt1 == "jpg" || pcExt1 == "JPG" || pcExt1 == "JEPG"

			|| pcExt1 == "jpeg" || pcExt1 == "gif" || pcExt1 == "GIF"){

			document.getElementById("flupmsgs").innerHTML=" ";

	}else if(pcExt1 !== "png" || pcExt1 !== "PNG" || pcExt1 !== "jpg" || pcExt1 !== "JPG" || pcExt1 !== "JEPG"

		|| pcExt1 !== "jpeg" || pcExt1 !== "gif" || pcExt1 !== "GIF" || img1==='' || img1===null){







		document.getElementById("flupmsgs").innerHTML="Please choose a Image file.";

	}



	if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"

		|| pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){

		document.getElementById("flupmsg").innerHTML=" ";

}else if(pcExt !== "png" || pcExt !== "PNG" || pcExt !== "jpg" || pcExt !== "JPG" || pcExt !== "JEPG"

	|| pcExt !== "jpeg" || pcExt !== "gif" || pcExt !== "GIF" || img1==='' || img1===null){

	$("#flupmsgs2").html("");

	document.getElementById("flupmsg").innerHTML="Please choose a Image file.";

}







}

</script>