﻿<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Grocery extends MX_Controller {

	public function __construct() {

		parent:: __construct();

		$this->load->library('session');

		$this->load->library('form_validation');

		$this->load->library('table');

		$this->load->library('pagination');

		$this->load->model('common_model');

		$this->load->model('common_model_new');

		$this->load->model('Common_model_new');

		$this->load->helper(array('common_helper'));

		$this->load->helper(array('url'));

		$this->load->helper(array('form'));   

		not_login();	

	}

	

	public function grocery_list()

	{

		

		$data['menuactive'] = $this->uri->segment(1);

		

	 //$lat = $this->session->userdata('lat');

	 //$long = $this->session->userdata('long');

		$lat = $this->input->get("lat");

		$long = $this->input->get("long");

		

		$data['unit']=$this->common_model_new->getAll('place_unit');

		



		if($data['unit'][0]->place_unit=='KM'){ 





			$url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=5000&type=supermarket&sensor=true&key=AIzaSyCrVGd7PYWdG6NMU-zXRvowyPY1qXr2mjQ";

		}else if($data['unit'][0]->place_unit=='Miles'){



			$radius = "8047";

			$url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=supermarket&sensor=true&key=AIzaSyCrVGd7PYWdG6NMU-zXRvowyPY1qXr2mjQ";

		}





	// print_r($getlocation);die;

	    //$url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=5000&type=supermarket&sensor=true&key=AIzaSyCrVGd7PYWdG6NMU-zXRvowyPY1qXr2mjQ";

		

		$supermarket = "supermarket";

		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$supermarket");

		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();

		$headers[] = "CAREPRO-AUTH-KEY: 12345";

		$headers[] = "Content-Type: application/x-www-form-urlencoded";

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);

		if (curl_errno($ch)) {

			

		}

		curl_close ($ch);

    //echo "<pre>";print_r($result);	die;

		$j_decoded = json_decode($result);

		// echo "<pre>";print_r($j_decoded);	die;

		$data['pagetoken'] = $j_decoded->next_page_token;

		foreach($j_decoded->results as $grocery){ 



			if(!empty($grocery->photos)){

				foreach($grocery->photos as $photo){ 

					$image = $photo->photo_reference;

					

					$image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyCrVGd7PYWdG6NMU-zXRvowyPY1qXr2mjQ";

					

					if($grocery->icon){

						$icon = $grocery->icon;

						$icon_path = "https://maps.gstatic.com/mapfiles/place_api/icons/$icon";

					}else{

						$icon_path = ''; 

					}

				}

			}else{

				$image_path = "";	

			}

			

			if(!empty($grocery->rating)){ 

				$rat = $grocery->rating;

			}else{

				$rat = '';

			}	





			

			$arrays[] = array(

				'grocery_name' => $grocery->name,

				'grocery_icon' =>  $icon_path,

				'grocery_rating' => $rat,

				'place_id' => $grocery->place_id,

				'user_id' => $this->session->userdata('logged_in')['id'],

				'grocery_address' => $grocery->vicinity,

				'grocery_lat' => $grocery->geometry->location->lat,

				'grocery_long' => $grocery->geometry->location->lng,

				'grocery_image' => $image_path,

				'grocery_created' => date('Y-m-d H:i:s')

			);

			

	//  $this->Common_model_new->insertData('cp_grocery',$array);



		}

		$per_page = '20';

		$offset = 0;

 //$groceries = $this->Common_model_new->grocery_listing($per_page,$offset);	



		$data['grocery_list'] = $arrays;





		$data['count_total'] = count($j_decoded->results);



		$data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'grocery')); 

		$this->template->set('title', 'Grocery');

		$this->template->load('user_dashboard_layout', 'contents', 'grocery',$data);

	}

	

	public function radius_grocery()

	{ 

		$data['menuactive'] = $this->uri->segment(1);

		$radius = $this->uri->segment("3");

		$lat = $_GET["lat"];

		$long = $_GET["long"];

		$pagetoken = $this->input->post("pagetoken");



		$data['unit']=$this->common_model_new->getAll('place_unit');

		$url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=supermarket&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo&pagetoken=$pagetoken";

		  //print_r($url);die;

		$supermarket = "supermarket";

		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$supermarket");

		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();

		$headers[] = "CAREPRO-AUTH-KEY: 12345";

		$headers[] = "Content-Type: application/x-www-form-urlencoded";

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);

		if (curl_errno($ch)) {

			

		}

		curl_close ($ch);

		

		$j_decoded = json_decode($result);

		$data['count_total'] = count($j_decoded->results);

		$data['pagetoken'] = $j_decoded->next_page_token;

		

		foreach($j_decoded->results as $key => $rest_details){



			if(isset($rest_details->photos[0]->photo_reference)){

				$image =  $rest_details->photos[0]->photo_reference;

			}else{

				$image="";

			}

			if(!empty($image)){

				$image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";

			}else{

				$image_path='';

			}

			

			if(!empty($rest_details->rating)){

				$rat = $rest_details->rating;

			}else{

				$rat = '';

			}

			



			$datas[] = array( 

				'grocery_id'=>$key,

				'grocery_name'=>$rest_details->name,

				'grocery_image'=>$image_path,

				'grocery_lat'=>$rest_details->geometry->location->lat,

				'grocery_long'=>$rest_details->geometry->location->lng,

				'grocery_address'=>$rest_details->vicinity,

				'place_id'=>$rest_details->place_id,

				'grocery_rating'=>$rat,

				'user_id'=>$this->session->userdata('logged_in')['id'],

				'grocery_created'=>date('Y-m-d H:i:s')

			);

			

		}



		$data['grocery_list'] = $datas;



//echo '<pre>';print_r($data['grocery_list']);die;



		$this->template->set('title', 'Grocery');

		$this->template->load('user_dashboard_layout', 'contents', 'grocery',$data);

	}

	

	public function getsearchdata()

	{ 

		$data['menuactive'] = $this->uri->segment(1);

		$segment = $this->uri->segment("2");

		$radius = $this->uri->segment("3");

		$data['unit']=$this->common_model_new->getAll('place_unit');

		$lat = $_GET["lat"];

		$long = $_GET["long"];

		

		

		if($radius == ''){

			$data['grocery_search'] = '';

			$url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$segment&type=supermarket&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";

		}else{

			$rest_search = str_replace('%20', ' ', $this->uri->segment("2"));

			$data['grocery_search'] = $rest_search;

			

			$url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=supermarket&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo&name=$segment";

		}

		

		

	  //print_r($url);die;

		

		

		$restaurant = "restaurant";

		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$restaurant");

		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();

		$headers[] = "CAREPRO-AUTH-KEY: 12345";

		$headers[] = "Content-Type: application/x-www-form-urlencoded";

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);

		if (curl_errno($ch)) {

			

		}

		curl_close ($ch);

		

		$j_decoded = json_decode($result);

		$data['pagetoken'] = $j_decoded->next_page_token;

		

		foreach($j_decoded->results as $key => $rest){



			

			foreach($rest->photos as $photo){ 

				$image = $photo->photo_reference;

				$image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyCrVGd7PYWdG6NMU-zXRvowyPY1qXr2mjQ";

				

				if($rest->icon){

					$icon = $rest->icon;

					$icon_path = "https://maps.gstatic.com/mapfiles/place_api/icons/$icon";

				}else{

					$icon_path = ''; 

				}

			} 

			if(!empty($rest->rating)){

				$rat = $rest->rating;

			}else{

				$rat = '';

			}	

			

			$datas[] = array(	        

				'grocery_name' => $rest->name,		  

				'grocery_rating' => $rat,

				'place_id' => $rest->place_id,

				'user_id' => $this->session->userdata('logged_in')['id'],

				'grocery_address' => $rest->vicinity,

				'grocery_lat' => $rest->geometry->location->lat,

				'grocery_long' => $rest->geometry->location->lng,

				'grocery_image' => $image_path,

				'grocery_created' => date('Y-m-d H:i:s')

			);

			

		}



		$data["count_total"] = count($j_decoded->results);



		$data['grocery_list'] = $datas;

//echo '<pre>';print_r($datas);die;

		$data['offset'] = '';

		$this->template->set('title', 'User Dashboard');

		$this->template->load('user_dashboard_layout', 'contents', 'grocery',$data);

	}

	

	function get_ajax_offset()

	{

		$data['menuactive'] = $this->uri->segment(1);

		$token = $this->input->post("offset"); 

		

		$lat = $_POST["lat"];

		$long = $_POST["long"];

		$radius = $this->input->post("radius");

		

		$url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=supermarket&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo&pagetoken=$token";

		$restaurant = "restaurant";

		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$restaurant");

		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();

		$headers[] = "CAREPRO-AUTH-KEY: 12345";

		$headers[] = "Content-Type: application/x-www-form-urlencoded";

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);

		if (curl_errno($ch)) {

			

		}

		curl_close ($ch);

		

		$j_decoded = json_decode($result);

		//echo '<pre>';print_r($j_decoded->results);die;

		$data['count_total'] = count($j_decoded->results);

		$data['pagetoken'] = $j_decoded->next_page_token;

		//echo $data['pagetoken'];

		

		$datas=array();



		foreach($j_decoded->results as $key => $rest_details){



			if(isset($rest_details->photos[0]->photo_reference)){

				$image =  $rest_details->photos[0]->photo_reference;

			}else{

				$image="";

			}

			if(!empty($image)){

				$image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";

			}else{

				$image_path='';

			}

			

			if(!empty($rest_details->rating)){

				$rat = $rest_details->rating;

			}else{

				$rat = '';

			}

			



			$datas[] = array( 

				'grocery_name' => $rest_details->name,		  

				'grocery_rating' => $rat,

				'place_id' => $rest_details->place_id,

				'user_id' => $this->session->userdata('logged_in')['id'],

				'grocery_address' => $rest_details->vicinity,

				'grocery_lat' => $rest_details->geometry->location->lat,

				'grocery_long' => $rest_details->geometry->location->lng,

				'grocery_image' => $image_path,

				'grocery_created' => date('Y-m-d H:i:s')

			);				    

		}

		

		

		

		

		

		



		$data['grocery_list'] = $datas;

 //$this->load->view('ajax_value_restaurant', $data);

		$list = $this->load->view('ajax_value_grocery', array('grocery_list' => $datas,'count_total' => $data['count_total'], 'pagetoken' => $data['pagetoken'],'radius' => $radius), true);



		$this->output->set_content_type('application/json');

        //$this->output->set_output(json_encode(array("code" => 100, "success" => true,'tab'=> $tab,'tabbing'=>$tabbing)));	



		$return = array('success' => true, 'list' => $list,'pagetoken' => $data['pagetoken']);

		echo json_encode($return);

	}

	

	public function search_grocery_list()

	{

		$data['menuactive'] = $this->uri->segment(1);

		$grocery_title = $this->input->post('grocery_title');

		$data['page'] = 0;

		$pagecon['per_page'] = 4;

		$allmusic = $this->Common_model_new->getsearchdatagrocery($grocery_title,$pagecon['per_page'],$data['page']);

		

		$data['grocery_list'] = $allmusic['rows'];

		$data['count_total'] = $allmusic['num_rows'];

		

		$this->load->view("ajax_grocery_search_value",$data);

		

	}

	

	function session_value()

	{

		$lat = $_POST['lat'];

		$long = $_POST['longt'];

		

		$this->session->set_userdata("lat",$lat);

		$this->session->set_userdata("long",$long);

		

		

	} 



	public function favorite_grocery()

	{

		$data['menuactive'] = $this->uri->segment(1);

		$grocery_id = $this->input->post('grocery_id'); 

		

		$value = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$grocery_id&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";

		

		$restaurant = "restaurant";

		$ch = curl_init($value);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$restaurant");

		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();

		$headers[] = "CAREPRO-AUTH-KEY: 12345";

		$headers[] = "Content-Type: application/x-www-form-urlencoded";

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);

		if (curl_errno($ch)) {

			

		}

		curl_close ($ch);

    //echo "<pre>";print_r($result);	die;

		$j_decoded = json_decode($result);

		

		$check_grocery = $this->common_model_new->getsingle("cp_fav_grocery",array("fav_grocery_place_id" => $grocery_id,"user_id" => $this->session->userdata('logged_in')['id']));

		

		

		if(!empty($check_grocery)){

			$this->common_model_new->delete("cp_fav_grocery",array("fav_grocery_place_id" => $grocery_id,"user_id" => $this->session->userdata('logged_in')['id']));

			echo '0';	

			

		}else{

			

			if(isset($j_decoded->result->photos[0]->photo_reference)){

				$image =  $j_decoded->result->photos[0]->photo_reference;

			}else{

				$image="";

			} 

			if(!empty($image)){

				$image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";

				

			}else{

				$image_path='';

			}

			if(!empty($j_decoded->result->rating)){

				$rat = $j_decoded->result->rating;

			}else{

				$rat = '';

			}

			

			$array = array(

				'fav_grocery_name' => $j_decoded->result->name,

				'fav_grocery_image' => $image_path,

				'fav_grocery_rating' => $rat,

			//'fav_rest_lat' => $restaurant_data->rest_lat,

			//'fav_rest_long' => $restaurant_data->rest_long,

				'fav_grocery_address' => $j_decoded->result->formatted_address,

				'fav_grocery_place_id' => $grocery_id,

				'user_id' => $this->session->userdata('logged_in')['id'],			

				'fav_grocery_created' => date('Y-m-d H:i:s')

			);

			

			$this->common_model_new->insertData("cp_fav_grocery",$array);

			echo '1';

		}

	}



	public function favorite_grocery_list()

	{

		$data['menuactive'] = $this->uri->segment(1);

		$checkLogin = $this->session->userdata('logged_in');

		if(!empty($checkLogin))

		{

			$data['page'] = 0;

			$pagecon['per_page'] = 20;

			$favorite_grocery1 = $this->Common_model_new->getfavoritegrocerylist($pagecon['per_page'],$data['page']);

		//echo '<pre>';print_r($favorite_grocery1);die;

			if (!empty($favorite_grocery1['rows'])) {

				$data['favorite_grocery'] = $favorite_grocery1['rows'];

				$data['count_total'] = $favorite_grocery1['num_rows'];

			} else {

				$data['favorite_grocery'] = '';

				$data['count_total'] = '';

			}


			$data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'favorite_grocery'));
			
			

			$this->template->set('title', 'Favorite Grocery');

			$this->template->load('user_dashboard_layout', 'contents', 'favorite_grocery',$data);

		}else{

			redirect('login');

		}

	}



	public function get_fav_ajax_offset()

	{ 

		$data['menuactive'] = $this->uri->segment(1);

		$data['offset'] = $this->input->post('offset');

		$like_val = $this->input->post('like_val');

		

		if(!empty($like_val)){

			

			$pagecon['per_page'] = 20;

			

			$allarticle = $this->Common_model_new->getsearchdatafavgrocery($like_val,$pagecon['per_page'],$data['offset']);

			

			

			if (!empty($allarticle['rows'])) {

				$data['favorite_grocery'] = $allarticle['rows'];

				$data['count_total'] = $allarticle['num_rows'];

			} else {

				$data['favorite_grocery'] = '';

				$data['count_total'] = '';

			}

			

			

		}else{

			

			$pagecon['per_page'] = 20;

			$nearbyrestaurants = $this->Common_model_new->getfavoritegrocerylist($pagecon['per_page'],$data['offset']);	

	  //echo '<pre>';print_r($music_list1);die;

			if (!empty($nearbyrestaurants['rows'])) {

				$data['favorite_grocery'] = $nearbyrestaurants['rows'];

				$data['count_total'] = $nearbyrestaurants['num_rows'];

			} else {

				$data['favorite_grocery'] = '';

				$data['count_total'] = '';

			}

			

		}

		$this->load->view('ajax_value_fav_grocery', $data);

	}



	public function search_fav_grocery()

	{

		$grocery_title = $this->input->post('grocery_title');

		$data['page'] = 0;

		$pagecon['per_page'] = 20;

		$allmusic = $this->Common_model_new->getsearchdatafavgrocery($grocery_title,$pagecon['per_page'],$data['page']);

		

		$data['favorite_grocery'] = $allmusic['rows'];

		$data['count_total'] = $allmusic['num_rows'];

		

		$this->load->view("ajax_grocery_fav_search_value",$data);

	}

	

	public function view_favorite_grocery()

	{

		$data['menuactive'] = $this->uri->segment(1);

		$segment = $this->uri->segment('2');

		

		$data['view_grocery'] = $this->common_model_new->getsingle("cp_fav_grocery",array("fav_grocery_id" => $segment)); 

		$this->template->set('title', 'Grocery');

		$this->template->load('user_dashboard_layout', 'contents', 'view_favorite_grocery',$data); 

	}

	

	public function view_grocery()

	{

		$data['menuactive'] = $this->uri->segment(1);       

		

		$radius = $this->uri->segment('3');

		$segment = base64_decode($this->uri->segment('2')); 

		

		$la = $_GET['lat'];

		$lo = $_GET['long'];

		$lat = str_replace("%","",$la);

		$long = str_replace("%","",$lo);

		

		$url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long &radius=$radius&type=supermarket&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";

		

		$value = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$segment&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";

		

		

		$restaurant = "restaurant";

		$ch = curl_init($value);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$restaurant");

		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();

		$headers[] = "CAREPRO-AUTH-KEY: 12345";

		$headers[] = "Content-Type: application/x-www-form-urlencoded";

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);

		if (curl_errno($ch)) {

			

		}

		curl_close ($ch);

    //echo "<pre>";print_r($result);	die;

		$j_decoded = json_decode($result);

		

	 // echo "<pre>";print_r($j_decoded);	die;

		

		

		if(isset($j_decoded->result->photos[0]->photo_reference)){

			$image =  $j_decoded->result->photos[0]->photo_reference;

		}else{

			$image="";

		} 

		if(!empty($image)){

			$image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";

			

		}else{

			$image_path='';

		}

		if(!empty($j_decoded->result->rating)){

			$rat = $j_decoded->result->rating;

		}else{

			$rat = '';

		}

		

		

		

		$array = array(

			'grocery_name' => $j_decoded->result->name,		  

			'grocery_rating' => $rat,

			'place_id' => $j_decoded->result->place_id,

			'user_id' => $this->session->userdata('logged_in')['id'],

			'grocery_address' => $j_decoded->result->formatted_address,

			'grocery_lat' => $j_decoded->result->geometry->location->lat,

			'grocery_long' => $j_decoded->result->geometry->location->lng,

			'grocery_image' => $image_path,

			'grocery_created' => date('Y-m-d H:i:s')

		);

		

		

		

		$data['view_grocery'] = $array;

		

	//echo '<pre>';print_r($array);die;

		

		$this->template->set('title', 'Grocery');

		$this->template->load('user_dashboard_layout', 'contents', 'view_grocery',$data);

	}





	/*view single favorite restaurant */

	public function view_favorite_restaurant()

	{

		

		$data['menuactive'] = $this->uri->segment(1);

		$segment = $this->uri->segment('2');

          //$data['view_restaurant'] = $this->common_model_new->getsingle("cp_restaurant",array("rest_id" => $segment)); 

		$data['view_restaurant'] = $this->common_model_new->getsingle("cp_fav_restaurant",array("fav_rest_id" => $segment)); 

		$this->template->set('title', 'Grocery');

		$this->template->load('user_dashboard_layout', 'contents', 'view_favorite_restaurant',$data);

		

	}



}    

?>

