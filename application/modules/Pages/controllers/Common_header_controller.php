<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Common_header_controller extends MX_Controller {
	
	public function __construct() {
		parent:: __construct();
		$this->load->library('session');
		$this->load->model('common_model');
		not_login();
	}


	public function get_alerts_cnt(){
		
		$checkLogin = $this->session->userdata('logged_in');
		//_dx($checkLogin);
		if(!empty($checkLogin) && $checkLogin !=''){

			$user_id = isset($checkLogin['id']) ? $checkLogin['id'] : "";
			$alerts_arr =$this->common_model->getAllRecordsById("alerts",array("user_id" => $user_id),'id','DESC');
			
			if(!empty($alerts_arr) && $alerts_arr !=''){
				
				$result = obj_to_array($alerts_arr);

				$result = array_map("unserialize", array_unique(array_map("serialize", $result)));
				//all status cnt
				$result = array_values(array_filter($result));
				
				foreach($result as $key=>$val){
					
					$result[$key]['total_read_flag'] = count($result);
					$read_flag = array_count_values(array_column($result, "read_flag"));
					
					foreach($read_flag as $k=>$v){
						
						switch ($k) {
						  case '1':
							$result[$key]['cnt_read_flag'] 	 = isset($v) ? $v : '0';
							break;
						  case '0':
							$result[$key]['cnt_unread_flag'] = isset($v) ? $v : '0';
							break;
						  default:
						}
						
						$cnt_read_flag 	= isset($result[$key]['cnt_read_flag']) ? $result[$key]['cnt_read_flag'] : '0';
						$cnt_unread_flag = isset($result[$key]['cnt_unread_flag']) ? $result[$key]['cnt_unread_flag'] : '0';
								
						if($result[$key]['total_read_flag'] == $cnt_unread_flag){
							$result[$key]['cnt_read_flag'] ='0';
						}
						
						if($result[$key]['total_read_flag'] == $cnt_read_flag){
							$result[$key]['cnt_unread_flag'] ='0';
						}
					}
				}
				$data = $result;
			}else{
				$data ='';
			}
		}else{
			$result='';
		}
		//_dx($data);
		$this->template->set('title','Dashboard header');
		//$this->load->view('',['data'=>$data]);
		$this->template->load('new_user_dashboard_layout','contents','dashboard_new',$data);
		
	}
}
  
?>
