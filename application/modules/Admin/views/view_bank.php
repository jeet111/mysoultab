<div class="container-fluid">
	<div class="dash-counter users-main my-sts-table user-list">
		<div class="">
			<div class="col-md-12">
				<div class="">
					<div class="card-body">
						<div class="my_info_one">
							<div class="Schedule_main_one">
								<div class="users-main">
									<h2>View Bank</h2>
									<div class="btn_topBack">
										<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">
											Back
										</a>
									</div>

									<div class="row">
										<?php //echo "<pre>"; print_r($singledata); echo "</pre>"; ?>
										<div class="col-md-12">
											<div class="general-info">
												<div class="card-header">
													<h5 class="card-header-text">About Bank</h5>
												</div>
												<div class="rrow">
													<div class="col-md-12">
														<div class="row">
															<div class="col-md-6 col-sm-6">
																<div class="row">
																	<div class="col-md-5 col-sm-5">
																		<div class="f_name"><b>Username:</b></div>
																	</div>
																	<div class="col-md-7 col-sm-7">
																		<div class="f_nameOne">
																			<?php echo $viewBank[0]->username;?></div>
																		</div>
																	</div>
																</div>
																<div class="col-md-6 col-sm-6">
																	<div class="row">
																		<div class="col-md-5 col-sm-5">
																			<div class="f_name"><b>Bank Name:</b></div>
																		</div>
																		<div class="col-md-7 col-sm-7">
																			<div class="f_nameOne"><?php echo $viewBank[0]->bank_name;?>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6 col-sm-6">
																<div class="row">
																	<div class="col-md-5 col-sm-5">
																		<div class="f_name"><b>Bank Address:</b></div>
																	</div>
																	<div class="col-md-7 col-sm-7">
																		<div class="f_nameOne">
																			<?php echo $viewBank[0]->bank_address;?>
																		</div>
																	</div>
																</div>

															</div>
															<div class="col-md-6 col-sm-6">
																<div class="row">
																	<div class="col-md-5 col-sm-5">
																		<div class="f_name"><b>Bank Rating:</b></div>
																	</div>
																	<div class="col-md-7 col-sm-7">
																		<div class="f_nameOne">
																			<ul class="abs">
																				<?php for($i=1;$i<=$viewBank[0]->bank_rating;$i++) { ?>
																					<li><i class="fa fa-star" aria-hidden="true"></i></li>
																				<?php }
																				if (strpos($viewBank->$viewBank[0]->bank_rating,'.')) { ?>
																					<li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
																					<?php $i++; } ?>
																					<?php while ($i<=5) { ?>
																						<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
																						<?php $i++; }
																						?>
																					</ul>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-6 col-sm-6">
																		<div class="row">
																			<div class="col-md-5 col-sm-5">
																				<div class="f_name"><b>Bank Image:</b></div>
																			</div>
																			<div class="col-md-7 col-sm-7">
																				<div class="f_nameOne">
																					<?php if(!empty($viewBank[0]->bank_image)){ ?>
																						<img src="<?php echo $viewBank[0]->bank_image; ?>" class="" alt=""  width="100px" hight="100px" >
																					<?php }else{ ?>
																						<img src="<?php echo base_url();?>assets/images/bank_img.jpg" class="" alt=""  width="100px" hight="100px">
																					<?php } ?>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<?php $admin_url = $this->uri->segment(2);
													$url = 'admin/email_list/list';
													if ($admin_url == 'adminprofile') {
														$url = 'admin/dashboard';
													}?>
												</div>
											</div>

									<!-- <div class="row">
										<div class="col-md-12">
											<div class="general-info">
												<div class="card-header">
													<h5 class="card-header-text">About Bank</h5>
												</div>
												<div class="rrow">
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-5 col-sm-5">
																<div class="f_name"><b>Username:</b></div>
															</div>
															<div class="col-md-7 col-sm-7">
																<div class="f_nameOne"><?php echo $viewBank[0]->username;?></div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-5 col-sm-5">
																<div class="f_name"><b>Bank Name:</b></div>
															</div>
															<div class="col-md-7 col-sm-7">
																<div class="f_nameOne"><?php echo $viewBank[0]->bank_name;?></div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-5 col-sm-5">
																<div class="f_name"><b>Bank Address:</b></div>
															</div>
															<div class="col-md-7 col-sm-7">
																<div class="f_nameOne"><?php echo $viewBank[0]->bank_address;?></div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-5 col-sm-5">
																<div class="f_name"><b>Bank Rating:</b></div>
															</div>
															<div class="col-md-7 col-sm-7">
																<div class="f_nameOne">
																	<ul class="abs">
																		<?php for($i=1;$i<=$viewBank[0]->bank_rating;$i++) { ?>
																		<li><i class="fa fa-star" aria-hidden="true"></i></li>
																		<?php }
																		if (strpos($viewBank->$viewBank[0]->bank_rating,'.')) { ?>
																		<li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
																		<?php $i++; } ?>
																		<?php while ($i<=5) { ?>
																		<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
																		<?php $i++; }
																		?>
																	</ul>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-5 col-sm-5">
																<div class="f_name"><b>Bank Image:</b></div>
															</div>
															<div class="col-md-7 col-sm-7">
																<div class="f_nameOne">
																	<?php if(!empty($viewBank[0]->bank_image)){ ?>
																	<img src="<?php echo $viewBank[0]->bank_image; ?>" class="" alt=""  width="100px" hight="100px" >
																	<?php }else{ ?>
																	<img src="<?php echo base_url();?>assets/images/bank_img.jpg" class="" alt=""  width="100px" hight="100px">
																	<?php } ?>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<?php $admin_url = $this->uri->segment(2);
											$url = 'admin/email_list/list';
											if ($admin_url == 'adminprofile') {
												$url = 'admin/dashboard';
											}?>

										</div>
									</div> -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style type="text/css">
ul.abs {
	list-style: none;
	margin: 5px 0px 0px;
	padding: 0px;
}
ul.abs li {
	display: inline-block;
	color: #ffdd2a;
	font-size: 16px;
}
</style>