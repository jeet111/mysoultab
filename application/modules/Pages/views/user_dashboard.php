<input type="hidden" name="get_lat" id="get_lat" value="" >
<input type="hidden" name="get_long" id="get_long" value="" >
<aside class="right-side">                
    <section class="content-header">
        <h1><i class="fa fa-dashboard"></i> User Dashboard</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User Dashboard</li>
        </ol>
    </section>                
    <section class="content">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="alert alert-success message">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <?php echo $this->session->flashdata('success'); ?></div>
            <?php } ?>
            <div class="row">
                <div class="col-lg-3 col-xs-6">                            
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3><?php echo count($doctor_appointments); ?></h3>
                            <p>Appointments</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?php echo base_url(); ?>appointment_list" class="small-box-footer">
                            <!-- More info  -->
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div><!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3><?php echo count($total_email); ?></h3>
                            <p>Emails</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="<?php echo base_url(); ?>email_list" class="small-box-footer">
                            <!-- More info  -->
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-yellow">
                        <div class="inner">
							<h3><?php
							if(!empty($count_photo)){
							 echo count($count_photo);
							} 
							 ?></h3>
                            <p>Photos</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?php echo base_url(); ?>user_photos" class="small-box-footer">
                            <!-- More info  -->
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div><!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <?php 
                //                     $ctemp_main =  $weather['main']['temp'];
                //                     $ctemp_min =  $weather['main']['temp_min'];
                //                     $ctemp_max =  $weather['main']['temp_max'];
                //             // $ctemp_min = ($ktemp_min - 273.15)*9/5+32;
                //             // $ctemp_max = ($ktemp_max - 273.15)*9/5+32;
                // $current_ctemp_main = number_format((float)$ctemp_main, 0, '.', '');
                // $current_ctemp_min = number_format((float)$ctemp_min, 0, '.', '');
                // $current_ctemp_max = number_format((float)$ctemp_max, 0, '.', ''); 
                            // print_r($weather['list']);
                            // die;
                            $previous_date='';
                            foreach ($weather['list'] as $key) {
                              $dt_txt = $key['dt_txt'];
                              $date = new DateTime($dt_txt);
                              $current_date = $date->format('Y-m-d');
                              if ($current_date != $previous_date) { 
                                $ctemp_main =  $key['main']['temp'];
                                $current_ctemp_main = number_format((float)$ctemp_main, 0, '.', '');
                                if(isset($key['weather'][0]['id'])){
                                    $cod = $key['weather'][0]['id'];
                                }
                                if($current_date==date('Y-m-d')){
                                    break;
                                }
                            }
                        }
                        ?>
                        <h3><?php echo $current_ctemp_main." &#8451; "; ?> </h3>
                        <p>Weather</p>
                    </div>
                    <div class="icon">
                        <div class="weather">
                            <?php 
                            switch($cod){
                                /*Group 2xx: Thunderstorm:*/
                                /*========================*/        
                                case '200':
                                echo 'Thunderstorm with light rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';
                                break;
                                case '201':
                                echo 'Thunderstorm with rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';
                                break;
                                case '202':
                                echo 'Thunderstorm with heavy rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';
                                break; 
                                case '210':
                                echo 'Light thunderstorm.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';
                                break; 
                                case '211':
                                echo 'Thunderstorm.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';
                                break; 
                                case '212': 
                                echo 'Heavy thunderstorm.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';
                                break;
                                case '221': 
                                echo 'Ragged thunderstorm.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';
                                break;
                                case '230': 
                                echo 'Thunderstorm with light drizzle.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';
                                break;
                                case '231':
                                echo 'Thunderstorm with drizzle'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';
                                break;
                                case '232':
                                echo 'Thunderstorm with heavy drizzle.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';
                                break; 
                                /*Group 3xx: Drizzle:*/
                                /*===================*/
                                case '300': 
                                echo 'Light intensity drizzle.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';            
                                break;
                                case '301':
                                echo 'Drizzle.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';
                                break;
                                case '302':
                                echo 'Heavy intensity drizzle.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';
                                break; 
                                case '310':
                                echo 'Light intensity drizzle rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';
                                break; 
                                case '311':
                                echo 'Drizzle rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';
                                break; 
                                case '312':
                                echo 'Heavy intensity drizzle rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">'; 
                                break;
                                case '313':
                                echo 'Shower rain and drizzle.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';
                                break;
                                case '314':
                                echo 'Heavy shower rain and drizzle.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';
                                break;
                                case '321':
                                echo 'Shower drizzle.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';
                                break;
                                /*Group 5xx: Rain:*/
                                /*=================*/
                                case'500':
                                echo 'Light rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/10d.png ">';
                                break; 
                                case'501':
                                echo 'Moderate rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/10d.png ">';
                                break;
                                case'502': 
                                echo 'Heavy intensity rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/10d.png ">';
                                break;
                                case'503': 
                                echo 'Very heavy rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/10d.png ">';
                                break;
                                case'504': 
                                echo 'Extreme rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/10d.png ">';
                                break;
                                case'511': 
                                echo 'Freezing rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';
                                break;
                                case'520': 
                                echo 'Light intensity shower rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';
                                break;
                                case'521': 
                                echo 'Shower rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';
                                break;
                                case'522': 
                                echo 'Heavy intensity shower rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';
                                break;
                                case'531':
                                echo 'Ragged shower rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';
                                break;
                                /*Group 6xx: Snow:*/
                                /*==================*/
                                case'600':
                                echo 'Light snow.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';
                                break;
                                case'601':
                                echo 'Snow.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';
                                break;
                                case'602':
                                echo 'Heavy snow.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';
                                break;
                                case'611':
                                echo 'Sleet.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';
                                break;
                                case'612':
                                echo 'Shower sleet.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';
                                break;
                                case'615':
                                echo 'Light rain and snow.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';
                                break;
                                case'616':
                                echo 'Rain and snow.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';
                                break;
                                case'620':
                                echo 'Light shower snow.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';
                                break;
                                case'621':
                                echo 'Shower snow.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';
                                break;
                                case'622':
                                echo 'Heavy shower snow.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';
                                break;
                                /*Group 7xx: Atmosphere:*/
                                /*======================*/
                                case'701':
                                echo 'Mist.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';
                                break; 
                                case'711':
                                echo 'Smoke.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';
                                break;
                                case'721':
                                echo 'Haze.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';
                                break;
                                case'731':
                                echo 'Sand, dust whirls.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';
                                break;
                                case'741':
                                echo 'Fog.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';
                                break;
                                case'751':
                                echo 'Sand.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';
                                break;
                                case'761':
                                echo 'Dust.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';
                                break;
                                case'762':
                                echo 'Volcanic ash.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';
                                break;
                                case'771':
                                echo 'Squalls.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';
                                break;
                                case'781':
                                echo 'Tornado.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';
                                break;
                                /*Group 800: Clear:*/
                                /*===================*/
                                case'800':
                                echo 'Clear sky.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/1d.png ">'; 
                                break;
                                /*Group 80x: Clouds:*/
                                /*===================*/
                                case'801':
                                echo 'Few clouds.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/2d.png ">';
                                break;
                                case'802':
                                echo 'Scattered clouds.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/3d.png ">';
                                break;
                                case'803': 
                                echo 'Broken clouds.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/4d.png ">';
                                break;
                                case'804': 
                                echo 'Overcast clouds.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/4d.png ">';
                                break;
                            }
                            ?>
                        </div>
                        <!-- <i class='fas fa-cloud-sun'></i> -->
                        <!-- <i class="ion ion-pie-graph"></i> -->
                    </div>
                    <a href="<?php echo base_url(); ?>weekly_weather" class="small-box-footer">
                        <!-- More info  -->
                        <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <!-- <div class="row"> -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <?php //$userAlerts=0; ?>
                            <?php if(!empty($userAlerts)){ ?>
                                <h3><font color="#FF0000" size="6"><?php echo count($userAlerts); ?>&nbsp;Alerts</font> </h3>
                            <?php }else{ ?>
                             <h3> <font color="009900" size="6">No Alerts</font></h3>
                         <?php } ?>
                         <p>Alerts</p>
                     </div>
                     <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        <!-- More info  -->
                        <i class="fa fa-arrow-circle-right"></i>
                    </a>
                <!-- </div> -->
            </div>

        </div>

            <!-- <div class="row">
            </div> -->
        </section>
                <?php //echo "<pre>";print_r($weather); 
                //echo $weather['coord']['lon'];
                //echo $weather['main']['temp_max'];
                ?>
            </aside>
            <!-- <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'> -->
            <script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>
            <script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>
