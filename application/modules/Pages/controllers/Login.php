<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Login extends MX_Controller {

  public function __construct() {

    parent:: __construct();

    $this->load->library('session');

    $this->load->library('form_validation');

    $this->load->library('table');

    $this->load->library('pagination');

    $this->load->model('common_model');

    $this->load->model('common_model_new');

    $this->load->model('Common_model_new');

    $this->load->helper(array('common_helper','cookie'));

    $this->load->helper(array('url'));

    $this->load->helper(array('form'));

    $this->load->helper('form');

    //is_login();


  }

  public function index() {



    $data = array();

    $data['menuactive'] = $this->uri->segment(1);  

    $data['CatData'] = $this->common_model_new->getAllwhere("cp_tabfeature_category",array("status" => 1));

    $data['featureData'] = $this->common_model_new->getAllwhere("cp_tabfeatures",array("status" => 1));  

    $data['testiData'] = $this->common_model_new->getAllwhere("testimonial",array("status" => 1));

    $data['memberStoryData'] = $this->common_model_new->getAllwhere("cp_member_stories",array("status" => 1));

    $this->template->set('title', 'Home');

    $this->template->load('page_layout', 'contents', 'home', $data);   

  }
  public function withings(){
    $this->template->load('new_user_dashboard_layout', 'contents', 'withings', '');
  }

  public function dashboard_new(){
    $data['menuactive'] = '';
    $checkLogin = $this->session->userdata('logged_in');

    if(!empty($checkLogin))
    {
    	$data = array();
		if(isset($checkLogin['id']) && $checkLogin['id'] != ''){
    	$data['user_id'] = $checkLogin['id'];
      	$data['menuactive'] = $this->uri->segment(1);
      	$this->template->set('title', 'Dashboard New');
		$this->load->model('Login_model');

			$complianceData = $this->Login_model->getComplieanceData($checkLogin['id']);
			if(!empty($complianceData)){
				$data['compliance'] = $complianceData;
			}
		}
      $this->template->load('new_user_dashboard_layout', 'contents', 'dashboard_new', $data);
    } 
    else{

      redirect('login');
  
    } 
  }

  


  public function dashboard(){ 

   //die('uryturytur');
  	//print_r($this->session->userdata('logged_in')); die;
   $data['menuactive'] = '';
   $checkLogin = $this->session->userdata('logged_in');

   if(!empty($checkLogin))
   {

	//echo $this->session->userdata('long');die;

	//$data['total_email'] = $this->common_model->jointwotablenm('cp_emails', 'user_id', 'cp_users', 'id',array('to_email ' => $checkLogin['email'],'email_status' => 1,'cp_emails.inbox_status' => 0),'','cp_emails.email_id','desc');


     $data['total_email'] = $this->Common_model_new->getEmails1($checkLogin['id']);

			//echo '<pre>';print_r($data['total_email']);die;

     $data['doctor_appointments'] = $this->common_model_new->getAllappointments($checkLogin['id']);

     $data['count_photo'] = $this->common_model_new->getAllwhere("cp_user_photo",array("user_id" => $checkLogin['id']));

     $data['paymentDetails'] = $this->common_model_new->getAllwhere("subscription_payment_transactions",array("user_id" => $checkLogin['id']));

    //$url = "http://api.openweathermap.org/data/2.5/weather?q=indore,IN&APPID=62f6de3f7c0803216a3a13bbe4ea9914&units=metric";


     $url = "https://api.openweathermap.org/data/2.5/forecast?q=indore,IN&APPID=62f6de3f7c0803216a3a13bbe4ea9914&units=metric";


     $json=file_get_contents($url);

     $data['weather']=json_decode($json,true);

     $data['user_data'] = $this->common_model->getSingleRecordById('cp_users',array('id' => $checkLogin['id']));

   //$data['menuactive'] = '';//

     $data['userAlerts'] = $this->common_model_new->getAllwhere("alerts",array("user_id" => $checkLogin['id'],'read_flag'=>0));
     //echo "<pre>";
     //print_r($data['userAlerts']);
     //die;
     $this->template->set('title', 'User Dashboard');

     $this->template->load('new_user_dashboard_layout', 'contents', 'dashboard_new', $data);

   }else{

    redirect('login');

  } 

}


public function about() {

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);

  $data['singleData'] = $this->common_model->getSingleRecordById('about_page',array('template_id'=> 1));  

  $this->template->set('title', 'About');

  $this->template->load('page_layout', 'contents', 'about', $data);

}

// public function features() {

//   $data = array();

//   $data['menuactive'] = $this->uri->segment(1);

//   $data['singleData'] = $this->common_model->getSingleRecordById('cp_cms_pages',array('page_id'=> 4));  

//   $this->template->set('title', 'Features');

//   $this->template->load('page_layout', 'contents', 'features', $data);

// }

public function features() {

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);



  $data['CatData'] = $this->common_model_new->getAllwhere("cp_tabfeature_category",array("status" => 1));

  $data['featureData'] = $this->common_model_new->getAllwhere("cp_tabfeatures",array("status" => 1));   
  $data['featureData'] = $this->common_model->getSingleRecordById('cp_cms_pages',array('page_id'=> 4));  

  $this->template->set('title', 'About');

  $this->template->load('page_layout', 'contents', 'features', $data);

}







public function featuresxyz() {

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);



  $data['CatData'] = $this->common_model_new->getAllwhere("cp_tabfeature_category",array("status" => 1));

  $data['featureData'] = $this->common_model_new->getAllwhere("cp_tabfeatures",array("status" => 1));   

  $this->template->set('title', 'About');

  $this->template->load('page_layout', 'contents', 'xfeatures', $data);

}



// public function features() {

//   $data = array();

//   $data['menuactive'] = $this->uri->segment(1);

//   $data['featureData'] = $this->common_model->getSingleRecordById('cp_cms_pages',array('page_id'=> 4));  

//   $this->template->set('title', 'Features');

//   $this->template->load('page_layout', 'contents', 'features', $data);

// }

public function faqs() {

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);  



  $data['singleData'] = $this->common_model->getSingleRecordById('cp_cms_pages',array('page_id'=> 8));  


  $data['faqsData'] = $this->common_model_new->getAllwhere("faq",array("status" => 1));



  $this->template->set('title', 'About');

  $this->template->load('page_layout', 'contents', 'faqs', $data);    

}





public function survey() {   

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);



    // $data['CatData'] = $this->common_model_new->getAllwhere("cp_tabfeature_category",array("status" => 1));

    // $data['featureData'] = $this->common_model_new->getAllwhere("cp_tabfeatures",array("status" => 1));   

  $this->template->set('title', 'About');

  $this->template->load('page_layout', 'contents', 'survey');

}





public function Feedback()

{

  $like_type = $this->input->post('like_type');

  $no_people = $this->input->post('no_people');

  $usrfeedback = $this->input->post('usrfeedback');

  $email = $this->input->post('email');

  $mobile = $this->input->post('mobile');



  if(!empty($like_type) && (!empty($no_people))){



    $checkLogin = $this->session->userdata('logged_in');



    if(!empty($checkLogin)){

      $user_id = $checkLogin['id'];

    }else{

      $user_id = '';

    }



    $fedDta=array(

      'mobile'=>$mobile,

      'email'=>$email,

      'how_muchlike'=>$like_type,

      'no_people'=>$no_people,

      'feed_desc'=>$usrfeedback,

      'user_id'=>$user_id,

      'created'=>date('Y-m-d i:s:m')

    );



    $insertId = $this->common_model->addRecords('user_survey_feedback', $fedDta);



    // echo $insertId;

    // die;



    if($insertId){

      echo 1;

    }else{

      echo 0;

    }



  }else{



    echo 2;

  }



}



public function NewsSubs()
{

  // $like_type = $this->input->post('like_type');
  // $no_people = $this->input->post('no_people');
  // $usrfeedback = $this->input->post('usrfeedback');
  $subs_email = $this->input->post('subs_email');
  //$mobile = $this->input->post('mobile');
  //if(!empty($like_type) && (!empty($no_people))){
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin)){
    $user_id = $checkLogin['id'];
  }else{
    $user_id = '';
  }
  $NewssubsDta=array(
    'email'=>$subs_email,
    'status'=>1,
    'created_at'=>date('Y-m-d h:i:s'),
    'updated_at'=>date('Y-m-d h:i:s')
  );
  $insertId = $this->common_model->addRecords('subscriptions', $NewssubsDta);
  // echo $insertId;
  // die;
  if($insertId){
    echo 1;
  }else{
    echo 0;
  }
  // }else{
  //   echo 2;
  // }
}



// public function News_Subs()
// {
//   echo "its calling";
//   die;

// }



public function temp_faqs() {

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);  





  $data['faqsData'] = $this->common_model_new->getAllwhere("faq",array("status" => 1));



  $this->template->set('title', 'About');

  $this->template->load('page_layout', 'contents', 'tempfaqs', $data);    

}





public function member_stories() {

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);  
  $data['singleData'] = $this->common_model->getSingleRecordById('cp_cms_pages',array('page_id'=> 9));  





    //$data['faqsData'] = $this->common_model_new->getAllwhere("faq",array("status" => 1));

  $data['memberStoryData'] = $this->common_model_new->getAllwhere("cp_member_stories",array("status" => 1));

  $this->template->set('title', 'Member stoties');

  $this->template->load('page_layout', 'contents', 'memberstories', $data);    

}





public function about_us() {



    //die('about');

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);



  $this->template->set('title', 'About');

  $this->template->load('page_layout', 'contents', 'home', $data);

}



public function subscriptions() {

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);



  $where=array('status'=>1);

  $data['subscriptionData'] = $this->Common_model_new->getAllwhere('subscription_plan', $where);

  $data['singleData'] = $this->common_model->getSingleRecordById('cp_cms_pages',array('page_id'=> 5));  


  $this->template->set('title', 'Subcscriptions');

  $this->template->load('page_layout', 'contents', 'subscriptions', $data);

}
public function buynow() {

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);




  $data['singleData'] = $this->common_model->getSingleRecordById('cp_cms_pages',array('page_id'=> 10));  


  $this->template->set('title', 'Buynow');

  $this->template->load('page_layout', 'contents', 'buynow', $data);

}





public function contact() {

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);

  $data['singleData'] = $this->common_model->getSingleRecordById('cp_cms_pages',array('page_id'=> 3));  

  $this->template->set('title', 'Contact-us');

  $this->template->load('page_layout', 'contents', 'contact', $data);

}

public function how_it_work() {

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);

  $this->template->set('title', 'How IT Work');

  $this->template->load('page_layout', 'contents', 'how-it-work', $data);

}

public function term_and_condition() {

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);



  $data['singleData'] = $this->common_model->getSingleRecordById('cp_cms_pages',array('page_id'=> 2));

  $this->template->set('title', 'Term And Condition');

  $this->template->load('page_layout', 'contents', 'term_and_condition', $data);

}





public function privacy_policy()

{

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);



  $data['singleData'] = $this->common_model->getSingleRecordById('cp_cms_pages',array('page_id'=> 1));

  $this->template->set('title', 'Privacy policy');

  $this->template->load('page_layout', 'contents', 'privacy_policy', $data);

}



/*Contact us form*/

public function contact_us()

{



 $fullname = $this->input->post('fullname');

 $email = $this->input->post('email');

 $message = $this->input->post('usrfeedback');

 $data=array(

   'full_name'=>$fullname,

   'email'=>$email,

   'comment'=>$message,

   'create_date'=>date('Y-m-d H:i:s')

 );



 $insertId = $this->common_model->addRecords('contact_us', $data);



 if($insertId){

   $message = "Thanks for contacting us.";

   $subject = "Contact Us";

    // $status = sendEmail($email, $subject, $message);

   $status= new_send_mail($message, $subject, $email, '');

   if($status!='' && $status==1){

     echo 1;

   }else{

     echo 0;

   }

 }

}





public function payment_process($plan_id='')

{





  $data = array();

  $data['menuactive'] = $this->uri->segment(1);



  $getparam = $this->uri->segment(2);

  $subsplan_id = base64_decode($getparam);  





  $data['subsplanDetails'] = $this->common_model->getSingleRecordById('subscription_plan',array('id'=> $subsplan_id));



  // echo "<pre>";

  // print_r($data['subsplanDetails']);



  // die;



  

  $this->template->set('title', 'Payment');

  $this->template->load('home_layout', 'contents', 'payment', $data);

  //return view('payment',$data);

}



// public function signup(){

//   $data = array();

//   $data['menuactive'] = $this->uri->segment(1);

//   if(isset($_POST['nnsubmit'])){

//     $this->form_validation->set_rules('username', 'username', 'trim|required|max_length[30]');

//     $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[6]');

//     $this->form_validation->set_rules('re_password', 'password confirmation', 'trim|required|matches[password]');

//     $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[cp_users.email]');

//     $this->form_validation->set_rules('mobile', 'mobile', 'trim|required|min_length[5]|max_length[14]|numeric');



//     $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');

//     $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');

//     $this->form_validation->set_rules('gender', 'Gender', 'trim|required');

//     $this->form_validation->set_rules('address', 'Address', 'trim|required');

//     $this->form_validation->set_rules('user_id', 'User ID', 'trim|required');

//     $this->form_validation->set_rules('dob', 'DOB', 'trim|required');







//     if($this->form_validation->run() == false)

//     {



//       $getparam = $this->uri->segment(2);

//       $data['plan_id'] = base64_decode($getparam);

//       $this->template->set('title', 'Signup');

//       $this->template->load('home_layout', 'contents', 'signup', $data);

//     }else{



//       $firstname = $this->input->post('firstname');

//       $lastname = $this->input->post('lastname');

//       $username = $this->input->post('username');

//       $dob = $this->input->post('dob');

//       $email = $this->input->post('email');

//       $mobile = $this->input->post('mobile');

//       $user_id = $this->input->post('user_id');

//       $gender = $this->input->post('gender');

//       $address = $this->input->post('address');

//       $digits = 4;

//       $otp = rand(pow(10, $digits-1), pow(10, $digits)-1);

//       $data = array(

//         'username' => $username,

//         'name' => $firstname,

//         'lastname' => $lastname,

//         'dob' => (!empty($dob) ? date('Y-m-d',strtotime($dob)) : null),

//         'email' =>  $email,

//         'mobile' => $mobile,

//         'password' => md5($this->input->post('password')),

//         'user_role' => 2,

//         'create_user' => date('Y-m-d H:i:s'),

//         'status' => 0,

//         'otp' => $otp,

//         'gender' => (!empty($gender) ? $gender : 1),

//         'address' => (!empty($address) ? $address : null),

//         'user_id' => (!empty($user_id) ? $user_id : null)

//       );

//       $insertId = $this->common_model->addRecords('cp_users', $data);



//       $getparam = $this->uri->segment(2);

//       $subsplan_id = base64_decode($getparam);





//       redirect('make-payment/'.$getparam);



//       if ($insertId) {



//         $email = $data['email'];

//         $password = $this->input->post('password');



//         $message = 'Dear '.$firstname.' '.$lastname.',<br><p>Welcome to our carepro app.</p><br><p>Please open tablet and please do further process and enjoy our services.</p><br><p>Best Wishes,</p><br><p>CarePro Team</p>';

//         $subject = "CarePro - Welcome";



//         $check = new_send_mail($message, $subject, $email, '');



//         $message = '<html>

//         <head>

//         <title></title>

//         </head>

//         <body>

//         <h2>Dear Admin,</h2>

//         <p>Please see user information.</p>

//         <table>

//         <thead>

//         <tr>

//         <th>User Name</th>

//         <th>Email</th>

//         <th>Contact No</th>

//         <th>User ID</th>

//         <th>Gender</th>

//         <th>Date Of Birth</th>

//         <th>Address</th>

//         </tr>

//         </thead>

//         <tbody>

//         <tr>

//         <td>'.$firstname.' '.$lastname.'</td>

//         <td>'.$email.'</td>

//         <td>'.$mobile.'</td>

//         <td>'.$user_id.'</td>

//         <td>'.($gender == 1 ? 'Male' : 'Female').'</td>

//         <td>'.(!empty($dob) ? date('Y-m-d',strtotime($dob)) : null).'</td>

//         <td>'.$address.'</td>

//         </tr>

//         </tbody>

//         </table>

//         </body>

//         </html>';

//         $subject = "CarePro - New Registration";

//         $email = "info@carepro.com";

//         $check = new_send_mail($message, $subject, $email, '');



//       }



//       $message = array('message' => 'User has been added successfully. Please open the carepro tablet and do further process and enjoy our services.<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');

//       $this->session->set_flashdata('success', $message);



//       $getparam = $this->uri->segment(2);

//       $data['plan_id'] = base64_decode($getparam);



//       $this->template->set('title', 'Signup');

//       $this->template->load('home_layout', 'contents', 'signup', $data);

//     }

//   }else{



//    $getparam = $this->uri->segment(2);

//    $data['plan_id'] = base64_decode($getparam);

//    $this->template->set('title', 'Signup');

//    $this->template->load('home_layout', 'contents', 'signup', $data);

//  }

// }





// function random_strings($length_of_string) 

// { 





//   $str_result = '0123456789'; 



//   return substr(str_shuffle($str_result), 

//     0, $length_of_string); 

// } 

public function signup(){

	$data = array();
	$data['menuactive'] = $this->uri->segment(1);

	if(isset($_POST['nnsubmit']) && $_POST['nnsubmit'] !=''){

		//$this->form_validation->set_rules('username', 'username', 'trim|required|max_length[30]');

		$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('re_password', 'password confirmation', 'trim|required|matches[password]');
		$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[cp_users.email]');
		//$this->form_validation->set_rules('mobile', 'mobile', 'trim|required|min_length[5]|max_length[14]|numeric');

		$this->form_validation->set_rules('mobile', 'mobile', 'trim|required');
		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');

		//$this->form_validation->set_rules('gender', 'Gender', 'trim|required');
		//$this->form_validation->set_rules('address', 'Address', 'trim|required');
		//$this->form_validation->set_rules('user_id', 'User ID', 'trim|required');
		//$this->form_validation->set_rules('dob', 'DOB', 'trim|required');
		// New fileds as per the clients's requirement
		//$this->form_validation->set_rules('apartment', 'Apartment Name', 'trim|required');
		
		$this->form_validation->set_rules('login_id', 'Login ID', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('zipcode', 'Zip Code', 'trim|required');
		//$this->form_validation->set_rules('area_code', 'Area Code', 'trim|required');
		$this->form_validation->set_rules('street_name', 'Street name', 'trim|required');
		//$this->form_validation->set_rules('user_role', 'userrole', 'trim|required');

		if($this->form_validation->run() == false){
		  //echo die('fail');
		  $getparam = $this->uri->segment(2);
		  $data['plan_id'] = base64_decode($getparam);
		  $this->template->set('title', 'Signup');
		  $this->template->load('home_layout', 'contents', 'signup', $data);
		}else{

						//if(empty($this->input->post('tempUsr'))){
						$getparam = $this->uri->segment(2);
						$data['plan_id'] = base64_decode($getparam);
						$ip_addr = $this->input->ip_address();
						$str= $this->input->post('mobile');
						$mob = str_replace("-","",$str);
						//echo die('pass');
						//$otp = rand(0000,9999);
						$firstname = $this->input->post('firstname');
						$lastname = $this->input->post('lastname');
						//$username = $this->input->post('username');
						//$dob = $this->input->post('dob');
						$email = $this->input->post('email');
						$mobile = $mob;
						//$user_id = $this->input->post('user_id');
						//$gender = $this->input->post('gender');
						//$address = $this->input->post('address');
						$street_name = $this->input->post('street_name');
						$apartment = $this->input->post('apartment');
						$login_id = $this->input->post('login_id');
						$city = $this->input->post('city');
						$state = $this->input->post('state');
						$zipcode = $this->input->post('zipcode');
						$area_code = $this->input->post('area_code');
						//$company_school = $this->input->post('company_school');

						$digits = 4;
						$otp = rand(pow(10, $digits-1), pow(10, $digits)-1);
				
						$data = array(
							'username' =>$login_id,  
							'ip_address'=>$ip_addr,  
							'name' => $firstname,
							'lastname' => $lastname,
							'city'=>$city,
							'state'=>$state,
							'zipcode'=>$zipcode,
							'country_code'=>'',
							'company_school'=>'',
							'apartment'=>$apartment,
							'dob' => '',
							'street_name'=>$street_name,
							//'dob' => (!empty($dob) ? date('Y-m-d',strtotime($dob)) : null),
							'email' =>  $email,
							'mobile' => $mobile,
							'pass'=>$this->input->post('password'),
							'password' => md5($this->input->post('password')),
							'user_role' => 2,
							'create_user' => date('Y-m-d H:i:s'),
							'status' => 0,
							'otp' => $otp,
							'gender' => '',
								//'gender' => (!empty($gender) ? $gender : 1),
							'address' => '',
							'plan_id' => $data['plan_id'],
							'user_id' => (!empty($login_id) ? $login_id : null)
						);

						$insertId = $this->common_model->addRecords('cp_users_temprory', $data);

						if(!empty($insertId) && $insertId !=''){
							$user_id = base64_encode($insertId);
						}

						if(!empty($user_id) && $user_id !=''){
							$getparam = $this->uri->segment(2);
							$subsplan_id = base64_decode($getparam);
							redirect('make-payment/'.$getparam.'/'.$user_id);
						}

						//echo $insertId;
						//die();
						//$email = $data['email'];
						//$password = $this->input->post('password');
						//$message ="Your account has been Registered";
						//$message ="Please verify account this OTP".$otp;
						//*$message = 'Your user verify otp code:'.$otp;*/
						//$message = 'Dear '.$firstname.' '.$lastname.',<br><p>Welcome to our carepro app.</p><br><p>Please open tablet and please do further process and enjoy our services.</p><br><p>Best Wishes,</p><br><p>CarePro Team</p>';
						// $subject = "CarePro - Welcome";
						// sendEmail($email,$subject,$message);
						// $check = new_send_mail($message, $subject, $email, '');
						// $message = '<html>
						// <head>
						// <title></title>
						// </head>
						// <body>
						// <h2>Dear Admin,</h2>
						// <p>Please see user information.</p>
						// <table>
						// <thead>
							// <tr>
							// <th>User Name</th>
							// <th>Email</th>
							// <th>Contact No</th>
							// <th>User ID</th>
							// <th>Gender</th>
							// <th>Date Of Birth</th>
							// <th>Address</th>
							// </tr>
						// </thead>
						// <tbody>
							// <tr>

							// <td>'.$firstname.' '.$lastname.'</td>
							// <td>'.$email.'</td>
							// <td>'.$mobile.'</td>
							// <td>'.$user_id.'</td>
							// <td>'.($gender == 1 ? 'Male' : 'Female').'</td>
							// <td>'.(!empty($dob) ? date('Y-m-d',strtotime($dob)) : null).'</td>
							// <td>'.$address.'</td>
							// </tr>
						// </tbody>
						// </table>
						// </body>
						// </html>';
						// $subject = "CarePro - New Registration";
						// $email = "info@carepro.com";
						// $check = new_send_mail($message, $subject, $email, '');
			}

			// }else{

			/*
				$getparam = $this->uri->segment(2);

				$subsplan_id = base64_decode($getparam);
				$user_id= base64_encode($this->input->post('tempUsr'));
				redirect('make-payment/'.$getparam.'/'.$user_id);
			} */

			/*$message = array('message' => 'User has been added successfully please check your email and activate your account.<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');*/
			// $message = array('message' => 'User has been added successfully. Please open the carepro tablet and do further process and enjoy our services.<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');
			// $this->session->set_flashdata('success', $message);
			/*redirect('otp_verify?email='.base64_encode($email));*/

			$getparam = $this->uri->segment(2);
			$data['plan_id'] = base64_decode($getparam);
			$this->template->set('title', 'Signup');
			$this->template->load('home_layout', 'contents', 'signup', $data);

  }else{

		$tempU = $this->input->post('tempuser');
		$data['tempUserDetail'] = $this->common_model->getSingleRecordById('cp_users_temprory',array('id'=> $tempU));
		$getparam = $this->uri->segment(2);
		$data['plan_id'] = base64_decode($getparam);
		$this->template->set('title', 'Signup');
		$this->template->load('home_layout', 'contents', 'signup', $data);
	}
}
/*Created by 95 on 6-2-2019 For Resend OTP*/

public function resendverifyOTP(){

  $data = array();

  $data['error_msg']="";

  $data['success_msg'] = "";

  $email = $this->input->post('user');

  $checkUser = $this->common_model->getSingleRecordById('cp_users',array('email'=> $email));

  if(!empty($checkUser)){

//$otp = rand(0000,9999);

    $digits = 4;

    $otp = rand(pow(10, $digits-1), pow(10, $digits)-1);

    $where_condition = array('id' => $checkUser['id']);

    $upd = $this->common_model->updateRecords('cp_users',array('status' => 0,'otp' => $otp),$where_condition);

    if($upd >=1)

//$message = "One time verify otp password:".$otp;

      $message = 'One time verify otp password:'.$otp;

    $subject = "New user registratio";

//sendEmail($email, $subject, $message);

    $check= new_send_mail($message, $subject, $email, '');

// $this->session->set_flashdata('success_resend', 'OTP sended succcessfully.!');

    echo 1;

  }

}

public function signup1(){

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);

  if(isset($_POST['submit'])){

    $otp = rand(0000,9999);

    $data = array(

      'username' => $this->input->post('username'),

      'email' =>  $this->input->post('email'),

      'mobile' => $this->input->post('mobile'),

      'password' => md5($this->input->post('password')),

      'user_role' => 2,

      'create_user' => date('Y-m-d H:i:s'),

      'status' =>0,

      'otp' => $otp

    );

    $insertId = $this->common_model->addRecords('cp_users', $data);

    if ($insertId) {

      $email = $data['email'];

      $password = $this->input->post('password');

//$message = "Your account has been Registered. Please  <a href='".base_url()."activate/".$insertId."'>Activate account Login </a> with the following details.<br>Email:".$email."<br>Password:".$password." ";

      $message ="Your account has been Registered";

      $message ="Please verify account this OTP".$otp;

      $subject = "New user registration";

//sendEmail($email,$subject,$message);

      $check= new_send_mail($message, $subject, $email, '');

    }

    $message = array('message' => 'User has been added successfully please check your email and activate your account.<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');

    $this->session->set_flashdata('success', $message);

    redirect('otp_verify');

  }else{

    $this->template->set('title', 'Signup');

    $this->template->load('home_layout', 'contents', 'signup', $data);

  }

}


public function verify_account_otp()

{

  if(isset($_POST['otp_number'])){

    $otp = $_POST['otp_number'];

    $checkUser = $this->common_model->getSingleRecordById('cp_users',array('otp'=> $otp));

  if($checkUser && $checkUser['user_role'] !== 1 AND !empty($checkUser)){ //

    $where_condition = array('id' => $checkUser['id']);

    $upd = $this->common_model->updateRecords('cp_users',array('status' => 1),$where_condition);

    if($upd >=1)

      $data['id']=$checkUser['id'];

    echo 1;

    die();

  } else {

    echo 0;

    die();

  }

}

$data['user_email']= $this->input->get('email');

$this->template->set('title', 'OTP');

$this->template->load('home_layout', 'contents', 'verify_account_otp', $data);

}

public function login1(){

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);

  if(isset($_POST['submit'])){

    $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');

    $this->form_validation->set_rules('password', 'password', 'trim|required');

    if($this->form_validation->run() == false)

    {

      $this->template->set('title', 'Signup');

      $this->template->load('home_layout', 'contents', 'login', $data);

    }

    else{

      $con = array(

        'email' => $this->input->post('email'),

        'password' =>  md5($this->input->post('password')));

      $checkLogin = $this->common_model->checkRow($con);

      if($checkLogin){

        if($checkLogin['user_role'] != 1){

          if($checkLogin['status'] == 0){

            $this->session->set_flashdata('error', 'Your account is not activated yet, Please contact to support.');

            redirect('login');

          }

          else{

            $this->load->library('session');

            $this->session->set_userdata('isUserLoggedIn',TRUE);

            $this->session->set_userdata('logged_in', $checkLogin);

            redirect('dashboard_new');

            echo "Logged in successfully";

          }

        }else{

          $error = base64_encode('Invalid email or password');

          redirect('login?error='.$error);

        }

      }else{

        $error = base64_encode('Invalid email or password');

        redirect('login?error='.$error);

      }

    }

  }else{

    $this->template->set('title', 'Login');

    $this->template->load('home_layout', 'contents', 'login', $data);

  }

}

public function login(){

	if($this->session->userdata('logged_in')){ 
		redirect("dashboard_new");
	}
  
  $data = array();
  $data['menuactive'] = $this->uri->segment(1);
			
	$submit = isset($_POST['submit']) ? $_POST['submit'] : '';

	if(!empty($submit) && $submit !=''){

		$email = isset($_POST['email']) ? $_POST['email'] : '';
		
		if (preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$email)){
			$user_role_2=2;
			$werearr2=array('email'=>$email,'user_role' => $user_role_2);
		  $userdata2 = $this->common_model->getSingleRecordById('cp_users',$werearr2);


			$user_role_4=4;
			$werearr4=array('email'=>$email,'user_role' => $user_role_4);
		  $userdata4 = $this->common_model->getSingleRecordById('cp_users',$werearr4);


			if(!empty($userdata2) && $userdata2 !='' || !empty($userdata4) && $userdata4 !=''){

				$con = array(
							  'email' => $this->input->post('email'),
							  'password' =>  md5($this->input->post('password'))
							);

				$checkLogin = $this->common_model->checkRow($con);
			
				if(!empty($checkLogin) && $checkLogin !=''){
					
					if($checkLogin['user_role'] != 1){
						
						if($checkLogin['status'] == 0){

							$this->session->set_flashdata('error', 'User inactive or deleted by admin.');
							$this->template->load('login_layout', 'contents', 'login', $data);
							//$this->template->load('home_layout', 'contents', 'login', $data);

						}else{

							if(!empty($_POST["remember"])) { 
								setcookie ("email",$_POST["email"],time()+ 3600);
								setcookie ("remember",$_POST["remember"],time()+ 3600);
							} else {
								setcookie("email","");
								setcookie("remember","");
							}

							$this->load->library('session');
							$this->session->set_userdata('isUserLoggedIn',TRUE);
							$this->session->set_userdata('logged_in', $checkLogin);
							
							$loginsts = $this->session->userdata('logged_in');

							if($checkLogin['user_role'] == 2 || $checkLogin['user_role'] == 4){ redirect("dashboard_new");  }
							echo "Logged in successfully";
						}

					}else{

						// $error = base64_encode('The inserted Email is not associated with any account
						// ');
						// redirect('login?error='.$error);

						$this->session->set_flashdata('error', 'The inserted email is not associated with any account.');
						$this->template->load('login_layout', 'contents', 'login', $data);
						//$this->template->load('home_layout', 'contents', 'login', $data);
					}

				}else{
			
					//_dx('No data');

				  $this->session->set_flashdata('error', 'The inserted password is not correct.');
				  $this->template->load('login_layout', 'contents', 'login', $data);
					//$this->template->load('home_layout', 'contents', 'login', $data);
					// $error = base64_encode('The inserted Email is not associated with any account
					// ');
					// redirect('login?error='.$error);
				}

			}else{

			$this->session->set_flashdata('error', 'The inserted email is not associated with any account.');
			$this->template->load('login_layout', 'contents', 'login', $data);
			//$this->template->load('home_layout', 'contents', 'login', $data);
		  }

		}else{

		  $userdata = $this->common_model->getSingleRecordById('cp_users',array('username'=>$email,'user_role' => '2'));
			//_dx($userdata);

			if(!empty($userdata) && $userdata !=''){
				
				$con = array(
						 'username' => $this->input->post('email'),
						 'password' =>  md5($this->input->post('password'))
						);

				$checkLogin = $this->common_model->checkRow($con);

				if($checkLogin){

					if($checkLogin['user_role'] != 1){

						if($checkLogin['status'] == 0){
							$this->session->set_flashdata('error', 'User inactive or deleted by admin.');
							$this->template->load('login_layout', 'contents', 'login', $data);
							//$this->template->load('home_layout', 'contents', 'login', $data);
						}else{

							if(!empty($_POST["remember"])) {

							  setcookie ("email",$_POST["email"],time()+ 3600);
							  setcookie ("password",$_POST["password"],time()+ 3600);
							  setcookie ("remember",$_POST["remember"],time()+ 3600);
								//echo "Cookies Set Successfuly";

							} else {

							  setcookie("email","");
							  setcookie("password","");
							  setcookie("remember","");
								//echo "Cookies Not Set";
							}

							$this->load->library('session');
							$this->session->set_userdata('isUserLoggedIn',TRUE);
							$this->session->set_userdata('logged_in', $checkLogin);

							//redirect('dashboard');

							if($checkLogin['user_role'] == 2 || $checkLogin['user_role'] == 4){
							  redirect('dashboard_new');
							}
							echo "Logged in successfully";
						}
					}else{

						// $error = base64_encode('The inserted Email is not associated with any account
						// ');
						// redirect('login?error='.$error);

						$this->session->set_flashdata('error', 'The inserted username is not associated with any account.');
						$this->template->load('login_layout', 'contents', 'login', $data);
						//$this->template->load('home_layout', 'contents', 'login', $data);
					}

				}else{

					$this->session->set_flashdata('error', 'The inserted password is not correct.');
					$this->template->load('login_layout', 'contents', 'login', $data);
					//$this->template->load('home_layout', 'contents', 'login', $data);
					// $error = base64_encode('The inserted Email is not associated with any account
					// ');
					// redirect('login?error='.$error);

				}

			}else{

			  $this->session->set_flashdata('error', 'The inserted username is not associated with any account.');
			  $this->template->load('login_layout', 'contents', 'login', $data);
			  //$this->template->load('home_layout', 'contents', 'login', $data);

			}
		}
	}else{

	  $this->template->set('title', 'Login');
	  //$this->template->load('home_layout', 'contents', 'login', $data);
	  $this->template->load('login_layout', 'contents', 'login', $data);
	}
}





public function activate(){

  $id =  $this->uri->segment(2);

//fetch user details

  $user = $this->common_model->getSingleRecordById('cp_users',array('id'=>$id));

//if code matches

  if($user['status'] == 0){

//update user active status

    $data['status'] = 1;

    $query = $this->common_model->activate($data, $id);

    if($query){

      $this->session->set_flashdata('message', 'User activated successfully');

    }

    else{

      $this->session->set_flashdata('message', 'Something went wrong in activating account');

    }

  }

  redirect('login');

}



public function usercontact_query(){

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);

  if(isset($_POST['submit'])){

    $this->form_validation->set_rules('fullname', 'fullname', 'trim|required');

    $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');

    $this->form_validation->set_rules('comment', 'comment', 'trim|required');

    if($this->form_validation->run() == false)

    {

      $this->template->set('title', 'Contact');

      $this->template->load('page_layout', 'contents', 'contact', $data);

    }else{

      $data = array(

        'full_name' => $this->input->post('fullname'),

        'email' =>  $this->input->post('email'),

        'comment' => $this->input->post('comment'),

        'create_date' => date('Y-m-d H:i:s'),

        'status' =>0

      );

      $insertId = $this->common_model->addRecords('contact_us', $data);

      if ($insertId) {

        $message = "Your message has been sumitted.";

        $this->session->set_flashdata('success', $message);

        redirect('contact');

      }

      $message = array('message' => 'Your message has not been sumitted.');

      $this->session->set_flashdata('success', $message);

      redirect('contact');

    }

  }else{

    $this->template->set('title', 'Contact');

    $this->template->load('page_layout', 'contents', 'contact', $data);

  }

}



public function userResetPassword() {

  $data = array();

  $data['menuactive'] = $this->uri->segment(1);

  if($this->input->post('submit')){

    $this->form_validation->set_rules('new_password', 'new password', 'required');

    $this->form_validation->set_rules('confirm_password', 'password confirmation', 'trim|required|matches[new_password]');

    $id = $this->input->post('user_id');

    $new_password = md5($this->input->post('new_password'));

    if ($this->form_validation->run() == true) {

      $checkUser = $this->common_model->getSingleRecordById('cp_users',array('id'=> $id));

//print_r($checkUser); die;

if($checkUser && $checkUser['user_role'] !== 1){ //customer

  $where_condition = array('id' => $id);

  $upd = $this->common_model->updateRecords('cp_users',array('password' => $new_password),$where_condition);

  if($upd >=1)

//$data['success_msg'] = 'Your reset password succcessfully.';

    $this->session->set_flashdata('success', 'Your password  reset successfully.');

  $this->template->load('home_layout', 'contents', 'otp_page', $data);

  redirect('login');

} else {

//$data['error_msg'] = 'Something Wrong, please try again.';

  $this->session->set_flashdata('error', 'Something Wrong, please try again.');

}

}

}

$this->template->set('title', 'Reset Password');

$this->template->load('home_layout', 'contents', 'reset_password', $data);

}



public function generateRandomString($length = 12) {

  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

  $charactersLength = strlen($characters);

  $randomString = '';

  for ($i = 0; $i < $length; $i++) {

    $randomString .= $characters[rand(0, $charactersLength - 1)];

  }

  return $randomString;

}



public function ForgetPassword(){

  $data = array();

  $data['error_msg']="";

  $data['success_msg'] = "";

  if($this->input->post('submit')) {

    $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email');

    $email = $this->input->post('user_email');

    if ($this->form_validation->run() == true) {

      $checkUser = $this->common_model->getSingleRecordById('cp_users',array('email'=> $email));

      if(!empty($checkUser)){

//$otp = rand(0000,9999);

        $digits = 4;

        $otp = rand(pow(10, $digits-1), pow(10, $digits)-1);

        $where_condition = array('id' => $checkUser['id']);

        $upd = $this->common_model->updateRecords('cp_users',array('status' => 0,'otp' => $otp),$where_condition);

        if($upd >=1)







          $TemplateData = $this->common_model->getSingleRecordById('mail_template',array('template_id'=> 2));





        //$findArray=array("%otp%");

        //$replaaceArray=array($otp);



        $description= str_replace("%otp%",$otp,$TemplateData['description']);

        $message = $description;

        //$message = "One time reset otp password:".$otp;

        $subject = $TemplateData['subject'];

//$check= sendEmail($email, $subject, $message);

        $check= new_send_mail($message, $subject, $email, '');



        if($check==1){





          $this->session->set_flashdata('success_msg', 'Your password reset OTP successfully send please check your email.');

          redirect('otp_page?email='.base64_encode($email));



        }else{



          $this->session->set_flashdata('errorr', 'There is something wrong please try again.');





//redirect('otp_page?email='.base64_encode($email));

          redirect('forgotpassword');



        }



// $this->session->set_flashdata('success_msg', 'Your password reset OTP successfully send please check your email.');

// redirect('otp_page?email='.base64_encode($email));

      }else{

        $this->session->set_flashdata('errorr', 'The inserted email is not associated with any account.');

        $this->template->load('home_layout', 'contents', 'forgot-password', $data);

      }

    }

  }

  $this->template->set('title', 'Forgot Password');

  $this->template->load('home_layout', 'contents' , 'forgot-password', $data);

}

/*Created by 95 on 6-2-2019 For Resend OTP*/

public function resendOtp(){

  $data = array();

  $data['error_msg']="";

  $data['success_msg'] = "";

  $email = $this->input->post('user');

  $checkUser = $this->common_model->getSingleRecordById('cp_users',array('email'=> $email));

  if(!empty($checkUser)){

//$otp = rand(0000,9999);

    $digits = 4;

    $otp = rand(pow(10, $digits-1), pow(10, $digits)-1);

    $where_condition = array('id' => $checkUser['id']);

    $upd = $this->common_model->updateRecords('cp_users',array('status' => 0,'otp' => $otp),$where_condition);

    if($upd >=1)



      $TemplateData = $this->common_model->getSingleRecordById('mail_template',array('template_id'=> 2));





        //$findArray=array("%otp%");

        //$replaaceArray=array($otp);



    $description= str_replace("%otp%",$otp,$TemplateData['description']);

    $message = $description;



    $subject = $TemplateData['subject'];

      //$message = "One time reset otp password:".$otp;

    //$subject = "Care Pro Forgot password";

//sendEmail($email, $subject, $message);

    $check= new_send_mail($message, $subject, $email, '');

// $this->session->set_flashdata('success_resend', 'OTP sended succcessfully.!');

    echo 1;

  }

}

// public function otp_page()

// {

//     $data = array();

//     $data['menuactive'] = $this->uri->segment(1);

//     if($this->input->post('submit')){

//         $this->form_validation->set_rules('otp_number', 'otp', 'required');

//         $otp = $this->input->post('otp_number');

//         if ($this->form_validation->run() == true) {

//           $checkUser = $this->common_model->getSingleRecordById('cp_users',array('otp'=> $otp));

//           //print_r($checkUser); die;

//           if($checkUser && $checkUser['user_role'] !== 1){ //customer

//             $where_condition = array('id' => $checkUser['id']);

//             $upd = $this->common_model->updateRecords('cp_users',array('status' => 1),$where_condition);

//             if($upd >=1)

//               $data['id']=$checkUser['id'];

//               $this->template->load('home_layout', 'contents', 'reset_password', $data);

//           } else {

//             //$data['error_msg'] = 'Wrong Otp, please try again.';

//             $this->session->set_flashdata('error', 'Wrong OTP, please try again.');

//             $this->template->load('home_layout', 'contents', 'otp_page', $data);

//           }

//         }

//     }

//     $data['userEmail']= $this->input->get('email');

//     $this->template->set('title', 'OTP');

//     $this->template->load('home_layout', 'contents', 'otp_page', $data);

// }

public function otp_page()

{

  if(isset($_POST['otp_number'])){

    $otp = $_POST['otp_number'];

    $checkUser = $this->common_model->getSingleRecordById('cp_users',array('otp'=> $otp));

  //print_r($checkUser); die;

  if($checkUser && $checkUser['user_role'] !== 1 AND !empty($checkUser)){ //customer

    $where_condition = array('id' => $checkUser['id']);

    $upd = $this->common_model->updateRecords('cp_users',array('status' => 1),$where_condition);

    if($upd >=1)

      $data['id']=$checkUser['id'];

    $this->session->set_userdata(array(

      'user_id'  => $checkUser['id'],

    ));

    echo 1;

    die();

  } else {

    echo 0;

    die();

  }

}

//$data['id']=$checkUser['id'];

$data['userEmail']= $this->input->get('email');

$this->template->set('title', 'OTP');

$this->template->load('home_layout', 'contents', 'otp_page', $data);

}



private function username_exists($email)

{

  $this->db->where('username', $email);

  $query = $this->db->get('cp_users');

  if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; }

}



function register_username_exists()

{

  if (array_key_exists('user_id',$_POST)) {

    if ( $this->username_exists($this->input->post('user_id')) == TRUE ) {

      echo json_encode(FALSE);

    } else {

      echo json_encode(TRUE);

    }

  }

}



public function validateData() {

  if ($this->input->get("email") != '') {



    $email = $this->input->get('email');

    $user_info = $this->common_model->getSingleRecordById('cp_users',array('email'=>$email));

    if (empty($user_info['id'])) {

      echo 'true';

    } else {

      echo 'false';

    }

// $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[cp_users.email]');

// if ($this->form_validation->run() == true) {

//   //die('true');

//   echo "true";

// } else {

//   //die('false');

//   echo "false";

// }

  }

  if ($this->input->get("mobile") != '') {



    $mobile = $this->input->get('mobile');

    $user_info = $this->common_model->getSingleRecordById('cp_users',array('mobile'=>$mobile));

    if (empty($user_info['id'])) {

      echo 'true';

    } else {

      echo 'false';

    }

// $this->form_validation->set_rules('mobile', 'Mobile', 'required|is_unique[cp_users.mobile]');

// if ($this->form_validation->run() == true) {

//   die('true');

// } else {

//   die('false');

// }

  }

  if ($this->input->get("username") != '') {

    $username = $this->input->get('username');

    $user_info = $this->common_model->getSingleRecordById('cp_users',array('username'=>$username));

    if (empty($user_info['id'])) {

      echo 'true';

    } else {

      echo 'false';

    }

// $this->form_validation->set_rules('username', 'Username', 'required|is_unique[cp_users.username]');

// if ($this->form_validation->run() == true) {

//   die('true');

// } else {

//   die('false');

// }

  }



  if ($this->input->get("login_id") != '') {

    $login_id = $this->input->get('login_id');

    $user_info = $this->common_model->getSingleRecordById('cp_users',array('user_id'=>$login_id));

    if (empty($user_info['id'])) {

      echo 'true';

    } else {

      echo 'false';

    }

// $this->form_validation->set_rules('username', 'Username', 'required|is_unique[cp_users.username]');

// if ($this->form_validation->run() == true) {

//   die('true');

// } else {

//   die('false');

// }

  }

}
	public function getDeviceStatus(){
		$login_id = $this->session->userdata('logged_in')['id'];
		if($login_id != '' && $login_id != NULL) {
			try {
				$device_info = $this->common_model->getSingleRecordById('device_details', array('device_user_id' => $login_id));
				$time = isset($device_info['device_last_online']) ? $device_info['device_last_online'] : '';
				if ($time != '') {
					$startdateObj = new \DateTime($time);
					$enddateObj = new \DateTime(date('Y-m-d H:i:s'));
					$diff = $startdateObj->diff($enddateObj);
					$duration = ($diff->days * 24 * 60 * 60) + ($diff->h * 60) + $diff->i;
					echo json_encode(array('duration' => $duration));
				}
			} catch (Exception $ex) {
				echo $ex->getMessage();
			}
		}
		exit();
	}

	/*
 * To show greeting message in header basis on time
 *@return: string
 */
	function showGreetingMessage(){
		$hour = $this->input->post('hour');
		$flag_wish = "";
		if ( $hour >= 3 && $hour <= 11 ) {
			$flag_wish = "Good Morning";
		} else if ( $hour >= 12 && $hour <= 15 ) {
			$flag_wish = "Good Afternoon";
		} else if ( $hour >= 16 && $hour <= 23 ) {
			$flag_wish = "Good Evening";
		}
		echo json_encode(array('message' => $flag_wish));
		exit();
	}


}
