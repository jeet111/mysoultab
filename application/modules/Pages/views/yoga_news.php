<style>
    tbody tr td:last-child {
        text-align: left;
    }
    .addNote a, .btn.btn-primary {
    max-width: 159px !important;
}
    .rem_add_button {
        width: 100%;
        /* text-align: center; */
        float: left;
        /* margin: 0 auto; */
        display: block;
        text-align: center;
        margin-top: 13px;
    }

  
    .btn-primary:hover {
        color: #000 !important;
    }

    .serv.col-md-3 {
        text-align: center;
        box-shadow: rgb(100 100 111 / 10%) 0px 7px 29px 0px;
        padding: 14px 0;
        margin-top: 20px;
    }

    .rem_add_button a {
        /* float: left; */
        /* padding: 8px 20px; */
        /* box-sizing: border-box; */
        /* border-radius: 34px; */
        /* text-align: center; */
        font-size: 19px;
        margin-right: 10px;
        margin-top: 20px;
    }
    img.game.app {
    height: auto !important;
    width: auto !important;
}
    .serv.col-md-3 .btn-primary {
        color: #000;
        background-color: transparent !important;
        /* border-color: #2e6da4; */
    }
    .modal-body .row {
    margin-bottom: 10px;
}
    table#sampleTable {
        table-layout: fixed;
        text-align: left;
    }

    td.lcol a i {
        font-style: normal;
    }

    td.lcol a {
        margin-right: 5px;
    }

    .tab_medi td a.edit_btn {
        background: #1f569e;
        color: #fff;
        padding: 10px 17px;
        max-width: 139px;
        width: 100%;
        border-radius: 41px;
    }

    .serv.col-md-3 h4 {
        font-size: 22px;
        color: #000;
    }

    .rem_add {
        margin-bottom: 14px;
        margin-top: 8px;
    }

</style>
<div class="text_heading">
    <div id="msg_show"></div>

    <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success message" id="success">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php } ?>
    <h3 class="text-center">
        <i class="fa fa-plus-square-o" aria-hidden="true"></i> Yoga News
    </h3>

    <div class="rem_add">
        <a href="#addyogaNewsModal" class="btn button btn-primary" data-toggle="modal">Add Yoga News</i> </a>
    </div>
</div>
<div class="row websiteAppClass">

<?php
if (!empty($yoganews_list)) {

    foreach ($yoganews_list as $value) {
    //    echo "<pre>";
    //    print_r($value);
      
?>
        <div class="serv col-md-3 bg-info">
            <h4>
                <?php echo $value->button_name; ?>

            </h4>

            <?php
            if ($value->social_type == "Website") {
            ?>

                <img class="game website" src="https://www.google.com/s2/favicons?domain=<?php echo $value->url; ?>" data-url="<?php echo $value->url; ?>">
            <?php } else {
               $app_list_data = $this->common_model_new->getAllwhere("app_list",array('id'=>$value->app_name));
           // _d($app_list_data);
            ?>
            <!-- <img class="game app" src='http://www.google.com/s2/favicons?domain=<?php echo $value->url; ?>' /> -->

                <img class="game app" src="<?php if(isset($app_list_data[0]->icon)){ echo $app_list_data[0]->icon; } ?>">

            <?php } ?>
            <div class="rem_add_button">
                <a href="#edityogaNewsModal" class="model-content edit edit_btn btn-primary" data-toggle="modal">
                    <i class="material-icons update" data-toggle="tooltip" data-socialid="<?php echo $value->id; ?>" data-socialid="<?php echo $value->social_type; ?>" data-url="<?php echo $value->url; ?>" data-app_name="Yoga News" data-button_name="<?php echo $value->button_name; ?>" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></i>
                </a>
                <a href="#deleteyogaNewsModal" class="model-content delete" data-id="<?php echo $value->id; ?>
                                            " data-toggle="modal"><i class="fa fa-trash" aria-hidden="true"></i></a>

            </div>
        </div>

    <?php }
} else { ?>
        <div class="nodataContainer"><h4 class="nodataFound">No Data Found!</h4></div>
<?php } ?>
</div>
<!-- Add Social News Modal -->
<div id="addyogaNewsModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="transportationform">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Yoga News</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">




                        <div class="row">


                            <div class="col-lg-3">
                                <label for="name"><b>News Type:</b></label>
                            </div>
                            <div class="col-lg-9 padding">

                                <label class="checkbox-inline">
                                    <input type="radio" class="yoganews_type" name="yoganews_type" data="_transportation" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="yoganews_type" value="App" data="_transportation" name="yoganews_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-9">
                                <input type="text" name="button_name" id="add_yoga_news">
                                <span style="color:red" id="add_yoga_news_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_yoganews">
                            <div class="col-lg-3">
                                <label for="yoganews"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-9">
                                <input type="text" name="url" id="url_add_yoganews">
                                <span style="color:red" id="url_add_yoganews_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_yoganews" style="display:none">
                            <div class="col-lg-3">
                                <label for="yoganews"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-9">
                                <select id="appname_add_yoganews" name="app_name" class="form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="appname_add_yoganews_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="1" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-success" id="btn-add-transportation">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Modal HTML -->
    <div id="edityogaNewsModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="update_form_yoganews">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Yoga News</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_u_yoganews" name="id" class="form-control" required>
                        <div class="row">


                            <div class="col-lg-4 margin">
                                <label for="name"><b>Yoga News Type:</b></label>
                            </div>
                            <div class="col-lg-8 padding" id="yoganews_u">

                                <label class="checkbox-inline">
                                    <input type="radio" class="yoganews_type" name="yoganews_type" data="_u_yoganews" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="yoganews_type" value="App" data="_u_yoganews" name="yoganews_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 margin">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_u_yoganews" class="editgamemyModal_button_name">
                                <span style="color:red" id="button_name_u_yoganews_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_u_yoganews">
                            <div class="col-lg-4 margin">
                                <label for="transportation"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_u_yoganews" class="editgamemyModal_url">
                                <span style="color:red" id="url_u_yoganews_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_u_yoganews" style="display:none">
                            <div class="col-lg-4 margin">
                                <label for="transportation"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8 padding">
                                <select name="app_name" id="app_name_u_yoganews" class="editgamemyModal_app_name form-select">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="app_name_u_yoganews_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="2" name="type">
                        <input type="hidden"  data-socialid="<?php echo $value->id; ?>" name="hiddenID" id="hiddenID">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-info" id="update_yogaNews">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="deleteyogaNewsModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>

                    <div class="modal-header">
                        <h4 class="modal-title">Delete transportation </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_d_transportation" name="id" class="form-control">
                        <p>Are you sure you want to delete these Records?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-danger" id="delete_transportation">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script type="text/javascript">
      $(".yoganews_type").click(function() {
            var value_game = $(this).val();
            // alert(value_game);
            var data_up = $(this).attr("data");
            if (value_game == 'App') {
                $("#Website_yoganews").hide();
                $("#App_yoganews").show();
            } else {
                $("#Website_yoganews").show();
                $("#App_yoganews").hide();
            }
        });
   $(document).on('click', '#btn-add-transportation', function(e) {
            var add_yoga_news = $("#add_yoga_news").val();
            var url_add_yoganews = $("#url_add_yoganews").val();
            var appname_add_yoganews = $("#appname_add_yoganews").val();
            var yoganews_type = $('input[name="yoganews_type"]:checked').val();
            var validate_flag_transportation = 0;

            if (add_yoga_news == '') {
                validate_flag_transportation = 1;
                $("#add_yoga_news_error").text("Button Name is Required");
            } else {
                validate_flag_transportation = 0;
                $("#add_yoga_news_error").text("");
            }

            if (yoganews_type == 'App') {
                if (appname_add_yoganews == '') {
                    validate_flag_transportation = 1;
                    $("#appname_add_yoganews_error").text("App Name is required");
                } else {
                    validate_flag_transportation = 0;
                    $("#appname_add_yoganews_error").text("");
                }
            } else {
                if (url_add_yoganews == '') {
                    validate_flag_transportation = 1;
                    $("#url_add_yoganews_error").text("Url is required");
                } else {
                    if (validURL(url_add_yoganews)) {
                        validate_flag_transportation = 0;
                        $("#url_add_yoganews_error").text("");
                    } else {
                        validate_flag_transportation = 1;
                        $("#url_add_yoganews_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#transportationform").serialize();
            if (validate_flag_transportation != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "<?php echo base_url();?>Pages/YogaNews/add_yoga_news",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#addyogaNewsModal').modal('hide');
                        //alert("transportation added successfully");
                        location.reload();

                    }
                });
            }

        });
        $(document).on('click', '.update', function(e) {
            var newsId = $(this).attr("data-socialid");
            var transportationtype = $(this).attr("data-transportationtype");
            var url = $(this).attr("data-url");
            var app_name = $(this).attr("data-app_name");
            var button_name = $(this).attr("data-button_name");
            $('#id_u_yoganews').val(newsId);
            $('#yoganews_u').find(':radio[name=yoganews_type][value="' + transportationtype + '"]').prop('checked', true);
            $('#url_u_yoganews').val(url);
            $('#app_name_u_yoganews').val(app_name);
            $('#button_name_u_yoganews').val(button_name);
            if (transportationtype == 'App') {
                $("#Website_u").hide();
                $("#App_u").show();
            } else {
                $("#Website_u").show();
                $("#App_u").hide();
            }
        });

        $(document).on('click', '#update_yogaNews', function(e) {
            var button_name_u_yoganews = $("#button_name_u_yoganews").val();
            var id = $("#hiddenID").attr("data-socialid");
         
            var url_u_yoganews = $("#url_u_yoganews").val();
            var app_name_u_yoganews = $("#app_name_u_yoganews").val();
            var yoganews_type = $('input[name="yoganews_type"]:checked').val();
            var validate_flag_transportation_u = 0;

            if (button_name_u_yoganews == '') {
                validate_flag_transportation_u = 1;
                $("#button_name_u_yoganews_error").text("Button Name is Required");
            } else {
                validate_flag_transportation_u = 0;
                $("#button_name_u_yoganews_error").text("");
            }

            if (yoganews_type == 'App') {
                if (app_name_u_yoganews == '') {
                    validate_flag_transportation_u = 1;
                    $("#app_name_u_yoganews_error").text("App Name is required");
                } else {
                    validate_flag_transportation_u = 0;
                    $("#app_name_u_yoganews_error").text("");
                }
            } else {
                if (url_u_yoganews == '') {
                    validate_flag_transportation_u = 1;
                    $("#url_u_yoganews_error").text("Url is required");
                } else {
                    if (validURL(url_u_yoganews)) {
                        validate_flag_transportation_u = 0;
                        $("#url_u_yoganews_error").text("");
                    } else {
                        validate_flag_transportation_u = 1;
                        $("#url_u_yoganews_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#update_form_yoganews").serialize();
            console.log(data);
            if (validate_flag_transportation_u != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "<?php echo base_url();?>Pages/YogaNews/edit_yoganews",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#edityogaNewsModal').modal('hide');
                        // alert("transportation Updated successfully");
                        location.reload();
                    }
                });
            }

        });
        $(document).on("click", ".delete", function() {
            var id = $(this).attr("data-id");
            $('#id_d_transportation').val(id);

        });
        $(document).on("click", "#deleteyogaNewsModal", function() {
            $.ajax({
                url: "<?php echo base_url();?>Pages/YogaNews/delete_yoganews",
                type: "POST",
                cache: false,
                data: {
                    type: 3,
                    id: $("#id_d_transportation").val()
                },
                success: function(dataResult) {
                    location.reload();
                    $('#deletetransportationmyModal').modal('hide');
                }
            });
        });
</script>