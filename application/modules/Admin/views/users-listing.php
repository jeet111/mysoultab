<?php

if (isset($staff_permission[2]['add_permission']) == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn"><a href="<?php echo base_url(); ?>admin/add/user">Add User</a></div>

	</div>

	<?php

} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn"><a href="<?php echo base_url(); ?>admin/add/user">Add User</a></div>

	</div>

	<?php	

}

?>



<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="card-body">

			<div class="status-cng"></div>

			<?php

			if($this->session->flashdata('success')) {

				$message = $this->session->flashdata('success');

				?>

				<div class="<?php echo $message['class'] ?>"><?php echo $message['message']; ?></div>

			<?php } ?>

			 <!-- <div class="bx_user">

	<div class="add-btn"><a href="<?php echo base_url(); ?>admin/add/user">Add User</a></div>

</div> -->



<div id="deletemsg"></div>





<table class="table table-hover tab_comn " id="sampleTable">

	<thead>

		<tr>

			<th>S no.</th>

			<th>Username</th>

			<th>Email</th>

			<th>Status</th>

			<!-- <th>Activation Status</th> -->

			<th>Action</th>

		</tr>

	</thead>

	<tbody>
				<?php //_dx($users_data); ?>
		<?php foreach ($users_data as $key => $user) {?>

			<tr>

				<td><?php echo ($key+1); ?></td>

				<td><?php echo $user['username']; ?></td>

				<td><?php echo $user['email']; ?></td>


				<td>
					<?php  
						$user_status = $user['status']; 
						$user_user_verify_by_app = $user['user_verify_by_app']; 
						//$user_device_verify = $user['device_verify']; 
						$user_device_verify_by_token = $user['device_verify_by_token']; 
					?>

					<?php if($user_status ==1 && $user_user_verify_by_app==1 && $user_device_verify_by_token==1){ ?> 

						<?php echo "<button type='' class='btn ybt btn-xs'  disabled>Active</button>"; ?>

					<?php }else{ ?>

						<?php echo "<button type='' class='btn xbt btn-xs'  disabled>Inactive</button>"; ?>

					<?php } ?>



				</td>

				

				<!-- <td id="btton_change_<?php echo $user['id']; ?>">

				<?php if($user['conversation_status']==1){ ?>

						<button type='button' data-activation='1' data-id='<?php echo $user['id']; ?>' class='btn ybt btn-xs activation_cls'  >Active</button>

					<?php }else{ ?>

						<button type='button' data-activation='0' data-id='<?php echo $user['id']; ?>' class='btn xbt btn-xs activation_cls'  >Inactive</button>

					<?php } ?>

				

				</td> -->

				

				<td>

					<div class="link-del-view">



						<?php

						if (isset($staff_permission[2]['view_permission']) == 1) {

							?>

							<div class="tooltip-2">

								<a href="admin/view_person/user/<?php echo $user['id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

								<span class="tooltiptext">View</span>

							</div>

							<?php

						} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

							?>

							<div class="tooltip-2">

								<a href="admin/view_person/user/<?php echo $user['id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

								<span class="tooltiptext">View</span>

							</div>

							<?php

						}

						?>

						



						<?php

						if (isset($staff_permission[2]['edit_permission']) == 1) {

							?>

							<div class="tooltip-2">

								<a href="admin/edit/user/<?php echo $user['id']; ?>">

									<i class="fa fa-edit" aria-hidden="true"></i>

								</a>

								<span class="tooltiptext">Edit</span>

							</div>

							<?php

						} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

							?>

							<div class="tooltip-2">

								<a href="admin/edit/user/<?php echo $user['id']; ?>">

									<i class="fa fa-edit" aria-hidden="true"></i>

								</a>

								<span class="tooltiptext">Edit</span>

							</div>

							<?php

						}

						?>

						

						<?php

						if (isset($staff_permission[2]['delete_permission']) == 1) {

							?>

							<div class="tooltip-2">

								<a href="javascript:void(0);" data-id="<?php echo $user['id']; ?>" id="deletemusiccat_<?php echo $user['id']; ?>">

									<i class="fa fa-trash-o" aria-hidden="true"></i>

								</a>

								<span class="tooltiptext">

									Delete

								</span>

								<!-- <a href="javascript:void(0)" onclick="deleteStatus(<?php echo $user['id']; ?>,'admin/delete/user')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span> -->

							</div>

							<?php

						} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

							?>

							<div class="tooltip-2">

								<a href="javascript:void(0);" data-id="<?php echo $user['id']; ?>" id="deletemusiccat_<?php echo $user['id']; ?>">

									<i class="fa fa-trash-o" aria-hidden="true"></i>

								</a>

								<span class="tooltiptext">

									Delete

								</span>

								<!-- <a href="javascript:void(0)" onclick="deleteStatus(<?php echo $user['id']; ?>,'admin/delete/user')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span> -->

							</div>

							<?php

						}

						?>

						





							<?php 	/* ?>

						<?php

						if ($staff_permission[2]['edit_permission'] == 1) {

							?>

							<?php if($user['status'] == 1) { ?>

								<div class="tooltip-2"><a href="javascript:void(0);" onclick="return updateStatus(<?php echo $user['id']; ?>, 'admin/user/status');"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>

									<span class="tooltiptext">Active</span>

								</div>

							<?php }else { ?>

								<div class="tooltip-2"><a href="javascript:void(0);" onclick="return updateStatus(<?php echo $user['id']; ?>, 'admin/user/status');"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>

									<span class="tooltiptext">Deactive</span>

								</div>

							<?php } ?>

							<?php

						} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

							?>

							<?php if($user['status'] == 1) { ?>

								<div class="tooltip-2"><a href="javascript:void(0);" onclick="return updateStatus(<?php echo $user['id']; ?>, 'admin/user/status');"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>

									<span class="tooltiptext">Active</span>

								</div>

							<?php }else { ?>

								<div class="tooltip-2"><a href="javascript:void(0);" onclick="return updateStatus(<?php echo $user['id']; ?>, 'admin/user/status');"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>

									<span class="tooltiptext">Deactive</span>

								</div>

							<?php } ?>

							<?php

						}

						?>



						<?php 	*/ ?>

						

					</div>

				</td>

			</tr>

		<?php } ?>

	</tbody>

</table>

</div>

</div>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>



<script type="text/javascript">







	$(document).on('click','[id^="deletemusiccat_"]',function(){

		var delete_music = $(this).attr('data-id');

		if(confirm("Are you sure, you want to delete this user ?")){

			$.ajax({

				type: "POST",

				url: '<?php echo base_url(); ?>Admin/Admin_user/delete_persone/',

				data: 'delete_music='+delete_music,

            //dataType: "json",

            success: function(data)

            {

            	if(data==1){

            		setTimeout(function(){

            			$("#deletemsg").html("<div class='alert alert-success'>Deleted Successfully.</div>");

            			location.reload();



            		}, 1000);

            	}

            }

        });

			$(this).parents(".card_one").animate({ backgroundColor: "#fbc7c7" }, "fast")

			.animate({ opacity: "hide" }, "slow");

		}

	});

	

	

	

	$(document).on('click','.activation_cls',function(){

		

		var activation_id = $(this).attr('data-activation');

		var user_id = $(this).attr('data-id');	

		$.ajax({

			type: "POST",

			url: '<?php echo base_url(); ?>Admin/Admin_user/activation_status/',

			data: 'activation_id='+activation_id+'&user_id='+user_id,

            //dataType: "json",

            success: function(data)

            {

            	if(data == 1){

            		$("#btton_change_"+user_id).html("<button type='button' data-activation='1' data-id='"+user_id+"' class='btn ybt btn-xs activation_cls'  >Active</button>");

            	}else{

            		$("#btton_change_"+user_id).html("<button type='button' data-activation='0' data-id='"+user_id+"' class='btn xbt btn-xs activation_cls'  >Inactive</button>");

            	}

            }

        });

	});	

</script>
