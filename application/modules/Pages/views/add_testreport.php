<style>
  img#output {
    width: 34%;
  }
  .coupon_checkbox input {
    margin-right: 6px !important;
  }
  .back_reminder .btn.btn-primary {
    margin: 0px 0 16px !important;
  }
  .form-group>label:not(.error) {
    color: #000;
  }
  .coupon_checkbox h3 {
    color: #000;
    padding-left: 14px;
    font-weight: bold;
    font-size: 18px;
    margin-top: 0;
    margin-bottom: 20px;
  }
  #flupmsgs {
    color: Red;
  }
  #add_testreport input.btn.btn-primary {
    max-width: 151px !important;
  }
  .text_heading {
    border-bottom: 0;
  }
</style>
<div class="text_heading">
  <h3 class="text-center">
    <!-- <i class="fa fa-paper-plane"></i>  -->
    <i class="fa fa-file-text" aria-hidden="true"></i>
    Add Test Report
  </h3>
  <div class="back_reminder"><a href="<?php echo base_url(); ?>testreport_list" class="btn btn-primary">Back</a></div>
</div>
<?php //echo "<pre>"; print_r($Testtypes); echo "<pre>"; 
?>
<section class="content photo-list">
  <div class="photo-listMain">
    <form id="add_testreport" enctype="multipart/form-data" method="post" class="frm_add">
      <div class="row">
        <div class="coupon_checkbox">
          <h3><input class="coupon_question" type="checkbox" name="coupon_question" value="1" onchange="valueChanged()" />Select Other Options</h3>
          <div class="answer">
            <div class="col-sm-6 col-md-6">
              <div class="form-group">
                <label>if your doctor name is not listed please enter your Doctor name here</label>
                <input type="text" name="other_doctor_name" class="form-control" id="other_doctor_name" placeholder="Other Doctor Name" value="" maxlength="100">
                <?php echo form_error('other_doctor_name'); ?>
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="form-group">
                <label>Email Address</label>
                <input type="text" name="other_doctor_email_address" class="form-control" id="other_doctor_email_address" placeholder="Email Address" value="" maxlength="100">
                <?php echo form_error('other_doctor_email_address'); ?>
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="form-group">
                <label>Fax Number</label>
                <input type="text" name="other_doctor_fax_number" class="form-control" id="other_doctor_fax_number" placeholder="Fax Number" value="" maxlength="100">
    
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="form-group">
                <label>Address</label>
                <input type="text" name="other_doctor_address" class="form-control" id="other_doctor_address" placeholder="Address" value="" maxlength="100">
    
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="form-group">
                <label>Phone Number</label>
                <input type="text" name="other_doctor_phone_number" class="form-control" id="other_doctor_phone_number" placeholder="Phone Number" value="" maxlength="100">
    
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="form-group">
                <label>if your Test type name is not listed please enter your Test type here</label>
                <input type="text" name="other_testreport_name" class="form-control" id="" placeholder="Other Test type" value="" maxlength="100">
                <?php echo form_error('other_testreport_name'); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-md-6">
          <div class="form-group">
            <label>Test Report Title<font style="color:red;">*</font>:</label>
            <input type="text" name="report_title" class="form-control valid" id="" placeholder="Test Report Title" value="" maxlength="100">
            <?php echo form_error('report_title'); ?>
          </div>
        </div>
        <div class="col-sm-6 col-md-6">
          <div class="form-group">
            <label>Select Doctor<font style="color:red;">*</font>:</label>
            <select name="doctor" class="form-control" id="doctorName">
              <option value="">Select Doctor</option>
              <?php foreach ($Doctors as $Doctor) { ?>
                <option value="<?php echo $Doctor->doctor_id; ?>"><?php echo ucwords($Doctor->doctor_name); ?></option>
              <?php } ?>
            </select>
            <?php echo form_error('doctor'); ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-md-6">
          <div class="form-group">
            <label>Select Test Type<font style="color:red;">*</font>:</label>
            <select name="test_type" class="form-control" id="testType">
              <option value="">Select Test Type</option>
              <?php foreach ($Testtypes as $type) { ?>
                <option value="<?php echo $type->id; ?>"><?php echo $type->test_type; ?> </option>
              <?php } ?>
            </select>
            <?php echo form_error('test_type'); ?>
          </div>
          <div class="form-group">
            <label>Description About Medicine<font style="color:red;">*</font>:</label>
            <textarea class="form-control" name="report_description" rows="3" placeholder="Write Description Here About Medicine."></textarea>
            <?php echo form_error('report_description'); ?>
          </div>
        </div>
        <div class="col-sm-6 col-md-6">
          <div class="form-group">
            <label>Pick Test Report<font style="color:red;">*</font>:</label>
            <input type="file" name="picture" class="form-control valid" id="reportFile" placeholder="Medicine Name" value="" onchange="readUrl(event)">
            <span style="color:red">Only jpg, jpeg, png, pdf file accepts</span>
            <div id="flupmsgs"></div>
            <img id='output' src="<?php echo base_url() . 'uploads/articles/default_icon.png'; ?>">
          </div>
        </div>
      </div>

      <div class="box-footer">
        <input type="submit" name="submit" class="btn btn-primary" value="Add Test Report" id="up_but">
        <input type="reset" name="cancel" class="btn btn-primary" value="Cancel" onclick="window.location='<?php echo base_url(); ?>testreport_list';">
      </div>
    </form>
  </div>
</section>
<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

<script type="text/javascript">
  $(".answer").hide();

  function valueChanged() {
    if ($('.coupon_question').is(":checked")) {
      $(".answer").show();
      $("#doctorName").prop('disabled', 'disabled');
      $("#testType").prop('disabled', 'disabled');
      $('input[type="text"]').attr('required', 'required');

    } else {
      $(".answer").hide();
      $("#doctorName").removeAttr("disabled");
      $("#testType").removeAttr("disabled");

    }
  }
  $(document).ready(function() {

    $("#add_testreport").validate({
      rules: {
        report_title: "required",
        doctor: "required",
        test_type: "required",
        report_description: "required",

        picture: {
          required: true,
        },
      },
      messages: {
        report_title: "Please enter test report title.",
        doctor: "Please select a doctor.",
        test_type: "Please select a test type.",
        report_description: "Please enter description.",
        picture: {
          required: "Please select test report.",
        },
      },
    });
  });

  function readUrl(event) {
    var pcFile = $('#reportFile').val().split('\\').pop();
    var pcExt = pcFile.split('.').pop();
    var output = document.getElementById('output');
    if (pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG" ||
      pcExt == "jpeg" || pcExt == "pdf" || pcExt == "PDF") {
      $("#up_but").prop('disabled', false);
      $('#flupmsgs').html('');
      output.src = URL.createObjectURL(event.target.files[0]);
      if (pcExt == "DOC" || pcExt == "doc" || pcExt == "docx" || pcExt == "DOCX" || pcExt == "TXT" || pcExt == "txt" || pcExt == "pdf" || pcExt == "PDF") {
        $('#output').hide();
      } else {
        $('#output').show();
      }
      $('#flupmsgs').html('');
    } else {
      $('#flupmsgs').html('Please select a valid file.');
      $('#output').hide();
      $("#up_but").prop('disabled', true);
    }
  };
  $(document).ready(function() {
    $('#output').hide();
  });
</script>

<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>
