<aside class="right-side">    

    <section class="content-header no-margin">

        <h1 class="text-center"><i class="fa fa-list"></i> Acknowledge List <div class="rem_add"><a href="#" class="btn btn-primary" >Add</a></div></h1>

    </section>    

    <section class="content photo-list">

	    <div class="photo-listMain reminder_listMain">

            <div class="row swt_cor">

                <div class="col-sm-6">

                    <label style="margin-right: 10px;">

                        <input type="checkbox" id="checkall">

                    </label>

                   <div class="delete-email">

                    <a href="#" name="reminder_all_del" id="reminder_all_del" class="reminder_all_del"><i aria-hidden="true" class="fa fa-trash-o"></i></a>                    

                    </div>

                </div>                    

            </div>

           <div class="table-responsive tb_swt">

            <div id="expire_msg"></div>

                <table class="table table-hover table-bordered table-mailbox send-user-mail tab_medi" id="sampleTable">        

                    <thead>

                        <tr>

            			    <th>#</th>

                            <th>Date</th>

                            <th>Medicine Name</th>

                            <th>Pharma Company Name</th>                           

                            <th>Action</th>				

                        </tr>

                    </thead>

                    <tbody>                

                        <tr>

                            <td class="small-col" >

                                <input type="checkbox" name="reminder_chk[]"  class="reminder_chk" value=""/>

                            </td>

                             <td>03/08/2017</td>  

                             <td>Paracetamol 500 MG</td>

                             <td>Drugs & Medications</td>                            

        			     	 <td>

        			            <a href="#" class="edit_btn">Edit</a>

        					   <a href="#" class="edit_btn">Delete</a>

        				    </td>

                        </tr> 

                        <tr>

                            <td class="small-col" >

                                <input type="checkbox" name="reminder_chk[]"  class="reminder_chk" value=""/>

                            </td>

                             <td>03/08/2017</td>  

                             <td>Paracetamol 500 MG</td>

                             <td>Drugs & Medications</td>                          

                             <td>

                                <a href="#" class="edit_btn">Edit</a>

                               <a href="#" class="edit_btn">Delete</a>

                            </td>

                        </tr> 

                        <tr>

                            <td class="small-col" >

                                <input type="checkbox" name="reminder_chk[]"  class="reminder_chk" value=""/>

                            </td>

                             <td>03/08/2017</td>  

                             <td>Paracetamol 500 MG</td>

                             <td>Drugs & Medications</td>                            

                             <td>

                                <a href="#" class="edit_btn">Edit</a>

                               <a href="#" class="edit_btn">Delete</a>

                            </td>

                        </tr> 

                        <tr>

                            <td class="small-col" >

                                <input type="checkbox" name="reminder_chk[]"  class="reminder_chk" value=""/>

                            </td>

                             <td>03/08/2017</td>  

                             <td>Paracetamol 500 MG</td>

                             <td>Drugs & Medications</td>                             

                             <td>

                                <a href="#" class="edit_btn">Edit</a>

                               <a href="#" class="edit_btn">Delete</a>

                            </td>

                        </tr> 

                        <tr>

                            <td class="small-col" >

                                <input type="checkbox" name="reminder_chk[]"  class="reminder_chk" value=""/>

                            </td>

                             <td>03/08/2017</td>  

                             <td>Paracetamol 500 MG</td>

                             <td>Drugs & Medications</td>                            

                             <td>

                                <a href="#" class="edit_btn">Edit</a>

                               <a href="#" class="edit_btn">Delete</a>

                            </td>

                        </tr>    			

                    </tbody>

                </table>

            </div>              

        </div>

    </section>

</aside>



<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script src="assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>

               