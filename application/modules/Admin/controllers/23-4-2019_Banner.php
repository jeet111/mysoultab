  <?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  class Banner extends MX_Controller {
    public function __construct(){   
      parent:: __construct();
      $this->load->library('session');
      $this->load->library('form_validation');
      $this->load->model('banner_model');
      $this->load->model('login_model');
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));
      $user_id = $this->session->userdata('logged_ins')['id'];
      if(empty($user_id)) {
        redirect('admin/login');
      }
    }
    
    /*show all banners*/
    public function banner_list()
    {
       $data['menuactive'] = $this->uri->segment(2);
     $data['BannerData'] = $this->banner_model->getAllbanner();
     $this->template->set('title', 'Banner');
     $this->template->load('admin_dashboard_layout', 'contents' , 'banner_list', $data);
   }

   /*Delete banner*/
   public function delete_banner($banner_id='')
   {
    $this->banner_model->delete('cp_music_banner',array('music_banner_id' =>$banner_id));
    $this->session->set_flashdata('success', 'Deleted successfully.');
    redirect('admin/banner_list');
  }

  /*Add banner*/
  public function add_banner()
  {
     $data['menuactive'] = $this->uri->segment(2);
    $this->form_validation->set_rules('banner_title','Banner Title','required');
    if ($this->form_validation->run() == TRUE) 
     {  
    if($_POST['submit']){
        /*for image file*/
       $random1 = $this->generateRandomString(10);
       $ext = pathinfo($_FILES['pictureFile']['name'], PATHINFO_EXTENSION);
       $file_name1 = $random1.".".$ext;
       $dirpath = './././uploads/music/banners/'.$file_name1;
       if(move_uploaded_file($_FILES['pictureFile']['tmp_name'], $dirpath)){
             $data['banner'] = $file_name1;
        }
        if(!empty($data['banner'])){
          $pictureFile =$data['banner'];
        }else{
          $pictureFile ='';
        }
      $data = array(
        'music_banner_text' => $this->input->post('banner_title'),
        'music_banner_image' => $pictureFile,
        'music_banner_created' =>date('Y-m-d H:i:s'),
        'music_banner_status'=>1
      );
      $this->banner_model->insertData('cp_music_banner',$data);
      $this->session->set_flashdata('success', 'Banner added successfully.');
      redirect('admin/banner_list');
     }
   }
    $this->template->set('title', 'Banner');
    $this->template->load('admin_dashboard_layout', 'contents' , 'add_banner', $data);
  }
  
  public function generateRandomString($length = 12) {
   $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
   $charactersLength = strlen($characters);
   $randomString = '';
   for ($i = 0; $i < $length; $i++) {
       $randomString .= $characters[rand(0, $charactersLength - 1)];
   }
   return $randomString;
   }

  /*Edit banner*/
  public function edit_banner($banner_id='')
  {
     $data['menuactive'] = $this->uri->segment(2);
    $this->form_validation->set_rules('banner_title','Banner Title','required');
  if ($this->form_validation->run() == TRUE) 
     {  
   if($_POST['submit']){
        /*for image file*/
       $random1 = $this->generateRandomString(10);
       $ext = pathinfo($_FILES['pictureFile']['name'], PATHINFO_EXTENSION);
       $file_name1 = $random1.".".$ext;
       $dirpath = './././uploads/music/banners/'.$file_name1;
       if(move_uploaded_file($_FILES['pictureFile']['tmp_name'], $dirpath)){
             $data['banner'] = $file_name1;
        }
        if(!empty($data['banner'])){
          $pictureFile =$data['banner'];
        }else{
          $pictureFile ='';
        }
      if(!empty($pictureFile)){
      $data = array(
        'music_banner_text' => $this->input->post('banner_title'),
        'music_banner_image' => $pictureFile,
        'music_banner_created' =>date('Y-m-d H:i:s'),
      );
      $this->banner_model->updateData('cp_music_banner',$data,array('music_banner_id'=>$banner_id));
      $this->session->set_flashdata('success', 'Banner updated successfully.');
      redirect('admin/banner_list');
    
     }else if(empty($pictureFile)|| $pictureFile==''){
      $data = array(
        'music_banner_text' => $this->input->post('banner_title'),
        
        'music_banner_created' =>date('Y-m-d H:i:s'),
        
      );
      $this->banner_model->updateData('cp_music_banner',$data,array('music_banner_id'=>$banner_id));
      $this->session->set_flashdata('success', 'Banner updated successfully.');
      redirect('admin/banner_list');
     }
    }
   }
    $data['editBanner']=$this->banner_model->getsingle('cp_music_banner',array('music_banner_id'=>$banner_id));
    $this->template->set('title', 'Banner');
    $this->template->load('admin_dashboard_layout','contents','edit_banner',$data);
  }

  /*view single banner detail*/
  public function view_banner($banner_id='')
    {
       $data['menuactive'] = $this->uri->segment(2);
     $data['viewBanner']=$this->banner_model->getsingle('cp_music_banner',array('music_banner_id'=>$banner_id));
     $this->template->set('title', 'Banner');
     $this->template->load('admin_dashboard_layout', 'contents' , 'view_banner', $data);
   }

   /*Update banner status*/
   public function updateBanner() {
    $banner_id = $this->input->post('banner_id');
    $con['conditions'] = array(
      'music_banner_id'=> $banner_id
    );
    $data['users_data'] = $this->login_model->getRows('cp_music_banner',$con);
    if($data['users_data'][0]['music_banner_status']==1){
      $return = $this->login_model->updateRecords("cp_music_banner", array('music_banner_status' => 0), array('music_banner_id'=> $banner_id));
    }else{
      $return = $this->login_model->updateRecords("cp_music_banner", array('music_banner_status' => 1), array('music_banner_id'=> $banner_id));
    }
    if($return > 0){
      echo 1;
    }
  }
  }