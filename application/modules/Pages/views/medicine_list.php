  <style>
    .addNote a,
    .btn.btn-primary {
      margin: 0px 0 15px !important;
    }
    .rem_add {
    margin: 14px 0 auto;
}
    .text_heading {
      border-bottom: none !important;
    }

    th.sorting_asc a {
      padding-left: 5px;
      color: #000;
    }

    th.sorting_asc input {
      margin: 0 !important;
      vertical-align: middle;
    }

    table#sampleTable th {
      border-bottom: none;
    }



    .table-responsive {
      overflow-x: initial !important;
    }

    .alert-success {
      margin-top: 17px;
      float: left;
      width: 100%;
    }

    .text_heading {
      border-bottom: 0;
    }
  </style>

  <div class="text_heading">
    <div id="msg_show"></div>
    <h3><i class="fa fa-plus-square-o" aria-hidden="true"></i> Medicine Management List</h3>
    <div class="tooltip-2">
      <h2>Display Medicine List button on Mobile App

        <?php
        if (is_array($btnsetingArray)) {

          foreach ($btnsetingArray as $res) {
            if ($res->settings == 1) { ?>
              <label class="switch ">
                <input type="checkbox" checked data-btn="<?php echo 'Medicine Schedule' ?>" class="updateStatus">
                <span class="slider round"></span>
              </label>

            <?php } else { ?>
              <label class="switch space">
                <input type="checkbox" data-btn="<?php echo 'Medicine Schedule' ?>" class="updateStatus">
                <span class="slider round"></span>
              </label>


            <?php } ?>
        <?php }
        }
        ?>
      </h2>


    </div>
    <div class="rem_add"><a href="<?php echo base_url(); ?>add_medicine" class="btn btn-primary">Add</a></div>

  </div>

  <section class="content photo-list">

    <div class="photo-listMain reminder_listMain">

      <div class="table-responsive tb_swt">
        <table class="table table-hover table-bordered table-mailbox send-user-mail tab_medi" id="medicalList">

          <thead>

            <tr>

              <th><input type="checkbox" id="checkallmedicine"><a href="javascript:void(0);" name="btn_delete" id="btn_delete" data-base="<?php echo base_url(); ?>" class="btn_delete"><i aria-hidden="true" class="fa fa-trash-o"></i> DELETE ALL</a></th>

              <th>Doctor Name</th>

              <th>Medicine Name</th>

              <th>Medicine Days</th>

              <!-- <th>Medicine Time</th> -->

              <!-- <th>Created Date</th>-->

              <th>Action</th>

            </tr>

          </thead>

          <tbody>

            <?php
            if (is_array($medicineData) && !empty($medicineData) && $medicineData != '') {
              foreach ($medicineData as $medicine) { ?>

                <tr>

                  <td class="small-col">

                    <input type="checkbox" name="medicine_chk[]" class="medicine_chk" value="<?php echo $medicine->medicine_id; ?>" data-id="<?php echo $medicine->medicine_id; ?>" />

                  </td>

                  <td data-label="Doctor Name"><?php echo $medicine->doctor_name; ?></td>

                  <td data-label="Medicine Name"><?php echo $medicine->medicine_name; ?></td>

                  <td data-label="Medicine Days"><?php echo $medicine->medicine_take_days; ?></td>

                  <!-- <td><?php echo $medicine->medicine_time; ?></td> -->

                  <!-- <td><?php //echo date('d-m-Y', strtotime($medicine->medicine_create_date)); 
                            ?></td> -->

                  <td class="act" data-label="Action">

                    <a href="<?php echo base_url(); ?>view_schedule/<?php echo $medicine->medicine_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    <a href="<?php echo base_url(); ?>edit_schedule/<?php echo $medicine->medicine_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                    <a href="<?php echo base_url(); ?>delete_shedule/<?php echo $medicine->medicine_id; ?>" onclick="return confirm('Are you sure, you want to remove this medicine schedule?')"><i class="fa fa-trash" aria-hidden="true"></i></a>

                  </td>

                </tr>

            <?php }
            }
            ?>



          </tbody>

        </table>

      </div>

    </div>

  </section>

  <script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>
  <script src="<?php echo base_url(); ?>assets/new_user_dashboard/js/jquery.validate.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      jQuery('#checkallmedicine').on('click', function(e) {

        if ($(this).is(':checked', true))

        {

          $(".medicine_chk").prop('checked', true);

        } else

        {

          $(".medicine_chk").prop('checked', false);

        }

      });
      jQuery('.medicine_chk').on('click', function() {

        if ($('.medicine_chk:checked').length == $('.medicine_chk').length) {

          $('#checkall').prop('checked', true);

        } else {

          $('#checkall').prop('checked', false);

        }

      });
      $('#medicalList').DataTable();
    });
    $(document).on('click', '.btn_delete', function(e) {


      var bodytype = $('input[name="medicine_chk[]"]:checked').map(function() {

        return this.value;

      }).get().join(",");



      if (bodytype != '') {

        if (confirm("Are you sure you want to remove this note ?")) {

          $.ajax({

            url: '<?php echo base_url(); ?>Pages/Medicine/medicine_all_del',

            type: 'POST',

            data: {
              bodytype: bodytype
            },

            success: function(data) {
              setTimeout(function() {
                $("#expire_msg").html("<div class='alert alert-success message'>Successfully deleted !</div>");
                location.reload();

              }, 1300);

            }

          });



        }

      } else {

        alert("Please select atleast one note.");

      }

    });
    $(document).on('click', '.updateStatus', function() {
      var update_status = $(this).attr('data-btn');

      var update_st = $(this).attr('data-st');
      //   alert(update_status+'-'+update_st);
      $.ajax({
        url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
        type: "POST",
        data: {
          'btn': update_status
        },
        dataType: "json",
        success: function(data) {
          $("#msg_show").html("<div class='alert alert-success'>" + data.status + "</div>");

        }
      });
    });
  </script>