<?php

if (isset($staff_permission[13]['add_permission']) == 1) {

	?>

	<div class="bx_user">  

		<div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_music">Add Music</a></div>

	</div>

	<?php

} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_music">Add Music</a></div>

	</div>

	<?php

}

?>



<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="row">

			<div class="col-md-12">

				<div class="">

					<?php //echo "<pre>"; print_r($MusicData); echo "</pre>";?>

					<div class="card-body">

						<div class="status-cng"></div>

						<?php if ($this->session->flashdata('success')) { ?>

							<div class="alert alert-success message">

								<button type="button" class="close" data-dismiss="alert">x</button>

								<?php echo $this->session->flashdata('success'); ?></div>

							<?php } ?>

							

							<!-- <div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_music">Add Music</a></div> -->

							<table class="table table-hover tab_comn" id="sampleTable">

								<thead>

									<tr>

										<th>S no.</th>

										<th>Music Title</th>

										<th>Music Description</th>							

										<th>Music Image</th>							

										<th>Action</th>

									</tr>

								</thead>

								<tbody>

									<?php 

									$i = 1;

									foreach ($MusicData as $Music) {

										

										?>

										<tr>

											<td><?php echo $i++; ?></td>

											<td><?php echo $Music->music_title; ?></td>

											<td><?php echo $Music->music_desc; ?></td>

											<td><img src="<?php echo base_url('uploads/music/image/'.$Music->music_image) ?>" height="50px" width="100px">    </td>

											

											<td>

												<div class="link-del-view">

													<?php

													if (isset($staff_permission[13]['view_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/view_music/<?php echo $Music->music_id; ?>">

																<i class="fa fa-eye" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">View</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/view_music/<?php echo $Music->music_id; ?>">

																<i class="fa fa-eye" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">View</span>

														</div>

														<?php

													}

													?>

													

													<?php

													if (isset($staff_permission[13]['edit_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/edit_music/<?php echo $Music->music_id; ?>">

																<i class="fa fa-edit" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">Edit</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/edit_music/<?php echo $Music->music_id; ?>">

																<i class="fa fa-edit" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">Edit</span>

														</div>

														<?php

													}

													?>

													

													<?php

													if (isset($staff_permission[13]['delete_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/delete_music/<?php echo $Music->music_id; ?>" onclick="return confirm('Are you sure, you want to delete this music ?')">

																<i class="fa fa-trash-o" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">Delete</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/delete_music/<?php echo $Music->music_id; ?>" onclick="return confirm('Are you sure, you want to delete this music ?')">

																<i class="fa fa-trash-o" aria-hidden="true"></i>

															</a>

															<span class="tooltiptext">Delete</span>

														</div>

														<?php

													}

													?>

													

													

													

												</div>

												

											</div>

										</td>

									</tr>

								<?php } ?>

							</tbody>

						</table>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>