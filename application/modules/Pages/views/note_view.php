<style>
    .text_heading {
        border-bottom: none !important;
    }

    table#sampleTable th {
        border-bottom: none;
    }

    .addNote {
        margin: 0 auto !important;
        text-align: center;
        display: inline-block;
        width: 100% !important;
    }

    .table-responsive {
        overflow-x: initial !important;
    }

    .photo-listMain {
        text-align: left;
        margin: 20px auto 20px !important;
        float: none;
        max-width: 520px;
        width: 100%;
    }

    .delete-email a i {
        font-size: 19px;
    }

    .delete-email {
        float: right;
        padding-left: 10px;
    }

    .table-bordered {
        border: 1px solid #ddd;
        table-layout: fixed;
    }

    .Cphoto-listMain.Creminder_listMain {
        background: #fff;
        margin-top: 21px;
        padding: 20px;
        box-sizing: border-box;
        float: left;
        width: 100%;
        box-shadow: rgba(100, 100, 111, 0.4) 0px 7px 29px 0px;
    }



    .tab_medi th,
    .tab_medi td {
        color: #000;
        text-align: left !important;
    }

    .error {
        color: #ff0000 !important;
        padding-top: 3px;
        text-align: left !important;
        width: 100% !important;
    }



    label#from_caregiver {
        background: #fff;
        width: 100%;
        padding: 11px;
        border: 1px solid #e7e7e7;
        text-align: left;
        color: #555;
    }

    .alert.alert-success.message {
        margin-top: 20px;
        width: 100%;
        margin-left: 0;
        margin-right: 0;
        width: 100% !important;
    }

    form#add_note {
        text-align: center;
    }


    textarea.form-control {
        height: 100px;
    }

    .photo-listMain {
        text-align: left;
    }

    .addNote a,
    input.btn.btn-primary {
        margin: 0 !important;
        padding: 10px 28px !important;
        max-width: 126px !important;
        width: 100% !important;
        border-radius: 50px !important;
    }
</style>



<div class="text_heading">
    <div id="msg_show"></div>
    <h3><i class="fa fa-sticky-note" aria-hidden="true"></i>
        Notes</h3>
    <div class="tooltip-2">
        <h2>Display Notes button on Mobile App

            <?php
            if (is_array($btnsetingArray)) {

                foreach ($btnsetingArray as $res) {
                    if ($res->settings == 1) { ?>
                        <label class="switch ">
                            <input type="checkbox" checked data-btn="<?php echo 'Note' ?>" class="updateStatus">
                            <span class="slider round"></span>
                        </label>

                    <?php } else { ?>
                        <label class="switch space">
                            <input type="checkbox" data-btn="<?php echo 'Note' ?>" class="updateStatus">
                            <span class="slider round"></span>
                        </label>


                    <?php } ?>
            <?php }
            }
            ?>
        </h2>
    </div>

    <div class="rem_add">
        <a href="#" class="btn btn-primary" id="onClickAdd">Add Note</a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div id="loders"></div>

        <div class="photo-listMain">
            <form id="add_note" enctype="multipart/form-data" method="post" class="frm_add">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                            <?php  ?>

                            <?php
                            $user = $this->session->userdata('logged_in');
                            // print_r($user_id);
                            ?>
                            <label name="from_caregiver" id="from_caregiver" data-value="<?php echo $user['id']; ?>"><?php echo $username = $user['name'] . ' ' . $user['lastname']; ?></label>

                            <?php echo form_error('from_caregiver'); ?>

                        </div>
                        <div class="form-group">
                            <select name="to_caregiver" id="to_caregiver" class="form-control valid">
                                <option value="">Select Caregiver</option>
                                <?php foreach ($care_giver as $to_care) { ?>
                                    <option value="<?php echo $to_care->id; ?>" <?php if ($to_care->id == $to_care->id) {
                                                                                    echo 'selected';
                                                                                } ?>><?php echo $to_care->name . ' ' . $to_care->lastname; ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_error('to_caregiver'); ?>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control valid" name="description" id="description" placeholder="Description">
                                <?php echo form_error('description'); ?>
              </textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-primary" value="Submit">
                            <input type="reset" name="cancel" class="btn btn-primary" value="Cancel" onclick="window.location='<?php echo base_url(); ?>notes';">
                        </div>

                    </div>

                </div>
            </form>
        </div>




        <section class="content">

            <div class="Cphoto-listMain Creminder_listMain">

                <div class="row swt_cor">

                    <div class="col-sm-12">






                    </div>

                </div>


                <div class="table-responsive tb_swt">

                    <div id="expire_msg"></div>

                    <table class="table table-hover table-bordered table-mailbox send-user-mail tab_medi" id="sampleTable">

                        <thead>

                            <tr>

                                <th> <input type="checkbox" id="checkall">

                                    <a href="javascript:void(0);" name="btn_delete" id="btn_delete" class="btn_delete"> <i aria-hidden="true" class="fa fa-trash-o"></i></a> Delete All

                                </th>

                                <th>Username</th>

                                <th>To Caregiver</th>
                                <th>Description</th>

                                <th>Action</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php

                            if (!empty($note_list)) {

                                foreach ($note_list as $note) {
                                    // print_R($note);

                                    if (!empty($note->from_caregiver)) {
                                        $note->from_caregiver = $note->from_caregiver;
                                    }


                                    $fromcaregiver = $this->Common_model_new->getsingle("cp_users", array("id" => $note->user_id));

                                    if (!empty($note->to_caregiver)) {
                                        $note->to_caregiver = $note->to_caregiver;
                                    }


                                    $tocaregiver = $this->Common_model_new->getsingle("cp_users", array("id" => $note->to_caregiver));

                            ?>

                                    <tr>

                                        <td class="small-col">

                                            <input type="checkbox" name="sub_chk[]" class="sub_chk" value="<?php echo $note->note_id; ?>" />

                                        </td>



                                        <td><?php if (!empty($fromcaregiver->name)) {
                                                echo $fromcaregiver->name . ' ' . $fromcaregiver->lastname;
                                            } ?></td>

                                        <td><?php if (!empty($tocaregiver->username)) {
                                                echo $tocaregiver->username;
                                            } ?></td>

                                        <td>
                                            <div class="bx_meSec note_desc"><?php echo $note->description; ?></div>
                                        </td>

                                        <td>

                                            <a href="<?php echo base_url(); ?>view_note/<?php echo $note->note_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>

                                            <a href="<?php echo base_url(); ?>edit_note/<?php echo $note->note_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                                            <a href="<?php echo base_url(); ?>delete_note/<?php echo $note->note_id; ?>" onclick="return confirm('Are you sure, you want to remove this note ?')"><i class="fa fa-trash" aria-hidden="true"></i></a>

                                        </td>

                                    </tr>



                            <?php }
                            } ?>



                        </tbody>

                    </table>

                </div>

            </div>

        </section>
    </div>
</div>

</div>

</div>

<script type="text/javascript">
    jQuery('#description').val(jQuery.trim($('#description').val()));

    jQuery('#checkall').on('click', function(e) {

        if ($(this).is(':checked', true))

        {

            $(".sub_chk").prop('checked', true);

        } else

        {

            $(".sub_chk").prop('checked', false);

        }

    });
    $(document).on('click', '.btn_delete', function(e) {





        var bodytype = $('input[name="sub_chk[]"]:checked').map(function() {

            return this.value;

        }).get().join(",");



        if (bodytype != '') {

            if (confirm("Are you sure you want to remove this note ?")) {

                $.ajax({

                    url: '<?php echo base_url(); ?>Pages/Note/delete_checkboxes',

                    type: 'POST',

                    data: {
                        bodytype: bodytype
                    },

                    //dataType: "json",

                    success: function(data) { //alert('swati');





                        setTimeout(function() {

                            $("#expire_msg").html("<div class='alert alert-success message'>Successfully deleted !</div>");

                            location.reload();

                            //window.location.href = email_url+"email_list";

                        }, 1300);

                    }

                });



            }

        } else {

            alert("Please select atleast one note.");

        }

    });

    $(document).on('click', '.updateStatus', function() {
        var update_status = $(this).attr('data-btn');
        var update_st = $(this).attr('data-st');
        //alert(update_status+'-'+update_st);
        $.ajax({
            url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
            type: "POST",
            data: {
                'btn': update_status
            },
            dataType: "json",
            success: function(data) {
                $("#msg_show").html("<div class='alert alert-success'>" + data.status + "</div>");

            }
        });
    });
    $(document).ready(function() {
        $('#sampleTable').DataTable();
        $('#add_note').validate({
            rules: {
                to_caregiver: 'required',
                from_caregiver: 'required',
                description: 'required'
            },

            messages: {
                to_caregiver: "Please enter To caregiver.",
                description: "Please enter description.",
                from_caregiver: "Please enter From caregiver."
            }
        });

        $(".photo-listMain").hide();
        $(document).on('click', '#onClickAdd', function() {
            $(".photo-listMain").slideToggle();

        });

        $(".frm_add").on('submit', (function(e) {
            var from_caregiver = $("#from_caregiver").attr('data-value');
            var to_caregiver = $("#to_caregiver").val();
            var description = $("#description").val();
            e.preventDefault();
            if ((from_caregiver != '') && (to_caregiver != '') && (description != '')) {

                $.ajax({
                    url: "<?php echo base_url(); ?>Pages/Note/add_note",
                    type: "POST",
                    data: {
                        'from_caregiver': from_caregiver,
                        'to_caregiver': to_caregiver,
                        'description': description
                    },
                    dataType: "json",
                    success: function(data) {
                        if (data.code == 1) {
                            $("#sub").prop('disabled', true);
                            $("#image_msg").html('<div class="alert alert-success">' + data.message + '<div>');

                            $("#image_msg").html("");
                            window.location.href = '<?php echo base_url(); ?>note_list';

                        } else {
                            $("#image_msg").html('<div class="alert alert-danger">' + data.message + '<div>');
                        }
                    }
                });
            }
        }));

    });
</script>
