<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Aboutus extends MX_Controller {

	private $staff_permission = array();
	private $access_permission = true;
	public function __construct(){

		parent:: __construct();

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model('Common_model');
		$this->load->model('login_model');
		$this->load->model('Feature_model');

		$this->load->helper(array('url'));
		$this->load->helper(array('form'));

		$user_id = $this->session->userdata('logged_ins')['id'];
		if(empty($user_id)) {
			redirect('admin/login');
		}

		if ($this->session->userdata('logged_ins')['user_role'] == 5) {
			$uid = $this->session->userdata('logged_ins')['id'];
			$permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
			if (!empty($permission_list)) 
			{
				foreach ($permission_list as $k => $v) 
				{
					$this->staff_permission[$v['permission_id']] = $v;
				}
			}
		}

	}

	public function permission_access($permission_id=6)
	{
		if ($this->session->userdata('logged_ins')['user_role'] != 1) 
		{
			if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
			{
				$this->access_permission = false;
			}
		}
	}

	public function add_edit_function(){
		$data['menuactive'] = $this->uri->segment(2);
		//$this->form_validation->set_rules('template_id','Template Id','required');
		//$this->form_validation->set_rules('subject','Subject','required');
		$data['singleData'] = $this->Common_model->getsingle('about_page',array('template_id' => 1));
		$this->form_validation->set_rules('description','Description','required');

		
		if($this->form_validation->run()==TRUE)
		{
			
			$description = $_POST['description'];
			$arrayData = array(
				'subject'=>'',
				'description'=>$description,
				'updated_date'=>date('Y-m-d H:i:s')
			);

			// print_r($arrayData);
			// die;

			$this->Common_model->updateData('about_page',$arrayData,array('template_id' => 1));			
			
			//$this->Common_model->updateData('about_page',$array,array('template_id' => 1));
			$this->session->set_flashdata('success', 'Successfully updated.');
			redirect('admin/about-us');
		}
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'About us');
			$this->template->load('admin_dashboard_layout', 'contents', 'editabout', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}





}