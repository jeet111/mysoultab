 <?php //echo $singleData['description']; ?> 
 <main id="main_inner">
 	<section class="contact_page_main">
 		<div class="container">
 			<div class="header-content">
 				<h1 class="et_pb_module_header">Get in Touch</h1>
 				<span class="et_pb_fullwidth_header_subhead">Want to get in touch? We'd love to hear from you. Here's how you can reach us...</span>
 				<div class="et_pb_header_content_wrapper"></div>
 			</div></div>
 		</section>
 		<section class="contact_page_main_one">
 			<div class="container">
 				<div class="left_side_div_c">
 					<div class="phone_icon"><i class="fa fa-phone fa-5x" aria-hidden="true"></i></div>
 					<div class="main_heading_c">
 						<h3>Talk to Sales</h3>
 						<p>2300 Barrington Rd<br>
 							Hoffman Estates, IL 60169<br>
 						USA.</p>
 						<div class="phone_c"><a>+(866) 861-8497</a></div>
 					</div>
 				</div>
 				<div class="left_side_div_c">
 					<div class="phone_icon"><i class="fa fa-envelope fa-5x" aria-hidden="true"></i></div>
 					<div class="main_heading_c">
 						<h3>Contact Customer Support</h3>
 						<p>2300 Barrington Rd <br>
 							Hoffman Estates, IL 60169 <br>
 						USA..</p>
 						<div class="phone_c"><a>support@mysoultab.com</a></div>
 					</div>
 				</div>
 			</div>
 		</section>
 		<section class="contact_page_main_two">
 			<div class="container">
 				<div class="et_pb_text_inner"><p style="text-align: center;">Contact Us</p></div>
 				<div class="contact_page_main_two_mm">
 					<div class="contact_page_main_two_l">
 						<div id="deletemsg" style="text-align: center;"></div>
 						<form id="admin_contact">
 							<div class="form-group">
 								<input type="text" name="fullname" value="" size="40" class="form-control" aria-required="true" placeholder="Name"></div>	
 								<div class="form-group">
 									<input type="text" name="email" value="" size="40" class="form-control" aria-required="true" placeholder="E-mail"></div>
 									<div class="form-group">
 										<input type="text" name="mobile" value="" size="40" class="form-control" aria-required="true" placeholder="Phone"></div>
 										<div class="form-group">
 											<textarea cols="40" rows="10" class="form-control" aria-invalid="false" placeholder="Message" id="usrfeedback" name ="usrfeedback"></textarea></div>
 											<div class="form-group">
 												<input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit btn" id="contact"></div>
 											</form>
 										</div>
 										<div class="contact_page_main_two_r">
 											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2962.0383590371684!2d-88.14787278504963!3d42.06379716175264!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880fa7df7aeafa9b%3A0x4eb84e315c9b79b2!2s2300%20Barrington%20Rd%2C%20Hoffman%20Estates%2C%20IL%2060169%2C%20USA!5e0!3m2!1sen!2sin!4v1596885654474!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
 										</div>
 									</div>
 								</div>
 							</section>
 						</main>

 						<script src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.js"></script>
 						<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>
 						<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script> 
 						<script type="text/javascript">
 							$('#admin_contact').validate({
 								rules: {
 									usrfeedback: "required",
 									fullname: "required",
 									//email: "required",
 									email:{

 										required:true,

 										email:true,

 									},
 									mobile:{

 										number:true,

 										required:true,

 									},
 									
 								},
 								messages: {

 									usrfeedback:"Please enter your message.",  

 									fullname: "Please enter your fullname.",
 									email:{

 										required:"Please enter email.",

 										email:"Please enter valide email.",

 									},
 									mobile:{

 										number:"Please enter valide mobile no.",

 										required:"Please enter mobile no.",

 									},
 									//email: "Please enter your email.",
 									
 								},
 								submitHandler: function(form) {
 									$.ajax({
 										type: 'post',
 										url: '<?php echo base_url();?>Pages/Login/contact_us',
 										data: $('form').serialize(),
 										beforeSend: function() { 
              $("#contact").prop('disabled', true); // disable button
              //console.log('ooo');
            },
            success: function (data) {
             if(data==1){
              $("#deletemsg").html('<div class="alert alert-success">'+'Message sent successfully.'+'</div>');
              setTimeout(function(){
               $("#deletemsg").html("");
             }, 2000);
                $("#contact").prop('disabled', false); // enable button
              }else{
               $("#deletemsg").html('<div class="alert alert-danger">'+'Please try again.'+'</div>');
               setTimeout(function(){
                $("#deletemsg").html("");
              }, 2000);
                $("#contact").prop('disabled', false); // enable button
              }
            }
          });
 								}
 							});
 						</script>