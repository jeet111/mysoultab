<style>
	table#sampleTable td {
		text-align: left !important;
	}

	.addNote a,
	.btn.btn-primary {
		margin: 0 0 14px 0 !important;
		padding: 10px 9px !important;
		max-width: 107px !important;
		width: 100% !important;
		border-radius: 50px !important;
	}

	.text_heading {

		margin-bottom: 0;
	}
</style>

<div class="text_heading">
	<div id="msg_show"></div>
	<h3 class="text-center"><i class="fa fa-medkit" aria-hidden="true"></i> Add Pharmacy</h3>
	<?php if (!empty($pharmacytArray) && $pharmacytArray != '') { ?>
		<div class="tooltip-2">
			<div id="msg_show"></div>
			<h2>Display Pharmacy List button on Mobile App
				<?php
				foreach ($pharmacytArray as $res) {
					if ($res->settings == 1) { ?>
						<label class="switch ">
							<input type="checkbox" checked data-btn="<?php echo 'Pharmacy Pickup' ?>" class="updateStatus">
							<span class="slider round"></span>
						</label>

					<?php } else { ?>
						<label class="switch space">
							<input type="checkbox" data-btn="<?php echo 'Pharmacy Pickup' ?>" class="updateStatus">
							<span class="slider round"></span>
						</label>
					<?php } ?>
				<?php } ?>
			</h2>
		</div>
	<?php } ?>

	<div class="rem_add">
		<a href="#addpharmacymyModal" class="btn button btn-primary" data-toggle="modal">Add </a>
	</div>
</div>
<div class="row websiteAppClass">

	<?php
	if (!empty($pharmacy_list) && $pharmacy_list != '') {
		foreach ($pharmacy_list as $key => $value) {
	?>
			<div class="serv col-md-3 bg-info">
				<h4><?php echo $value->button_name; ?></h4>
				<?php
				if ($value->pharmacy_type == "Website") {
				?>
					<img class="game" src="https://www.google.com/s2/favicons?domain=<?php echo $value->url; ?>" data-url="<?php echo $value->url; ?>">
				<?php } else {
					$app_list_data = $this->common_model_new->getAllwhere("app_list", array('id' => $value->app_name));
				?>
					<img class="game app" src="<?php echo $app_list_data[0]->icon; ?>">

				<?php } ?>
				<div class="rem_add_button">
					<a href="#editpharmacymyModal" class="model-content edit edit_btn" data-toggle="modal">
						<i class="material-icons update" data-toggle="tooltip" data-pharmacyid="<?php echo $value->id; ?>" data-pharmacytype="<?php echo $value->pharmacy_type; ?>" data-url="<?php echo $value->url; ?>" data-app_name="<?php echo $value->app_name; ?>" data-button_name="<?php echo $value->button_name; ?>" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></i>
					</a><a href="#deletepharmacymyModal" class="model-content delete edit_btn" data-id="<?php echo $value->id; ?>
                                            " data-toggle="modal"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
											</div>
        </div>

    <?php }
} else { ?>
        <div class="nodataContainer"><h4 class="nodataFound">No Data Found!</h4></div>
<?php } ?>
</div>
			<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css" rel="stylesheet">
			<script src="<?php echo base_url(); ?>assets/js/jquery-3.5.1.js"></script>
			<script type="text/javascript">
				$(document).ready(function() {
					setTimeout(function() {
						$('#success').fadeOut('fast');
					}, 2500);
				});

				$(document).on('click', '.updateStatus', function() {
					var update_status = $(this).attr('data-btn');
					var update_st = $(this).attr('data-st');
					//alert(update_status+'-'+update_st);
					$.ajax({
						url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
						type: "POST",
						data: {
							'btn': update_status
						},
						dataType: "json",
						success: function(data) {
							$("#msg_show").html("<div class='alert alert-success' id='hide_msg'>" + data.status + "</div>");
							setTimeout(function() {
								$('#hide_msg').fadeOut('fast');
							}, 2500);
						}
					});
				});
			</script>