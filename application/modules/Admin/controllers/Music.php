  <?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  class Music extends MX_Controller {

    private $staff_permission = array();
    private $access_permission = true;
    public function __construct(){   
      parent:: __construct();
      $this->load->library('session');
      $this->load->library('form_validation');
      $this->load->model('music_model');
	    $this->load->model('Common_model');
      $this->load->model('login_model');
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));
      $user_id = $this->session->userdata('logged_ins')['id'];
      if(empty($user_id)) {
        redirect('admin/login');
      }

      if ($this->session->userdata('logged_ins')['user_role'] == 5) {
        $uid = $this->session->userdata('logged_ins')['id'];
        $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
        if (!empty($permission_list)) 
        {
           foreach ($permission_list as $k => $v) 
           {
              $this->staff_permission[$v['permission_id']] = $v;
           }
        }
      }
    }

    public function permission_access($permission_id=13)
    {
      if ($this->session->userdata('logged_ins')['user_role'] != 1) 
      {
         if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
         {
            $this->access_permission = false;
         }
      }
    }

    /*Created by 95 for show all music*/
    public function music_list()
    {
       $data['menuactive'] = $this->uri->segment(2);
     $data['MusicData'] = $this->music_model->getAllmusicorder();
     
     $data['staff_permission'] = $this->staff_permission;
     
     $this->permission_access(13);
     if ($this->access_permission) {
        $this->template->set('title', 'Music');
        $this->template->load('admin_dashboard_layout', 'contents', 'music_list', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }


   /*Created by 95 for delete music*/
   public function delete_music($music_id='')
   {
	 $image_name =   $this->Common_model->getsingle('music',array("music_id" => $music_id));
	   unlink("uploads/music/".$image_name->music_file);
	   unlink("uploads/music/image/".$image_name->music_image);
    $this->music_model->delete('music',array('music_id' =>$music_id));
	
	$this->music_model->delete('cp_music_favorite',array('music_id' =>$music_id));
	
    $this->session->set_flashdata('success', 'Banner is successfully deleted');
    redirect('admin/music_list');
  }


  /*Created by 95 for add music*/
  public function add_music()
  {

 $data['menuactive'] = $this->uri->segment(2);
  $this->form_validation->set_rules('music_title','Music Title','required');
   
    $this->form_validation->set_rules('music_description','Music Description','required');
    $this->form_validation->set_rules('music_artist','Music Artist','required');
    $this->form_validation->set_rules('music_type','Music Type','required');
	$data['category_name'] = $this->Common_model->getAllwhereorderby("cp_music_category",array("music_category_status" => 1),"music_category_name","asc");
if ($this->form_validation->run() == TRUE) 
     {  
    if($_POST['submit']){
      /*for audio file*/
       $random = $this->generateRandomString(10);
       $ext = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
       $file_name = $random.".".$ext;
       $dirpath = './././uploads/music/'.$file_name;
       if(move_uploaded_file($_FILES['picture']['tmp_name'], $dirpath)){
             $data['new_pic'] = $file_name;
        }
        if(!empty($data['new_pic'])){
          $picture =$data['new_pic'];

          /*For image resize*/
           $this->load->library('image_lib');
          $configer =  array(
              'image_library'   => 'gd2',
              'source_image'    =>  $dirpath,
              'maintain_ratio'  =>  TRUE,
              'width'           =>  150,
              'height'          =>  150,
            );
            $this->image_lib->clear();
            $this->image_lib->initialize($configer);
            $this->image_lib->resize();


        }else{
          $picture ='';
        }

        /*for image file*/
       $random1 = $this->generateRandomString(10);
       $ext = pathinfo($_FILES['pictureFile']['name'], PATHINFO_EXTENSION);
       $file_name1 = $random1.".".$ext;
       $dirpath = './././uploads/music/image/'.$file_name1;
       if(move_uploaded_file($_FILES['pictureFile']['tmp_name'], $dirpath)){
             $data['music_pic'] = $file_name1;
        }
        if(!empty($data['music_pic'])){
          $pictureFile =$data['music_pic'];
        }else{
          $pictureFile ='';
        }

      $data = array(
        'music_title' => $this->input->post('music_title'),
        'music_desc' => $this->input->post('music_description'),
        'music_artist' => $this->input->post('music_artist'),
        'music_type' => $this->input->post('music_type'),
		'category_id' => $this->input->post('music_type'),
        'music_file' => $picture,
        'music_image' => $pictureFile,
        'music_created' =>date('Y-m-d H:i:s')
      );

      $this->music_model->insertData('music',$data);
      $this->session->set_flashdata('success', 'Music added successfully.');
      redirect('admin/music_list');
     }


   }

    $data['staff_permission'] = $this->staff_permission;
    
     $this->permission_access(13);
     if ($this->access_permission) {
        $this->template->set('title', 'Music');
        $this->template->load('admin_dashboard_layout', 'contents' , 'add_music',$data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
  }
  


  public function generateRandomString($length = 12) {
   $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
   $charactersLength = strlen($characters);
   $randomString = '';
   for ($i = 0; $i < $length; $i++) {
       $randomString .= $characters[rand(0, $charactersLength - 1)];
   }
   return $randomString;
   }
  

  /*Created by 95 for edit music*/
  public function edit_music($music_id='')
  {
     $data['menuactive'] = $this->uri->segment(2);
    $this->form_validation->set_rules('music_title','Music Title','required');
   
    $this->form_validation->set_rules('music_description','Music Description','required');
    $this->form_validation->set_rules('music_artist','Music Artist','required');
     $this->form_validation->set_rules('music_type','Music Type','required');
	 $data['category_name'] = $this->Common_model->getAllwhereorderby("cp_music_category",array("music_category_status" => 1),"music_category_name","asc");
if ($this->form_validation->run() == TRUE) 
     {  

   if($_POST['submit']){
      /*for audio file*/
       $random = $this->generateRandomString(10);
       $ext = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
       $file_name = $random.".".$ext;
       $dirpath = './././uploads/music/'.$file_name;
       if(move_uploaded_file($_FILES['picture']['tmp_name'], $dirpath)){
             $data['new_pic'] = $file_name;
        }
        if(!empty($data['new_pic'])){
          $picture =$data['new_pic'];


          /*For image resize*/
           $this->load->library('image_lib');
          $configer =  array(
              'image_library'   => 'gd2',
              'source_image'    =>  $dirpath,
              'maintain_ratio'  =>  TRUE,
              'width'           =>  150,
              'height'          =>  150,
            );
            $this->image_lib->clear();
            $this->image_lib->initialize($configer);
            $this->image_lib->resize();
        }else{
          $picture ='';
        }

        /*for image file*/
       $random1 = $this->generateRandomString(10);
       $ext = pathinfo($_FILES['pictureFile']['name'], PATHINFO_EXTENSION);
       $file_name1 = $random1.".".$ext;
       $dirpath = './././uploads/music/image/'.$file_name1;
       if(move_uploaded_file($_FILES['pictureFile']['tmp_name'], $dirpath)){
             $data['music_pic'] = $file_name1;
        }
        if(!empty($data['music_pic'])){
          $pictureFile =$data['music_pic'];
        }else{
          $pictureFile ='';
        }
      
      if(!empty($pictureFile)){

      $data = array(
        'music_title' => $this->input->post('music_title'),
        'music_desc' => $this->input->post('music_description'),
        'music_artist' => $this->input->post('music_artist'),
        'music_type' => $this->input->post('music_type'),
		'category_id' => $this->input->post('music_type'),
        'music_image' => $pictureFile,
        'music_created' =>date('Y-m-d H:i:s')
      );
      $this->music_model->updateData('music',$data,array('music_id'=>$music_id));
      $this->session->set_flashdata('success', 'Music updated successfully.');
      redirect('admin/music_list');
    }else if(!empty($picture)){
      $data = array(
        'music_title' => $this->input->post('music_title'),
        'music_desc' => $this->input->post('music_description'),
        'music_artist' => $this->input->post('music_artist'),
        'music_type' => $this->input->post('music_type'),
		'category_id' => $this->input->post('music_type'),
        'music_file' => $picture,
        'music_created' =>date('Y-m-d H:i:s')
      );
      $this->music_model->updateData('music',$data,array('music_id'=>$music_id));
      
      $this->session->set_flashdata('success', 'Music updated successfully.');
      redirect('admin/music_list');
     }else if(empty($pictureFile)|| $pictureFile=='' && empty($picture)|| $picture==''){

      $data = array(
        'music_title' => $this->input->post('music_title'),
        'music_desc' => $this->input->post('music_description'),
        'music_artist' => $this->input->post('music_artist'),
        'music_type' => $this->input->post('music_type'),
		'category_id' => $this->input->post('music_type'),
        'music_created' =>date('Y-m-d H:i:s')
      );

      $this->music_model->updateData('music',$data,array('music_id'=>$music_id));
      
      $this->session->set_flashdata('success', 'Music updated successfully.');
      redirect('admin/music_list');



     }
    }


   }

    $data['editMusic']=$this->music_model->getsingle('music',array('music_id'=>$music_id));
    
    $data['staff_permission'] = $this->staff_permission;
    
     $this->permission_access(13);
     if ($this->access_permission) {
        $this->template->set('title', 'Music');
        $this->template->load('admin_dashboard_layout','contents','edit_music',$data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
  }


  public function view_music($music_id='')
    {
       $data['menuactive'] = $this->uri->segment(2);
     $data['viewMusic']=$this->music_model->getsingle('music',array('music_id'=>$music_id));
     
     $data['staff_permission'] = $this->staff_permission;
     
     $this->permission_access(13);
     if ($this->access_permission) {
        $this->template->set('title', 'Music');
        $this->template->load('admin_dashboard_layout', 'contents' , 'view_music', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }
   
   public function add_music_category()
   {
	   $data['menuactive'] = $this->uri->segment(2);
	   $unique_id = substr(number_format(time() * rand(),0,'',''),0,4);
	  $this->form_validation->set_rules('category_name','Category Name','required');
	  
	  
     $data['staff_permission'] = $this->staff_permission;
     
     $this->permission_access(13);
     if ($this->access_permission) {
        $this->template->set('title', 'Music');
        $this->template->load('admin_dashboard_layout', 'contents' , 'add_music_category', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }  
   
    public function ajax_music_category()
	{
		 
		 
		 
           if ($_FILES["category_icon"]["error"] > 0)
  {
   $return = array('code' => 2,'message' => 'File loading error!');
  }else{

           $data['upload_path'] = 'uploads/music/category_icon/';
            $data['allowed_types'] = 'jpg|png|gif|jpeg';
            $data['max_size'] = '5000';
            $data['encrypt_name'] = true;

            $this->load->library('upload', $data);


            if ($this->upload->do_upload('category_icon')) {
                $attachment_data = array('upload_data' => $this->upload->data());
                $uploadfile = $attachment_data['upload_data']['file_name'];
				
				 $array = array(
	      'music_category_name' => $_POST['category_name'],
		  'music_category_icon' => $uploadfile,
		  'music_category_desc' => $_POST['category_desc'],
		  'music_category_status' => 1,
		  'music_category_create_date' => date("Y-m-d H:i:s")
	   );
	   $this->Common_model->insertData("cp_music_category",$array);
				
$return = array('code' => 1,'message' => 'Successfully added');

	 
		 }else{
			$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
		 }
		 
	}
	echo json_encode($return);
	}
	
	public function music_category_list()	
	{
		 $data['menuactive'] = $this->uri->segment(2);
		 
		 $data['category_list'] = $this->Common_model->getAllorderby("cp_music_category","music_category_id","desc");
		 
		 //echo '<pre>';print_r($data['category_list']);die;
 		 
		 $data['staff_permission'] = $this->staff_permission;
     
     $this->permission_access(13);
     if ($this->access_permission) {
        $this->template->set('title', 'Music');
        $this->template->load('admin_dashboard_layout', 'contents' , 'music_category_list', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
	}
	
	public function edit_music_category()
	{
		$data['menuactive'] = $this->uri->segment(2);
		
		$segment = $this->uri->segment("3");
		$data['segment'] = $segment;
		
		$data['singleData'] = $this->Common_model->getsingle("cp_music_category",array("music_category_id" => $segment));
		
    $data['staff_permission'] = $this->staff_permission;
    

     $this->permission_access(13);
     if ($this->access_permission) {
        $this->template->set('title', 'Music');
        $this->template->load('admin_dashboard_layout', 'contents', 'edit_music_category', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }

	}
	
	 public function ajax_edit_music_category()
	{
	
	  $singledata = $this->Common_model->getsingle("cp_music_category",array("music_category_id" => $_POST['cat_id']));

           $data['upload_path'] = 'uploads/music/category_icon/';
            $data['allowed_types'] = 'jpg|png|gif|jpeg';
            $data['max_size'] = '5000';
            $data['encrypt_name'] = true;

            $this->load->library('upload', $data);


            if ($this->upload->do_upload('category_icon')) {
                $attachment_data = array('upload_data' => $this->upload->data());
                $uploadfile = $attachment_data['upload_data']['file_name'];
			
		 }
		 
		 if(!empty($uploadfile)){
			 $files = $uploadfile;
		 }else{
			 $files = $singledata->music_category_icon;
		 }
		 
		  $array = array(
	      'music_category_name' => $_POST['category_name'],
		  'music_category_icon' => $files,
		  'music_category_desc' => $_POST['category_desc'],
		  'music_category_create_date' => date("Y-m-d H:i:s")
	   );
	   $this->Common_model->updateData("cp_music_category",$array,array("music_category_id" => $_POST['cat_id']));
				
$return = array('code' => 1,'message' => 'Successfully updated');
	echo json_encode($return);	 
	
	
	}
	
	public function delete_music_category()
	{
		
		$delete_music = $this->input->post("delete_music");
		$image_name = $this->Common_model->getsingle("cp_music_category",array("music_category_id" => $delete_music));
		
		$music_name = $this->Common_model->getAllwhere("music",array("category_id" => $delete_music));
		
		 
		 unlink("uploads/music/category_icon/".$image_name->music_category_icon);
		 foreach($music_name as $del){
		 unlink("uploads/music/".$image_name->music_file);
		 unlink("uploads/music/image/".$image_name->music_image);
		 $this->Common_model->delete('music',array('category_id' =>$delete_music));
		 
		 }
		$this->Common_model->delete('cp_music_category',array('music_category_id' =>$delete_music));
		
       
	}
	
	public function view_music_category()
	{
		$data['menuactive'] = $this->uri->segment(2);
		
		$data['segment'] = $this->uri->segment(3);
		
		$data['singledata'] = $this->Common_model->getsingle("cp_music_category",array("music_category_id" => $data['segment']));
		
	  $data['staff_permission'] = $this->staff_permission;
    

     $this->permission_access(13);
     if ($this->access_permission) {
        $this->template->set('title', 'Music');
        $this->template->load('admin_dashboard_layout', 'contents', 'view_music_category', $data);
     } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }

	}
	
	public function category_status()
	{
		 $music_id = $this->input->post("music_id");
		 $check_category = $this->Common_model->getsingle("cp_music_category",array("music_category_id" => $music_id));
		 if($check_category->music_category_status == 1){
			 $this->Common_model->updateData("cp_music_category",array("music_category_status" => 0),array('music_category_id' => $music_id));
			 echo '2';
		 }else{
			$this->Common_model->updateData("cp_music_category",array("music_category_status" => 1),array('music_category_id' => $music_id));
echo '1';			
		 }
	}
	
	public function image_upload()
	{
		$config1['upload_path'] = 'uploads/movie/';
		   $config1['allowed_types'] = 'mp3';
           
           //$data['max_size'] = '5000';
            $config1['encrypt_name'] = true;

            $this->load->library('upload', $config1);


            if ($this->upload->do_upload('picture')) {
                $attachment_data = array('upload_data' => $this->upload->data());
                $uploadfile = $attachment_data['upload_data']['file_name'];
			$return = array("code" => 1);
		 }
		 echo json_encode($return);
	}
	
	public function ajax_music_add()
	{
			if ($_FILES["picture"]["error"] > 0 && $_FILES["pictureFile"]["error"] > 0)
  {
   $return = array('code' => 2,'message' => 'File loading error!');
  }else{ 

           $config1['upload_path'] = 'uploads/music/';
		   $config1['allowed_types'] = 'mp3';
           
           //$data['max_size'] = '5000';
            $config1['encrypt_name'] = true;

            $this->load->library('upload', $config1);

            if ($this->upload->do_upload('picture')) {  
                $attachment_data = array('upload_data' => $this->upload->data());
                $uploadfile = $attachment_data['upload_data']['file_name'];
			// $return = array('code' => 1,'message' => 'Successfully added');	
		 }else{ 
			$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
		 }
		 
		 
		 $config2['upload_path'] = 'uploads/music/image/';
         $config2['allowed_types'] = 'jpg|png|gif|jpeg';
           //$data['max_size'] = '5000';
         $config2['encrypt_name'] = true;

         $this->upload->initialize($config2);


          if ($this->upload->do_upload('pictureFile')) {
          $attachment_data_image = array('upload_data' => $this->upload->data());
           $picturefile = $attachment_data_image['upload_data']['file_name'];
      //  $return = array('code' => 1,'message' => 'Successfully added');	


            /*For image resize*/
           $this->load->library('image_lib');
          $configer =  array(
              'image_library'   => 'gd2',
              'source_image'    =>  $attachment_data_image['upload_data']['full_path'],
              'maintain_ratio'  =>  TRUE,
              'width'           =>  150,
              'height'          =>  150,
            );
            $this->image_lib->clear();
            $this->image_lib->initialize($configer);
            $this->image_lib->resize();


		 }else{ 
			$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
			
		 }
		 
		
		 if($return['code'] != 2){ 
		 $array = array(
	      'music_title' => $this->input->post('music_title'),
          'music_desc' => $this->input->post('music_description'),
          'music_artist' => $this->input->post('music_artist'),
          'music_type' => $this->input->post('music_type'),
          'music_file' => $uploadfile,
		  'music_image' =>  $picturefile,
		  'category_id' => $this->input->post('music_type'),
          // 'music_image' => $pictureFile,
        'music_created' =>date('Y-m-d H:i:s')
	   );
	   $this->Common_model->insertData("music",$array);
				
$return = array('code' => 1,'message' => 'Successfully added');
		 }
	}
	echo json_encode($return);	
	}
	
	public function ajax_music_edit()
	{
		$music_id = $this->input->post('music_id');
		
		$singleData = $this->Common_model->getsingle("music",array("music_id" => $music_id));
		
		
  //print_r($_FILES["picture"]["name"]);
 // print_r($_FILES["pictureFile"]["name"]);die;
           $config1['upload_path'] = 'uploads/music/';
		   $config1['allowed_types'] = 'mp3';
           
           //$data['max_size'] = '5000';
            $config1['encrypt_name'] = true;

            $this->load->library('upload', $config1);

          if(!empty($_FILES["picture"]["name"])){ 
            if ($this->upload->do_upload('picture')) { 
                $attachment_data = array('upload_data' => $this->upload->data());
                $uploadfile = $attachment_data['upload_data']['file_name'];
			// $return = array('code' => 1,'message' => 'Successfully added');	
		 }else{ 
			$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
		 }
		  }
		 
		 $config2['upload_path'] = 'uploads/music/image/';
         $config2['allowed_types'] = 'jpg|png|gif|jpeg';
           //$data['max_size'] = '5000';
         $config2['encrypt_name'] = true;

         $this->upload->initialize($config2);

        if(!empty($_FILES["pictureFile"]["name"])){ 
          if ($this->upload->do_upload('pictureFile')) {
          $attachment_data_image = array('upload_data' => $this->upload->data());
           $picturefile = $attachment_data_image['upload_data']['file_name'];
      //  $return = array('code' => 1,'message' => 'Successfully added');	


      /*For image resize*/
           $this->load->library('image_lib');
          $configer =  array(
              'image_library'   => 'gd2',
              'source_image'    =>  $attachment_data_image['upload_data']['full_path'],
              'maintain_ratio'  =>  TRUE,
              'width'           =>  150,
              'height'          =>  150,
            );
            $this->image_lib->clear();
            $this->image_lib->initialize($configer);        
            $this->image_lib->resize();	   
		 }else{ 
			$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
			
		 }
		}
		 
		 if(!empty($picturefile)){
			 $picfile = $picturefile;
		 }else{
			 $picfile = $singleData->music_image;
		 }
		 
		 if(!empty($uploadfile)){
			 $upfile = $uploadfile;
		 }else{
			 $upfile = $singleData->music_file;
		 }
		
		 if($return['code'] != 2){ 
		 $array = array(
	      'music_title' => $this->input->post('music_title'),
          'music_desc' => $this->input->post('music_description'),
          'music_artist' => $this->input->post('music_artist'),
          'music_type' => $this->input->post('music_type'),
          'music_file' => $upfile,
		  'music_image' =>  $picfile,
		  'category_id' => $this->input->post('music_type'),
          // 'music_image' => $pictureFile,
        'music_created' =>date('Y-m-d H:i:s')
	   );
	   $this->Common_model->updateData("music",$array,array('music_id' => $this->input->post("music_id")));
				
$return = array('code' => 1,'message' => 'Music is successfully updated');
		 }
	
	
	echo json_encode($return);
	}
	
  }