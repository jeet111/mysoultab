<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="row">

			<div class="col-md-12">

				<div class="">

					<?php //echo "<pre>"; print_r($acknowledgeData); echo "</pre>";?>

					<div class="card-body">

						<div class="status-cng"></div>

						<?php if ($this->session->flashdata('success')) { ?>

							<div class="alert alert-success message">

								<button type="button" class="close" data-dismiss="alert">x</button>

								<?php echo $this->session->flashdata('success'); ?></div>

							<?php } ?>



							<table class="table table-hover tab_comn" id="sampleTable">

								<thead>

									<tr>

										<th>S no.</th>

										<th>Medicine Name</th>

										<th>Pharma Company Name</th>

										<th>Acknowledge Description</th>

										<th>Action</th>

									</tr>

								</thead>

								<tbody>

									<?php

									$i = 1;

									foreach ($acknowledgeData as $acknowledge) {



										?>

										<tr>

											<td><?php echo $i++; ?></td>

											<td><?php echo $acknowledge->medicine_name; ?></td>

											<td><?php echo $acknowledge->name; ?></td>

											<td>

												<span class="ttt">

													<?php echo $acknowledge->acknowledge_desc; ?>

												</span>

											</td>



											<td>

												<div class="link-del-view">



										<!-- <div class="tooltip-2">

											<a href="<?php echo base_url(); ?>admin/edit_acknowledge/<?php echo $acknowledge->acknowledge_id; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>

											<span class="tooltiptext">Edit</span>

										</div> -->



										<div class="tooltip-2">

											<?php

											if (isset($staff_permission[8]['view_permission']) == 1) {

												?>

												<a href="<?php echo base_url(); ?>admin/view_acknowledge/<?php echo $acknowledge->acknowledge_id; ?>">

													<i class="fa fa-eye" aria-hidden="true"></i></a>

													<span class="tooltiptext">View</span>

												</div>

												<?php

											} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

												?>

												<a href="<?php echo base_url(); ?>admin/view_acknowledge/<?php echo $acknowledge->acknowledge_id; ?>">

													<i class="fa fa-eye" aria-hidden="true"></i></a>

													<span class="tooltiptext">View</span>

												</div>

												<?php

											}

											?>

											





											<?php

											if (isset($staff_permission[8]['delete_permission']) == 1) {

												?>

												<div class="tooltip-2">

													<a href="<?php echo base_url(); ?>admin/delete_acknowledge/<?php echo $acknowledge->acknowledge_id; ?>" onclick="return confirm('Are you sure, you want to delete this acknowledge ?')">

														<i class="fa fa-trash-o" aria-hidden="true"></i>

													</a>

													<span class="tooltiptext">Delete</span>

												</div>

												<?php

											} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

												?>

												<div class="tooltip-2">

													<a href="<?php echo base_url(); ?>admin/delete_acknowledge/<?php echo $acknowledge->acknowledge_id; ?>" onclick="return confirm('Are you sure, you want to delete this acknowledge ?')">

														<i class="fa fa-trash-o" aria-hidden="true"></i>

													</a>

													<span class="tooltiptext">Delete</span>

												</div>

												<?php

											}

											?>

											





										</div>



									</div>

								</td>

							</tr>

						<?php } ?>

					</tbody>

				</table>

			</div>

		</div>

	</div>

</div>

</div>

</div>





<style type="text/css">

	.ttt{

		display:inline-block;

		width:180px;

		white-space: nowrap;

		overflow:hidden !important;

		text-overflow: ellipsis;

	}

</style>

