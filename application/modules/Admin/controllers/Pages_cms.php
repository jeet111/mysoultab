<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Pages_cms extends MX_Controller {

	private $staff_permission = array();
	private $access_permission = true;
	public function __construct(){

		parent:: __construct();

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model('Common_model');
		$this->load->model('login_model');
		$this->load->model('Feature_model');

		$this->load->helper(array('url'));
		$this->load->helper(array('form'));

		$user_id = $this->session->userdata('logged_ins')['id'];
		if(empty($user_id)) {
			redirect('admin/login');
		}

		if ($this->session->userdata('logged_ins')['user_role'] == 5) {
			$uid = $this->session->userdata('logged_ins')['id'];
			$permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
			if (!empty($permission_list)) 
			{
				foreach ($permission_list as $k => $v) 
				{
					$this->staff_permission[$v['permission_id']] = $v;
				}
			}
		}

	}

	public function permission_access($permission_id=6)
	{
		if ($this->session->userdata('logged_ins')['user_role'] != 1) 
		{
			if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
			{
				$this->access_permission = false;
			}
		}
	}

	public function pages_list()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$data['PageslistData'] = $this->Common_model->getAllorderby('cp_cms_pages','page_id ','desc');
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'Pages CMS');
			$this->template->load('admin_dashboard_layout', 'contents', 'pages_list', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}
	public function add_custom_page()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$this->form_validation->set_rules('feature_name', 'Feature Name', 'required');
		$this->form_validation->set_rules('feature_desc', 'Feature Description.', 'required');
		$this->form_validation->set_rules('feature_Cat', 'Feature Category.', 'required');

		$feature_name = $this->input->post('feature_name');
		$feature_Cat = $this->input->post('feature_Cat');
		$feature_desc = $this->input->post('feature_desc');

		if ($this->form_validation->run() == TRUE)
		{


			$data['upload_path'] = 'uploads/featureImages/feature_icons';
			$data['allowed_types'] = 'jpg|png|gif|jpeg';    
			$data['max_size'] = '5000';
			$data['encrypt_name'] = true;
			$this->load->library('upload', $data);

			if ($this->upload->do_upload('feature_icon')) {
				$attachment_data = array('upload_data' => $this->upload->data());
				$uploadfile = $attachment_data['upload_data']['file_name'];
				
				$array = array(
					'feture_name' => $feature_name,
					'cat_id'=>$feature_Cat,
					'feature_icon'=>$uploadfile,
					'feature_desc'=>$feature_desc,
					'create_date' => date('Y-m-d H:i:s')
				);
			 //echo '<pre>';print_r($array );die;
				$test_type_id = $this->Feature_model->insertData('cp_tabfeatures',$array);

			}


			$this->session->set_flashdata('success', 'Feature Successfully Added.');
			redirect('admin/add_custom_page');

		}

		$data['staff_permission'] = $this->staff_permission;

		$this->permission_access(6);
		if ($this->access_permission) {
			$data['featurescat_list'] = $this->Common_model->getAllorderby('cp_tabfeature_category','id','desc');
			$this->template->set('title', 'Tab Features');
			$this->template->load('admin_dashboard_layout', 'contents', 'add_custom_page', $data);
		} else {
			$data['featurescat_list'] = $this->Common_model->getAllorderby('cp_tabfeature_category','id','desc');
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}

	public function edit_page(){
		$data['menuactive'] = $this->uri->segment(2);
    //$this->form_validation->set_rules('template_id','Template Id','required');
    //$this->form_validation->set_rules('subject','Subject','required');
		$page_id = $this->uri->segment('3');
		$data['singleData'] = $this->Common_model->getsingle('cp_cms_pages',array('page_id' => $page_id));
    //$this->form_validation->set_rules('description','Description','required');
    //$this->form_validation->set_rules('template_id','Template Id','required');
		$this->form_validation->set_rules('page_name','Page Name','required');
		//$this->form_validation->set_rules('subject','Subject','required');
		$this->form_validation->set_rules('editor1','Description','required');

		if($this->form_validation->run()==TRUE)
		{
			$page_name = $_POST['page_name'];
			$description = $_POST['editor1'];
			$arrayData = array(
				'page_name'=>$page_name,
				'description'=>$description,
				'updated_date'=>date('Y-m-d H:i:s')
			);

      // print_r($arrayData);
      // die;

			$this->Common_model->updateData('cp_cms_pages',$arrayData,array('page_id' =>$page_id));     

      //$this->Common_model->updateData('about_page',$array,array('template_id' => 1));
			$this->session->set_flashdata('success', 'Successfully updated.');
			redirect('admin/pages_list');
		}
		$data['staff_permission'] = $this->staff_permission;

		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'CMS');
			$this->template->load('admin_dashboard_layout', 'contents', 'cms_pages_editor', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}


	public function delete_feature_category()
	{
		$segment = $this->uri->segment('3');
		$this->Feature_model->delete('cp_tabfeature_category',array('id' => $segment));
		//$this->Common_model->delete('cp_vital_test_type_unit',array('test_type_id' => $segment));
		$this->session->set_flashdata('success', 'Successfully deleted');
		redirect('admin/feature_category_list');
	}


	public function getFeture()
	{
		$fetr_id = $this->input->post('feature_id');
		$fetrData= $this->Common_model->getsingle('cp_tabfeatures',array('id' => $fetr_id));


		echo json_encode($fetrData);
	}	

}