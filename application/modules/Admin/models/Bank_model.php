<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bank_model extends CI_Model{
    function __construct() {
        
    }
	
		public function insertData($table,$datainsert)
	{
		$this->db->insert($table,$datainsert);
		return $this->db->insert_id();
	}
	
	public function updateData($table,$data,$where)
	{
	   $this->db->update($table,$data,$where);
	   return $this->db->affected_rows();
	}
	

	public function updateData1($table,$data)
	{
	   $this->db->update($table,$data);
	   return $this->db->affected_rows();
	}
	
	public function delete($table,$where)
	{
	   $this->db->delete($table,$where);
	   return;
	}
	
	
	public function getsingle($table,$where)
	{
		$q = $this->db->get_where($table,$where);
		return $q->row();
	}

    
    
   public function getAllwhereorderby($table,$where,$order_id,$order_by)
	{
		$this->db->select('*');
		$this->db->order_by($order_id,$order_by);
		$q = $this->db->get_where($table,$where);	
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	
	public function getAll($table)
	{
		$this->db->select('*');		
		$q = $this->db->get($table);		
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	
	public function getAllorderby($table,$order_id,$order_by)
	{
		$this->db->select('*');	
        $this->db->order_by($order_id,$order_by);		
		$q = $this->db->get($table);		
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	
	
	
	public function getAllwhere($table,$where)
	{
		$this->db->select('*');
		$q = $this->db->get_where($table,$where);
		$num_rows = $q->num_rows();
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	
	function jointwotablenn($table, $field_first, $table1, $field_second,$where='',$field,$order_id,$order_by) {

        $this->db->select($field);
	   $this->db->order_by( $order_id, $order_by);
        $this->db->from("$table");
        $this->db->join("$table1", "$table1.$field_second = $table.$field_first"); 
        if($where !=''){
        $this->db->where($where); 
        }
        $q = $this->db->get();
        if($q->num_rows() > 0) {
            foreach($q->result() as $rows) {
                $data[] = $rows;
            }
            $q->free_result();
            return $data;
        }
    }
      
   
/*Created by 95 for show all banks */
    public function getAllbanks()
    {
    	 $this->db->select('*');
      	 $this->db->from('cp_bank');
      	 $this->db->join('cp_users','cp_users.id=cp_bank.user_id','left');
      	 $this->db->order_by('bank_id','desc');
         $query = $this->db->get();
         return $query->result();
    }



/*Created by 95 for show single bank */
    public function getBank($bank_id='')
    {
    	 $this->db->select('*');
      	 $this->db->from('cp_bank');
      	 $this->db->join('cp_users','cp_users.id=cp_bank.user_id','left');
      	 $this->db->where('bank_id',$bank_id);
         $query = $this->db->get();
         return $query->result();
    }





/*Created by 95 for show all favourite banks */
public function getAllfavourite_banks_list(){
		 $this->db->select('*');
      	 $this->db->from('cp_fav_banks');
      	 $this->db->join('cp_users','cp_users.id=cp_fav_banks.user_id','left');
      	 $this->db->order_by('fav_bank_id','desc');
         $query = $this->db->get();
         return $query->result();
}


/*Created by 95 for show single favourite bank */
public function getFavourite_bank($fav_bank_id=''){
		 $this->db->select('*');
      	 $this->db->from('cp_fav_banks');
      	 $this->db->join('cp_users','cp_users.id=cp_fav_banks.user_id','left');
      	 $this->db->where('fav_bank_id',$fav_bank_id);
         $query = $this->db->get();
         return $query->result();
}


}