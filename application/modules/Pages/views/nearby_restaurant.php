<aside class="right-side">

	<section class="content-header no-margin">

		<h1 class="text-center"><i class="fa fa-cutlery" aria-hidden="true"></i>

			All Restaurant <a href="javascript:void(0)" class="search-open"><i class="fa fa-search"></i></a>

		</h1>

		<div class="searching">

			<div class="search-inline">

				<form method="post" action="" name="restaurant_search" id="restaurant_search">

					<input type="text" class="form-control" placeholder="Search Restaurant" name="restaurant_title" id="restaurant_title" value="<?php if(!empty($rest_search)){ echo $rest_search; }?>">

					<a href="javascript:void(0)" class="dnt_drop" id="search_sub"><i class="fa fa-search"></i></a>

					<a href="javascript:void(0)" class="search-close"><i class="fa fa-times"></i></a>

				</form>

			</div>

		</div>

	</section>

	<section class="content weather-bg news_section">
		
		<!-- setting start -->
		<div class="col-md-12">
			<div class="col-md-10">
				<h3>All Restaurant</h3>
				<?php if(!empty($btnsetingArray)){ ?>
					<?php if($btnsetingArray[0]->settings=='1'){ ?>

						<label><input type="radio" name="btnshow" value='1' checked="checked">Now All Restaurant app showing on the mysoultab APP<br></label>

					<?php }else{ ?>

						<label> <input type="radio" name="btnshow" value='1' checked="checked">Now All Restaurant app not showing on the mysoultab APP </label>

					<?php } ?>

				<?php } ?> 
			</div>
			<div class="col-md-2 delete-email">
				<button class="btn btn-primary chk">Settings</button>
			</div> 
			<div id="para" style="display: none "> 



				<form id="submit_form" method="post" action="<?php echo base_url()?>savsettings "> 
					<div class="col-md-10">
						<label> <input type="checkbox" name="butn_seting" id="butn_seting" value="">Please check if want to show the All Restaurant button in mysoultab App </label>
						<input type="hidden" name="btn" value="all_restaurant">
					</div>
					<div class="col-md-2">
						<input class="btn btn-primary chkdd" type="submit" name="submit" value="submit" >
					</div>
				</form>
				<div id="response"></div>
			</div>

			<!-- </div> -->

			<!-- End -->
		</section>

		
		<input type="hidden" name="get_lat" id="get_lat" value="<?php echo $_GET["lat"]; ?>">

		<input type="hidden" name="get_long" id="get_long" value="<?php echo $_GET["long"]; ?>">

		<section class="content photo-list">

			<div class="photo-listMain main_restaurant" id="main_restaurant">

				<div class="row">

					<div class="col-md-12">







						<h5 class="sr_rest">



							<!-- Restaurants List -->

						</h5>





						<div class="select_radius">










							<?php if($unit[0]->place_unit=='KM'){ ?>



								<select class="form-control" id="get_redius" name="get_redius">

									<option value="5000" <?php if($this->uri->segment("3") == '5000'){ echo 'selected'; } ?> >5 <?php echo $unit[0]->place_unit; ?></option>

									<option value="10000" <?php if($this->uri->segment("3") == '10000'){ echo 'selected'; } ?>>10 <?php echo $unit[0]->place_unit; ?></option>

									<option value="15000" <?php if($this->uri->segment("3") == '15000'){ echo 'selected'; } ?>>15 <?php echo $unit[0]->place_unit; ?></option>

								</select>



							<?php }else if($unit[0]->place_unit=='Miles'){ ?>

								<select class="form-control" id="get_redius" name="get_redius">

									<option value="8047" <?php if($this->uri->segment("3") == '8047'){ echo 'selected'; } ?> >5 <?php echo $unit[0]->place_unit; ?></option>

									<option value="16093" <?php if($this->uri->segment("3") == '16093'){ echo 'selected'; } ?>>10 <?php echo $unit[0]->place_unit; ?></option>

									<option value="24140" <?php if($this->uri->segment("3") == '24140'){ echo 'selected'; } ?>>15 <?php echo $unit[0]->place_unit; ?></option>

								</select>



							<?php } ?>



						</div>



					</div>



				</div>

				<input type="hidden" name="pagetoken" id="pagetoken" value="<?php echo $pagetoken; ?>">

				<div class="row" id="main-cntdiv">



					<?php

					$image = '';

					$i=1;

					if(!empty($nearbyrestaurants)){

						foreach($nearbyrestaurants as $rest){



							$image = $rest['rest_image'];

							$fav = $this->common_model->getSingleRecordById('cp_fav_restaurant',array('fav_rest_place_id'=> $rest['place_id'],'user_id' => $this->session->userdata('logged_in')["id"]));

							?>

							<div class="col-sm-6 col-md-4 loadData" id="myData_<?php echo $i;?>">





								<div class="bx_rest">

									<div class="bx_mig">



										<?php if($image){ ?>



											<img src='<?php echo $image; ?>' class="" alt="">



										<?php }else{ ?>

											<img src="<?php echo base_url();?>/assets/images/default_rest.jpg" class="" alt="">

											<!-- <img src='<?php echo base_url();?>uploads/' class="" alt=""> -->



										<?php } ?>

										<span id="re_<?php echo $rest['place_id']; ?>"> <a href="javascript:void(0);" class="fav_none_<?php echo $rest['place_id']; ?>" id="favorite_restaurant_<?php echo $rest['place_id']; ?>" data-restaurant="<?php echo $rest['place_id']; ?>"><?php if($fav['fav_rest_place_id']){ ?><i class="fa fa-heart"></i><?php }else{ ?><i class="fa fa-heart-o remove"></i><?php } ?></a></span>

										<span style="display:none" id="renew_<?php echo $rest['place_id']; ?>"> <a href="javascript:void(0);" id="favorite_restaurant_<?php echo $rest['place_id']; ?>" data-restaurant="<?php echo $rest['place_id']; ?>"><i class="fa fa-heart"></i></a></span>

										<span style="display:none" id="renew2_<?php echo $rest['place_id']; ?>"> <a href="javascript:void(0);" id="favorite_restaurant_<?php echo $rest['place_id']; ?>" data-restaurant="<?php echo $rest['place_id']; ?>"><i class="fa fa-heart-o remove"></i></a></span>

									</div>

									<div class="bx_name">



										<h4>

											<?php

											if($unit[0]->place_unit=='KM'){

												$uri = "5000";

											}else if($unit[0]->place_unit=='Miles'){

												$uri = "8047";

											}

											?>



											<a href="<?php echo base_url() ?>view_restaurant/<?php echo base64_encode($rest['place_id']); ?>/<?php if($this->uri->segment(2) == ''){ echo $uri; }if($this->uri->segment(2) == 'radius' || !empty($this->uri->segment(3))){ echo $this->uri->segment(3); }else if($this->uri->segment(2) != 'radius' || empty($this->uri->segment(3)) || !empty($this->uri->segment(2))){ echo $this->uri->segment(2); } ?>?lat='<?php echo $_GET["lat"] ?>'&long='<?php echo $_GET["long"] ?>'" ><?php echo $rest['rest_name']; ?></a></h4>

											<ul class="abs">

												<?php for($i=1;$i<=$rest['rating'];$i++) { ?>



													<li><i class="fa fa-star" aria-hidden="true"></i></li>

												<?php }

												if (strpos($rest['rating'],'.')) { ?>



													<li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>

													<?php $i++; } ?>

													<?php while ($i<=5) { ?>

														<li><i class="fa fa-star-o" aria-hidden="true"></i></li>

														<?php $i++; }



														?>



													</ul>

												</div>

											</div>

										</div>

									<?php }}else{ ?>

										<div class="photo-list-empty">

											<span class="blnk_photo"><i class="fa fa-picture-o" aria-hidden="true"></i></span>

											<h4>Restaurant not found</h4>

										</div>

									<?php } ?>

								</div>



							</div>

						</section>

					</aside>

					<script src="<?php echo base_url(); ?>assets/js/jquery.min2.js"></script>

					<script>

  /*var geocoder;



  if (navigator.geolocation) {

    navigator.geolocation.getCurrentPosition(successFunction, errorFunction);

}

//Get the latitude and the longitude;

function successFunction(position) {

    var lat = position.coords.latitude;

	$("#get_lat").val(lat);



    var lng = position.coords.longitude;

	$("#get_long").val(lng);



}



function errorFunction(){

  //  alert("Geocoder failed");

}*/







$('form').submit(function(){

	return false;

});

// javascript

document.getElementById('restaurant_search').onsubmit = function() {

	return false;

}

$('#restaurant_title').keypress(function(e) {

	if (e.keyCode == $.ui.keyCode.ENTER) {

		e.preventDefault();

		var restaurant_title = $("#restaurant_title").val();

		var radius = $("#get_redius").val();

		var lat = "<?php echo $_GET["lat"] ?>";

		var longt = "<?php echo $_GET["long"] ?>";



		window.location = "<?php echo base_url(); ?>nearby_restaurant/"+restaurant_title+'/'+radius+'?lat='+lat+'&long='+longt;

      //  submit_with_ajax(answer);

  }

});





/*$.getJSON('https://ipinfo.io/geo', function(response) {

	var loc = response.loc.split(',');

	var coords = {

		latitude: loc[0],

		longitude: loc[1]

	};

	$("#get_lat").val(loc[0]);

	$("#get_long").val(loc[1]);



});*/





function initialize()

{

	var lat = $("#get_lat").val();

	var longt = $("#get_long").val();

	$.ajax({

		type:'POST',

		url: '<?php echo base_url(); ?>Pages/session_value',

		data: 'lat='+lat+'&longt='+longt,

		dataType:'json',



		success:function(data){



		}



	});

}









</script>



<script>

	$(document).ready(function (e)

	{

		$(window).scroll(function()

		{

			scrollMore();

		});



	});



	function scrollMore()

	{

		if($(window).scrollTop() == ($(document).height() - $(window).height()))

		{

	//$(window).unbind("scroll");

	var records = $("#pagetoken").val();



	//var offset = $('[id^="myData_"]').length;



	if(records !=''){



		$('#main-cntdiv').append('<img id="loader_img" style="width:50px" src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif" />');

		loadMoreData(records);



	}



}

}



function loadMoreData(offset)

{



	var radius = $("#get_redius").val();

	var off = $("#pagetoken").val();

	var lat = $("#get_lat").val();

	var longt = $("#get_long").val();



	$.ajax({

		type: 'post',

		async:false,

		url: '<?php echo base_url(); ?>Pages/get_ajax_offset_token',

		data: 'offset='+off+'&radius='+radius+'&lat='+lat+'&long='+longt,

		dataType: "json",

		success: function(data)

		{



			$("#pagetoken").val(data.pagetoken);



			if(data.pagetoken == 'null'){



			}else{

				$('#main-cntdiv').append(data.list);

			}

			$('#loader_img').remove();

		},

		error: function(data)

		{

			alert("ajax error occured�"+data);

		}

	}).done(function()

	{



		$(window).bind("scroll",function()

		{



			scrollMore();

		});

	});

}



$(document).on('keyup','#restaurant_title1',function(e){

 // $( "#music_title" ).keypress(function( e ) {

 	e.preventDefault();

 	var restaurant_title = $("#restaurant_title").val();



 	$.ajax

 	({

 		url: "<?php echo base_url(); ?>Pages/getsearchdata",

 		type: "POST",

 		data: 'restaurant_title='+restaurant_title,

			//dataType: "json",

			success: function(response)

			{

				$('#main_restaurant').html(response);

			}



		});



 });



$(document).on('click','#search_sub',function(e){

	var restaurant_title = $("#restaurant_title").val();

	var radius = $("#get_redius").val();

	var lat = "<?php echo $_GET["lat"] ?>";

	

	var longt = "<?php echo $_GET["long"] ?>";

	if(restaurant_title!='' && radius!=''){



		window.location = '<?php echo base_url(); ?>nearby_restaurant/'+restaurant_title+'/'+radius+'?lat='+lat+'&long='+longt;



	}else{



		window.location = '<?php echo base_url(); ?>nearby_restaurant?lat='+lat+'&long='+longt;



	}

});





$(document).on('click','[id^="favorite_restaurant_"]',function(){

	var restaurant_id = $(this).attr('data-restaurant');

	$.ajax({

		type:'POST',

		url: "<?php echo base_url().'Pages/favorite_restaurant'; ?>",

		data: 'restaurant_id='+restaurant_id,

		success:function(data){

			$("#re_"+restaurant_id).hide();

			if(data == '0'){

				$("#renew_"+restaurant_id).hide();

				$(".fav_none_"+restaurant_id).hide();

				$("#renew2_"+restaurant_id).show();

			}else{

				$(".fav_none_"+restaurant_id).hide();

				$("#renew2_"+restaurant_id).hide();

				$("#renew_"+restaurant_id).show();

			}



		}

	});

});





$(document).on('change','#get_redius',function(){

	var radius = $("#get_redius").val();

	var lat = "<?php echo $_GET["lat"] ?>";

	

	var longt = "<?php echo $_GET["long"] ?>";

	window.location = "<?php echo base_url(); ?>nearby_restaurant/radius/"+radius+'?lat='+lat+'&long='+longt;

});



</script>

<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>









<script>

	var sp = document.querySelector('.search-open');

	var searchbar = document.querySelector('.search-inline');

	var shclose = document.querySelector('.search-close');

	function changeClass() {

		searchbar.classList.add('search-visible');

	}

	function closesearch() {

		searchbar.classList.remove('search-visible');

	}

	sp.addEventListener('click', changeClass);

	shclose.addEventListener('click', closesearch);



</script>

