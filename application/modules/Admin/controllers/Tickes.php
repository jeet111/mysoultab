<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Tickes extends MX_Controller {

	private $staff_permission = array();
	private $access_permission = true;
	public function __construct(){

		parent:: __construct();

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model('Common_model');
		$this->load->model('login_model');
		$this->load->model('Feature_model');

		$this->load->helper(array('url'));
		$this->load->helper(array('form'));

		$user_id = $this->session->userdata('logged_ins')['id'];
		if(empty($user_id)) {
			redirect('admin/login');
		}

		if ($this->session->userdata('logged_ins')['user_role'] == 5) {
			$uid = $this->session->userdata('logged_ins')['id'];
			$permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
			if (!empty($permission_list)) 
			{
				foreach ($permission_list as $k => $v) 
				{
					$this->staff_permission[$v['permission_id']] = $v;
				}
			}
		}

	}

	public function permission_access($permission_id=6)
	{
		if ($this->session->userdata('logged_ins')['user_role'] != 1) 
		{
			if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
			{
				$this->access_permission = false;
			}
		}
	}

	public function tickes_list()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$data['ticketData'] = $this->Common_model->getAllorderby('user_tickets','ticket_id','desc');
		


		$data['replyData'] = $this->Common_model->getAllorderby('tickets_reply','reply_id ','desc');
		
		$data['userData'] = $this->Common_model->getAllorderby('cp_users','id','desc');		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(6);
		if ($this->access_permission) {
			$this->template->set('title', 'Tickes');
			$this->template->load('admin_dashboard_layout', 'contents', 'ticket_list', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}



	public function view_detail()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$ticket_id = $this->uri->segment('3');
		$data['singleData'] = $this->Common_model->getsingle('user_tickets',array('ticket_id' => $ticket_id));
		$data['userData'] = $this->Common_model->getAllorderby('cp_users','id','desc');	
		$data['staff_permission'] = $this->staff_permission;

		$this->permission_access(7);
		if ($this->access_permission) {
			$this->template->set('title', 'Tickes');
			$this->template->load('admin_dashboard_layout', 'contents', 'view_ticket_detail', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}

	}

	

	

	
	public function reply_to_ticket()
	{
		$data['menuactive'] = $this->uri->segment(2);
		//$this->form_validation->set_rules('feature_name', 'Feature Name', 'required');
		$this->form_validation->set_rules('reply_desc', 'Feature Description.', 'required');
		//$this->form_validation->set_rules('feature_Cat', 'Feature Category.', 'required');


		$ticke_id = $this->uri->segment('3');
		$data['singleData'] = $this->Common_model->getsingle('user_tickets',array('ticket_id' => $ticke_id));
		
		$data['userData'] = $this->Common_model->getAllorderby('cp_users','id','desc');	

		// echo "<pre>";
		// print_r($data['singleData']);
		// echo "<br>";
		// echo $data['singleData']->genrate_ticketid;
		// die;

		//$feature_name = $this->input->post('feature_name');
		//$feature_Cat = $this->input->post('feature_Cat');
		$ticket_id=$data['singleData']->ticket_id;
		$genrated_id=$data['singleData']->genrate_ticketid;
		$user_id=$data['singleData']->user_id;
		$reply_desc = $this->input->post('reply_desc');

		if ($this->form_validation->run() == TRUE)
		{


			// $data['upload_path'] = 'uploads/featureImages/feature_icons';
			// $data['allowed_types'] = 'jpg|png|gif|jpeg';    
			// $data['max_size'] = '5000';
			// $data['encrypt_name'] = true;
			// $this->load->library('upload', $data);

			// if ($this->upload->do_upload('feature_icon')) {
			// 	$attachment_data = array('upload_data' => $this->upload->data());
			// 	$uploadfile = $attachment_data['upload_data']['file_name'];

			// 	$array = array(
			// 		'feture_name' => $feature_name,
			// 		'cat_id'=>$feature_Cat,
			// 		'feature_icon'=>$uploadfile,
			// 		'feature_desc'=>$feature_desc,
			// 		'create_date' => date('Y-m-d H:i:s')
			// 	);


			// 	$this->Common_model->updateData('cp_tabfeatures',$array,array('id' => $feature_id));


			// }else{

			$array = array(
				'ticket_id' => $ticket_id,
				'by_user'=>'',
				'by_admin'=> $this->session->userdata('logged_ins')['id'],
				'genrate_ticketid'=>$genrated_id,
				'user_id'=>$user_id,
				'reply_desc'=>$reply_desc,
				'created' => date('Y-m-d H:i:s')
			);
			 //echo '<pre>';print_r($array );die;
			$this->Feature_model->insertData('tickets_reply',$array);
			//$this->Common_model->updateData('tickets_reply',$array,array('id' => $feature_id));


		//}


			$this->session->set_flashdata('success', 'Message sent successfully.');
			redirect('admin/ticket_list');

		}

		$data['staff_permission'] = $this->staff_permission;

		$this->permission_access(6);
		if ($this->access_permission) {
			//$data['featurescat_list'] = $this->Common_model->getAllorderby('cp_tabfeature_category','id','desc');
			$this->template->set('title', 'Tickets');
			$this->template->load('admin_dashboard_layout', 'contents', 'reply_ticket', $data);
		} else {
			//$data['featurescat_list'] = $this->Common_model->getAllorderby('cp_tabfeature_category','id','desc');
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}

	// End
	


	

	public function view_ticket_conversation()
	{


		$data['menuactive'] = $this->uri->segment(2);
		$ticket_id = $this->uri->segment('3');
		//$data['singleData'] = $this->Common_model->getsingle('user_tickets',array('ticket_id' => $ticket_id));

		$data['singleData']= $this->Common_model->getAllwhereorderby('tickets_reply',array('ticket_id' => $ticket_id),'reply_id ','ASC');

		// echo "<pre>";
		// print_r($data['singleData']);
		// die;

		$data['userData'] = $this->Common_model->getAllorderby('cp_users','id','desc');	
		$data['staff_permission'] = $this->staff_permission;

		$this->permission_access(7);
		if ($this->access_permission) {
			$this->template->set('title', 'Tickes');
			$this->template->load('admin_dashboard_layout', 'contents', 'view_ticket_con_detail', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}

	}

	public function getTicketData()
	{
		echo "rtiuritur";
		die;
		// $data['menuactive'] = $this->uri->segment(2);
		// $ticket_id = $this->uri->segment('3');
		// $data['singleData'] = $this->Common_model->getsingle('tickets_reply',array('ticket_id' => $ticket_id));
		// //$replyData= $this->Common_model->getAllwhereorderby('tickets_reply',array('ticket_id' => $ticket_id),'reply_id ','ASC');
		// $data['userData'] = $this->Common_model->getAllorderby('cp_users','id','desc');	
		// $data['staff_permission'] = $this->staff_permission;

		// $this->permission_access(7);
		// if ($this->access_permission) {
		// 	$this->template->set('title', 'Tickes');
		// 	$this->template->load('admin_dashboard_layout', 'contents', 'view_ticket_con_detail', $data);
		// } else {
		// 	$this->template->set('title', 'Admin Panel - Staff');
		// 	$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		// }

	}	

}