<aside class="right-side">

  <section class="content-header no-margin">

    <h1 class="text-center"><i class="fa fa-file-text-o" aria-hidden="true"></i> Article

    <div class="back_reminder"><a href="javascript:window.history.back()" class="btn btn-danger">Back</a></div>

    </h1>

  </section>

  <div id="msgs"></div>

  <section class="content photo-list">

    <div class="articl_main">

      <div class="row">

        <div class="col-md-8">

          <div class="bx_artLeft">

            <div class="art_img">
		<?php if(!empty($detail_article->article_image) && $detail_article->article_image !=''){ ?>

			<?php if($detail_article->article_image){ ?>

              <img src='<?php echo base_url(); ?>/uploads/articles/<?php echo $detail_article->article_image; ?>' class="" alt="">

			<?php }else{ ?>

			<img src='<?php echo base_url(); ?>/uploads/articles/artic_img.jpg' class="" alt="">

			<?php } ?>

		<?php } ?>

            </div>

            <div class="bx_minImg">

              <div class="min_img">
							<?php if(!empty($detail_article->article_image) && $detail_article->article_image !=''){ ?>
								<?php if($detail_article->artist_image){ ?>
												<img src="<?php echo base_url(); ?>uploads/articles/<?php echo $detail_article->artist_image; ?>" class="" alt="">
								<?php }else{ ?>
								<img src="<?php echo base_url(); ?>uploads/doctor/happydoctor.jpg" class="" alt="">
								<?php } ?>
							<?php } ?>

              </div>

              <div class="min_txt">

							<?php if(!empty($detail_article->artist_name) && $detail_article->artist_name !='' || !empty($detail_article->article_created) && $detail_article->article_created !=''){ ?>
							
                <h5><?php echo $detail_article->artist_name; ?></h5>

                <span><?php echo date('j F Y, g:i a',strtotime($detail_article->article_created)); ?></span>
							<?php } ?>
              </div>

              <ul class="list_like">

                <li id="re"><a href="javascript:void(0);" id="favourite_article" data-segment="<?php echo $this->uri->segment('2'); ?>"><?php if($check_article){ ?><i class="fa fa-heart remove" aria-hidden="true"></i><?php }else{ ?><i class="fa fa-heart-o remove"></i><?php } ?></a></li>

				<li style="display:none" id="renew"><a href="javascript:void(0);" id="favourite_article" data-segment="<?php echo $this->uri->segment('2'); ?>"><i class="fa fa-heart" aria-hidden="true"></i></a></li>

				<li style="display:none" id="renew2"><a href="javascript:void(0);" id="favourite_article" data-segment="<?php echo $this->uri->segment('2'); ?>"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>

                <!-- <li>

                  <a href="<?php //echo base_url(); ?>delete_article/<?php //echo $detail_article->article_id; ?>" onclick="return confirm('Delete this record?')"><i class="fa fa-trash-o"></i></a></li> -->

              </ul>



            </div>
						<?php if(!empty($detail_article->article_title) && $detail_article->article_title !='' || !empty($detail_article->article_desc) && $detail_article->article_desc !=''){ ?>
						
            <h4><?php echo $detail_article->article_title; ?></h4>
            <p><?php echo $detail_article->article_desc; ?></p>
						
						<?php } ?>
          

          </div>

        </div>

        <div class="col-md-4">

          <div class="">

            <div class="magmax_Posts">

              <h2 class="mag-title">Recent Articles</h2>        

              <div class="magmax-popular-post">

              <ul class="list_popu">

			  <?php if(!empty($popular_article)){

                    foreach($popular_article as $popular){

			  ?>

                <li class="single-item">

                  <div class="popular-post-wrapper">

                    <div class="post-thumb-wrapper">

					  <?php if($popular->article_image){ ?>



              <a href="<?php echo base_url(); ?>detail_article/<?php echo base64_encode($popular->article_id); ?>">



<img src="<?php echo base_url(); ?>uploads/articles/<?php echo $popular->article_image; ?>" >

</a>

						<?php }else{ ?>

                      <a class="post-thumb" href="#">

                      						

						<img src="http://demo.themexpert.com/wordpress/magmax/wp-content/uploads/2018/04/constitution-gavel-and-handcuffs-PQBC5FA-150x150.jpg" class="attachment-thumbnail" alt="">

						

                      </a>

						<?php } ?>

                    </div>                    

                    <div class="bx_txt">

                      <div class="bx_pop">

                        <h4><a href="<?php echo base_url(); ?>detail_article/<?php echo base64_encode($popular->article_id); ?>"><?php echo $popular->article_title; ?></a></h4>

                        <div class="popular_time"><?php echo date('j F Y',strtotime($popular->article_created)); ?></div>

                      </div>                      

                    </div>

                  </div>

                </li>

			  <?php } ?>

			  

			  <?php }else{ ?>

			  No records found

			  <?php } ?>

              

              </ul>

            </div>            

          </div>



          

        </div>

      </div>

    </div>

    

  </div>

</section>

</aside>

<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>



<script>

   

   $(document).on('click','#favourite_article',function(){

	   var article_id = $(this).attr('data-segment');

	   $.ajax({

            type:'POST',

            url: "<?php echo base_url().'Pages/favourite_article'; ?>",

            data: 'article_id=' + article_id,            

            success:function(data){	

			$("#re").hide();

               if(data == '0'){

                  $("#renew").hide();		   

                  $("#renew2").show();

				$("#msgs").html('<div class="alert alert-success">'+'Successfully unfavourite'+'</div>');

                setTimeout(function(){

                      $("#msgs").html("");

                     }, 1500);  

				  

			   }else{

				   $("#msgs").html('<div class="alert alert-success">'+'Successfully favourite'+'</div>');

                setTimeout(function(){

                      $("#msgs").html("");

                     }, 1500);  

		          $("#renew2").hide();		   

                  $("#renew").show();

			   }			   

				

            }

        });

	}); 



	

</script>



<script src="<?php echo base_url(); ?>/assets/js/jquery.minnn.js"></script>

<script src="<?php echo base_url();?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>
