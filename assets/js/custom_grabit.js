/*$(".alert").delay(4000).slideUp(200, function() {
  $(this).alert('close');
});*/
 
$(document).ready(function() {
  var owl = $("#owl-demo2");
  owl.owlCarousel({
    itemsCustom : [
      [0, 1],
      [480, 1],
      [640, 2],
      [854, 3],
      [1000, 3],
      [1200, 3],
      [1400, 3],
      [1600, 3]
    ],
    navigation : true
  });
});
  
$("#addClass").click(function () {
    $('#qnimate').addClass('popup-box-on');
});

$("#removeClass").click(function () {
    $('#qnimate').removeClass('popup-box-on');
});

/*------ Newsletter ------*/
$("#newsletter_form").validate({
    rules:{
        semail:{
            required: true,
            email:true,
            remote: {
            url: baseurl + "validate/email",
            type: "post",
          }
        },
    },
    messages: {
        semail: {
            remote: "Email already in use!"
        }
    },
    submitHandler:function(e){
        $(".newsletter .loading_image").show();
        $(".submit_news").prop("disabled", true);
        $.ajax({
            url: baseurl+"subscription",
            data:$("#newsletter_form").serialize(),
            type:"POST",
            dataType: "json",
            success:function(data){
                $(".newsletter .loading_image").hide();
                $(".submit_news").prop("disabled", false);
                $(".newsletter .updateemailmsg").html(data.msg);
                setTimeout(function(){
                    $(".newsletter .updateemailmsg").html("");
                }, 2000);
            }
        });
        return false;
    }
});

/*------ Login ------*/
$("#loginForm1").validate({
    rules:{
        uname:{
            required: true,
            /*remote: {
            url: baseurl + "check/userdata",
            type: "post",
          }*/
        },
        password:{
            required: true,
            minlength: 6,
        }
    },
    messages: {
        uname: {
            remote: "Details not matched"
        }
    },
    submitHandler:function(e){
        $("#loginForm .loading_image").show();
        $("#loginForm .login-btn").prop("disabled", true);
        $.ajax({
            url: baseurl+"user/login",
            data:$("#loginForm").serialize(),
            type:"POST",
            dataType: "json",
            success:function(data){
                $("#loginForm .loading_image").hide();
                $("#loginForm .login-btn").prop("disabled", false);
                
                $("#loginForm .alert").html("");
                if (data.status == 1) {
                    $("#loginForm .alert.alert-success").show();
                } else {
                    $("#loginForm .alert.alert-danger").show();
                }
                $("#loginForm .alert").html(data.msg);
                
                setTimeout(function(){
                    $("#loginForm .alert").hide();
                }, 2000);
            }
        });
        return false;
    }
});