<style type="text/css">
  html {
    width: 100%;
    overflow-x: hidden;
    overflow-y: hidden;
  }
</style>

<div class="signup-page forgot_pass_sec_n">
	<section class="signup">
    <!-- <img src="images/signup-bg.jpg" alt=""> -->
    <div class="container">
      <div class="signup-content forgot_pass_sec_n">

        <form method="POST" id="resestpass" class="signup-form" action="reset_password">
          <h2 class="form-title">Reset Password</h2>
          <?php if($this->session->flashdata('message')){
            ?>
            <div class="text-center" style="color: green; font-size: 18px;">
              <?php echo $this->session->flashdata('message'); ?>
            </div>
            <?php
          }elseif($this->session->flashdata('error')){
            ?>
            <div class="text-center" style="color: red; font-size: 18px;">
              <?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php
          }  
          ?>  

          <?php
          
          $id = $this->session->userdata('user_id');
          
          ?>

          <input type="hidden" name="user_id" id="user_id" value="<?php echo $id; ?>">


          <div class="form-group">
           <input type="password" class="form-input" name="new_password" id="new_password" placeholder="New Password"/>
           <td class="error"><?php echo form_error('new_password'); ?></td>
         </div>  
         <div class="form-group">
           <input type="password" class="form-input" name="confirm_password" id="confirm_password" placeholder="Confirm Password"/>
           <td class="error"><?php echo form_error('confirm_password'); ?></td>
         </div>                      
         <div class="form-group">
          <input type="submit" name="submit" id="submit" class="form-submit" value="Reset Password"/>
        </div>
      </form>
    </div>
  </div>
</section>
</div>
