<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Restaurant extends MX_Controller {

  private $staff_permission = array();
  private $access_permission = true;
  public function __construct(){   

    parent:: __construct();

    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->model('Common_model');
    $this->load->model('login_model');
    $this->load->helper(array('url'));
    $this->load->helper(array('form'));
	$this->load->library("pagination");

    $user_id = $this->session->userdata('logged_ins')['id'];
    if(empty($user_id)) {
      redirect('admin/login');
    }

    if ($this->session->userdata('logged_ins')['user_role'] == 5) {
      $uid = $this->session->userdata('logged_ins')['id'];
      $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
      if (!empty($permission_list)) 
      {
         foreach ($permission_list as $k => $v) 
         {
            $this->staff_permission[$v['permission_id']] = $v;
         }
      }
    }

  }

  public function permission_access($permission_id=11)
  {
    if ($this->session->userdata('logged_ins')['user_role'] != 1) 
    {
       if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
       {
          $this->access_permission = false;
       }
    }
  }
  
  public function restaurant_list()
  {
	  $data['menuactive'] = $this->uri->segment(2);
	 $data['restaurant_data'] = $this->Common_model->getAllorderby('cp_restaurant','rest_id','desc');
	 
	  $data['staff_permission'] = $this->staff_permission;
    

    $this->permission_access(11);
    if ($this->access_permission) {
      $this->template->set('title', 'Restaurant Management');
      $this->template->load('admin_dashboard_layout', 'contents', 'restaurant_list', $data);
    } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
    }
  }
  
  public function view_restaurant()
  {
	  $data['menuactive'] = $this->uri->segment(2);
	  
	  $segment = $this->uri->segment('3');
	  
	  $data['single_restaurant'] = $this->Common_model->getsingle("cp_fav_restaurant",array("fav_rest_id" => $segment));
		  
	    $data['staff_permission'] = $this->staff_permission;
      
	    
      $this->permission_access(11);
      if ($this->access_permission) {
        $this->template->set('title', 'View restaurant');
        $this->template->load('admin_dashboard_layout', 'contents' , 'view_restaurant', $data);
      } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
      } 
  }
  
  public function favorite_restaurant_list()
  {
	  $data['menuactive'] = $this->uri->segment(2);
	  $data['favorite_restaurant_data'] = $this->Common_model->getAllorderby('cp_fav_restaurant','fav_rest_id','desc');
    $data['users']=$this->Common_model->getAllorderby('cp_users','id','DESC');
    //echo "<pre>"; print_r($data['favorite_restaurant_data']);die;
	  
    $data['staff_permission'] = $this->staff_permission;
    
    $this->permission_access(11);
    if ($this->access_permission) {
      $this->template->set('title', 'Restaurant Management');
      $this->template->load('admin_dashboard_layout', 'contents' , 'favorite_restaurant_list', $data);
    } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
    } 
  }
  
  public function delete_fav_restaurant()
  {
	  $data['menuactive'] = $this->uri->segment(2);
	  $segment = $this->uri->segment('3');
	  $this->Common_model->delete("cp_fav_restaurant",array("fav_rest_id" =>$segment));
	  $this->session->set_flashdata('success', 'Successfully deleted');
      redirect('admin/favorite_restaurant_list'); 
  }
  
  public function delete_restaurant()
  {
	  $data['menuactive'] = $this->uri->segment(2);
	  $segment = $this->uri->segment('3');
	  
	  
	  $this->Common_model->delete("cp_restaurant",array("rest_id" =>$segment));
	  $this->Common_model->delete("cp_fav_restaurant",array("restaurant_id" =>$segment));
	  $this->session->set_flashdata('success', 'Successfully deleted');
      redirect('admin/restaurant_list'); 
  }
  
}