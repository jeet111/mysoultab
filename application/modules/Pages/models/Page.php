<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends CI_Model{

   public function __construct() {
        $this->userTbl = 'gb_users';
        $this->userTblSubs = 'subscribers';
        $this->testimonial = "gb_testimonial";
        $this->contentTbl = "gb_site_contents";
        $this->ordersTbl = "gb_orders";
        $this->catTbl = "otd_business_category";
        $this->bannerTbl = "gb_home_banners";
        $this->shoptravelTbl = "gb_shoper_travel";
        $this->servicesTbl = "gb_services";
        $this->userCatTbl = "otd_user_business_category";
        $this->ftTbl = "otd_footer_content";
        $this->pytTbl = "otd_user_option_payment";
        $this->notiTbl = "otd_notifications";
        $this->emailTpTbl = "otd_email_templates";
    }

    public function insert_data($table,$data) {

      $sql=$this->db->insert_string($table,$data);

      $this->db->query($sql);

      $last_id = $this->db->insert_id();

      return $last_id;
    }

    /*
    * Get testimonials
    */
    /*function getTestimonials(){
        $query = $this->db->order_by("test_date_created", "DESC")->get_where($this->testimonial, array("test_status"=>1));
        return $query->result_array();
    }*/
    function getTestimonials($params = array()){
      $this->db->select('*');
      $this->db->from($this->testimonial);
      if(array_key_exists("conditions",$params)){
        foreach ($params['conditions'] as $key => $value) {
          $this->db->where($key,$value);
        }
      }

      if(array_key_exists("test_id",$params)){
        $this->db->where('test_id',$params['testimonial_id']);
        $query = $this->db->get();            
        $result = $query->row_array();
      }else{
        //set start and limit
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
          $this->db->limit($params['limit'],$params['start']);
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
          $this->db->limit($params['limit']);
        }
        $query = $this->db->get();

        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
          $result = $query->num_rows();
        }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
          $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
        }else{
          $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
      }
      return $result;
    }

    /**
    *  GET LAT & LONG FROM ADDRESS
    */
    function get_lat_long($address){
       $address = str_replace(" ", "+", $address);
       $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
       $json = json_decode($json);
       if((isset($json)) && ($json->{'status'} == "OK")){
        $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'}; 
        $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};        
       }else{
         $lat = -25.274398; 
         $long = 133.775136; 
       }       
       $response = array('lat'=>$lat,'lng'=>$long);
       return $response;
    }


    function getArticles($args,$lang){
      $query = $this->db->get_where($this->contentTbl, array("content_id"=>$args,"lang_name"=>$lang));
      return $query->row_array();
    }

    function getOrders(){
      $this->db->select("*, (SELECT profile_image FROM gb_users WHERE id=user_id) AS shopper_img, (SELECT profile_image FROM gb_users WHERE id=traveler_id) AS traveler_img");
      $this->db->from('gb_orders');
      $this->db->where('order_status', 1);
      $this->db->order_by("o_id", "desc");
      $this->db->limit(3);
      $rows = $this->db->get();
      
      $result = $rows->result_array();
      //echo $this->db->last_query();
      //die();

      return $result;
    }

    /*
    * get categories lists
    */   
    function getCategoryRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->catTbl);  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("cat_id",$params)){
            $this->db->where('cat_id',$params['cat_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * Get Banner Images
    */
    function getBannerImages($params = array()){
        $this->db->select('*');
        $this->db->from($this->bannerTbl);
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }

        if(array_key_exists("bn_id",$params)){
            $this->db->where('bn_id',$params['content_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * Get Shopper Traveler
    */
    function getshoppertraveler($params = array()){
      $this->db->select('*');
      $this->db->from($this->shoptravelTbl);
      if(array_key_exists("conditions",$params)){
          foreach ($params['conditions'] as $key => $value) {
              $this->db->where($key,$value);
          }
      }

      if(array_key_exists("st_id",$params)){
          $this->db->where('st_id',$params['st_id']);
          $query = $this->db->get();            
          $result = $query->row_array();
      }else{
          //set start and limit
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
          $this->db->limit($params['limit'],$params['start']);
            
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
          $this->db->limit($params['limit']);
        }

          $query = $this->db->get();

        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
            $result = $query->num_rows();
        }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
          $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
        }else{
          $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
      }
      return $result;
    }

    /*
    * Get services
    */
    function getservices($params = array()){
      $this->db->select('*');
      $this->db->from($this->servicesTbl);
      if(array_key_exists("conditions",$params)){
          foreach ($params['conditions'] as $key => $value) {
              $this->db->where($key,$value);
          }
      }

      if(array_key_exists("service_id",$params)){
          $this->db->where('service_id',$params['service_id']);
          $query = $this->db->get();            
          $result = $query->row_array();
      }else{
          //set start and limit
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
          $this->db->limit($params['limit'],$params['start']);
            
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
          $this->db->limit($params['limit']);
        }

          $query = $this->db->get();

        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
            $result = $query->num_rows();
        }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
          $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
        }else{
          $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
      }
      return $result;
    }

    /*
    * external function for user purchase
    */
    function updateTransaction($params = array(), $where =array() ){
        $this->db->update($this->pytTbl, $params, $where);
        return true;
    }

    /*
    * get rows for business opening time table
    */
    function getEmailTemplateRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->emailTpTbl);        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        if(array_key_exists("tmplate_id",$params)){
            $this->db->where('tmplate_id',$params['tmplate_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    } 

/*
* External function for user purchase end
*/
}
?>