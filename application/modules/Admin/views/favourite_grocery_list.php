<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="row">

			<div class="col-md-12">

				<div class="">

					<?php //echo "<pre>"; print_r($favgroceryData); echo "</pre>";?>

					<div class="card-body">

						<div class="status-cng"></div>

						<?php if ($this->session->flashdata('success')) { ?>

                    <div class="alert alert-success message">

                        <button type="button" class="close" data-dismiss="alert">x</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                <?php } ?>

					<table class="table table-hover tab_comn" id="sampleTable">

						<thead>

							<tr>

								<th>S no.</th>

								<th>Username</th>

								<th>Grocery Name</th>

								<th>Grocery Address</th>							

								<th>Grocery Image</th>							

								<th>Action</th>

							</tr>

						</thead>

						<tbody>

							<?php 

							$i = 1;

						foreach ($favgroceryData as $grocery) {

								?>

							<tr>

								<td><?php echo $i++; ?></td>

								<td><?php echo $grocery->username; ?></td>

								<td><?php echo $grocery->fav_grocery_name; ?></td>

								<td><?php echo $grocery->fav_grocery_address; ?></td>

								<td>

									<?php if(!empty($grocery->fav_grocery_image)){ ?>

									<img src="<?php echo $grocery->fav_grocery_image; ?>" class="" alt="">

								<?php }else{ ?>

									<img src="<?php echo base_url();?>assets/images/bank_img.jpg" class="" alt="">

								<?php } ?>

								</td>

								<td>



									<div class="link-del-view">

										<?php

										if (isset($staff_permission[12]['view_permission']) == 1) {

											?>

											<div class="tooltip-2">

												<a href="<?php echo base_url(); ?>admin/view_favourite_grocery/<?php echo $grocery->fav_grocery_id; ?>">

													<i class="fa fa-eye" aria-hidden="true"></i></a>

												<span class="tooltiptext">View</span>

											</div>

											<?php

										} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

											?>

											<div class="tooltip-2">

												<a href="<?php echo base_url(); ?>admin/view_favourite_grocery/<?php echo $grocery->fav_grocery_id; ?>">

													<i class="fa fa-eye" aria-hidden="true"></i></a>

												<span class="tooltiptext">View</span>

											</div>

											<?php

										}

										?>

										

										<?php

										if (isset($staff_permission[12]['delete_permission']) == 1) {

											?>

											<div class="tooltip-2">

												<a href="<?php echo base_url(); ?>admin/delete_favourite_grocery/<?php echo $grocery->fav_grocery_id; ?>" onclick="return confirm('Delete this favourite grocery ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span> 

											</div>

											<?php

										} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

											?>

											<div class="tooltip-2">

												<a href="<?php echo base_url(); ?>admin/delete_favourite_grocery/<?php echo $grocery->fav_grocery_id; ?>" onclick="return confirm('Delete this favourite grocery ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span> 

											</div>

											<?php

										}

										?>

										

									</div>

									

							</div>

						</td>

					</tr>

					<?php } ?>

				</tbody>

			</table>

		</div>

	</div>

</div>

</div>

</div>

</div>