<aside class="right-side">    
    <section class="content-header no-margin">
        <h1 class="text-center"><i class="fa fa-plus-square-o" aria-hidden="true"></i>Update Profile</h1>
    </section>    
    <section class="content photo-list">
        <div class="photo-listMain">
             <form id="update_pro" enctype="multipart/form-data" method="post" class="frm_add">
		<?php echo validation_errors(); ?>	
	
                <div class="row"> 

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Name<font style="color: red;">*</font></label>
                            <input type="text" class="form-control required" id="name_user" name="name_user" placeholder="Name" value="<?php echo $singleData->name; ?>">
                        </div>
                    </div>



                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Username<font style="color: red;">*</font></label>
                            <input type="text" class="form-control" id="username" name="username" readonly placeholder="Username" value="<?php echo $singleData->username; ?>">
                        </div>
                    </div>
                    

                    </div>
               
                <div class="row"> 
                       <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Mobile no<font style="color: red;">*</font></label>
                            <input type="text" class="form-control required" id="mobile" name="mobile" placeholder="Mobile No" value="<?php echo $singleData->mobile; ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Profile Image<font style="color: red;">*</font></label>
                            <input type="file" class="form-control" id="profile_image" name="profile_image" onchange="loadFile(event)" placeholder="Profile Image" value="">
				<div id="flupmsg"></div>			
							<?php if($singleData->profile_image==''){ ?>
				<img id='output'>

			<?php }else{ ?>

				<img id='output' src="<?php echo base_url().'uploads/profile_images/'.$singleData->profile_image; ?>">

			<?php } ?>
                        </div>
                    </div>
                   
                </div>
				 <div class="row"> 
                       <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Email<font style="color: red;">*</font></label>
                            <input type="text" class="form-control required" id="mobile" name="mobile" readonly placeholder="Mobile No" value="<?php echo $singleData->mobile; ?>">
                        </div>
                    </div>
					 </div>
                   
                <div class="box-footer">
				<input type="submit" name="submit" id="submit" value="Submit" class="btn btn-primary" >                   
					<a href="<?php echo base_url(); ?>family_member_dashboard"  class="btn btn-danger">Back</a>
                </div>
            </form>              
        </div>
    </section>
</aside>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script> 

<script>
	function loadFile (event) { 
		 var pcFile = $('#profile_image').val().split('\\').pop();
  var pcExt     = pcFile.split('.').pop(); 
  var output = document.getElementById('output');
  if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"
             || pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){
		//$(".up_but").prop('disabled', false);
        $('#flupmsg').html('');			 
      
       output.src = URL.createObjectURL(event.target.files[0]);
	   $('#output').show();
      $('#flupmsg').html('');
  }else{
      $('#flupmsg').html('Please select only Image file');
      $('#output').hide();
	  //$(".up_but").prop('disabled', true);
  }
};


    $(document).ready(function() {
    $("#update_pro").validate({
            rules: {
                name_user:"required",
                mobile: "required",
                                      
             },
        
            messages: {
                name_user:"Please enter name.",
                mobile: "Please enter mobile no.",
                                                 
            },
        });

    });
</script>
    

                