<div class="footer-full">
	<div class="footer wow fadeInDown animated">
		<div class="container">
	    	<div class="row">
	    		<div class="col-md-4">
	    			<div class="footer-first">
	    				<div class="logo"><a href="<?php echo base_url(); ?>">Health Care</a></div>
	    				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer.</p>
    				
	    			</div>
	    		</div>
	    		<div class="col-md-2">
	    			<div class="footer-second">
	    				<h4>Company</h4>
	    					<ul>
                                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                                <li><a href="about-us">About</a></li>
								<li><a href="#">Features</a></li>
								<li><a href="contact">Contat</a></li>								
                            </ul>
	    			</div>
	    		</div>

	    		<div class="col-md-3">
			        <div class="footer-third">
			  	      <h4>Contac us</h4>
			  	      <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
			            681 4th Lorem Ipsum is <br>
			            New York, USA</p>
			            <p><i class="fa fa-phone" aria-hidden="true"></i> 123-456-7890</p>
			            <ul class="social-icons">		        			
		        			<li><a href="#"><i class="fa fa-facebook"></i></a></li>
		        			<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
		        			<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
		        			<li><a href="#"><i class="fa fa-twitter"></i></a></li>	        			
		        		</ul>
			        </div>
				</div>

				<div class="col-md-3">
			        <div class="footer-fourth">
			  	      <h4>RECENT LISTINGS</h4>
			  	      <p>Lorem simply<br>
						24One Residences<br>
						Kandis Residence<br>
						Lorem Ipsum is simply @ PLQ
					  </p>
			        </div>
				</div>

	    	</div>
	    </div>
	</div>

	<div class="footer_bottom">
		<div class="container">
	    	<div class="row">
	    		<div class="col-md-12">
	    			<div class="footer_bootom_first">
	    				<p>© Copyrights 2019 Care Pro. All Rights Reserved</p>
	    			</div>
	    		</div>
	    	</div>
	    </div>
	</div>
</div>

</div>

<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/styles/bootstrap-4.1.2/popper.js"></script>
<script src="assets/styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="assets/plugins/greensock/TweenMax.min.js"></script>
<script src="assets/plugins/greensock/TimelineMax.min.js"></script>
<script src="assets/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="assets/plugins/greensock/animation.gsap.min.js"></script>
<script src="assets/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="assets/plugins/OwlCarousel2-2.3.4/owl.carousel.js"></script>
<script src="assets/plugins/easing/easing.js"></script>
<script src="assets/plugins/progressbar/progressbar.min.js"></script>
<script src="assets/plugins/parallax-js-master/parallax.min.js"></script>
<script src="assets/plugins/jquery-datepicker/jquery-ui.js"></script>
<script src="assets/plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="assets/js/custom.js"></script>
</body>
</html>