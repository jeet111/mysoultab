<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Common_model_new extends CI_Model{
	function __construct() {
	}
	public function insertData($table,$datainsert)
	{
		$this->db->insert($table,$datainsert);
		return $this->db->insert_id();
	}
	public function updateData($table,$data,$where)
	{
		$this->db->update($table,$data,$where);
		return $this->db->affected_rows();
	}
	public function delete($table,$where)
	{
		$this->db->delete($table,$where);
		return;
	}
	public function deleteFaqs($table,$where)
	{
		extract($where);
		$this->db->where('faq_id', $faq_id);
		$this->db->update($table, array('status' => 3));
		return true;
	}
	public function getsingle($table,$where)
	{
		$q = $this->db->get_where($table,$where);
		return $q->row();
	}
	public function check_login_credentials($postdata){ 
		extract( $postdata ); 
		$this->db->select('*');
		//$this->db->from('admin');
		$this->db->from('admin');
		$this->db->where('username', $uname); 
		$this->db->where('password', md5($password));
		//$this->db->where('role', 1); 
		$query = $this->db->get();
		return $query->row();
	}
	public function get_row_with_con($table,$where)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		//echo $this->db->last_query();
		//die;
		return $query->row(); 
	} 
	public function getAllwhereorderby($table,$where,$order_id,$order_by)
	{
		$this->db->select('*');
		$this->db->order_by($order_id,$order_by);
		$q = $this->db->get_where($table,$where);	
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAll($table)
	{
		$this->db->select('*');		
		$q = $this->db->get($table);		
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAllFaq()
	{
		$this->db->select('*');
		$this->db->from("faqs");
		$this->db->where("status !=", 3);
		$query = $this->db->get();
		$result = ($query->num_rows() > 0)?$query->result_array():FALSE;
		return $result;
	}
	public function getAllor($table,$order_id,$order_by)
	{
		$this->db->select('*');	
		$this->db->order_by($order_id,$order_by);		
		$q = $this->db->get($table);		
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAllor1($table,$order_id,$order_by)
	{
		$this->db->select('*');	
		$this->db->order_by($order_id,$order_by);		
		$q = $this->db->get($table);		
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAllwhere($table,$where)
	{ 
		$this->db->select('*');
		$q = $this->db->get_where($table,$where);
		$num_rows = $q->num_rows();
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	function jointwotablenn($table, $field_first, $table1, $field_second,$where='',$field,$order_id,$order_by) {
		$this->db->select($field);
		$this->db->order_by( $order_id, $order_by);
		$this->db->from("$table");
		$this->db->join("$table1", "$table1.$field_second = $table.$field_first"); 
		if($where !=''){
			$this->db->where($where); 
		}
		$q = $this->db->get();
		if($q->num_rows() > 0) {
			foreach($q->result() as $rows) {
				$data[] = $rows;
			}
			$q->free_result();
			return $data;
		}
	}
	public function lang_data(){
		if($this->session->userdata("filterLanguage1")){
			$lang = $this->session->userdata("filterLanguage1");
		}else{
			$lang = 1;
		}
		$this->db->select('*');
		$this->db->from('language_contents');
		$this->db->where('content_language_id',$lang);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	function getfootercategories($lang){
		$this->db->select('*');	   
		$this->db->from("gc_footer_category_detail");
		$this->db->join("gc_footer_data_detail", "gc_footer_data_detail.cat_id = gc_footer_category_detail.cat_id","RIGHT"); 
		$this->db->where("gc_footer_data_detail.status", 1);
		$this->db->where("gc_footer_category_detail.status", 1);
		$this->db->where("gc_footer_category_detail.lang_id", $lang);
		$this->db->group_by("gc_footer_category_detail.cat_name");
		$this->db->order_by("gc_footer_category_detail.cat_id", "ASC");
		$query = $this->db->get();        
   //echo $this->db->last_query();die;    
		foreach ($query->result() as $category)
		{
			$return[$category->cat_id] = $category;
       $return[$category->cat_id]->subs = $this->get_sub_categories($category->cat_id,$lang); // Get the categories sub categories
   }
   return $return;   
}
public function get_sub_categories($category_id,$lang)
{
	$this->db->where('cat_id', $category_id);
	$this->db->where('status',1); 
	$this->db->where('lang_id',$lang);	
	$query = $this->db->get('gc_footer_data_detail');
  //echo $this->db->last_query();
	return $query->result();
}
function jointwotable($table, $field_first, $table1, $field_second,$where='',$field) {
	$this->db->select($field);
	$this->db->from("$table");
	$this->db->join("$table1", "$table1.$field_second = $table.$field_first","left"); 
	if($where !=''){
		$this->db->where($where); 
	}
	$q = $this->db->get();
	if($q->num_rows() > 0) {
		return $q->row();
	}else{
		return false;
	}
}


public function getcalenderDataBy($curentDate,$getByDate,$user)
{
	

	$sql = "SELECT * FROM cp_reminder WHERE reminder_date = '".$curentDate."'  AND user_id='".$user."'";

	
	//$sql = "SELECT * FROM cp_events WHERE event_date= '".$curentDate."' AND event_date<= '".$getByDate."' AND user_id='".$user."'";
	$q = $this->db->query($sql);
	// print_r($sql );

	$data =  array() ;
	foreach($q->result_array() as $row)     
	{
		$data[]  = $row;
	}
	return $data; 


	
} 


public function getcalenderDataByWeekly($user)
{
	

	$sql = "SELECT * FROM cp_reminder WHERE reminder_date >= NOW() AND reminder_date <= NOW() + INTERVAL 7 DAY  AND user_id='".$user."'";

	
	//$sql = "SELECT * FROM cp_events WHERE event_date= '".$curentDate."' AND event_date<= '".$getByDate."' AND user_id='".$user."'";
	$q = $this->db->query($sql);

	$data =  array() ;
	foreach($q->result_array() as $row)     
	{
		$data[]  = $row;
	}
	return $data; 


	
}


public function getcalenderDataMonthly($curentDate,$getByDate,$user)
{
	// $sql = "SELECT * FROM cp_events WHERE event_date > CURDATE()  AND user_id='".$user."'";
	$sql = "SELECT * FROM cp_reminder WHERE reminder_date > CURDATE()  AND user_id='".$user."'";

	$q = $this->db->query($sql);
	$data =  array() ;
	foreach($q->result_array() as $row)     
	{
		$data[]  = $row;
	}
	return $data; 


	
}


public function get_searched_dateresult($date)
{
	$user_id = $this->session->userdata('logged_in')['id'];
	// $sql = "SELECT * FROM `cp_events` WHERE `event_date` = '$date' AND `user_id`='$user_id'";	
	$sql = "SELECT * FROM `cp_reminder` WHERE `reminder_date`='$date' AND `user_id`='$user_id'";
	$q = $this->db->query($sql);
	$data =  array();	 
	foreach ($q->result_array() as $row) {
		$data[] = $row;
	}
	$ret = $data;
	return $ret;

}
public function get_yearwise_result($selecteddate)
{
	$user_id = $this->session->userdata('logged_in')['id'];
	// $sql = "SELECT`event_date`, `event_title`,`event_description`,YEAR(`event_date`) FROM `cp_events` WHERE YEAR(`event_date`)='$selecteddate' AND `user_id`='$user_id'";


	$sql = "SELECT `reminder_date`, `reminder_title`,`reminder_description`,YEAR(`reminder_date`) FROM `cp_reminder` WHERE YEAR(`reminder_date`)='$selecteddate' AND `user_id`='$user_id'";
	$q = $this->db->query($sql);
		// echo $this->db->last_query();
		// die;
	$data =  array();	 
	foreach ($q->result_array() as $row) {		
		$data[] = $row;
	}
	$ret = $data;
	return $ret;

}
/********Monthwise Record *******/
public function get_searched_monthwise_result($selecteddate)
{
	// _dx($selecteddate);
	$user_id = $this->session->userdata('logged_in')['id'];
	// $sql = "SELECT`event_date`, `event_title`,`event_description`,Month(`event_date`) FROM `cp_events` WHERE Month(`event_date`)='$selecteddate' AND `user_id`='$user_id'";
	$sql = "SELECT `reminder_date`, `reminder_title`,`reminder_description`,Month(`reminder_date`) FROM `cp_reminder` WHERE Month(`reminder_date`)='$selecteddate' AND `user_id`='$user_id'";
	$q = $this->db->query($sql);
		// echo $this->db->last_query();
		// die;
	$data =  array();	 
	foreach ($q->result_array() as $row) {		
		$data[] = $row;
	}
	$ret = $data;
	return $ret;

}
}