<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Test_type extends MX_Controller {

	private $staff_permission = array();
	private $access_permission = true;
  public function __construct(){   

    parent:: __construct();

    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->model('Common_model');
    $this->load->model('login_model');
    $this->load->helper(array('url'));
    $this->load->helper(array('form'));

    $user_id = $this->session->userdata('logged_ins')['id'];
    if(empty($user_id)) {
      redirect('admin/login');
    }

    if ($this->session->userdata('logged_ins')['user_role'] == 5) {
	  $uid = $this->session->userdata('logged_ins')['id'];
	  $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
	  if (!empty($permission_list)) 
	  {
	     foreach ($permission_list as $k => $v) 
	     {
	        $this->staff_permission[$v['permission_id']] = $v;
	     }
	  }
	}

  }
  
  	public function permission_access($permission_id=4)
    {
      if ($this->session->userdata('logged_ins')['user_role'] != 1) 
      {
         if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
         {
            $this->access_permission = false;
         }
      }
   	}
  
  public function test_type_list()
  {
  	 $data['menuactive'] = $this->uri->segment(2);
	 $data['testType_list'] = $this->Common_model->getAllorderby('cp_test_type','id','desc');
		 
	$data['staff_permission'] = $this->staff_permission; 
    
    $this->permission_access(4);
    if ($this->access_permission) {
      $this->template->set('title', 'Test type Management');
      $this->template->load('admin_dashboard_layout', 'contents' , 'test_type_list', $data);
    } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
    }
  }
  
  
  public function add_test_type()
  {
  	 $data['menuactive'] = $this->uri->segment(2);
	 $this->form_validation->set_rules('test_type', 'Test Type', 'required');
	 
	  $test_type = $this->input->post('test_type');
	 
	  if ($this->form_validation->run() == TRUE) 
		 {  
	
	         $array = array(
			    'test_type' => $test_type,				
				'create_date' => date('Y-m-d H:i:s')
			 );
			 //echo '<pre>';print_r($array );die;
			 $this->Common_model->insertData('cp_test_type',$array);
			 
			 $this->session->set_flashdata('success', 'Test type Successfully Added');
			 redirect('admin/test_type_list');
			 
		 }
	
	$data['staff_permission'] = $this->staff_permission;
    
    $this->permission_access(4);
    if ($this->access_permission) {
      $this->template->set('title', 'Test type Management');
      $this->template->load('admin_dashboard_layout', 'contents', 'add_test_type', $data);
    } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents', 'no_access', $data);
    }
	  
  }
  
  public function edit_test_type()
  {
  		 $data['menuactive'] = $this->uri->segment(2);
	   $this->form_validation->set_rules('test_type', 'Test type', 'required');
	 
	  $segment = $this->uri->segment('3');
	  
	   $test_type = $this->input->post('test_type');
	  
	  $data['singleData'] = $this->Common_model->getsingle('cp_test_type',array('id' => $segment));
	 
	  
	  if ($this->form_validation->run() == TRUE) 
		 {  
	        $array = array(
			    'test_type' => $test_type,
				'create_date' => date('Y-m-d H:i:s'),				
			 );
			// echo '<pre>';print_r($array );die; 
			 $this->Common_model->updateData('cp_test_type',$array,array('id' => $segment));

			 $this->session->set_flashdata('success', 'Successfully updated');
			 redirect('admin/test_type_list');
			 
		 }
	
	$data['staff_permission'] = $this->staff_permission;
    
    $this->permission_access(4);
    if ($this->access_permission) {
      $this->template->set('title', 'Test type Management');
      $this->template->load('admin_dashboard_layout', 'contents' , 'add_test_type', $data);
    } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents', 'no_access', $data);
    }
  }
  
  public function delete_test_type()
  {
	  $segment = $this->uri->segment('3');
	  $this->Common_model->delete('cp_test_type',array('id' => $segment));
	   $this->session->set_flashdata('success', 'Successfully deleted');
			 redirect('admin/test_type_list');
  }
  

}