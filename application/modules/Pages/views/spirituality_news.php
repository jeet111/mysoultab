<style>
    tbody tr td:last-child {
        text-align: left;
    }
    img.game.app {
    height: auto !important;
    width: auto !important;
}
    .rem_add_button {
        width: 100%;
        /* text-align: center; */
        float: left;
        /* margin: 0 auto; */
        display: block;
        text-align: center;
        margin-top: 13px;
    }


    .btn-primary:hover {
        color: #000 !important;
    }

    .serv.col-md-3 {
        text-align: center;
        box-shadow: rgb(100 100 111 / 10%) 0px 7px 29px 0px;
        padding: 14px 0;
        margin-top: 20px;
    }
    .addNote a, .btn.btn-primary {
    max-width: 184px !important;
}
    .rem_add_button a {
        /* float: left; */
        /* padding: 8px 20px; */
        /* box-sizing: border-box; */
        /* border-radius: 34px; */
        /* text-align: center; */
        font-size: 19px;
        margin-right: 10px;
        margin-top: 20px;
    }

    .serv.col-md-3 .btn-primary {
        color: #000;
        background-color: transparent !important;
        /* border-color: #2e6da4; */
    }

    table#sampleTable {
        table-layout: fixed;
        text-align: left;
    }

    td.lcol a i {
        font-style: normal;
    }

    td.lcol a {
        margin-right: 5px;
    }

    .tab_medi td a.edit_btn {
        background: #1f569e;
        color: #fff;
        padding: 10px 17px;
        max-width: 139px;
        width: 100%;
        border-radius: 41px;
    }

    .serv.col-md-3 h4 {
        font-size: 22px;
        color: #000;
    }

    .rem_add {
        margin-bottom: 14px;
        margin-top: 8px;
    }

</style>
<div class="text_heading">
    <div id="msg_show"></div>

    <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success message" id="success">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php } ?>
    <h3 class="text-center">
        <i class="fa fa-plus-square-o" aria-hidden="true"></i> Spirituality News
    </h3>

    <div class="rem_add">
        <a href="#addSpiritualityNewsModal" class="btn button btn-primary" data-toggle="modal">Add Spirituality News</i> </a>
    </div>
</div>
<div class="row websiteAppClass">

<?php
if (!empty($yoganews_list)) {

    foreach ($yoganews_list as $value) {
        //    echo "<pre>";
        //    print_r($value);

?>
        <div class="serv col-md-3 bg-info">
            <h4>
                <?php echo $value->button_name; ?>

            </h4>

            <?php
            if ($value->social_type == "Website") {
            ?>

                <img class="game website" src="https://www.google.com/s2/favicons?domain=<?php echo $value->url; ?>" data-url="<?php echo $value->url; ?>">
            <?php } else {
                $app_list_data = $this->common_model_new->getAllwhere("app_list", array('id' => $value->app_name));

            ?>
                <img class="game app" src="<?php if(isset($app_list_data[0]->icon)){ echo $app_list_data[0]->icon; } ?>">


            <?php } ?>
            <div class="rem_add_button">
                <a href="#editSpriNewsModal" class="model-content edit edit_btn btn-primary" data-toggle="modal">
                    <i class="material-icons update" data-toggle="tooltip" data-socialid="<?php echo $value->id; ?>" data-socialid="<?php echo $value->social_type; ?>" data-url="<?php echo $value->url; ?>" data-app_name="<?php echo $value->app_name; ?>" data-button_name="<?php echo $value->button_name; ?>" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></i>
                </a>
                <a href="#deleteSpirNewsModal" class="model-content delete" data-id="<?php echo $value->id; ?>
                                            " data-toggle="modal"><i class="fa fa-trash" aria-hidden="true"></i></a>

            </div>
        </div>

    <?php }
} else { ?>
        <div class="nodataContainer"><h4 class="nodataFound">No Data Found!</h4></div>
<?php } ?>
</div>
<!-- Add Social News Modal -->
<div id="addSpiritualityNewsModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="spiritualityForm">
                <div class="modal-header">
                    <h4 class="modal-title">Add Spirituality News</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">




                    <div class="row">


                        <div class="col-lg-4">
                            <label for="name"><b>Spirituality News Type:</b></label>
                        </div>
                        <div class="col-lg-8">

                            <label class="checkbox-inline">
                                <input type="radio" class="spirinews_type" name="spirinews_type" data="_spiriNews" value="Website" checked>Website
                            </label>
                            <label class="checkbox-inline">
                                <input type="radio" class="spirinews_type" value="App" data="_spiriNews" name="spirinews_type">App
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 ">
                            <label for="name"><b>Button Name:</b></label>
                        </div>

                        <div class="col-lg-8">
                            <input type="text" name="button_name" id="add_spiri_news">
                            <span style="color:red" id="add_spiri_news_error"></span>
                        </div>
                    </div>


                    <div class="row" id="Website_u_spiriNews">
                        <div class="col-lg-4">
                            <label for="spirituality_news"><b>Website URL:</b></label>
                        </div>

                        <div class="col-lg-8">
                            <input type="text" name="url" id="url_add_spirinews">
                            <span style="color:red" id="url_add_spirinews_error"></span>
                        </div>
                    </div>
                    <div class="row" id="App_u_spiriNews" style="display:none">
                        <div class="col-lg-4 ">
                            <label for="spirituality_news"><b>App Name:</b></label>
                        </div>

                        <div class="col-lg-8 padding">
                            <select id="appname_add_spirinews" name="app_name" class="form-select">
                                <option>Select</option>
                                <?php foreach ($app_list as $key => $app) { ?>


                                    <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                <?php } ?>

                            </select>
                            <span style="color:red" id="appname_add_spirinews_error"></span>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <input type="hidden" value="1" name="type">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                    <button type="button" class="btn btn-success" id="btn-add-spiriNews">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Modal HTML -->
<div id="editSpriNewsModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="update_form_spiriNews">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Spirituality News</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id_u_spirinews" name="id" class="form-control" required>
                    <div class="row">


                        <div class="col-lg-4">
                            <label for="name"><b>Spirituality News Type:</b></label>
                        </div>
                        <div class="col-lg-8 padding" id="yoganews_u">

                            <label class="checkbox-inline">
                                <input type="radio" class="spirinews_type" name="spirinews_type" data="_u_spiriNews" value="Website" checked>Website
                            </label>
                            <label class="checkbox-inline">
                                <input type="radio" class="spirinews_type" value="App" data="_u_spiriNews" name="spirinews_type">App
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <label for="name"><b>Button Name:</b></label>
                        </div>

                        <div class="col-lg-8">
                            <input type="text" name="button_name" id="button_name_u_spirinews" class="editgamemyModal_button_name">
                            <span style="color:red" id="button_name_u_spirinews_error"></span>
                        </div>
                    </div>


                    <div class="row" id="Website_u_spiriNews">
                        <div class="col-lg-4">
                            <label for="spirituality_news"><b>Website URL:</b></label>
                        </div>

                        <div class="col-lg-8">
                            <input type="text" name="url" id="url_u_spirinews" class="editgamemyModal_url">
                            <span style="color:red" id="url_u_spirinews_error"></span>
                        </div>
                    </div>
                    <div class="row" id="App_u_spiriNews" style="display:none">
                        <div class="col-lg-4">
                            <label for="spirituality_news"><b>App Name:</b></label>
                        </div>

                        <div class="col-lg-8 padding">
                            <select name="app_name" id="app_name_u_spirinews" class="editgamemyModal_app_name form-select">
                                <option>Select</option>
                                <?php foreach ($app_list as $key => $app) { ?>


                                    <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                <?php } ?>

                            </select>
                            <span style="color:red" id="app_name_u_spirinews_error"></span>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <input type="hidden" value="2" name="type">
                    <input type="hidden"  data-socialid="<?php echo $value->id; ?>" name="hiddenID" id="hiddenID">

                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                    <button type="button" class="btn btn-info" id="update_spiriNews">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="deleteSpirNewsModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form>

                <div class="modal-header">
                    <h4 class="modal-title">Delete Spirituality News </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id_d_spiriNews" name="id" class="form-control">
                    <p>Are you sure you want to delete these Records?</p>
                    <p class="text-warning"><small>This action cannot be undone.</small></p>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                    <button type="button" class="btn btn-danger" id="delete_spiriNews">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script type="text/javascript">
      $(".spirinews_type").click(function() {
            var value_game = $(this).val();
            // alert(value_game);
            var data_up = $(this).attr("data");
            if (value_game == 'App') {
                $("#Website_u_spiriNews").hide();
            $("#App_u_spiriNews").show();
            } else {
                $("#Website_u_spiriNews").show();
                $("#App_u_spiriNews").hide();
            }
        });
    $(document).on('click', '#btn-add-spiriNews', function(e) {
        var add_spiri_news = $("#add_spiri_news").val();
        var url_add_spirinews = $("#url_add_spirinews").val();
        var appname_add_spirinews = $("#appname_add_spirinews").val();
        var spirinews_type = $('input[name="spirinews_type"]:checked').val();
        var validate_flag_spiriNews = 0;

        if (add_spiri_news == '') {
            validate_flag_spiriNews = 1;
            $("#add_spiri_news_error").text("Button Name is Required");
        } else {
            validate_flag_spiriNews = 0;
            $("#add_spiri_news_error").text("");
        }

        if (spirinews_type == 'App') {
            if (appname_add_spirinews == '') {
                validate_flag_spiriNews = 1;
                $("#appname_add_spirinews_error").text("App Name is required");
            } else {
                validate_flag_spiriNews = 0;
                $("#appname_add_spirinews_error").text("");
            }
        } else {
            if (url_add_spirinews == '') {
                validate_flag_spiriNews = 1;
                $("#url_add_spirinews_error").text("Url is required");
            } else {
                if (validURL(url_add_spirinews)) {
                    validate_flag_spiriNews = 0;
                    $("#url_add_spirinews_error").text("");
                } else {
                    validate_flag_spiriNews = 1;
                    $("#url_add_spirinews_error").text("Url is not vaild");
                }
            }
        }
        var data = $("#spiritualityForm").serialize();
        if (validate_flag_spiriNews != 1) {
            $.ajax({
                data: data,
                type: "post",
                url: "<?php echo base_url(); ?>Pages/SpritualityNews/add_spiri_news",
                success: function(dataResult) {
                    var dataResult = JSON.parse(dataResult);
                    console.log(dataResult.msg);
                    $('#addSpiritualityNewsModal').modal('hide');
                    location.reload();

                }
            });
        }

    });
    $(document).on('click', '.update', function(e) {
        var spiriId = $(this).attr("data-socialid");
        var newstype = $(this).attr("data-spirinews_type");
        var url = $(this).attr("data-url");
        var app_name = $(this).attr("data-app_name");
        var button_name = $(this).attr("data-button_name");
        $('#id_u_spirinews').val(spiriId);
        $('#yoganews_u').find(':radio[name=spirinews_type][value="' + newstype + '"]').prop('checked', true);
        $('#url_u_spirinews').val(url);
        $('#app_name_u_spirinews').val(app_name);
        $('#button_name_u_spirinews').val(button_name);
        if (newstype == 'App') {
            $("#Website_u_spiriNews").hide();
            $("#App_u_spiriNews").show();
        } else {
            $("#Website_u_spiriNews").show();
            $("#App_u_spiriNews").hide();
        }
    });

    $(document).on('click', '#update_spiriNews', function(e) {
        var button_name_u_spirinews = $("#button_name_u_spirinews").val();
        var id = $("#hiddenID").attr("data-socialid");

        var url_u_spirinews = $("#url_u_spirinews").val();
        var app_name_u_spirinews = $("#app_name_u_spirinews").val();
        var spirinews_type = $('input[name="spirinews_type"]:checked').val();
        var validate_flag_spiriNews_u = 0;

        if (button_name_u_spirinews == '') {
            validate_flag_spiriNews_u = 1;
            $("#button_name_u_spirinews_error").text("Button Name is Required");
        } else {
            validate_flag_spiriNews_u = 0;
            $("#button_name_u_spirinews_error").text("");
        }

        if (spirinews_type == 'App') {
            if (app_name_u_spirinews == '') {
                validate_flag_spiriNews_u = 1;
                $("#app_name_u_spirinews_error").text("App Name is required");
            } else {
                validate_flag_spiriNews_u = 0;
                $("#app_name_u_spirinews_error").text("");
            }
        } else {
            if (url_u_spirinews == '') {
                validate_flag_spiriNews_u = 1;
                $("#url_u_spirinews_error").text("Url is required");
            } else {
                if (validURL(url_u_spirinews)) {
                    validate_flag_spiriNews_u = 0;
                    $("#url_u_spirinews_error").text("");
                } else {
                    validate_flag_spiriNews_u = 1;
                    $("#url_u_spirinews_error").text("Url is not vaild");
                }
            }
        }
        var data = $("#update_form_spiriNews").serialize();
        console.log(data);
        if (validate_flag_spiriNews_u != 1) {
            $.ajax({
                data: data,
                type: "post",
                url: "<?php echo base_url(); ?>Pages/SpritualityNews/edit_spirinews",
                success: function(dataResult) {
                    var dataResult = JSON.parse(dataResult);
                    console.log(dataResult.msg);
                    $('#editSpriNewsModal').modal('hide');
                    location.reload();
                }
            });
        }

    });
    $(document).on("click", ".delete", function() {
        var id = $(this).attr("data-id");
        $('#id_d_spiriNews').val(id);

    });
    $(document).on("click", "#deleteSpirNewsModal", function() {
        $.ajax({
            url: "<?php echo base_url(); ?>Pages/SpritualityNews/delete_yoganews",
            type: "POST",
            cache: false,
            data: {
                type: 3,
                id: $("#id_d_spiriNews").val()
            },
            success: function(dataResult) {
                location.reload();
                $('#deleteSpirNewsModal').modal('hide');
            }
        });
    });
</script>