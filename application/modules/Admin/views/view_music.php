

<div class="container-fluid">
<div class="dash-counter users-main my-sts-table user-list">
<div class="">
<div class="col-md-12">
<div class="">
<div class="card-body">
<div class="my_info_one">
<div class="Schedule_main_one">
	<div class=" users-main">
		<h2>View Music</h2>
		
		<div class="btn_topBack">
			<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">
				Back
			</a>
		</div>
		

										<div class="row">
									<?php //echo "<pre>"; print_r($singledata); echo "</pre>"; ?>
									<div class="col-md-12">
										<div class="general-info">
											<div class="card-header">
												<h5 class="card-header-text">About Music</h5>
											</div>
											<div class="rrow">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-12 col-sm-12">
															<div class="row">
																<div class="col-md-4 col-sm-4">
																	<div class="f_name"><b>Title:</b></div>
																</div>
																<div class="col-md-8 col-sm-8">
																	<div class="f_nameOne">
																		<?php echo $viewMusic->music_title;?></div>
																</div>
															</div>
														</div>
														<div class="col-md-12 col-sm-12">
															<div class="row">
																<div class="col-md-4 col-sm-4">
																	<div class="f_name"><b>Description:</b></div>
																</div>
																<div class="col-md-8 col-sm-8">
																	<div class="f_nameOne"><?php echo $viewMusic->music_desc;?>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
													<?php 
	$category_name = $this->Common_model->getsingle("cp_music_category",array("music_category_id" => $viewMusic->category_id)); 
	?> 
														
														<div class="col-md-12 col-sm-12">
															<div class="row">
																<div class="col-md-4 col-sm-4">
																	<div class="f_name"><b>Category:</b></div>
																</div>
																<div class="col-md-8 col-sm-8">
																	<div class="f_nameOne">
																		<?php echo $category_name->music_category_name;?>
																	</div>
																</div>
															</div>
														</div>
													
													
														<div class="col-md-12 col-sm-12">
															<div class="row">
																<div class="col-md-4 col-sm-4">
																	<div class="f_name"><b>File:</b></div>
																</div>
																<div class="col-md-8 col-sm-8">
																	<div class="f_nameOne">
																		<audio controls="controls" autoplay="true"> <source src= "<?php echo base_url();?>uploads/music/<?php echo $viewMusic->music_file;?>" type="audio/mpeg" > </source> </audio>
																	</div>
																</div>
															</div>
														</div>


														
													</div>
													<div class="row">
														
														<div class="col-md-12 col-sm-12">
															<div class="row">
																<div class="col-md-4 col-sm-4">
																	<div class="f_name"><b>Image:</b></div>
																</div>
																<div class="col-md-8 col-sm-8">
																	<div class="f_nameOne">
																		<img  src="<?php echo base_url();?>uploads/music/image/<?php echo $viewMusic->music_image;?>">
																	</div>
																</div>
															</div>
														</div>
														
														
														
													</div>
												</div>
											</div>
										</div>
										<?php $admin_url = $this->uri->segment(2);
									$url = 'admin/email_list/list';
								if ($admin_url == 'adminprofile') {
									$url = 'admin/dashboard';
									}?>
										
									</div>
								</div>




		<!-- <div class="row">
			<div class="col-md-9">
				<div class="general-info">
					<div class="card-header">
						<h5 class="card-header-text">About Music</h5>
					</div>
					<div class="rrow">
						<div class="col-md-6">
							
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="f_name">Title:</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="f_nameOne"><?php echo $viewMusic->music_title;?></div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="f_name"> Description:</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="f_nameOne"><?php echo $viewMusic->music_desc;?></div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="f_name"> Image:</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="f_nameOne">

										<img height="50px" width="50px" src="<?php echo base_url();?>uploads/music/image/<?php echo $viewMusic->music_image;?>">
										</div>
								</div>
							</div>
	<?php 
	$category_name = $this->Common_model->getsingle("cp_music_category",array("music_category_id" => $viewMusic->category_id)); 
	?> 						
							
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="f_name">Category:</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="f_nameOne">

										<?php echo $category_name->music_category_name;?>
										</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="f_name"> File:</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="f_nameOne">
 <audio controls="controls" autoplay="true"> <source src= "<?php echo base_url();?>uploads/music/<?php echo $viewMusic->music_file;?>" type="audio/mpeg" > </source> </audio>
										</div>
								</div>
							</div>
							
							
						</div>
					</div>
				</div>
				<?php $admin_url = $this->uri->segment(2);
				$url = 'admin/email_list/list';
				if ($admin_url == 'adminprofile') {
					$url = 'admin/dashboard';
				}?>
				
			</div>
		</div> -->
	</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
