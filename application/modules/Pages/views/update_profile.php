<style>
.text_heading {
    border: 0;
    margin-bottom: 14px;
}
.active {
    background-color: transparent;
    color: #878787;
}
    #flupmsg:{



       color:red;

   }

</style>

<div class=" text_heading">


        <h3><i class="fa fa-plus-square-o" aria-hidden="true"></i> Profile</h3>
         <div class="back_reminder"><a href="<?php echo base_url(); ?>dashboard_new" class="btn btn-primary">Back</a></div>

    </div>    



    <section class="content photo-list">

        <div class="photo-listMain"> 



            <?php echo validation_errors(); ?>

            <?php if ($this->session->flashdata('success')) { ?>

                <div class="alert alert-success message">

                    <button type="button" class="close" data-dismiss="alert">x</button>

                    <?php echo $this->session->flashdata('success'); ?></div>

                <?php } ?>

                <?php if ($this->session->flashdata('danger')) { ?>

                    <div class="alert alert-danger message">

                        <button type="button" class="close" data-dismiss="alert">x</button>

                        <?php echo $this->session->flashdata('danger'); ?></div>

                    <?php } ?>

                    <div class="tabbable-panel">

                        <div class="tabbable-line">

                            <ul class="nav nav-tabs cus_tab">

                                <li class="active">

                                    <a href="#tab_default_1" data-toggle="tab">

                                    Update Profile </a>

                                </li>

                                <li>

                                    <a href="#tab_default_2" data-toggle="tab">

                                    Change password </a>

                                </li> 


                                <li>

                                    <a href="#tab_default_3" data-toggle="tab">

                                    Transactions </a>

                                </li>               

                            </ul>

                            <div class="tab-content">

                                <div class="tab-pane active" id="tab_default_1">                    

                                    <form id="update_pro" enctype="multipart/form-data" method="post" class="frm_add">

                                        <div class="row">

                                            <div class="col-md-6">

                                                <div class="form-group">

                                                    <label for="">Name<font style="color: red;">*</font></label>

                                                    <input type="text" class="form-control required" id="name_user" name="name_user" placeholder="Name" value="<?php echo $singleData->name; ?>">

                                                </div>

                                            </div>

                                            <div class="col-md-6">

                                                <div class="form-group">

                                                    <label for="">Username<font style="color: red;">*</font></label>

                                                    <input type="text" class="form-control" id="username" name="username" readonly placeholder="Username" value="<?php echo $singleData->username; ?>">

                                                </div>

                                            </div>                                    

                                        </div>                                

                                        <div class="row">

                                            <div class="col-md-6">

                                                <div class="form-group">

                                                    <label for="">Mobile Number<font style="color: red;">*</font></label>

                                                    <input type="text" class="form-control required" id="mobile" name="mobile" placeholder="Mobile No" value="<?php echo $singleData->mobile; ?>">

                                                </div>

                                                <div class="form-group">

                                                    <label for="">Email<font style="color: red;">*</font></label>

                                                    <input type="text" class="form-control required" id="email" name="email" readonly placeholder="Email" value="<?php echo $singleData->email; ?>">

                                                </div>

                                            </div>

                                            <div class="col-md-6">

                                                <div class="form-group">

                                                    <label for="">Profile Image</label>

                                                    <input type="file" class="form-control" id="profile_image" name="profile_image" onchange="loadFile(event)" placeholder="Profile Image" value="">

                                                    <div id="flupmsg" style="color:red;"></div>

                                                    <div class="profile-up">

                                                        <?php if($singleData->profile_image==''){ ?>

                                                            <img id='output' src="<?php echo base_url(); ?>assets/user_dashboard/img/avatar3.png">

                                                        <?php }else{ ?>

                                                            <img  id='output' src="<?php echo base_url().'uploads/profile_images/'.$singleData->profile_image; ?>">

                                                        <?php } ?>  

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                        <div class="box-footer">

                                            <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-primary" >
                                             <input type="reset" name="cancel" class="btn btn-primary" value="Cancel" onclick="window.location='<?php echo base_url(); ?>user/update_profile';">

                                        </div>

                                    </form>

                                </div>

                                <div class="tab-pane" id="tab_default_2">                       

                                    <form id="change_pass" enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>change_password" class="frm_add">

                                        <div class="row">

                                            <div class="col-md-6">

                                                <div class="form-group">

                                                    <label for="">Old Password<font style="color: red;">*</font></label>

                                                    <input type="password" class="form-control required" id="old_password" name="old_password" placeholder="Old Password" value="">

                                                </div>

                                            </div>

                                            <div class="col-md-6">

                                                <div class="form-group">

                                                    <label for="">New Password<font style="color: red;">*</font></label>

                                                    <input type="password" class="form-control" id="new_password" name="new_password"  placeholder="New Password" value="">

                                                </div>

                                            </div>                                    

                                        </div>                                

                                        <div class="row">

                                            <div class="col-md-6">

                                                <div class="form-group">

                                                    <label for="">Confirm Password<font style="color: red;">*</font></label>

                                                    <input type="password" class="form-control required" id="confirm_password" name="confirm_password" placeholder="Confirm Password" value="">

                                                </div>                                        

                                            </div>

                                        </div>

                                        <div class="box-footer">

                                            <input type="submit" name="change_password" id="submit" value="Submit" class="btn btn-primary">                                    

                                        </div>

                                    </form>      

                                </div>

                                <div class="tab-pane" id="tab_default_3">                       

                                    <div class="table-responsive tb_swt">





                                        <table class="table table-hover table-bordered table-mailbox send-user-mail" id="sampleTable">

                                            <thead>

                                                <tr>

                                                

                                                   <th>Transaction Id</th>

                                                   <th>Plan</th>

                                                   <th>Transaction date</th>

                                                   <th>Amount</th>

                                                   <th>Status</th>

                                               </tr>

                                           </thead>

                                           <tbody>

                                            <?php

                                            $i = 1;

            //echo "<pre>"; print_r($user_data);

                                            if(!empty($paymentDetails)){

                                                foreach ($paymentDetails as $list) {?>

                                                    <tr>

                                                        <!-- <td class="small-col" ></td> -->

                                                        <td><?php echo  $list->txn_id; ?></td>

                                                        <td>

                                                            <?php foreach ($plans as $plan) { ?>





                                                                <?php if($plan->id==$list->plan_id){ echo $plan->plan_name; }?>

                                                            <?php   } ?>

                                                        </td>

                                                        <td><?php echo  $list->txn_date; ?></td>

                                                        <td><?php echo  $list->amount; ?></td>

                                                        <td class="act">

                                                            <?php echo  $list->payment_status ; ?>

                                                        </td>

                                                    </tr>

                                                <?php }} ?>

                                            </tbody>

                                        </table>

                                    </div>      

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </section>





        </aside>

        <script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>

        <script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

        <script>





        //$('.profile-up').hide();



        function loadFile (event) {

            var pcFile = $('#profile_image').val().split('\\').pop();

            var pcExt     = pcFile.split('.').pop();

            var output = document.getElementById('output');

            if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"

                || pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){

                $("#submit").prop('disabled', false);

            $('#flupmsg').html('');



            output.src = URL.createObjectURL(event.target.files[0]);

            $('.profile-up').show();

            $('#output').show();

            $('#flupmsg').html('');

        }else{

            $('#flupmsg').html('Please select only Image file');

            $('#output').hide();

            $("#submit").prop('disabled', true);

            $('.profile-up').hide();

        }

    };

    $(document).ready(function() {



        $.validator.addMethod("lettersonly", function(value, element) {

            return this.optional(element) || /^[a-z\s]+$/i.test(value);

        }, "Only alphabetical characters");



        $("#update_pro").validate({

            rules: {

                name_user:{

                    required:true,

                    lettersonly:true,

                },

                mobile:{

                    required:true,

                    number:true,

                    

                    maxlength:10,

                    

                },

            },



            messages: {

                name_user:

                {

                    required:"Please enter name.",

                },

                mobile: {

                    required: "Please enter your Mobile Number ",

                    number: "Please enter only numeric value",

                    

                    maxlength: "Please enter no more than 10 characters.",

                }

            },

        });

    });



    $(document).ready(function(){



     $.validator.addMethod("pwcheck", function (value) { return /[\@\#\$\%\^\&\*\(\)\_\+\!]/.test(value) && /[a-z]/.test(value) && /[0-9]/.test(value) && /[A-Z]/.test(value) });



     $("#change_pass").validate({

        rules: {



            new_password: {

                required: true,

                pwcheck:true,

                minlength: 8,

                maxlength: 30,

            },

            confirm_password:{

                required: true,

                equalTo : "#new_password",

                minlength: 6

            },

            

            old_password: {

                required: true,

                minlength: 6



            },



        },

        messages:{

            new_password:{

                required: "Please enter new password",

                pwcheck:"Must contain at least one number and one uppercase and lowercase and  punctuation mark letter, and at least 8 or more characters.",

                minlength: "Password must be at least 8 characters.",

                maxlength: "Password at maximum 30 characters.",

            },

            old_password:{

                required: "Please enter confirm old password",

            },

            confirm_password:{

                required: "Please enter confirm password",

                equalTo : "Password and Confirm Password should be same",

            }

        }

    });

 });

</script>



<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

