<div class="container-fluid">
	<div class="dash-counter users-main my-sts-table">
		<div class="row">
			<div class="col-md-12">
				<div class="">
					<?php //echo "<pre>"; print_r($bankData); echo "</pre>";?>
					<div class="card-body">
						<div class="status-cng"></div>
						<?php if ($this->session->flashdata('success')) { ?>
                    <div class="alert alert-success message">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <?php echo $this->session->flashdata('success'); ?></div>
                <?php } ?>
					<table class="table table-hover tab_comn" id="sampleTable">
						<thead>
							<tr>
								<th>S no.</th>
								<th>Username</th>
								<th>Bank Name</th>
								<!-- <th>Banks Address</th>							 -->
								<th>Bank Image</th>							
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i = 1;
						foreach ($bankData as $bank) {
								?>
							<tr>
								<td><?php echo $i++; ?></td>
								<td><?php echo $bank->username; ?></td>
								<td><?php echo $bank->bank_name; ?></td>
								<!-- <td><?php echo $bank->bank_address; ?></td> -->
								<td>
									<?php if(!empty($bank->bank_image)){ ?>
									<img src="<?php echo $bank->bank_image; ?>" class="" alt="">
								<?php }else{ ?>
									<img src="<?php echo base_url();?>assets/images/bank_img.jpg" class="" alt="">
								<?php } ?>
								</td>
								<td>
									<div class="link-del-view">
										<div class="tooltip-2">
											<a href="<?php echo base_url(); ?>admin/view_bank/<?php echo $bank->bank_id; ?>">
												<i class="fa fa-eye" aria-hidden="true"></i></a>
											<span class="tooltiptext">View</span>
										</div>

										<div class="tooltip-2">
											<a href="<?php echo base_url(); ?>admin/delete_bank/<?php echo $bank->bank_id; ?>" onclick="return confirm('Delete this bank ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

											
										</div>
										
										
									</div>
									
							</div>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>
</div>
</div>