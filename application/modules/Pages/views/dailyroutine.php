    <style>
    .text_heading {
    text-align: center;
    border: 0;
}
</style>
    <?php

    if (!empty($check_dailyroutine)) {
        $morning = explode(",", $check_dailyroutine->morning);
        $noon  = explode(",", $check_dailyroutine->noon);
        $evening = explode(",", $check_dailyroutine->evening);
        $notes = explode(",", $check_dailyroutine->notes);
        $dinner = explode(",", $check_dailyroutine->dinner);
        $bedtime = explode(",", $check_dailyroutine->bedtime);
    } else {
        $morning = [];
        $noon  = [];
        $evening = [];
        $notes = [];
        $dinner = [];
        $bedtime = [];
    }

    ?>



    <div class=" text_heading">
        
        <h3><i class='fas fa-atom' aria-hidden="true"></i> Dailyroutine</h3>

		<div id="msg_show"></div>
		<div id="failed_msg"></div>
        <?php if ($this->session->flashdata('success')) { ?>
		<div class="alert alert-success message" id="successmsg">
			<button type="button" class="close" data-dismiss="alert" >x</button>
			<?php echo $this->session->flashdata('success'); ?>
		</div>
		<?php } ?>

		<?php if ($this->session->flashdata('failed')) { ?>
		<div class="alert alert-danger message" id="failedmsg">
			<button type="button" class="close" data-dismiss="alert" >x</button>
			<?php echo $this->session->flashdata('failed'); ?>
		</div>
		<?php } ?>

        <div class="tooltip-2">
            <h2>Display Dailyroutine button on Mobile App
			<?php
                if (!empty($btnsetingArray) && $btnsetingArray != '') {
					foreach ($btnsetingArray as $res) {
						if ($res->settings == 1) { ?>
							<label class="switch ">
								<input type="checkbox" checked data-btn="<?php echo 'DailyRoutine' ?>" class="updateStatus">
								<span class="slider round"></span>
							</label>

						<?php } else { ?>
							<label class="switch space">
								<input type="checkbox" data-btn="<?php echo 'DailyRoutine' ?>" class="updateStatus">
								<span class="slider round"></span>
							</label>

						<?php } ?>
				<?php } ?>
            <?php } else { ?>
					<label class="switch space">
						<input type="checkbox" data-btn="<?php echo 'DailyRoutine' ?>" class="updateStatus">
						<span class="slider round"></span>
					</label>
            <?php } ?>

            </h2>
        </div>

    </div>
    <form enctype="multipart/form-data" method="post" id="dailyform">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th><strong>Time</strong></th>
						<th><strong>Meal</strong></th>
						<th><strong>Snack</strong></th>
						<th><strong>Medicine</strong></th>
						<th><strong>Walk</strong></th>
						<th><strong>Yoga</strong></th>
						<th><strong>Meditation</strong></th>
						<th><strong>Nap</strong></th>
					</tr>
                </thead>
                <tbody>
                    <tr>
                        <td data-label="Time">morning</td>
                        <td data-label="Meal"><input name="morning[]" type="checkbox" value="Meal" <?php if (in_array("Meal", $morning)) {
                                                                                        echo "checked";
                                                                                    } ?>></td>
                        <td data-label="Snack"><input name="morning[]" type="checkbox" value="Snack" <?php if (in_array("Snack", $morning)) {
                                                                                        echo "checked";
                                                                                    } ?>></td>
                        <td data-label="Medicine"><input name="morning[]" type="checkbox" value="Medicine" <?php if (in_array("Medicine", $morning)) {
                                                                                            echo "checked";
                                                                                        } ?>></td>
                        <td data-label="Walk"><input name="morning[]" type="checkbox" value="Walk" <?php if (in_array("Walk", $morning)) {
                                                                                        echo "checked";
                                                                                    } ?>></td>
                        <td data-label="Yoga"><input name="morning[]" type="checkbox" value="Yoga" <?php if (in_array("Yoga", $morning)) {
                                                                                        echo "checked";
                                                                                    } ?>></td>
                        <td data-label="Meditation"><input name="morning[]" type="checkbox" value="Meditation" <?php if (in_array("Meditation", $morning)) {
                                                                                            echo "checked";
                                                                                        } ?>></td>
                        <td data-label="Nap"><input name="morning[]" type="checkbox" value="Nap" <?php if (in_array("Nap", $morning)) {
                                                                                    echo "checked";
                                                                                } ?>></td>
                    </tr>
                    <tr>
                        <td data-label="Time">noon</td>
                        <td data-label="Meal"><input name="noon[]" type="checkbox" value="Meal" <?php if (in_array("Meal", $noon)) {
                                                                                    echo "checked";
                                                                                } ?>></td>
                        <td data-label="Snack"><input name="noon[]" type="checkbox" value="Snack" <?php if (in_array("Snack", $noon)) {
                                                                                    echo "checked";
                                                                                } ?>></td>
                        <td data-label="Medicine"><input name="noon[]" type="checkbox" value="Medicine" <?php if (in_array("Medicine", $noon)) {
                                                                                        echo "checked";
                                                                                    } ?>></td>
                        <td data-label="Walk"><input name="noon[]" type="checkbox" value="Walk" <?php if (in_array("Walk", $noon)) {
                                                                                    echo "checked";
                                                                                } ?>></td>
                        <td data-label="Yoga"><input name="noon[]" type="checkbox" value="Yoga" <?php if (in_array("Yoga", $noon)) {
                                                                                    echo "checked";
                                                                                } ?>></td>
                        <td data-label="Meditation"><input name="noon[]" type="checkbox" value="Meditation" <?php if (in_array("Meditation", $noon)) {
                                                                                        echo "checked";
                                                                                    } ?>></td>
                        <td data-label="Nap"><input name="noon[]" type="checkbox" value="Nap" <?php if (in_array("Nap", $noon)) {
                                                                                    echo "checked";
                                                                                } ?>></td>
                    </tr>
                    <tr>
                        <td data-label="Time">evening</td>
                        <td data-label="Meal"><input name="evening[]" type="checkbox" value="Meal" <?php if (in_array("Meal", $evening)) {
                                                                                        echo "checked";
                                                                                    } ?>></td>
                        <td data-label="Snack"><input name="evening[]" type="checkbox" value="Snack" <?php if (in_array("Snack", $evening)) {
                                                                                        echo "checked";
                                                                                    } ?>></td>
                        <td data-label="Medicine"><input name="evening[]" type="checkbox" value="Medicine" <?php if (in_array("Medicine", $evening)) {
                                                                                            echo "checked";
                                                                                        } ?>></td>
                        <td data-label="Walk"><input name="evening[]" type="checkbox" value="Walk" <?php if (in_array("Walk", $evening)) {
                                                                                        echo "checked";
                                                                                    } ?>></td>
                        <td data-label="Yoga"><input name="evening[]" type="checkbox" value="Yoga" <?php if (in_array("Yoga", $evening)) {
                                                                                        echo "checked";
                                                                                    } ?>></td>
                        <td data-label="Meditation"><input name="evening[]" type="checkbox" value="Meditation" <?php if (in_array("Meditation", $evening)) {
                                                                                            echo "checked";
                                                                                        } ?>></td>
                        <td data-label="Nap"><input name="evening[]" type="checkbox" value="Nap" <?php if (in_array("Nap", $evening)) {
                                                                                    echo "checked";
                                                                                } ?>></td>
                    </tr>
                    <tr>
                        <td data-label="Time">dinner</td>
                        <td data-label="Meal"><input name="dinner[]" type="checkbox" value="Meal" <?php if (in_array("Meal", $dinner)) {
                                                                                    echo "checked";
                                                                                } ?>></td>
                        <td data-label="Snack"><input name="dinner[]" type="checkbox" value="Snack" <?php if (in_array("Snack", $dinner)) {
                                                                                        echo "checked";
                                                                                    } ?>></td>
                        <td data-label="Medicine"><input name="dinner[]" type="checkbox" value="Medicine" <?php if (in_array("Medicine", $dinner)) {
                                                                                        echo "checked";
                                                                                    } ?>></td>
                        <td data-label="Walk"><input name="dinner[]" type="checkbox" value="Walk" <?php if (in_array("Walk", $dinner)) {
                                                                                    echo "checked";
                                                                                } ?>></td>
                        <td data-label="Yoga"><input name="dinner[]" type="checkbox" value="Yoga" <?php if (in_array("Yoga", $dinner)) {
                                                                                    echo "checked";
                                                                                } ?>></td>
                        <td data-label="Meditation"><input name="dinner[]" type="checkbox" value="Meditation" <?php if (in_array("Meditation", $dinner)) {
                                                                                            echo "checked";
                                                                                        } ?>></td>
                        <td data-label="Nap"><input name="dinner[]" type="checkbox" value="Nap" <?php if (in_array("Nap", $dinner)) {
                                                                                    echo "checked";
                                                                                } ?>></td>
                    </tr>
                    <tr>
                        <td data-label="Time">bedtime</td>
                        <td data-label="Meal"><input name="bedtime[]" type="checkbox" value="Meal" <?php if (in_array("Meal", $bedtime)) {
                                                                                        echo "checked";
                                                                                    } ?>></td>
                        <td data-label="Snack"><input name="bedtime[]" type="checkbox" value="Snack" <?php if (in_array("Snack", $bedtime)) {
                                                                                        echo "checked";
                                                                                    } ?>></td>
                        <td data-label="Medicine"><input name="bedtime[]" type="checkbox" value="Medicine" <?php if (in_array("Medicine", $bedtime)) {
                                                                                            echo "checked";
                                                                                        } ?>></td>
                        <td data-label="Walk"><input name="bedtime[]" type="checkbox" value="Walk" <?php if (in_array("Walk", $bedtime)) {
                                                                                        echo "checked";
                                                                                    } ?>></td>
                        <td data-label="Yoga"><input name="bedtime[]" type="checkbox" value="Yoga" <?php if (in_array("Yoga", $bedtime)) {
                                                                                        echo "checked";
                                                                                    } ?>></td>
                        <td data-label="Meditation"><input name="bedtime[]" type="checkbox" value="Meditation" <?php if (in_array("Meditation", $bedtime)) {
                                                                                            echo "checked";
                                                                                        } ?>></td>
                        <td data-label="Nap"><input name="bedtime[]" type="checkbox" value="Nap" <?php if (in_array("Nap", $bedtime)) {
                                                                                    echo "checked";
                                                                                } ?>></td>
                    </tr>


                </tbody>
            </table>
            <input name="morning[]" type="hidden" value="">
            <input name="dinner[]" type="hidden" value="">
            <input name="evening[]" type="hidden" value="">
            <input name="bedtime[]" type="hidden" value="">
            <input name="noon[]" type="hidden" value="">
            <button type="submit" class="btn btn-primary" name="submit">Submit</button>
             <input type="reset" name="cancel" class="btn btn-primary" value="Cancel" id="cancel" onclick="">
        </div>
    </form>
    </div>
    <script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

	<script>
	$(document).ready(function () {
	
		$('#dailyform').submit(function() {

			var validate_count= 35;
			var current_count = $('input:checked', this).length;
			
			if(current_count == 0){
				var msg="Plase select your perfer dailyroutine";
				$("#failed_msg").html("<div class='alert alert-danger' id='msg'>" + msg + "</div>");
				setTimeout(function() {
					$('#msg').fadeOut('fast');
				}, 2000);
				return false;
			}
		});
		

		setTimeout(function() {
			$('#successmsg').fadeOut('fast');
		}, 2000);
		setTimeout(function() {
			$('#failedmsg').fadeOut('fast');
		}, 2000);

		$('#cancel').click(function() {
			var url = '<?php echo base_url()."dailyroutine" ?>';
			window.location.href = url;
			return false;
		});
	});
	</script>

	<script type="text/javascript">
       $(document).on('click', '.updateStatus', function() {
		var update_status = $(this).attr('data-btn');
		var update_st = $(this).attr('data-st');
		//alert(update_status+'-'+update_st);
		$.ajax({
			url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
			type: "POST",
			data: {
				'btn': update_status
			},
			dataType: "json",
			success: function(data) {
				$("#msg_show").html("<div class='alert alert-success' id='hide_msg'>" + data.status + "</div>");
				setTimeout(function() {
					$('#hide_msg').fadeOut('fast');
				}, 2500);
			}
		});
	});
    </script>
