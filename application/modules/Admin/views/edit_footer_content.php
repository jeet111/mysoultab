<div class="container-fluid">
	<div class="dash-counter">
		<div class="Schedule_main_one">
			<div class="users-main">
				<?php
				echo "<h2>" ?><?php if($this->uri->segment('3')){ ?>Edit Footer Content<?php }else{ ?>Add Footer Content<?php } ?></h2>

        <div class="btn_topBack">
          <a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">
            Back
          </a>
        </div>

        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" name="featureForm" id="featureForm">
          <?php if ($this->session->flashdata('success')) { ?>
            <div class="alert alert-success message">
              <button type="button" class="close" data-dismiss="alert">x</button>
              <?php echo $this->session->flashdata('success'); ?></div>
            <?php } ?>
            <div class="row">
              <div class="col-md-6">
               <div class="form-group">
                <label class="control-label col-md-4" for="">Description heading:<span class="field_req">*</span></label>
                <div class="col-md-8">
                  <input type="text" placeholder="Description heading" class="form-control" name="desc_heading" id="desc_heading" value="<?php echo $singleData->description_heading; ?>">
                  <?php echo form_error('desc_heading'); ?>

                </div>
              </div>
            </div>

            <div class="col-md-6">
             <div class="form-group">
              <label class="control-label col-md-4" for="">Description:<span class="field_req">*</span></label>
              <div class="col-md-8">
                <!-- <textarea name="feature_name" id="feature_name" placeholder="Feature Name"> </textarea> -->
                <textarea class="form-control" name="footer_desc" id="footer_desc" rows="10" cols="50"><?php echo $singleData->footer_desc; ?></textarea>
                <!-- <input type="text" placeholder="Feature Name" class="form-control" name="feature_name" id="feature_name" value=""> -->
                <?php echo form_error('footer_desc'); ?>

              </div>
            </div>
          </div>  
        </div>




        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label col-md-4" for="">Contact email:<span class="field_req">*</span></label>
              <div class="col-md-8">
                <input type="text" class="form-control" name="contact_email" id="contact_email" value="<?php echo $singleData->contact_email; ?>">
                
              </div>

            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label col-md-4" for="">Contact phone:<span class="field_req">*</span></label>
              <div class="col-md-8">
                <input type="text" class="form-control" name="contact_phone" id="contact_phone" value="<?php echo $singleData->contact_phone; ?>">
                
              </div>

            </div>
          </div>  
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label col-md-4" for="">Rights reserved:<span class="field_req">*</span></label>
              <div class="col-md-8">
                <input type="text" class="form-control" name="right_reserved" id="right_reserved" value="<?php echo $singleData->rights_reserved; ?>">
                
              </div>

            </div>
          </div>

          <div class="col-md-6">

          </div>  
        </div>

        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <input type="submit" class="btn btn-default up_but" id="postdata" class="form-control" name="submit" value="Submit">


              <!-- <a class="cancel-btn btn" href="javascript:window.history.back()">
                        Back
                      </a> -->
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        <!--Added by 95 on 5-2-2019 For Doctor category for validation-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>
        <script type="text/javascript">

         $(document).ready(function() {
           $("#featureForm").validate({
             rules: {
              desc_heading: "required",
              footer_desc: "required",
              contact_email: "required",
              contact_phone: "required",
              right_reserved:"required",

            },
            messages:{
              desc_heading: "Please enter description heading.",
              footer_desc: "Please enter description.",
              contact_email: "Please enter contact email.",
              contact_phone: "Please enter contact phone.",
              right_reserved:"Please enter rights reserved.",

            }

          });

         });


         /*Created by 95 on 6-2-2019 For Image validation*/
// function showFileSize(){
//    var img  = document.getElementById('profile_images').value;
//    	var file = img.lastIndexOf('.');
//    	var fileType	= img.slice(file);

//     if(img==='' || img===null){
//     	document.getElementById("demo").innerHTML="Please choose an image.";
// 	}else if(fileType!='.jpg'||fileType!='.png'||fileType!='.gif'||fileType!='.jpeg'){

// 			document.getElementById("demo").innerHTML="Only allowed image formats jpg,png,gif,jpeg";
// 		}else{
// 			document.getElementById("demo").innerHTML=" ";
// 		}
// 	}


// function showFileSize(){
//  var img  = document.getElementById('profile_images').value;
//  if(img==='' || img===null){
//    document.getElementById("demo").innerHTML="Please choose an image.";
//  }else{
//    document.getElementById("demo").innerHTML=" ";
//  }
// }


// /*Created by 95 on 6-2-2019 For Image validation*/
function showFileSize(){
 var img  = document.getElementById('feature_icon').value;
 if(img==='' || img===null){
   document.getElementById("flupmsgs").innerHTML="Please choose an image.";
 }else{
   document.getElementById("flupmsgs").innerHTML=" ";
 }
}


/*Created by 95 on 22-2-2019 for disable alphabets in dr. fees field*/
$('#doctor_fees').keypress(function (e) {
  var regex = new RegExp("^[0-9]+$");
  var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
  if (regex.test(str)) {
    return true;
  }

  e.preventDefault();
  return false;
});


/*Created by 95 on 22-2-2019 for disable alphabets in dr. mobile field*/
$('#doctor_mobile').keypress(function (e) {
  var regex = new RegExp("^[0-9]+$");
  var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
  if (regex.test(str)) {
    return true;
  }
  e.preventDefault();
  return false;
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#doctor_category').multiselect({
      includeSelectAllOption: true,
      allSelectedText: 'No option left ...'
    });
  });


// function readURL(input) {
//         if (input.files && input.files[0]) {
//             var reader = new FileReader();

//             reader.onload = function (e) {
//                 $('#img-upload')
//                     .attr('src', e.target.result)
//                     .width(150)
//                     .height(200);
//             };

//             reader.readAsDataURL(input.files[0]);
//         }
//     }


function loadFile (event) {
  var pcFile = $('#feature_icon').val().split('\\').pop();
  var pcExt     = pcFile.split('.').pop();
  var output = document.getElementById('img-upload');
  if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"
    || pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){
    $('#img-upload').show();
  $("#postdata").prop('disabled', false);
  output.src = URL.createObjectURL(event.target.files[0]);
  $('#flupmsgs').html('');
}else{
  $("#postdata").prop('disabled', true);
  $('#flupmsgs').html('Please select only Image file.');
  $('#img-upload').hide();
}
};

$(document).ready(function() {
  $('#img-upload').show();
});




function readURL (event) {
  var pcFile = $('input[type=file]').val().split('\\').pop();
  var pcExt     = pcFile.split('.').pop();
  var output = document.getElementById('img-upload');
  if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"
   || pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){
    $('#img-upload').show();
  output.src = URL.createObjectURL(event.target.files[0]);
  $('#flupmsg').html('');
}else{
  $('#flupmsg').html('Please select only Image file');
  $('#img-upload').hide();
}
};

</script>

<style>
  article, aside, figure, footer, header, hgroup,
  menu, nav, section { display: block; }
</style>