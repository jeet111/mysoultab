<?php defined('BASEPATH') or exit('No direct script access allowed');

class Surveytest extends MX_Controller
{

  public function __construct()
  {

    parent::__construct();

    $this->load->library('session');

    $this->load->library('form_validation');

    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

    $this->load->model('testreport_model');

    $this->load->model('Common_model_new');

    $this->load->helper(array('common_helper'));

    $this->load->helper(array('url'));

    $this->load->helper(array('form'));
  }

  //new servey

  public function survey_pop()

  {

    $wher = array('status' => 1, 'user_verify_by_app' => 1, 'device_verify' => 1, 'device_verify_by_token' => 1, 'user_role' => 2);

    $users_list = $this->Api_model->getAllUsers('cp_users', 'id', 'desc', $wher);

    $user = array();

    if (!empty($users_list)) {

      foreach ($users_list as $ur) {

        $Datelist  = $this->Api_model->getAllor('api_hit_date', 'id', 'desc', 1);

        if (!empty($Datelist)) {

          foreach ($Datelist as $dat) {

            //$TimArr = explode(",", $dat->time);

            //foreach ($TimArr as $eachTime) {

            //$time = date('H:i:s',strtotime($eachTime));

            //if ($dat->time <= date('h:i a')) {

            $dt = $dat->date;

            $TemplateData = $this->common_model->getSingleRecordById('survey_popalert', array('user_id' => $ur->id, 'admin_setdate' => $dt, 'settime' => $dat->time));

            if (!empty($TemplateData)) {

              if ($TemplateData['status'] != 1) {

                $registrationIds[] = $ur->device_token;

                //$response = $this->send_notification($registrationIds);



                $res = json_decode($response);

                if ($res->success != 0) {

                  $data = array('status' => 1, 'updateat' => date('Y-m-d H:i:s'));

                  $this->Api_model->UpdatePopAlertByUserId($TemplateData['id'], $data);

                  $user[$ur->id] = TRUE;
                } else {

                  $data = array('status' => 0, 'updateat' => date('Y-m-d H:i:s'));

                  $this->Api_model->UpdatePopAlertByUserId($TemplateData['id'], $data);

                  $user[$ur->id] = FALSE;
                }
              }
            } else {

              $newtime = date('h:i a', strtotime($dat->time));

              $registrationIds[] = $ur->device_token;

              $response = $this->send_notification($registrationIds);



              $res = json_decode($response);

              if ($res->success != 0) {

                $result = array(

                  'user_id' => $ur->id,

                  'admin_setdate' => date('Y-m-d'),

                  'settime' => $newtime,

                  'status' => 1,

                  'created_at' => date('Y-m-d H:i:s'),

                );

                $user[$ur->id] = TRUE;
              } else {

                $result = array(

                  'user_id' => $ur->id,

                  'admin_setdate' => date('Y-m-d'),

                  'settime' => $newtime,

                  'status' => 0,

                  'created_at' => date('Y-m-d H:i:s'),

                );

                $user[$ur->id] = FALSE;
              }

              $replyId = $this->common_model->addRecords('survey_popalert', $result);
            }

            //}

            //} 

          }
        }
      }
    }

    echo json_encode($user);
  }

  // End

  public function send_notification($registrationIds)

  {







    $header = [

      'Authorization: Key=AAAA-vHtgpk:APA91bHYvBeAoUQrvzzGPRNS_KSy_idvKzFdA6ebK4r1gC5vJhXZssvlpVkEnooCS_k_cgEVwMXhDbuEPbGdpxcg5muNpopjPCjJ5ZHhjo7pN3WfxtdNVHuR3wAJz5Fu-xDSyXu-PQUN',

      'Content-Type: Application/json'

    ];



    $msg = [

      'title' => 'Survey',

      'body' => 'Please click here for survey.'

    ];

    $payload = [

      'registration_ids'  => $registrationIds,

      'data'        => $msg,

      'notification' => $msg

    ];

    $curl = curl_init();

    curl_setopt_array($curl, array(

      CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",

      CURLOPT_RETURNTRANSFER => true,

      CURLOPT_CUSTOMREQUEST => "POST",

      CURLOPT_POSTFIELDS => json_encode($payload),

      CURLOPT_HTTPHEADER => $header

    ));

    $response = curl_exec($curl);

    curl_close($curl);



    // echo "<pre>";

    // print_r($response);

    // echo "<br>";

    // //die;

    return $response;
  }





  // public function newmethod($registrationIds)

  // {



  //   $msg = [

  //     'title' => 'xyz',

  //     'body' => 'notification.'

  //   ];



  //   $payload = [

  //     'registration_ids'  => $registrationIds,

  //     'data'        => $msg,

  //     //'notification'=> $msg

  //   ];





  //   $headers = array (

  // //'Authorization: key=' . API_ACCESS_KEY,

  //     'Authorization: Key=AAAA-vHtgpk:APA91bHYvBeAoUQrvzzGPRNS_KSy_idvKzFdA6ebK4r1gC5vJhXZssvlpVkEnooCS_k_cgEVwMXhDbuEPbGdpxcg5muNpopjPCjJ5ZHhjo7pN3WfxtdNVHuR3wAJz5Fu-xDSyXu-PQUN',

  //     'Content-Type: application/json'

  //   );



  //   $ch = curl_init();

  //   curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );

  //   curl_setopt( $ch,CURLOPT_POST, true );

  //   curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );

  //   curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );

  //   curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );

  //   curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $payload ) );

  //   $result = curl_exec($ch );

  //   curl_close( $ch );

  //   return $result;



  // }





}
?>