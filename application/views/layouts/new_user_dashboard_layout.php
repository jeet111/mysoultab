<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SoulTab</title>
    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">
    <!-- Styles -->
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
  
    <script src="<?php echo base_url(); ?>assets/new_user_dashboard/js/jquery.min2.js"></script>
    <link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/font-awesome2.css" rel="stylesheet" type='text/css'>
     <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type='text/css'>

    <link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/themify-icons.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/sidebar.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/custom_style.css" rel="stylesheet">
    <!-- <link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/custom.css" rel="stylesheet"> -->
    <link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/update.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/jquery-ui.css"
    rel="stylesheet" type="text/css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <!-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->

    <link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/bootstrap-timepicker.min.css" rel="stylesheet" />

    <!-- <link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/new_update1.css" rel="stylesheet"> -->
    <link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/header.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/new_update2.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/new_user_dashboard/confirmo/contributors.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/new_user_dashboard/confirmo/confirmo.css" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/new_user_dashboard/css/bootstrap.min.css">

<!-- <link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/responsive.css" rel="stylesheet"> -->
<link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/responsive2.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/new_user_dashboard/css/jquery.dataTables.min.css">

    <script src="<?php echo base_url(); ?>assets/new_user_dashboard/js/Chart.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/new_user_dashboard/js/utils.js"></script>
    <link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/popup.css" rel="stylesheet">


</head>

<body>
    <div class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0">
        <div class="header">
            <div class="col-md-1">
                <div class="logo">
                    <a href="<?php echo base_url(); ?>dashboard_new">
                        <img id="logoImg" src="<?php echo base_url(); ?>assets/new_user_dashboard/images/logo.png" data-logo_big="images/logo.png" alt="SoulTab" />
                    </a>
                </div>
            </div>
            <div class="col-md-11">
                <?php $user_name = $this->session->userdata('logged_in')['name'] . ' ' . $this->session->userdata('logged_in')['lastname'];
                ?>
				<h1><span id="greeting">Good Morning</span>, <?php echo $user_name; ?>
                    <a href="<?php echo base_url(); ?>logout" class="logout">Logout</a></h1>
					<div class="toggle-switch-container">
						<div class="toggle-switch switch-vertical">
							<input id="toggle-a" type="radio" name="switch"  />
							<label for="toggle-a">Active</label>
							<input id="toggle-b" type="radio" name="switch" checked="checked"/>
							<label for="toggle-b">Inactive</label>
							<span class="toggle-outside">
        <span class="toggle-inside"></span>
      </span>
						</div>
					</div>
			</div>


                

            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="topnav" id="myTopnav">

                <div class="dropdown">
                    <button class="dropbtn">Personal
                        <i class="fa fa-caret-down"></i>
                    </button>
                    <div class="dropdown-content">
                        <a href="<?php echo base_url(); ?>social_dashboard">Social</a>
                        <a href="<?php echo base_url(); ?>transportation_dashboard">Transportation</a>
                        <a href="<?php echo base_url(); ?>email_dashboard">Emails</a>

                        <a href="<?php echo base_url(); ?>call">Call</a>
                        <a href="<?php echo base_url(); ?>yoga">Yoga</a>
                        <a href="<?php echo base_url(); ?>spirituality">Spirituality</a>
                        <!-- <a href="<?php echo base_url(); ?>reminder_list">Reminder</a> -->

                        <a href="<?php echo base_url(); ?>show_activities"> All Activities</a>
                    </div>
                </div>
                <div class="dropdown">
                    <button class="dropbtn">Concierge
                        <i class="fa fa-caret-down"></i>
                    </button>
                    <div class="dropdown-content">
                        <a href="<?php echo base_url(); ?>call">Call</a>
                        <!-- <a href="#"> Request medicine</a> -->

                        <a href="<?php echo base_url(); ?>vital_signs">Vital signs </a>

                        <a href="<?php echo base_url(); ?>appointment_list"> Doctor Appointments</a>
                        <a href="<?php echo base_url(); ?>testreport_list"> Test Report </a>

                        <a href="<?php echo base_url(); ?>medicine_list"> Medicine Schedule</a>

                        <a href="<?php echo base_url(); ?>pharmacy_dashboard"> Pharmacy</a>
                        <a href="<?php echo base_url(); ?>dailyroutine"> Daily Routine</a>

                    </div>
                </div>
                <div class="dropdown">
                    <button class="dropbtn">Entertainment
                        <i class="fa fa-caret-down"></i>
                    </button>
                    <div class="dropdown-content">

                        <a href="<?php echo base_url(); ?>weather">Weather</a>
                        <a href="<?php echo base_url(); ?>music">Music</a>
                        <a href="<?php echo base_url(); ?>dashboard_game">Game</a>
                        <a href="<?php echo base_url();?>user_photos">Photo</a>
                        <a href="<?php echo base_url(); ?>camera">Camera</a>
                        <a href="#">Articles</a>
                        <a href="<?php echo base_url(); ?>movie">Movie</a>
                        <a href="<?php echo base_url(); ?>internet">Internet</a>
                        <a href="<?php echo base_url(); ?>news">News</a>
                        <!-- <a href="<?php echo base_url(); ?>videocall">Video call</a> -->

                    </div>
                </div>
                <div class="dropdown">
                    <button class="dropbtn">Profile
                        <i class="fa fa-caret-down"></i>
                    </button>
                    <div class="dropdown-content">
                <a href="<?php echo base_url(); ?>address_book">Address Book</a>
                <a href="<?php echo base_url(); ?>user_profile">Caregiver</a>
                <a href="<?php echo base_url(); ?>user/update_profile">User Profile</a>
                </div>
                </div>
                <!-- <a href="#">Settings</a>
                <a href="#">New Feature </a>
                <a href="#">Personal</a> -->

                <div class="dropdown">
                    <button class="dropbtn">Device
                        <i class="fa fa-caret-down"></i>
                    </button>
                    <div class="dropdown-content">
                        <a href="<?php echo base_url(); ?>devices">Tablet</a>
                    </div>
                </div>

                <a href="javascript:void(0);" style="font-size:15px;" class="icon" id="responsive_icon" onclick="myFunction()">&#9776;</a>
            </div>

            <div class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <div class="nano-content" id="vertical-menu">
                        <ul id="menu-menu-1" class="menu">
                            <li class="treeview">
                                <h3 class="dropdown-toggle head" data-toggle="collapse" onclick="$('#AAA').toggleClass('fa-plus fa-minus')"><img src="<?php echo base_url(); ?>assets/new_user_dashboard/images/personal-icon.png" alt="icon">Personal <i class="fas fa fa-minus" id="AAA"></i>
                                </h3>
                                <ul class="treeview-menu" id="toggle_text">
                                    <li><a href="<?php echo base_url(); ?>social_dashboard" <?php if ($this->uri->segment(1) == "social_dashboard") {
                                                                                                echo 'class="active"';
                                                                                            } ?>><i class="fa fa-angle-double-right"></i> Social</a></li>
                         
                                    <li><a href="<?php echo base_url(); ?>transportation_dashboard" <?php if ($this->uri->segment(1) == "transportation_dashboard") {
                                                                                                        echo 'class="active"';
                                                                                                    } ?>><i class="fa fa-angle-double-right"></i> Transportation</a></li>

                                    <li><a href="<?php echo base_url(); ?>email_dashboard" <?php if ($this->uri->segment(1) == "email_dashboard") {
                                                                                                echo 'class="active"';
                                                                                            } ?>><i class="fa fa-angle-double-right"></i> Email</a></li>
                                   
                                    <li><a href="<?php echo base_url(); ?>yoga" <?php if ($this->uri->segment(1) == "yoga") {
                                                                                    echo 'class="active"';
                                                                                } ?>><i class="fa fa-angle-double-right"></i> Yoga</a></li>
                                                                                           <li><a href="<?php echo base_url(); ?>yoga_news" <?php if ($this->uri->segment(1) == "yoga_news") {
                                                                                                echo 'class="active"';
                                                                                            } ?>><i class="fa fa-angle-double-right"></i> Yoga News</a></li>

                                    <li><a href="<?php echo base_url(); ?>spirituality" <?php if ($this->uri->segment(1) == "spirituality") {
                                                                                            echo 'class="active"';
                                                                                        } ?>><i class="fa fa-angle-double-right"></i> Spirituality</a></li>

<li><a href="<?php echo base_url(); ?>spirituality_news" <?php if ($this->uri->segment(1) == "spirituality_news") {
                                                                                                echo 'class="active"';
                                                                                            } ?>><i class="fa fa-angle-double-right"></i> Spirituality News</a></li>
                                    <!-- <li><a href="<?php echo base_url(); ?>reminder_list">Reminder</a></li> -->
                                    <li><a href="<?php echo base_url(); ?>show_activities" <?php if ($this->uri->segment(1) == "show_activities") {
                                                                                                echo 'class="active"';
                                                                                            } ?>><i class="fa fa-angle-double-right"></i> Calendar</a></li>
                                    <li> <a href="<?php echo base_url(); ?>dailyroutine" <?php if ($this->uri->segment(1) == "dailyroutine") {
                                                                                                echo 'class="active"';
                                                                                            } ?>><i class="fa fa-angle-double-right"></i> Daily Routine</a></li>



                                </ul>
                            </li>

                            <li>
                                <h3 class="dropdown-toggle head" data-toggle="collapse" onclick="$('#bbb').toggleClass('fa-minus fa-plus')"><img src="<?php echo base_url(); ?>assets/new_user_dashboard/images/concierge-icon.png" alt="icon">Concierge <i class="fas fa fa-minus" id="bbb"></i></h3>
                                <ul class="treeview-menu" id="toggle_text">
                                    <li><a href="<?php echo base_url(); ?>notes" <?php if ($this->uri->segment(1) == "notes") {
                                                                                        echo 'class="active"';
                                                                                    } ?>><i class="fa fa-angle-double-right"></i> Note</a></li>

                                    <!-- <li><a href="#"> Request medicine</a></li> -->
                                    <li><a href="<?php echo base_url(); ?>vital_signs" <?php if ($this->uri->segment(1) == "vital_signs") {
                                                                                            echo 'class="active"';
                                                                                        } ?>><i class="fa fa-angle-double-right"></i> Vital signs </a></li>

<li><a href="<?php echo base_url(); ?>appointment_list" <?php if ($this->uri->segment(1) == "appointment_list") {
                                                                                                echo 'class="active"';
                                                                                            } ?>><i class="fa fa-angle-double-right"></i> Doctor Appointments</a></li>
                                    <li><a href="<?php echo base_url(); ?>testreport_list" <?php if ($this->uri->segment(1) == "testreport_list") {
                                                                                                echo 'class="active"';
                                                                                            } ?>><i class="fa fa-angle-double-right"></i> Test Report </a></li>

                                    <li><a href="<?php echo base_url(); ?>medicine_list" <?php if ($this->uri->segment(1) == "medicine_list") {
                                                                                                echo 'class="active"';
                                                                                            } ?>><i class="fa fa-angle-double-right"></i> Medicine Schedule</a></li>
                                    <li><a href="<?php echo base_url(); ?>pharmacy_dashboard" <?php if ($this->uri->segment(1) == "pharmacy_dashboard") {
                                                                                                    echo 'class="active"';
                                                                                                } ?>> <i class="fa fa-angle-double-right"></i> Pharmacy</a></li>



                                </ul>

                            </li>
                            <li>
                                <h3 class="dropdown-toggle head" data-toggle="collapse" onclick="$('#ccc').toggleClass('fa-minus fa-plus')"><img src="<?php echo base_url(); ?>assets/new_user_dashboard/images/entertainment icon.png" alt="icon">Entertainment <i class="fas fa fa-minus" id="ccc"></i></h3>
                                <ul class="treeview-menu" id="toggle_text">
                                    <li><a href="<?php echo base_url(); ?>weather" <?php if ($this->uri->segment(1) == "weather") {
                                                                                        echo 'class="active"';
                                                                                    } ?>><i class="fa fa-angle-double-right"></i> Weather</a></li>
                                    <li><a href="<?php echo base_url(); ?>music" <?php if ($this->uri->segment(1) == "music") {
                                                                                        echo 'class="active"';
                                                                                    } ?>><i class="fa fa-angle-double-right"></i> Music</a></li>
                                    <li><a href="<?php echo base_url(); ?>dashboard_game" <?php if ($this->uri->segment(1) == "dashboard_game") {
                                                                                                echo 'class="active"';
                                                                                            } ?>><i class="fa fa-angle-double-right"></i> Game</a></li>
                                    <li><a href="<?php echo base_url();?>user_photos"  <?php if ($this->uri->segment(1) == "user_photos") {
                                                                                                echo 'class="active"';
                                                                                            } ?>><i class="fa fa-angle-double-right"></i> Photo</a></li>
                                    <li><a href="<?php echo base_url(); ?>camera" <?php if ($this->uri->segment(1) == "camera") {
                                                                                        echo 'class="active"';
                                                                                    } ?>><i class="fa fa-angle-double-right"></i> Camera</a></li>
                                    <li> <a href="#"><i class="fa fa-angle-double-right"></i> Articles</a></li>
                                    <li><a href="<?php echo base_url(); ?>movie" <?php if ($this->uri->segment(1) == "movie") {
                                                                                        echo 'class="active"';
                                                                                    } ?>><i class="fa fa-angle-double-right"></i> Movie</a></li>
                                    <li><a href="<?php echo base_url(); ?>internet" <?php if ($this->uri->segment(1) == "internet") {
                                                                                        echo 'class="active"';
                                                                                    } ?>><i class="fa fa-angle-double-right"></i> Internet</a></li>

                                    <li><a href="<?php echo base_url(); ?>news" <?php if ($this->uri->segment(1) == "news") {
                                                                                    echo 'class="active"';
                                                                                } ?>><i class="fa fa-angle-double-right"></i> News</a></li>
                                    <!-- <li><a href="<?php echo base_url(); ?>videocall" <?php if ($this->uri->segment(1) == "videocall") {
                                                                                                echo 'class="active"';
                                                                                            } ?>> <i class="fa fa-angle-double-right"></i> Video call</a></li> -->





                                </ul>
                            </li>
							<?php if($this->session->userdata('logged_in')['user_role']==2 || $this->session->userdata('logged_in')['user_role'] =='2'){ ?>
                            <li>
                                <h3 class="dropdown-toggle head" data-toggle="collapse" onclick="$('#ddd').toggleClass('fa-minus fa-plus')"><img src="<?php echo base_url(); ?>assets/new_user_dashboard/images/profile icon.png" alt="icon">Profile <i class="fas fa fa-minus" id="ddd"></i></h3>
                                <ul class="treeview-menu" id="toggle_text">

                                    <li><a href="<?php echo base_url(); ?>address_book" <?php if ($this->uri->segment(1) == "address_book") {
                                                                                            echo 'class="active"';
                                                                                        } ?>><i class="fa fa-angle-double-right"></i> Address Book</a></li>
                                    <li><a href="<?php echo base_url(); ?>user_profile" <?php if ($this->uri->segment(1) == "user_profile") {
                                                                                            echo 'class="active"';
                                                                                        } ?>><i class="fa fa-angle-double-right"></i> Caregiver</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/update_profile" <?php if ($this->uri->segment(1) == "user/update_profile") {
                                                                                                    echo 'class="active"';
                                                                                                } ?>><i class="fa fa-angle-double-right"></i> User Profile</a></li>


                                </ul>
                            </li>
							<?php } ?>																		
                            <!-- <li>

                                <h3><img src="<?php echo base_url(); ?>assets/new_user_dashboard/images/settings icon.png" alt="icon">Settings <span class="plus">+</span></h3>

                            </li>
                            <li>
                                <h3><img src="<?php echo base_url(); ?>assets/new_user_dashboard/images/new features icon.png" alt="icon">New Feature <span class="plus">+</span></h3>
                            </li>
                            <li>
                                <h3><img src="<?php echo base_url(); ?>assets/new_user_dashboard/images/personal-icon.png" alt="icon">Personal <span class="plus">+</span></h3>

                            </li> -->
                            <li>
                                <h3 class="dropdown-toggle head" data-toggle="collapse" onclick="$('#eee').toggleClass('fa-minus fa-plus')"><img src="<?php echo base_url(); ?>assets/new_user_dashboard/images/device-icon.png" alt="icon">Device <i class="fas fa fa-minus" id="eee"></i></h3>
                                <ul class="treeview-menu" id="toggle_text">
                                    <li><a href="<?php echo base_url(); ?>devices" <?php if ($this->uri->segment(1) == "devices") {
                                                                                        echo 'class="active"';
                                                                                    } ?>><i class="fa fa-angle-double-right fa fa-tablet"></i>Tablet</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
            <main role="main" class="col-md-10 ml-sm-auto col-lg-10 px-4 main_section">


                <div class="main-content">
                    <div class="row">

                        <div class="col-lg-12 dashboard_shadow">
                            <div class="st-tabs firstSection">
                                <!-- <button name="dropdownDashboard" id="dropdownDashboard">Dashboard menus</button> -->
                                <div class="st-buttons">
                                    <a href="<?php echo base_url(); ?>dashboard_new">Dashboard</a>

                                    <a href="<?php echo base_url(); ?>alerts">Alerts</a>
                                    <!-- <button style="color: #000000">Messages(1)</button> -->
                                    <a href="<?php echo base_url(); ?>email_dashboard">Email</a>
                                    <a href="<?php echo base_url(); ?>videocall">Video Call</a>
                                    <a href="<?php echo base_url(); ?>yoga">Yoga</a>
                                    <a href="<?php echo base_url(); ?>dailyroutine">Daily Routine</a>

                                </div>
                                <div class="st-buttons">
                                    <a href="<?php echo base_url(); ?>appointment_list">Doctor's Appointment</a>
                                    <a href="<?php echo base_url(); ?>medicine_list">Medicine Management</a>
                                    <a href="<?php echo base_url(); ?>testreport_list"> Test Report </a>
                                    <a href="<?php echo base_url(); ?>notes">Notes</a>
                                    <a href="<?php echo base_url(); ?>vital_signs">Vital Signs</a>
                                    <a href="<?php echo base_url(); ?>show_activities">Calendar</a>
                                </div>
                            </div>
                        </div>


                        <?php echo $contents; ?>
                    </div>


                    <!-- /# row -->

                    <!-- /# main content -->
                </div>
            </main>
            <!-- /# container-fluid -->
        </div>
    </div>


    <!-------- popup---->

    <main id="myContainer" class="MainContainer">

        <!-- Open The Modal -->


    </main>
    <!-- add game model -->
    <div id="addgamemyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="gameform">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Game</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">




                        <div class="row">


                            <div class="col-lg-4" style="padding-top:0;">
                                <label for="name"><b>Game Type:</b></label>
                            </div>
                            <div class="col-lg-8">

                                <label class="checkbox-inline">
                                    <input type="radio" class="game_type" name="game_type" data="" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="game_type" value="App" data="" name="game_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_add_game">
                                <span style="color:red" id="button_name_add_game_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website">
                            <div class="col-lg-4">
                                <label for="email"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_add_game">
                                <span style="color:red" id="url_add_game_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App" style="display:none">
                            <div class="col-lg-4">
                                <label for="email"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select id="appname_add_game" name="app_name" class="form-control">
                                    <option value="">Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="appname_add_game_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="1" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-success" id="btn-add">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Modal HTML -->
    <div id="editgamemyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="update_form">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Game</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_u" name="id" class="form-control" required>
                        <div class="row">


                            <div class="col-lg-4" style="padding-top:0;">
                                <label for="name"><b>Game Type:</b></label>
                            </div>
                            <div class="col-lg-8" id="gametype_u">

                                <label class="checkbox-inline">
                                    <input type="radio" class="game_type" name="game_type" data="_u" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="game_type" value="App" data="_u" name="game_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_u" class="editgamemyModal_button_name">
                                <span style="color:red" id="button_name_u_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_u">
                            <div class="col-lg-4">
                                <label for="email"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_u" class="editgamemyModal_url">
                                <span style="color:red" id="url_u_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_u" style="display:none">
                            <div class="col-lg-4">
                                <label for="email"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select name="app_name" id="app_name_u" class="editgamemyModal_app_name form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="app_name_u_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="2" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-info" id="update_game">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="deletegamemyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>

                    <div class="modal-header">
                        <h4 class="modal-title">Delete Game </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_d" name="id" class="form-control">
                        <p>Are you sure you want to delete these Records?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-danger" id="delete_game">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Email Modal -->

    <!-- add email model -->
    <div id="addemailmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="emailform">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Email</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">




                        <div class="row">


                            <div class="col-lg-4" style="padding-top:0;">
                                <label for="name"><b>Email Type:</b></label>
                            </div>
                            <div class="col-lg-8">

                                <label class="checkbox-inline">
                                    <input type="radio" class="email_type" name="email_type" data="_email" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="email_type" value="App" data="_email" name="email_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_add_email">
                                <span style="color:red" id="button_name_add_email_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_email">
                            <div class="col-lg-4">
                                <label for="email"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_add_email">
                                <span style="color:red" id="url_add_email_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_email" style="display:none">
                            <div class="col-lg-4">
                                <label for="email"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select id="appname_add_email" name="app_name" class="form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="appname_add_email_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="1" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-success" id="btn-add-email">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Modal HTML -->
    <div id="editemailmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="update_form_email">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Email</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_u_email" name="id" class="form-control" required>
                        <div class="row">


                            <div class="col-lg-4" style="padding-top:0;">
                                <label for="name"><b>Email Type:</b></label>
                            </div>
                            <div class="col-lg-8" id="emailtype_u">

                                <label class="checkbox-inline">
                                    <input type="radio" class="email_type" name="email_type" data="_u_email" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="email_type" value="App" data="_u_email" name="email_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_u_email" class="editgamemyModal_button_name">
                                <span style="color:red" id="button_name_u_email_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_u_email">
                            <div class="col-lg-4">
                                <label for="email"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_u_email" class="editgamemyModal_url">
                                <span style="color:red" id="url_u_email_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_u_email" style="display:none">
                            <div class="col-lg-4">
                                <label for="email"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select name="app_name" id="app_name_u_email" class="editgamemyModal_app_name form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="app_name_u_email_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="2" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-info" id="update_email">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="deleteemailmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>

                    <div class="modal-header">
                        <h4 class="modal-title">Delete Email </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_d_email" name="id" class="form-control">
                        <p>Are you sure you want to delete these Records?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-danger" id="delete_email">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- add social model -->
    <div id="addsocialmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="socialform">
                    <div class="modal-header">
                        <h4 class="modal-title">Add social</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">




                        <div class="row">


                            <div class="col-lg-4">
                                <label for="name"><b>Social Type:</b></label>
                            </div>
                            <div class="col-lg-8">

                                <label class="checkbox-inline">
                                    <input type="radio" class="social_type" name="social_type" data="_social" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="social_type" value="App" data="_social" name="social_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_add_social">
                                <span style="color:red" id="button_name_add_social_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_social">
                            <div class="col-lg-4">
                                <label for="social"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_add_social">
                                <span style="color:red" id="url_add_social_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_social" style="display:none">
                            <div class="col-lg-4">
                                <label for="social"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select id="appname_add_social" name="app_name" class="form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="appname_add_social_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="1" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-success" id="btn-add-social">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Modal HTML -->
    <div id="editsocialmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="update_form_social">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit social</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_u_social" name="id" class="form-control" required>
                        <div class="row">


                            <div class="col-lg-4">
                                <label for="name"><b>social Type:</b></label>
                            </div>
                            <div class="col-lg-8" id="socialtype_u">

                                <label class="checkbox-inline">
                                    <input type="radio" class="social_type" name="social_type" data="_u_social" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="social_type" value="App" data="_u_social" name="social_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_u_social" class="editgamemyModal_button_name">
                                <span style="color:red" id="button_name_u_social_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_u_social">
                            <div class="col-lg-4">
                                <label for="social"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_u_social" class="editgamemyModal_url">
                                <span style="color:red" id="url_u_social_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_u_social" style="display:none">
                            <div class="col-lg-4">
                                <label for="social"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select name="app_name" id="app_name_u_social" class="editgamemyModal_app_name form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="app_name_u_social_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="2" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-info" id="update_social">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="deletesocialmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>

                    <div class="modal-header">
                        <h4 class="modal-title">Delete social </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_d_social" name="id" class="form-control">
                        <p>Are you sure you want to delete these Records?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-danger" id="delete_social">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- add transportation model -->
    <div id="addtransportationmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="transportationform">
                    <div class="modal-header">
                        <h4 class="modal-title">Add transportation</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">




                        <div class="row">


                            <div class="col-lg-4" style="padding-top: 0;">
                                <label for="name"><b>Transportation Type:</b></label>
                            </div>
                            <div class="col-lg-8">

                                <label class="checkbox-inline">
                                    <input type="radio" class="transportation_type" name="transportation_type" data="_transportation" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="transportation_type" value="App" data="_transportation" name="transportation_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_add_transportation">
                                <span style="color:red" id="button_name_add_transportation_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_transportation">
                            <div class="col-lg-4">
                                <label for="transportation"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_add_transportation">
                                <span style="color:red" id="url_add_transportation_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_transportation" style="display:none">
                            <div class="col-lg-4">
                                <label for="transportation"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select id="appname_add_transportation" name="app_name" class="form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="appname_add_transportation_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="1" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-success" id="btn-add-transportation">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Modal HTML -->
    <div id="edittransportationmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="update_form_transportation">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit transportation</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_u_transportation" name="id" class="form-control" required>
                        <div class="row">


                            <div class="col-lg-4">
                                <label for="name"><b>transportation Type:</b></label>
                            </div>
                            <div class="col-lg-8" id="transportationtype_u">

                                <label class="checkbox-inline">
                                    <input type="radio" class="transportation_type" name="transportation_type" data="_u_transportation" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="transportation_type" value="App" data="_u_transportation" name="transportation_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_u_transportation" class="editgamemyModal_button_name">
                                <span style="color:red" id="button_name_u_transportation_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_u_transportation">
                            <div class="col-lg-4">
                                <label for="transportation"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_u_transportation" class="editgamemyModal_url">
                                <span style="color:red" id="url_u_transportation_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_u_transportation" style="display:none">
                            <div class="col-lg-4">
                                <label for="transportation"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select name="app_name" id="app_name_u_transportation" class="editgamemyModal_app_name form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="app_name_u_transportation_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="2" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-info" id="update_transportation">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="deletetransportationmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>

                    <div class="modal-header">
                        <h4 class="modal-title">Delete transportation </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_d_transportation" name="id" class="form-control">
                        <p>Are you sure you want to delete these Records?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-danger" id="delete_transportation">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- add pharmacy model -->
    <div id="addpharmacymyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="pharmacyform">
                    <div class="modal-header">
                        <h4 class="modal-title">Add pharmacy</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">




                        <div class="row">


                            <div class="col-lg-4">
                                <label for="name"><b>pharmacy Type:</b></label>
                            </div>
                            <div class="col-lg-8">

                                <label class="checkbox-inline">
                                    <input type="radio" class="pharmacy_type" name="pharmacy_type" data="_pharmacy" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="pharmacy_type" value="App" data="_pharmacy" name="pharmacy_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_add_pharmacy">
                                <span style="color:red" id="button_name_add_pharmacy_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_pharmacy">
                            <div class="col-lg-4">
                                <label for="pharmacy"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_add_pharmacy">
                                <span style="color:red" id="url_add_pharmacy_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_pharmacy" style="display:none">
                            <div class="col-lg-4">
                                <label for="pharmacy"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select id="appname_add_pharmacy" name="app_name" class="form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="appname_add_pharmacy_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="1" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-success" id="btn-add-pharmacy">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Modal HTML -->
    <div id="editpharmacymyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="update_form_pharmacy">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit pharmacy</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_u_pharmacy" name="id" class="form-control" required>
                        <div class="row">


                            <div class="col-lg-4">
                                <label for="name"><b>pharmacy Type:</b></label>
                            </div>
                            <div class="col-lg-8" id="pharmacytype_u">

                                <label class="checkbox-inline">
                                    <input type="radio" class="pharmacy_type" name="pharmacy_type" data="_u_pharmacy" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="pharmacy_type" value="App" data="_u_pharmacy" name="pharmacy_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_u_pharmacy" class="editgamemyModal_button_name">
                                <span style="color:red" id="button_name_u_pharmacy_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_u_pharmacy">
                            <div class="col-lg-4">
                                <label for="pharmacy"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_u_pharmacy" class="editgamemyModal_url">
                                <span style="color:red" id="url_u_pharmacy_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_u_pharmacy" style="display:none">
                            <div class="col-lg-4">
                                <label for="pharmacy"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select name="app_name" id="app_name_u_pharmacy" class="editgamemyModal_app_name form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="app_name_u_pharmacy_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="2" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-info" id="update_pharmacy">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="deletepharmacymyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>

                    <div class="modal-header">
                        <h4 class="modal-title">Delete pharmacy </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_d_pharmacy" name="id" class="form-control">
                        <p>Are you sure you want to delete these Records?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-danger" id="delete_pharmacy">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- add Movie model -->
    <div id="addMoviemyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="Movieform">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Movie</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">




                        <div class="row">


                            <div class="col-lg-4" style="padding-top:0;">
                                <label for="name"><b>Movie Type:</b></label>
                            </div>
                            <div class="col-lg-8">

                                <label class="checkbox-inline">
                                    <input type="radio" class="Movie_type" name="Movie_type" data="_Movie" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="Movie_type" value="App" data="_Movie" name="Movie_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_add_Movie">
                                <span style="color:red" id="button_name_add_Movie_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_Movie">
                            <div class="col-lg-4">
                                <label for="Movie"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_add_Movie">
                                <span style="color:red" id="url_add_Movie_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_Movie" style="display:none">
                            <div class="col-lg-4">
                                <label for="Movie"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select id="appname_add_Movie" name="app_name" class="form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="appname_add_Movie_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="1" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-success" id="btn-add-Movie">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Modal HTML -->
    <div id="editMoviemyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="update_form_Movie">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Movie</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_u_Movie" name="id" class="form-control" required>
                        <div class="row">


                            <div class="col-lg-4" style="padding-top:0;">
                                <label for="name"><b>Movie Type:</b></label>
                            </div>
                            <div class="col-lg-8" id="Movietype_u">

                                <label class="checkbox-inline">
                                    <input type="radio" class="Movie_type" name="Movie_type" data="_u_Movie" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="Movie_type" value="App" data="_u_Movie" name="Movie_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_u_Movie" class="editgamemyModal_button_name">
                                <span style="color:red" id="button_name_u_Movie_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_u_Movie">
                            <div class="col-lg-4">
                                <label for="Movie"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_u_Movie" class="editgamemyModal_url">
                                <span style="color:red" id="url_u_Movie_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_u_Movie" style="display:none">
                            <div class="col-lg-4">
                                <label for="Movie"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select name="app_name" id="app_name_u_Movie" class="editgamemyModal_app_name form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="app_name_u_Movie_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="2" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-info" id="update_Movie">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="deleteMoviemyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>

                    <div class="modal-header">
                        <h4 class="modal-title">Delete Movie </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_d_Movie" name="id" class="form-control">
                        <p>Are you sure you want to delete these Records?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-danger" id="delete_Movie">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- add music model -->
    <div id="addmusicmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="musicform">
                    <div class="modal-header">
                        <h4 class="modal-title">Add music</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">




                        <div class="row">


                            <div class="col-lg-4" style="padding-top:0;">
                                <label for="name"><b>Music Type:</b></label>
                            </div>
                            <div class="col-lg-8">

                                <label class="checkbox-inline">
                                    <input type="radio" class="music_type" name="music_type" data="_music" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="music_type" value="App" data="_music" name="music_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_add_music">
                                <span style="color:red" id="button_name_add_music_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_music">
                            <div class="col-lg-4 margin">
                                <label for="music"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_add_music">
                                <span style="color:red" id="url_add_music_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_music" style="display:none">
                            <div class="col-lg-4 margin">
                                <label for="music"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select id="appname_add_music" name="app_name" class="form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="appname_add_music_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="1" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-success" id="btn-add-music">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Modal HTML -->
    <div id="editmusicmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="update_form_music">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit music</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_u_music" name="id" class="form-control" required>
                        <div class="row">


                            <div class="col-lg-4" style="padding-top:0;"> 
                                <label for="name"><b>Music Type:</b></label>
                            </div>
                            <div class="col-lg-8" id="musictype_u">

                                <label class="checkbox-inline">
                                    <input type="radio" class="music_type" name="music_type" data="_u_music" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="music_type" value="App" data="_u_music" name="music_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 margin">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_u_music" class="editgamemyModal_button_name">
                                <span style="color:red" id="button_name_u_music_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_u_music">
                            <div class="col-lg-4 margin">
                                <label for="music"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_u_music" class="editgamemyModal_url">
                                <span style="color:red" id="url_u_music_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_u_music" style="display:none">
                            <div class="col-lg-4 margin">
                                <label for="music"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select name="app_name" id="app_name_u_music" class="editgamemyModal_app_name form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="app_name_u_music_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="2" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-info" id="update_music">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="deletemusicmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>

                    <div class="modal-header">
                        <h4 class="modal-title">Delete music </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_d_music" name="id" class="form-control">
                        <p>Are you sure you want to delete these Records?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-danger" id="delete_music">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- add weather model -->
    <div id="addweathermyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="weatherform">
                    <div class="modal-header">
                        <h4 class="modal-title">Add weather</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">




                        <div class="row">


                            <div class="col-lg-4 " style="padding-top:0;"> 
                                <label for="name"><b>Weather Type:</b></label>
                            </div>
                            <div class="col-lg-8 ">

                                <label class="checkbox-inline">
                                    <input type="radio" class="weather_type" name="weather_type" data="_weather" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="weather_type" value="App" data="_weather" name="weather_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 ">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_add_weather">
                                <span style="color:red" id="button_name_add_weather_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_weather">
                            <div class="col-lg-4 margin">
                                <label for="weather"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_add_weather">
                                <span style="color:red" id="url_add_weather_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_weather" style="display:none">
                            <div class="col-lg-4 ">
                                <label for="weather"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8 ">
                                <select id="appname_add_weather" name="app_name" class="form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="appname_add_weather_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="1" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-success" id="btn-add-weather">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Modal HTML -->
    <div id="editweathermyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="update_form_weather">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit weather</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_u_weather" name="id" class="form-control" required>
                        <div class="row">


                            <div class="col-lg-4" style="padding-top:0;">
                                <label for="name"><b>Weather Type:</b></label>
                            </div>
                            <div class="col-lg-8" id="weathertype_u">

                                <label class="checkbox-inline">
                                    <input type="radio" class="weather_type" name="weather_type" data="_u_weather" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="weather_type" value="App" data="_u_weather" name="weather_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_u_weather" class="editgamemyModal_button_name">
                                <span style="color:red" id="button_name_u_weather_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_u_weather">
                            <div class="col-lg-4 margin">
                                <label for="weather"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_u_weather" class="editgamemyModal_url">
                                <span style="color:red" id="url_u_weather_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_u_weather" style="display:none">
                            <div class="col-lg-4 margin">
                                <label for="weather"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select name="app_name" id="app_name_u_weather" class="editgamemyModal_app_name form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="app_name_u_weather_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="2" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-info" id="update_weather">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="deleteweathermyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>

                    <div class="modal-header">
                        <h4 class="modal-title">Delete weather </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_d_weather" name="id" class="form-control">
                        <p>Are you sure you want to delete these Records?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-danger" id="delete_weather">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- add news model -->
    <div id="addnewsmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="newsform">
                    <div class="modal-header">
                        <h4 class="modal-title">Add news</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">




                        <div class="row">


                            <div class="col-lg-4" style="padding-top:0;">
                                <label for="name"><b>News Type:</b></label>
                            </div>
                            <div class="col-lg-8">

                                <label class="checkbox-inline">
                                    <input type="radio" class="news_type" name="news_type" data="_news" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="news_type" value="App" data="_news" name="news_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_add_news">
                                <span style="color:red" id="button_name_add_news_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_news">
                            <div class="col-lg-4">
                                <label for="news"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_add_news">
                                <span style="color:red" id="url_add_news_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_news" style="display:none">
                            <div class="col-lg-4 margin">
                                <label for="news"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select id="appname_add_news" name="app_name" class="form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="appname_add_news_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="1" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-success" id="btn-add-news">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Modal HTML -->
    <div id="editnewsmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="update_form_news">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit news</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_u_news" name="id" class="form-control" required>
                        <div class="row">


                            <div class="col-lg-4" style="padding-top:0;">
                                <label for="name"><b>News Type:</b></label>
                            </div>
                            <div class="col-lg-8" id="newstype_u">

                                <label class="checkbox-inline">
                                    <input type="radio" class="news_type" name="news_type" data="_u_news" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="news_type" value="App" data="_u_news" name="news_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_u_news" class="editgamemyModal_button_name">
                                <span style="color:red" id="button_name_u_news_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_u_news">
                            <div class="col-lg-4">
                                <label for="news"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_u_news" class="editgamemyModal_url">
                                <span style="color:red" id="url_u_news_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_u_news" style="display:none">
                            <div class="col-lg-4">
                                <label for="news"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select name="app_name" id="app_name_u_news" class="editgamemyModal_app_name form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="app_name_u_news_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="2" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-info" id="update_news">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="deletenewsmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>

                    <div class="modal-header">
                        <h4 class="modal-title">Delete news </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_d_news" name="id" class="form-control">
                        <p>Are you sure you want to delete these Records?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-danger" id="delete_news">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- add internet model -->
    <div id="addinternetmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="internetform">
                    <div class="modal-header">
                        <h4 class="modal-title">Add internet</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">




                        <div class="row">


                            <div class="col-lg-4" style="padding-top:0;">
                                <label for="name"><b>Internet Type:</b></label>
                            </div>
                            <div class="col-lg-8">

                                <label class="checkbox-inline">
                                    <input type="radio" class="internet_type" name="internet_type" data="_internet" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="internet_type" value="App" data="_internet" name="internet_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_add_internet">
                                <span style="color:red" id="button_name_add_internet_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_internet">
                            <div class="col-lg-4">
                                <label for="internet"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_add_internet">
                                <span style="color:red" id="url_add_internet_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_internet" style="display:none">
                            <div class="col-lg-4">
                                <label for="internet"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select id="appname_add_internet" name="app_name" class="form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="appname_add_internet_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="1" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-success" id="btn-add-internet">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Modal HTML -->
    <div id="editinternetmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="update_form_internet">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit internet</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_u_internet" name="id" class="form-control" required>
                        <div class="row">


                            <div class="col-lg-4" style="padding-top:0;">
                                <label for="name"><b>Internet Type:</b></label>
                            </div>
                            <div class="col-lg-8" id="internettype_u">

                                <label class="checkbox-inline">
                                    <input type="radio" class="internet_type" name="internet_type" data="_u_internet" value="Website" checked>Website
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="internet_type" value="App" data="_u_internet" name="internet_type">App
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 margin">
                                <label for="name"><b>Button Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="button_name" id="button_name_u_internet" class="editgamemyModal_button_name">
                                <span style="color:red" id="button_name_u_internet_error"></span>
                            </div>
                        </div>


                        <div class="row" id="Website_u_internet">
                            <div class="col-lg-4 margin">
                                <label for="internet"><b>Website URL:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <input type="text" name="url" id="url_u_internet" class="editgamemyModal_url">
                                <span style="color:red" id="url_u_internet_error"></span>
                            </div>
                        </div>
                        <div class="row" id="App_u_internet" style="display:none">
                            <div class="col-lg-4 margin">
                                <label for="internet"><b>App Name:</b></label>
                            </div>

                            <div class="col-lg-8">
                                <select name="app_name" id="app_name_u_internet" class="editgamemyModal_app_name form-control">
                                    <option>Select</option>
                                    <?php foreach ($app_list as $key => $app) { ?>


                                        <option value="<?php echo $app->id; ?>"><?php echo $app->name; ?></option>
                                    <?php } ?>

                                </select>
                                <span style="color:red" id="app_name_u_internet_error"></span>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="2" name="type">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-info" id="update_internet">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="deleteinternetmyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>

                    <div class="modal-header">
                        <h4 class="modal-title">Delete internet </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id_d_internet" name="id" class="form-control">
                        <p>Are you sure you want to delete these Records?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-danger" id="delete_internet">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript" src="<?php echo base_url(); ?>assets/new_user_dashboard/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/new_user_dashboard/js/bootstrap.min1.js"></script>
    <script src="<?php echo base_url(); ?>assets/new_user_dashboard/js/jquery.validate.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/new_user_dashboard/js/plugins/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/magnific-popup/jquery.magnific-popup.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/new_user_dashboard/js/plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/new_user_dashboard/confirmo/confirmo.js" crossorigin=""></script>
    <script src="<?php echo base_url(); ?>assets/new_user_dashboard/confirmo/contributors.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>   
    <script src="<?php echo base_url(); ?>assets/new_user_dashboard/js/moment.min.js"></script>
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script> -->
<script src="<?php echo base_url(); ?>assets/new_user_dashboard/js/bootstrap-datetimepicker.min.js"></script>

<script src="<?php echo base_url(); ?>assets/new_user_dashboard/js/bootstrap-timepicker.min.js"></script>

       <script>
           $(document).load($(window).bind("resize", checkPosition));

function checkPosition()
{
    if($(window).width() < 813)
    {
        $(".st-buttons").hide();
        $(document).on('click', '#dropdownDashboard', function() {
            $(".st-buttons").slideToggle("slow");

        });   
    }
}

        $("#responsive_icon").click(function() {

            $(".responsive").slideDown(400);

        });

        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>
    <script>
        //     $('#toggle_icon').toggle(function() {

        // $('#toggle_icon').text('-');
        // $('#toggle_text').slideToggle();

        // }, function() {

        // $('#toggle_icon').text('+');
        // $('#toggle_text').slideToggle();

        // });
        $(document).ready(function() {
            $(".modal").on("hidden.bs.modal", function(){
                $(this).find('form').trigger('reset');

});
            //call default first time
            checkDeviceStatus();
            //call after 10 min  (1000*60*10)
            setInterval(function(){ checkDeviceStatus(); }, 600000);
	function checkDeviceStatus(){
            $.ajax({
                type: "get",
                url: "device_login",
                success: function(dataResult) {
                    var dataResult = JSON.parse(dataResult);
                    // console.log('ddd',dataResult.duration);
                    if(dataResult.duration < 10) {
                        $('#toggle-b').prop( "checked", false );
                        $('#toggle-a').prop( "checked", true );
                    }else {
                        $('#toggle-a').prop( "checked", false );
                        $('#toggle-b').prop( "checked", true );

					}

                }
            });
    }

            var visitortime = new Date();
            var hours  = visitortime.getHours()
            $.ajax({
                type: "post",
                url: "show_greeting_message",
				data: {hour: hours},
                success: function(dataResult) {
                    var dataResult = JSON.parse(dataResult);
                    if(dataResult.message !='' ) {
                     $('#greeting').text(dataResult.message);
                    }

                }
            });

            $("ul.treeview-menu").addClass("open_menu");

            jQuery(".treeview-menu li a").click(function() {
                if ($(".treeview-menu li a").hasClass("active")) {
                    $(".active").closest("ul.treeview-menu").addClass("open_menu");

                }
            });
            $(".head").on("click", function() {
                $(this).next("ul.treeview-menu").slideToggle();
                $(this).next("ul.treeview-menu").toggleClass("open_menu")

                let $list = $(this).next("ul.treeview-menu");
                $("ul.treeview-menu").not($list).hide();
                // $list.slideToggle();
                //    $(this).next(".plus").addClass("minus");

            });

        });
    </script>
    <!-- scripit init-->
    <script type="text/javascript">
        $(".game_type").click(function() {
            var value_game = $(this).val();
            var data_up = $(this).attr("data");
            if (value_game == 'App') {
                $("#Website" + data_up).hide();
                $("#App" + data_up).show();
            } else {
                $("#Website" + data_up).show();
                $("#App" + data_up).hide();
            }
        });

        $(".email_type").click(function() {
            var value_game = $(this).val();
            var data_up = $(this).attr("data");
            if (value_game == 'App') {
                $("#Website" + data_up).hide();
                $("#App" + data_up).show();
            } else {
                $("#Website" + data_up).show();
                $("#App" + data_up).hide();
            }
        });

        $(".social_type").click(function() {
            var value_game = $(this).val();
            var data_up = $(this).attr("data");
            if (value_game == 'App') {
                $("#Website" + data_up).hide();
                $("#App" + data_up).show();
            } else {
                $("#Website" + data_up).show();
                $("#App" + data_up).hide();
            }
        });

        $(".pharmacy_type").click(function() {
            var value_game = $(this).val();
            var data_up = $(this).attr("data");
            if (value_game == 'App') {
                $("#Website" + data_up).hide();
                $("#App" + data_up).show();
            } else {
                $("#Website" + data_up).show();
                $("#App" + data_up).hide();
            }
        });

        $(".transportation_type").click(function() {
            var value_game = $(this).val();
            var data_up = $(this).attr("data");
            if (value_game == 'App') {
                $("#Website" + data_up).hide();
                $("#App" + data_up).show();
            } else {
                $("#Website" + data_up).show();
                $("#App" + data_up).hide();
            }
        });

        $(".Movie_type").click(function() {
            var value_game = $(this).val();
            var data_up = $(this).attr("data");
            if (value_game == 'App') {
                $("#Website" + data_up).hide();
                $("#App" + data_up).show();
            } else {
                $("#Website" + data_up).show();
                $("#App" + data_up).hide();
            }
        });

        $(".music_type").click(function() {
            var value_game = $(this).val();
            var data_up = $(this).attr("data");
            if (value_game == 'App') {
                $("#Website" + data_up).hide();
                $("#App" + data_up).show();
            } else {
                $("#Website" + data_up).show();
                $("#App" + data_up).hide();
            }
        });

        $(".weather_type").click(function() {
            var value_game = $(this).val();
            var data_up = $(this).attr("data");
            if (value_game == 'App') {
                $("#Website" + data_up).hide();
                $("#App" + data_up).show();
            } else {
                $("#Website" + data_up).show();
                $("#App" + data_up).hide();
            }
        });

        $(".internet_type").click(function() {
            var value_game = $(this).val();
            var data_up = $(this).attr("data");
            if (value_game == 'App') {
                $("#Website" + data_up).hide();
                $("#App" + data_up).show();
            } else {
                $("#Website" + data_up).show();
                $("#App" + data_up).hide();
            }
        });

        $(".news_type").click(function() {
            var value_game = $(this).val();
            var data_up = $(this).attr("data");
            if (value_game == 'App') {
                $("#Website" + data_up).hide();
                $("#App" + data_up).show();
            } else {
                $("#Website" + data_up).show();
                $("#App" + data_up).hide();
            }
        });
    </script>


    <!-- script related to game section -->
    <script type="text/javascript">
        $(document).on('click', '#btn-add', function(e) {
            var button_name_add_game = $("#button_name_add_game").val();
            var url_add_game = $("#url_add_game").val();
            var appname_add_game = $("#appname_add_game").val();
            var game_type = $('input[name="game_type"]:checked').val();
            var validate_flag = 0;

            if (button_name_add_game == '') {
                validate_flag = 1;
                $("#button_name_add_game_error").text("Button Name is Required");
            } else {
                validate_flag = 0;
                $("#button_name_add_game_error").text("");
            }

            if (game_type == 'App') {
                if (appname_add_game == '') {
                    validate_flag = 1;
                    $("#appname_add_game_error").text("App Name is required");
                } else {
                    validate_flag = 0;
                    $("#appname_add_game_error").text("");
                }
            } else {
                if (url_add_game == '') {
                    validate_flag = 1;
                    $("#url_add_game_error").text("Url is required");
                } else {
                    if (validURL(url_add_game)) {
                        validate_flag = 0;
                        $("#url_add_game_error").text("");
                    } else {
                        validate_flag = 1;
                        $("#url_add_game_error").text("Url is not vaild");
                    }
                }
            }
            // console.log(game_type);
            var data = $("#gameform").serialize();
            if (validate_flag != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "add_game",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#addgamemyModal').modal('hide');
                        //alert("Game added successfully");
                        location.reload();

                    }
                });
            }

        });
        $(document).on('click', '.update', function(e) {
            var gameid = $(this).attr("data-gameid");
            var gametype = $(this).attr("data-gametype");
            var url = $(this).attr("data-url");
            var app_name = $(this).attr("data-app_name");
            var button_name = $(this).attr("data-button_name");
            $('#id_u').val(gameid);
            $('#gametype_u').find(':radio[name=game_type][value="' + gametype + '"]').prop('checked', true);
            $('#url_u').val(url);
            $('#app_name_u').val(app_name);
            $('#button_name_u').val(button_name);
            if (gametype == 'App') {
                $("#Website_u").hide();
                $("#App_u").show();
            } else {
                $("#Website_u").show();
                $("#App_u").hide();
            }
        });

        $(document).on('click', '#update_game', function(e) {
            var button_name_u = $("#button_name_u").val();
            var url_u = $("#url_u").val();
            var app_name_u = $("#app_name_u").val();
            var game_type = $('input[name="game_type"]:checked').val();
            var validate_flag_u = 0;

            if (button_name_u == '') {
                validate_flag_u = 1;
                $("#button_name_u_error").text("Button Name is Required");
            } else {
                validate_flag_u = 0;
                $("#button_name_u_error").text("");
            }

            if (game_type == 'App') {
                if (app_name_u == '') {
                    validate_flag_u = 1;
                    $("#app_name_u_error").text("App Name is required");
                } else {
                    validate_flag_u = 0;
                    $("#app_name_u_error").text("");
                }
            } else {
                if (url_u == '') {
                    validate_flag_u = 1;
                    $("#url_u_error").text("Url is required");
                } else {
                    if (validURL(url_u)) {
                        validate_flag_u = 0;
                        $("#url_u_error").text("");
                    } else {
                        validate_flag_u = 1;
                        $("#url_u_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#update_form").serialize();
            console.log(data);
            if (validate_flag_u != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "edit_game",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#editgamemyModal').modal('hide');
                        //alert("Game Updated successfully");
                        location.reload();
                    }
                });
            }

        });
        $(document).on("click", ".delete", function() {
            var id = $(this).attr("data-id");
            $('#id_d').val(id);

        });
        $(document).on("click", "#delete_game", function() {
            $.ajax({
                url: "delete_game",
                type: "POST",
                cache: false,
                data: {
                    type: 3,
                    id: $("#id_d").val()
                },
                success: function(dataResult) {
                    location.reload();
                    $('#deletegamemyModal').modal('hide');
                }
            });
        });
    </script>
    <!-- script related to email section -->
    <script type="text/javascript">
        $(document).on('click', '#btn-add-email', function(e) {
            var button_name_add_email = $("#button_name_add_email").val();
            var url_add_email = $("#url_add_email").val();
            var appname_add_email = $("#appname_add_email").val();
            var email_type = $('input[name="email_type"]:checked').val();
            var validate_flag_email = 0;

            if (button_name_add_email == '') {
                validate_flag_email = 1;
                $("#button_name_add_email_error").text("Button Name is Required");
            } else {
                validate_flag_email = 0;
                $("#button_name_add_email_error").text("");
            }

            if (email_type == 'App') {
                if (appname_add_email == '') {
                    validate_flag_email = 1;
                    $("#appname_add_email_error").text("App Name is required");
                } else {
                    validate_flag_email = 0;
                    $("#appname_add_email_error").text("");
                }
            } else {
                if (url_add_email == '') {
                    validate_flag_email = 1;
                    $("#url_add_email_error").text("Url is required");
                } else {
                    if (validURL(url_add_email)) {
                        validate_flag_email = 0;
                        $("#url_add_email_error").text("");
                    } else {
                        validate_flag_email = 1;
                        $("#url_add_email_error").text("Url is not vaild");
                    }
                }
            }

            var data = $("#emailform").serialize();
            if (validate_flag_email != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "add_email",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#addemailmyModal').modal('hide');
                        //alert("Email added successfully");
                        location.reload();

                    }
                });
            }
        });
        $(document).on('click', '.update', function(e) {
            var emailid = $(this).attr("data-emailid");
            var emailtype = $(this).attr("data-emailtype");
            var url = $(this).attr("data-url");
            var app_name = $(this).attr("data-app_name");
            var button_name = $(this).attr("data-button_name");
            $('#id_u_email').val(emailid);
            $('#emailtype_u').find(':radio[name=email_type][value="' + emailtype + '"]').prop('checked', true);
            $('#url_u_email').val(url);
            $('#app_name_u_email').val(app_name);
            $('#button_name_u_email').val(button_name);
            if (emailtype == 'App') {
                $("#Website_u").hide();
                $("#App_u").show();
            } else {
                $("#Website_u").show();
                $("#App_u").hide();
            }
        });

        $(document).on('click', '#update_email', function(e) {
            var button_name_u_email = $("#button_name_u_email").val();
            var url_u_email = $("#url_u_email").val();
            var app_name_u_email = $("#app_name_u_email").val();
            var email_type = $('input[name="email_type"]:checked').val();
            var validate_flag_email_u = 0;

            if (button_name_u_email == '') {
                validate_flag_email_u = 1;
                $("#button_name_add_email_error").text("Button Name is Required");
            } else {
                validate_flag_email_u = 0;
                $("#button_name_add_email_error").text("");
            }

            if (email_type == 'App') {
                if (app_name_u_email == '') {
                    validate_flag_email_u = 1;
                    $("#app_name_u_email_error").text("App Name is required");
                } else {
                    validate_flag_email_u = 0;
                    $("#app_name_u_email_error").text("");
                }
            } else {
                if (url_u_email == '') {
                    validate_flag_email_u = 1;
                    $("#url_u_email_error").text("Url is required");
                } else {
                    if (validURL(url_u_email)) {
                        validate_flag_email_u = 0;
                        $("#url_u_email_error").text("");
                    } else {
                        validate_flag_email_u = 1;
                        $("#url_u_email_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#update_form_email").serialize();
            console.log(data);
            if (validate_flag_email_u != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "edit_email",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#editemailmyModal').modal('hide');
                        //alert("Email Updated successfully");
                        location.reload();
                    }
                });
            }

        });
        $(document).on("click", ".delete", function() {
            var id = $(this).attr("data-id");
            $('#id_d_email').val(id);

        });
        $(document).on("click", "#delete_email", function() {
            $.ajax({
                url: "delete_email",
                type: "POST",
                cache: false,
                data: {
                    type: 3,
                    id: $("#id_d_email").val()
                },
                success: function(dataResult) {
                    location.reload();
                    $('#deleteemailmyModal').modal('hide');
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).on('click', '#btn-add-social', function(e) {
            var button_name_add_social = $("#button_name_add_social").val();
            var url_add_social = $("#url_add_social").val();
            var appname_add_social = $("#appname_add_social").val();
            var social_type = $('input[name="social_type"]:checked').val();
            var validate_flag_social = 0;

            if (button_name_add_social == '') {
                validate_flag_social = 1;
                $("#button_name_add_social_error").text("Button Name is Required");
            } else {
                validate_flag_social = 0;
                $("#button_name_add_social_error").text("");
            }

            if (social_type == 'App') {
                if (appname_add_social == '') {
                    validate_flag_social = 1;
                    $("#appname_add_social_error").text("App Name is required");
                } else {
                    validate_flag = 0;
                    $("#appname_add_social_error").text("");
                }
            } else {
                if (url_add_social == '') {
                    validate_flag_social = 1;
                    $("#url_add_social_error").text("Url is required");
                } else {
                    if (validURL(url_add_social)) {
                        validate_flag_social = 0;
                        $("#url_add_social_error").text("");
                    } else {
                        validate_flag_social = 1;
                        $("#url_add_social_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#socialform").serialize();
            console.log(data);
            if (validate_flag_social != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "add_new_social",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult);
                        $('#addsocialmyModal').modal('hide');
                        // alert("social added successfully");
                        location.reload();

                    }
                });
            }
        });
        $(document).on('click', '.update', function(e) {
            var socialid = $(this).attr("data-socialid");
            var socialtype = $(this).attr("data-socialtype");
            var url = $(this).attr("data-url");
            var app_name = $(this).attr("data-app_name");
            var button_name = $(this).attr("data-button_name");
            $('#id_u_social').val(socialid);
            $('#socialtype_u').find(':radio[name=social_type][value="' + socialtype + '"]').prop('checked', true);
            $('#url_u_social').val(url);
            $('#app_name_u_social').val(app_name);
            $('#button_name_u_social').val(button_name);
            if (socialtype == 'App') {
                $("#Website_u").hide();
                $("#App_u").show();
            } else {
                $("#Website_u").show();
                $("#App_u").hide();
            }
        });

        $(document).on('click', '#update_social', function(e) {
            var button_name_u_social = $("#button_name_u_social").val();
            var url_u_social = $("#url_u_social").val();
            var app_name_u_social = $("#app_name_u_social").val();
            var social_type = $('input[name="social_type"]:checked').val();
            var validate_flag_social_u = 0;

            if (button_name_u_social == '') {
                validate_flag_social_u = 1;
                $("#button_name_add_social_error").text("Button Name is Required");
            } else {
                validate_flag_social_u = 0;
                $("#button_name_add_social_error").text("");
            }

            if (social_type == 'App') {
                if (app_name_u_social == '') {
                    validate_flag_social_u = 1;
                    $("#app_name_u_social_error").text("App Name is required");
                } else {
                    validate_flag_social_u = 0;
                    $("#app_name_u_social_error").text("");
                }
            } else {
                if (url_u_social == '') {
                    validate_flag_social_u = 1;
                    $("#url_u_social_error").text("Url is required");
                } else {
                    if (validURL(url_u_social)) {
                        validate_flag_social_u = 0;
                        $("#url_u_social_error").text("");
                    } else {
                        validate_flag_social_u = 1;
                        $("#url_u_social_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#update_form_social").serialize();
            console.log(data);
            if (validate_flag_social_u != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "edit_social",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#editsocialmyModal').modal('hide');
                        //alert("social Updated successfully");
                        location.reload();
                    }
                });
            }

        });
        $(document).on("click", ".delete", function() {
            var id = $(this).attr("data-id");
            $('#id_d_social').val(id);

        });
        $(document).on("click", "#delete_social", function() {
            $.ajax({
                url: "delete_social",
                type: "POST",
                cache: false,
                data: {
                    type: 3,
                    id: $("#id_d_social").val()
                },
                success: function(dataResult) {
                    location.reload();
                    $('#deletesocialmyModal').modal('hide');
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).on('click', '#btn-add-transportation', function(e) {
            var button_name_add_transportation = $("#button_name_add_transportation").val();
            var url_add_transportation = $("#url_add_transportation").val();
            var appname_add_transportation = $("#appname_add_transportation").val();
            var transportation_type = $('input[name="transportation_type"]:checked').val();
            var validate_flag_transportation = 0;

            if (button_name_add_transportation == '') {
                validate_flag_transportation = 1;
                $("#button_name_add_transportation_error").text("Button Name is Required");
            } else {
                validate_flag_transportation = 0;
                $("#button_name_add_transportation_error").text("");
            }

            if (transportation_type == 'App') {
                if (appname_add_transportation == '') {
                    validate_flag_transportation = 1;
                    $("#appname_add_transportation_error").text("App Name is required");
                } else {
                    validate_flag_transportation = 0;
                    $("#appname_add_transportation_error").text("");
                }
            } else {
                if (url_add_transportation == '') {
                    validate_flag_transportation = 1;
                    $("#url_add_transportation_error").text("Url is required");
                } else {
                    if (validURL(url_add_transportation)) {
                        validate_flag_transportation = 0;
                        $("#url_add_transportation_error").text("");
                    } else {
                        validate_flag_transportation = 1;
                        $("#url_add_transportation_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#transportationform").serialize();
            if (validate_flag_transportation != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "add_transportation",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#addtransportationmyModal').modal('hide');
                        //alert("transportation added successfully");
                        location.reload();

                    }
                });
            }

        });
        $(document).on('click', '.update', function(e) {
            var transportationid = $(this).attr("data-transportationid");
            var transportationtype = $(this).attr("data-transportationtype");
            var url = $(this).attr("data-url");
            var app_name = $(this).attr("data-app_name");
            var button_name = $(this).attr("data-button_name");
            $('#id_u_transportation').val(transportationid);
            $('#transportationtype_u').find(':radio[name=transportation_type][value="' + transportationtype + '"]').prop('checked', true);
            $('#url_u_transportation').val(url);
            $('#app_name_u_transportation').val(app_name);
            $('#button_name_u_transportation').val(button_name);
            if (transportationtype == 'App') {
                $("#Website_u").hide();
                $("#App_u").show();
            } else {
                $("#Website_u").show();
                $("#App_u").hide();
            }
        });

        $(document).on('click', '#update_transportation', function(e) {
            var button_name_u_transportation = $("#button_name_u_transportation").val();
            var url_u_transportation = $("#url_u_transportation").val();
            var app_name_u_transportation = $("#app_name_u_transportation").val();
            var transportation_type = $('input[name="transportation_type"]:checked').val();
            var validate_flag_transportation_u = 0;

            if (button_name_u_transportation == '') {
                validate_flag_transportation_u = 1;
                $("#button_name_u_transportation_error").text("Button Name is Required");
            } else {
                validate_flag_transportation_u = 0;
                $("#button_name_u_transportation_error").text("");
            }

            if (transportation_type == 'App') {
                if (app_name_u_transportation == '') {
                    validate_flag_transportation_u = 1;
                    $("#app_name_u_transportation_error").text("App Name is required");
                } else {
                    validate_flag_transportation_u = 0;
                    $("#app_name_u_transportation_error").text("");
                }
            } else {
                if (url_u_transportation == '') {
                    validate_flag_transportation_u = 1;
                    $("#url_u_transportation_error").text("Url is required");
                } else {
                    if (validURL(url_u_transportation)) {
                        validate_flag_transportation_u = 0;
                        $("#url_u_transportation_error").text("");
                    } else {
                        validate_flag_transportation_u = 1;
                        $("#url_u_transportation_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#update_form_transportation").serialize();
            console.log(data);
            if (validate_flag_transportation_u != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "edit_transportation",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#edittransportationmyModal').modal('hide');
                        // alert("transportation Updated successfully");
                        location.reload();
                    }
                });
            }

        });
        $(document).on("click", ".delete", function() {
            var id = $(this).attr("data-id");
            $('#id_d_transportation').val(id);

        });
        $(document).on("click", "#delete_transportation", function() {
            $.ajax({
                url: "delete_transportation",
                type: "POST",
                cache: false,
                data: {
                    type: 3,
                    id: $("#id_d_transportation").val()
                },
                success: function(dataResult) {
                    location.reload();
                    $('#deletetransportationmyModal').modal('hide');
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).on('click', '#btn-add-pharmacy', function(e) {
            var button_name_add_pharmacy = $("#button_name_add_pharmacy").val();
            var url_add_pharmacy = $("#url_add_pharmacy").val();
            var appname_add_pharmacy = $("#appname_add_pharmacy").val();
            var pharmacy_type = $('input[name="pharmacy_type"]:checked').val();
            var validate_flag_pharmacy = 0;

            if (button_name_add_pharmacy == '') {
                validate_flag_pharmacy = 1;
                $("#button_name_add_pharmacy_error").text("Button Name is Required");
            } else {
                validate_flag_pharmacy = 0;
                $("#button_name_add_pharmacy_error").text("");
            }

            if (pharmacy_type == 'App') {
                if (appname_add_pharmacy == '') {
                    validate_flag_pharmacy = 1;
                    $("#appname_add_pharmacy_error").text("App Name is required");
                } else {
                    validate_flag_pharmacy = 0;
                    $("#appname_add_pharmacy_error").text("");
                }
            } else {
                if (url_add_pharmacy == '') {
                    validate_flag_pharmacy = 1;
                    $("#url_add_pharmacy_error").text("Url is required");
                } else {
                    if (validURL(url_add_pharmacy)) {
                        validate_flag_pharmacy = 0;
                        $("#url_add_pharmacy_error").text("");
                    } else {
                        validate_flag_pharmacy = 1;
                        $("#url_add_pharmacy_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#pharmacyform").serialize();

            if (validate_flag_pharmacy != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "add_pharmacy",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#addpharmacymyModal').modal('hide');
                        //  alert("pharmacy added successfully");
                        location.reload();

                    }
                });
            }

        });
        $(document).on('click', '.update', function(e) {
            var pharmacyid = $(this).attr("data-pharmacyid");
            var pharmacytype = $(this).attr("data-pharmacytype");
            var url = $(this).attr("data-url");
            var app_name = $(this).attr("data-app_name");
            var button_name = $(this).attr("data-button_name");
            $('#id_u_pharmacy').val(pharmacyid);
            $('#pharmacytype_u').find(':radio[name=pharmacy_type][value="' + pharmacytype + '"]').prop('checked', true);
            $('#url_u_pharmacy').val(url);
            $('#app_name_u_pharmacy').val(app_name);
            $('#button_name_u_pharmacy').val(button_name);
            if (pharmacytype == 'App') {
                $("#Website_u").hide();
                $("#App_u").show();
            } else {
                $("#Website_u").show();
                $("#App_u").hide();
            }
        });

        $(document).on('click', '#update_pharmacy', function(e) {
            var button_name_u_pharmacy = $("#button_name_u_pharmacy").val();
            var url_u_pharmacy = $("#url_u_pharmacy").val();
            var app_name_u_pharmacy = $("#app_name_u_pharmacy").val();
            var pharmacy_type = $('input[name="pharmacy_type"]:checked').val();
            var validate_flag_pharmacy_u = 0;

            if (button_name_u_pharmacy == '') {
                validate_flag_pharmacy_u = 1;
                $("#button_name_u_pharmacy_error").text("Button Name is Required");
            } else {
                validate_flag_pharmacy_u = 0;
                $("#button_name_u_pharmacy_error").text("");
            }

            if (pharmacy_type == 'App') {
                if (app_name_u_pharmacy == '') {
                    validate_flag_pharmacy_u = 1;
                    $("#app_name_u_pharmacy_error").text("App Name is required");
                } else {
                    validate_flag_pharmacy_u = 0;
                    $("#app_name_u_pharmacy_error").text("");
                }
            } else {
                if (url_u_pharmacy == '') {
                    validate_flag_pharmacy_u = 1;
                    $("#url_u_pharmacy_error").text("Url is required");
                } else {
                    if (validURL(url_u_pharmacy)) {
                        validate_flag_pharmacy_u = 0;
                        $("#url_u_pharmacy_error").text("");
                    } else {
                        validate_flag_pharmacy_u = 1;
                        $("#url_u_pharmacy_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#update_form_pharmacy").serialize();
            console.log(data);
            if (validate_flag_pharmacy_u != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "edit_pharmacy",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#editpharmacymyModal').modal('hide');
                        //alert("pharmacy Updated successfully");
                        location.reload();
                    }
                });
            }

        });
        $(document).on("click", ".delete", function() {
            var id = $(this).attr("data-id");
            $('#id_d_pharmacy').val(id);

        });
        $(document).on("click", "#delete_pharmacy", function() {
            $.ajax({
                url: "delete_pharmacy",
                type: "POST",
                cache: false,
                data: {
                    type: 3,
                    id: $("#id_d_pharmacy").val()
                },
                success: function(dataResult) {
                    location.reload();
                    $('#deletepharmacymyModal').modal('hide');
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).on('click', '#btn-add-Movie', function(e) {
            var button_name_add_Movie = $("#button_name_add_Movie").val();
            var url_add_Movie = $("#url_add_Movie").val();
            var appname_add_Movie = $("#appname_add_Movie").val();
            var Movie_type = $('input[name="Movie_type"]:checked').val();
            var validate_flag_Movie = 0;

            if (button_name_add_Movie == '') {
                validate_flag_Movie = 1;
                $("#button_name_add_Movie_error").text("Button Name is Required");
            } else {
                validate_flag_Movie = 0;
                $("#button_name_add_Movie_error").text("");
            }

            if (Movie_type == 'App') {
                if (appname_add_Movie == '') {
                    validate_flag_Movie = 1;
                    $("#appname_add_Movie_error").text("App Name is required");
                } else {
                    validate_flag_Movie = 0;
                    $("#appname_add_Movie_error").text("");
                }
            } else {
                if (url_add_Movie == '') {
                    validate_flag_Movie = 1;
                    $("#url_add_Movie_error").text("Url is required");
                } else {
                    if (validURL(url_add_Movie)) {
                        validate_flag_Movie = 0;
                        $("#url_add_Movie_error").text("");
                    } else {
                        validate_flag_Movie = 1;
                        $("#url_add_Movie_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#Movieform").serialize();
            if (validate_flag_Movie != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "add_movie",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#addMoviemyModal').modal('hide');
                        //alert("Movie added successfully");
                        location.reload();

                    }
                });
            }

        });
        $(document).on('click', '.update', function(e) {
            var Movieid = $(this).attr("data-Movieid");
            var Movietype = $(this).attr("data-Movietype");
            var url = $(this).attr("data-url");
            var app_name = $(this).attr("data-app_name");
            var button_name = $(this).attr("data-button_name");
            $('#id_u_Movie').val(Movieid);
            $('#Movietype_u').find(':radio[name=Movie_type][value="' + Movietype + '"]').prop('checked', true);
            $('#url_u_Movie').val(url);
            $('#app_name_u_Movie').val(app_name);
            $('#button_name_u_Movie').val(button_name);
            if (Movietype == 'App') {
                $("#Website_u").hide();
                $("#App_u").show();
            } else {
                $("#Website_u").show();
                $("#App_u").hide();
            }
        });

        $(document).on('click', '#update_Movie', function(e) {
            var button_name_u_Movie = $("#button_name_u_Movie").val();
            var url_u_Movie = $("#url_u_Movie").val();
            var app_name_u_Movie = $("#app_name_u_Movie").val();
            var Movie_type = $('input[name="Movie_type"]:checked').val();
            var validate_flag_Movie_u = 0;

            if (button_name_u_Movie == '') {
                validate_flag_Movie_u = 1;
                $("#button_name_u_Movie_error").text("Button Name is Required");
            } else {
                validate_flag_Movie_u = 0;
                $("#button_name_u_Movie_error").text("");
            }

            if (Movie_type == 'App') {
                if (app_name_u_Movie == '') {
                    validate_flag_Movie_u = 1;
                    $("#app_name_u_Movie_error").text("App Name is required");
                } else {
                    validate_flag_Movie_u = 0;
                    $("#app_name_u_Movie_error").text("");
                }
            } else {
                if (url_u_Movie == '') {
                    validate_flag_Movie_u = 1;
                    $("#url_u_Movie_error").text("Url is required");
                } else {
                    if (validURL(url_u_Movie)) {
                        validate_flag_Movie_u = 0;
                        $("#url_u_Movie_error").text("");
                    } else {
                        validate_flag_Movie_u = 1;
                        $("#url_u_Movie_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#update_form_Movie").serialize();
            console.log(data);
            if (validate_flag_Movie_u != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "edit_movie",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#editMoviemyModal').modal('hide');
                        //alert("Movie Updated successfully");
                        location.reload();
                    }
                });
            }

        });
        $(document).on("click", ".delete", function() {
            var id = $(this).attr("data-id");
            $('#id_d_Movie').val(id);

        });
        $(document).on("click", "#delete_Movie", function() {
            $.ajax({
                url: "delete_movie",
                type: "POST",
                cache: false,
                data: {
                    type: 3,
                    id: $("#id_d_Movie").val()
                },
                success: function(dataResult) {
                    location.reload();
                    $('#deleteMoviemyModal').modal('hide');
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).on('click', '#btn-add-music', function(e) {
            var button_name_add_music = $("#button_name_add_music").val();
            var url_add_music = $("#url_add_music").val();
            var appname_add_music = $("#appname_add_music").val();
            var music_type = $('input[name="music_type"]:checked').val();
            var validate_flag_music = 0;

            if (button_name_add_music == '') {
                validate_flag_music = 1;
                $("#button_name_add_music_error").text("Button Name is Required");
            } else {
                validate_flag_music = 0;
                $("#button_name_add_music_error").text("");
            }

            if (music_type == 'App') {
                if (appname_add_music == '') {
                    validate_flag_music = 1;
                    $("#appname_add_music_error").text("App Name is required");
                } else {
                    validate_flag_music = 0;
                    $("#appname_add_music_error").text("");
                }
            } else {
                if (url_add_music == '') {
                    validate_flag_music = 1;
                    $("#url_add_music_error").text("Url is required");
                } else {
                    if (validURL(url_add_music)) {
                        validate_flag_music = 0;
                        $("#url_add_music_error").text("");
                    } else {
                        validate_flag_music = 1;
                        $("#url_add_music_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#musicform").serialize();
            if (validate_flag_music != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "add_music",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        //console.log(dataResult.msg);
                        $('#addmusicmyModal').modal('hide');
                        //alert(dataResult.msg);
                        location.reload();

                    }
                });
            }

        });
        $(document).on('click', '.update', function(e) {
            var musicid = $(this).attr("data-musicid");
            var musictype = $(this).attr("data-musictype");
            var url = $(this).attr("data-url");
            var app_name = $(this).attr("data-app_name");
            var button_name = $(this).attr("data-button_name");
            $('#id_u_music').val(musicid);
            $('#musictype_u').find(':radio[name=music_type][value="' + musictype + '"]').prop('checked', true);
            $('#url_u_music').val(url);
            $('#app_name_u_music').val(app_name);
            $('#button_name_u_music').val(button_name);
            if (musictype == 'App') {
                $("#Website_u").hide();
                $("#App_u").show();
            } else {
                $("#Website_u").show();
                $("#App_u").hide();
            }
        });

        $(document).on('click', '#update_music', function(e) {
            var button_name_u_music = $("#button_name_u_music").val();
            var url_u_music = $("#url_u_music").val();
            var app_name_u_music = $("#app_name_u_music").val();
            var music_type = $('input[name="music_type"]:checked').val();
            var validate_flag_music_u = 0;

            if (button_name_u_music == '') {
                validate_flag_music_u = 1;
                $("#button_name_u_music_error").text("Button Name is Required");
            } else {
                validate_flag_music_u = 0;
                $("#button_name_u_music_error").text("");
            }

            if (music_type == 'App') {
                if (app_name_u_music == '') {
                    validate_flag_music_u = 1;
                    $("#app_name_u_music_error").text("App Name is required");
                } else {
                    validate_flag_music_u = 0;
                    $("#app_name_u_music_error").text("");
                }
            } else {
                if (url_u_music == '') {
                    validate_flag_music_u = 1;
                    $("#url_u_music_error").text("Url is required");
                } else {
                    if (validURL(url_u_music)) {
                        validate_flag_music_u = 0;
                        $("#url_u_music_error").text("");
                    } else {
                        validate_flag_music_u = 1;
                        $("#url_u_music_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#update_form_music").serialize();
            console.log(data);
            if (validate_flag_music_u != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "edit_music",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        // console.log(dataResult.msg);
                        $('#editmusicmyModal').modal('hide');
                        //alert(dataResult.msg);
                        location.reload();
                    }
                });
            }

        });
        $(document).on("click", ".delete", function() {
            var id = $(this).attr("data-id");
            $('#id_d_music').val(id);

        });
        $(document).on("click", "#delete_music", function() {
            $.ajax({
                url: "delete_music",
                type: "POST",
                cache: false,
                data: {
                    type: 3,
                    id: $("#id_d_music").val()
                },
                success: function(dataResult) {
                    location.reload();
                    $('#deletemusicmyModal').modal('hide');
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).on('click', '#btn-add-news', function(e) {
            var button_name_add_news = $("#button_name_add_news").val();
            var url_add_news = $("#url_add_news").val();
            var appname_add_news = $("#appname_add_news").val();
            var news_type = $('input[name="news_type"]:checked').val();
            var validate_flag_news = 0;

            if (button_name_add_news == '') {
                validate_flag_news = 1;
                $("#button_name_add_news_error").text("Button Name is Required");
            } else {
                validate_flag_news = 0;
                $("#button_name_add_news_error").text("");
            }

            if (news_type == 'App') {
                if (appname_add_news == '') {
                    validate_flag_news = 1;
                    $("#appname_add_news_error").text("App Name is required");
                } else {
                    validate_flag_news = 0;
                    $("#appname_add_news_error").text("");
                }
            } else {
                if (url_add_news == '') {
                    validate_flag_news = 1;
                    $("#url_add_news_error").text("Url is required");
                } else {
                    if (validURL(url_add_news)) {
                        validate_flag_news = 0;
                        $("#url_add_news_error").text("");
                    } else {
                        validate_flag_news = 1;
                        $("#url_add_news_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#newsform").serialize();
            if (validate_flag_news != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "add_news",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#addnewsmyModal').modal('hide');
                        //alert(dataResult.msg);
                        location.reload();

                    }
                });
            }
        });
        $(document).on('click', '.update', function(e) {
            var newsid = $(this).attr("data-newsid");
            var newstype = $(this).attr("data-newstype");
            var url = $(this).attr("data-url");
            var app_name = $(this).attr("data-app_name");
            var button_name = $(this).attr("data-button_name");
            $('#id_u_news').val(newsid);
            $('#newstype_u').find(':radio[name=news_type][value="' + newstype + '"]').prop('checked', true);
            $('#url_u_news').val(url);
            $('#app_name_u_news').val(app_name);
            $('#button_name_u_news').val(button_name);
            if (newstype == 'App') {
                $("#Website_u").hide();
                $("#App_u").show();
            } else {
                $("#Website_u").show();
                $("#App_u").hide();
            }
        });

        $(document).on('click', '#update_news', function(e) {
            var button_name_u_news = $("#button_name_u_news").val();
            var url_u_news = $("#url_u_news").val();
            var app_name_u_news = $("#app_name_u_news").val();
            var news_type = $('input[name="news_type"]:checked').val();
            var validate_flag_news_u = 0;

            if (button_name_u_news == '') {
                validate_flag_news_u = 1;
                $("#button_name_u_news_error").text("Button Name is Required");
            } else {
                validate_flag_news_u = 0;
                $("#button_name_u_news_error").text("");
            }

            if (news_type == 'App') {
                if (app_name_u_news == '') {
                    validate_flag_news_u = 1;
                    $("#app_name_u_news_error").text("App Name is required");
                } else {
                    validate_flag_news_u = 0;
                    $("#app_name_u_news_error").text("");
                }
            } else {
                if (url_u_news == '') {
                    validate_flag_news_u = 1;
                    $("#url_u_news_error").text("Url is required");
                } else {
                    if (validURL(url_u_news)) {
                        validate_flag_news_u = 0;
                        $("#url_u_news_error").text("");
                    } else {
                        validate_flag_news_u = 1;
                        $("#url_u_news_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#update_form_news").serialize();
            console.log(data);
            if (validate_flag_news_u != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "edit_news",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#editnewsmyModal').modal('hide');
                        //alert(dataResult.msg);
                        location.reload();
                    }
                });
            }

        });
        $(document).on("click", ".delete", function() {
            var id = $(this).attr("data-id");
            $('#id_d_news').val(id);

        });
        $(document).on("click", "#delete_news", function() {
            $.ajax({
                url: "delete_news",
                type: "POST",
                cache: false,
                data: {
                    type: 3,
                    id: $("#id_d_news").val()
                },
                success: function(dataResult) {
                    location.reload();
                    $('#deletenewsmyModal').modal('hide');
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).on('click', '#btn-add-weather', function(e) {
            var button_name_add_weather = $("#button_name_add_weather").val();
            var url_add_weather = $("#url_add_weather").val();
            var appname_add_weather = $("#appname_add_weather").val();
            var weather_type = $('input[name="weather_type"]:checked').val();
            var validate_flag_weather = 0;

            if (button_name_add_weather == '') {
                validate_flag_weather = 1;
                $("#button_name_add_weather_error").text("Button Name is Required");
            } else {
                validate_flag_weather = 0;
                $("#button_name_add_weather_error").text("");
            }

            if (weather_type == 'App') {
                if (appname_add_weather == '') {
                    validate_flag_weather = 1;
                    $("#appname_add_weather_error").text("App Name is required");
                } else {
                    validate_flag_weather = 0;
                    $("#appname_add_weather_error").text("");
                }
            } else {
                if (url_add_weather == '') {
                    validate_flag_weather = 1;
                    $("#url_add_weather_error").text("Url is required");
                } else {
                    if (validURL(url_add_weather)) {
                        validate_flag_weather = 0;
                        $("#url_add_weather_error").text("");
                    } else {
                        validate_flag_weather = 1;
                        $("#url_add_weather_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#weatherform").serialize();
            if (validate_flag_weather != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "add_weather",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#addweathermyModal').modal('hide');
                        //alert(dataResult.msg);
                        location.reload();

                    }
                });
            }

        });
        $(document).on('click', '.update', function(e) {
            var weatherid = $(this).attr("data-weatherid");
            var weathertype = $(this).attr("data-weathertype");
            var url = $(this).attr("data-url");
            var app_name = $(this).attr("data-app_name");
            var button_name = $(this).attr("data-button_name");
            $('#id_u_weather').val(weatherid);
            $('#weathertype_u').find(':radio[name=weather_type][value="' + weathertype + '"]').prop('checked', true);
            $('#url_u_weather').val(url);
            $('#app_name_u_weather').val(app_name);
            $('#button_name_u_weather').val(button_name);
            if (weathertype == 'App') {
                $("#Website_u").hide();
                $("#App_u").show();
            } else {
                $("#Website_u").show();
                $("#App_u").hide();
            }
        });

        $(document).on('click', '#update_weather', function(e) {
            var button_name_u_weather = $("#button_name_u_weather").val();
            var url_u_weather = $("#url_u_weather").val();
            var app_name_u_weather = $("#app_name_u_weather").val();
            var weather_type = $('input[name="weather_type"]:checked').val();
            var validate_flag_weather_u = 0;

            if (button_name_u_weather == '') {
                validate_flag_weather_u = 1;
                $("#button_name_u_weather_error").text("Button Name is Required");
            } else {
                validate_flag_weather_u = 0;
                $("#button_name_u_weather_error").text("");
            }

            if (weather_type == 'App') {
                if (app_name_u_weather == '') {
                    validate_flag_weather_u = 1;
                    $("#app_name_u_weather_error").text("App Name is required");
                } else {
                    validate_flag_weather_u = 0;
                    $("#app_name_u_weather_error").text("");
                }
            } else {
                if (url_u_weather == '') {
                    validate_flag_weather_u = 1;
                    $("#url_u_weather_error").text("Url is required");
                } else {
                    if (validURL(url_u_weather)) {
                        validate_flag_weather_u = 0;
                        $("#url_u_weather_error").text("");
                    } else {
                        validate_flag_weather_u = 1;
                        $("#url_u_weather_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#update_form_weather").serialize();
            console.log(data);
            if (validate_flag_weather_u != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "edit_weather",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#editweathermyModal').modal('hide');
                        //alert(dataResult.msg);
                        location.reload();
                    }
                });
            }

        });
        $(document).on("click", ".delete", function() {
            var id = $(this).attr("data-id");
            $('#id_d_weather').val(id);

        });
        $(document).on("click", "#delete_weather", function() {
            $.ajax({
                url: "delete_weather",
                type: "POST",
                cache: false,
                data: {
                    type: 3,
                    id: $("#id_d_weather").val()
                },
                success: function(dataResult) {
                    location.reload();
                    $('#deleteweathermyModal').modal('hide');
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).on('click', '#btn-add-internet', function(e) {
            var button_name_add_internet = $("#button_name_add_internet").val();
            var url_add_internet = $("#url_add_internet").val();
            var appname_add_internet = $("#appname_add_internet").val();
            var internet_type = $('input[name="internet_type"]:checked').val();
            var validate_flag_internet = 0;

            if (button_name_add_internet == '') {
                validate_flag_internet = 1;
                $("#button_name_add_internet_error").text("Button Name is Required");
            } else {
                validate_flag_internet = 0;
                $("#button_name_add_internet_error").text("");
            }

            if (internet_type == 'App') {
                if (appname_add_internet == '') {
                    validate_flag_internet = 1;
                    $("#appname_add_internet_error").text("App Name is required");
                } else {
                    validate_flag_internet = 0;
                    $("#appname_add_internet_error").text("");
                }
            } else {
                if (url_add_internet == '') {
                    validate_flag_internet = 1;
                    $("#url_add_internet_error").text("Url is required");
                } else {
                    if (validURL(url_add_internet)) {
                        validate_flag_internet = 0;
                        $("#url_add_internet_error").text("");
                    } else {
                        validate_flag_internet = 1;
                        $("#url_add_internet_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#internetform").serialize();
            if (validate_flag_internet != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "add_internet",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#addinternetmyModal').modal('hide');
                        //alert(dataResult.msg);
                        location.reload();

                    }
                });
            }

        });
        $(document).on('click', '.update', function(e) {
            var internetid = $(this).attr("data-internetid");
            var internettype = $(this).attr("data-internettype");
            var url = $(this).attr("data-url");
            var app_name = $(this).attr("data-app_name");
            var button_name = $(this).attr("data-button_name");
            $('#id_u_internet').val(internetid);
            $('#internettype_u').find(':radio[name=internet_type][value="' + internettype + '"]').prop('checked', true);
            $('#url_u_internet').val(url);
            $('#app_name_u_internet').val(app_name);
            $('#button_name_u_internet').val(button_name);
            if (internettype == 'App') {
                $("#Website_u").hide();
                $("#App_u").show();
            } else {
                $("#Website_u").show();
                $("#App_u").hide();
            }
        });

        $(document).on('click', '#update_internet', function(e) {
            var button_name_u_internet = $("#button_name_u_internet").val();
            var url_u_internet = $("#url_u_internet").val();
            var app_name_u_internet = $("#app_name_u_internet").val();
            var internet_type = $('input[name="internet_type"]:checked').val();
            var validate_flag_internet_u = 0;

            if (button_name_u_internet == '') {
                validate_flag_internet_u = 1;
                $("#button_name_u_internet_error").text("Button Name is Required");
                exit();
            } else {
                validate_flag_internet_u = 0;
                $("#button_name_u_internet_error").text("");
            }

            if (internet_type == 'App') {
                if (app_name_u_internet == '') {
                    validate_flag_internet_u = 1;
                    $("#app_name_u_internet_error").text("App Name is required");
                } else {
                    validate_flag_internet_u = 0;
                    $("#app_name_u_internet_error").text("");
                }
            } else {
                if (url_u_internet == '') {
                    validate_flag_internet_u = 1;
                    $("#url_u_internet_error").text("Url is required");
                } else {
                    if (validURL(url_u_internet)) {
                        validate_flag_internet_u = 0;
                        $("#url_u_internet_error").text("");
                    } else {
                        validate_flag_internet_u = 1;
                        $("#url_u_internet_error").text("Url is not vaild");
                    }
                }
            }
            var data = $("#update_form_internet").serialize();
            console.log(data);
            if (validate_flag_internet_u != 1) {
                $.ajax({
                    data: data,
                    type: "post",
                    url: "edit_internet",
                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        console.log(dataResult.msg);
                        $('#editinternetmyModal').modal('hide');
                        //alert(dataResult.msg);
                        location.reload();
                    }
                });
            }

        });
        $(document).on("click", ".delete", function() {
            var id = $(this).attr("data-id");
            $('#id_d_internet').val(id);

        });
        $(document).on("click", "#delete_internet", function() {
            $.ajax({
                url: "delete_internet",
                type: "POST",
                cache: false,
                data: {
                    type: 3,
                    id: $("#id_d_internet").val()
                },
                success: function(dataResult) {
                    location.reload();
                    $('#deleteinternetmyModal').modal('hide');
                }
            });
        });
    </script>

    <script type="text/javascript">
        function validURL(str) {
            var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
                '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
                '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
                '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
                '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
                '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
            return !!pattern.test(str);
        }
    </script>

</body>

</html>
