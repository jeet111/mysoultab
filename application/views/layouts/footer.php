



<script src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url();?>assets/styles/bootstrap-4.1.2/popper.js"></script>
<!-- <script src="assets/styles/bootstrap-4.1.2/bootstrap.min.js"></script> -->
<script src="<?php echo base_url();?>assets/plugins/greensock/TweenMax.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/greensock/TimelineMax.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/greensock/animation.gsap.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/greensock/ScrollToPlugin.min.js"></script>
<!-- <script src="assets/plugins/OwlCarousel2-2.3.4/owl.carousel.js"></script> -->
<script src="<?php echo base_url();?>assets/plugins/easing/easing.js"></script>
<script src="<?php echo base_url();?>assets/plugins/progressbar/progressbar.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/parallax-js-master/parallax.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-datepicker/jquery-ui.js"></script>
<script src="<?php echo base_url();?>assets/plugins/colorbox/jquery.colorbox-min.js"></script>

<script src="<?php echo base_url();?>assets/js/custom.js"></script>






<!-- <script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script> -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
      	scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
<script type="text/javascript">
  $(function() {
    $("#resestpass").validate({
            // Specify validation rules
            rules: {
              new_password: {
                required: true,
                minlength: 6,
              },
              confirm_password: {
                required: true,
                minlength: 6,
                equalTo : "#new_password",
              },
            },
            // Specify validation error messages
            messages: {
              //email: "Enter your email"

              new_password: {
                required: "Enter a new password for your account",
                minlength: "Password must be at least 6 characters",
              },
              // new_password: {
              //   required: "Enter a new password for your account",
              //   email: "Password must be at least 6 characters",
              // },
              confirm_password: {
                required: "Please enter your confirm password.",
                equalTo: "Not match your confirm password.",
              },
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
              form.submit();
            }
          });
  });
</script>

</body>
</html>