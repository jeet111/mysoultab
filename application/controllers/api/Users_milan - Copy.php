<?php
defined('BASEPATH') or exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . 'libraries/twilio-php-master/Twilio/autoload.php';

use League\Csv\Reader;
use Twilio\Rest\Client;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VoiceGrant;
use Twilio\Jwt\Grants\VideoGrant;

class Users_milan extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library("security");
		$this->load->helper('array');
		/*For Language*/
		if ($_POST) {
			///$this->param = $_POST;
			$this->param = $this->security->xss_clean($_POST);
		} else {
			//$this->param = json_decode(file_get_contents("php://input"),true); 
			$this->param = $this->security->xss_clean(json_decode(file_get_contents("php://input"), true));
		}
		//$userData = $this->param['user_language'];
		// $GLOBALS['user_lang'] = $userData;
	}
	/*___________________________________ -START FROM HEAR-_____________________________*/
	/* User Login */
	public function login_post()
	{
		//$pdata = file_get_contents("php://input");
		//$data = json_decode($pdata, true);
		//$pdata = file_get_contents("php://input");
		//$data = json_decode($pdata, true);
		$data = $this->param;
		//echo $data['password'];
		//print_r($data);
		//die;
		$required_parameter = array('email', 'password', 'device_type', 'user_roll');
		//$required_parameter = array('email','password','device_id','device_type','user_roll');
		$chk_error = check_required_value($required_parameter, $data);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		} else {
			$email = (!empty($data['email']) ? $data['email'] : '');
			$password = (!empty($data['password']) ? md5($data['password']) : '');
			$user_roll = (!empty($data['user_roll']) ? $data['user_roll'] : '');
			if ($this->isValidEmail($email)) {
				$check_email = $this->common_model->getRecordCount('cp_users', array('email' => $email));
				$check_email_validation = $this->common_model->getsingle('cp_users', array('email' => $email));
				if ($check_email > 0) {
					if ($check_email_validation->email == $data['email']) {
						if ($check_email_validation->password == md5($data['password'])) {
							$conditions = array('email' => $email, 'password' => $password, 'user_role' => $user_roll);
							$conditions1 = array('email' => $email, 'password' => $password, 'status' => 1, 'user_role' => $user_roll);
							$users = $this->common_model->getSingleRecordById('cp_users', $conditions);
							if (!empty($users)) {
								$condition = array('id' => $users['id']);
								$device_type = isset($data['device_type']) ? $data['device_type'] : '';
								//$device_id   = isset($data['device_id']) ? $data['device_id'] : '';
								$updateArr = array('device_type' => $device_type);
								//$updateArr = array('device_type'=>$device_type,'device_id'=>$device_id);
								$update_login = $this->common_model->updateRecords('cp_users', $updateArr, $condition);
								$users1 = $this->common_model->getSingleRecordById('cp_users', $conditions1);
								//if(!empty($users1)){
								$user_details = array(
									'id' => $users['id'],
									'name' => $users['name'],
									'lastname' => $users['lastname'],
									'dob' => $users['dob'],
									'email' => $users['email'],
									'Otp_verification' => $users['status'],
									//'email' => $users['email'],
									'mobile' => $users['mobile'],
									//'device_id' => $users['device_id'],
									'device_type' => $users['device_type'],
									'profile_image' => base_url() . 'uploads/profile_images/' . $users['profile_image'],
								);
								if ($users['status'] == 1) {
									$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Login successfully', 'response' => $user_details);
								} else {
									// OTP code start
									$mob = $users['mobile'];
									$datee = strtotime(date("h:i:sa")) + 600;
									$datee = date("Y-m-d h:i:s", $datee);
									$user_name = $users['email'];
									$otp = rand(1000, 9999);
									$uid = array('id' => $users['id']);
									$data_otp = array('otp' => $otp, 'otp_expire' => $datee);
									//$response= $this->send_sms($otp,$mob);
									$response = $this->send_sms($otp, $mob);
									$subject = 'CarePro OTP Code';
									$message = 'Your user verify OTP code:' . $otp;
									$chk_mail = new_send_mail($message, $subject, $user_name, '');
									// echo $chk_mail;
									// die;
									$otp_id = $this->common_model->updateRecords('cp_users', $data_otp, $uid);
									// OTP code end
									$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Account not verified.', 'response' => $user_details);
								}
								// }else{
								// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => "User inactive or deleted by admin",'response' => array('message' => 'User inactive or deleted by admin'));
								// }
							} else {
								$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'The email or password inserted is wrong', 'response' => array('message' => 'The email or password inserted is wrong'));
							}
						} else {
							$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Password inserted is wrong', 'response' => array('message' => 'Password inserted is wrong'));
						}
					} else {
						$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Email inserted is wrong', 'response' => array('message' => 'Email inserted is wrong'));
					}
				} else {
					// $resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'The inserted email is not associated with any account sdfsdf','response' => array('message' => 'The inserted Email is not associated with any account tst'));
					$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Please enter correct username and password.', 'response' => array('message' => 'Please enter correct username and password.'));
				}
			} else {
				$check_username = $this->common_model->getRecordCount('cp_users', array('username' => $email));
				if ($check_username > 0) {
					$conditions = array('username' => $email, 'password' => $password, 'user_role' => $user_roll);
					$conditions1 = array('username' => $email, 'password' => $password, 'status' => 1, 'user_role' => $user_roll);
					$users = $this->common_model->getSingleRecordById('cp_users', $conditions);
					if (!empty($users)) {
						$condition = array('id' => $users['id']);
						$device_type = isset($data['device_type']) ? $data['device_type'] : '';
						//$device_id   = isset($data['device_id']) ? $data['device_id'] : '';
						//$updateArr = array('device_type'=>$device_type,'device_id'=>$device_id);
						$updateArr = array('device_type' => $device_type);
						$update_login = $this->common_model->updateRecords('cp_users', $updateArr, $condition);
						$users1 = $this->common_model->getSingleRecordById('cp_users', $conditions1);
						//if(!empty($users1)){
						$user_details = array(
							'id' => $users['id'],
							'name' => $users['name'],
							'username' => $users['username'],
							'lastname' => $users['lastname'],
							'dob' => $users['dob'],
							'email' => $users['email'],
							'Otp_verification' => $users['status'],
							'mobile' => $users['mobile'],
							//'device_id' => $users['device_id'],
							'device_type' => $users['device_type'],
							'profile_image' => base_url() . 'uploads/profile_images/' . $users['profile_image'],
						);
						if ($users['status'] == 1) {
							$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Login successfully', 'response' => $user_details);
						} else {
							// OTP code start
							$mob = $users['mobile'];
							$datee = strtotime(date("h:i:sa")) + 600;
							$datee = date("Y-m-d h:i:s", $datee);
							$user_name = $users['email'];
							$otp = rand(1000, 9999);
							$uid = array('id' => $users['id']);
							$data_otp = array('otp' => $otp, 'otp_expire' => $datee);
							//$response= $this->send_sms($otp,$mob);
							$response = $this->send_sms($otp, $mob);
							$subject = 'CarePro OTP Code';
							$message = 'Your user verify OTP code:' . $otp;
							$chk_mail = new_send_mail($message, $subject, $user_name, '');
							// echo $chk_mail;
							// die;
							$otp_id = $this->common_model->updateRecords('cp_users', $data_otp, $uid);
							// OTP code end
							$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Account not verified.', 'response' => $user_details);
						}
						// }else{
						// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => "User inactive or deleted by admin",'response' => array('message' => 'User inactive or deleted by admin'));
						// }
					} else {
						$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'The username or password inserted is wrong', 'response' => array('message' => 'The username or password inserted is wrong'));
					}
				} else {
					$check_phone = $this->common_model->getRecordCount('cp_users', array('mobile_with_code' => $email));
					if ($check_phone > 0) {
						$conditions = array('mobile_with_code' => $email, 'password' => $password);
						$conditions1 = array('mobile_with_code' => $email, 'password' => $password, 'status' => 1);
						$users = $this->common_model->getSingleRecordById('cp_users', $conditions);
						if (!empty($users)) {
							$condition = array('id' => $users['id']);
							$device_type = isset($data['device_type']) ? $data['device_type'] : '';
							//$device_id   = isset($data['device_id']) ? $data['device_id'] : '';
							//$updateArr = array('device_type'=>$device_type,'device_id'=>$device_id);
							$updateArr = array('device_type' => $device_type);
							$update_login = $this->common_model->updateRecords('cp_users', $updateArr, $condition);
							$users1 = $this->common_model->getSingleRecordById('cp_users', $conditions1);
							//if(!empty($users1)){
							$user_details = array(
								'id' => $users['id'],
								'name' => $users['name'],
								'username' => $users['username'],
								'email' => $users['email'],
								'Otp_verification' => $users['status'],
								'mobile' => $users['mobile'],
								//'device_id' => $users['device_id'],
								'device_type' => $users['device_type'],
								'profile_image' => base_url() . 'uploads/profile_images/' . $users['profile_image'],
							);
							if ($users['status'] == 1) {
								$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Login successfully', 'response' => $user_details);
							} else {
								// OTP code start
								$mob = $users['mobile'];
								$datee = strtotime(date("h:i:sa")) + 600;
								$datee = date("Y-m-d h:i:s", $datee);
								$user_name = $users['email'];
								$otp = rand(1000, 9999);
								$uid = array('id' => $users['id']);
								$data_otp = array('otp' => $otp, 'otp_expire' => $datee);
								//$response= $this->send_sms($otp,$mob);
								$response = $this->send_sms($otp, $mob);
								$subject = 'CarePro OTP Code';
								$message = 'Your user verify OTP code:' . $otp;
								$chk_mail = new_send_mail($message, $subject, $user_name, '');
								// echo $chk_mail;
								// die;
								$otp_id = $this->common_model->updateRecords('cp_users', $data_otp, $uid);
								// OTP code end
								$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Account not verified.', 'response' => $user_details);
							}
							// }else{
							// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => "User inactive or deleted by admin",'response' => array('message' => 'User inactive or deleted by admin'));
							// }
						} else {
							$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'The mobile number or password inserted is wrong', 'response' => array('message' => 'The mobile number or password inserted is wrong'));
						}
					} else {
						//$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'The inserted mobile number is not associated with any account sfd','response' => array('message' => 'The inserted Mobile Number is not associated with any account'));
						$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Please enter correct username and password.', 'response' => array('message' => 'Please enter correct username and password.'));
					}
				}
			}
		}
		$this->response($resp);
	}
	function isValidEmail($email)
	{
		return filter_var($email, FILTER_VALIDATE_EMAIL)
			&& preg_match('/@.+\./', $email);
	}
	/*User Signup */
	public function signup_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$mob = $data['mobile_no'];
		// $required_parameter = array("user_roll","name","lastname","dob","username","email","mobile_no","country_code","password","confirm_password","device_id","device_type");
		$required_parameter = array("user_roll", "name", "lastname", "dob", "username", "email", "mobile_no", "country_code", "password", "confirm_password", "device_type");
		$chk_error = check_required_value($required_parameter, $data);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' =>  'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		if (!$this->isValidEmail($data['email'])) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'input must have valid email in the text', 'response' => array('message' => 'input must have valid email in the text'));
			$this->response($resp);
		}
		if (!is_numeric($data['mobile_no'])) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Input must have numbers only', 'response' => array('message' => 'Input must have numbers only'));
			$this->response($resp);
		}
		// security changes start
		$password = trim($data['password']);
		$regex_lowercase = '/[a-z]/';
		$regex_uppercase = '/[A-Z]/';
		$regex_number = '/[0-9]/';
		$regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';
		// if (preg_match_all($regex_lowercase, $password) < 1)
		// {
		// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => message(43), 'response' => array('message' => message(43)));
		// 	$this->response($resp);
		// }
		if (preg_match_all($regex_lowercase, $password) < 1) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must contains lowercase letter.', 'response' => array('message' => 'Your password must contains lowercase letter.'));
			$this->response($resp);
		}
		if (preg_match_all($regex_uppercase, $password) < 1) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must contains uppercase letter.', 'response' => array('message' => 'Your password must contains uppercase letter.'));
			$this->response($resp);
		}
		if (preg_match_all($regex_number, $password) < 1) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must contains number.', 'response' => array('message' => 'Your password must contains number.'));
			$this->response($resp);
		}
		if (preg_match_all($regex_special, $password) < 1) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must contains special character.', 'response' => array('message' => 'Your password must contains special character.'));
			$this->response($resp);
		}
		if (strlen($password) < 6) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must be at least 6 characters long.', 'response' => array('message' => 'Your password must be at least 6 characters long.'));
			$this->response($resp);
		}
		if (strlen($password) > 30) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must not exceed 30 characters long.', 'response' => array('message' => 'Your password must not exceed 30 characters long.'));
			$this->response($resp);
		}
		// security changes end
		if ($data['password'] != $data['confirm_password']) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Input must be the same as the password', 'response' => array('message' => 'Input must be the same as the password'));
			$this->response($resp);
		} else {
			$check_email = $this->common_model->getRecordCount('cp_users', array('email' => $data['email'], 'status!=' => 3));
			// echo $check_email;
			// die;
			if ($check_email > 0) {
				// if($check_email > 0){
				// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'This email is already exit, please login.', 'response' => array('message' =>'This email is already exit, please login.'));
				// 	$this->response($resp);
				// 	exit;
				// }
				// $check_username = $this->common_model->getRecordCount('cp_users', array('username' => $data['username'],'status!='=>3));
				// if($check_username > 0){
				// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'This userid is already exit, please login.', 'response' => array('message' =>'This userid is already exit, please login.'));
				// 	$this->response($resp);
				// 	exit;
				// }
				// echo $data['country_code'].$data['mobile_no'];
				// die;
				$enter_email = $data['email'];
				$enter_username = $data['username'];
				$enter_mobile_no = $data['mobile_no'];
				$enter_password = md5($data['password']);
				$ChechUser = $this->common_model->getSingleRecordById('cp_users', array('email' => $enter_email));
				$get_email =	$ChechUser['email'];
				$get_mobile =	$ChechUser['mobile'];
				$get_username =	$ChechUser['username'];
				$get_password =	$ChechUser['password'];
				// echo 'Enter email'.' '.$enter_email;
				// echo "<br>";
				// echo 'Get email'.' '.$get_email;
				// echo "<br>";
				// echo 'Enter username'.' '.$enter_username;
				// echo "<br>";
				// echo 'Get username'.' '.$get_username;
				// echo "<br>";
				// echo 'Enter mobile'.' '.$enter_mobile_no;
				// echo "<br>";
				// echo 'Get mobile'.' '.$get_mobile;
				// echo "<br>";
				// echo 'Enter pass'.' '.$enter_password;
				// echo "<br>";
				// echo 'Get pass'.' '.$get_password;
				// echo "<br>"; 
				// die;
				// echo "check2";
				// echo "<pre>";
				// print_r($ChechUser['email']);
				// die;
				// echo $enter_email;
				// echo "<br>";
				// echo $get_email;
				// die;
				$error_count = 0;
				if ($enter_email != $get_email) {
					$error_count++;
				}
				if ($enter_username != $get_username) {
					$error_count++;
				}
				if ($enter_mobile_no != $get_mobile) {
					$error_count++;
				}
				if ($enter_password != $get_password) {
					$error_count++;
				}
				if ($error_count > 0) {
					$resp = array('status_code' => ERROR, 'status' => 'false', 'count' => $error_count, 'message' => 'Sorry you are not verified.', 'response' => array('message' => 'Sorry you are not verified.'));
				} else {
					$u_id = $ChechUser['id'];
					$where_condition = array('id' => $u_id);
					$verificationData = array('user_verify_by_app' => 1);
					$this->common_model->updateRecords('cp_users', $verificationData, $where_condition);
					// echo "if true";  
					// die;
					// echo "check2";
					// echo "<pre>";
					// print_r($enter_email);
					// die;
					$data = array(
						'username' => (!empty($ChechUser['username']) ? $ChechUser['username'] : ''),
						'name' => (!empty($ChechUser['name']) ? $ChechUser['name'] : ''),
						'lastname' => (!empty($ChechUser['lastname']) ? $ChechUser['lastname'] : ''),
						'dob' => (!empty($ChechUser['dob']) ? $ChechUser['dob'] : ''),
						'email' => (!empty($ChechUser->email) ? $ChechUser['email'] : ''),
						'mobile' => (!empty($ChechUser->mobile) ? $ChechUser['mobile'] : ''),
						//'password' => (!empty($data['password']) ? md5($ChechUser->email) : ''),
						//'device_id' => (!empty($ChechUser->device_id) ? $ChechUser['device_id'] : ''),
						'device_type' => (!empty($ChechUser->device_type) ? $ChechUser['device_type'] : ''),
						//'create_user' => date('Y-m-d h:i:s'),
						//'Otp_verification' =>$ChechUser->status, 
						'Otp_verification' => $ChechUser['status'],
						//'user_verification_status'=> $ChechUser['user_verify_by_app'],       
						//'status'      => 0,
						'user_role'      => $ChechUser['user_role'],
						'country_code'      => $ChechUser['country_code'],
						'mobile_with_code'    => $ChechUser['country_code'] . $ChechUser['mobile'],
					);
					// echo "<pre>";
					// print_r($data);
					// die;
					//$userId = $this->common_model->addRecords('cp_users', $data);
					$userId = $ChechUser['id'];
					// echo $userId;
					// die;   
					if ($userId) {
						$mob = $get_mobile;
						$datee = strtotime(date("h:i:sa")) + 600;
						$datee = date("Y-m-d h:i:s", $datee);
						$user_name = $ChechUser['email'];
						$otp = rand(1000, 9999);
						$uid = array('id' => $userId);
						$data_otp = array('otp' => $otp, 'otp_expire' => $datee);
						//$response= $this->send_sms($otp,$mob);
						$response = $this->send_sms($otp, $mob);
						$subject = 'CarePro OTP Code';
						$message = 'Your user verify OTP code:' . $otp;
						$chk_mail = new_send_mail($message, $subject, $user_name, '');
						// echo $chk_mail;
						// die;
						$otp_id = $this->common_model->updateRecords('cp_users', $data_otp, $uid);
						//$userData = $data;
						$userData = $this->common_model->getSingleRecordById('cp_users', array('id' => $userId));
						// echo "<pre>";
						// print_r($userData);
						// die;
						$UsrData = array(
							'id' => $userData['id'],
							'name' => $userData['name'],
							'lastname' => $userData['lastname'],
							'dob' =>	$userData['dob'],
							'username' => $userData['username'],
							'city' => $userData['city'],
							'state' => $userData['state'],
							'zipcode' => $userData['zipcode'],
							'company_school' => $userData['company_school'],
							'email'	 => $userData['email'],
							'mobile' => $userData['mobile'],
							'password'	=> $userData['password'],
							'profile_image'	=> $userData['profile_image'],
							//'device_id'	=>$userData['device_id'] ,
							'device_type' => $userData['device_type'],
							'device_token' => $userData['device_token'],
							'user_role'	=> $userData['user_role'],
							'act_link' => $userData['act_link'],
							'status' => $userData['status'],
							'Otp_verification' => $userData['status'],
							'user_verification_status' => $userData['user_verify_by_app'],
							'conversation_status' => $userData['conversation_status'],
							'create_user' => $userData['create_user'],
							'update_user' => $userData['update_user'],
							'otp' => $userData['otp'],
							'otp_expire' => $userData['otp_expire'],
							'country_code' => $userData['country_code'],
							'mobile_with_code' => $userData['mobile_with_code'],
							'gender' => $userData['gender'],
							'address' => $userData['address'],
							'user_id' => $userData['user_id'],
						);
						$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Verify successfully.', 'response' => $UsrData);
					}
					// else{
					// 	$resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again','response' => array('message' =>'Some error occured, please try again'));
					// }
				}
			} else {
				$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Please check entered email address is wrong.', 'response' => array('message' => 'Please check entered email address is wrong.'));
				// echo "email not found";
				// die;
			}
		}
		$this->response($resp);
	}
	public function old_signup_post()
	{
		$pdata = file_get_contents("php://input");
		$data = json_decode($pdata, true);
		$mob = $data['mobile_no'];
		$required_parameter = array("user_roll", "name", "lastname", "dob", "username", "email", "mobile_no", "country_code", "password", "confirm_password", "device_id", "device_type");
		$chk_error = check_required_value($required_parameter, $data);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' =>  'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		if (!$this->isValidEmail($data['email'])) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'input must have valid email in the text', 'response' => array('message' => 'input must have valid email in the text'));
			$this->response($resp);
		}
		if (!is_numeric($data['mobile_no'])) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Input must have numbers only', 'response' => array('message' => 'Input must have numbers only'));
			$this->response($resp);
		}
		if ($data['password'] != $data['confirm_password']) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Input must be the same as the password', 'response' => array('message' => 'Input must be the same as the password'));
			$this->response($resp);
		} else {
			$check_email = $this->common_model->getRecordCount('cp_users', array('email' => $data['email'], 'status!=' => 3));
			if ($check_email > 0) {
				// $resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'This email is already exit', 'response' => array('message' =>'This email is already exit'));
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'This email is already exit, please login.', 'response' => array('message' => 'This email is already exit, please login.'));
				$this->response($resp);
				exit;
			}
			$check_username = $this->common_model->getRecordCount('cp_users', array('username' => $data['username'], 'status!=' => 3));
			if ($check_username > 0) {
				// $resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'This email is already exit', 'response' => array('message' =>'This email is already exit'));
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'This userid is already exit, please login.', 'response' => array('message' => 'This userid is already exit, please login.'));
				$this->response($resp);
				exit;
			}
			$data = array(
				'username' => (!empty($data['username']) ? $data['username'] : ''),
				'name' => (!empty($data['name']) ? $data['name'] : ''),
				'lastname' => (!empty($data['lastname']) ? $data['lastname'] : ''),
				'dob' => (!empty($data['dob']) ? $data['dob'] : ''),
				'email' => (!empty($data['email']) ? $data['email'] : ''),
				'mobile' => (!empty($data['mobile_no']) ? $data['mobile_no'] : ''),
				'password' => (!empty($data['password']) ? md5($data['password']) : ''),
				'device_id' => (!empty($data['device_id']) ? $data['device_id'] : ''),
				'device_type' => (!empty($data['device_type']) ? $data['device_type'] : ''),
				'create_user' => date('Y-m-d h:i:s'),
				'status'      => 0,
				'user_role'      => $data['user_roll'],
				'country_code'      => $data['country_code'],
				'mobile_with_code'      => $data['country_code'] . $data['mobile_no'],
			);
			$userId = $this->common_model->addRecords('cp_users', $data);
			if ($userId) {
				$datee = strtotime(date("h:i:sa")) + 600;
				$datee = date("Y-m-d h:i:s", $datee);
				$user_name = $data['email'];
				$otp = rand(1000, 9999);
				$uid = array('id' => $userId);
				$data_otp = array('otp' => $otp, 'otp_expire' => $datee);
				//echo $data['mobile_no'];die;
				$response = $this->send_sms($otp, $mob);
				$subject = 'CarePro OTP Code';
				$message = 'Your user verify OTP code:' . $otp;
				// $chk_mail= sendEmail($user_name,$subject,$message);
				$chk_mail = new_send_mail($message, $subject, $user_name, '');
				$otp_id = $this->common_model->updateRecords('cp_users', $data_otp, $uid);
				$userData = $this->common_model->getSingleRecordById('cp_users', array('id' => $userId));
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Successfully registered', 'response' => $userData);
			} else {
				$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
			}
		}
		$this->response($resp);
	}
	/*Verify Otp*/
	public function verifyotp_post()
	{
		/* Check for required parameter */
		// $pdata = file_get_contents("php://input");
		// $data = json_decode( $pdata,true );
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('email', 'otp');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		$check_email = $this->common_model->getRecordCount('cp_users', array('email' => $data['email']));
		//echo $this->db->last_query();die;
		// echo '<pre>';print_r($check_email);die;
		if ($check_email == 0) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => "The inserted Email is not associated with any account", 'response' => array('message' => "The inserted Email is not associated with any account"));
		} else {
			//$resultuser=$this->common_model->getSingleRecordById('cp_users',array('email' => $data['email'],'otp' => $data['otp']));
			//$data['otp']=1111;
			$resultuser = $this->common_model->getSingleRecordById('cp_users', array('email' => $data['email'], 'otp' => $data['otp']));
			//echo '<pre>';print_r($resultuser);die;
			//2019-07-03 04:11:22
			//2019-07-03 03:45:00
			if (!empty($resultuser)) {
				$today_date = date("Y-m-d h:i:s");
				//echo $resultuser['otp_expire'] .'>='. $today_date;die;
				if ($resultuser['otp_expire'] >= $today_date) {
					$updateArr = array('status' => 1);
					$condition = array('email' => $data['email']); //die;
					$this->common_model->updateRecords('cp_users', $updateArr, $condition);
					// New changes
					$user_details = array(
						'id' => $resultuser['id'],
						'name' => $resultuser['name'],
						'lastname' => $resultuser['lastname'],
						'dob' =>	$resultuser['dob'],
						'username' => $resultuser['username'],
						'city' => $resultuser['city'],
						'state' => $resultuser['state'],
						'zipcode' => $resultuser['zipcode'],
						'company_school' => $resultuser['company_school'],
						'email'	 => $resultuser['email'],
						'mobile' => $resultuser['mobile'],
						'password'	=> $resultuser['password'],
						'profile_image'	=> $resultuser['profile_image'],
						'device_id'	=> $resultuser['device_id'],
						'device_type' => $resultuser['device_type'],
						'device_token' => $resultuser['device_token'],
						'user_role'	=> $resultuser['user_role'],
						'act_link' => $resultuser['act_link'],
						'status' => $resultuser['status'],
						'Otp_verification' => $resultuser['status'],
						'conversation_status' => $resultuser['conversation_status'],
						'create_user' => $resultuser['create_user'],
						'update_user' => $resultuser['update_user'],
						'otp' => $resultuser['otp'],
						'otp_expire' => $resultuser['otp_expire'],
						'country_code' => $resultuser['country_code'],
						'mobile_with_code' => $resultuser['mobile_with_code'],
						'gender' => $resultuser['gender'],
						'address' => $resultuser['address'],
						'user_id' => $resultuser['user_id'],
					);
					// End    
					$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => "Your OTP has been verified", 'response' => $user_details);
				} else {
					$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => "Your OTP has been expired", 'response' => array('message' => "Your OTP has been expired"));
				}
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => "The OTP entered is incorrect", 'response' => array('message' => "The OTP entered is incorrect"));
			}
		}
		$this->response($resp);
	}
	/*Forgot Password*/
	public function forgot_password_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('email');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		$user_name = $data['email'];
		$type = $data['type'];
		$conditions = array('email' => $user_name);
		$users = $this->common_model->getSingleRecordById('cp_users', $conditions);
		if (!empty($users)) {
			$otp = rand(1000, 9999);
			$datee = strtotime(date("h:i:sa")) + 600;
			$datee = date("Y-m-d h:i:s", $datee);
			$post_data = array('otp' => $otp, 'status' => 0, 'otp_expire' => $datee);
			$where_condition = array('id' => $users['id']);
			$this->common_model->updateRecords('cp_users', $post_data, $where_condition);
			$subject = 'Care pro password code';
			//$message = 'Your reset password OTP:'.$otp;
			$base_url = base_url() . 'assets/images/logo_img1.png';
			$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Care Pro</title> <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
		</head>
		<body style="margin: 0;font-family:"open sans";">
		<div style="max-width: 700px;margin: 30px auto 0; background:#f4f4f4;">
		<div style="border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;">
		<a href="#" target="_blank" style="display:inline-block;padding:20px 0;margin: 0 auto 40px;">
		<img style="max-width:200px;width:100%;margin: 0 auto;" src=' . $base_url . '>
		</a>
		</div>
		<div style="border: none;padding:0; max-width: 580px; margin:-40px auto 0px; border-radius:4px;">
		<div class="temp_cont" style="width:100%; margin:0 auto;">
		<div style="padding:25px 30px 25px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px;     border: 1px solid #cccccc6e;">
		<h2 style="font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;">
		"Forgot Password"
		</h2>
		<p align="center" style="font-size: 16px;line-height: 25px;font-family: open sans;color: #515151;margin-top: 0;">
		<span style="color:#000;text-transform: uppercase;margin-bottom:15px;display:block;text-align: center;font-size: 15px;font-weight:600">Hi ' . $users['username'] . '</span>
		<span style="color:#000;font-weight:600;text-transform:uppercase;margin-bottom: 5px;display:block;text-align:center;font-size:21px;">One time reset OTP password:</span>
		</p>
		<p style="text-align:center;color:#000;">
		' . $otp . '
		</p>
		</div>
		<div style="background:#2f8dccdb;padding: 13px 10px 13px;color:#fff;text-align:center;     border-radius: 3px; margin-bottom:25px; display:inline-block; width: 100%;">
		<p style="font-size: 13px;margin: 0 0 8px;">Thank You for visit</p>
		<p style="font-size: 13px;margin:0;">
		<a style="color:#fff;font-weight:600;text-decoration:none;" href="#" target="_blank">https://mobivdigital.com/carepro/</a>
		</p>
		<div style="display:block;height: 1px;background:#fff;margin:10px 0;"></div>
		<p style="font-size: 13px;margin:0 0 8px;">Care Pro</p>
		<p style="font-size: 13px;margin:0;">
		<a style="color:#fff;font-weight:600;text-decoration:none;" href="javascript:void(0)">EMAIL: info@carepro.com/</a>
		</p>
		</div>
		</div>
		</div>
		</div>
		</body>
		</html>';
			$from_m = "info@carepro.com";
			$fromname = "Carepro";
			//$chk_mail= sendEmail($user_name,$subject,$message);
			//$chk_mail= new_send_mail($message, $subject, $user_name,'');
			$chk_mail =  socialEmail($user_name, $from_m, $fromname, $subject, $message);
			$response = $this->send_sms($otp, $users['mobile']);
			// echo "<pre>";
			// print_r($response);
			// die;
			if ($chk_mail) {
				if ($type == 1) {
					$msgs = "Your password OTP successfully sent please check your email.";
				} else {
					$msgs = "Your password reset OTP successfully sent please check your email.";
				}
				$user_info = array('email' => $users['email'], 'mobile_no' => $users['mobile'], 'message' => $msgs);
				// echo "<pre>";
				// print_r($user_info);
				// die;
				echo json_encode(array('status_code' => SUCCESS, 'status' => 'true', 'message' => $msgs, 'response' => $user_info));
			} else {
				$error = show_error($this->email->print_debugger());
				echo json_encode(array('status_code' => FAILURE, 'status' => 'false', 'message' => "your password reset OTP not send", 'response' => array('message' => "your password reset OTP not send")));
			}
		} else {
			echo json_encode(array('status_code' => FAILURE, 'status' => 'false', 'message' => "The inserted Email is not associated with any account", 'response' => array('message' => "The inserted Email is not associated with any account")));
		}
	}
	/*Reset Password*/
	public function resetpassword_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('email', 'password', 'confirm_password');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		// security changes start
		$password = trim($data['password']);
		$regex_lowercase = '/[a-z]/';
		$regex_uppercase = '/[A-Z]/';
		$regex_number = '/[0-9]/';
		$regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';
		// if (preg_match_all($regex_lowercase, $password) < 1)
		// {
		// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => message(43), 'response' => array('message' => message(43)));
		// 	$this->response($resp);
		// }
		if (preg_match_all($regex_lowercase, $password) < 1) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must contains lowercase letter.', 'response' => array('message' => 'Your password must contains lowercase letter.'));
			$this->response($resp);
		}
		if (preg_match_all($regex_uppercase, $password) < 1) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must contains uppercase letter.', 'response' => array('message' => 'Your password must contains uppercase letter.'));
			$this->response($resp);
		}
		if (preg_match_all($regex_number, $password) < 1) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must contains number.', 'response' => array('message' => 'Your password must contains number.'));
			$this->response($resp);
		}
		if (preg_match_all($regex_special, $password) < 1) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must contains special character.', 'response' => array('message' => 'Your password must contains special character.'));
			$this->response($resp);
		}
		if (strlen($password) < 6) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must be at least 6 characters long.', 'response' => array('message' => 'Your password must be at least 6 characters long.'));
			$this->response($resp);
		}
		if (strlen($password) > 30) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must not exceed 30 characters long.', 'response' => array('message' => 'Your password must not exceed 30 characters long.'));
			$this->response($resp);
		}
		// security changes end
		if ($data['password'] != $data['confirm_password']) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => "Input must be the same as the password", 'response' => array('message' => "Input must be the same as the password"));
			$this->response($resp);
		}
		$email = $data['email'];
		/* Check for user id */
		$check_key = $this->common_model->getRecordCount('cp_users', array('email' => $email));
		if ($check_key == 1) {
			/* Change password */
			$condition = array('email' => $email);
			$updateArr = array('password' => md5($data['password']));
			$this->common_model->updateRecords('cp_users', $updateArr, $condition);
			/* Response array */
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => "Your password reset successfully", 'response' => array('message' => "Your password reset successfully"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => "Invalid details, please try again", 'response' => array('message' => "Invalid details, please try again"));
		}
		$this->response($resp);
	}
	/*send mail */
	public function send_mail_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('to_email', 'subject', 'message', 'user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$post_data = array(
			'to_email' => $data['to_email'],
			'subject' => $data['subject'],
			'message' => $data['message'],
			'user_id' => $data['user_id'],
			'create_email' => date('Y-m-d H:i:s'),
			//'create_email' => date('Y-m-d'),
			'email_status' => 1,
		);
		// New changes for email template start 
		$send_touserData = $this->common_model->getSingleRecordById('cp_users', array('email' => $data['to_email']));
		$send_tousername = $send_touserData['username'];
		$send_byuserData = $this->common_model->getSingleRecordById('cp_users', array('id' => $data['user_id']));
		$send_byname = $send_byuserData['name'];
		$emailId = $this->common_model->addRecords('cp_emails', $post_data);
		$getemail_Data = $this->common_model->getSingleRecordById('cp_emails', array('email_id' => $emailId));
		$create_email_date = $getemail_Data['create_email'];
		$currentDateTime = $create_email_date;
		$newDateTime = date('d-m-y, h:i A', strtotime($currentDateTime));
		// New changes for email template end
		if ($emailId) {
			$user_name = $data['to_email'];
			$subject = $data['subject'];
			//$message = $data['message'];
			$message = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
		<html xmlns='http://www.w3.org/1999/xhtml'>
		<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
		<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>
		</head>
		<body style='margin: 0;font-family:'open sans';'>
		<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;'>
		<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>
		<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin: 0 auto 0px;'>
		<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
		</a>
		</div>
		<div style='border:none; padding:0px 60px; max-width:580px; margin:-40px auto 0px; border-radius:0px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>
		<div class='temp_cont' style='width:100%; margin:0 auto;'>
		<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>
		<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
		<span style='display:block;width:100%;'>
		<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>
		</span>
		Send mail
		</h2>
		<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
		Hi &nbsp;" . $send_tousername . ",
		</h3>
		<style>
							#customers td, #customers th {
		border:1px solid #ddd;
		padding:8px;
	}
							#customers th {
	padding-top:8px;
	padding-bottom:8px;
	text-align:left;
	background-color:#4CAF50;
	color:white;
}
</style>
<table id='customers' style='width:100%; border-collapse: collapse;'>
<tr>
<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $send_byname . "</td>
</tr>						  
<tr>
<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Date and Time:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $newDateTime . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Description:</th>
<td style='font-size:13px; padding:8px;border:1px solid #ddd;'>" . $data['message'] . "</td>
</tr>
</table>
</div>
<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%; box-sizing:border-box;'>
<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>
</p>						
<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>
<p style='font-size: 13px;margin:0 0 8px;'>Care Pro</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>EMAIL: info@carepro.com/</a>
</p>
</div>
</div>
</div>
</div>
</body>
</html>";
			//$message = $data['message'];
			// echo $message;
			// die;
			$chk_mail = sendEmail($user_name, $subject, $message);
			// echo $chk_mail;
			// die;
			$emailData = $this->common_model->getSingleRecordById('cp_emails', array('email_id' => $emailId));
			if ($emailData['create_email'] == "0000-00-00") {
				$emails_create = $emailData['create_email'];
			} else {
				$emails_create = date("M d, Y", strtotime($emailData['create_email']));
			}
			$emailData1 = array(
				'to_email' => $emailData['to_email'],
				'subject' => $emailData['subject'],
				'message' => $emailData['message'],
				'user_id' => $emailData['user_id'],
				'create_date' => $emails_create,
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Message sent.', 'response' => $emailData1);
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/*list mail */
	public function emaillist1_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$limit = 5;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$emailCount = $this->common_model->getEmailCount($data['user_id']);
		$start  = ($data['page_no'] - 1) * $limit;
		$pages = ceil(count($emailCount) / $limit);
		$emailData = $this->common_model->getEmails($start, $limit, $data['user_id']);
		if (!empty($emailData)) {
			foreach ($emailData as $row) {
				if ($emailData['create_email'] == "0000-00-00") {
					$emails_create = $row['create_email'];
				} else {
					$emails_create = date("M d, Y", strtotime($row['create_email']));
				}
				$attachment = $this->orderImage($row['email_id']);
				$email[] = array(
					'id' => $row['email_id'],
					'user_id' => $row['user_id'],
					'to_email' => $row['to_email'],
					'subject' => $row['subject'],
					'message' => $row['message'],
					'create_date' => $row['create_email'],
					'attachment' => $attachment,
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('email_data' => $email));
		} else {
			$email = array();
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Email not found', 'response' => array('message' => 'Email not found', 'email_data' => $email));
		}
		$this->response($resp);
	}
	public function  orderImage($email_id)
	{
		$this->db->select('attachment_file');
		$this->db->from('cp_emails_attachment');
		$this->db->where('email_id', $email_id);
		$query = $this->db->get();
		$result = $query->result_array();
		foreach ($result as $result_details) {
			$imageArray[]  = base_url() . 'uploads/email/' . $result_details['attachment_file'];
		}
		$result_new = implode(",", $imageArray);
		return $result_new;
	}
	/*list mail */
	public function inboxlist1_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$limit = 5;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$emailId = $this->common_model->getSingleRecordById('cp_users', array('id' => $data['user_id']));
		$emailId = $emailId['email'];
		$emailCount = $this->common_model->getEmailCount1($emailId);
		//echo $emailCount;die;
		$start  = ($data['page_no'] - 1) * $limit;
		$pages = ceil(count($emailCount) / $limit);
		$emailData = $this->common_model->getEmails1($start, $limit, $emailId);
		// print_r("<pre/>");
		//print_r($emailData);
		// die;
		if (!empty($emailData)) {
			foreach ($emailData as $row) {
				if ($emailData['create_email'] == "0000-00-00") {
					$emails_create = $row['create_email'];
				} else {
					$emails_create = date("M d, Y", strtotime($row['create_email']));
				}
				$attachment = $this->orderImage($row['email_id']);
				$email[] = array(
					'id' => $row['email_id'],
					'user_id' => $row['user_id'],
					'to_email' => $row['to_email'],
					'subject' => $row['subject'],
					'message' => $row['message'],
					'create_date' => $emails_create,
					"attachment" => $attachment
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('email_data' => $email));
		} else {
			$email = array();
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Email not found', 'response' => array('message' => 'Email not found', 'email_data' => $email));
		}
		$this->response($resp);
	}
	/*photo list  */
	public function photolist_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$limit = 5;
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$emailCount = $this->common_model->getPhotoCount($data['user_id']);
		$start  = ($data['page_no'] - 1) * $limit;
		$pages = ceil(count($emailCount) / $limit);
		$emailData = $this->common_model->getPhoto($start, $limit, $data['user_id']);
		//$emailData =$this->common_model->getAllRecordsById("cp_user_photo",array('u_photo_status'=>1,'user_id'=>$data['user_id']));
		if (!empty($emailData)) {
			foreach ($emailData as $row) {
				$fav  = $this->getfav($row['u_photo_id']);
				$email[] = array(
					'id' => $row['u_photo_id'],
					'user_id' => $row['user_id'],
					'photo' => base_url() . 'uploads/photos/' . $row['u_photo'],
					'fav' => $fav,
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('photo_data' => $email));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Email not found', 'response' => array('message' => 'Email not found'));
		}
		$this->response($resp);
	}
	public function  getfav($photo_id)
	{
		$this->db->select('*');
		$this->db->from('cp_photo_favourite');
		$this->db->where('photo_id', $photo_id);
		//$this->db->where('order_id',$order_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return "1";
		}
		return "0";
	}
	public function  getArticleFav($article_id)
	{
		$this->db->select('*');
		$this->db->from('cp_article_favourite');
		$this->db->where('article_id', $article_id);
		//$this->db->where('order_id',$order_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return "1";
		}
		return "0";
	}
	public function  getArticleFavuserby($article_id, $user_id)
	{
		$this->db->select('*');
		$this->db->from('cp_article_favourite');
		$this->db->where('article_id', $article_id);
		$this->db->where('user_id', $user_id);
		//$this->db->where('order_id',$order_id);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		if ($query->num_rows() > 0) {
			return "1";
		}
		return "0";
	}
	public function  getMusicFav($music_id)
	{
		$this->db->select('*');
		$this->db->from('cp_music_favorite');
		$this->db->where('music_id', $music_id);
		//$this->db->where('order_id',$order_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return "1";
		}
		return "0";
	}
	public function  getMusicUserFav($music_id, $user_id)
	{
		$this->db->select('*');
		$this->db->from('cp_music_favorite');
		$this->db->where('music_id', $music_id);
		$this->db->where('user_id', $user_id);
		//$this->db->where('order_id',$order_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return "1";
		}
		return "0";
	}
	public function  getMovieFav($movie_id)
	{
		$this->db->select('*');
		$this->db->from('cp_movie_favorite');
		$this->db->where('movie_id', $movie_id);
		//$this->db->where('order_id',$order_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return "1";
		}
		return "0";
	}
	public function  getMovieUserFav($movie_id, $user_id)
	{
		$this->db->select('*');
		$this->db->from('cp_movie_favorite');
		$this->db->where('movie_id', $movie_id);
		$this->db->where('user_id', $user_id);
		//$this->db->where('order_id',$order_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return "1";
		}
		return "0";
	}
	public function  getRestFav($rest_id)
	{
		$this->db->select('*');
		$this->db->from('cp_fav_restaurant');
		$this->db->where('restaurant_id', $rest_id);
		//$this->db->where('order_id',$order_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return "1";
		}
		return "0";
	}
	public function  getBankFav($bank_id)
	{
		$this->db->select('*');
		$this->db->from('cp_fav_banks');
		$this->db->where('bank_id', $bank_id);
		//$this->db->where('order_id',$order_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return "1";
		}
		return "0";
	}
	public function  getGroceryFav($grocery_id)
	{
		$this->db->select('*');
		$this->db->from('cp_fav_grocery');
		$this->db->where('grocery_id', $grocery_id);
		//$this->db->where('order_id',$order_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return "1";
		}
		return "0";
	}
	/*delete mail */
	public function emaildelete1_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('email_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$ids = explode(',', $data['email_id']);
		$emailid = $this->common_model->deleteEmails($ids);
		if ($emailid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('email_data' => "Message successfully deleted"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Email not found', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/*delete mail */
	public function emailInboxDelete_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('email_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$ids = explode(',', $data['email_id']);
		$emailid = $this->common_model->deleteEmails1($ids);
		if ($emailid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('email_data' => "Message successfully deleted"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Email not found', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/* add photo */
	public function addPhoto_post()
	{
		//$data = $_POST;
		// print_r($_POST);
		// die;
		$data = $this->param;
		// print_r($data);
		// die;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		if (!empty($_FILES['photo_image']['name'])) {
			// echo $_FILES['photo_image']['name'];
			// die;
			// security chnages start
			$imgfile = $_FILES['photo_image']['name'];
			$extension = substr($imgfile, strlen($imgfile) - 4, strlen($imgfile));
			// echo $imgfile;
			// die;
			// allowed extensions
			$allowed_extensions = array(".jpg", ".jpeg", ".png");
			//$allowed_extensions = array(".jpg",".jpeg");
			// security chnages start
			if (in_array($extension, $allowed_extensions)) // security check start
			{
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['photo_image']['name'], PATHINFO_EXTENSION);
				$file_name = $random . "." . $ext;
				$target_dir = "uploads/photos/";
				$target_file = $target_dir . $file_name;
				if (move_uploaded_file($_FILES["photo_image"]["tmp_name"], $target_file)) {
					$photoFile = $file_name;
				}
			} else {  // security check end
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your image file is not valide. allow file types jpg, jpeg, png', 'response' => array('message' => 'Your image file is not valide. allow file types jpg, jpeg, png'));
				$this->response($resp);
			}
		}
		$post_data = array(
			'user_id' => $data['user_id'],
			'u_photo_created' => date('Y-m-d h:i:s'),
			'u_photo_status' => 1,
			'u_photo'        => $photoFile,
		);
		$photoId = $this->common_model->addRecords('cp_user_photo', $post_data);
		if ($photoId) {
			$photoData = $this->common_model->getSingleRecordById('cp_user_photo', array('u_photo_id' => $photoId));
			$photoData1 = array(
				'id' => $photoData['u_photo_id'],
				'user_id' => $photoData['user_id'],
				'image_url' => base_url() . 'uploads/photos/' . $photoData['u_photo']
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'photo added successfully', 'response' => $photoData1);
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => message(18)));
		}
		$this->response($resp);
	}
	public function generateRandomString($length = 5)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	/* add fav photo */
	public function addFavPhoto_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id', 'photo_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$FavPhotoData1 = $this->common_model->getSingleRecordById('cp_photo_favourite', array('user_id' => $data['user_id'], "photo_id" => $data['photo_id']));
		if (empty($FavPhotoData1)) {
			$post_data = array(
				'user_id' => $data['user_id'],
				'photo_id' => $data['photo_id'],
				'photo_fav_created' => date('Y-m-d h:i:s'),
				'favourite' => 1,
			);
			$FavPhotoId = $this->common_model->addRecords('cp_photo_favourite', $post_data);
			if ($FavPhotoId) {
				//$FavPhotoData = $this->common_model->getSingleRecordById('cp_photo_favourite', array('photo_fav_id' => $FavPhotoId));
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'fav_status' => '1', 'message' => 'Favorite successfully', 'response' => array('fav_data' => "Favorite successfully"));
			} else {
				$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
			}
		} else {
			$this->db->delete('cp_photo_favourite', array("user_id" => $data['user_id'], "photo_id" => $data['photo_id']));
			$FavPhotoId = $this->db->affected_rows();
			if ($FavPhotoId) {
				//$FavPhotoData = $this->common_model->getSingleRecordById('cp_photo_favourite', array('photo_fav_id' => $FavPhotoId));
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'fav_status' => '0', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));
			} else {
				$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
			}
		}
		$this->response($resp);
	}
	/* favourite photo list  */
	public function favphotolist_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		$favData = $this->common_model->jointwotable('cp_photo_favourite', 'photo_id', 'cp_user_photo', 'u_photo_id', array('cp_photo_favourite.user_id' => $data['user_id']), '*');
		if (!empty($favData)) {
			foreach ($favData as $row) {
				$favphoto[] = array(
					'id' => $row->photo_fav_id,
					'user_id' => $row->user_id,
					'photo' => base_url() . 'uploads/photos/' . $row->u_photo,
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('favPhoto_data' => $favphoto));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Favourite Data not found', 'response' => array('message' => 'Favourite Data not found'));
		}
		$this->response($resp);
	}
	/*photo delete */
	public function photodelete_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('photo_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$ids = explode(',', $data['photo_id']);
		$photoid = $this->common_model->deletePhotos($ids);
		if ($photoid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('photo_data' => "photo deleted succesfully"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'photo not found', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/* fav photo delete */
	public function favphotodelete_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('fav_photo_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$ids = explode(',', $data['fav_photo_id']);
		$photoid = $this->common_model->deletePhotosfav($ids);
		if ($photoid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('photo_data' => "Unfavorite succesfully"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/*photo list  */
	public function allPhotoList_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		$emailData = $this->common_model->getAllRecordsById("cp_user_photo", array('u_photo_status' => 1, 'user_id' => $data['user_id']));
		if (!empty($emailData)) {
			foreach ($emailData as $row) {
				$fav  = $this->getfav($row['u_photo_id']);
				$email[] = array(
					'id' => $row['u_photo_id'],
					'user_id' => $row['user_id'],
					'photo' => base_url() . 'uploads/photos/' . $row['u_photo'],
					'fav' => $fav,
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('photo_data' => $email));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Photo not found', 'response' => array('message' => 'Photo not found'));
		}
		$this->response($resp);
	}
	public function sharePhoto_post()
	{
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('sender_id', "receiver_ids", "photo_ids");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		} else {
			$sender_id = $data['sender_id'];
			$receiver_ids = $data['receiver_ids'];
			$photo_ids = $data['photo_ids'];
			$description = $data['description'];
			$sender_info = $this->common_model->getSingleRecordById('cp_users', array('id' => $sender_id));
			//$user_arr = $this->common_model->getWhereIn('cp_users','email',$receiver_ids);
			$photo_arr = $this->common_model->getWhereIn('cp_user_photo', 'u_photo_id', $photo_ids);
			$photoarr = array_chunk($photo_arr, 2);
			if (!empty($receiver_ids)) {
				//foreach ($user_arr as $key => $value) {
				$user_name = $receiver_ids; //$value['email'];
				$subject = 'Carepro - Share Photos';
				$message = '<!DOCTYPE html>
					<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
					<head>
					<meta charset="utf-8">
					<!-- utf-8 works for most cases -->
					<meta name="viewport" content="width=device-width">
					<!-- Forcing initial-scale shouldnt be necessary -->
					<meta http-equiv="X-UA-Compatible" content="IE=edge">
					<!-- Use the latest (edge) version of IE rendering engine -->
					<meta name="x-apple-disable-message-reformatting">
					<!-- Disable auto-scale in iOS 10 Mail entirely -->
					<title></title>
					<!-- The title tag shows in email notifications, like Android 4.4. -->
					<link href="https://fonts.googleapis.com/css?family=Work+Sans:200,300,400,500,600,700" rel="stylesheet">
					<!-- CSS Reset : BEGIN -->
					<style>
					html,
					body {
						margin: 0 auto !important;
						padding: 0 !important;
						height: 100% !important;
						width: 100% !important;
						background: #f1f1f1;
					}
								         * {
					-ms-text-size-adjust: 100%;
					-webkit-text-size-adjust: 100%;
				}
				div[style*="margin: 16px 0"] {
					margin: 0 !important;
				}
				table,
				td {
					mso-table-lspace: 0pt !important;
					mso-table-rspace: 0pt !important;
				}
				table {
					border-spacing: 0 !important;
					border-collapse: collapse !important;
					table-layout: fixed !important;
					margin: 0 auto !important;
				}
				img {
					-ms-interpolation-mode:bicubic;
				}
				a {
					text-decoration: none;
				}
								         *[x-apple-data-detectors],
				.unstyle-auto-detected-links *,
				.aBn {
					border-bottom: 0 !important;
					cursor: default !important;
					color: inherit !important;
					text-decoration: none !important;
					font-size: inherit !important;
					font-family: inherit !important;
					font-weight: inherit !important;
					line-height: inherit !important;
				}
				.a6S {
					display: none !important;
					opacity: 0.01 !important;
				}
				.im {
					color: inherit !important;
				}
				img.g-img + div {
					display: none !important;
				}
				@media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
					u ~ div .email-container {
						min-width: 320px !important;
					}
				}
				@media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
					u ~ div .email-container {
						min-width: 375px !important;
					}
				}
				@media only screen and (min-device-width: 414px) {
					u ~ div .email-container {
						min-width: 414px !important;
					}
				}
				</style>
				<!-- CSS Reset : END -->
				<!-- Progressive Enhancements : BEGIN -->
				<style>
				.primary{
					background: #2f89fc;
				}
				.bg_white{
					background: #ffffff;
				}
				.bg_light{
					background: #fafafa;
				}
				.bg_black{
					background: #000000;
				}
				.bg_dark{
					background: rgba(0,0,0,.8);
				}
				.email-section{
					padding:2.5em;
				}
				.btn{
					padding: 5px 15px;
					display: inline-block;
				}
				.btn.btn-primary{
					border-radius: 5px;
					background: #2f89fc;
					color: #ffffff;
				}
				.btn.btn-white{
					border-radius: 5px;
					background: #ffffff;
					color: #000000;
				}
				.btn.btn-white-outline{
					border-radius: 5px;
					background: transparent;
					border: 1px solid #fff;
					color: #fff;
				}
				h1,h2,h3,h4,h5,h6{
					font-family: Work Sans, sans-serif;
					color: #000000;
					margin-top: 0;
					font-weight: 400;
				}
				body{
					font-family: Work Sans, sans-serif;
					font-weight: 400;
					font-size: 15px;
					line-height: 1.8;
					color: rgba(0,0,0,.4);
				}
				a{
					color: #2f89fc;
				}
				table{
				}
				.logo h1{
					margin: 0;
				}
				.logo h1 a{
					color: #000000;
					font-size: 20px;
					font-weight: 700;
					text-transform: uppercase;
					font-family: Poppins, sans-serif;
				}
				.navigation{
					padding: 0;
				}
				.navigation li{
					list-style: none;
					display: inline-block;;
					margin-left: 5px;
					font-size: 13px;
					font-weight: 500;
				}
				.navigation li a{
					color: rgba(0,0,0,.4);
				}
				.hero{
					position: relative;
					z-index: 0;
				}
				.hero .text{
					color: rgba(0,0,0,.3);
				}
				.hero .text h2{
					color: #000;
					font-size: 30px;
					margin-bottom: 0;
					font-weight: 300;
				}
				.hero .text h2 span{
					font-weight: 600;
					color: #2f89fc;
				}
				.heading-section{
				}
				.heading-section h2{
					color: #000000;
					font-size: 28px;
					margin-top: 0;
					line-height: 1.4;
					font-weight: 400;
				}
				.heading-section .subheading{
					margin-bottom: 20px !important;
					display: inline-block;
					font-size: 13px;
					text-transform: uppercase;
					letter-spacing: 2px;
					color: rgba(0,0,0,.4);
					position: relative;
				}
				.heading-section .subheading::after{
					position: absolute;
					left: 0;
					right: 0;
					bottom: -10px;
					width: 100%;
					height: 2px;
					background: #2f89fc;
					margin: 0 auto;
				}
				.heading-section-white{
					color: rgba(255,255,255,.8);
				}
				.heading-section-white h2{
					font-family: 
					line-height: 1;
					padding-bottom: 0;
				}
				.heading-section-white h2{
					color: #ffffff;
				}
				.heading-section-white .subheading{
					margin-bottom: 0;
					display: inline-block;
					font-size: 13px;
					text-transform: uppercase;
					letter-spacing: 2px;
					color: rgba(255,255,255,.4);
				}
				.text-project{
					padding-top: 10px;
				}
				.text-project h3{
					margin-bottom: 0;
				}
				.text-project h3 a{
					color: #000;
				}
				.footer{
					color: rgba(255,255,255,.5);
				}
				.footer .heading{
					color: #ffffff;
					font-size: 20px;
				}
				.footer ul{
					margin: 0;
					padding: 0;
				}
				.footer ul li{
					list-style: none;
					margin-bottom: 10px;
				}
				.footer ul li a{
					color: rgba(255,255,255,1);
				}
				@media screen and (max-width: 500px) {
				}
				</style>
				</head>
				<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #222222;">
				<center style="width: 100%; background-color: #f1f1f1;">
				<div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
				&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
				</div>
				<div style="max-width: 600px; margin: 0 auto;" class="email-container">
				<!-- BEGIN BODY -->
				<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
				<tr>
				<td valign="top" class="bg_white" style="padding: 1em 2.5em;">
				<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				<td class="logo" style="text-align: center;">
				<h1><a href="#">Carepro</a></h1>
				</td>
				</tr>
				</table>
				</td>
				</tr>
				<!-- end tr -->
				<tr>
				<td valign="middle" class="hero hero-2 bg_white" style="padding: 4em 0;">
				<table>
				<tr>
				<td>
				<div class="text" style="padding: 0 3em; text-align: center;">
				<h2>' . (!empty($sender_info["name"]) ? $sender_info["name"] . " " . $sender_info["lastname"] : "User") . ' share some photo with you. Please have a look.</h2>
				</div>
				</td>
				</tr>
				</table>
				</td>
				</tr>
				<!-- end tr -->
				<tr>
				<td class="bg_white">
				<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td class="bg_white email-section">
				<div class="heading-section" style="text-align: center; padding: 0 30px;">
				<h2>Photos</h2>
				<!-- <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p> -->
				</div>
				<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">';
				if (!empty($photoarr)) {
					foreach ($photoarr as $k => $v) {
						if (!empty($v)) {
							$message = $message . '<tr>';
							foreach ($v as $k1 => $v1) {
								$photo_url = base_url() . "/uploads/photos/" . $v1["u_photo"];
								$message = $message . '<td valign="top" width="50%">
								<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
								<td style="padding-top: 20px; padding-right: 10px;">
								<a href="#"><img src="' . $photo_url . '" alt="" style="width: 100%; max-width: 600px; height: auto; margin: auto; display: block;"></a>
								</td>
								</tr>
								</table>
								</td>';
							}
							$message = $message . '</tr>';
						}
					}
				}
				$message = $message . '</table>
				</td>
				</tr>
				<!-- end: tr -->
				</table>
				</td>
				</tr>
				<tr>
				<td valign="middle" class="hero hero-2 bg_white" style="padding: 4em 0;">
				<table>
				<tr>
				<td>
				<div class="text" style="text-align: center;">
				<h2>' . (!empty($description) ? $description : "") . '</h2>
				</div>
				</td>
				</tr>
				</table>
				</td>
				</tr>
				<!-- end:tr -->
				<!-- 1 Column Text + Button : END -->
				</table>
				<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
				<!-- end: tr -->
				<tr>
				<td valign="middle" class="bg_black footer email-section">
				<table style="width: 100%;">
				<tr>
				<td valign="top" colspan="2" width="33.333%">
				<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td style="text-align: left; padding-right: 10px;">
				<p>&copy; 2020 Carepro. All Rights Reserved</p>
				</td>
				</tr>
				</table>
				</td>
				</tr>
				</table>
				</td>
				</tr>
				</table>
				</div>
				</center>
				</body>
				</html>';
				$chk_mail = new_send_mail($message, $subject, $user_name, '');
				//}
			}
			//echo $user_name; die;
			/*print_r($user_arr);
			print_r($photo_arr);*/
			//$this->common_model->getAllRecordsById($table,$conditions);
			$receiverids = implode(',', $receiver_ids);
			$photoids = implode(',', $photo_ids);
			$record_data = array(
				'sender_id' => $sender_id,
				'receiver_ids' => $receiverids,
				'photo_ids' => $photoids,
				'description' => (!empty($description) ? $description : ''),
				'status' => 1,
				'created_at' => date('Y-m-d H:i:s'),
				'created_at' => date('Y-m-d H:i:s')
			);
			$record_id = $this->common_model->addRecords('share_gallery', $record_data);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Photo has been shared!');
			$this->response($resp);
		}
	}
	public function shareArticla_post()
	{
		$data = $this->param;
		$object_info = $data;
		//$required_parameter = array('sender_id',"receiver_ids","article_id");
		$required_parameter = array('sender_id', "email_ids", "article_id", "spritulity");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		} else {
			$sender_id = $data['sender_id'];
			//$receiver_ids = $data['receiver_ids'];
			$email_ids = $data['email_ids'];
			$article_id = $data['article_id'];
			$spritulity = $data['spritulity'];
			$sender_info = $this->common_model->getSingleRecordById('cp_users', array('id' => $sender_id));
			//$user_arr = $this->common_model->getWhereIn('cp_users','id',$receiver_ids);
			if ($spritulity == 0) {
				$article_info = $this->common_model->getSingleRecordById('cp_articles', array('article_id' => $article_id));
			} else {
				$article_info = $this->common_model->getSingleRecordById('cp_meditation_articles', array('article_id' => $article_id));
			}
			//if (!empty($user_arr)) {
			//foreach ($user_arr as $key => $value) {
			$value['email'] = $email_ids;
			$user_name = $value['email'];
			$subject = 'Carepro - Share Article';
			$message = '<!DOCTYPE html>
			<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
			<head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="x-apple-disable-message-reformatting">
			<title></title>
			<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,600,700|Lato:300,400,700" rel="stylesheet">
			<style>
			html,
			body {
				margin: 0 auto !important;
				padding: 0 !important;
				height: 100% !important;
				width: 100% !important;
				background: #f1f1f1;
			}
									         * {
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%;
		}
		div[style*="margin: 16px 0"] {
			margin: 0 !important;
		}
		table,
		td {
			mso-table-lspace: 0pt !important;
			mso-table-rspace: 0pt !important;
		}
		table {
			border-spacing: 0 !important;
			border-collapse: collapse !important;
			table-layout: fixed !important;
			margin: 0 auto !important;
		}
		img {
			-ms-interpolation-mode:bicubic;
		}
		a {
			text-decoration: none;
		}
									         *[x-apple-data-detectors],
		.unstyle-auto-detected-links *,
		.aBn {
			border-bottom: 0 !important;
			cursor: default !important;
			color: inherit !important;
			text-decoration: none !important;
			font-size: inherit !important;
			font-family: inherit !important;
			font-weight: inherit !important;
			line-height: inherit !important;
		}
		.a6S {
			display: none !important;
			opacity: 0.01 !important;
		}
		.im {
			color: inherit !important;
		}
		img.g-img + div {
			display: none !important;
		}
		@media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
			u ~ div .email-container {
				min-width: 320px !important;
			}
		}
		@media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
			u ~ div .email-container {
				min-width: 375px !important;
			}
		}
		@media only screen and (min-device-width: 414px) {
			u ~ div .email-container {
				min-width: 414px !important;
			}
		}
		</style>
		<style>
		.primary{
			background: #448ef6;
		}
		.bg_white{
			background: #ffffff;
		}
		.bg_light{
			background: #fafafa;
		}
		.bg_black{
			background: #000000;
		}
		.bg_dark{
			background: rgba(0,0,0,.8);
		}
		.email-section{
			padding:2.5em;
		}
		.btn{
			padding: 5px 15px;
			display: inline-block;
		}
		.btn.btn-primary{
			border-radius: 30px;
			background: #448ef6;
			color: #ffffff;
		}
		.btn.btn-white{
			border-radius: 30px;
			background: #ffffff;
			color: #000000;
		}
		.btn.btn-white-outline{
			border-radius: 30px;
			background: transparent;
			border: 1px solid #fff;
			color: #fff;
		}
		h1,h2,h3,h4,h5,h6{
			font-family: Josefin Sans, sans-serif;
			color: #000000;
			margin-top: 0;
			font-weight: 400;
		}
		body{
			font-family: Josefin Sans, sans-serif;
			font-weight: 400;
			font-size: 15px;
			line-height: 1.8;
			color: rgba(0,0,0,.4);
		}
		a{
			color: #448ef6;
		}
		table{
		}
		.logo{
			margin: 0;
			display: inline-block;
			position: absolute;
			top: 10px;
			left: 0;
			right: 0;
			margin-bottom: 0;
		}
		.logo a{
			color: #fff;
			font-size: 24px;
			font-weight: 700;
			text-transform: uppercase;
			font-family: Josefin Sans, sans-serif;
			display: inline-block;
			border: 2px solid #fff;
			line-height: 1.3;
			padding: 10px 15px 4px 15px;
			margin: 0;
		}
		.logo h1 a span{
			line-height: 1;
		}
		.navigation{
			padding: 0;
		}
		.navigation li{
			list-style: none;
			display: inline-block;;
			margin-left: 5px;
			font-size: 13px;
			font-weight: 500;
		}
		.navigation li a{
			color: rgba(0,0,0,.4);
		}
		.hero{
			position: relative;
			z-index: 0;
		}
		.hero .overlay{
			position: absolute;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			width: 100%;
			background: #000000;
			z-index: -1;
			opacity: .3;
		}
		.hero .text{
			color: rgba(255,255,255,.9);
		}
		.hero .text h2{
			color: #fff;
			font-size: 40px;
			margin-bottom: 0;
			font-weight: 600;
			line-height: 1;
			text-transform: uppercase;
		}
		.hero .text h2 span{
			font-weight: 600;
			color: #448ef6;
		}
		.heading-section{
		}
		.heading-section h2{
			color: #000000;
			font-size: 28px;
			margin-top: 0;
			line-height: 1.4;
			font-weight: 700;
			text-transform: uppercase;
			letter-spacing: 1px;
		}
		.heading-section .subheading{
			margin-bottom: 20px !important;
			display: inline-block;
			font-size: 13px;
			text-transform: uppercase;
			letter-spacing: 2px;
			color: rgba(0,0,0,.4);
			position: relative;
		}
		.heading-section .subheading::after{
			position: absolute;
			left: 0;
			right: 0;
			bottom: -10px;
			width: 100%;
			height: 2px;
			background: #448ef6;
			margin: 0 auto;
		}
		.heading-section-white{
			color: rgba(255,255,255,.8);
		}
		.heading-section-white h2{
			font-family: 
			line-height: 1;
			padding-bottom: 0;
		}
		.heading-section-white h2{
			color: #ffffff;
		}
		.heading-section-white .subheading{
			margin-bottom: 0;
			display: inline-block;
			font-size: 13px;
			text-transform: uppercase;
			letter-spacing: 2px;
			color: rgba(255,255,255,.4);
		}
		.blog-entry{
			border: 1px solid red;
			padding-bottom: 30px !important !important;
		}
		.text-blog .meta{
			text-transform: uppercase;
			font-size: 13px;
			margin-bottom: 0;
		}
		.footer{
			color: rgba(255,255,255,.5);
		}
		.footer .heading{
			color: #ffffff;
			font-size: 20px;
		}
		.footer ul{
			margin: 0;
			padding: 0;
		}
		.footer ul li{
			list-style: none;
			margin-bottom: 10px;
		}
		.footer ul li a{
			color: rgba(255,255,255,1);
		}
		@media screen and (max-width: 500px) {
		}
		</style>
		</head>
		<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #222222;">
		<center style="width: 100%; background-color: #f1f1f1;">
		<div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
		&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
		</div>
		<div style="max-width: 600px; margin: 0 auto;" class="email-container">
		<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
		<tr>
		<td class="bg_white email-section">
		<div class="heading-section" style="text-align: center; padding: 0 30px;">
		<h2>' . (!empty($sender_info["name"]) ? $sender_info["name"] . " " . $sender_info["lastname"] : "User") . ' share article with you. Please have a look.</h2>
		</div>
		<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
		<td style="padding-bottom: 30px;">
		<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
		<td valign="middle" width="100%">
		<img src="' . base_url() . '/uploads/articles/' . $article_info["artist_image"] . '" alt="" style="width: 100px;max-width: 600px;height: auto;margin: auto auto 20px;display: block;border-radius: 50%;">
		<h3 class="name" style="text-align: center;">' . $article_info["artist_name"] . '</h3>
		</td>
		</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
		<td class="bg_white">
		<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td>
		<a href="#"><img src="' . base_url() . '/uploads/articles/' . $article_info["article_image"] . '" alt="" style="width: 100%; max-width: 600px; height: auto; margin: auto; display: block;"></a>
		</td>
		</tr>
		<tr>
		<td class="text-blog" style="text-align: center; padding: 2em 2.5em">
		<p class="meta"><span>Posted on ' . (!empty($article_info["article_created"]) ? date("d M Y", strtotime($article_info["article_created"])) : "N/A") . '</span> </p>
		<h3 style="font-size: 24px;">' . $article_info["article_title"] . '</h3>
		<p>' . $article_info["article_desc"] . '</p>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
		<tr>
		<td valign="middle" class="bg_black footer email-section">
		<table>
		<tr>
		<td valign="top" width="33.333%">
		<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td style="text-align: left; padding-right: 10px;">
		<p>&copy; 2020 Carepro. All Rights Reserved</p>
		</td>
		</tr>
		</table>
		</td>
		<td valign="top" width="33.333%">
		<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td style="text-align: right; padding-left: 5px; padding-right: 5px;">
		<p>
		</p>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		</div>
		</center>
		</body>
		</html>';
			$chk_mail = new_send_mail($message, $subject, $user_name, '');
			//}
			//}
			/*print_r($user_arr);
			print_r($photo_arr);*/
			//$this->common_model->getAllRecordsById($table,$conditions);
			$emailids = implode(',', $email_ids);
			//$receiverids = implode(',', $receiver_ids);
			$record_data = array(
				'sender_id' => $sender_id,
				'receiver_ids' => $emailids,
				'article_id' => $article_id,
				'status' => 1,
				'created_at' => date('Y-m-d H:i:s'),
				'created_at' => date('Y-m-d H:i:s')
			);
			$record_id = $this->common_model->addRecords('share_article', $record_data);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Article has been shared!');
			$this->response($resp);
		}
	}
	/* add reminder */
	public function reminder_add_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		//$required_parameter = array('title',"date","time","user_id","reminder_before","repeat");
		$required_parameter = array('title', "date", "time", "user_id", "reminder_before", "repeat", "snooze", 'after_time', 'for_time');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$post_data = array(
			'user_id' => $data['user_id'],
			'reminder_title' => $data['title'],
			'reminder_date' => $data['date'],
			'reminder_time' => $data['time'],
			'reminder_before' => $data['reminder_before'],
			'repeat' => $data['repeat'],
			'snooze' => $data['snooze'],
			'after_time' => $data['after_time'],
			'for_time' => $data['for_time'],
			'reminder_create' => date('Y-m-d H:i:s'),
		);
		$reminderId = $this->common_model->addRecords('cp_reminder', $post_data);
		if ($reminderId) {
			$reminderData = $this->common_model->getSingleRecordById('cp_reminder', array('reminder_id' => $reminderId));
			$reminderData1 = array(
				'id' => $reminderData['reminder_id'],
				'title' => $reminderData['reminder_title'],
				'Date' => $reminderData['reminder_date'],
				'Time' => $reminderData['reminder_time'],
				'user_id' => $reminderData['user_id'],
				'reminder_before' => $reminderData['reminder_before'],
				'repeat' => $reminderData['repeat'],
				'snooze' => $reminderData['snooze'],
				'after_time' => $reminderData['after_time'],
				'for_time' => $reminderData['for_time'],
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Reminder added successfully', 'response' => $reminderData1);
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function show_reminder_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		$reminderData = $this->common_model->getAllRecordsById('cp_reminder', array('user_id' => $data['user_id']));
		if (!empty($reminderData)) {
			foreach ($reminderData as $row) {
				if ($row['reminder_date'] == "0000-00-00") {
					$reminder_date = $row['reminder_date'];
				} else {
					$reminder_date = date("M d, Y", strtotime($row['reminder_date']));
				}
				$reminder[] = array(
					'id' => $row['reminder_id'],
					'title' => $row['reminder_title'],
					//'date' => $reminder_date,
					'date' => date('m-d-Y', strtotime($row['reminder_date'])),
					'time' => $row['reminder_time'],
					'user_id' => $row['user_id'],
					'reminder_before' => $row['reminder_before'],
					'repeat' => $row['repeat'],
					'snooze' => $row['snooze'],
					'after_time' => $row['after_time'],
					'for_time' => $row['for_time'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('reminder_data' => $reminder));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Reminder Data not found', 'response' => array('message' => 'Reminder Data not found'));
		}
		$this->response($resp);
	}
	/*delete reminder */
	public function reminderdelete_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('reminder_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$ids = explode(',', $data['reminder_id']);
		$reminderid = $this->common_model->deleteReminders($ids);
		if ($reminderid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('reminder_data' => "Reminder data deleted succesfully"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function updatereminder_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('title', "date", "time", "reminder_id", "reminder_before", "repeat", "snooze", 'after_time', 'for_time');
		//$required_parameter = array('title',"date","time","user_id","reminder_before","repeat","snooze",'after_time','for_time');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$post_data = array(
			'reminder_title' => $data['title'],
			'reminder_date' =>  $data['date'],
			'reminder_time' =>  $data['time'],
			'reminder_before' =>  $data['reminder_before'],
			'repeat' =>  $data['repeat'],
			'snooze' => $data['snooze'],
			'after_time' => $data['after_time'],
			'for_time' => $data['for_time'],
		);
		$reminderId = $this->common_model->updateRecords('cp_reminder', $post_data, array('reminder_id' => $data['reminder_id']));
		if ($reminderId) {
			$reminderData = $this->common_model->getSingleRecordById('cp_reminder', array('reminder_id' => $data['reminder_id']));
			$users1 = array(
				'id' => $reminderData['reminder_id'],
				'title' => $reminderData['reminder_title'],
				'date' => $reminderData['reminder_date'],
				'time' => $reminderData['reminder_time'],
				'reminder_before' => $reminderData['reminder_before'],
				'repeat' => $reminderData['repeat'],
				'snooze' => $reminderData['snooze'],
				'after_time' => $reminderData['after_time'],
				'for_time' => $reminderData['for_time'],
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Reminder update successfully', 'response' => $users1);
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function doctorCategory_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$limit = 4;
		// if(!isset($data['page_no'])){
		// 	$data['page_no']=1;
		// }
		$start  = (!empty($data['page_no']) - 1) * $limit;
		$con1['returnType'] = 'count';
		$table = "cp_doctor_categories";
		$category_count  = $this->Api_model->getRows($table, $con1);
		//echo $category_count;die;
		$pages = ceil($category_count / $limit);
		//echo $pages;die;
		$con['sorting'] = array("dc_id" => "DESC");
		if (isset($data['page_no'])) {
			$con['limit'] = $limit;
			$con['start'] = $start;
		}
		$categoryDatas  = $this->Api_model->getRows($table, $con);
		if (!empty($categoryDatas)) {
			foreach ($categoryDatas as $categoryData_details) {
				if (!empty($categoryData_details['dc_icon'])) {
					$img =	$categoryData_details['dc_icon'];
				} else {
					$img = 'catDefault.png';
				}
				$categoryeData[] = array(
					'id' => $categoryData_details['dc_id'],
					'name' => $categoryData_details['dc_name'],
					'icon' => base_url() . 'uploads/doctor_category/' . $img,
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('category_data' => $categoryeData));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Category not found', 'response' => array('message' => 'Category not found'));
		}
		$this->response($resp);
	}
	public function doctor_list_post1()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('category_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$limit = 5;
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$start  = ($data['page_no'] - 1) * $limit;
		$con1['returnType'] = 'count';
		$table = "cp_doctor";
		$con1['conditions'] = array("doctor_category_id" => $data['category_id']);
		$category_count  = $this->Api_model->getRows($table, $con1);
		$pages = ceil(count($category_count) / $limit);
		$con['sorting'] = array("doctor_id" => "DESC");
		$con['limit'] = $limit;
		$con['start'] = $start;
		$con['conditions'] = array("doctor_category_id" => $data['category_id']);
		$doctorDatas  = $this->Api_model->getRows($table, $con);
		if (!empty($doctorDatas)) {
			foreach ($doctorDatas as $doctorData_details) {
				$doctorData[] = array(
					'id' => $doctorData_details['doctor_id'],
					'name' => $doctorData_details['doctor_name'],
					'fees' => $doctorData_details['doctor_fees'],
					'address' => $doctorData_details['doctor_address'],
					'contact' => $doctorData_details['doctor_mob_no'],
					'image' => base_url() . 'uploads/doctor/' . $doctorData_details['doctor_pic'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('doctor_data' => $doctorData));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Doctor not found', 'response' => array('message' => 'Doctor not found'));
		}
		$this->response($resp);
	}
	// Add Restaurant data
	public function add_restaurnat_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id', "name", "image", "lat", "long", "address", "place_id", "rating", 'icon');
		$chk_error = check_required_value($required_parameter, $data['rests'][0]);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		for ($i = 0; $i < count($data['rests']); $i++) {
			$place_id = $this->common_model->getSingleRecordById('cp_restaurant', array('place_id' => $data['rests'][$i]['place_id'], "user_id" => $data['rests'][$i]['user_id']));
			if ($place_id) {
				continue;
			}
			$post_data = array(
				'user_id' => (!empty($data['rests'][$i]['user_id']) ? $data['rests'][$i]['user_id'] : ''),
				'rest_name' => (!empty($data['rests'][$i]['name']) ? $data['rests'][$i]['name'] : ''),
				'rest_image' => (!empty($data['rests'][$i]['image']) ? $data['rests'][$i]['image'] : ''),
				'rest_lat' => (!empty($data['rests'][$i]['lat']) ? $data['rests'][$i]['lat'] : ''),
				'rest_long' => (!empty($data['rests'][$i]['long']) ? $data['rests'][$i]['long'] : ''),
				'place_id' => (!empty($data['rests'][$i]['place_id']) ? $data['rests'][$i]['place_id'] : ''),
				'rating' => (!empty($data['rests'][$i]['rating']) ? $data['rests'][$i]['rating'] : ''),
				'rest_icon' => (!empty($data['rests'][$i]['icon']) ? $data['rests'][$i]['icon'] : ''),
				'rest_address' => (!empty($data['rests'][$i]['address']) ? $data['rests'][$i]['address'] : ''),
				'rest_created' => date('Y-m-d H:i:s'),
				'rating' => (!empty($data['rests'][$i]['rating']) ? $data['rests'][$i]['rating'] : '')
			);
			$restaurantId = $this->common_model->addRecords('cp_restaurant', $post_data);
		}
		$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Add Restaurant successfully', 'response' => array('message' => 'Add Restaurant successfully'));
		$this->response($resp);
	}
	// list restaurant data
	public function rest_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$limit = 10;
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$start  = ($data['page_no'] - 1) * $limit;
		$con1['returnType'] = 'count';
		$table = "cp_restaurant";
		$con1['conditions'] = array("user_id" => $data['user_id']);
		$category_count  = $this->Api_model->getRows($table, $con1);
		$pages = ceil(count($category_count) / $limit);
		$con['sorting'] = array("rest_id" => "DESC");
		$con['limit'] = $limit;
		$con['start'] = $start;
		$con['conditions'] = array("user_id" => $data['user_id']);
		$restaurantDatas  = $this->Api_model->getRows($table, $con);
		if (!empty($restaurantDatas)) {
			foreach ($restaurantDatas as $restaurantData_details) {
				$fav  = $this->getRestFav($restaurantData_details['rest_id']);
				$restaurantData[] = array(
					'id' => $restaurantData_details['rest_id'],
					'name' => $restaurantData_details['rest_name'],
					'image' => $restaurantData_details['rest_image'],
					'address' => $restaurantData_details['rest_address'],
					'user_id' => $restaurantData_details['user_id'],
					'lat' => $restaurantData_details['rest_lat'],
					'long' => $restaurantData_details['rest_long'],
					'rating' => $restaurantData_details['rating'],
					'fav' => $fav,
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('restaurant_data' => $restaurantData));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Restaurant not found', 'response' => array('message' => 'Restaurant not found'));
		}
		$this->response($resp);
	}
	public function add_fav_restaurant_post()
	{
		$data = $this->param;
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$object_info = $data;
		$required_parameter = array('restaurant_name', 'restaurant_image', 'restaurant_address', 'rating', 'place_id', 'user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$address  =  $this->get_address($data['lat'], $data['long']);
		$post_data = array(
			'user_id' => $data['user_id'],
			'fav_rest_name' => $data['restaurant_name'],
			'fav_rest_image' => $data['restaurant_image'],
			'fav_rest_address' => $data['restaurant_address'],	'fav_rest_rating' => $data['rating'],
			'fav_rest_place_id' => $data['place_id'],
			'fav_rest_created' => date('Y-m-d H:i:s'),
		);
		$restaurantId = $this->common_model->addRecords('cp_fav_restaurant', $post_data);
		if ($restaurantId) {
			$restaurantData = $this->common_model->getSingleRecordById('cp_fav_restaurant', array('fav_rest_id' => $restaurantId));
			$restaurantData1 = array(
				'id' => $restaurantData['fav_rest_id'],
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Favorite successfully', 'response' => array('fav_data' => "favorite successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function get_address($lat, $long)
	{
		$geocode = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&sensor=false&key=AIzaSyCJyDp4TLGUigRfo4YN46dXcWOPRqLD0gQ";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $geocode);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);
		$output = json_decode($response);
		$dataarray = get_object_vars($output);
		$address = $dataarray['results'][0]->formatted_address;
		return $address;
	}
	public function unfav_restaurant_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('place_id', 'user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$restid = $this->common_model->deleteRecords('cp_fav_restaurant', array('fav_rest_place_id' => $data['place_id'], 'user_id' => $data['user_id']));
		if ($restid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function restFav_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$limit = 5;
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$start  = ($data['page_no'] - 1) * $limit;
		$con1['returnType'] = 'count';
		$table = "cp_fav_restaurant";
		$con1['conditions'] = array("cp_fav_restaurant.user_id" => $data['user_id']);
		$category_count  = $this->Api_model->getRowsRestaurantnew($table, $con1);
		$pages = ceil(count($category_count) / $limit);
		$con['sorting'] = array("fav_rest_id" => "DESC");
		$con['limit'] = $limit;
		$con['start'] = $start;
		$con['conditions'] = array("cp_fav_restaurant.user_id" => $data['user_id']);
		$restaurantDatas  = $this->Api_model->getRowsRestaurantnew($table, $con);
		//print_r("<pre/>");
		//print_r($restaurantDatas);
		// die;
		if (!empty($restaurantDatas)) {
			foreach ($restaurantDatas as $restaurantData_details) {
				$restaurantData[] = array(
					'id' => $restaurantData_details['fav_rest_id'],
					'name' => $restaurantData_details['fav_rest_name'],
					'image' => $restaurantData_details['fav_rest_image'],
					'address' => $restaurantData_details['fav_rest_address'],
					'user_id' => $restaurantData_details['user_id'],
					'rating' => $restaurantData_details['fav_rest_rating'],
					'place_id' => $restaurantData_details['fav_rest_place_id']
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('restaurant_data' => $restaurantData));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Favorite Restaurant not found', 'response' => array('message' => 'Favorite Restaurant not found'));
		}
		$this->response($resp);
	}
	public function add_bank_post111()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id', "name", "image", "lat", "long", "address", "place_id", "rating", 'icon');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$address  =  $this->get_address($data['lat'], $data['long']);
		$post_data = array(
			'user_id' => $data['user_id'],
			'bank_name' => $data['name'],
			'bank_image' => $data['image'],
			'bank_lat' => $data['lat'],
			'bank_long' => $data['long'],
			'place_id' => $data['place_id'],
			'bank_rating' => $data['rating'],
			'bank_icon' => $data['icon'],
			'bank_address' => (!empty($address) ? $address : ''),
			'bank_created' => date('Y-m-d H:i:s'),
		);
		$bankId = $this->common_model->addRecords('cp_bank', $post_data);
		if ($bankId) {
			$bankData = $this->common_model->getSingleRecordById('cp_bank', array('bank_id' => $bankId));
			$bankData1 = array(
				'id' => $bankData['bank_id'],
				'name' => $bankData['bank_name'],
				'image' => $bankData['bank_image'],
				'lat' => $bankData['bank_lat'],
				'long' => $bankData['bank_long'],
				'address' => $bankData['bank_address'],
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Bank added successfully', 'response' => $bankData1);
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function add_bank_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id', "name", "image", "lat", "long", "address", "place_id", "rating", 'icon');
		$chk_error = check_required_value($required_parameter, $data['rests'][0]);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		for ($i = 0; $i < count($data['rests']); $i++) {
			$place_id = $this->common_model->getSingleRecordById('cp_bank', array('place_id' => $data['rests'][$i]['place_id'], "user_id" => $data['rests'][$i]['user_id']));
			if ($place_id) {
				continue;
			}
			$post_data = array(
				'user_id' => (!empty($data['rests'][$i]['user_id']) ? $data['rests'][$i]['user_id'] : ''),
				'rest_name' => (!empty($data['rests'][$i]['name']) ? $data['rests'][$i]['rest_name'] : ''),
				'rest_image' => (!empty($data['rests'][$i]['image']) ? $data['rests'][$i]['image'] : ''),
				'rest_lat' => (!empty($data['rests'][$i]['lat']) ? $data['rests'][$i]['lat'] : ''),
				'rest_long' => (!empty($data['rests'][$i]['long']) ? $data['rests'][$i]['long'] : ''),
				'place_id' => (!empty($data['rests'][$i]['place_id']) ? $data['rests'][$i]['place_id'] : ''),
				'rating' => (!empty($data['rests'][$i]['rating']) ? $data['rests'][$i]['rating'] : ''),
				'rest_icon' => (!empty($data['rests'][$i]['icon']) ? $data['rests'][$i]['icon'] : ''),
				'rest_address' => (!empty($data['rests'][$i]['address']) ? $data['rests'][$i]['address'] : ''),
				'rest_created' => date('Y-m-d H:i:s'),
				'rating' => (!empty($data['rests'][$i]['rating']) ? $data['rests'][$i]['rating'] : '')
			);
			$restaurantId = $this->common_model->addRecords('cp_bank', $post_data);
		}
		$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Add Bank successfully', 'response' => array('message' => 'Add Bank successfully'));
		$this->response($resp);
	}
	public function add_fav_bank_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter =  array('bank_name', 'bank_image', 'bank_address', 'rating', 'place_id', 'user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		// $address  =  $this->get_address($data['lat'],$data['long']);
		$post_data = array(
			'user_id' => $data['user_id'],
			'fav_bank_name' => $data['bank_name'],
			'fav_bank_image' => $data['bank_image'],
			'fav_bank_address' => $data['bank_address'],	'fav_bank_rating' => $data['rating'],
			'fav_place_id' => $data['place_id'],
			'fav_bank_created' => date('Y-m-d H:i:s'),
		);
		$bankId = $this->common_model->addRecords('cp_fav_banks', $post_data);
		if ($bankId) {
			$bankData = $this->common_model->getSingleRecordById('cp_fav_banks', array('fav_bank_id' => $bankId));
			$bankData1 = array(
				'id' => $bankData['fav_bank_id'],
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Favorite successfully', 'response' => array('message' => 'Favorite successfully'));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function bank_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$limit = 10;
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$start  = ($data['page_no'] - 1) * $limit;
		$con1['returnType'] = 'count';
		$table = "cp_bank";
		$con1['conditions'] = array("user_id" => $data['user_id']);
		$bank_count  = $this->Api_model->getRows($table, $con1);
		$pages = ceil(count($bank_count) / $limit);
		$con['sorting'] = array("bank_id" => "DESC");
		$con['limit'] = $limit;
		$con['start'] = $start;
		$con['conditions'] = array("user_id" => $data['user_id']);
		$bankDatas  = $this->Api_model->getRows($table, $con);
		if (!empty($bankDatas)) {
			foreach ($bankDatas as $bankDatas_details) {
				$fav  = $this->getBankFav($bankDatas_details['bank_id']);
				$bankData[] = array(
					'id' => $bankDatas_details['bank_id'],
					'name' => $bankDatas_details['bank_name'],
					'image' => $bankDatas_details['bank_image'],
					'address' => $bankDatas_details['bank_address'],
					'user_id' => $bankDatas_details['user_id'],
					'rating' => $bankDatas_details['bank_rating'],
					'fav' => $fav,
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('bank_data' => $bankData));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Bank not found', 'response' => array('message' => 'Bank not found'));
		}
		$this->response($resp);
	}
	public function fav_bank_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$limit = 10;
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$start  = ($data['page_no'] - 1) * $limit;
		$con1['returnType'] = 'count';
		$table = "cp_fav_banks";
		$con1['conditions'] = array("cp_fav_banks.user_id" => $data['user_id']);
		$bank_count  = $this->Api_model->getRowsBanknew($table, $con1);
		$pages = ceil(count($bank_count) / $limit);
		$con['sorting'] = array("cp_fav_banks.bank_id" => "DESC");
		$con['limit'] = $limit;
		$con['start'] = $start;
		$con['conditions'] = array("cp_fav_banks.user_id" => $data['user_id']);
		$bankDatas  = $this->Api_model->getRowsBanknew($table, $con);
		//  echo $this->db->last_query();die;
		if (!empty($bankDatas)) {
			foreach ($bankDatas as $bankDatas_details) {
				$bankData[] = array(
					'id' => $bankDatas_details['fav_bank_id'],
					'name' => $bankDatas_details['fav_bank_name'],
					'image' => $bankDatas_details['fav_bank_image'],
					'address' => $bankDatas_details['fav_bank_address'],
					'user_id' => $bankDatas_details['user_id'],
					'rating' => $bankDatas_details['fav_bank_rating'],
					'place_id' => $bankDatas_details['fav_place_id']
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('bank_data' => $bankData));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Fav Bank not found', 'response' => array('message' => 'Fav Bank not found'));
		}
		$this->response($resp);
	}
	public function unfav_bank_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('place_id', 'user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$restid = $this->common_model->deleteRecords('cp_fav_banks', array('fav_place_id' => $data['place_id'], 'user_id' => $data['user_id']));
		if ($restid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function doctor_available_time_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('doctor_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$table = "dr_available_date";
		if ($data['date']) {
			$date = $data['date'];
		} else {
			$date = date('Y-m-d');
		}
		$con['conditions'] = array("avdate_date" => $date, 'avdate_dr_id' => $data['doctor_id']);
		$doctorAvail  = $this->Api_model->getRows($table, $con);
		if (!empty($doctorAvail)) {
			foreach ($doctorAvail as $doctorAvail_details) {
				$availTime  = $this->getTimeSchedule($doctorAvail_details['avdate_id']);
				$doctorAvail_details1[] = array(
					'id' => $doctorAvail_details['avdate_id'],
					'Available_data' => $doctorAvail_details['avdate_date'],
					'Available_time' => $availTime,
					// 'Available_time_slot'=>$availTime,
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('doctor_avail_data' => $doctorAvail_details1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Doctor not Available', 'response' => array('message' => 'Doctor not Available'));
		}
		$this->response($resp);
	}
	public function  getTimeSchedule($avdate_id)
	{
		$this->db->select('*');
		$this->db->from('dr_available_time');
		$this->db->where('avtime_date_id', $avdate_id);
		$query = $this->db->get();
		$result = $query->result_array();
		foreach ($result as $result_details) {
			$imageArray[] = array('id' => $result_details['avtime_id'], 'name' => $result_details['avtime_text'], 'time_slot' => $result_details['avtime_day_slot']);
		}
		return $imageArray;
	}
	public function add_appointment_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('doctor_id', "user_id", "date_id", "time_id");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$new_appint = $this->common_model->getsingle("doctor_appointments", array("doctor_id" => $data['doctor_id'], "dr_appointment_date_id" => $data['date_id'], "dr_appointment_time_id" => $data['time_id']));
		// echo "<pre>";
		// print_r($new_appint);
		// die;
		if (empty($new_appint)) {
			$post_data = array(
				'doctor_id' => $data['doctor_id'],
				'user_id' => $data['user_id'],
				'dr_appointment_date_id' => $data['date_id'],
				'dr_appointment_time_id' => $data['time_id'],
				'appointments_reminder' => (!empty($data['reminder']) ? $data['reminder'] : 0),
			);
			$drAppId = $this->common_model->addRecords('doctor_appointments', $post_data);
			// -------------------------Email functionality start-------------------------
			$doctor_id = $data['doctor_id'];
			$user_id = $data['user_id'];
			$appointment_id = $drAppId;
			// echo $doctor_id;
			// die; 
			$docDetail = $this->common_model->getSingleRecordById('cp_doctor', array('doctor_id' => $doctor_id));
			//$docDetail = $this->common_model_new->getsingle('cp_doctor',array('doctor_id'=>$doctor_id));
			// echo "<pre>";
			// print_r($docDetail);
			// die;
			// user deetail
			$userDetail = $this->common_model->getSingleRecordById('cp_users', array('id' => $user_id));
			$name = $userDetail['name'];
			$lastname = $userDetail['lastname'];
			$usrfullname = $name . ' ' . $lastname;
			$usremail = $userDetail['email'];
			$usrmobile = $userDetail['mobile'];
			// echo "<pre>";
			// print_r($userDetail['id']);
			// die;
			//Appointment detail
			$appointment_data = $this->common_model->getAppointmentDetail($appointment_id);
			// echo "<pre>";
			// print_r($appointment_data);
			// die;
			//$appointment_data[0]->doctor_id
			$apointDate = date('d-m-Y', strtotime($appointment_data[0]->avdate_date));
			$apointTime = $appointment_data[0]->avtime_text;
			$doctorEmail = $docDetail['doctor_email'];
			$doctorName = $docDetail['doctor_name'];
			$TemplateData = $this->common_model->getSingleRecordById('mail_template', array('template_id' => 6));
			//$TemplateData = $this->common_model_new->getsingle('mail_template',array('template_id'=>6));
			$findArray = array("%Drname%", "%Date%", "%Time%", "%name%", "%email%", "%Mobile%");
			$replaaceArray = array($doctorName, $apointDate, $apointTime, $usrfullname, $usremail, $usrmobile);
			$description = str_replace($findArray, $replaaceArray, $TemplateData['description']);
			$message = $description;
			//$message = 'Dear '.$firstname.' '.$lastname.',<br><p>Welcome to our carepro app.</p><br><p>Please open tablet and please do further process and enjoy our services.</p><br><p>Best Wishes,</p><br><p>CarePro Team</p>';
			$subject = $TemplateData['subject'];
			// echo $message;
			// echo "<br>";
			// echo $subject;
			// echo "<br>";
			// echo $doctorEmail;
			// //echo "<br>";
			// die;
			$check = new_send_mail($message, $subject, $doctorEmail, '');
			// -------------------------End-------------------------
			if ($drAppId) {
				$drAppData = $this->Api_model->getAppointments($drAppId);
				$drAppData1 = array(
					'id' => $drAppData[0]['dr_appointment_id'],
					'selected_date' => $drAppData[0]['avdate_date'],
					'schedule_time' => $drAppData[0]['avtime_text'],
					'reminder' => $drAppData[0]['appointments_reminder'],
				);
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Appointment Added successfully', 'response' => $drAppData1);
			} else {
				$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
			}
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'This appointment time is already scheduled by a user, please select any other.', 'response' => array('message' => 'This appointment time is already scheduled by a user, please select any other.'));
		}
		$this->response($resp);
	}
	public function doctor_list_post()
	{
		//echo "Testing";die;
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('category_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$limit = 5;
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$start  = ($data['page_no'] - 1) * $limit;
		$category_count  = $this->Api_model->getDoctorListCount($data['category_id']);
		$pages = ceil(count($category_count) / $limit);
		/*$con['sorting'] = array("doctor_id"=>"DESC");
	$con['limit'] = $limit;
	$con['start'] = $start;
	$con['conditions'] = array("doctor_category_id"=>$data['category_id']);*/
		// $doctorDatas  = $this->Api_model->getDoctorList($data['category_id'],$limit,$start);
		$doctorDatas  = $this->Api_model->getDoctorList($data['category_id']);
		$categoryImage = $this->getCategoryImage($data['category_id']);
		if (!empty($categoryImage)) {
			$catImg = base_url() . 'uploads/doctor_category/' . $categoryImage;
		} else {
			$catImg = base_url() . 'uploads/doctor_category/catDefault.png';
		}
		if (!empty($doctorDatas)) {
			foreach ($doctorDatas as $doctorData_details) {
				if (!empty($doctorData_details['doctor_pic'])) {
					$docPic = base_url() . 'uploads/doctor/' . $doctorData_details['doctor_pic'];
				} else {
					$docPic = base_url() . 'uploads/doctor_category/def.png';
				}
				$doctorData[] = array(
					'id' => $doctorData_details['doctor_id'],
					'name' => $doctorData_details['doctor_name'],
					'fees' => $doctorData_details['doctor_fees'],
					'address' => $doctorData_details['doctor_address'],
					'contact' => $doctorData_details['doctor_mob_no'],
					'category_name' => $this->getCategory($doctorData_details['doctor_category_id']),
					'category_id' => $doctorData_details['doctor_category_id'],
					'image' => $catImg,
					'doctor_image' => $docPic,
					"doctor_email" => $doctorData_details['doctor_email'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('doctor_data' => $doctorData));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Doctor not found', 'response' => array('message' => 'Doctor not found'));
		}
		$this->response($resp);
	}
	public function getCategory($id)
	{
		$id = explode(',', $id);
		$this->db->select('dc_name');
		$this->db->from('cp_doctor_categories');
		$this->db->where_in('dc_id', $id);
		$query = $this->db->get();
		$result = $query->result_array();
		$last_names1 = array_column($result, 'dc_name');
		$result_data = implode(',', $last_names1);
		//print_r("<pre/>");
		//print_r($last_names1);
		//die;
		return $result_data;
	}
	public function getCategoryImage($category_id)
	{
		$this->db->select('dc_icon');
		$this->db->from('cp_doctor_categories');
		$this->db->where('dc_id', $category_id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result[0]['dc_icon'];
	}
	public function activity_details_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		if (!isset($data['to_date'])) {
		
			$data['to_date'] = '';
		}
		if (!isset($data['from_date'])) {
			$data['from_date'] = '';
		}
		$reminderData = $this->Api_model->getUserReminders($data['user_id'], $data['from_date'], $data['to_date']);
		
		$AppData = $this->Api_model->getUserAppointments($data['user_id'], $data['from_date'], $data['to_date']);
	
		if (empty($AppData)) {
			$reminder['Appointments'] = array();
		}
		if (empty($reminderData)) {
			$reminder['reminders'] = array();
		}
		//$reminder['reminders']= array();
		if (!empty($reminderData) or !empty($AppData)) {
			//$selected_time = '';
			$new_time_arr = array();
			foreach ($reminderData as $row) {
				if ($row['reminder_date'] == "0000-00-00") {
					$reminder_date = $row['reminder_date'];
				} else {
					$reminder_date = date("M d, Y", strtotime($row['reminder_date']));
				}
				if ($row['snooze'] != 'false') {
					$row['reminder_time'];
					$after_time = $row['after_time'];
					$for_time = $row['for_time'];
					$reminder_id = $row['reminder_id'];
					$increase_time = $row['reminder_time'];
					$newtime = '';
					//$newtime = array();
					for ($i = 0; $i < $for_time; $i++) {
						$new_time =  strtotime("+" . $after_time . "minutes", strtotime($increase_time));
						$new_time = date('h:i A', $new_time);
						$increase_time = $new_time;
						$newtime = $newtime . $new_time . ',';
						//$newtime[] = $new_time;
					}
					$new_time_arr = array($newtime);
				}
				// else{
				//   $new_time_arr[] = $row['reminder_time'];
				// }
				//  $times=array();
				//  for ($i=1; $i<=$row['for_time']; $i++) {
				//   if (empty($selected_time)) {
				//     $selectedTime =$row['reminder_time'];
				//     $selected_time = $row['reminder_time'];
				//   } else {
				//       //$selected_time = $selected_time+5;
				//     $selected_minutestime = strtotime("+".'5', strtotime($selected_time));
				//     $selectedTime =$selected_time;
				//   }
				//   $after_time = $row['after_time'];
				//   $endTime = strtotime("+".$after_time."minutes", strtotime($selectedTime));
				//   $time = date('h:i A', $endTime);
				//   $times=$time;
				// }
				//$reminder['reminders'][]='';
				$currentDate = date('Y-m-d');
				//if($row['reminder_date'] >= $currentDate){
				$reminder['reminders'][] = array(
					'id' => $row['reminder_id'],
					'title' => $row['reminder_title'],
					//'date' => $reminder_date,
					'date' => $row['reminder_date'],
					'actual_time' => $row['reminder_time'],
					'time' => $new_time_arr,
					'snooze' => $row['snooze'],
					'for_time' => $row['for_time'],
					'after_time' => $row['after_time'],
					'user_id' => $row['user_id'],
					'reminder_before' => $row['reminder_before'],
					'repeat' => $row['repeat'],
				);
				// if(empty($reminder['reminders'])){
				// 	echo "data";
				// }else{
				// 	echo "No data";
				// }
				//}
			}
			// echo "<pre>";
			// print_r($new_time_arr);
			// echo "</pre>";
			// die;
			//die('kfjdsk');
			foreach ($AppData as $row_data) {
				$doctordata = $this->common_model->getsingle("cp_doctor", array("doctor_id" => $row_data['doctor_id']));
				$currentDate = date('Y-m-d');
				//if($row_data['avdate_date'] >= $currentDate){
				$reminder['Appointments'][] = array(
					'id' => $row_data['dr_appointment_id'],
					'selected_date' => $row_data['avdate_date'],
					'schedule_time' => $row_data['avtime_text'],
					'user_id' => $row_data['user_id'],
					'doctorName' => $doctordata->doctor_name,
					'doctoraddress' => $doctordata->doctor_address,
				);
				// if(empty($reminder['Appointments'])){
				// 	echo "data";
				// }else{
				// 	echo "No data";
				// }
				//}
			}
			//echo "<pre>";
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('Activities' => $reminder));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Activity Data not found', 'response' => array('message' => 'Activity Data not found'));
		}
		$this->response($resp);
	}
	public function update_appointment_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array("date_id", "time_id", "appointment_id", "user_id");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$singledata = $this->common_model->getsingle("doctor_appointments", array('dr_appointment_id' => $data['appointment_id']));
		//$array_appoint = "doctor_id=".$singledata->doctor_id.' and dr_appointment_date_id='.$data['date_id'].' and dr_appointment_time_id='.$data['time_id'].' and '
		$new_appint = $this->common_model->getsingle("doctor_appointments", array("doctor_id" => $singledata->doctor_id, "dr_appointment_date_id" => $data['date_id'], "dr_appointment_time_id" => $data['time_id']));
		if (empty($new_appint) || $new_appint->user_id == $data['user_id']) {
			$post_data = array(
				//'doctor_id'=> $data['doctor_id'],
				// 'user_id'=> $data['user_id'],
				'dr_appointment_date_id' => $data['date_id'],
				'dr_appointment_time_id' => $data['time_id'],
				'appointments_reminder' => (!empty($data['reminder']) ? $data['reminder'] : 0),
			);
			$drAppId = $this->common_model->updateRecords('doctor_appointments', $post_data, array('dr_appointment_id' => $data['appointment_id']));
			if ($drAppId) {
				$drAppData = $this->Api_model->getAppointments($data['appointment_id']);
				$drAppData1 = array(
					'id' => $drAppData[0]['dr_appointment_id'],
					'selected_date' => $drAppData[0]['avdate_date'],
					'schedule_time' => $drAppData[0]['avtime_text'],
					'reminder' => $drAppData[0]['appointments_reminder'],
				);
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Appointment Updated successfully', 'response' => $drAppData1);
			} else {
				$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
			}
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'This appointment time is already scheduled by a user, please select any other.', 'response' => array('message' => 'This appointment time is already scheduled by a user, please select any other.'));
		}
		$this->response($resp);
	}
	public function smsapi1($otp, $otp_resend_type)
	{
		$apiKey = urlencode('AQGYL+5ECgs-8hDM6ND0expDH4NWQ5uydvdAnsUhsZ');
		// Message details
		$numbers = array($otp_resend_type);
		$sender = urlencode('TXTLCL');
		$message = rawurlencode('This is your reset otp message:' . $otp);
		$numbers = implode(',', $numbers);
		// Prepare data for POST request
		$data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
		// Send the POST request with cURL
		$ch = curl_init('https://api.textlocal.in/send/');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		// Process your response here
		return $response;
	}
	public function smsapi($otp, $mobile)
	{
		$apikey = "hGS9pj6kqEOA5DggePuVqQ";
		$apisender = "SMSTST";
		$msg = $otp;
		$num = $mobile;    // MULTIPLE NUMBER VARIABLE PUT HERE...!
		$ms = rawurlencode($msg);   //This for encode your message content
		$url = 'https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey=' . $apikey . '&senderid=' . $apisender . '&channel=2&DCS=0&flashsms=0&number=' . $num . '&text=' . $ms . '&route=1';
		//echo $url;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 2);
		$data = curl_exec($ch);
		//print_r("<pre/>");
		//print_r($data);die;
		//echo '<br/> <br/>';
		//print($data); /* result of API call*/
	}
	/* add medicine */
	public function add_medicine_schedule_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('name', "days", "time", "user_id", "doctor_id", "reminder");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		if (!empty($data['other_doctor_name'])) {
			$array_otherdoc = array('other_doctor_name' => $data['other_doctor_name'], 'doctor_name' => $data['other_doctor_name'], 'doctor_created' => date("Y-m-d H:i:s"));
			$new_doc = $this->common_model->addRecords('cp_doctor', $array_otherdoc);
		}
		if (!empty($data['other_doctor_name'])) {
			$doct = $new_doc;
		} else {
			$doct = $data['doctor_id'];
		}
		$post_data = array(
			'medicine_name' => $data['name'],
			'medicine_take_days' => $data['days'],
			'medicine_time' => $data['time'],
			'user_id' => $data['user_id'],
			'doctor_id' => $doct,
			'medicine_reminder' => $data['reminder'],
			'end_date' => $data['end_date'],
			'description' => $data['description'],
			'medicine_create_date' => date('Y-m-d H:i:s'),
		);
		$medicineId = $this->common_model->addRecords('medicine_schedule', $post_data);
		if ($medicineId) {
			$medicineData = $this->common_model->getSingleRecordById('medicine_schedule', array('medicine_id' => $medicineId));
			$medicineData1 = array(
				'id' => $medicineData['medicine_id'],
				'user_id' => $medicineData['user_id'],
				'doctor_id' => $medicineData['doctor_id'],
				'name' => $medicineData['medicine_name'],
				'time' => $medicineData['medicine_time'],
				'end_date' => $medicineData['end_date'],
				'description' => $medicineData['description'],
				'reminder' => $medicineData['medicine_reminder'],
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Medicine Schedule added successfully', 'response' => $medicineData1);
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function update_medicine_schedule_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('name', "days", "time", "medicine_id", "doctor_id", "reminder");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$medicineData = $this->common_model->getSingleRecordById('medicine_schedule', array('medicine_id' => $data['medicine_id']));
		if (!empty($data['other_doctor_name'])) {
			$this->common_model->updateRecords('cp_doctor', array("doctor_name" => $data['other_doctor_name'], 'other_doctor_name' => $data['other_doctor_name']), array("doctor_id" => $medicineData['doctor_id']));
			$newd = $this->common_model->getsingle('cp_doctor', array('doctor_id' => $medicineData['doctor_id']));
			$newvaldoc = $medicineData['doctor_id'];
		} else {
			$newvaldoc = $data['doctor_id'];
		}
		$post_data = array(
			'medicine_name' => $data['name'],
			'medicine_take_days' => $data['days'],
			'doctor_id' => $newvaldoc,
			'medicine_time' => $data['time'],
			'medicine_create_date' => $data['start_date'],
			'end_date' => $data['end_date'],
			'description' => $data['description'],
			'medicine_reminder' => $data['reminder'],
		);
		$medicineId = $this->common_model->updateRecords('medicine_schedule', $post_data, array('medicine_id' => $data['medicine_id']));
		if ($medicineId) {
			$users1 = array(
				'id' => $medicineData['medicine_id'],
				'user_id' => $medicineData['user_id'],
				'doctor_id' => $medicineData['doctor_id'],
				'name' => $medicineData['medicine_name'],
				'days' => $medicineData['medicine_take_days'],
				'start_date' => $medicineData['medicine_create_date'],
				'end_date' => $medicineData['end_date'],
				'description' => $medicineData['description'],
				'time' => $medicineData['medicine_time'],
				'reminder' => $medicineData['medicine_reminder'],
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Medicine updated successfully', 'response' => $users1);
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function show_medicine_schedule_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		$data_post['start_date'] = $data['start_date'];
		$data_post['end_date'] = $data['end_date'];
		//$medicineData = $this->common_model->getAllRecordsById('medicine_schedule', array('user_id' => $data['user_id']));
		$medicineData = $this->common_model->getDetailField(array("search" => $data_post), $data['user_id']);
		//echo '<pre>';print_r($this->db->last_query());die;
		//echo '<pre>';print_R($medicineData);die;
		if (!empty($medicineData)) {
			foreach ($medicineData as $row) {
				$currentDate = date('m-d-Y');
				$medicnDate = date('m-d-Y', strtotime($row->medicine_create_date));
				$end_date = date('m-d-Y', strtotime($row->end_date));
				//$Enteredend_date = date('m-d-Y',strtotime($data_post['end_date']));
				if ($medicnDate >= $currentDate || $end_date >= $currentDate) {
					//if($end_date <= $Enteredend_date){
					$medicine[] = array(
						'id' => $row->medicine_id,
						'user_id' => $row->user_id,
						'doctor_id' => $row->doctor_id,
						'name' => $row->medicine_name,
						'days' => $row->medicine_take_days,
						'time' => $row->medicine_time,
						'reminder' => $row->medicine_reminder,
						'end_date' => date('m-d-Y', strtotime($row->end_date)),
						'description' => $row->description,
						'medicine_date' => date('m-d-Y', strtotime($row->medicine_create_date)),
					);
					//}
				}
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('medicine_data' => $medicine));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Medicine Data not found', 'response' => array('message' => 'Medicine Data not found'));
		}
		$this->response($resp);
	}
	public function get_medicine_schedule_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('medicine_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		$medicineData = $this->common_model->getSingleRecordById('medicine_schedule', array('medicine_id' => $data['medicine_id']));
		//print_r("<pre/>");
		//print_r($medicineData);
		// die;
		if (!empty($medicineData)) {
			//foreach ( $medicineData as $row ){
			$medicine = array(
				'id' => $medicineData['medicine_id'],
				'user_id' => $medicineData['user_id'],
				'doctor_id' => $medicineData['doctor_id'],
				'name' => $medicineData['medicine_name'],
				'days' => $medicineData['medicine_take_days'],
				'time' => $medicineData['medicine_time'],
				'reminder' => $medicineData['medicine_reminder'],
				'medicine_date' => $medicineData['medicine_create_date'],
			);
			// }
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('medicine_data' => $medicine));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Favourite Data not found', 'response' => array('message' => 'Favourite Data not found'));
		}
		$this->response($resp);
	}
	public function send_sms($otp, $mobile_number)
	{
		//echo $mobile_number;die;
		//echo $mobile_number;die;
		//$mobile_number = trim($mobile_number);
		$account_sid = 'AC6746ef6886924e71439cb1ef3dc7595a';
		$auth_token = '5bddd7cc3a5a59b3a3e46b96708c0e71';
		$twilio_number = "+12512379625";
		//$twilio_number = "+15005550006";
		$client = new Client($account_sid, $auth_token);
		//echo $client;die;
		$client->messages->create(
			// Where to send a text message (your cell phone?)
			//"+1".trim($mobile_number),
			"+91" . $mobile_number,
			//"+1".$mobile_number,
			array(
				'from' => $twilio_number,
				'body' => 'This is your otp message:' . $otp
			)
		);
	}
	public function video_access_token_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('identity');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		$sid    = "AC5ac3a5fd26d52942e039e626af48a4b4";
		$token  = "598a5da83008bb2c918fb84aa2195add";
		$twilio = new Client($sid, $token);
		$new_key = $twilio->newKeys
			->create();
		$twilioAccountSid = 'AC5ac3a5fd26d52942e039e626af48a4b4';
		$twilioApiKey = $new_key->sid;
		$twilioApiSecret = $new_key->secret;
		$identity = $data['identity'];
		$token = new AccessToken(
			$twilioAccountSid,
			$twilioApiKey,
			$twilioApiSecret,
			3600,
			$identity
		);
		$roomName = $data['roomName'];
		$videoGrant = new VideoGrant();
		$videoGrant->setRoom($roomName);
		$token->addGrant($videoGrant);
		// render token to string
		echo $token->toJWT();
	}
	public function access_token_post()
	{
		//$account_sid = 'SKaeb6d7b26adb79e1d74b7f1889afaad4';
		//$auth_token = 'AIzaSyBZmM30jCgAaA4B1X-ND7L0fB8drAZaZZ0';
		//$client = new Client($account_sid, $auth_token);
		//$twilioAccountSid = 'AC5ac3a5fd26d52942e039e626af48a4b4';
		// $twilioApiKey = 'SKaeb6d7b26adb79e1d74b7f1889afaad4';
		// $twilioApiSecret = 'iR2Qaw0cg7PCwMLvtV8Oqxf6G03X1552';
		//$sid    = "AC50352170c8ed9744b2c4ccb3cf42f4c4";
		//$sid    = "AC5ac3a5fd26d52942e039e626af48a4b4";
		$sid    = "AC5ac3a5fd26d52942e039e626af48a4b4";
		//$token  = "c8ead357ad788b270266ee1d4a82e7bc";
		//$token  = "598a5da83008bb2c918fb84aa2195add";
		$token  = "598a5da83008bb2c918fb84aa2195add";
		$twilio = new Client($sid, $token);
		$new_key = $twilio->newKeys
			->create();
		//print_r($new_key);die;
		//$twilioAccountSid = 'AC50352170c8ed9744b2c4ccb3cf42f4c4';
		// $twilioApiKey = 'c8ead357ad788b270266ee1d4a82e7bc';
		//$twilioAccountSid = 'AC50352170c8ed9744b2c4ccb3cf42f4c4';
		//$twilioAccountSid = 'AC5ac3a5fd26d52942e039e626af48a4b4';
		$twilioAccountSid = 'AC5ac3a5fd26d52942e039e626af48a4b4';
		$twilioApiKey = $new_key->sid;
		//$twilioApiSecret = 'B8ZKYbl5XlbWS43ZRMGFFQ0JXQii2Pqp';
		$twilioApiSecret = $new_key->secret;
		//$outgoingApplicationSid = 'APdbdbe2a3099a474f9b09d0ecaf82b5d7';
		//$outgoingApplicationSid = 'AP13639ec86f321bfdf716757d7d45c056';
		$outgoingApplicationSid = 'AP13639ec86f321bfdf716757d7d45c056';
		$identity = "+12098782148";
		$token = new AccessToken(
			$twilioAccountSid,
			$twilioApiKey,
			$twilioApiSecret,
			3600,
			$identity
		);
		$voiceGrant = new VoiceGrant();
		$voiceGrant->setOutgoingApplicationSid($outgoingApplicationSid);
		//$voiceGrant->setOutgoingApplicationSid($outgoingApplicationSid);
		// $voiceGrant->setIncomingAllow(true);
		//$voiceGrant->setIncomingAllow(true);
		// Add grant to token
		$token->addGrant($voiceGrant);
		// render token to string
		echo $token->toJWT();
		die;
		//$client1 = new AccessToken("ACe2c924ce8254191edcea440ac8802b87","SKaeb6d7b26adb79e1d74b7f1889afaad4","iR2Qaw0cg7PCwMLvtV8Oqxf6G03X1552");
	}
	public function access_token1_post()
	{
		$sid    = "AC50352170c8ed9744b2c4ccb3cf42f4c4";
		$token  = "c8ead357ad788b270266ee1d4a82e7bc";
		$twilio = new Client($sid, $token);
		$new_key = $twilio->newKeys
			->create();
		//print_r($new_key);die;
		$incoming_phone_number = $twilio->incomingPhoneNumbers
			->create(
				array(
					"phoneNumber" => "+15005550006",
					"voiceUrl" => "http://demo.twilio.com/docs/voice.xml"
				)
			);
		$twilioAccountSid = 'AC50352170c8ed9744b2c4ccb3cf42f4c4';
		//$twilioApiKey = 'c8ead357ad788b270266ee1d4a82e7bc';
		$twilioApiKey = 'SKc94350436e67ad70e8db146a590272dc';
		$twilioApiSecret = 'B8ZKYbl5XlbWS43ZRMGFFQ0JXQii2Pqp';
		$outgoingApplicationSid = 'APdbdbe2a3099a474f9b09d0ecaf82b5d7';
		$token = new AccessToken(
			$twilioAccountSid,
			$twilioApiKey,
			$twilioApiSecret
		);
		$voiceGrant = new VoiceGrant();
		$voiceGrant->setOutgoingApplicationSid($outgoingApplicationSid);
		// $voiceGrant->setIncomingAllow(true);
		$token->addGrant($voiceGrant);
		// render token to string
		echo $token->toJWT();
		die;
	}
	// view doctor appointment
	public function show_appointments_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('appointment_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		$AppData = $this->Api_model->viewAppointments($data['appointment_id']);
		//print_r("<pre/>");
		//print_r($AppData);
		//die;
		if (!empty($AppData)) {
			foreach ($AppData as $row) {
				$appointment[] = array(
					'id' => $row['dr_appointment_id'],
					'doctor_name' => $row['doctor_name'],
					'doctor_id' => $row['doctor_id'],
					'doctor_fees' => $row['doctor_fees'],
					'doctor_address' => $row['doctor_address'],
					'contact' => $row['doctor_mob_no'],
					'date' => $row['avdate_date'],
					'time' => $row['avtime_text'],
					'category_name' => $this->getCategory($row['doctor_category_id']),
					'category_id' => $row['doctor_category_id'],
					'image' => base_url() . 'uploads/doctor/' . $row['doctor_pic'],
					'time_id' => $row['dr_appointment_time_id'],
					'date_id' => $row['dr_appointment_date_id'],
					'reminder' => $row['appointments_reminder'],
					'doctor_image' => base_url() . 'uploads/doctor/' . $row['doctor_pic'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('appointment_data' => $appointment));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Favourite Data not found', 'response' => array('message' => 'Favourite Data not found'));
		}
		$this->response($resp);
	}
	// view doctor appointment
	public function list_appointments_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		$AppData = $this->Api_model->showAppointments($data['user_id']);
		// echo "<pre>";
		// print_r($AppData);
		// die;
		if (!empty($AppData)) {
			foreach ($AppData as $row) {
				$currentDate = date('m-d-Y');
				$appointDate = date('m-d-Y', strtotime($row['avdate_date']));
				if ($appointDate >= $currentDate) {
					// echo "yes";
					// die;
					// $appointment[] = array(
					// 	'test'=> $appointDate,
					// 	'tes'=>date('m-d-Y H:i:s')
					// );
					$appointment[] = array(
						'id' => $row['dr_appointment_id'],
						'doctor_name' => $row['doctor_name'],
						'date' => date('m-d-Y', strtotime($row['avdate_date'])),
						'time' => $row['avtime_text'],
					);
				}
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('appointment_data' => $appointment));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Appointments Data not found', 'response' => array('message' => 'Appointments Data not found'));
		}
		$this->response($resp);
	}
	/*delete appointment */
	public function appointmentdelete_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('appointment_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$ids = explode(',', $data['appointment_id']);
		$emailid = $this->common_model->deleteAppointments($ids);
		if ($emailid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('appointment_data' => "Appointment deleted successfully"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Email not found', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/*----Doctor list---------*/
	public function get_all_doctor_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$doctors_list  = $this->Api_model->getAllor('cp_doctor', 'doctor_id', 'desc');
		if (!empty($doctors_list)) {
			foreach ($doctors_list as $doctor_details) {
				$doctorData[] = array('id' => $doctor_details->doctor_id, 'doctor_name' => $doctor_details->doctor_name);
				$test_other = array('id' => 0, 'doctor_name' => 'Other');
				$merged_array = array_merge($doctorData, array($test_other));
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('doctor_data' => $merged_array));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Doctor not found', 'response' => array('message' => 'Doctor not found'));
		}
		$this->response($resp);
	}
	/*-------Test type list--------*/
	public function get_all_test_type_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$test_type_list  = $this->Api_model->getAllor('cp_test_type', 'test_type', 'asc');
		if (!empty($test_type_list)) {
			foreach ($test_type_list as $type_details) {
				$typeData[] = array(
					'id' => $type_details->id,
					'test_type_name' => $type_details->test_type
				);
				$test_other = array('id' => 0, 'test_type_name' => 'Other');
				$merged_array = array_merge($typeData, array($test_other));
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('doctor_data' => $merged_array));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Test type not found', 'response' => array('message' => 'Test type not found'));
		}
		$this->response($resp);
	}
	/*-------Delete medicine schedule--------*/
	public function delete_medicine_schedule_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('medicine_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$ids = explode(',', $data['medicine_id']);
		$pickup_medicine = $this->common_model->getsingle("picup_medicine", array("medicine_id" => $data['medicine_id']));
		$acknowledge_medicine = $this->common_model->getsingle("acknowledge", array("medicine_id" => $data['medicine_id']));
		if (empty($pickup_medicine) && empty($acknowledge_medicine)) {
			$emailid = $this->common_model->deletemedicineschedule($ids);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('medicine_data' => "Medicine deleted successfully"));
		} else {
			$resp = array('status_code' => SUCCESS, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		if ($emailid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('medicine_data' => "Medicine deleted successfully"));
		} else {
			$resp = array('status_code' => SUCCESS, 'status' => 'false', 'message' => 'Medicine not found', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function delete_test_report_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('report_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$ids = explode(',', $data['report_id']);
		$emailid = $this->common_model->deletetestreport($ids);
		if ($emailid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('email_data' => "email data deleted succesfully"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Email not found', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function show_all_test_report1_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$limit = 5;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$emailCount = $this->common_model->gettestreportCount1($data['user_id']);
		$start  = ($data['page_no'] - 1) * $limit;
		$pages = ceil(count($emailCount) / $limit);
		$emailData = $this->common_model->getalltestreport($start, $limit, $data['user_id']);
		// print_r("<pre/>");
		//print_r($emailData);
		// die;
		if (!empty($emailData)) {
			foreach ($emailData as $row) {
				$doctor_name = $this->common_model->getsingle("cp_doctor", array("doctor_id" => $row['doctor_id']));
				$test_type = $this->common_model->getsingle("cp_test_type", array("id" => $row['test_type_id']));
				$username = $this->common_model->getsingle("cp_users", array("id" => $row['user_id']));
				// print_r($test_type);
				$email[] = array(
					'id' => $row['id'],
					'user_id' => $row['user_id'],
					'doctor_name' => $doctor_name->doctor_name,
					'test_type' => $test_type->test_type,
					'image' => base_url() . 'uploads/test_report/' . $row['image'],
					'user_name' => $username->username,
				);
			}
			//print_r($email);die;
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('email_data' => $email));
		} else {
			$email = array();
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Email not found', 'response' => array('message' => 'Email not found', 'email_data' => $email));
		}
		//print_r($resp);die;
		$this->response($resp);
	}
	public function test_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$limit = 5;
		if (!isset($data['page_no']) == '') {
			$data['page_no'] = 1;
		}
		/* $start  = ($data['page_no']-1)*$limit;
	$con1['returnType'] = 'count';
	$table = "cp_test_report";
	$category_count  = $this->Api_model->getRows($table,$con1);
	$pages = ceil($category_count / $limit);
	$con['sorting'] = array("id"=>"DESC");
	if($data['page_no']){
	$con['limit'] = $limit;
	$con['start'] = $start;
}*/
		$pages = '';
		$table = "cp_test_report";
		$con['sorting'] = array("id" => "DESC");
		$categoryDatas  = $this->Api_model->testlist($data['user_id']);
		if (!empty($categoryDatas)) {
			foreach ($categoryDatas as $categoryData_details) {
				if ($categoryData_details['test_type_id'] == '0') {
				}
				$testunit = $this->common_model->getsingle('cp_vital_test_type_unit', array('test_type_id' => $categoryData_details['type_id']));
				$categoryeData[] = array(
					'id' => $categoryData_details['id'],
					'name' => $categoryData_details['test_report_title'],
					'user_id' => $categoryData_details['user_id'],
					'description' => $categoryData_details['description'],
					'image' => base_url() . 'uploads/test_report/' . $categoryData_details['image'],
					'doctor_name' => $categoryData_details['doctor_name'],
					'doctor_id' => $categoryData_details['doctor_id'],
					'test_type_id' => $categoryData_details['type_id'],
					'test_type_name' => $categoryData_details['test_type_name'],
					'unit' => (!empty($testunit->unit) ? $testunit->unit : ''),
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('test_data' => $categoryeData));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Test Report not found', 'response' => array('message' => 'Test Report not found'));
		}
		$this->response($resp);
	}
	/*-------Acknowledge list--------*/
	public function social_list_post()
	{
		//die('check');  
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array("user_id");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$con['sorting'] = array("acknowledge_id" => "DESC");
		//$acknowledgeData  = $this->Api_model->acknowledgeList($data['user_id']);
		$socialData  = $this->common_model->getAllwhereorderby('socials', array('user_id' => $data['user_id']), 'id', 'desc');
		if (!empty($socialData)) {
			foreach ($socialData as $socialDetail) {
				$socialData1[] = array(
					'id' => $socialDetail->id,
					'user_id' => $socialDetail->user_id,
					'social_name' =>	$socialDetail->social_name,
					'type' => $socialDetail->type,
					'social_url' => $socialDetail->social_url,
					'app_url' =>	$socialDetail->app_url,
					'social_icon' => $socialDetail->social_icon,
					'updated' =>	$socialDetail->updated,
					'created' =>	$socialDetail->created,
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('social_data' => $socialData1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Social Data not found', 'response' => array('message' => 'Social Data not found'));
		}
		$this->response($resp);
	}
	public function btn_setting_list_post()
	{
		//die('check');  
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array("user_id");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$con['sorting'] = array("acknowledge_id" => "DESC");
		//$acknowledgeData  = $this->Api_model->acknowledgeList($data['user_id']);
		$socialData  = $this->common_model->getAllwhereorderby('setting_butns', array('settings' => 1, 'user_id' => $data['user_id']), 'id', 'desc');
		if (!empty($socialData)) {
			foreach ($socialData as $socialDetail) {
				$socialData1[] = array(
					'id' => $socialDetail->id,
					'user_id' => $socialDetail->user_id,
					'btnname' =>	$socialDetail->btnname,
					'btn_icon' => base_url() . 'assets/images/btn/' . $socialDetail->btnicon,
					'settings' => $socialDetail->settings,
					'created' => $socialDetail->created,
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('setting_data' => $socialData1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Setting Data not found', 'response' => array('message' => 'Setting Data not found'));
		}
		$this->response($resp);
	}
	
	public function btn_personal_detail_post()
	{
		$data = $this->param;
		$object_info = $data;
		

		$device_unique_id 		= isset($object_info['device_unique_id']) ? $object_info['device_unique_id'] : '';
		$device_user_id			= isset($object_info['device_user_id']) ? $object_info['device_user_id'] : '';
		$device_last_online		= isset($object_info['device_last_online']) ? $object_info['device_last_online'] : '';
		$device_os_version		= isset($object_info['device_os_version']) ? $object_info['device_os_version'] : '';
		$device_app_version		= isset($object_info['device_app_version']) ? $object_info['device_app_version'] : '';
		$device_connection_type	= isset($object_info['device_connection_type']) ? $object_info['device_connection_type'] : '';
		$device_signal_strength	= isset($object_info['device_signal_strength']) ? $object_info['device_signal_strength'] : '';
		$device_last_known_location	= isset($object_info['device_last_known_location']) ? $object_info['device_last_known_location'] : '';
		$device_battery_percentage	= isset($object_info['device_battery_percentage']) ? $object_info['device_battery_percentage'] : '';

		$required_parameter = array('device_user_id');
		
		/*Start new code*/
		$data = $this->common_model->getsingle("device_details", array("device_user_id" => $device_user_id));
		
		
		$device_kiosk_mode = isset($data->device_kiosk_mode) ? $data->device_kiosk_mode : '0';
		
		
		if (!empty($data) && $data->device_user_id == $device_user_id) {

			$update_array = array(
				"device_unique_id"				=> $device_unique_id,
				"device_last_known_location"	=> $device_last_known_location,
				"device_battery_percentage"		=> $device_battery_percentage,
				"device_last_online"			=> $device_last_online,
				"device_os_version"				=> $device_os_version,
				"device_app_version"			=> $device_app_version,
				"device_connection_type"		=> $device_connection_type,
				"device_signal_strength"		=> $device_signal_strength
			);

			$query = $this->common_model->updateRecords('device_details', $update_array, array('device_user_id' => $data->device_user_id));
			
			
			
			/* $response['message'] = "UPDATE DEVICE DATA SUCCESS', 'response";
			$response['ok'] = 1;
			$response['data'] = array('device_details' => $update_array);
				
			echo (json_encode($response));
			exit; */
		
		} else {
			$insert_array = array(
				'device_unique_id' 			=>	$device_unique_id,
				'device_user_id' 			=>	$device_user_id,
				'device_kiosk_mode' 		=>	'0',
				'device_btnname' 			=>	'devices',
				'device_last_known_location' =>	$device_last_known_location,
				'device_battery_percentage'	=>	$device_battery_percentage,
				'device_last_online'		=>	$device_last_online,
				'device_os_version'			=>	$device_os_version,
				'device_app_version'		=>	$device_app_version,
				'device_connection_type'	=>	$device_connection_type,
				'device_signal_strength'	=>	$device_signal_strength
			);

			$query = $this->common_model->addRecords('device_details', $insert_array);
			
			/*$response['message'] = "INSERT DEVICE DATA SUCCESS', 'response";
			$response['ok'] = 1;
			$response['data'] = array('device_details' => $insert_array);
			
			echo (json_encode($response));
			exit; */
		}

		/*End new code*/

		$chk_error = check_required_value($required_parameter, $object_info);
		//($chk_error);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));


			$this->response($resp);
		}

		// if($data['button_id']==1 || $data['button_id']==2){
		$btnData = $this->common_model->jointwotablenn("setting_butns", "type","cp_tabfeatures", "id",  array("setting_butns.user_id"=>$device_user_id,"setting_butns.settings"=>'1'), "setting_butns.id,setting_butns.type,setting_butns.btnicon,setting_butns.btnname,cp_tabfeatures.feature_icon,setting_butns.isSocialList,setting_butns.settings,setting_butns.tab_name,setting_butns.created as created,setting_butns.created as updated", "setting_butns.id", "desc");

		if (empty($btnData) || $btnData == 'Null') {
			$btnData = $this->common_model->jointwotablenn("setting_butns", "type","cp_tabfeatures", "id",  array("setting_butns.user_id"=>"883","setting_butns.settings"=>'1'), "setting_butns.id,setting_butns.type,setting_butns.btnicon,setting_butns.btnname,cp_tabfeatures.feature_icon,setting_butns.isSocialList,setting_butns.settings,setting_butns.tab_name,setting_butns.created as created,setting_butns.created as updated", "setting_butns.id", "desc");
		}

		$remove_duplicate = array();
		
		$btnDatad = array();
		if (sizeOf($btnData) > 0) {
			foreach ($btnData as $btnDataDetail) {
				$tab_name = $btnDataDetail->tab_name;
				$btnDataDetail->user_id = $device_user_id;
				$btnDataDetail->btnicon = $btnDataDetail->btnicon;
				$settings = $btnDataDetail->settings;
				if($tab_name != 'Transportation'){
					$btnDatad[$tab_name][] = $btnDataDetail;
				}
			}
		}
		if(sizeof($btnDatad) == 0){
			$btnDatad = (object)[];
		}
		$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS','device_kiosk_mode' => $device_kiosk_mode, 'response' => $btnDatad);
		$this->response($resp);
	}


	public function time_zone_post()
	{
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array("user_id");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}

		$where="`cp_ct_id`=`cp_ct_id`";
		$zone_data =  $this->common_model->getAllwhere('cp_country_timezone',$where);

		$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => $zone_data);
		$this->response($resp);


	}
	
	public function btn_list_by_type_post()
	{
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array("user_id", "type");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}

		$btnData  = $this->common_model->getAllwhereorderby('setting_butns', array('settings' => 1, 'user_id' => $data['user_id'], 'tab_name' => $data['type']), 'id', 'desc');
		//$btnData = $this->common_model->getPersondata('setting_butns', array('user_id' => $data['user_id']));
		// echo "<pre>";
		// print_r($btnData);   
		// die;
		$btnDatad = [];
		foreach ($btnData as $btnDataDetail) {
			$tab_name = $btnDataDetail->tab_name;
			$settings = $btnDataDetail->settings;
			if ($tab_name != "" && $settings != 0) {
				$btnDatad[$tab_name][] = $btnDataDetail;
			}
		}
		$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => $btnDatad);
		$this->response($resp);
	}
	public function btn_detail_post()
	{
		// die('check');  
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		// $required_parameter = array("user_id","button_id");
		$required_parameter = array("user_id", "type");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}

		if ($data['type'] == 2) {
			$btnData  = $this->common_model->getAllJoinwhereorderby("cp_email", "app_name", "app_list", "id", array("cp_email.user_id" => $data['user_id']), "cp_email.id,cp_email.user_id,cp_email.button_name as email_name,cp_email.email_type as type,cp_email.url as web_url,app_list.url as app_url,app_list.icon,cp_email.created", "cp_email.id", "desc");
			if(!empty($btnData)){
				foreach ($btnData as $btnDatanew) {
					$btnDatad[] = array(
						'id' => $btnDatanew->id,
						'user_id' => $btnDatanew->user_id,
						'name' => $btnDatanew->email_name,
						'type' => $btnDatanew->type,
						'app_id' => "",
						'web_url' => ($btnDatanew->web_url != null)?$btnDatanew->web_url:'',
						'app_url' => ($btnDatanew->app_url != null)?$btnDatanew->app_url:'',
						'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
						'created' => $btnDatanew->created,
						'updated' => $btnDatanew->created,
					);
				}
			} else {
				$btnData  = $this->common_model->getAllJoinwhereorderby("cp_email", "app_name", "app_list", "id", array("cp_email.user_id" => '883'), "cp_email.id,cp_email.user_id,cp_email.button_name as email_name,cp_email.email_type as type,cp_email.url as web_url,app_list.url as app_url,app_list.icon,cp_email.created", "cp_email.id", "desc");
				if(!empty($btnData)){
					foreach ($btnData as $btnDatanew) {
						$btnDatad[] = array(
							'id' => $btnDatanew->id,
							'user_id' => $btnDatanew->user_id,
							'name' => $btnDatanew->email_name,
							'type' => $btnDatanew->type,
							'app_id' => "",
							'web_url' => ($btnDatanew->web_url != null)?$btnDatanew->web_url:'',
							'app_url' => ($btnDatanew->app_url != null)?$btnDatanew->app_url:'',
							'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
							'created' => $btnDatanew->created,
							'updated' => $btnDatanew->created,
						);
					}
				}
			}
		} elseif ($data['type'] == 1) {
			$btnData  = $this->common_model->getAllwhereorderby('calls', array('user_id' => $data['user_id']), 'id', 'desc');
			if(!empty($btnData)){
				foreach ($btnData as $btnDatanew) {
					$btnDatad[] = array(
						'id' => $btnDatanew->id,
						'user_id' => $btnDatanew->user_id,
						'name' => $btnDatanew->call_name,
						'type' => $btnDatanew->type,
						'app_id' => $btnDatanew->app_id,
						'web_url' => $btnDatanew->web_url,
						'app_url' => $btnDatanew->app_url,
						'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
						'created' => $btnDatanew->created,
						'updated' => $btnDatanew->created,
					);
				}
			} else {
				$btnData  = $this->common_model->getAllwhereorderby('calls', array('user_id' =>'883'), 'id', 'desc');
					if(!empty($btnData)){
						foreach ($btnData as $btnDatanew) {
							$btnDatad[] = array(
								'id' => $btnDatanew->id,
								'user_id' => $btnDatanew->user_id,
								'name' => $btnDatanew->call_name,
								'type' => $btnDatanew->type,
								'app_id' => $btnDatanew->app_id,
								'web_url' => $btnDatanew->web_url,
								'app_url' => $btnDatanew->app_url,
								'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
								'created' => $btnDatanew->created,
								'updated' => $btnDatanew->created,
							);
						}
					}
			}
		} elseif ($data['type'] == 4) {
			$btnData  = $this->common_model->getAllJoinwhereorderby("cp_social", "app_name", "app_list", "id", array("cp_social.user_id" => $data['user_id']), "cp_social.id,cp_social.user_id,cp_social.button_name as social_name,cp_social.social_type as type,cp_social.url as web_url,app_list.url as app_url,app_list.icon,cp_social.created", "cp_social.id", "desc");
			if(!empty($btnData)){
				foreach ($btnData as $btnDatanew) {
					$btnDatad[] = array(
						'id' => $btnDatanew->id,
						'user_id' => $btnDatanew->user_id,
						'name' => $btnDatanew->social_name,
						'type' => $btnDatanew->type,
						'app_id' => "",
						'web_url' => ($btnDatanew->web_url != null)?$btnDatanew->web_url:'',
						'app_url' => ($btnDatanew->app_url != null)?$btnDatanew->app_url:'',
						'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
						'created' => $btnDatanew->created,
						'updated' => $btnDatanew->created,
					);
				}
			} else {
				$btnData  = $this->common_model->getAllJoinwhereorderby("cp_social", "app_name", "app_list", "id", array("cp_social.user_id" => '883'), "cp_social.id,cp_social.user_id,cp_social.button_name as social_name,cp_social.social_type as type,cp_social.url as web_url,app_list.url as app_url,app_list.icon,cp_social.created", "cp_social.id", "desc");
				if(!empty($btnData)){
					foreach ($btnData as $btnDatanew) {
						$btnDatad[] = array(
							'id' => $btnDatanew->id,
							'user_id' => $btnDatanew->user_id,
							'name' => $btnDatanew->social_name,
							'type' => $btnDatanew->type,
							'app_id' => "",
							'web_url' => ($btnDatanew->web_url != null)?$btnDatanew->web_url:'',
							'app_url' => ($btnDatanew->app_url != null)?$btnDatanew->app_url:'',
							'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
							'created' => $btnDatanew->created,
							'updated' => $btnDatanew->created,
						);
					}
				}
			}
		} elseif ($data['type'] == 5) {
			$btnData  = $this->common_model->getAllJoinwhereorderby("cp_transportation", "app_name", "app_list", "id", array("cp_transportation.user_id" => $data['user_id']),"cp_transportation.id,cp_transportation.user_id,cp_transportation.button_name as transportation_name,cp_transportation.transportation_type as type,cp_transportation.url as web_url,app_list.url as app_url,app_list.icon,cp_transportation.created", "cp_transportation.id", "desc");
			if(!empty($btnData)){
				foreach ($btnData as $btnDatanew) {
					$btnDatad[] = array(
						'id' => $btnDatanew->id,
						'user_id' => $btnDatanew->user_id,
						'name' => $btnDatanew->transportation_name,
						'type' => $btnDatanew->type,
						'app_id' => "",
						'web_url' => ($btnDatanew->web_url != null)?$btnDatanew->web_url:'',
						'app_url' => ($btnDatanew->app_url != null)?$btnDatanew->app_url:'',
						'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
						'created' => $btnDatanew->created,
						'updated' => $btnDatanew->created,
					);
				}
			} else {
				$btnData  = $this->common_model->getAllJoinwhereorderby("cp_transportation", "app_name", "app_list", "id", array("cp_transportation.user_id" => '883'),"cp_transportation.id,cp_transportation.user_id,cp_transportation.button_name as transportation_name,cp_transportation.transportation_type as type,cp_transportation.url as web_url,app_list.url as app_url,app_list.icon,cp_transportation.created", "cp_transportation.id", "desc");
				if(!empty($btnData)){
					foreach ($btnData as $btnDatanew) {
						$btnDatad[] = array(
							'id' => $btnDatanew->id,
							'user_id' => $btnDatanew->user_id,
							'name' => $btnDatanew->transportation_name,
							'type' => $btnDatanew->type,
							'app_id' => "",
							'web_url' => ($btnDatanew->web_url != null)?$btnDatanew->web_url:'',
							'app_url' => ($btnDatanew->app_url != null)?$btnDatanew->app_url:'',
							'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
							'created' => $btnDatanew->created,
							'updated' => $btnDatanew->created,
						);
					}
				}
			}
		} elseif ($data['type'] == 20) {
			$btnData  = $this->common_model->getAllwhereorderby('medicine_request_services', array('user_id' => $data['user_id']), 'id', 'desc');
			if(!empty($btnData)){
				foreach ($btnData as $btnDatanew) {
					$btnDatad[] = array(
						'id' => $btnDatanew->id,
						'user_id' => $btnDatanew->user_id,
						'name' => $btnDatanew->name,
						'type' => $btnDatanew->type,
						'app_id' => "",
						'web_url' => (!empty($btnDatanew->web_url)) ? $btnDatanew->web_url : "",
						'app_url' => (!empty($btnDatanew->app_url)) ? $btnDatanew->app_url : "",
						'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
						'created' => $btnDatanew->created,
						'updated' => $btnDatanew->created,
					);
				}
			} else {
				$btnData  = $this->common_model->getAllwhereorderby('medicine_request_services', array('user_id' => '883'), 'id', 'desc');
					if(!empty($btnData)){
						foreach ($btnData as $btnDatanew) {
							$btnDatad[] = array(
								'id' => $btnDatanew->id,
								'user_id' => $btnDatanew->user_id,
								'name' => $btnDatanew->name,
								'type' => $btnDatanew->type,
								'app_id' => "",
								'web_url' => (!empty($btnDatanew->web_url)) ? $btnDatanew->web_url : "",
								'app_url' => (!empty($btnDatanew->app_url)) ? $btnDatanew->app_url : "",
								'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
								'created' => $btnDatanew->created,
								'updated' => $btnDatanew->created,
							);
						}
					}
			}
		} elseif ($data['type'] == 14) {
			$btnData  = $this->common_model->getAllJoinwhereorderby("cp_game", "app_name", "app_list", "id", array("cp_game.user_id" => $data['user_id']), "cp_game.id,cp_game.user_id,cp_game.button_name as game_name,cp_game.game_type as type,cp_game.url as web_url,app_list.url as app_url,app_list.icon,cp_game.created", "cp_game.id", "desc");
			if(!empty($btnData)){
			foreach ($btnData as $btnDatanew) {
				$btnDatad[] = array(
					'id' => $btnDatanew->id,
					'user_id' => $btnDatanew->user_id,
					'name' => $btnDatanew->game_name,
					'type' => $btnDatanew->type,
					'app_id' => "",
					'web_url' => (!empty($btnDatanew->web_url)) ? $btnDatanew->web_url : "",
					'app_url' => (!empty($btnDatanew->app_url)) ? $btnDatanew->app_url : "",
					'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
					'created' => $btnDatanew->created,
					'updated' => $btnDatanew->created,
				);
			}
			} else {
				$btnData  = $this->common_model->getAllJoinwhereorderby("cp_game", "app_name", "app_list", "id", array("cp_game.user_id" => '883'), "cp_game.id,cp_game.user_id,cp_game.button_name as game_name,cp_game.game_type as type,cp_game.url as web_url,app_list.url as app_url,app_list.icon,cp_game.created", "cp_game.id", "desc");
					if(!empty($btnData)){
					foreach ($btnData as $btnDatanew) {
						$btnDatad[] = array(
							'id' => $btnDatanew->id,
							'user_id' => $btnDatanew->user_id,
							'name' => $btnDatanew->game_name,
							'type' => $btnDatanew->type,
							'app_id' => "",
							'web_url' => (!empty($btnDatanew->web_url)) ? $btnDatanew->web_url : "",
							'app_url' => (!empty($btnDatanew->app_url)) ? $btnDatanew->app_url : "",
							'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
							'created' => $btnDatanew->created,
							'updated' => $btnDatanew->created,
						);
					}
					}
			}
		} elseif ($data['type'] == 11) {
			$btnData  = $this->common_model->getAllJoinwhereorderby("cp_movie", "app_name", "app_list", "id", array("cp_movie.user_id" => $data['user_id']), "cp_movie.id,cp_movie.user_id,cp_movie.button_name as movie_name,cp_movie.movie_type as type,cp_movie.url as web_url,app_list.url as app_url,app_list.icon,cp_movie.created", "cp_movie.id", "desc");
			if(!empty($btnData)){
			foreach ($btnData as $btnDatanew) {
				$btnDatad[] = array(
					'id' => $btnDatanew->id,
					'user_id' => $btnDatanew->user_id,
					'name' => $btnDatanew->movie_name,
					'type' => $btnDatanew->type,
					'app_id' => "",
					'web_url' => (!empty($btnDatanew->web_url)) ? $btnDatanew->web_url : "",
					'app_url' => (!empty($btnDatanew->app_url)) ? $btnDatanew->app_url : "",
					'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
					'created' => $btnDatanew->created,
					'updated' => $btnDatanew->created,
				);
			}
			} else {
				$btnData  = $this->common_model->getAllJoinwhereorderby("cp_movie", "app_name", "app_list", "id", array("cp_movie.user_id" => '883'), "cp_movie.id,cp_movie.user_id,cp_movie.button_name as movie_name,cp_movie.movie_type as type,cp_movie.url as web_url,app_list.url as app_url,app_list.icon,cp_movie.created", "cp_movie.id", "desc");
					if(!empty($btnData)){
					foreach ($btnData as $btnDatanew) {
						$btnDatad[] = array(
							'id' => $btnDatanew->id,
							'user_id' => $btnDatanew->user_id,
							'name' => $btnDatanew->movie_name,
							'type' => $btnDatanew->type,
							'app_id' => "",
							'web_url' => (!empty($btnDatanew->web_url)) ? $btnDatanew->web_url : "",
							'app_url' => (!empty($btnDatanew->app_url)) ? $btnDatanew->app_url : "",
							'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
							'created' => $btnDatanew->created,
							'updated' => $btnDatanew->created,
						);
					}
					}
			}
		} elseif ($data['type'] == 12) {
			$btnData  = $this->common_model->getAllJoinwhereorderby("cp_music", "app_name", "app_list", "id", array("cp_music.user_id" => $data['user_id']), "cp_music.id,cp_music.user_id,cp_music.button_name as music_name,cp_music.music_type as type,cp_music.url as web_url,app_list.url as app_url,app_list.icon,cp_music.created", "cp_music.id", "desc");
			if(!empty($btnData)){
			foreach ($btnData as $btnDatanew) {
				$btnDatad[] = array(
					'id' => $btnDatanew->id,
					'user_id' => $btnDatanew->user_id,
					'name' => $btnDatanew->music_name,
					'type' => $btnDatanew->type,
					'app_id' => "",
					'web_url' => (!empty($btnDatanew->web_url)) ? $btnDatanew->web_url : "",
					'app_url' => (!empty($btnDatanew->app_url)) ? $btnDatanew->app_url : "",
					'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
					'created' => $btnDatanew->created,
					'updated' => $btnDatanew->created,
				);
			}
			} else {
				$btnData  = $this->common_model->getAllJoinwhereorderby("cp_music", "app_name", "app_list", "id", array("cp_music.user_id" => '883'), "cp_music.id,cp_music.user_id,cp_music.button_name as music_name,cp_music.music_type as type,cp_music.url as web_url,app_list.url as app_url,app_list.icon,cp_music.created", "cp_music.id", "desc");
					if(!empty($btnData)){
					foreach ($btnData as $btnDatanew) {
						$btnDatad[] = array(
							'id' => $btnDatanew->id,
							'user_id' => $btnDatanew->user_id,
							'name' => $btnDatanew->music_name,
							'type' => $btnDatanew->type,
							'app_id' => "",
							'web_url' => (!empty($btnDatanew->web_url)) ? $btnDatanew->web_url : "",
							'app_url' => (!empty($btnDatanew->app_url)) ? $btnDatanew->app_url : "",
							'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
							'created' => $btnDatanew->created,
							'updated' => $btnDatanew->created,
						);
					}
					}
			}
		} elseif ($data['type'] == 9) {
			$btnData  = $this->common_model->getAllJoinwhereorderby("cp_weather", "app_name", "app_list", "id", array("cp_weather.user_id" => $data['user_id']), "cp_weather.id,cp_weather.user_id,cp_weather.button_name as weather_name,cp_weather.weather_type as type,cp_weather.url as web_url,app_list.url as app_url,app_list.icon,cp_weather.created", "cp_weather.id", "desc");
			if(!empty($btnData)){
			foreach ($btnData as $btnDatanew) {
				$btnDatad[] = array(
					'id' => $btnDatanew->id,
					'user_id' => $btnDatanew->user_id,
					'name' => $btnDatanew->weather_name,
					'type' => $btnDatanew->type,
					'app_id' => "",
					'web_url' => (!empty($btnDatanew->web_url)) ? $btnDatanew->web_url : "",
					'app_url' => (!empty($btnDatanew->app_url)) ? $btnDatanew->app_url : "",
					'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
					'created' => $btnDatanew->created,
					'updated' => $btnDatanew->created,
				);
			}
			} else {
				$btnData  = $this->common_model->getAllJoinwhereorderby("cp_weather", "app_name", "app_list", "id", array("cp_weather.user_id" => '883'), "cp_weather.id,cp_weather.user_id,cp_weather.button_name as weather_name,cp_weather.weather_type as type,cp_weather.url as web_url,app_list.url as app_url,app_list.icon,cp_weather.created", "cp_weather.id", "desc");
				if(!empty($btnData)){
				foreach ($btnData as $btnDatanew) {
					$btnDatad[] = array(
						'id' => $btnDatanew->id,
						'user_id' => $btnDatanew->user_id,
						'name' => $btnDatanew->weather_name,
						'type' => $btnDatanew->type,
						'app_id' => "",
						'web_url' => (!empty($btnDatanew->web_url)) ? $btnDatanew->web_url : "",
						'app_url' => (!empty($btnDatanew->app_url)) ? $btnDatanew->app_url : "",
						'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
						'created' => $btnDatanew->created,
						'updated' => $btnDatanew->created,
					);
					}
				}
			}
		} elseif ($data['type'] == 23) {
			$btnData  = $this->common_model->getAllJoinwhereorderby("cp_pharmacy", "app_name", "app_list", "id", array("cp_pharmacy.user_id" => $data['user_id']), "cp_pharmacy.id,cp_pharmacy.user_id,cp_pharmacy.button_name,cp_pharmacy.pharmacy_type as type,cp_pharmacy.url as web_url,app_list.url as app_url,app_list.icon,cp_pharmacy.created", "cp_pharmacy.id", "desc");
			if(!empty($btnData)){
				foreach ($btnData as $btnDatanew) {
					$btnDatad[] = array(
						'id' => $btnDatanew->id,
						'user_id' => $btnDatanew->user_id,
						'name' => $btnDatanew->button_name,
						'type' => $btnDatanew->type,
						'app_id' => "",
						'web_url' => ($btnDatanew->web_url != null)?$btnDatanew->web_url:'',
						'app_url' => ($btnDatanew->app_url != null)?$btnDatanew->app_url:'',
						'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
						'created' => $btnDatanew->created,
						'updated' => $btnDatanew->created,
					);
				}
			} else {
				$btnData  = $this->common_model->getAllJoinwhereorderby("cp_pharmacy", "app_name", "app_list", "id", array("cp_pharmacy.user_id" => "883"), "cp_pharmacy.id,cp_pharmacy.user_id,cp_pharmacy.button_name,cp_pharmacy.pharmacy_type as type,cp_pharmacy.url as web_url,app_list.url as app_url,app_list.icon,cp_pharmacy.created", "cp_pharmacy.id", "desc");
				if(!empty($btnData)){
					foreach ($btnData as $btnDatanew) {
						$btnDatad[] = array(
							'id' => $btnDatanew->id,
							'user_id' => $btnDatanew->user_id,
							'name' => $btnDatanew->button_name,
							'type' => $btnDatanew->type,
							'app_id' => "",
							'web_url' => ($btnDatanew->web_url != null)?$btnDatanew->web_url:'',
							'app_url' => ($btnDatanew->app_url != null)?$btnDatanew->app_url:'',
							'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
							'created' => $btnDatanew->created,
							'updated' => $btnDatanew->created,
						);
					}
				}
			}
		} elseif ($data['type'] == 21) {
			$btnData  = $this->common_model->getAllJoinwhereorderby("cp_internet", "app_name", "app_list", "id", array("cp_internet.user_id" => $data['user_id']), "cp_internet.id,cp_internet.user_id,cp_internet.button_name,cp_internet.internet_type as type,cp_internet.url as web_url,app_list.url as app_url,app_list.icon,cp_internet.created", "cp_internet.id", "desc");
			if(!empty($btnData)){
				foreach ($btnData as $btnDatanew) {
					$btnDatad[] = array(
						'id' => $btnDatanew->id,
						'user_id' => $btnDatanew->user_id,
						'name' => $btnDatanew->button_name,
						'type' => $btnDatanew->type,
						'app_id' => "",
						'web_url' => ($btnDatanew->web_url != null)?$btnDatanew->web_url:'',
						'app_url' => ($btnDatanew->app_url != null)?$btnDatanew->app_url:'',
						'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
						'created' => $btnDatanew->created,
						'updated' => $btnDatanew->created,
					);
				}
			} else {
				$btnData  = $this->common_model->getAllJoinwhereorderby("cp_internet", "app_name", "app_list", "id", array("cp_internet.user_id" => "883"), "cp_internet.id,cp_internet.user_id,cp_internet.button_name,cp_internet.internet_type as type,cp_internet.url as web_url,app_list.url as app_url,app_list.icon,cp_internet.created", "cp_internet.id", "desc");
				if(!empty($btnData)){
					foreach ($btnData as $btnDatanew) {
						$btnDatad[] = array(
							'id' => $btnDatanew->id,
							'user_id' => $btnDatanew->user_id,
							'name' => $btnDatanew->button_name,
							'type' => $btnDatanew->type,
							'app_id' => "",
							'web_url' => ($btnDatanew->web_url != null)?$btnDatanew->web_url:'',
							'app_url' => ($btnDatanew->app_url != null)?$btnDatanew->app_url:'',
							'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
							'created' => $btnDatanew->created,
							'updated' => $btnDatanew->created,
						);
					}
				}
			}
		} elseif ($data['type'] == 19) {
			$btnData  = $this->common_model->getAllJoinwhereorderby("cp_news", "app_name", "app_list", "id", array("cp_news.user_id" => $data['user_id']), "cp_news.id,cp_news.user_id,cp_news.button_name,cp_news.news_type as type,cp_news.url as web_url,app_list.url as app_url,app_list.icon,cp_news.created", "cp_news.id", "desc");
			if(!empty($btnData)){
				foreach ($btnData as $btnDatanew) {
					$btnDatad[] = array(
						'id' => $btnDatanew->id,
						'user_id' => $btnDatanew->user_id,
						'name' => $btnDatanew->button_name,
						'type' => $btnDatanew->type,
						'app_id' => "",
						'web_url' => ($btnDatanew->web_url != null)?$btnDatanew->web_url:'',
						'app_url' => ($btnDatanew->app_url != null)?$btnDatanew->app_url:'',
						'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
						'created' => $btnDatanew->created,
						'updated' => $btnDatanew->created,
					);
				}
			} else {
				$btnData  = $this->common_model->getAllJoinwhereorderby("cp_news", "app_name", "app_list", "id", array("cp_news.user_id" => "883"), "cp_news.id,cp_news.user_id,cp_news.button_name,cp_news.news_type as type,cp_news.url as web_url,app_list.url as app_url,app_list.icon,cp_news.created", "cp_news.id", "desc");
				if(!empty($btnData)){
					foreach ($btnData as $btnDatanew) {
						$btnDatad[] = array(
							'id' => $btnDatanew->id,
							'user_id' => $btnDatanew->user_id,
							'name' => $btnDatanew->button_name,
							'type' => $btnDatanew->type,
							'app_id' => "",
							'web_url' => ($btnDatanew->web_url != null)?$btnDatanew->web_url:'',
							'app_url' => ($btnDatanew->app_url != null)?$btnDatanew->app_url:'',
							'icon' => (!empty($btnDatanew->icon)) ? $btnDatanew->icon : "https://www.google.com/s2/favicons?sz=128&domain_url=".$btnDatanew->web_url,
							'created' => $btnDatanew->created,
							'updated' => $btnDatanew->created,
						);
					}
				}
			}
		} else {
			$btnDatad = array();
		}

		//$btnData = $this->common_model->getsingle('setting_butns', array('user_id' => $data['user_id']));



		if (!empty($btnDatad)) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('Btn_setting_data' => $btnDatad));
		} else {
			// $resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Setting Data not found', 'response' => array('message' => 'Setting Data not found'));
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('Btn_setting_data' => []));
		}
		
		$this->response($resp);
	}


	/*-------Acknowledge list--------*/
	public function acknowledge_list_post()
	{
		//die('check');  
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array("user_id");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$con['sorting'] = array("acknowledge_id" => "DESC");
		$acknowledgeData  = $this->Api_model->acknowledgeList($data['user_id']);
		if (!empty($acknowledgeData)) {
			foreach ($acknowledgeData as $acknowledgeData_datails) {
				$acknowledgeData1[] = array(
					'id' => $acknowledgeData_datails['acknowledge_id'],
					'description' => $acknowledgeData_datails['descp'],
					'title' => $acknowledgeData_datails['title'],
					'medicine_name' => $acknowledgeData_datails['medicine'],
					'medicine_name2' => $acknowledgeData_datails['medicine2'],
					'medicine_name3' => $acknowledgeData_datails['medicine3'],
					'pharma_company' => $acknowledgeData_datails['pharma_name'],
					'user_id' => $data['user_id'],
					'name' => $acknowledgeData_datails['users_name'],
					'lastname' => $acknowledgeData_datails['lastname'],
					'dob' => $acknowledgeData_datails['dob'],
					'selected_user' => $acknowledgeData_datails['selected_user'],
					'send_date' => $acknowledgeData_datails['send_date'],
					'send_time' => $acknowledgeData_datails['send_time'],
					'date' => $acknowledgeData_datails['acknowledge_created'],
					'rx_number' => $acknowledgeData_datails['rx_number'],
					'supply_days' => $acknowledgeData_datails['supply_days'],
					'supply_days2' => $acknowledgeData_datails['supply2_days'],
					'supply_days3' => $acknowledgeData_datails['supply3_days'],
					//'user_id'=>$acknowledgeData_datails['user_id'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('acknowledge_data' => $acknowledgeData1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Acknowledge not found', 'response' => array('message' => 'Acknowledge Data not found'));
		}
		$this->response($resp);
	}
	/*-------Send Acknowledge--------*/
	public function acknowledge_send_post()
	{
		//die('testing');
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array("pharma_company_id", "medicine_id", "user_id", "select_user_id", "date", "time");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		if (!empty($data['rx_number'])) {
			$test_rx = $data['rx_number'];
		} else {
			$test_rx = '';
		}
		if (!empty($data['medicine2_id'])) {
			$medicine2_id = $data['medicine2_id'];
		} else {
			$medicine2_id = '';
		}
		if (!empty($data['medicine3_id'])) {
			$medicine3_id = $data['medicine3_id'];
		} else {
			$medicine3_id = '';
		}
		if (!empty($data['supply_days'])) {
			$test_supply = $data['supply_days'];
		} else {
			$test_supply = '';
		}
		if (!empty($data['supply2_days'])) {
			$supply2_days = $data['supply2_days'];
		} else {
			$supply2_days = '';
		}
		if (!empty($data['supply3_days'])) {
			$supply3_days = $data['supply3_days'];
		} else {
			$supply3_days = '';
		}
		$post_data = array(
			'acknowledge_desc' => $data['desc'],
			//'acknowledge_title'=> $data['title'],
			'pharma_company_id ' => $data['pharma_company_id'],
			'medicine_id' => $data['medicine_id'],
			'medicine2_id' => $medicine2_id,
			'medicine3_id' => $medicine3_id,
			'user_id' => $data['user_id'],
			'name' => $data['name'],
			'lastname' => $data['lastname'],
			'dob' => $data['dob'],
			'send_date' => $data['date'],
			'send_time' => $data['time'],
			'selected_user' => $data['select_user_id'],
			'rx_number' => $test_rx,
			'supply_days' => $test_supply,
			'supply2_days' => $supply2_days,
			'supply3_days' => $supply3_days,
			'acknowledge_created' => date('Y-m-d H:i:s'),
		);
		$acknowledgeId = $this->common_model->addRecords('acknowledge', $post_data);
		$acknowledgeData = $this->common_model->getSingleRecordById('acknowledge', array('acknowledge_id' => $acknowledgeId));
		// $medicine_id = $acknowledgeData['medicine_id'];
		// $medicineData = $this->common_model->getSingleRecordById('medicine_schedule', array('medicine_id' => $medicine_id));
		// $medicine_name = $medicineData['medicine_name'];
		// echo "<pre>";
		// print_r($acknowledgeData['medicine_id']);
		// die;
		$email_pharma_company = $this->common_model->getsingle("pharma_company", array("id" => $data['pharma_company_id']));
		$medicine_name_mail = $this->common_model->getsingle("medicine_schedule", array("medicine_id" => $data['medicine_id']));
		if (!empty($medicine2_id)) {
			$medicine_name_mail2 = $this->common_model->getsingle("medicine_schedule", array("medicine_id" => $medicine2_id));
		} else {
			$medicine_name_mail2 = '';
		}
		if (!empty($medicine3_id)) {
			$medicine_name_mail3 = $this->common_model->getsingle("medicine_schedule", array("medicine_id" => $medicine3_id));
		} else {
			$medicine_name_mail3 = '';
		}
		$username_mail = $this->common_model->getsingle("cp_users", array("id" => $data['user_id']));
		$selectUser_mail = $this->common_model->getsingle("cp_users", array("id" => $data['select_user_id']));
		$from_m = "info@carepro.com";
		$fromname = "Carepro";
		if (empty($medicine_name_mail2) && empty($medicine_name_mail3)) {
			$message = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
		<html xmlns='http://www.w3.org/1999/xhtml'>
		<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
		<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>
		</head>
		<body style=margin: 0;font-family:'open sans';'>
		<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;position:relative;'>
		<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>
		<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin:0 auto 0px;'>
		<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
		</a>
		</div>
		<div style='border: none;padding:0px 60px; margin:-40px auto 0px; border-radius:0px; position: absolute; max-width: 580px; top: 110px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>
		<div class='temp_cont' style='width:100%; margin:-40px auto 0px !important;'>
		<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>
		<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
		<span style='display:block;width:100%;'>
		<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>
		</span>
		Medicine Pick up request
		</h2>
		<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
		Hi &nbsp;" . $username_mail->username . ",
		</h3>
		<p style='text-align: center; font-size: 14px; margin-top: 0px;'>Patient Name has requested you to pick up medicines from pharmacy, please see medicine and pharmacy information below</p>
		<style>
								#customers td, #customers th {
		border:1px solid #ddd;
		padding:8px;
	}
								#customers th {
	padding-top:8px;
	padding-bottom:8px;
	text-align:left;
	background-color:#4CAF50;
	color:white;
}
</style>
<table id='customers' style='width:100%; border-collapse: collapse;'>
<tr>
<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>#1 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $medicine_name_mail->medicine_name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; text-align:left; background-color:#4CAF50; color:#fff;'>Description:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $data['desc'] . "</td>
</tr> 
<tr>
<th style='width:200px; font-size:15px; padding:8px; text-align:left; background-color:#4CAF50;color:#fff;border: 1px solid #ddd;'>Date and Time for pickup:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $acknowledgeData['send_date'] . "," . $acknowledgeData['send_time'] . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; padding:8px; border: 1px solid #ddd; text-align:left; background-color:#4CAF50;color:#fff;'>Pharmacy Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Address:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->address . "<isindex></isindex></td>
</tr>
<tr>
<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Number:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->contact_no . "</td>
</tr> 
</table>
</div>
<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%;     box-sizing: border-box;'>
<p style='font-size: 13px;margin: 0 0 8px;'>Please call 'sender user name' if you are not able to pick up these medicines</p>						
<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>
<div style='width:50%; float:left;'>
<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>
</p>
</div>
<div style='width:50%; float:right;'>
<div style='border:none;'>
<a href='#' target='_blank' style='display:inline-block;'>
<img style='max-width:120px;width:100%;margin:0 auto 10px;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
</a>
</div>
<p style='font-size: 13px;margin:0 0 8px;'>Carepro customer service</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>9876543210</a>
</p>
</div>
</div>
</div>
</div>
</div>
</body>
</html>";
		} else if (!empty($medicine_name_mail2) && empty($medicine_name_mail3)) {
			$message = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
	<html xmlns='http://www.w3.org/1999/xhtml'>
	<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
	<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>
	</head>
	<body style='margin: 0;font-family:'open sans';'>
	<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;position:relative;'>
	<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>
	<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin:0 auto 0px;'>
	<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
	</a>
	</div>
	<div style='border: none;padding:0px 60px; margin:-40px auto 0px; border-radius:0px; position: absolute; max-width: 580px; top: 110px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>
	<div class='temp_cont' style='width:100%; margin:-40px auto 0px !important;'>
	<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>
	<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
	<span style='display:block;width:100%;'>
	<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>
	</span>
	Medicine Pick up request
	</h2>
	<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
	Hi &nbsp;" . $username_mail->username . ",
	</h3>
	<p style='text-align: center; font-size: 14px; margin-top: 0px;'>Patient Name has requested you to pick up medicines from pharmacy, please see medicine and pharmacy information below</p>
	<style>
								#customers td, #customers th {
	border:1px solid #ddd;
	padding:8px;
}
								#customers th {
padding-top:8px;
padding-bottom:8px;
text-align:left;
background-color:#4CAF50;
color:white;
}
</style>
<table id='customers' style='width:100%; border-collapse: collapse;'>
<tr>
<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#1 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $medicine_name_mail->medicine_name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#2 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $medicine_name_mail2->medicine_name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; text-align:left; background-color:#4CAF50; color:#fff;'>Description:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $data['desc'] . "</td>
</tr> 
<tr>
<th style='width:200px; font-size:15px; padding:8px; text-align:left; background-color:#4CAF50;color:#fff;border: 1px solid #ddd;'>Date and Time for pickup:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $acknowledgeData['send_date'] . "," . $acknowledgeData['send_time'] . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; padding:8px; border: 1px solid #ddd; text-align:left; background-color:#4CAF50;color:#fff;'>Pharmacy Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Address:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->address . "<isindex></isindex></td>
</tr>
<tr>
<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Number:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->contact_no . "</td>
</tr> 
</table>
</div>
<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%;     box-sizing: border-box;'>
<p style='font-size: 13px;margin: 0 0 8px;'>Please call 'sender user name' if you are not able to pick up these medicines</p>						
<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>
<div style='width:50%; float:left;'>
<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>
</p>
</div>
<div style='width:50%; float:right;'>
<div style='border:none;'>
<a href='#' target='_blank' style='display:inline-block;'>
<img style='max-width:120px;width:100%;margin:0 auto 10px;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
</a>
</div>
<p style='font-size: 13px;margin:0 0 8px;'>Carepro customer service</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>9876543210</a>
</p>
</div>
</div>
</div>
</div>
</div>
</body>
</html>";
		} else if (!empty($medicine_name_mail3) && empty($medicine_name_mail2)) {
			$message = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
	<html xmlns='http://www.w3.org/1999/xhtml'>
	<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
	<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>
	</head>
	<body style='margin: 0;font-family:'open sans';'>
	<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;position:relative;'>
	<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>
	<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin:0 auto 0px;'>
	<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
	</a>
	</div>
	<div style='border: none;padding:0px 60px; margin:-40px auto 0px; border-radius:0px; position: absolute; max-width: 580px; top: 110px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>
	<div class='temp_cont' style='width:100%; margin:-40px auto 0px !important;'>
	<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>
	<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
	<span style='display:block;width:100%;'>
	<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>
	</span>
	Medicine Pick up request
	</h2>
	<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
	Hi &nbsp;" . $username_mail->username . ",
	</h3>
	<p style='text-align: center; font-size: 14px; margin-top: 0px;'>Patient Name has requested you to pick up medicines from pharmacy, please see medicine and pharmacy information below</p>
	<style>
								#customers td, #customers th {
	border:1px solid #ddd;
	padding:8px;
}
								#customers th {
padding-top:8px;
padding-bottom:8px;
text-align:left;
background-color:#4CAF50;
color:white;
}
</style>
<table id='customers' style='width:100%; border-collapse: collapse;'>
<tr>
<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#1 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $medicine_name_mail->medicine_name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#2 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $medicine_name_mail3->medicine_name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; text-align:left; background-color:#4CAF50; color:#fff;'>Description:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $data['desc'] . "</td>
</tr> 
<tr>
<th style='width:200px; font-size:15px; padding:8px; text-align:left; background-color:#4CAF50;color:#fff;border: 1px solid #ddd;'>Date and Time for pickup:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $acknowledgeData['send_date'] . "," . $acknowledgeData['send_time'] . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; padding:8px; border: 1px solid #ddd; text-align:left; background-color:#4CAF50;color:#fff;'>Pharmacy Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Address:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->address . "<isindex></isindex></td>
</tr>
<tr>
<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Number:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->contact_no . "</td>
</tr> 
</table>
</div>
<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%;     box-sizing: border-box;'>
<p style='font-size: 13px;margin: 0 0 8px;'>Please call 'sender user name' if you are not able to pick up these medicines</p>						
<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>
<div style='width:50%; float:left;'>
<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>
</p>
</div>
<div style='width:50%; float:right;'>
<div style='border:none;'>
<a href='#' target='_blank' style='display:inline-block;'>
<img style='max-width:120px;width:100%;margin:0 auto 10px;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
</a>
</div>
<p style='font-size: 13px;margin:0 0 8px;'>Carepro customer service</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>9876543210</a>
</p>
</div>
</div>
</div>
</div>
</div>
</body>
</html>";
		} else if (!empty($medicine_name_mail3) && !empty($medicine_name_mail2)) {
			$message = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
	<html xmlns='http://www.w3.org/1999/xhtml'>
	<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
	<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>
	</head>
	<body style='margin: 0;font-family:'open sans';'>
	<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;position:relative;'>
	<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>
	<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin:0 auto 0px;'>
	<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
	</a>
	</div>
	<div style='border: none;padding:0px 60px; margin:-40px auto 0px; border-radius:0px; position: absolute; max-width: 580px; top: 110px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>
	<div class='temp_cont' style='width:100%; margin:-40px auto 0px !important;'>
	<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>
	<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
	<span style='display:block;width:100%;'>
	<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>
	</span>
	Medicine Pick up request
	</h2>
	<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
	Hi &nbsp;" . $username_mail->username . ",
	</h3>
	<p style='text-align: center; font-size: 14px; margin-top: 0px;'>Patient Name has requested you to pick up medicines from pharmacy, please see medicine and pharmacy information below</p>
	<style>
								#customers td, #customers th {
	border:1px solid #ddd;
	padding:8px;
}
								#customers th {
padding-top:8px;
padding-bottom:8px;
text-align:left;
background-color:#4CAF50;
color:white;
}
</style>
<table id='customers' style='width:100%; border-collapse: collapse;'>
<tr>
<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#1 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $medicine_name_mail->medicine_name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#2 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $medicine_name_mail2->medicine_name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#3 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $medicine_name_mail3->medicine_name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; text-align:left; background-color:#4CAF50; color:#fff;'>Description:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $data['desc'] . "</td>
</tr> 
<tr>
<th style='width:200px; font-size:15px; padding:8px; text-align:left; background-color:#4CAF50;color:#fff;border: 1px solid #ddd;'>Date and Time for pickup:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $acknowledgeData['send_date'] . "," . $acknowledgeData['send_time'] . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; padding:8px; border: 1px solid #ddd; text-align:left; background-color:#4CAF50;color:#fff;'>Pharmacy Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Address:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->address . "<isindex></isindex></td>
</tr>
<tr>
<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Number:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->contact_no . "</td>
</tr> 
</table>
</div>
<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%;     box-sizing: border-box;'>
<p style='font-size: 13px;margin: 0 0 8px;'>Please call 'sender user name' if you are not able to pick up these medicines</p>						
<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>
<div style='width:50%; float:left;'>
<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>
</p>
</div>
<div style='width:50%; float:right;'>
<div style='border:none;'>
<a href='#' target='_blank' style='display:inline-block;'>
<img style='max-width:120px;width:100%;margin:0 auto 10px;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
</a>
</div>
<p style='font-size: 13px;margin:0 0 8px;'>Carepro customer service</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>9876543210</a>
</p>
</div>
</div>
</div>
</div>
</div>
</body>
</html>";
		}
		socialEmail($email_pharma_company->email, $from_m, $fromname, "Acknowledge", $message);
		socialEmail($selectUser_mail->email, $from_m, $fromname, "Acknowledge", $message);
		if ($acknowledgeId) {
			$acknowledgeData = $this->common_model->getSingleRecordById('acknowledge', array('acknowledge_id' => $acknowledgeId));
			$acknowledgeData1 = array(
				'id' => $acknowledgeData['acknowledge_id'],
				'desc' => $acknowledgeData['acknowledge_desc'],
				//'title' => $acknowledgeData['acknowledge_title'],
				'medicine_id' => $acknowledgeData['medicine_id'],
				'select_user_id' => $data['select_user_id'],
				'date' => $data['date'],
				'time' => $data['time'],
				'rx_number' => $data['rx_number'],
				'supply_days' => $data['supply_days'],
				'pharma_company_id' => $acknowledgeData['pharma_company_id'],
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Acknowledge added successfully', 'response' => $acknowledgeData1);
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/*-------Send Acknowledge--------*/
	public function send_test_mail_post()
	{
		//die('testing');
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array("pharma_company_id", "medicine_id", "user_id", "select_user_id", "date", "time");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		if (!empty($data['rx_number'])) {
			$test_rx = $data['rx_number'];
		} else {
			$test_rx = '';
		}
		if (!empty($data['supply_days'])) {
			$test_supply = $data['supply_days'];
		} else {
			$test_supply = '';
		}
		$post_data = array(
			'acknowledge_desc' => $data['desc'],
			//'acknowledge_title'=> $data['title'],
			'pharma_company_id ' => $data['pharma_company_id'],
			'medicine_id' => $data['medicine_id'],
			'user_id' => $data['user_id'],
			'name' => $data['name'],
			'lastname' => $data['lastname'],
			'dob' => $data['dob'],
			'send_date' => $data['date'],
			'send_time' => $data['time'],
			'selected_user' => $data['select_user_id'],
			'rx_number' => $test_rx,
			'supply_days' => $test_supply,
			'acknowledge_created' => date('Y-m-d H:i:s'),
		);
		$acknowledgeId = $this->common_model->addRecords('acknowledge', $post_data);
		$acknowledgeData = $this->common_model->getSingleRecordById('acknowledge', array('acknowledge_id' => $acknowledgeId));
		// $medicine_id = $acknowledgeData['medicine_id'];
		// $medicineData = $this->common_model->getSingleRecordById('medicine_schedule', array('medicine_id' => $medicine_id));
		// $medicine_name = $medicineData['medicine_name'];
		// echo "<pre>";
		// print_r($acknowledgeData['medicine_id']);
		// die;
		$email_pharma_company = $this->common_model->getsingle("pharma_company", array("id" => $data['pharma_company_id']));
		$medicine_name_mail = $this->common_model->getsingle("medicine_schedule", array("medicine_id" => $data['medicine_id']));
		$username_mail = $this->common_model->getsingle("cp_users", array("id" => $data['user_id']));
		$selectUser_mail = $this->common_model->getsingle("cp_users", array("id" => $data['select_user_id']));
		$from_m = "info@carepro.com";
		$fromname = "Carepro";
		$message = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
	<html xmlns='http://www.w3.org/1999/xhtml'>
	<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
	<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>
	</head>
	<body style='margin: 0;font-family:'open sans';'>
	<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;'>
	<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>
	<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin: 0 auto 0px;'>
	<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
	</a>
	</div>
	<div style='border:none; padding:0px 60px; max-width:580px; margin:-40px auto 0px; border-radius:0px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>
	<div class='temp_cont' style='width:100%; margin:0 auto;'>
	<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>
	<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
	<span style='display:block;width:100%;'>
	<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>
	</span>
	Subject here
	</h2>
	<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
	Hi &nbsp;" . $username_mail->username . ",
	</h3>
	<style>
							#customers td, #customers th {
	border:1px solid #ddd;
	padding:8px;
}
							#customers th {
padding-top:8px;
padding-bottom:8px;
text-align:left;
background-color:#4CAF50;
color:white;
}
</style>
<table id='customers' style='width:100%; border-collapse: collapse;'>
<tr>
<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>Jhon</td>
</tr>						  
<tr>
<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Date and Time:</th>
<td style='padding:8px;border:1px solid #ddd;'>24-11-2019, 03:00 AM</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Description:</th>
<td style='font-size:13px; padding:8px;border:1px solid #ddd;'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</td>
</tr>
</table>
</div>
<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%; box-sizing:border-box;'>
<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>
</p>						
<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>
<p style='font-size: 13px;margin:0 0 8px;'>Care Pro</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>EMAIL: info@carepro.com/</a>
</p>
</div>
</div>
</div>
</div>
</body>
</html>";
		socialEmail($email_pharma_company->email, $from_m, $fromname, "Acknowledge", $message);
		socialEmail($selectUser_mail->email, $from_m, $fromname, "Acknowledge", $message);
		if ($acknowledgeId) {
			$acknowledgeData = $this->common_model->getSingleRecordById('acknowledge', array('acknowledge_id' => $acknowledgeId));
			$acknowledgeData1 = array(
				'id' => $acknowledgeData['acknowledge_id'],
				'desc' => $acknowledgeData['acknowledge_desc'],
				//'title' => $acknowledgeData['acknowledge_title'],
				'medicine_id' => $acknowledgeData['medicine_id'],
				'select_user_id' => $data['select_user_id'],
				'date' => $data['date'],
				'time' => $data['time'],
				'rx_number' => $data['rx_number'],
				'supply_days' => $data['supply_days'],
				'pharma_company_id' => $acknowledgeData['pharma_company_id'],
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Acknowledge added successfully', 'response' => $acknowledgeData1);
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function update_acknowledge_post()
	{
		//die('test');
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array("pharma_company_id", "medicine_id", "acknowledge_id", "select_user_id", "date", "time");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		if (!empty($data['rx_number'])) {
			$test_rx = $data['rx_number'];
		} else {
			$test_rx = '';
		}
		if (!empty($data['medicine2_id'])) {
			$medicine2_id = $data['medicine2_id'];
		} else {
			$medicine2_id = '';
		}
		if (!empty($data['medicine3_id'])) {
			$medicine3_id = $data['medicine3_id'];
		} else {
			$medicine3_id = '';
		}
		if (!empty($data['supply_days'])) {
			$test_supply = $data['supply_days'];
		} else {
			$test_supply = '';
		}
		if (!empty($data['supply2_days'])) {
			$supply2_days = $data['supply2_days'];
		} else {
			$supply2_days = '';
		}
		if (!empty($data['supply3_days'])) {
			$supply3_days = $data['supply3_days'];
		} else {
			$supply3_days = '';
		}
		$post_data = array(
			'acknowledge_desc' => $data['desc'],
			//'acknowledge_title'=> $data['title'],
			'name' => $data['name'],
			'lastname' => $data['lastname'],
			'dob' => $data['dob'],
			'send_date' => $data['date'],
			'send_time' => $data['time'],
			'selected_user' => $data['select_user_id'],
			'rx_number' => $test_rx,
			'supply_days' => $test_supply,
			'supply2_days' => $supply2_days,
			'supply3_days' => $supply3_days,
			'pharma_company_id' => $data['pharma_company_id'],
			'medicine_id' => $data['medicine_id'],
			'medicine2_id' => $medicine2_id,
			'medicine3_id' => $medicine3_id,
		);
		$acknowledgeId = $this->common_model->updateRecords('acknowledge', $post_data, array('acknowledge_id' => $data['acknowledge_id']));
		$email_pharma_company = $this->common_model->getsingle("pharma_company", array("id" => $data['pharma_company_id']));
		if ($acknowledgeId) {
			$acknowledgeData = $this->common_model->getSingleRecordById('acknowledge', array('acknowledge_id' => $data['acknowledge_id']));
			$email_pharma_company = $this->common_model->getsingle("pharma_company", array("id" => $data['pharma_company_id']));
			$medicine_name_mail = $this->common_model->getsingle("medicine_schedule", array("medicine_id" => $data['medicine_id']));
			if (!empty($medicine2_id)) {
				$medicine_name_mail2 = $this->common_model->getsingle("medicine_schedule", array("medicine_id" => $medicine2_id));
			}
			if (!empty($medicine3_id)) {
				$medicine_name_mail3 = $this->common_model->getsingle("medicine_schedule", array("medicine_id" => $medicine3_id));
			}
			$username_mail = $this->common_model->getsingle("cp_users", array("id" => $acknowledgeData['user_id']));
			$selectUser_mail = $this->common_model->getsingle("cp_users", array("id" => $data['select_user_id']));
			$from_m = "info@carepro.com";
			$fromname = "Carepro";
			if (empty($medicine_name_mail2) && empty($medicine_name_mail3)) {
				$message = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
			<html xmlns='http://www.w3.org/1999/xhtml'>
			<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
			<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>
			</head>
			<body style='margin: 0;font-family:'open sans';'>
			<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;position:relative;'>
			<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>
			<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin:0 auto 0px;'>
			<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
			</a>
			</div>
			<div style='border: none;padding:0px 60px; margin:-40px auto 0px; border-radius:0px; position: absolute; max-width: 580px; top: 110px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>
			<div class='temp_cont' style='width:100%; margin:-40px auto 0px !important;'>
			<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>
			<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
			<span style='display:block;width:100%;'>
			<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>
			</span>
			Medicine Pick up request
			</h2>
			<p style='text-align: center; font-size: 14px; margin-top: 0px;'>Patient Name has requested you to pick up medicines from pharmacy, please see medicine and pharmacy information below</p>
			<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
			Hi &nbsp;" . $username_mail->username . ",
			</h3>
			<style>
								#customers td, #customers th {
			border:1px solid #ddd;
			padding:8px;
		}
								#customers th {
		padding-top:8px;
		padding-bottom:8px;
		text-align:left;
		background-color:#4CAF50;
		color:white;
	}
	</style>
	<table id='customers' style='width:100%; border-collapse: collapse;'>
	<tr>
	<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#1 Medicine Name:</th>
	<td style='padding:8px;border:1px solid #ddd;'>" . $medicine_name_mail->medicine_name . "</td>
	</tr>
	<!-- <tr>
	<th style='width:200px; border: 1px solid #ddd; font-size:15px; padding:8px; background-color:#4CAF50; text-align:left; color:#fff;'>#2 Medicine Name:</th>
	<td style='padding:8px; border:1px solid #ddd;'>Ibuprofen</td>
	</tr>
	<tr>
	<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; background-color:#4CAF50; text-align:left;color:#fff;'>#3 Medicine Name:</th>
	<td style='padding:8px;border:1px solid #ddd;'>Tylenol</td>
	</tr>
	<tr>
	<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; background-color:#4CAF50; text-align:left;color:#fff;'>#4 Medicine Name:</th>
	<td style='padding:8px;border:1px solid #ddd;'>Ernst Handel</td>
	</tr>-->
	<tr>
	<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; text-align:left; background-color:#4CAF50; color:#fff;'>Description:</th>
	<td style='padding:8px;border:1px solid #ddd;'>" . $data['desc'] . "</td>
	</tr> 
	<tr>
	<th style='width:200px; font-size:15px; padding:8px; text-align:left; background-color:#4CAF50;color:#fff;border: 1px solid #ddd;'>Date and Time for pickup:</th>
	<td style='padding:8px;border:1px solid #ddd;'>" . $acknowledgeData['send_date'] . "," . $acknowledgeData['send_time'] . "</td>
	</tr>
	<tr>
	<th style='width:200px; font-size:15px; padding:8px; border: 1px solid #ddd; text-align:left; background-color:#4CAF50;color:#fff;'>Pharmacy Name:</th>
	<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->name . "</td>
	</tr>
	<tr>
	<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Address:</th>
	<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->address . "<isindex></isindex></td>
	</tr>
	<tr>
	<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Number:</th>
	<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->contact_no . "</td>
	</tr> 
	</table>
	</div>
	<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%;     box-sizing: border-box;'>
	<p style='font-size: 13px;margin: 0 0 8px;'>Please call 'sender user name' if you are not able to pick up these medicines</p>						
	<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>
	<div style='width:50%; float:left;'>
	<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>
	<p style='font-size: 13px;margin:0;'>
	<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>
	</p>
	</div>
	<div style='width:50%; float:right;'>
	<div style='border:none;'>
	<a href='#' target='_blank' style='display:inline-block;'>
	<img style='max-width:120px;width:100%;margin:0 auto 10px;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
	</a>
	</div>
	<p style='font-size: 13px;margin:0 0 8px;'>Carepro customer service</p>
	<p style='font-size: 13px;margin:0;'>
	<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>9876543210</a>
	</p>
	</div>
	</div>
	</div>
	</div>
	</div>
	</body>
	</html>";
			} else if (!empty($medicine_name_mail2) && empty($medicine_name_mail3)) {
				$message = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
	<html xmlns='http://www.w3.org/1999/xhtml'>
	<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
	<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>
	</head>
	<body style='margin: 0;font-family:'open sans';'>
	<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;position:relative;'>
	<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>
	<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin:0 auto 0px;'>
	<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
	</a>
	</div>
	<div style='border: none;padding:0px 60px; margin:-40px auto 0px; border-radius:0px; position: absolute; max-width: 580px; top: 110px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>
	<div class='temp_cont' style='width:100%; margin:-40px auto 0px !important;'>
	<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>
	<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
	<span style='display:block;width:100%;'>
	<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>
	</span>
	Medicine Pick up request
	</h2>
	<p style='text-align: center; font-size: 14px; margin-top: 0px;'>Patient Name has requested you to pick up medicines from pharmacy, please see medicine and pharmacy information below</p>
	<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
	Hi &nbsp;" . $username_mail->username . ",
	</h3>
	<style>
								#customers td, #customers th {
	border:1px solid #ddd;
	padding:8px;
}
								#customers th {
padding-top:8px;
padding-bottom:8px;
text-align:left;
background-color:#4CAF50;
color:white;
}
</style>
<table id='customers' style='width:100%; border-collapse: collapse;'>
<tr>
<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#1 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $medicine_name_mail->medicine_name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#2 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $medicine_name_mail2->medicine_name . "</td>
</tr>
<!-- <tr>
<th style='width:200px; border: 1px solid #ddd; font-size:15px; padding:8px; background-color:#4CAF50; text-align:left; color:#fff;'>#2 Medicine Name:</th>
<td style='padding:8px; border:1px solid #ddd;'>Ibuprofen</td>
</tr>
<tr>
<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; background-color:#4CAF50; text-align:left;color:#fff;'>#3 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>Tylenol</td>
</tr>
<tr>
<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; background-color:#4CAF50; text-align:left;color:#fff;'>#4 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>Ernst Handel</td>
</tr>-->
<tr>
<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; text-align:left; background-color:#4CAF50; color:#fff;'>Description:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $data['desc'] . "</td>
</tr> 
<tr>
<th style='width:200px; font-size:15px; padding:8px; text-align:left; background-color:#4CAF50;color:#fff;border: 1px solid #ddd;'>Date and Time for pickup:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $acknowledgeData['send_date'] . "," . $acknowledgeData['send_time'] . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; padding:8px; border: 1px solid #ddd; text-align:left; background-color:#4CAF50;color:#fff;'>Pharmacy Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Address:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->address . "<isindex></isindex></td>
</tr>
<tr>
<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Number:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->contact_no . "</td>
</tr> 
</table>
</div>
<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%;     box-sizing: border-box;'>
<p style='font-size: 13px;margin: 0 0 8px;'>Please call 'sender user name' if you are not able to pick up these medicines</p>						
<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>
<div style='width:50%; float:left;'>
<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>
</p>
</div>
<div style='width:50%; float:right;'>
<div style='border:none;'>
<a href='#' target='_blank' style='display:inline-block;'>
<img style='max-width:120px;width:100%;margin:0 auto 10px;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
</a>
</div>
<p style='font-size: 13px;margin:0 0 8px;'>Carepro customer service</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>9876543210</a>
</p>
</div>
</div>
</div>
</div>
</div>
</body>
</html>";
			} else if (!empty($medicine_name_mail3) && empty($medicine_name_mail2)) {
				$message = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
	<html xmlns='http://www.w3.org/1999/xhtml'>
	<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
	<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>
	</head>
	<body style='margin: 0;font-family:'open sans';'>
	<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;position:relative;'>
	<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>
	<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin:0 auto 0px;'>
	<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
	</a>
	</div>
	<div style='border: none;padding:0px 60px; margin:-40px auto 0px; border-radius:0px; position: absolute; max-width: 580px; top: 110px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>
	<div class='temp_cont' style='width:100%; margin:-40px auto 0px !important;'>
	<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>
	<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
	<span style='display:block;width:100%;'>
	<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>
	</span>
	Medicine Pick up request
	</h2>
	<p style='text-align: center; font-size: 14px; margin-top: 0px;'>Patient Name has requested you to pick up medicines from pharmacy, please see medicine and pharmacy information below</p>
	<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
	Hi &nbsp;" . $username_mail->username . ",
	</h3>
	<style>
								#customers td, #customers th {
	border:1px solid #ddd;
	padding:8px;
}
								#customers th {
padding-top:8px;
padding-bottom:8px;
text-align:left;
background-color:#4CAF50;
color:white;
}
</style>
<table id='customers' style='width:100%; border-collapse: collapse;'>
<tr>
<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#1 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $medicine_name_mail->medicine_name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#2 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $medicine_name_mail3->medicine_name . "</td>
</tr>
<!-- <tr>
<th style='width:200px; border: 1px solid #ddd; font-size:15px; padding:8px; background-color:#4CAF50; text-align:left; color:#fff;'>#2 Medicine Name:</th>
<td style='padding:8px; border:1px solid #ddd;'>Ibuprofen</td>
</tr>
<tr>
<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; background-color:#4CAF50; text-align:left;color:#fff;'>#3 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>Tylenol</td>
</tr>
<tr>
<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; background-color:#4CAF50; text-align:left;color:#fff;'>#4 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>Ernst Handel</td>
</tr>-->
<tr>
<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; text-align:left; background-color:#4CAF50; color:#fff;'>Description:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $data['desc'] . "</td>
</tr> 
<tr>
<th style='width:200px; font-size:15px; padding:8px; text-align:left; background-color:#4CAF50;color:#fff;border: 1px solid #ddd;'>Date and Time for pickup:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $acknowledgeData['send_date'] . "," . $acknowledgeData['send_time'] . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; padding:8px; border: 1px solid #ddd; text-align:left; background-color:#4CAF50;color:#fff;'>Pharmacy Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Address:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->address . "<isindex></isindex></td>
</tr>
<tr>
<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Number:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->contact_no . "</td>
</tr> 
</table>
</div>
<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%;     box-sizing: border-box;'>
<p style='font-size: 13px;margin: 0 0 8px;'>Please call 'sender user name' if you are not able to pick up these medicines</p>						
<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>
<div style='width:50%; float:left;'>
<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>
</p>
</div>
<div style='width:50%; float:right;'>
<div style='border:none;'>
<a href='#' target='_blank' style='display:inline-block;'>
<img style='max-width:120px;width:100%;margin:0 auto 10px;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
</a>
</div>
<p style='font-size: 13px;margin:0 0 8px;'>Carepro customer service</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>9876543210</a>
</p>
</div>
</div>
</div>
</div>
</div>
</body>
</html>";
			} else if (!empty($medicine_name_mail3) && !empty($medicine_name_mail2)) {
				$message = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
	<html xmlns='http://www.w3.org/1999/xhtml'>
	<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
	<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>
	</head>
	<body style='margin: 0;font-family:'open sans';'>
	<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;position:relative;'>
	<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>
	<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin:0 auto 0px;'>
	<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
	</a>
	</div>
	<div style='border: none;padding:0px 60px; margin:-40px auto 0px; border-radius:0px; position: absolute; max-width: 580px; top: 110px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>
	<div class='temp_cont' style='width:100%; margin:-40px auto 0px !important;'>
	<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>
	<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
	<span style='display:block;width:100%;'>
	<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>
	</span>
	Medicine Pick up request
	</h2>
	<p style='text-align: center; font-size: 14px; margin-top: 0px;'>Patient Name has requested you to pick up medicines from pharmacy, please see medicine and pharmacy information below</p>
	<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
	Hi &nbsp;" . $username_mail->username . ",
	</h3>
	<style>
								#customers td, #customers th {
	border:1px solid #ddd;
	padding:8px;
}
								#customers th {
padding-top:8px;
padding-bottom:8px;
text-align:left;
background-color:#4CAF50;
color:white;
}
</style>
<table id='customers' style='width:100%; border-collapse: collapse;'>
<tr>
<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#1 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $medicine_name_mail->medicine_name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#2 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $medicine_name_mail2->medicine_name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff; '>#3 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $medicine_name_mail3->medicine_name . "</td>
</tr>
<!-- <tr>
<th style='width:200px; border: 1px solid #ddd; font-size:15px; padding:8px; background-color:#4CAF50; text-align:left; color:#fff;'>#2 Medicine Name:</th>
<td style='padding:8px; border:1px solid #ddd;'>Ibuprofen</td>
</tr>
<tr>
<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; background-color:#4CAF50; text-align:left;color:#fff;'>#3 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>Tylenol</td>
</tr>
<tr>
<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; background-color:#4CAF50; text-align:left;color:#fff;'>#4 Medicine Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>Ernst Handel</td>
</tr>-->
<tr>
<th style='width:200px; font-size:15px;border: 1px solid #ddd; padding:8px; text-align:left; background-color:#4CAF50; color:#fff;'>Description:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $data['desc'] . "</td>
</tr> 
<tr>
<th style='width:200px; font-size:15px; padding:8px; text-align:left; background-color:#4CAF50;color:#fff;border: 1px solid #ddd;'>Date and Time for pickup:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $acknowledgeData['send_date'] . "," . $acknowledgeData['send_time'] . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; padding:8px; border: 1px solid #ddd; text-align:left; background-color:#4CAF50;color:#fff;'>Pharmacy Name:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->name . "</td>
</tr>
<tr>
<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Address:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->address . "<isindex></isindex></td>
</tr>
<tr>
<th style='width:200px; font-size:15px; text-align:left; border: 1px solid #ddd; padding:8px; background-color:#4CAF50;color:#fff;'>Pharmacy Number:</th>
<td style='padding:8px;border:1px solid #ddd;'>" . $email_pharma_company->contact_no . "</td>
</tr> 
</table>
</div>
<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%;     box-sizing: border-box;'>
<p style='font-size: 13px;margin: 0 0 8px;'>Please call 'sender user name' if you are not able to pick up these medicines</p>						
<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>
<div style='width:50%; float:left;'>
<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>
</p>
</div>
<div style='width:50%; float:right;'>
<div style='border:none;'>
<a href='#' target='_blank' style='display:inline-block;'>
<img style='max-width:120px;width:100%;margin:0 auto 10px;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
</a>
</div>
<p style='font-size: 13px;margin:0 0 8px;'>Carepro customer service</p>
<p style='font-size: 13px;margin:0;'>
<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>9876543210</a>
</p>
</div>
</div>
</div>
</div>
</div>
</body>
</html>";
			}
			socialEmail($email_pharma_company->email, $from_m, $fromname, "Acknowledge", $message);
			socialEmail($selectUser_mail->email, $from_m, $fromname, "Acknowledge", $message);
			$users1 = array(
				'id' => $acknowledgeData['acknowledge_id'],
				'desc' => $acknowledgeData['acknowledge_desc'],
				'select_user_id' => $data['select_user_id'],
				'date' => $data['date'],
				'time' => $data['time'],
				'medicine_id' => $acknowledgeData['medicine_id'],
				'rx_number' => $data['rx_number'],
				'supply_days' => $data['supply_days'],
				'pharma_company_id' => $acknowledgeData['pharma_company_id'],
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Acknowledge update successfully', 'response' => $users1);
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function delete_acknowledge_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('acknowledge_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$ids = explode(',', $data['acknowledge_id']);
		$emailid = $this->common_model->deleteacknowledge($ids);
		if ($emailid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('acknowledge_data' => "Acknowledgment deleted successfully"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Email not found', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/*-------Pharma list--------*/
	public function pharma_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$con['sorting'] = array("id" => "DESC");
		$acknowledgeData  = $this->Api_model->newpharmaList();
		if (!empty($acknowledgeData)) {
			foreach ($acknowledgeData as $acknowledgeData_datails) {
				$acknowledgeData1[] = array(
					'id' => $acknowledgeData_datails['id'],
					'name' => $acknowledgeData_datails['name'],
					'email' => $acknowledgeData_datails['email'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('pharma_data' => $acknowledgeData1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Doctor not found', 'response' => array('message' => 'Acknowledge Data not found'));
		}
		$this->response($resp);
	}
	/*-------Article list--------*/
	public function article_list1_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$con['sorting'] = array("article_id" => "DESC");
		$acknowledgeData  = $this->Api_model->articleList();
		if (!empty($acknowledgeData)) {
			foreach ($acknowledgeData as $acknowledgeData_datails) {
				$FavPhotoData1 = $this->common_model->getSingleRecordById('cp_article_trash', array('user_id' => $data['user_id'], "article_id" => $acknowledgeData_datails['article_id']));
				if ($FavPhotoData1) {
					continue;
				}
				$fav  = $this->getArticleFav($acknowledgeData_datails['article_id']);
				$acknowledgeData1[] = array(
					'id' => $acknowledgeData_datails['article_id'],
					'title' => $acknowledgeData_datails['article_title'],
					'artist_name' => $acknowledgeData_datails['artist_name'],
					'description' => $acknowledgeData_datails['article_desc'],
					'article_image' => base_url() . 'uploads/articles/' . $acknowledgeData_datails['article_image'],
					'artist_image' => base_url() . 'uploads/articles/' . $acknowledgeData_datails['artist_image'],
					'article_icon' => base_url() . 'uploads/articles/' . $acknowledgeData_datails['article_icon'],
					'date' => $acknowledgeData_datails['article_created'],
					'fav' => $fav,
					'user_id' => $acknowledgeData_datails['user_id'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('article_data' => $acknowledgeData1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Article not found', 'response' => array('message' => 'Article not found'));
		}
		$this->response($resp);
	}
	/*-------Fav Article list--------*/
	public function fav_article_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$con['sorting'] = array("article_fav_id" => "DESC");
		$acknowledgeData  = $this->Api_model->favarticleList();
		$acknowledgeData1  = array();
		if (!empty($acknowledgeData)) {
			foreach ($acknowledgeData as $acknowledgeData_datails) {
				$trashData = $this->common_model->getSingleRecordById('cp_article_trash', array('user_id' => $acknowledgeData_datails['user_id'], "article_id" => $acknowledgeData_datails['article_id']));
				if ($trashData) {
					continue;
				}
				$acknowledgeData1[] = array(
					'id' => $acknowledgeData_datails['article_fav_id'],
					'title' => $acknowledgeData_datails['article_title'],
					'artist_name' => $acknowledgeData_datails['artist_name'],
					'description' => $acknowledgeData_datails['article_desc'],
					'article_image' => base_url() . 'uploads/articles/' . $acknowledgeData_datails['article_image'],
					'artist_image' => base_url() . 'uploads/articles/' . $acknowledgeData_datails['artist_image'],
					'date' => $acknowledgeData_datails['article_fav_created'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('articlefav_data' => $acknowledgeData1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Fav Article not found', 'response' => array('message' => 'Fav Article not found'));
		}
		$this->response($resp);
	}
	/*-------Fav Article list--------*/
	public function unfav_article_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('article_fav_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$restid = $this->common_model->deleteRecords('cp_article_favourite', array('article_fav_id' => $data['article_fav_id']));
		if ($restid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/*-------Fav Article list--------*/
	public function article_delete1_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('article_id', 'user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$post_data = array(
			'user_id' => $data['user_id'],
			'article_id' => $data['article_id'],
			'article_trash_date' => date('Y-m-d h:i:s'),
		);
		$restid = $this->common_model->addRecords('cp_article_trash', $post_data);
		// $restid =$this->common_model->deleteRecords('cp_articles',array('article_id'=>$data['article_id']));
		if ($restid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Article Delete successfully', 'response' => array('article_data' => "Article Delete  successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/* add fav photo */
	public function addFavArticle_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id', 'article_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$FavPhotoData1 = $this->common_model->getSingleRecordById('cp_article_favourite', array('user_id' => $data['user_id'], "article_id" => $data['article_id']));
		if (empty($FavPhotoData1)) {
			$post_data = array(
				'user_id' => $data['user_id'],
				'article_id' => $data['article_id'],
				'article_fav_created ' => date('Y-m-d h:i:s'),
				'fav' => 1,
			);
			$FavPhotoId = $this->common_model->addRecords('cp_article_favourite', $post_data);
			if ($FavPhotoId) {
				//$FavPhotoData = $this->common_model->getSingleRecordById('cp_photo_favourite', array('photo_fav_id' => $FavPhotoId));
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Favorite successfully', 'response' => array('fav_data' => "Favorite successfully"));
			} else {
				$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
			}
		} else {
			$this->db->delete('cp_article_favourite', array("user_id" => $data['user_id'], "article_id" => $data['article_id']));
			$FavPhotoId = $this->db->affected_rows();
			if ($FavPhotoId) {
				//$FavPhotoData = $this->common_model->getSingleRecordById('cp_photo_favourite', array('photo_fav_id' => $FavPhotoId));
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));
			} else {
				$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
			}
		}
		$this->response($resp);
	}
	/*-------Music list--------*/
	public function music_list1_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$con['sorting'] = array("music_id" => "DESC");
		$acknowledgeData  = $this->Api_model->musicList();
		if (!empty($acknowledgeData)) {
			foreach ($acknowledgeData as $acknowledgeData_datails) {
				$fav  = $this->getMusicFav($acknowledgeData_datails['music_id']);
				$acknowledgeData1[] = array(
					'id' => $acknowledgeData_datails['music_id'],
					'title' => $acknowledgeData_datails['music_title'],
					'desc' => $acknowledgeData_datails['music_desc'],
					'artist' => $acknowledgeData_datails['music_artist'],
					'music_image' => base_url() . 'uploads/music/image/' . $acknowledgeData_datails['music_image'],
					'music_file' => base_url() . 'uploads/music/' . $acknowledgeData_datails['music_file'],
					'fav' => $fav,
					'type' => $acknowledgeData_datails['music_type']
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('music_data' => $acknowledgeData1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Music not found', 'response' => array('message' => 'Music not found'));
		}
		$this->response($resp);
	}
	/*-------fav Music list--------*/
	public function fav_music_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$con['sorting'] = array("music_id" => "DESC");
		$acknowledgeData  = $this->Api_model->favMusicList($data['user_id']);
		if (!empty($acknowledgeData)) {
			foreach ($acknowledgeData as $acknowledgeData_datails) {
				$acknowledgeData1[] = array(
					'id' => $acknowledgeData_datails['music_fav_id'],
					'title' => $acknowledgeData_datails['music_title'],
					'desc' => $acknowledgeData_datails['music_desc'],
					'artist' => $acknowledgeData_datails['music_artist'],
					'music_id' => $acknowledgeData_datails['music_id'],
					'music_image' => base_url() . 'uploads/music/image/' . $acknowledgeData_datails['music_image'],
					'music_file' => base_url() . 'uploads/music/' . $acknowledgeData_datails['music_file'],
					'user_id' => $acknowledgeData_datails['user_id'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('music_data' => $acknowledgeData1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Music not found', 'response' => array('message' => 'Music not found'));
		}
		$this->response($resp);
	}
	/* add fav photo */
	public function addFavMusic_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id', 'music_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$FavPhotoData1 = $this->common_model->getSingleRecordById('cp_music_favorite', array('user_id' => $data['user_id'], "music_id" => $data['music_id']));
		if (empty($FavPhotoData1)) {
			$post_data = array(
				'user_id' => $data['user_id'],
				'music_id' => $data['music_id'],
				'music_fav_created ' => date('Y-m-d h:i:s'),
				'fav' => 1,
			);
			$FavPhotoId = $this->common_model->addRecords('cp_music_favorite', $post_data);
			if ($FavPhotoId) {
				//$FavPhotoData = $this->common_model->getSingleRecordById('cp_photo_favourite', array('photo_fav_id' => $FavPhotoId));
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Favorite successfully', 'response' => array('fav_data' => "Favorite successfully"));
			} else {
				$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
			}
		} else {
			$this->db->delete('cp_music_favorite', array("user_id" => $data['user_id'], "music_id" => $data['music_id']));
			$FavPhotoId = $this->db->affected_rows();
			if ($FavPhotoId) {
				//$FavPhotoData = $this->common_model->getSingleRecordById('cp_photo_favourite', array('photo_fav_id' => $FavPhotoId));
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));
			} else {
				$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
			}
		}
		$this->response($resp);
	}
	public function music_delete_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('music_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$restid = $this->common_model->deleteRecords('music', array('music_id' => $data['music_id']));
		if ($restid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Music Delete successfully', 'response' => array('music_data' => "Music Delete  successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/*-------Movie list--------*/
	public function movie_list1_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$con['sorting'] = array("movie_id" => "DESC");
		$acknowledgeData  = $this->Api_model->movieList();
		if (!empty($acknowledgeData)) {
			foreach ($acknowledgeData as $acknowledgeData_datails) {
				$fav  = $this->getMovieFav($acknowledgeData_datails['movie_id']);
				$acknowledgeData1[] = array(
					'id' => $acknowledgeData_datails['movie_id'],
					'title' => $acknowledgeData_datails['movie_title'],
					'desc' => $acknowledgeData_datails['movie_desc'],
					'artist' => $acknowledgeData_datails['movie_artist'],
					'movie_image' => base_url() . 'uploads/movie/image/' . $acknowledgeData_datails['movie_image'],
					'movie_file' => base_url() . 'uploads/movie/' . $acknowledgeData_datails['movie_file'],
					'type' => $acknowledgeData_datails['movie_type'],
					'fav' => $fav
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('movie_data' => $acknowledgeData1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Music not found', 'response' => array('message' => 'Movie not found'));
		}
		$this->response($resp);
	}
	public function unFavMulRestaurant_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('fav_rest_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$ids = explode(',', $data['fav_rest_id']);
		$emailid = $this->common_model->deleteRest($ids);
		if ($emailid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('fav_data' => "Unfavorite successfully"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Email not found', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/* add fav photo */
	public function addFavMovie_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id', 'movie_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$FavPhotoData1 = $this->common_model->getSingleRecordById('cp_movie_favorite', array('user_id' => $data['user_id'], "movie_id" => $data['movie_id']));
		if (empty($FavPhotoData1)) {
			$post_data = array(
				'user_id' => $data['user_id'],
				'movie_id' => $data['movie_id'],
				'movie_fav_created' => date('Y-m-d h:i:s'),
				'fav' => 1,
			);
			$FavPhotoId = $this->common_model->addRecords('cp_movie_favorite', $post_data);
			if ($FavPhotoId) {
				//$FavPhotoData = $this->common_model->getSingleRecordById('cp_photo_favourite', array('photo_fav_id' => $FavPhotoId));
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Favorite successfully', 'response' => array('fav_data' => "Favorite successfully"));
			} else {
				$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
			}
		} else {
			$this->db->delete('cp_movie_favorite', array("user_id" => $data['user_id'], "movie_id" => $data['movie_id']));
			$FavPhotoId = $this->db->affected_rows();
			if ($FavPhotoId) {
				//$FavPhotoData = $this->common_model->getSingleRecordById('cp_photo_favourite', array('photo_fav_id' => $FavPhotoId));
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));
			} else {
				$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
			}
		}
		$this->response($resp);
	}
	/*-------fav Music list--------*/
	public function fav_movie_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$pdata = file_get_contents("php://input");
		$data = json_decode($pdata, true);
		$object_info = $data;
		$con['sorting'] = array("movie_id" => "DESC");
		$acknowledgeData  = $this->Api_model->favMovieList($data['user_id']);
		if (!empty($acknowledgeData)) {
			foreach ($acknowledgeData as $acknowledgeData_datails) {
				$acknowledgeData1[] = array(
					'id' => $acknowledgeData_datails['movie_fav_id'],
					'user_id' => $acknowledgeData_datails['user_id'],
					'title' => $acknowledgeData_datails['movie_title'],
					'desc' => $acknowledgeData_datails['movie_desc'],
					'artist' => $acknowledgeData_datails['movie_artist'],
					'movie_id' => $acknowledgeData_datails['movie_id'],
					'movie_image' => base_url() . 'uploads/movie/image/' . $acknowledgeData_datails['movie_image'],
					'movie_file' => base_url() . 'uploads/music/' . $acknowledgeData_datails['movie_file'],
					'fav' => 1,
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('movie_data' => $acknowledgeData1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Movie not found', 'response' => array('message' => 'Movie not found'));
		}
		$this->response($resp);
	}
	/*-------Movie list--------*/
	public function show_caregiver_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$acknowledgeData  = $this->Api_model->caregiverList();
		if (!empty($acknowledgeData)) {
			foreach ($acknowledgeData as $acknowledgeData_datails) {
				$acknowledgeData1[] = array(
					'id' => $acknowledgeData_datails['id'],
					'username' => $acknowledgeData_datails['username'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('caregiver_data' => $acknowledgeData1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Caregiver  not found', 'response' => array('message' => 'Caregiver not found'));
		}
		$this->response($resp);
	}
	/*-------caregiver list with self field--------*/
	public function show_caregiver_self_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		if (!empty($object_info['user_id'])) {
			$user_id = $object_info['user_id'];
		}
		$acknowledgeData  = $this->Api_model->caregiverList();
		if (!empty($acknowledgeData)) {
			foreach ($acknowledgeData as $acknowledgeData_datails) {
				$acknowledgeData1[] = array(
					'id' => $acknowledgeData_datails['id'],
					'username' => $acknowledgeData_datails['username'],
				);
				$test_self = array('id' => $user_id, 'username' => 'Self');
				$merged_array = array_merge($acknowledgeData1, array($test_self));
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('caregiver_data' => $merged_array));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Caregiver  not found', 'response' => array('message' => 'Caregiver not found'));
		}
		$this->response($resp);
	}
	public function unFavMulArticle_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('fav_article_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$ids = explode(',', $data['fav_article_id']);
		$emailid = $this->common_model->deleteArticle($ids);
		if ($emailid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('fav_data' => "Unfavorite successfully"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Email not found', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function send_test_report_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		//$data = $this->param;
		//$object_info = $data;
		$data = $_POST;
		//echo "<pre>" ;print_r($data); die;
		$object_info = $data;
		// print_r($object_info);
		// die;
		$required_parameter = array('test_id', 'description', 'user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		// echo "<pre>";print_r($_FILES);
		// die;
		if (!empty($_FILES['image']['name'])) {
			$random = $this->generateRandomString(10);
			$ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
			$file_name = $random . "." . $ext;
			$target_dir = "uploads/test_report/";
			$target_file = $target_dir . $file_name;
			if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
				$photoFile = $file_name;
			}
			if ($photoFile) {
				$image = $photoFile;
			} else {
				$image = '';
			}
			$test_report_data['image'] = $image;
			$condition = array('id' => $data['test_id']);
			$Id = $this->common_model->updateRecords('cp_test_report', $test_report_data, $condition);
		}
		$testReportFile = $this->common_model->getsingle("cp_test_report", array("id" => $data['test_id']));
		$this->db->select('image');
		$this->db->from('cp_test_report');
		$this->db->where('id', $data['test_id']);
		$query = $this->db->get();
		$result = $query->result_array();
		// echo "<pre>";
		// print_r($testReportFile);
		// echo "<br>";
		// echo "<pre>";
		// print_r($result);
		// die;
		//$result_details['image']="yW2q9AoyVZ.jpg";
		foreach ($result as $result_details) {
			$imageArray[]  = base_url() . 'uploads/test_report/' . $result_details['image'];
		}
		$caregiverData = $this->common_model->getsingle("cp_users", array("email" => $data['caregiver_id'], 'user_role' => 4));
		//echo"<pre>"; print_r($data); die;
		//if(empty($caregiverData)){
		if (!$this->isValidEmail($data['caregiver_id'])) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'input must have valid email in the text', 'response' => array('message' => 'input must have valid email in the text'));
			$this->response($resp);
			// $resp = array('status_code' => FAILURE,'status' => 'false', 'message' => "The inserted Email is not associated with any caregiver's account",'response' => array('message' => "The inserted Email is not associated with any caregiver's account"));
			// $this->response($resp);
		} else {
			$post_data = array(
				'test_id' => $data['test_id'],
				'caregiver_id' => (!empty($data['caregiver_id']) ? $data['caregiver_id'] : ''),
				'doc_id' => (!empty($data['doc_id']) ? $data['doc_id'] : ''),
				'description' => $data['description'],
				'user_id' => $data['user_id'],
				'send_test_report_created' => date('Y-m-d H:i:s')
			);
			$currentDateTime = $post_data['send_test_report_created'];
			$newDateTime = date('d-m-y, h:i A', strtotime($currentDateTime));
			$emailId = $this->common_model->addRecords('send_test_report', $post_data);
		}
		// echo $emailId;
		// die;
		if ($emailId) {
			$testType = $this->common_model->getsingle("cp_test_report", array("id" => $data['test_id']));
			// echo $this->db->last_query();
			// die;
			// print_r($testType);
			// die;
			$username = $this->common_model->getsingle("cp_users", array("id" => $data['user_id']));
			$subject = "Send test report";
			// 		if($data['doc_id']){
			// 			$doctormail = $this->common_model->getsingle("cp_doctor",array("doctor_id" => $data['doc_id']));
			// 			$message ="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
			// 			<html xmlns='http://www.w3.org/1999/xhtml'>
			// 			<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
			// 			<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>
			// 			</head>
			// 			<body style='margin: 0;font-family:'open sans';'>
			// 			<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;'>
			// 			<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>
			// 			<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin: 0 auto 0px;'>
			// 			<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
			// 			</a>
			// 			</div>
			// 			<div style='border:none; padding:0px 60px; max-width:580px; margin:-40px auto 0px; border-radius:0px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>
			// 			<div class='temp_cont' style='width:100%; margin:0 auto;'>
			// 			<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>
			// 			<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
			// 			<span style='display:block;width:100%;'>
			// 			<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>
			// 			</span>
			// 			Test report
			// 			</h2>
			// 			<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
			// 			Hi &nbsp;".$username->username."</h3>
			// 			<p style='text-align: center; font-size: 14px; margin-top: 0px; padding:0px 15px;' >Patient Name has sent you a document to view, please click on attached file to view the document.</p>
			// 			<style>
			// 						#customers td, #customers th {
			// 			border:1px solid #ddd;
			// 			padding:8px;
			// 		}
			// 						#customers th {
			// 		padding-top:8px;
			// 		padding-bottom:8px;
			// 		text-align:left;
			// 		background-color:#4CAF50;
			// 		color:white;
			// 	}
			// 	</style>
			// 	<table id='customers' style='width:100%; border-collapse: collapse;'>
			// 	<tr>
			// 	<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Doctor Name:</th>
			// 	<td style='padding:8px;border:1px solid #ddd;'>".$doctormail->doctor_name."</td>
			// 	</tr>						  
			// 	<tr>
			// 	<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Date and Time:</th>
			// 	<td style='padding:8px;border:1px solid #ddd;'>".$newDateTime."</td>
			// 	</tr>
			// 	<tr>
			// 	<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Description:</th>
			// 	<td style='font-size:13px; padding:8px;border:1px solid #ddd;'>".$data['description']."</td>
			// 	</tr>
			// 	</table>
			// 	</div>
			// 	<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%; box-sizing:border-box;'>
			// 	<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>
			// 	<p style='font-size: 13px;margin:0;'>
			// 	<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>
			// 	</p>						
			// 	<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>
			// 	<p style='font-size: 13px;margin:0 0 8px;'>Care Pro</p>
			// 	<p style='font-size: 13px;margin:0;'>
			// 	<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>EMAIL: info@carepro.com/</a>
			// 	</p>
			// 	</div>
			// 	</div>
			// 	</div>
			// 	</div>
			// 	</body>
			// 	</html>";
			// //$chk_mail1 = sendEmail1($doctormail->doctor_email,$subject,$message,$imageArray);
			// 	$chk_mail1= new_send_mail($message, $subject, $doctormail->doctor_email,$imageArray);
			// }
			if (!empty($testType->test_report_title)) {
				$Type = $testType->test_report_title;
			} else {
				$Type = "N.A.";
			}
			if ($data['caregiver_id']) {
				// echo $emailId;
				// die;
				//$caregiver_data = $this->common_model->getsingle("cp_users",array("id" => $caregiverData->id));
				$message = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
			<html xmlns='http://www.w3.org/1999/xhtml'>
			<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
			<title>Care Pro</title> <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>
			</head>
			<body style='margin: 0;font-family:'open sans';'>
			<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;'>
			<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>
			<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin: 0 auto 0px;'>
			<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
			</a>
			</div>
			<div style='border:none; padding:0px 60px; max-width:580px; margin:-40px auto 0px; border-radius:0px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>
			<div class='temp_cont' style='width:100%; margin:0 auto;'>
			<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>
			<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
			<span style='display:block;width:100%;'>
			<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>
			</span>
			Test report
			</h2>
			<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
			Hi &nbsp;" . $username->username . ",
			</h3>
			<p style='text-align: center; font-size: 14px; margin-top: 0px; padding:0px 15px;' >" . $username->username . " has sent you a document to view, please click on attached file to view the document.</p>
			<style>
							#customers td, #customers th {
			border:1px solid #ddd;
			padding:8px;
		}
							#customers th {
		padding-top:8px;
		padding-bottom:8px;
		text-align:left;
		background-color:#4CAF50;
		color:white;
	}
	</style>
	<table id='customers' style='width:100%; border-collapse: collapse;'>
	<tr>
	<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Caregiver Email:</th>
	<td style='padding:8px;border:1px solid #ddd;'>" . $data['caregiver_id'] . "</td>
	</tr>
	<tr>
	<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Test Type:</th>
	<td style='padding:8px;border:1px solid #ddd;'>" . $Type . "</td>
	</tr>						  
	<tr>
	<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Date and Time:</th>
	<td style='padding:8px;border:1px solid #ddd;'>" . $newDateTime . "</td>
	</tr>
	<tr>
	<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Description:</th>
	<td style='font-size:13px; padding:8px;border:1px solid #ddd;'>" . $data['description'] . "</td>
	</tr>
	</table>
	</div>
	<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%; box-sizing:border-box;'>
	<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>
	<p style='font-size: 13px;margin:0;'>
	<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>
	</p>						
	<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>
	<p style='font-size: 13px;margin:0 0 8px;'>Care Pro</p>
	<p style='font-size: 13px;margin:0;'>
	<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>EMAIL: info@carepro.com/</a>
	</p>
	</div>
	</div>
	</div>
	</div>
	</body>
	</html>";
				// $message =
				// "<table>
				// <tr><th>Caregiver:</th>
				// <td>".$caregiver_data->username."</td></tr>
				// <tr><th>Username:</th>
				// <td>".$username->username."</td></tr>
				// <tr><th>Description:</th>
				// <td>".$data['description']."</td></tr>
				// </table>";
				// $chk_mail2 = sendEmail1($caregiver_data->email,$subject,$message,$imageArray);
				$chk_mail2 = new_send_mail($message, $subject, $data['caregiver_id'], $imageArray);
				// echo $chk_mail2;
				// die;
			}
			$emailData = $this->common_model->getSingleRecordById('send_test_report', array('send_test_report_id' => $emailId));
			// echo "<pre>";
			// print_r($emailData);
			// die;
			$orderData = array(
				'test_id' => $emailData['test_id'],
				'description' => $emailData['description'],
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Send Test Report successfully', 'response' => $orderData);
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function add_pickup_medicine_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('medicine_id', 'user_id', 'caregiver_id', 'pharmacy');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$post_data = array(
			// 'doctor_id'=> $data['doctor_id'],
			'medicine_id' => $data['medicine_id'],
			'user_id' => $data['user_id'],
			'pharmacy' => $data['pharmacy'],
			'caregiver_id' => $data['caregiver_id'],
			'create_date' => date('Y-m-d H:i:s')
		);
		$emailId = $this->common_model->addRecords('picup_medicine', $post_data);
		if ($emailId) {
			$caregiver_data = $this->common_model->getsingle("cp_users", array("id" => $data['caregiver_id']));
			$doctormail = $this->common_model->getsingle("cp_doctor", array("doctor_id" => $data['doctor_id']));
			$username = $this->common_model->getsingle("cp_users", array("id" => $data['user_id']));
			$medicine = $this->common_model->getsingle("medicine_schedule", array("medicine_id" => $data['medicine_id']));
			$subject = "Pickup Medicine";
			$message =
				"<table>
		<tr><th>Username:</th>
		<td>" . $caregiver_data->username . "</td></tr>
		<tr><th>Medicine:</th>
		<td>" . $medicine->medicine_name . "</td></tr>
		</table>";
			//$chk_mail1= new_send_mail($message, $subject, $caregiver_data->email,'');
			$from_m = "info@carepro.com";
			$fromname = "Carepro";
			$chk_mail =  socialEmail($caregiver_data->email, $from_m, $fromname, $subject, $message);
			$orderData = array(
				'caregiver_name' => $caregiver_data->username,
				//  'doctor_name' => $doctormail->doctor_name,
				'medicine_name' => $medicine->medicine_name
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Pickup medicine inserted successfully', 'response' => $orderData);
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/*-------show pick up medicine--------*/
	public function pickup_medicine_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$con['sorting'] = array("id" => "DESC");
		$acknowledgeData  = $this->Api_model->pickupList();
		if (!empty($acknowledgeData)) {
			foreach ($acknowledgeData as $acknowledgeData_datails) {
				$userData = $this->common_model->getsingle("cp_users", array("id" => $acknowledgeData_datails['caregiver_id']));
				$pharma_company = $this->common_model->getsingle("pharma_company", array("id" => $acknowledgeData_datails['pharmacy']));
				$acknowledgeData1[] = array(
					'id' => $acknowledgeData_datails['id'],
					'title' => $acknowledgeData_datails['user_id'],
					'medicine_name' => $acknowledgeData_datails['medicine_name'],
					'user_id' => $userData->id,
					'username' => $userData->username,
					'medicine_id' => $acknowledgeData_datails['medicine_id'],
					'pharmacy' => $pharma_company->name,
					'pharmacy_id' => $acknowledgeData_datails['pharmacy'],
					// 'doctor_name'=>$acknowledgeData_datails['doctor_name'],
					'date' => $acknowledgeData_datails['create_date'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('pickupList' => $acknowledgeData1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'pickup medicine not found', 'response' => array('message' => 'pickup medicine not found'));
		}
		$this->response($resp);
	}
	/*-------Delete pick up medicine--------*/
	public function delete_pickup_medicine_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('pickup_medicine_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$ids = explode(',', $data['pickup_medicine_id']);
		$emailid = $this->common_model->deletepickupmedicine($ids);
		if ($emailid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('pickup_medicine_data' => "Pickup medicine deleted successfully"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'pickup medicine not found', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/*-------Update pick up medicine--------*/
	public function update_pickup_medicine_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array("medicine_id", 'caregiver_id', 'user_id', 'pickup_medicine_id', 'pharmacy');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$post_data = array(
			// 'doctor_id'=> $data['doctor_id'],
			'medicine_id' => $data['medicine_id'],
			'pharmacy' => $data['pharmacy'],
			'caregiver_id' => $data['caregiver_id'],
			'user_id' => $data['user_id'],
		);
		$acknowledgeId = $this->common_model->updateRecords('picup_medicine', $post_data, array('id' => $data['pickup_medicine_id']));
		if ($acknowledgeId) {
			$caregiver_data = $this->common_model->getsingle("cp_users", array("id" => $data['caregiver_id']));
			$username = $this->common_model->getsingle("cp_users", array("id" => $data['user_id']));
			$medicine = $this->common_model->getsingle("medicine_schedule", array("medicine_id" => $data['medicine_id']));
			$subject = "Pickup Medicine";
			$message =
				"<table>
		<tr><th>Username:</th>
		<td>" . $caregiver_data->username . "</td></tr>
		<tr><th>Medicine:</th>
		<td>" . $medicine->medicine_name . "</td></tr>
		</table>";
			//$chk_mail1= new_send_mail($message, $subject, $caregiver_data->email,'');
			$from_m = "info@carepro.com";
			$fromname = "Carepro";
			$chk_mail =  socialEmail($caregiver_data->email, $from_m, $fromname, $subject, $message);
			$acknowledgeData = $this->common_model->getSingleRecordById('picup_medicine', array('id' => $data['pickup_medicine_id']));
			$users1 = array(
				'id' => $acknowledgeData['id'],
				//'doctor_id' => $acknowledgeData['doctor_id'],
				'medicine_id' => $acknowledgeData['medicine_id'],
				'caregiver_id' => $acknowledgeData['caregiver_id'],
				'user_id' => $acknowledgeData['user_id'],
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Pickup medicine update successfully', 'response' => $users1);
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/*-------show contact list--------*/
	public function user_contact_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$con['sorting'] = array("contact_id" => "DESC");
		$contactData  = $this->Api_model->contactList($data['user_id']);
		if (!empty($contactData)) {
			foreach ($contactData as $contactData_datails) {
				$acknowledgeData1[] = array(
					'id' => $contactData_datails['contact_id'],
					'contact_name' => $contactData_datails['contact_name'],
					'mobile_number' => $contactData_datails['contact_mobile_number'],
					'user_id' => $contactData_datails['contact_user_id'],
					//'contact_image'=>$contactData_datails['user_contact_image'],
					'contact_image' => base_url() . 'uploads/contact/' . $contactData_datails['user_contact_image'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('contactList' => $acknowledgeData1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Contact not found', 'response' => array('message' => 'Contact not found'));
		}
		$this->response($resp);
	}
	/*-------Delete user contact--------*/
	public function delete_user_contact_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_contact_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$ids = explode(',', $data['user_contact_id']);
		$contactid = $this->common_model->deleteContact($ids);
		if ($contactid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('pickup_medicine_data' => "contact data deleted succesfully"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'contact not found', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/*-------show contact list--------*/
	public function family_contact_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$con['sorting'] = array("contact_id" => "DESC");
		$contactData  = $this->Api_model->contactList($data['user_id']);
		if (!empty($contactData)) {
			foreach ($contactData as $contactData_datails) {
				$acknowledgeData1[] = array(
					'id' => $contactData_datails['contact_id'],
					'contact_name' => $contactData_datails['contact_name'],
					'mobile_number' => $contactData_datails['contact_mobile_number'],
					'user_id' => $contactData_datails['contact_user_id'],
					//'contact_image'=>$contactData_datails['user_contact_image'],
					'contact_image' => base_url() . 'uploads/contact/' . $contactData_datails['user_contact_image'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('contactList' => $acknowledgeData1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Contact not found', 'response' => array('message' => 'Contact not found'));
		}
		$this->response($resp);
	}
	/*-------Delete  family contact--------*/
	public function delete_family_contact_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_contact_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$ids = explode(',', $data['user_contact_id']);
		$contactid = $this->common_model->deleteContact($ids);
		if ($contactid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('pickup_medicine_data' => "contact data deleted succesfully"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'contact not found', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function get_facebook_url_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('facebook_url' => "https://www.facebook.com/"));
		$this->response($resp);
	}
	public function get_twitter_url_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('twitter_url' => "https://twitter.com/"));
		$this->response($resp);
	}
	// Add Restaurant data
	public function add_grocery_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id', "name", "image", "lat", "long", "address", "place_id", "rating", 'icon');
		$chk_error = check_required_value($required_parameter, $data['rests'][0]);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		for ($i = 0; $i < count($data['rests']); $i++) {
			$place_id = $this->common_model->getSingleRecordById('cp_grocery', array('place_id' => $data['rests'][$i]['place_id'], "user_id" => $data['rests'][$i]['user_id']));
			if ($place_id) {
				continue;
			}
			$post_data = array(
				'user_id' => (!empty($data['rests'][$i]['user_id']) ? $data['rests'][$i]['user_id'] : ''),
				'grocery_name' => (!empty($data['rests'][$i]['name']) ? $data['rests'][$i]['name'] : ''),
				'grocery_image' => (!empty($data['rests'][$i]['image']) ? $data['rests'][$i]['image'] : ''),
				'grocery_lat' => (!empty($data['rests'][$i]['lat']) ? $data['rests'][$i]['lat'] : ''),
				'grocery_long' => (!empty($data['rests'][$i]['long']) ? $data['rests'][$i]['long'] : ''),
				'place_id' => (!empty($data['rests'][$i]['place_id']) ? $data['rests'][$i]['place_id'] : ''),
				'grocery_address' => (!empty($data['rests'][$i]['rating']) ? $data['rests'][$i]['rating'] : ''),
				'grocery_icon' => (!empty($data['rests'][$i]['icon']) ? $data['rests'][$i]['icon'] : ''),
				'grocery_address' => (!empty($data['rests'][$i]['address']) ? $data['rests'][$i]['address'] : ''),
				'grocery_created' => date('Y-m-d H:i:s'),
				'grocery_rating' => (!empty($data['rests'][$i]['rating']) ? $data['rests'][$i]['rating'] : '')
			);
			$restaurantId = $this->common_model->addRecords('cp_grocery', $post_data);
		}
		$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Add Grocery successfully', 'response' => array('message' => 'Add Grocery successfully'));
		$this->response($resp);
	}
	// list restaurant data
	public function grocery_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$limit = 10;
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$start  = ($data['page_no'] - 1) * $limit;
		$con1['returnType'] = 'count';
		$table = "cp_grocery";
		$con1['conditions'] = array("user_id" => $data['user_id']);
		$category_count  = $this->Api_model->getRows($table, $con1);
		$pages = ceil(count($category_count) / $limit);
		$con['sorting'] = array("grocery_id" => "DESC");
		$con['limit'] = $limit;
		$con['start'] = $start;
		$con['conditions'] = array("user_id" => $data['user_id']);
		$restaurantDatas  = $this->Api_model->getRows($table, $con);
		if (!empty($restaurantDatas)) {
			foreach ($restaurantDatas as $restaurantData_details) {
				$fav  = $this->getGroceryFav($restaurantData_details['grocery_id']);
				$restaurantData[] = array(
					'id' => $restaurantData_details['grocery_id'],
					'name' => $restaurantData_details['grocery_name'],
					'image' => $restaurantData_details['grocery_image'],
					'address' => $restaurantData_details['grocery_address'],
					'user_id' => $restaurantData_details['user_id'],
					'lat' => $restaurantData_details['grocery_lat'],
					'long' => $restaurantData_details['grocery_long'],
					'rating' => $restaurantData_details['grocery_rating'],
					'fav' => $fav
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('grocery_data' => $restaurantData));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Grocery not found', 'response' => array('message' => 'Restaurant not found'));
		}
		$this->response($resp);
	}
	public function add_fav_grocery_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('grocery_name', 'grocery_address', 'grocery_image', 'rating', 'place_id', 'user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		//$address  =  $this->get_address($data['lat'],$data['long']);
		$post_data = array(
			'user_id' => $data['user_id'],
			'fav_grocery_name' => $data['grocery_name'],
			'fav_grocery_image' => $data['grocery_image'],
			'fav_grocery_address' => $data['grocery_address'],	'fav_grocery_rating' => $data['rating'],
			'fav_grocery_place_id' => $data['place_id'],
			'fav_grocery_created' => date('Y-m-d H:i:s'),
		);
		$restaurantId = $this->common_model->addRecords('cp_fav_grocery', $post_data);
		if ($restaurantId) {
			$restaurantData = $this->common_model->getSingleRecordById('cp_fav_grocery', array('fav_grocery_id' => $restaurantId));
			$restaurantData1 = array(
				'id' => $restaurantData['fav_grocery_id'],
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Favorite successfully', 'response' => array('fav_data' => "favorite successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function unfav_grocery_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('place_id', 'user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$restid = $this->common_model->deleteRecords('cp_fav_grocery', array('fav_grocery_place_id' => $data['place_id'], 'user_id' => $data['user_id']));
		if ($restid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function fav_grocery_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$limit = 10;
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$start  = ($data['page_no'] - 1) * $limit;
		$con1['returnType'] = 'count';
		$table = "cp_fav_grocery";
		$con1['conditions'] = array("cp_fav_grocery.user_id" => $data['user_id']);
		$bank_count  = $this->Api_model->getRowsGrocerynew($table, $con1);
		$pages = ceil(count($bank_count) / $limit);
		$con['sorting'] = array("cp_fav_grocery.fav_grocery_id" => "DESC");
		$con['limit'] = $limit;
		$con['start'] = $start;
		$con['conditions'] = array("cp_fav_grocery.user_id" => $data['user_id']);
		$bankDatas  = $this->Api_model->getRowsGrocerynew($table, $con);
		//echo '<pre>';print_r($bankDatas);die;
		// echo $this->db->last_query();die;
		if (!empty($bankDatas)) {
			foreach ($bankDatas as $bankDatas_details) {
				$bankData[] = array(
					'id' => $bankDatas_details['fav_grocery_id'],
					'name' => $bankDatas_details['fav_grocery_name'],
					'image' => $bankDatas_details['fav_grocery_image'],
					'address' => $bankDatas_details['fav_grocery_address'],
					'user_id' => $bankDatas_details['user_id'],
					'rating' => $bankDatas_details['fav_grocery_rating'],
					'place_id' => $bankDatas_details['fav_grocery_place_id'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('grocery_data' => $bankData));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Fav Grocery not found', 'response' => array('message' => 'Fav Grocery not found'));
		}
		$this->response($resp);
	}
	/*-------Pharma list--------*/
	public function note_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		// $post_data = array
		// (
		// 	'user_id'=> $data['user_id'],
		// );
		$con['sorting'] = array("note_id" => "DESC");
		if ($data['read_status'] == '0') {
			$user_id = $data['user_id'];
			$acknowledgeData  = $this->common_model->getAllordynamic($data['read_status'], $user_id);
		} else {
			$user_id = $data['user_id'];
			$acknowledgeData  = $this->common_model->getAllornew("cp_note", $user_id);
		}
		// $acknowledgeData  = $this->Api_model->noteList();
		if (!empty($acknowledgeData)) {
			foreach ($acknowledgeData as $acknowledgeData_datails) {
				$acknowledgeData1[] = array(
					'id' => $acknowledgeData_datails['note_id'],
					'from_caregiver' => $this->getGiver($acknowledgeData_datails['from_caregiver']),
					'to_caregiver' => $this->getGiver($acknowledgeData_datails['to_caregiver']),
					'description' => $acknowledgeData_datails['description'],
					'read_status' => $acknowledgeData_datails['read_status'],
					'date' => $acknowledgeData_datails['create_date'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('note_data' => $acknowledgeData1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Note Data not found', 'response' => array('message' => 'Note Data not found'));
		}
		$this->response($resp);
	}
	function getGiver($user_id)
	{
		$this->db->select('*');
		$this->db->from('cp_users');
		$this->db->where('id', $user_id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result[0]['username'];
	}
	public function add_note_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('from_caregiver', "to_caregiver", "description", "user_id");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$post_data = array(
			'from_caregiver' => $data['from_caregiver'],
			'to_caregiver' => $data['to_caregiver'],
			'user_id' => $data['user_id'],
			'description' => $data['description'],
			'create_date' => date('Y-m-d h:i:s'),
		);
		$drAppId = $this->common_model->addRecords('cp_note', $post_data);
		if ($drAppId) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Note Added successfully', 'response' => array('read_status' => 0));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function update_note_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('from_caregiver', "to_caregiver", "description", "note_id", 'read_status');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$post_data = array(
			'from_caregiver' => $data['from_caregiver'],
			'to_caregiver' => $data['to_caregiver'],
			'user_id' => $data['user_id'],
			'description' => $data['description'],
			'note_id' => $data['note_id'],
			'read_status' => $data['read_status'],
			'create_date' => date('Y-m-d h:i:s'),
		);
		$acknowledgeId = $this->common_model->updateRecords('cp_note', $post_data, array('note_id' => $data['note_id']));
		if ($acknowledgeId) {
			$getdata = $this->common_model->getsingle("cp_note", array('note_id' => $data['note_id']));
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Note update successfully', 'response' => array("read_status" => $getdata->read_status));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function delete_note_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('note_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$restid = $this->common_model->deleteRecords('cp_note', array('note_id' => $data['note_id']));
		if ($restid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Note deleted successfully', 'response' => array('note_data' => "Note deleted successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function add_website_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id', "name", "image", "lat", "long", "address", "place_id", "rating", 'icon');
		$chk_error = check_required_value($required_parameter, $data['rests'][0]);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		for ($i = 0; $i < count($data['rests']); $i++) {
			$place_id = $this->common_model->getSingleRecordById('cp_website', array('place_id' => $data['rests'][$i]['place_id'], "user_id" => $data['rests'][$i]['user_id']));
			if ($place_id) {
				continue;
			}
			$post_data = array(
				'user_id' => (!empty($data['rests'][$i]['user_id']) ? $data['rests'][$i]['user_id'] : ''),
				'website_name' => (!empty($data['rests'][$i]['name']) ? $data['rests'][$i]['name'] : ''),
				'website_image' => (!empty($data['rests'][$i]['image']) ? $data['rests'][$i]['image'] : ''),
				'website_lat' => (!empty($data['rests'][$i]['lat']) ? $data['rests'][$i]['lat'] : ''),
				'website_long' => (!empty($data['rests'][$i]['long']) ? $data['rests'][$i]['long'] : ''),
				'place_id' => (!empty($data['rests'][$i]['place_id']) ? $data['rests'][$i]['place_id'] : ''),
				'website_icon' => (!empty($data['rests'][$i]['icon']) ? $data['rests'][$i]['icon'] : ''),
				'website_address' => (!empty($data['rests'][$i]['address']) ? $data['rests'][$i]['address'] : ''),
				'website_created' => date('Y-m-d H:i:s'),
				'website_rating' => (!empty($data['rests'][$i]['rating']) ? $data['rests'][$i]['rating'] : '')
			);
			$restaurantId = $this->common_model->addRecords('cp_website', $post_data);
		}
		$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Add Website successfully', 'response' => array('message' => 'Add Website successfully'));
		$this->response($resp);
	}
	public function website_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$limit = 10;
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$start  = ($data['page_no'] - 1) * $limit;
		$con1['returnType'] = 'count';
		$table = "cp_website";
		$con1['conditions'] = array("user_id" => $data['user_id']);
		$bank_count  = $this->Api_model->getRows($table, $con1);
		$pages = ceil(count($bank_count) / $limit);
		$con['sorting'] = array("website_id" => "DESC");
		$con['limit'] = $limit;
		$con['start'] = $start;
		$con['conditions'] = array("user_id" => $data['user_id']);
		$bankDatas  = $this->Api_model->getRows($table, $con);
		if (!empty($bankDatas)) {
			foreach ($bankDatas as $bankDatas_details) {
				//$fav  = $this->getBankFav($bankDatas_details['bank_id']);
				$bankData[] = array(
					'id' => $bankDatas_details['website_id'],
					'name' => $bankDatas_details['website_name'],
					'image' => $bankDatas_details['website_image'],
					'address' => $bankDatas_details['website_address'],
					'user_id' => $bankDatas_details['user_id'],
					'rating' => $bankDatas_details['website_rating'],
					//'fav'=>$fav,
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('website_data' => $bankData));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Bank not found', 'response' => array('message' => 'Bank not found'));
		}
		$this->response($resp);
	}
	public function add_fav_website_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('website_id', 'user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		//$address  =  $this->get_address($data['lat'],$data['long']);
		$post_data = array(
			'user_id' => $data['user_id'],
			'website_id' => $data['website_id'],
			'fav_website_created' => date('Y-m-d H:i:s'),
		);
		$restaurantId = $this->common_model->addRecords('cp_fav_website', $post_data);
		if ($restaurantId) {
			$restaurantData = $this->common_model->getSingleRecordById('cp_fav_website', array('fav_website_id' => $restaurantId));
			$restaurantData1 = array(
				'id' => $restaurantData['fav_website_id'],
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Favorite successfully', 'response' => array('fav_data' => "favorite successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function unfav_website_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('website_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$restid = $this->common_model->deleteRecords('cp_fav_website', array('website_id' => $data['website_id']));
		if ($restid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Unfavorite successfully', 'response' => array('fav_data' => "Unfavorite successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	// all music list
	public function music_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$limit = 10;
		// get banner data
		$bannerCon['sorting'] = array("music_banner_id" => "DESC");
		$bannerCon['conditions'] = array("music_banner_status" => 1);
		$bannerCon['limit'] = $limit;
		$banner_data  = $this->Api_model->getRows('cp_music_banner', $bannerCon);
		$bannerTotalCon['conditions'] = array("music_banner_status" => 1);
		$banner_Total_data  = $this->Api_model->getRows('cp_music_banner', $bannerTotalCon);
		if (empty($banner_data)) {
			$musicList['Banners'] = array();
		} else {
			$musicList['Banners']['bannerCount'] = count($banner_Total_data);
			foreach ($banner_data as $row_banner_data) {
				$musicList['Banners']['bannerList'][] = array(
					'id' => $row_banner_data['music_banner_id'],
					'music_banner_path' => base_url() . 'uploads/music/banners/' . $row_banner_data['music_banner_image'],
					'music_banner_file' => base_url() . 'uploads/music/banners/files/' . $row_banner_data['music_banner_file']
				);
			}
		}
		// get popular music data
		$popularCon['sorting'] = array("music_view" => "DESC");
		$popularCon['limit'] = $limit;
		$popular_music_data  = $this->Api_model->getRows('music', $popularCon);
		$popular_Total_data  = $this->Api_model->getRows('music');
		if (empty($popular_music_data)) {
			$musicList['Populars']['popularCount'] = "0";
			$musicList['Populars']['popularList'] = array();
		} else {
			$musicList['Populars']['popularCount'] = count($popular_Total_data);
			foreach ($popular_music_data as $row_popular_data) {
				$popularFav  = $this->getMusicUserFav($row_popular_data['music_id'], $data['user_id']);
				$popularCategoryName  = $this->getCategoryName($row_popular_data['category_id']);
				$musicList['Populars']['popularList'][] = array(
					'music_id' => $row_popular_data['music_id'],
					'title' => $row_popular_data['music_title'],
					'desc' => $row_popular_data['music_desc'],
					'artist' => $row_popular_data['music_artist'],
					'music_image' => base_url() . 'uploads/music/image/' . $row_popular_data['music_image'],
					'music_file' => base_url() . 'uploads/music/' . $row_popular_data['music_file'],
					'fav' => $popularFav,
					'category_name' => $popularCategoryName,
					'user_id' => $row_popular_data['user_id'],
				);
			}
		}
		// get recent music data
		$recentCon['sorting'] = array("music_id" => "DESC");
		$recentCon['limit'] = $limit;
		$recent_music_data  = $this->Api_model->getRows('music', $recentCon);
		$recent_Total_data  = $this->Api_model->getRows('music');
		if (empty($recent_music_data)) {
			$musicList['Recents']['recentCount'] = "0";
			$musicList['Recents']['recentList'] = array();
		} else {
			$musicList['Recents']['recentCount'] = count($recent_Total_data);
			foreach ($recent_music_data as $row_recent_data) {
				$recentFav  = $this->getMusicUserFav($row_recent_data['music_id'], $data['user_id']);
				$recentCategoryName  = $this->getCategoryName($row_recent_data['category_id']);
				$musicList['Recents']['recentList'][] = array(
					'music_id' => $row_recent_data['music_id'],
					'title' => $row_recent_data['music_title'],
					'desc' => $row_recent_data['music_desc'],
					'artist' => $row_recent_data['music_artist'],
					'music_image' => base_url() . 'uploads/music/image/' . $row_recent_data['music_image'],
					'music_file' => base_url() . 'uploads/music/' . $row_recent_data['music_file'],
					'fav' => $recentFav,
					'category_name' => $recentCategoryName,
					'user_id' => $row_recent_data['user_id'],
				);
			}
		}
		// get category data
		$categoryCon['sorting'] = array("music_category_id" => "DESC");
		$categoryCon['limit'] = $limit;
		$categoryCon['conditions'] = array("music_category_status" => 1);
		$category_data  = $this->Api_model->getRows('cp_music_category', $categoryCon);
		$categoryTotalCon['conditions'] = array("music_category_status" => 1);
		$category_Total_data  = $this->Api_model->getRows('cp_music_category', $categoryTotalCon);
		if (empty($category_data)) {
			$musicList['Categories'] = array();
		} else {
			$musicList['Categories']['categoryCount'] = count($category_Total_data);
			foreach ($category_data as $row_category_data) {
				$musicList['Categories']['categoryList'][] = array(
					'id' => $row_category_data['music_category_id'],
					'name' => $row_category_data['music_category_name'],
					'category_icon' => base_url() . 'uploads/music/category_icon/' . $row_category_data['music_category_icon'],
				);
			}
		}
		// get my music data
		$myMusicCon['sorting'] = array("music_id" => "DESC");
		$myMusicCon['conditions'] = array("user_id" => $data['user_id']);
		$myMusicCon['limit'] = $limit;
		$my_music_data  = $this->Api_model->getRows('music', $myMusicCon);
		$myMusicTotalCon['conditions'] = array("user_id" => $data['user_id']);
		$myMusic_Total_data  = $this->Api_model->getRows('music', $myMusicTotalCon);
		if (empty($my_music_data)) {
			$musicList['myMusic']['myMusicCount'] = "0";
			$musicList['myMusic']['myMusicList'] = array();
		} else {
			$musicList['myMusic']['myMusicCount'] = count($myMusic_Total_data);
			foreach ($my_music_data as $row_mymusic_data) {
				$myMusicFav  = $this->getMusicUserFav($row_mymusic_data['music_id'], $data['user_id']);
				$myMusicCategoryName  = $this->getCategoryName($row_mymusic_data['category_id']);
				$musicList['myMusic']['myMusicList'][] = array(
					'music_id' => $row_mymusic_data['music_id'],
					'title' => $row_mymusic_data['music_title'],
					'desc' => $row_mymusic_data['music_desc'],
					'artist' => $row_mymusic_data['music_artist'],
					'music_image' => base_url() . 'uploads/music/image/' . $row_mymusic_data['music_image'],
					'music_file' => base_url() . 'uploads/music/' . $row_mymusic_data['music_file'],
					'fav' => $myMusicFav,
					'category_name' => $myMusicCategoryName,
					'user_id' => $row_mymusic_data['user_id'],
				);
			}
		}
		// get favourite music data
		$favMusicCon['sorting'] = array("music_fav_id" => "DESC");
		$favMusicCon['limit'] = $limit;
		$favMusicCon['conditions'] = array("cp_music_favorite.user_id" => $data['user_id']);
		$fav_music_data  = $this->Api_model->getRowsFavouriteMusic('cp_music_favorite', $favMusicCon);
		// print_r("<pre/>");
		//print_r($fav_music_data);
		//  die;
		$favMusicTotalCon['conditions'] = array("cp_music_favorite.user_id" => $data['user_id']);
		$favMusic_Total_data  = $this->Api_model->getRowsFavouriteMusic('cp_music_favorite', $favMusicTotalCon);
		if (empty($fav_music_data)) {
			$musicList['Favourite']['favCount'] = "0";
			$musicList['Favourite']['favList'] = array();
		} else {
			$musicList['Favourite']['favCount'] = count($favMusic_Total_data);
			foreach ($fav_music_data as $row_favmusic_data) {
				$favMusicCategoryName  = $this->getCategoryName($row_favmusic_data['category_id']);
				$musicList['Favourite']['favList'][] = array(
					'id' => $row_favmusic_data['music_fav_id'],
					'title' => $row_favmusic_data['music_title'],
					'desc' => $row_favmusic_data['music_desc'],
					'artist' => $row_favmusic_data['music_artist'],
					'music_image' => base_url() . 'uploads/music/image/' . $row_favmusic_data['music_image'],
					'music_file' => base_url() . 'uploads/music/' . $row_favmusic_data['music_file'],
					'music_id' => $row_favmusic_data['music_id'],
					'category_name' => $favMusicCategoryName,
					'fav' => 1,
					'user_id' => $data['user_id'],
				);
			}
		}
		if (!empty($banner_data) or !empty($popular_music_data) or !empty($recent_music_data) or !empty($category_data) or !empty($my_music_data) or !empty($fav_music_data)) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('musicList' => $musicList));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Music Data not found', 'response' => array('message' => 'Music Data not found'));
		}
		$this->response($resp);
	}
	// add popular data
	public function add_popular_music_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('music_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$this->db->select('music_view');
		$this->db->from('music');
		$this->db->where("music_id", $data['music_id']);
		$query = $this->db->get();
		$result = $query->row_array();
		$view = $result['music_view'];
		$count_where  = array('music_id' => $data['music_id']);
		$count_array  = array('music_view' => $view + 1);
		$musicId = $this->common_model->updateRecords('music', $count_array, $count_where);
		if ($musicId) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Added successfully', 'response' => array('message' => 'Added successfully'));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	// get category name
	public function getCategoryName($music_category_id)
	{
		$this->db->select('music_category_name');
		$this->db->from('cp_music_category');
		$this->db->where("music_category_id", $music_category_id);
		$query = $this->db->get();
		$result = $query->row_array();
		$categoryName = $result['music_category_name'];
		if ($categoryName) {
			return $categoryName;
		} else {
			return "NA";
		}
	}
	public function add_popular_movie_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('movie_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$this->db->select('movie_view');
		$this->db->from('movie');
		$this->db->where("movie_id", $data['movie_id']);
		$query = $this->db->get();
		$result = $query->row_array();
		$view = $result['movie_view'];
		$count_where  = array('movie_id' => $data['movie_id']);
		$count_array  = array('movie_view' => $view + 1);
		$movieId = $this->common_model->updateRecords('movie', $count_array, $count_where);
		if ($movieId) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Added successfully', 'response' => array('message' => 'Added successfully'));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	// movie listing api
	public function movie_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$limit = 10;
		// get banner data
		$bannerCon['sorting'] = array("movie_banner_id" => "DESC");
		$bannerCon['conditions'] = array("movie_banner_status" => 1);
		$bannerCon['limit'] = $limit;
		$banner_data  = $this->Api_model->getRows('cp_movie_banner', $bannerCon);
		$bannerTotalCon['conditions'] = array("movie_banner_status" => 1);
		$banner_Total_data  = $this->Api_model->getRows('cp_movie_banner', $bannerTotalCon);
		if (empty($banner_data)) {
			$movieList['Banners'] = array();
		} else {
			$movieList['Banners']['bannerCount'] = count($banner_Total_data);
			foreach ($banner_data as $row_banner_data) {
				$movieList['Banners']['bannerList'][] = array(
					'id' => $row_banner_data['movie_banner_id'],
					'movie_banner_path' => base_url() . 'uploads/movie/banners/' . $row_banner_data['movie_banner_image'],
					'movie_banner_file' => base_url() . 'uploads/movie/banners/files/' . $row_banner_data['movie_banner_file']
				);
			}
		}
		// get popular music data
		$popularCon['sorting'] = array("movie_view" => "DESC");
		$popularCon['limit'] = $limit;
		$popular_movie_data  = $this->Api_model->getRows('movie', $popularCon);
		$popular_Total_data  = $this->Api_model->getRows('movie');
		if (empty($popular_movie_data)) {
			$movieList['Populars']['popularCount'] = 0;
			$movieList['Populars']['popularList'] = array();
		} else {
			$movieList['Populars']['popularCount'] = count($popular_Total_data);
			foreach ($popular_movie_data as $row_popular_data) {
				$popularFav  = $this->getMovieUserFav($row_popular_data['movie_id'], $data['user_id']);
				$popularCategoryName  = $this->getMovieCategoryName($row_popular_data['category_id']);
				$mypopu = $this->common_model->getsingle("movie", array("user_id" => $row_popular_data['user_id']));
				if ($mypopu->user_id == $data['user_id']) {
					$isedit_popular = "1";
				} else {
					$isedit_popular = "";
				}
				$movieList['Populars']['popularList'][] = array(
					'id' => $row_popular_data['movie_id'],
					'title' => $row_popular_data['movie_title'],
					'desc' => $row_popular_data['movie_desc'],
					'artist' => $row_popular_data['movie_artist'],
					'movie_image' => base_url() . 'uploads/movie/image/' . $row_popular_data['movie_image'],
					'movie_file' => base_url() . 'uploads/movie/' . $row_popular_data['movie_file'],
					'fav' => $popularFav,
					'category_name' => $popularCategoryName,
					'isEditable' => $isedit_popular
				);
			}
		}
		// get recent music data
		$recentCon['sorting'] = array("movie_id" => "DESC");
		$recentCon['limit'] = $limit;
		$recent_movie_data  = $this->Api_model->getRows('movie', $recentCon);
		$recent_Total_data  = $this->Api_model->getRows('movie');
		if (empty($recent_movie_data)) {
			$movieList['Recents']['recentCount'] = 0;
			$movieList['Recents']['recentList'] = array();
		} else {
			$movieList['Recents']['recentCount'] = count($recent_Total_data);
			foreach ($recent_movie_data as $row_recent_data) {
				$recentFav  = $this->getMovieFav($row_recent_data['movie_id']);
				$recentCategoryName  = $this->getMovieCategoryName($row_recent_data['category_id']);
				$myrece = $this->common_model->getsingle("movie", array("user_id" => $row_recent_data['user_id']));
				if ($myrece->user_id == $data['user_id']) {
					$isedit_recent = "1";
				} else {
					$isedit_recent = "";
				}
				$movieList['Recents']['recentList'][] = array(
					'id' => $row_recent_data['movie_id'],
					'title' => $row_recent_data['movie_title'],
					'desc' => $row_recent_data['movie_desc'],
					'artist' => $row_recent_data['movie_artist'],
					'movie_image' => base_url() . 'uploads/movie/image/' . $row_recent_data['movie_image'],
					'movie_file' => base_url() . 'uploads/movie/' . $row_recent_data['movie_file'],
					'fav' => $recentFav,
					'category_name' => $recentCategoryName,
					'isEditable' => $isedit_recent
				);
			}
		}
		// get category data
		$categoryCon['sorting'] = array("movie_category_id" => "DESC");
		$categoryCon['limit'] = $limit;
		$category_data  = $this->Api_model->getRows('cp_movie_category', $categoryCon);
		$categoryTotalCon['conditions'] = array("movie_category_id" => 1);
		$category_Total_data  = $this->Api_model->getRows('cp_movie_category', $categoryTotalCon);
		if (empty($category_data)) {
			$movieList['Categories'] = array();
		} else {
			$movieList['Categories']['categoryCount'] = count($category_Total_data);
			foreach ($category_data as $row_category_data) {
				$movieList['Categories']['categoryList'][] = array(
					'id' => $row_category_data['movie_category_id'],
					'name' => $row_category_data['movie_category_name'],
					'category_icon' => base_url() . 'uploads/movie/category_icon/' . $row_category_data['movie_category_icon'],
				);
			}
		}
		// get my movie data
		$myMovieCon['sorting'] = array("movie_id" => "DESC");
		$myMovieCon['conditions'] = array("user_id" => $data['user_id']);
		$myMovieCon['limit'] = $limit;
		$my_movie_data  = $this->Api_model->getRows('movie', $myMovieCon);
		$myMovieTotalCon['conditions'] = array("user_id" => $data['user_id']);
		$myMovie_Total_data  = $this->Api_model->getRows('movie', $myMovieTotalCon);
		if (empty($my_movie_data)) {
			$movieList['myMovie']['myMovieCount'] = "0";
			$movieList['myMovie']['myMovieList'] = array();
		} else {
			$movieList['myMovie']['myMovieCount'] = count($myMovie_Total_data);
			foreach ($my_movie_data as $row_mymovie_data) {
				$myMovieFav  = $this->getMovieFav($row_mymovie_data['movie_id']);
				$myMovieCategoryName  = $this->getMovieCategoryName($row_mymovie_data['category_id']);
				$movieList['myMovie']['myMovieList'][] = array(
					'id' => $row_mymovie_data['movie_id'],
					'title' => $row_mymovie_data['movie_title'],
					'desc' => $row_mymovie_data['movie_desc'],
					'artist' => $row_mymovie_data['movie_artist'],
					'movie_image' => base_url() . 'uploads/movie/image/' . $row_mymovie_data['movie_image'],
					'movie_file' => base_url() . 'uploads/movie/' . $row_mymovie_data['movie_file'],
					'fav' => $myMovieFav,
					'category_name' => $myMovieCategoryName,
					'isEditable' => 1
				);
			}
		}
		$favMovieCon['sorting'] = array("movie_fav_id" => "DESC");
		$favMovieCon['limit'] = $limit;
		$favMovieCon['conditions'] = array("cp_movie_favorite.user_id" => $data['user_id']);
		$fav_movie_data  = $this->Api_model->getRowsFavouriteMovie('cp_movie_favorite', $favMovieCon);
		$favMovieTotalCon['conditions'] = array("cp_movie_favorite.user_id" => $data['user_id']);
		$favMovie_Total_data  = $this->Api_model->getRowsFavouriteMovie('cp_movie_favorite', $favMovieTotalCon);
		if (empty($fav_movie_data)) {
			$movieList['Favourite']['favCount'] = "0";
			$movieList['Favourite']['favList'] = array();
		} else {
			$movieList['Favourite']['favCount'] = count($favMovie_Total_data);
			foreach ($fav_movie_data as $row_favmovie_data) {
				$favMovieCategoryName  = $this->getCategoryName($row_favmovie_data['category_id']);
				$myfavmo = $this->common_model->getsingle("movie", array("user_id" => $row_favmovie_data['user_id']));
				if ($myfavmo->user_id == $data['user_id']) {
					$isedit_fav = "1";
				} else {
					$isedit_fav = "";
				}
				$movieList['Favourite']['favList'][] = array(
					'id' => $row_favmovie_data['movie_fav_id'],
					'title' => $row_favmovie_data['movie_title'],
					'desc' => $row_favmovie_data['movie_desc'],
					'artist' => $row_favmovie_data['movie_artist'],
					'movie_image' => base_url() . 'uploads/movie/image/' . $row_favmovie_data['movie_image'],
					'movie_file' => base_url() . 'uploads/movie/' . $row_favmovie_data['movie_file'],
					'movie_id' => $row_favmovie_data['movie_id'],
					'category_name' => $favMovieCategoryName,
					'fav' => 1,
					"isEditable" => $isedit_fav
				);
			}
		}
		if (!empty($banner_data) or !empty($popular_movie_data) or !empty($recent_movie_data) or !empty($category_data) or !empty($my_movie_data) or !empty($fav_movie_data)) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('movieList' => $movieList));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Moview Data not found', 'response' => array('message' => 'Movie Data not found'));
		}
		$this->response($resp);
	}
	// get movie category name
	public function getMovieCategoryName($movie_category_id)
	{
		$this->db->select('movie_category_name');
		$this->db->from('cp_movie_category');
		$this->db->where("movie_category_id", $movie_category_id);
		$query = $this->db->get();
		$result = $query->row_array();
		$categoryName = $result['movie_category_name'];
		if ($categoryName) {
			return $categoryName;
		} else {
			return "NA";
		}
	}
	public function changepassword1_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('email', 'password', 'confirm_password');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		// security changes start
		$password = trim($data['password']);
		$regex_lowercase = '/[a-z]/';
		$regex_uppercase = '/[A-Z]/';
		$regex_number = '/[0-9]/';
		$regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';
		// if (preg_match_all($regex_lowercase, $password) < 1)
		// {
		// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => message(43), 'response' => array('message' => message(43)));
		// 	$this->response($resp);
		// }
		if (preg_match_all($regex_lowercase, $password) < 1) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must contains lowercase letter.', 'response' => array('message' => 'Your password must contains lowercase letter.'));
			$this->response($resp);
		}
		if (preg_match_all($regex_uppercase, $password) < 1) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must contains uppercase letter.', 'response' => array('message' => 'Your password must contains uppercase letter.'));
			$this->response($resp);
		}
		if (preg_match_all($regex_number, $password) < 1) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must contains number.', 'response' => array('message' => 'Your password must contains number.'));
			$this->response($resp);
		}
		if (preg_match_all($regex_special, $password) < 1) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must contains special character.', 'response' => array('message' => 'Your password must contains special character.'));
			$this->response($resp);
		}
		if (strlen($password) < 6) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must be at least 6 characters long.', 'response' => array('message' => 'Your password must be at least 6 characters long.'));
			$this->response($resp);
		}
		if (strlen($password) > 30) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must not exceed 30 characters long.', 'response' => array('message' => 'Your password must not exceed 30 characters long.'));
			$this->response($resp);
		}
		// security changes end
		if ($data['password'] != $data['confirm_password']) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => "Input must be the same as the password", 'response' => array('message' => "Input must be the same as the password"));
			$this->response($resp);
		}
		$email = $data['email'];
		/* Check for user id */
		$check_key = $this->common_model->getRecordCount('cp_users', array('email' => $email));
		if ($check_key == 1) {
			/* Change password */
			$condition = array('email' => $email);
			$updateArr = array('password' => md5($data['password']));
			$this->common_model->updateRecords('cp_users', $updateArr, $condition);
			/* Response array */
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => "Your password reset successfully", 'response' => array('message' => "Your password reset successfully"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => "Invalid details, please try again", 'response' => array('message' => "Invalid details, please try again"));
		}
		$this->response($resp);
	}
	public function changepassword_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id', 'old_password', 'password');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$user_pass = $this->common_model->getSingleRecordById('cp_users', array('id' => $data['user_id']));
		// security changes start
		$password = trim($data['password']);
		$regex_lowercase = '/[a-z]/';
		$regex_uppercase = '/[A-Z]/';
		$regex_number = '/[0-9]/';
		$regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';
		// if (preg_match_all($regex_lowercase, $password) < 1)
		// {
		// 	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => message(43), 'response' => array('message' => message(43)));
		// 	$this->response($resp);
		// }
		if (preg_match_all($regex_lowercase, $password) < 1) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must contains lowercase letter.', 'response' => array('message' => 'Your password must contains lowercase letter.'));
			$this->response($resp);
		}
		if (preg_match_all($regex_uppercase, $password) < 1) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must contains uppercase letter.', 'response' => array('message' => 'Your password must contains uppercase letter.'));
			$this->response($resp);
		}
		if (preg_match_all($regex_number, $password) < 1) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must contains number.', 'response' => array('message' => 'Your password must contains number.'));
			$this->response($resp);
		}
		if (preg_match_all($regex_special, $password) < 1) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must contains special character.', 'response' => array('message' => 'Your password must contains special character.'));
			$this->response($resp);
		}
		if (strlen($password) < 6) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must be at least 6 characters long.', 'response' => array('message' => 'Your password must be at least 6 characters long.'));
			$this->response($resp);
		}
		if (strlen($password) > 30) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your password must not exceed 30 characters long.', 'response' => array('message' => 'Your password must not exceed 30 characters long.'));
			$this->response($resp);
		}
		// security changes end
		//print_r($user_pass); die;
		if (md5($data['old_password']) != $user_pass['password']) {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => "Please old password must be correct", 'response' => array('message' => "Please old password must be correct"));
			$this->response($resp);
		}
		/* if($data['password'] != $data['confirm_password']) {
	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => message(13), 'response' => array('message' => message(13)));
	$this->response($resp);
}*/
		$userId = $data['user_id'];
		/* Check for user id */
		$check_key = $this->common_model->getRecordCount('cp_users', array('id' => $userId));
		if ($check_key == 1) {
			/* Change password */
			$condition = array('id' => $userId);
			$updateArr = array('password' => md5($data['password']));
			$this->common_model->updateRecords('cp_users', $updateArr, $condition);
			/* Response array */
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => "Your password change successfully", 'response' => array('message' => "Your password change successfully"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => "Invalid details, please try again", 'response' => array('message' => "Invalid details, please try again"));
		}
		$this->response($resp);
	}
	public function getUser_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array("user_id");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$con['sorting'] = array("id" => "DESC");
		$userData = $this->common_model->getSingleRecordById('cp_users', array('id' => $data['user_id']));
		if (!empty($userData)) {
			$userData1 = array(
				'id' => $userData['id'],
				'name' => $userData['name'],
				'username' => $userData['username'],
				'email' => $userData['email'],
				'phone' => $userData['mobile'],
				'profile_image' => base_url() . 'uploads/profile_images/' . $userData['profile_image']
			);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('user_data' => $userData1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Doctor not found', 'response' => array('message' => 'Acknowledge Data not found'));
		}
		$this->response($resp);
	}
	/*list mail */
	public function inboxlist_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$limit = 10;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$emailId = $this->common_model->getSingleRecordById('cp_users', array('id' => $data['user_id']));
		$emailId = $emailId['email'];
		$emailCount = $this->common_model->getEmailCount1($emailId, $data['user_id']);
		$start  = ($data['page_no'] - 1) * $limit;
		$pages = ceil(count($emailCount) / $limit);
		$emailData = $this->common_model->getEmails1($start, $limit, $emailId, $data['user_id']);
		//print_r("<pre/>");
		//print_r($emailCount);
		//  die;
		if (!empty($emailData)) {
			foreach ($emailData as $row) {
				if ($emailData['create_email'] == "0000-00-00") {
					$emails_create = $row['create_email'];
				} else {
					$emails_create = date("M d, Y H:i:s", strtotime($row['create_email']));
				}
				$attachment = $this->orderImage($row['email_id']);
				$toEmail = $this->toEmail($row['email_id']);
				$getMail = $this->getMail($row['user_id']);
				$email[] = array(
					'id' => $row['email_id'],
					'user_id' => $row['user_id'],
					'to_email' => $toEmail,
					'subject' => $row['subject'],
					'message' => $row['message'],
					'create_date' => $emails_create,
					"attachment" => $attachment,
					"username" => $row['username'],
					'profile_image' => base_url() . 'uploads/profile_images/' . $row['profile_image'],
					'send_email' => $getMail,
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'total' => count($emailCount), 'per_page_records' => $limit, 'response' => array('email_data' => $email));
		} else {
			$email = array();
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Email not found', 'response' => array('message' => 'Email not found', 'email_data' => $email));
		}
		$this->response($resp);
	}
	public function emaillist_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$limit = 10;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$emailCount = $this->common_model->getEmailCount($data['user_id']);
		$start  = ($data['page_no'] - 1) * $limit;
		$pages = ceil(count($emailCount) / $limit);
		$emailData = $this->common_model->getEmails($start, $limit, $data['user_id']);
		if (!empty($emailData)) {
			foreach ($emailData as $row) {
				if ($row['create_email'] == "0000-00-00") {
					$emails_create = $row['create_email'];
				} else {
					$emails_create = date("M d, Y H:i:s", strtotime($row['create_email']));
				}
				$attachment = $this->orderImage($row['email_id']);
				$toEmail = $this->toEmail($row['email_id']);
				$getMail = $this->getMail($row['user_id']);
				$email[] = array(
					'id' => $row['email_id'],
					'user_id' => $row['user_id'],
					'to_email' => $toEmail,
					'subject' => $row['subject'],
					'message' => $row['message'],
					'create_date' => $emails_create,
					'attachment' => $attachment,
					"username" => $row['username'],
					'profile_image' => base_url() . 'uploads/profile_images/' . $row['profile_image'],
					'send_email' => $getMail,
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'total' => count($emailCount), 'per_page_records' => $limit, 'response' => array('email_data' => $email));
		} else {
			$email = array();
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Email not found', 'response' => array('message' => 'Email not found', 'email_data' => $email));
		}
		$this->response($resp);
	}
	/*-------User list--------*/
	public function user_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$con['sorting'] = array("id" => "DESC");
		$userData  = $this->common_model->getAllRecordsById('cp_users', array('status' => 1));
		if (!empty($userData)) {
			foreach ($userData as $userData_details) {
				$userData_details1[] = array(
					'user_id' => $userData_details['id'],
					'name' => $userData_details['name'],
					'email' => $userData_details['email'],
					'profile_image' => base_url() . 'uploads/profile_images/' . $userData_details['profile_image'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('user_data' => $userData_details1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'User not found', 'response' => array('message' => 'User Data not found'));
		}
		$this->response($resp);
	}
	public function toEmail($email_id)
	{
		$this->db->select('to_email');
		$this->db->from('cp_to_email');
		$this->db->where('email_id', $email_id);
		$query = $this->db->get();
		$result = $query->result_array();
		foreach ($result as $result_details) {
			$imageArray[] = $result_details['to_email'];
		}
		$result_new = implode(",", $imageArray);
		return $result_new;
	}
	public function getMail($user_id)
	{
		$this->db->select('email');
		$this->db->from('cp_users');
		$this->db->where('id', $user_id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result[0]['email'];
	}
	/*public function emaildelete_post(){
	$pdata = file_get_contents("php://input");
	$data = json_decode($pdata, true);
	$object_info = $data;
	$required_parameter = array('email_id','user_id');
	$chk_error = check_required_value($required_parameter, $object_info);
	if ($chk_error) {
	$resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));
	$this->response($resp);
	}
	$ids = explode(',',$data['email_id']);
	foreach($ids as $ids_details){
	$post_data = array('email_id'=>$ids_details,'user_id'=>$data['user_id']);
	$emailid = $this->common_model->addRecords('cp_email_trash',$post_data);
	}
	if($emailid) {
	$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS', 'response' => array('email_data' => "email data deleted succesfully"));
	}
	else {
	$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Email not found','response' => array('message' => 'Some error occured, please try again'));
	}
	$this->response($resp);
}*/
	public function emaildelete_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('email_id', 'user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$ids = explode(',', $data['email_id']);
		foreach ($ids as $ids_details) {
			$post_data = array('email_id' => $ids_details, 'user_id' => $data['user_id'], 'check_email' => $data['delete_mail'], 'trash_created' => date('Y-m-d H:i:s'));
			$emailid = $this->common_model->addRecords('cp_email_trash', $post_data);
		}
		if ($emailid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('email_data' => "Message successfully deleted"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Email not found', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/*-------User list--------*/
	public function trash_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$limit = 10;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$trashCount = $this->common_model->getCountTrash($data['user_id']);
		$start  = ($data['page_no'] - 1) * $limit;
		$pages = ceil(count($trashCount) / $limit);
		$userData  = $this->common_model->getTrash($start, $limit, $data['user_id']);
		if (!empty($userData)) {
			foreach ($userData as $userData_details) {
				$toEmail = $this->toEmail($userData_details['email_id']);
				if ($userData_details['trash_created'] == "0000-00-00 00:00:00") {
					$emails_create = $userData_details['trash_created'];
				} else {
					$emails_create = date("M d, Y H:i:s", strtotime($userData_details['trash_created']));
				}
				$userData_details1[] = array(
					'id' => $userData_details['trash_email_id'],
					'username' => $userData_details['username'],
					'user_id' => $userData_details['user_id'],
					'to_email' => $toEmail,
					'subject' => $userData_details['subject'],
					'message' => $userData_details['message'],
					'create_date' => $emails_create,
					'profile_image' => base_url() . 'uploads/profile_images/' . $userData_details['profile_image'],
					'mail_status' => $userData_details['check_email']
				);
			}
			//$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('email_data' => $userData_details1));
			//$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('email_data' => $userData_details1));
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'total' => count($trashCount), 'per_page_records' => $limit, 'response' => array('email_data' => $userData_details1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Data not found', 'response' => array('message' => 'Data not found'));
		}
		$this->response($resp);
	}
	/*-------User list--------*/
	public function musicCateogry_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$musicCategoryData  = $this->common_model->getAllRecordsById('cp_music_category', array('music_category_status' => 1));
		if (!empty($musicCategoryData)) {
			foreach ($musicCategoryData as $musicCategoryData_details) {
				$musicCategoryData_details1[] = array(
					'id' => $musicCategoryData_details['music_category_id'],
					'name' => $musicCategoryData_details['music_category_name'],
					'category_icon' => base_url() . 'uploads/music/category_icon/' . $musicCategoryData_details['music_category_icon'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('category_data' => $musicCategoryData_details1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'User not found', 'response' => array('message' => 'User Data not found'));
		}
		$this->response($resp);
	}
	public function addUserMusic_post()
	{
		$data = $_POST;
		$object_info = $data;
		$required_parameter = array('category_id', 'user_id', 'music_title', 'music_desc', 'music_artist');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		if (!empty($_FILES['music_image']['name'])) {
			$imgfile = $_FILES['music_image']['name'];
			$extension = substr($imgfile, strlen($imgfile) - 4, strlen($imgfile));
			// echo $imgfile;
			// die;
			// allowed extensions
			$allowed_extensions = array(".jpg", ".jpeg", ".png", ".gif");
			//$allowed_extensions = array(".jpg",".jpeg");
			// security chnages start
			if (in_array($extension, $allowed_extensions)) // security check start
			{
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['music_image']['name'], PATHINFO_EXTENSION);
				$music_file_name = $random . "." . $ext;
				$target_dir = "uploads/music/image/";
				$target_file = $target_dir . $music_file_name;
				if (move_uploaded_file($_FILES["music_image"]["tmp_name"], $target_file)) {
					$musicImage = $music_file_name;
				}
			} else {  // security check end
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your image file is not valide. allow file types jpg, jpeg, png', 'response' => array('message' => 'Your image file is not valide. allow file types jpg, jpeg, png'));
				$this->response($resp);
			}
		}
		if (!empty($_FILES['music_file']['name'])) {
			$random = $this->generateRandomString(10);
			$ext = pathinfo($_FILES['music_file']['name'], PATHINFO_EXTENSION);
			$file_name = $random . "." . $ext;
			$target_dir = "uploads/music/";
			$target_file = $target_dir . $file_name;
			if (move_uploaded_file($_FILES["music_file"]["tmp_name"], $target_file)) {
				$musicFile = $file_name;
			}
		}
		$post_data = array(
			'category_id' => $data['category_id'],
			'user_id' => $data['user_id'],
			'music_title' => $data['music_title'],
			'music_desc' => $data['music_desc'],
			'music_artist' => $data['music_artist'],
			'music_image'        => $musicImage,
			'music_file'        => $musicFile,
			'music_created' => date('Y-m-d h:i:s'),
		);
		$musicUserId = $this->common_model->addRecords('music', $post_data);
		if ($musicUserId) {
			$musicData = $this->common_model->getSingleRecordById('music', array('music_id' => $musicUserId));
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'music added successfully', 'response' => array('music_data' => "music added successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function updateUserMusic_post()
	{
		// $data = $_POST;
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('category_id', 'music_title', 'music_desc', 'music_artist', 'music_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		if (!empty($_FILES['music_image']['name'])) {
			$imgfile = $_FILES['music_image']['name'];
			$extension = substr($imgfile, strlen($imgfile) - 4, strlen($imgfile));
			$allowed_extensions = array(".jpg", ".jpeg", ".png", ".gif");
			//$allowed_extensions = array(".jpeg",".png",".gif");
			if (in_array($extension, $allowed_extensions)) // security check start
			{
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['music_image']['name'], PATHINFO_EXTENSION);
				$music_file_name = $random . "." . $ext;
				$target_dir = "uploads/music/image/";
				$target_file = $target_dir . $music_file_name;
				if (move_uploaded_file($_FILES["music_image"]["tmp_name"], $target_file)) {
					$musicImage = $music_file_name;
					$post_data['music_image'] = $musicImage;
				}
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your image file is not valide. allow file types jpg, jpeg, png', 'response' => array('message' => 'Your image file is not valide. allow file types jpg, jpeg, png, gif'));
				$this->response($resp);
			}
			// security check end
		}
		if (!empty($_FILES['music_file']['name'])) {
			$random = $this->generateRandomString(10);
			$ext = pathinfo($_FILES['music_file']['name'], PATHINFO_EXTENSION);
			$file_name = $random . "." . $ext;
			$target_dir = "uploads/music/";
			$target_file = $target_dir . $file_name;
			if (move_uploaded_file($_FILES["music_file"]["tmp_name"], $target_file)) {
				$musicFile = $file_name;
				$post_data['music_file'] = $musicFile;
			}
		}
		$post_data['category_id'] = $data['category_id'];
		$post_data['user_id'] = $data['user_id'];
		$post_data['music_title'] = $data['music_title'];
		$post_data['music_desc'] = $data['music_desc'];
		$post_data['music_artist'] = $data['music_artist'];
		$musicUserId = $this->common_model->updateRecords('music', $post_data, array('music_id' => $data['music_id']));
		$data_music = $this->common_model->getsingle("music", array('music_id' => $data['music_id']));
		$Fav  = $this->getMusicUserFav($data_music->music_id, $data_music->user_id);
		$CategoryName  = $this->getCategoryName($data_music->category_id);
		$music_data_array = array(
			"title" => $data_music->music_title,
			"desc" => $data_music->music_desc,
			"artist" => $data_music->music_artist,
			"music_image" => base_url() . 'uploads/music/image/' . $data_music->music_image,
			"music_file" => base_url() . 'uploads/music/' . $data_music->music_file,
			"fav" => $Fav,
			"category_name" => $CategoryName,
			"user_id" => $data_music->user_id,
			"music_id" => $data_music->music_id,
		);
		if ($musicUserId) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'music update successfully', 'response' => array('music_data' => $music_data_array));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function  get_musicCategory_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('category_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$pdata = file_get_contents("php://input");
		$data = json_decode($pdata, true);
		$object_info = $data;
		$musicCategoryData  = $this->common_model->getAllRecordsById('music', array('category_id' => $data['category_id']));
		if (!empty($musicCategoryData)) {
			foreach ($musicCategoryData as $musicCategoryData_details) {
				$Fav  = $this->getMusicFav($musicCategoryData_details['music_id']);
				$CategoryName  = $this->getCategoryName($musicCategoryData_details['category_id']);
				$musicCategoryData_details1[] = array(
					'music_id' => $musicCategoryData_details['music_id'],
					'title' => $musicCategoryData_details['music_title'],
					'desc' => $musicCategoryData_details['music_desc'],
					'artist' => $musicCategoryData_details['music_artist'],
					'music_image' => base_url() . 'uploads/music/image/' . $musicCategoryData_details['music_image'],
					'music_file' => base_url() . 'uploads/music/' . $musicCategoryData_details['music_file'],
					'fav' => $Fav,
					'category_name' => $CategoryName
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('music_data' => $musicCategoryData_details1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Music not found', 'response' => array('message' => 'Music Data not found'));
		}
		$this->response($resp);
	}
	public function  my_music_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$pdata = file_get_contents("php://input");
		$data = json_decode($pdata, true);
		$object_info = $data;
		$musicCategoryData  = $this->common_model->getAllRecordsById('music', array('user_id' => $data['user_id']));
		if (!empty($musicCategoryData)) {
			foreach ($musicCategoryData as $musicCategoryData_details) {
				$Fav  = $this->getMusicFav($musicCategoryData_details['music_id']);
				$CategoryName  = $this->getCategoryName($musicCategoryData_details['category_id']);
				$musicCategoryData_details1[] = array(
					'music_id' => $musicCategoryData_details['music_id'],
					'title' => $musicCategoryData_details['music_title'],
					'desc' => $musicCategoryData_details['music_desc'],
					'artist' => $musicCategoryData_details['music_artist'],
					'music_image' => base_url() . 'uploads/music/image/' . $musicCategoryData_details['music_image'],
					'music_file' => base_url() . 'uploads/music/' . $musicCategoryData_details['music_file'],
					'fav' => $Fav,
					'category_name' => $CategoryName,
					'user_id' => $musicCategoryData_details['user_id']
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('music_data' => $musicCategoryData_details1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'User not found', 'response' => array('message' => 'User Data not found'));
		}
		$this->response($resp);
	}
	public function movieCateogry_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		//$musicCategoryData  = $this->common_model->getAllRecordsById('cp_movie_category',array('movie_category_status'=>1));
		$musicCategoryData  = $this->common_model->getAllwhereorderby('cp_movie_category', array('movie_category_status' => 1), 'movie_category_name', 'ASC');
		//print_r("<pre/>");
		//print_r($musicCategoryData);
		// die;
		if (!empty($musicCategoryData)) {
			foreach ($musicCategoryData as $musicCategoryData_details) {
				$musicCategoryData_details1[] = array(
					'id' => $musicCategoryData_details->movie_category_id,
					'name' => $musicCategoryData_details->movie_category_name,
					'category_icon' => base_url() . 'uploads/movie/category_icon/' . $musicCategoryData_details->movie_category_icon,
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('category_data' => $musicCategoryData_details1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'User not found', 'response' => array('message' => 'User Data not found'));
		}
		$this->response($resp);
	}
	public function  all_music_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('type');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$pdata = file_get_contents("php://input");
		$data = json_decode($pdata, true);
		$object_info = $data;
		$musicCategoryData  = $this->Api_model->view_all_music($data['type']);
		if (!empty($musicCategoryData)) {
			foreach ($musicCategoryData as $musicCategoryData_details) {
				$Fav  = $this->getMusicFav($musicCategoryData_details['music_id']);
				$CategoryName  = $this->getCategoryName($musicCategoryData_details['category_id']);
				$musicCategoryData_details1[] = array(
					'music_id' => $musicCategoryData_details['music_id'],
					'title' => $musicCategoryData_details['music_title'],
					'desc' => $musicCategoryData_details['music_desc'],
					'artist' => $musicCategoryData_details['music_artist'],
					'music_image' => base_url() . 'uploads/music/image/' . $musicCategoryData_details['music_image'],
					'music_file' => base_url() . 'uploads/music/' . $musicCategoryData_details['music_file'],
					'fav' => $Fav,
					'category_name' => $CategoryName,
					'user_id' => $musicCategoryData_details['user_id'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('music_data' => $musicCategoryData_details1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'User not found', 'response' => array('message' => 'User Data not found'));
		}
		$this->response($resp);
	}
	// add user movie
	public function addUserMovie_post()
	{
		$data = $_POST;
		$object_info = $data;
		$required_parameter = array('category_id', 'user_id', 'movie_title', 'movie_desc', 'movie_artist');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		if (!empty($_FILES['movie_image']['name'])) {
			$imgfile = $_FILES['movie_image']['name'];
			$extension = substr($imgfile, strlen($imgfile) - 4, strlen($imgfile));
			$allowed_extensions = array(".jpg", ".jpeg", ".png", ".gif");
			//$allowed_extensions = array(".jpg",".jpeg");
			// security chnages start
			if (in_array($extension, $allowed_extensions)) // security check start
			{
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['movie_image']['name'], PATHINFO_EXTENSION);
				$movie_file_name = $random . "." . $ext;
				$target_dir = "uploads/movie/image/";
				$target_file = $target_dir . $movie_file_name;
				if (move_uploaded_file($_FILES["movie_image"]["tmp_name"], $target_file)) {
					$movieImage = $movie_file_name;
				}
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your image file is not valide. allow file types jpg, jpeg, png, gif', 'response' => array('message' => 'Your image file is not valide. allow file types jpg, jpeg, png, gif'));
				$this->response($resp);
			}
			// security check end
		}
		if (!empty($_FILES['movie_file']['name'])) {
			$random = $this->generateRandomString(10);
			$ext = pathinfo($_FILES['movie_file']['name'], PATHINFO_EXTENSION);
			$file_name = $random . "." . $ext;
			$target_dir = "uploads/movie/";
			$target_file = $target_dir . $file_name;
			if (move_uploaded_file($_FILES["movie_file"]["tmp_name"], $target_file)) {
				$movieFile = $file_name;
			}
		}
		$post_data = array(
			'category_id' => $data['category_id'],
			'user_id' => $data['user_id'],
			'movie_title' => $data['movie_title'],
			'movie_desc' => $data['movie_desc'],
			'movie_artist' => $data['movie_artist'],
			'movie_image' => $movieImage,
			'movie_file' => $movieFile,
			'movie_created' => date('Y-m-d h:i:s'),
		);
		// echo "<pre>";
		// print_r($post_data);
		// echo "</pre>";
		// die;
		$movieUserId = $this->common_model->addRecords('movie', $post_data);
		if ($movieUserId) {
			$movieData = $this->common_model->getSingleRecordById('movie', array('movie_id' => $movieUserId));
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'movie added successfully', 'response' => array('music_data' => "movie added successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	// update user movie
	public function updateUserMovie_post()
	{
		//$data = $_POST;
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('category_id', 'movie_title', 'movie_desc', 'movie_artist', 'movie_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		if (!empty($_FILES['movie_image']['name'])) {
			$imgfile = $_FILES['movie_image']['name'];
			$extension = substr($imgfile, strlen($imgfile) - 4, strlen($imgfile));
			$allowed_extensions = array(".jpg", ".jpeg", ".png", ".gif");
			if (in_array($extension, $allowed_extensions)) // security check start
			{
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['movie_image']['name'], PATHINFO_EXTENSION);
				$movie_file_name = $random . "." . $ext;
				$target_dir = "uploads/movie/image/";
				$target_file = $target_dir . $movie_file_name;
				if (move_uploaded_file($_FILES["movie_image"]["tmp_name"], $target_file)) {
					$movieImage = $movie_file_name;
					$post_data['movie_image'] = $movieImage;
				}
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your image file is not valide. allow file types jpg, jpeg, png, gif', 'response' => array('message' => 'Your image file is not valide. allow file types jpg, jpeg, png, gif'));
				$this->response($resp);
			}
			// security check end
		}
		if (!empty($_FILES['movie_file']['name'])) {
			$random = $this->generateRandomString(10);
			$ext = pathinfo($_FILES['movie_file']['name'], PATHINFO_EXTENSION);
			$file_name = $random . "." . $ext;
			$target_dir = "uploads/movie/";
			$target_file = $target_dir . $file_name;
			if (move_uploaded_file($_FILES["movie_file"]["tmp_name"], $target_file)) {
				$movieFile = $file_name;
				$post_data['movie_file'] = $movieFile;
			}
		}
		$post_data['category_id'] = $data['category_id'];
		$post_data['user_id'] = $data['user_id'];
		$post_data['movie_title'] = $data['movie_title'];
		$post_data['movie_desc'] = $data['movie_desc'];
		$post_data['movie_artist'] = $data['movie_artist'];
		$movieUserId = $this->common_model->updateRecords('movie', $post_data, array('movie_id' => $data['movie_id']));
		if ($movieUserId) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'movie update successfully', 'response' => array('movie_data' => "movie update successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function  get_movieCategory_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('category_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$pdata = file_get_contents("php://input");
		$data = json_decode($pdata, true);
		$object_info = $data;
		$musicCategoryData  = $this->common_model->getAllRecordsById('movie', array('category_id' => $data['category_id']));
		if (!empty($musicCategoryData)) {
			foreach ($musicCategoryData as $musicCategoryData_details) {
				$Fav  = $this->getMovieFav($musicCategoryData_details['movie_id']);
				$CategoryName  = $this->getMovieCategoryName($musicCategoryData_details['category_id']);
				$musicCategoryData_details1[] = array(
					'id' => $musicCategoryData_details['movie_id'],
					'title' => $musicCategoryData_details['movie_title'],
					'desc' => $musicCategoryData_details['movie_desc'],
					'artist' => $musicCategoryData_details['movie_artist'],
					'movie_image' => base_url() . 'uploads/movie/image/' . $musicCategoryData_details['movie_image'],
					'movie_file' => base_url() . 'uploads/movie/' . $musicCategoryData_details['movie_file'],
					'fav' => $Fav,
					'category_name' => $CategoryName
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('movie_data' => $musicCategoryData_details1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Movie not found', 'response' => array('message' => 'Movie Data not found'));
		}
		$this->response($resp);
	}
	public function  get_meditationVideo_by_Category_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('category_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$pdata = file_get_contents("php://input");
		$data = json_decode($pdata, true);
		$object_info = $data;
		if (empty($data['search'])) {
			$musicCategoryData  = $this->common_model->getAllRecordsById('meditation_videos', array('category_id' => $data['category_id']));
			// echo $this->db->last_query();
			// die('if');
			// echo "<pre>";print_r($musicCategoryData);
			// echo "<br>";
			// die('if');
		} else {
			$conditions = array('category_id' => $data['category_id'], 'movie_title' => $data['search']);
			$musicCategoryData  = $this->common_model->getRecordsByName('meditation_videos', $conditions);
			// echo $this->db->last_query();
			// echo "<br>";
			// die('elsepart');
			// echo "<pre>";
			// print_r($musicCategoryData);
			// die('elsepart');
		}
		if (!empty($musicCategoryData)) {
			foreach ($musicCategoryData as $musicCategoryData_details) {
				$Fav  = $this->getMovieFav($musicCategoryData_details['movie_id']);
				$CategoryName  = $this->getMovieCategoryName($musicCategoryData_details['category_id']);
				if (!empty($musicCategoryData_details['movie_url'])) {
					$flag = "1";
				} else {
					$flag = "0";
				}
				$musicCategoryData_details1[] = array(
					'id' => $musicCategoryData_details['movie_id'],
					'title' => $musicCategoryData_details['movie_title'],
					'desc' => $musicCategoryData_details['movie_desc'],
					'artist' => $musicCategoryData_details['movie_artist'],
					'movie_image' => base_url() . 'uploads/meditation_videos/image/' . $musicCategoryData_details['movie_image'],
					'movie_file' => base_url() . 'uploads/meditation_videos/' . $musicCategoryData_details['movie_file'],
					'meditation_video_url' => (!empty($musicCategoryData_details['movie_url']) ? $musicCategoryData_details['movie_url'] : ''),
					'flag' => $flag,
					'fav' => $Fav,
					'category_name' => $CategoryName
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('meditation_video_data' => $musicCategoryData_details1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Meditation video not found.', 'response' => array('message' => 'Meditation video not found.'));
		}
		$this->response($resp);
	}
	public function  all_movie_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('type');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$pdata = file_get_contents("php://input");
		$data = json_decode($pdata, true);
		$object_info = $data;
		$musicCategoryData  = $this->Api_model->view_all_movie($data['type']);
		if (!empty($musicCategoryData)) {
			foreach ($musicCategoryData as $musicCategoryData_details) {
				$Fav  = $this->getMovieUserFav($musicCategoryData_details['movie_id'], $data['user_id']);
				$CategoryName  = $this->getMovieCategoryName($musicCategoryData_details['category_id']);
				$musicCategoryData_details1[] = array(
					'id' => $musicCategoryData_details['movie_id'],
					'title' => $musicCategoryData_details['movie_title'],
					'desc' => $musicCategoryData_details['movie_desc'],
					'artist' => $musicCategoryData_details['movie_artist'],
					'movie_image' => base_url() . 'uploads/movie/image/' . $musicCategoryData_details['movie_image'],
					'movie_file' => base_url() . 'uploads/movie/' . $musicCategoryData_details['movie_file'],
					'fav' => $Fav,
					'category_name' => $CategoryName
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('movie_data' => $musicCategoryData_details1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Movie not found', 'response' => array('message' => 'Movie Data not found'));
		}
		$this->response($resp);
	}
	public function  my_movie_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$pdata = file_get_contents("php://input");
		$data = json_decode($pdata, true);
		$object_info = $data;
		$musicCategoryData  = $this->common_model->getAllRecordsById('movie', array('user_id' => $data['user_id']));
		if (!empty($musicCategoryData)) {
			foreach ($musicCategoryData as $musicCategoryData_details) {
				$Fav  = $this->getMovieUserFav($musicCategoryData_details['movie_id'], $data['user_id']);
				$CategoryName  = $this->getMovieCategoryName($musicCategoryData_details['category_id']);
				$musicCategoryData_details1[] = array(
					'id' => $musicCategoryData_details['movie_id'],
					'title' => $musicCategoryData_details['movie_title'],
					'desc' => $musicCategoryData_details['movie_desc'],
					'artist' => $musicCategoryData_details['movie_artist'],
					'movie_image' => base_url() . 'uploads/movie/image/' . $musicCategoryData_details['movie_image'],
					'movie_file' => base_url() . 'uploads/movie/' . $musicCategoryData_details['movie_file'],
					'fav' => $Fav,
					'category_name' => $CategoryName,
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('movie_data' => $musicCategoryData_details1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Movie not found', 'response' => array('message' => 'Movie Data not found'));
		}
		$this->response($resp);
	}
	public function movie_delete_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('movie_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$restid = $this->common_model->deleteRecords('movie', array('movie_id' => $data['movie_id']));
		if ($restid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Movie Delete successfully', 'response' => array('movie_data' => "Movie Delete  successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function articleCateogry_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$articleCategoryData  = $this->common_model->getAllRecordsById('cp_article_category', array('article_category_status' => 1));
		if (!empty($articleCategoryData)) {
			foreach ($articleCategoryData as $articleCategoryData_details) {
				$articleCategoryData_details1[] = array(
					'id' => $articleCategoryData_details['article_category_id'],
					'name' => $articleCategoryData_details['article_category_name'],
					'category_icon' => base_url() . 'uploads/articles/category_icon/' . $articleCategoryData_details['article_category_icon'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('category_data' => $articleCategoryData_details1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Category not found', 'response' => array('message' => 'Category Data not found'));
		}
		$this->response($resp);
	}
	public function addUserArticle_post()
	{
		//$data = $_POST;
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id', 'article_title', 'article_desc', 'artist_name', 'category_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		if (!empty($_FILES['article_image']['name'])) {
			$imgfile = $_FILES['article_image']['name'];
			$extension = substr($imgfile, strlen($imgfile) - 4, strlen($imgfile));
			$allowed_extensions = array(".jpg", ".jpeg", ".png", ".gif");
			if (in_array($extension, $allowed_extensions)) // security check start
			{
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['article_image']['name'], PATHINFO_EXTENSION);
				$article_image = $random . "." . $ext;
				$target_dir = "uploads/articles/";
				$target_file = $target_dir . $article_image;
				if (move_uploaded_file($_FILES["article_image"]["tmp_name"], $target_file)) {
					//echo "testfds";die;
					$articleImage = $article_image;
					$post_data['article_image'] = $articleImage;
				}
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your image file is not valide. allow file types jpg, jpeg, png', 'response' => array('message' => 'Your image file is not valide. allow file types jpg, jpeg, png'));
				$this->response($resp);
			}
			// security check end
		}
		if (!empty($_FILES['article_icon']['name'])) {
			$imgfile = $_FILES['article_icon']['name'];
			$extension = substr($imgfile, strlen($imgfile) - 4, strlen($imgfile));
			$allowed_extensions = array(".jpg", ".jpeg", ".png", ".gif");
			if (in_array($extension, $allowed_extensions)) // security check start
			{
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['article_icon']['name'], PATHINFO_EXTENSION);
				$article_icon = $random . "." . $ext;
				$target_dir = "uploads/articles/";
				$target_file = $target_dir . $article_icon;
				if (move_uploaded_file($_FILES["article_icon"]["tmp_name"], $target_file)) {
					$articleIcon = $article_icon;
					$post_data['article_icon'] = $articleIcon;
				}
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your image file is not valide. allow file types jpg, jpeg, png', 'response' => array('message' => 'Your image file is not valide. allow file types jpg, jpeg, png'));
				$this->response($resp);
			}
			// security check end
		}
		if (!empty($_FILES['artist_image']['name'])) {
			$imgfile = $_FILES['artist_image']['name'];
			$extension = substr($imgfile, strlen($imgfile) - 4, strlen($imgfile));
			$allowed_extensions = array(".jpg", ".jpeg", ".png", ".gif");
			if (in_array($extension, $allowed_extensions)) // security check start
			{
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['artist_image']['name'], PATHINFO_EXTENSION);
				$artist_image = $random . "." . $ext;
				$target_dir = "uploads/articles/";
				$target_file = $target_dir . $artist_image;
				if (move_uploaded_file($_FILES["artist_image"]["tmp_name"], $target_file)) {
					$artisImage = $artist_image;
					$post_data['artist_image'] = $artisImage;
				}
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your image file is not valide. allow file types jpg, jpeg, png', 'response' => array('message' => 'Your image file is not valide. allow file types jpg, jpeg, png'));
				$this->response($resp);
			}
			// security check end
		}
		$post_data['category_id']  = $data['category_id'];
		$post_data['user_id'] = $data['user_id'];
		$post_data['article_title'] = $data['article_title'];
		$post_data['article_desc'] = $data['article_desc'];
		$post_data['artist_name'] = $data['artist_name'];
		$post_data['article_created'] = date('Y-m-d h:i:s');
		$articleId = $this->common_model->addRecords('cp_articles', $post_data);
		if ($articleId) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'article added successfully', 'response' => array('article_data' => "article added successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function article_delete_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('article_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$restid = $this->common_model->deleteRecords('cp_articles', array('article_id' => $data['article_id']));
		if ($restid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Article Delete successfully', 'response' => array('article_data' => "Article Delete  successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	// update user movie
	public function updateUserArticle_post()
	{
		//$data = $_POST;
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('article_id', 'article_title', 'article_desc', 'artist_name', 'category_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		if (!empty($_FILES['article_image']['name'])) {
			$imgfile = $_FILES['article_image']['name'];
			$extension = substr($imgfile, strlen($imgfile) - 4, strlen($imgfile));
			$allowed_extensions = array(".jpg", ".jpeg", ".png", ".gif");
			if (in_array($extension, $allowed_extensions)) // security check start
			{
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['article_image']['name'], PATHINFO_EXTENSION);
				$article_image = $random . "." . $ext;
				$target_dir = "uploads/articles/";
				$target_file = $target_dir . $article_image;
				if (move_uploaded_file($_FILES["article_image"]["tmp_name"], $target_file)) {
					//echo "testfds";die;
					$articleImage = $article_image;
					$post_data['article_image'] = $articleImage;
				}
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your image file is not valide. allow file types jpg, jpeg, png', 'response' => array('message' => 'Your image file is not valide. allow file types jpg, jpeg, png'));
				$this->response($resp);
			}
		}
		if (!empty($_FILES['article_icon']['name'])) {
			$imgfile = $_FILES['article_icon']['name'];
			$extension = substr($imgfile, strlen($imgfile) - 4, strlen($imgfile));
			$allowed_extensions = array(".jpg", ".jpeg", ".png", ".gif");
			if (in_array($extension, $allowed_extensions)) // security check start
			{
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['article_icon']['name'], PATHINFO_EXTENSION);
				$article_icon = $random . "." . $ext;
				$target_dir = "uploads/articles/";
				$target_file = $target_dir . $article_icon;
				if (move_uploaded_file($_FILES["article_icon"]["tmp_name"], $target_file)) {
					$articleIcon = $article_icon;
					$post_data['article_icon'] = $articleIcon;
				}
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your image file is not valide. allow file types jpg, jpeg, png', 'response' => array('message' => 'Your image file is not valide. allow file types jpg, jpeg, png'));
				$this->response($resp);
			}
		}
		if (!empty($_FILES['artist_image']['name'])) {
			$imgfile = $_FILES['artist_image']['name'];
			$extension = substr($imgfile, strlen($imgfile) - 4, strlen($imgfile));
			$allowed_extensions = array(".jpg", ".jpeg", ".png", ".gif");
			if (in_array($extension, $allowed_extensions)) // security check start
			{
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['artist_image']['name'], PATHINFO_EXTENSION);
				$artist_image = $random . "." . $ext;
				$target_dir = "uploads/articles/";
				$target_file = $target_dir . $artist_image;
				if (move_uploaded_file($_FILES["artist_image"]["tmp_name"], $target_file)) {
					$artisImage = $artist_image;
					$post_data['artist_image'] = $artisImage;
				}
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Your image file is not valide. allow file types jpg, jpeg, png', 'response' => array('message' => 'Your image file is not valide. allow file types jpg, jpeg, png'));
				$this->response($resp);
			}
		}
		$post_data['category_id']  = $data['category_id'];
		//$post_data['user_id'] = $data['user_id'];
		$post_data['article_title'] = $data['article_title'];
		$post_data['article_desc'] = $data['article_desc'];
		$post_data['artist_name'] = $data['artist_name'];
		// $post_data['article_created'] = date('Y-m-d h:i:s');
		$articleUserId = $this->common_model->updateRecords('cp_articles', $post_data, array('article_id' => $data['article_id']));
		$articledata = $this->common_model->getsingle("cp_articles", array('article_id' => $data['article_id']));
		$fav  = $this->getArticleFavuserby($articledata->article_id, $articledata->user_id);
		$acknowledgeData1 = array(
			'article_id' => $articledata->article_id,
			'article_title' => $articledata->article_title,
			'artist_name' => $articledata->artist_name,
			'article_desc' => $articledata->article_desc,
			'article_image' => base_url() . 'uploads/articles/' . $articledata->article_image,
			'artist_image' => base_url() . 'uploads/articles/' . $articledata->artist_image,
			'create_date' => $articledata->article_created,
			'fav' => $fav,
			'user_id' => $articledata->user_id,
			'category_id' => $articledata->category_id,
		);
		if ($articleUserId) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'article update successfully', 'response' => array('article_data' => $acknowledgeData1));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function article_list_by_categoryid_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id', 'category_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$limit = 10;
		// get popular article data
		$popularCon['sorting'] = array("article_view" => "DESC");
		$popularCon['limit'] = $limit;
		$popularCon['conditions'] = array("category_id" => $data['category_id']);
		$popular_music_data  = $this->Api_model->getRows('cp_articles', $popularCon);
		$popularTotalCon['conditions'] = array("category_id" => $data['category_id']);
		$popular_Total_data  = $this->Api_model->getRows('cp_articles', $popularTotalCon);
		//print_r($popular_Total_data);die;
		if (empty($popular_music_data)) {
			$articleList['Populars'] = array();
		} else {
			$articleList['Populars']['popularCount'] = count($popular_Total_data);
			foreach ($popular_music_data as $row_popular_data) {
				$fav_article = $this->common_model->getsingle("cp_article_favourite", array("article_id" => $row_popular_data['article_id'], 'user_id' => $data['user_id']));
				//echo '<pre>';print_r($fav_article);
				if (!empty($fav_article)) {
					$fav = '1';
				} else {
					$fav = '0';
				}
				//echo '<pre>';print_r($popularFav);die;
				//$popularCategoryName  = $this->getCategoryName($row_popular_data['category_id']);
				$articleList['Populars']['popularList'][] = array(
					'article_id' => $row_popular_data['article_id'],
					'article_title' => $row_popular_data['article_title'],
					'article_desc' => $row_popular_data['article_desc'],
					'artist_name' => $row_popular_data['artist_name'],
					'article_image' => base_url() . 'uploads/articles/' . $row_popular_data['article_image'],
					'artist_image' => base_url() . 'uploads/articles/' . $row_popular_data['artist_image'],
					'fav' => $fav,
					'create_date' => $row_popular_data['article_created'],
					// 'category_name'=>$popularCategoryName,
					'user_id' => $row_popular_data['user_id'],
					'category_id' => $data['category_id'],
				);
			}
		}
		// get recent music data
		$recentCon['sorting'] = array("article_id" => "DESC");
		$recentCon['limit'] = $limit;
		$recentCon['conditions'] = array("category_id" => $data['category_id']);
		$recent_music_data  = $this->Api_model->getRows('cp_articles', $recentCon);
		$recentTotalCon['conditions'] = array("category_id" => $data['category_id']);
		$recent_Total_data  = $this->Api_model->getRows('cp_articles', $recentTotalCon);
		if (empty($recent_music_data)) {
			$articleList['Recents'] = array();
		} else {
			$articleList['Recents']['recentCount'] = count($recent_Total_data);
			foreach ($recent_music_data as $row_recent_data) {
				$refav_article = $this->common_model->getsingle("cp_article_favourite", array("article_id" => $row_recent_data['article_id'], 'user_id' => $data['user_id']));
				if (!empty($refav_article)) {
					$favre = '1';
				} else {
					$favre = '0';
				}
				//$recentFav  = $this->getMusicUserFav($row_recent_data['music_id'],$data['user_id']);
				//$recentCategoryName  = $this->getCategoryName($row_recent_data['category_id']);
				$articleList['Recents']['recentList'][] = array(
					'article_id' => $row_recent_data['article_id'],
					'article_title' => $row_recent_data['article_title'],
					'article_desc' => $row_recent_data['article_desc'],
					'artist_name' => $row_recent_data['artist_name'],
					'article_image' => base_url() . 'uploads/articles/' . $row_recent_data['article_image'],
					'artist_image' => base_url() . 'uploads/articles/' . $row_recent_data['artist_image'],
					'fav' => $favre,
					'create_date' => $row_recent_data['article_created'],
					'category_id' => $data['category_id'],
					'user_id' => $row_recent_data['user_id'],
				);
			}
		}
		// get my music data
		$myMusicCon['sorting'] = array("article_id" => "DESC");
		$myMusicCon['conditions'] = array("user_id" => $data['user_id'], "category_id" => $data['category_id']);
		$myMusicCon['limit'] = $limit;
		$my_music_data  = $this->Api_model->getRows('cp_articles', $myMusicCon);
		$myMusicTotalCon['conditions'] = array("user_id" => $data['user_id'], "category_id" => $data['category_id']);
		$myMusic_Total_data  = $this->Api_model->getRows('cp_articles', $myMusicTotalCon);
		if (empty($my_music_data)) {
			$articleList['myArticle']['myArticleCount'] = "0";
			$articleList['myArticle']['myArticleList'] = array();
		} else {
			$articleList['myArticle']['myArticleCount'] = count($myMusic_Total_data);
			foreach ($my_music_data as $row_mymusic_data) {
				$myfav_article = $this->common_model->getsingle("cp_article_favourite", array("article_id" => $row_mymusic_data['article_id'], 'user_id' => $data['user_id']));
				if (!empty($myfav_article)) {
					$favmy = '1';
				} else {
					$favmy = '0';
				}
				//$myMusicFav  = $this->getMusicUserFav($row_mymusic_data['music_id'],$data['user_id']);
				//$myMusicCategoryName  = $this->getCategoryName($row_mymusic_data['category_id']);
				$articleList['myArticle']['myArticleList'][] = array(
					'article_id' => $row_mymusic_data['article_id'],
					'article_title' => $row_mymusic_data['article_title'],
					'article_desc' => $row_mymusic_data['article_desc'],
					'artist_name' => $row_mymusic_data['artist_name'],
					'article_image' => base_url() . 'uploads/articles/' . $row_mymusic_data['article_image'],
					'artist_image' => base_url() . 'uploads/articles/' . $row_mymusic_data['artist_image'],
					'fav' => $favmy,
					'create_date' => $row_mymusic_data['article_created'],
					// 'category_name'=>$popularCategoryName,
					'category_id' => $data['category_id'],
					'user_id' => $row_mymusic_data['user_id'],
				);
			}
		}
		// get favourite music data
		$favMusicCon['sorting'] = array("article_fav_id" => "DESC");
		$favMusicCon['limit'] = $limit;
		$favMusicCon['conditions'] = array("cp_article_favourite.user_id" => $data['user_id'], "cp_articles.category_id" => $data['category_id']);
		$fav_music_data  = $this->Api_model->getRowsFavouriteArticle('cp_article_favourite', $favMusicCon);
		// print_r("<pre/>");
		//print_r($fav_music_data);
		//  die;
		$favMusicTotalCon['conditions'] = array(
			"cp_article_favourite.user_id" => $data['user_id'],
			"cp_articles.category_id" => $data['category_id']
		);
		$favMusic_Total_data  = $this->Api_model->getRowsFavouriteArticle('cp_article_favourite', $favMusicTotalCon);
		if (empty($fav_music_data)) {
			$articleList['Favourite']['favCount'] = "0";
			$articleList['Favourite']['favList'] = array();
		} else {
			$articleList['Favourite']['favCount'] = count($favMusic_Total_data);
			foreach ($fav_music_data as $row_favmusic_data) {
				$myfav_article1 = $this->common_model->getsingle("cp_article_favourite", array("article_id" => $row_favmusic_data['article_id'], 'user_id' => $data['user_id']));
				if (!empty($myfav_article1)) {
					$fav1 = '1';
				} else {
					$fav1 = '0';
				}
				//$favMusicCategoryName  = $this->getCategoryName($row_favmusic_data['category_id']);
				$articleList['Favourite']['favList'][] = array(
					'article_id' => $row_favmusic_data['article_id'],
					'article_title' => $row_favmusic_data['article_title'],
					'article_desc' => $row_favmusic_data['article_desc'],
					'artist_name' => $row_favmusic_data['artist_name'],
					'article_image' => base_url() . 'uploads/articles/' . $row_favmusic_data['article_image'],
					'artist_image' => base_url() . 'uploads/articles/' . $row_favmusic_data['artist_image'],
					'fav' => 1,
					'create_date' => $row_favmusic_data['article_created'],
					// 'category_name'=>$popularCategoryName,
					'category_id' => $data['category_id'],
					'user_id' => $row_favmusic_data['user_id'],
				);
			}
		}
		if (!empty($banner_data) or !empty($popular_music_data) or !empty($recent_music_data) or !empty($category_data) or !empty($my_music_data) or !empty($fav_music_data)) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('articleList' => $articleList));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Music Data not found', 'response' => array('message' => 'Music Data not found'));
		}
		$this->response($resp);
	}
	// add popular data
	public function add_popular_article_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('article_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$this->db->select('article_view');
		$this->db->from('music');
		$this->db->where("article_id", $data['article_id']);
		$query = $this->db->get();
		$result = $query->row_array();
		$view = $result['article_view'];
		$count_where  = array('article_id' => $data['article_id']);
		$count_array  = array('article_view' => $view + 1);
		$musicId = $this->common_model->updateRecords('music', $count_array, $count_where);
		if ($musicId) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Added successfully', 'response' => array('message' => 'Added successfully'));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function view_all_article_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('category_id', "type");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		if ($data['type'] == '1') {
			$article_data1 = $this->common_model->getAllwhereorderby("cp_articles", array("category_id" => $data['category_id']), "article_view", "desc");
		} elseif ($data['type'] == '2') {
			$article_data1 = $this->common_model->getAllwhereorderby("cp_articles", array("category_id" => $data['category_id']), "article_id", "desc");
		}
		if (!empty($article_data1)) {
			foreach ($article_data1 as $article_data) {
				$fav_article = $this->common_model->getsingle("cp_article_favourite", array("article_id" => $article_data->article_id, 'user_id' => $article_data->user_id));
				if (!empty($fav_article)) {
					$fav = '1';
				} else {
					$fav = '0';
				}
				$data['article_data'][] = array(
					'article_id' => $article_data->article_id,
					'article_title' => $article_data->article_title,
					'artist_name' => $article_data->artist_name,
					'article_desc' => $article_data->article_desc,
					'article_image' => base_url() . 'uploads/articles/' . $article_data->article_image,
					'artist_image' => base_url() . 'uploads/articles/' . $article_data->artist_image,
					'user_id' => $article_data->user_id,
					'category_id' => $article_data->category_id,
					'fav' => $fav,
					'create_date' => $article_data->article_created
				);
			}
		}
		if (!empty($article_data1)) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('articleList' => $data));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Article Data not found', 'response' => array('message' => 'Article Data not found'));
		}
		$this->response($resp);
	}
	public function view_all_my_article_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('category_id', "user_id");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$article_data1 = $this->common_model->getAllwhereorderby("cp_articles", array("category_id" => $data['category_id'], "user_id" => $data['user_id']), "article_id", "desc");
		if (!empty($article_data1)) {
			foreach ($article_data1 as $article_data) {
				$fav_article = $this->common_model->getsingle("cp_article_favourite", array("article_id" => $article_data->article_id, 'user_id' => $article_data->user_id));
				if (!empty($fav_article)) {
					$fav = '1';
				} else {
					$fav = '0';
				}
				$data['article_data'][] = array(
					'article_id' => $article_data->article_id,
					'article_title' => $article_data->article_title,
					'artist_name' => $article_data->artist_name,
					'article_desc' => $article_data->article_desc,
					'article_image' => base_url() . 'uploads/articles/' . $article_data->article_image,
					'artist_image' => base_url() . 'uploads/articles/' . $article_data->artist_image,
					'user_id' => $article_data->user_id,
					'category_id' => $article_data->category_id,
					'fav' => $fav,
					'create_date' => $article_data->article_created
				);
			}
		}
		if (!empty($article_data1)) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('articleList' => $data));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Article Data not found', 'response' => array('message' => 'Article Data not found'));
		}
		$this->response($resp);
	}
	public function view_all_favorite_article_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('category_id', "user_id");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$article_data1 = $this->common_model->getAllMyFavArticle($data["category_id"], $data["user_id"]);
		if (!empty($article_data1)) {
			foreach ($article_data1 as $article_data) {
				$fav_article = $this->common_model->getsingle("cp_article_favourite", array("article_id" => $article_data->article_id, 'user_id' => $article_data->user_id));
				if (!empty($fav_article)) {
					$fav = '1';
				} else {
					$fav = '0';
				}
				$data['article_data'][] = array(
					'article_id' => $article_data->article_id,
					'article_title' => $article_data->article_title,
					'artist_name' => $article_data->artist_name,
					'article_desc' => $article_data->article_desc,
					'article_image' => base_url() . 'uploads/articles/' . $article_data->article_image,
					'artist_image' => base_url() . 'uploads/articles/' . $article_data->artist_image,
					'user_id' => $article_data->user_id,
					'category_id' => $article_data->category_id,
					'fav' => $fav,
					'create_date' => $article_data->article_fav_created
				);
			}
		}
		if (!empty($article_data1)) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('articleList' => $data));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Article Data not found', 'response' => array('message' => 'Article Data not found'));
		}
		$this->response($resp);
	}
	public function doctor_available_date_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('doctor_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$Doctor_dates = $this->common_model->getAllDoctorappointmets("dr_available_date", array("avdate_dr_id" => $data["doctor_id"]), "avdate_date", "avdate_date");
		foreach ($Doctor_dates as $dates) {
			$data['doctor_data'][] = array(
				"available_date" => $dates["avdate_date"]
			);
		}
		//echo '<pre>';print_r($data);die;
		if (!empty($Doctor_dates)) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('doctor_data' => $data));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Article Data not found', 'response' => array('message' => 'Article Data not found'));
		}
		$this->response($resp);
	}
	public function contact_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		//print_r("<pre/>");
		//print_r($data);				
		// die;				
		$data = $this->param;
		foreach ($data["ContactList"] as $contact) {
			$ph_number = preg_replace("/[^0-9]/", "", $contact["contact_number"]);
			$insert_array = array("contact_number" => $contact["contact_number"], "user_id" => $data["user_id"]);
			$this->common_model->addRecords("cp_contact_list", $insert_array);
			//$check_user = $this->common_model->getuserwhere($contact["contact_number"],$contact["contact_email"],$contact["contact_name"]);
			$check_user = $this->common_model->getuserwhere($ph_number);
			//echo $this->db->last_query();die;
			if (!empty($check_user)) {
				foreach ($check_user as $me) {
					if (!empty($me['profile_image'])) {
						$user_image = base_url() . "uploads/profile_images/" . $me['profile_image'];
					} else {
						$user_image = "";
					}
					$arrds[] = array(
						"contact_id" => $me['id'],
						"contact_name" => $me['username'],
						"contact_number" => $me['mobile'],
						"contact_email" => $me['email'],
						"firebase_token" => $me['device_token'],
						"contact_image" => $user_image,
					);
				}
			}
		}
		$array = array(
			"user_id" => $data["user_id"],
			"ContactList" => $arrds
		);
		//echo '<pre>';print_r($arrds);die;
		if (!empty($arrds)) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('contact_data' => $array));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Contact Data not found', 'response' => array('message' => 'Contact Data not found'));
		}
		$this->response($resp);
	}
	public function add_notification_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id', 'user_name', 'other_user_id', 'other_user_name', 'type');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$notification_array = array(
			'user_id' => $data['user_id'],
			'user_name' => $data['user_name'],
			'other_user_id' => $data['other_user_id'],
			'other_user_name' => $data['other_user_name'],
			'roomName' => $data['roomName'],
			'type' => $data['type'],
			'mobile_number' => $data['mobile_number'],
			'other_mobile_number' => $data['other_mobile_number'],
			'create_date' => date('Y-m-d H:i:s')
		);
		$notification_ids = $this->common_model->addRecords('cp_app_notification', $notification_array);
		$other_data = $this->common_model->getsingle("cp_users", array("id" => $data['other_user_id']));
		$session_data = $this->common_model->getsingle("cp_users", array("id" => $data['user_id']));
		sendNotification($other_data->device_token, $data['user_id'], $data['other_user_id'], $session_data->username, $other_data->username, $data['roomName'], $data['type'], $data['mobile_number'], $data['other_mobile_number'], $other_data->device_id);
		if (!empty($notification_ids)) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS');
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Contact Data not found');
		}
		$this->response($resp);
	}
	public function device_token_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('device_id', 'device_token', 'user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$data_array = array(
			'device_id' => $data['device_id'],
			'device_token' => $data['device_token'],
		);
		$update = $this->common_model->updateRecords("cp_users", $data_array, array('id' => $data['user_id']));
		$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS');
		$this->response($resp);
	}
	public function search_email_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('textmessage', 'user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$search_email = $this->common_model->getsearchemail($data['textmessage'], $data['user_id']);
		//echo '<pre>';print_r($search_email);die;
		foreach ($search_email as $search) {
			if ($search['user_id'] == $data['user_id']) {
				$inbox_status = 1;
			} else {
				$inbox_status = 2;
			}
			if ($search['create_email'] == "0000-00-00") {
				$emails_create = $search['create_email'];
			} else {
				$emails_create = date("M d, Y H:i:s", strtotime($search['create_email']));
			}
			$from_ids = $this->common_model->getsingle("cp_users", array("id" => $search['user_id']));
			$attachment = $this->orderImage($search['email_id']);
			$getMail = $this->getMail($search['user_id']);
			$array[] = array(
				'user_id' => $search['user_id'],
				"to_email" => $search['to_email'],
				"subject" => $search['subject'],
				"message" => $search['message'],
				"create_date" => $emails_create,
				"username" => $from_ids->username,
				"attachment" => $attachment,
				'profile_image' => base_url() . 'uploads/profile_images/' . $from_ids->profile_image,
				"id" => $search['email_id'],
				'send_email' => $getMail,
				'mail_status' => $inbox_status,
			);
		}
		if (!empty($search_email)) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('email_data' => $array));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'No messages matched your search');
		}
		$this->response($resp);
	}
	public function place_unit_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('unit_type', 'user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$unit_test = $this->common_model->getsingle("place_unit", array("place_unit_id" => 1));
		$placetype['placeUnit'] = array(
			"unit_id" => $unit_test->place_unit_id,
			"place_unit" => $unit_test->place_unit
		);
		if ($data['unit_type'] == 'restaurant') {
			$restaurant_fav = $this->common_model->getAllwhere("cp_fav_restaurant", array("user_id" => $data['user_id']));
			foreach ($restaurant_fav as $rest_fav) {
				$placetype['restaurant_data'][] = array(
					"id" => $rest_fav->fav_rest_id,
					"name" => $rest_fav->fav_rest_name,
					"image" => $rest_fav->fav_rest_image,
					"address" => $rest_fav->fav_rest_address,
					"user_id" => $rest_fav->user_id,
					"rating" => $rest_fav->fav_rest_rating,
					'place_id' => $rest_fav->fav_rest_place_id
				);
			}
		} elseif ($data['unit_type'] == 'bank') {
			$bank_fav = $this->common_model->getAllwhere("cp_fav_banks", array("user_id" => $data['user_id']));
			foreach ($bank_fav as $bnk_fv) {
				$placetype['bank_data'][] = array(
					"id" => $bnk_fv->fav_bank_id,
					"name" => $bnk_fv->fav_bank_name,
					"image" => $bnk_fv->fav_bank_image,
					"address" => $bnk_fv->fav_bank_address,
					"user_id" => $bnk_fv->user_id,
					"rating" => $bnk_fv->fav_bank_rating,
					'place_id' => $bnk_fv->fav_place_id
				);
			}
		} elseif ($data['unit_type'] == 'supermarket') {
			$grocery_fav = $this->common_model->getAllwhere("cp_fav_grocery", array("user_id" => $data['user_id']));
			foreach ($grocery_fav as $groc_fv) {
				$placetype['grocery_data'][] = array(
					"id" => $groc_fv->fav_grocery_id,
					"name" => $groc_fv->fav_grocery_name,
					"image" => $groc_fv->fav_grocery_image,
					"address" => $groc_fv->fav_grocery_address,
					"user_id" => $groc_fv->user_id,
					"rating" => $groc_fv->fav_grocery_rating,
					'place_id' => $groc_fv->fav_grocery_place_id
				);
			}
		}
		$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => $placetype);
		$this->response($resp);
	}
	public function permanent_delete_post1()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('trash_email_id', 'user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$trash_dta = $this->common_model->getsingle("cp_email_trash", array("trash_email_id" => $data['trash_email_id']));
		if ($data['mail_status'] == 1) {
			$this->common_model->deleteRecords("cp_to_email", array("email_id" => $trash_dta->email_id, "user_id" => $data['user_id']));
		} else {
			$this->common_model->deleteRecords("cp_emails", array("email_id" => $trash_dta->email_id, "user_id" => $data['user_id']));
		}
		$restid = $this->common_model->deleteRecords("cp_email_trash", array("trash_email_id" => $data['trash_email_id']));
		if ($restid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Successfully delete', 'response' => array('email_data' => "Successfully delete"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function permanent_delete_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('trash_email_id', 'user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$explode = explode(",", $data["trash_email_id"]);
		foreach ($explode as $email_t) {
			$trash_dta = $this->common_model->getsingle("cp_email_trash", array("trash_email_id" => $email_t));
			if ($data['mail_status'] == 1) {
				$this->common_model->deleteRecords("cp_to_email", array("email_id" => $trash_dta->email_id, "user_id" => $data['user_id']));
			} else {
				$this->common_model->deleteRecords("cp_emails", array("email_id" => $trash_dta->email_id, "user_id" => $data['user_id']));
			}
			$restid = $this->common_model->deleteRecords("cp_email_trash", array("trash_email_id" => $email_t));
		}
		if ($restid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Successfully delete', 'response' => array('email_data' => "Successfully delete"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function all_fav_article_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('category_id', 'user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$see_all_article = $this->common_model->jointwotablenn("cp_articles", "user_id", "cp_article_favourite", "user_id", array("cp_articles.category_id" => $data['category_id'], "cp_article_favourite.user_id" => $data['user_id']), "*", "cp_article_favourite.article_fav_id", "desc");
		//echo $this->db->last_query();die;
		//echo '<pre>';print_r($see_all_article);die;
		if (!empty($see_all_article)) {
			foreach ($see_all_article as $see_art) {
				//$article_data[]
				$data['article_data'][] = array(
					'id' =>  $see_art->article_fav_id,
					'title' => $see_art->article_title,
					'artist_name' => $see_art->artist_name,
					'description' => $see_art->article_desc,
					'article_image' => base_url() . 'uploads/articles/' . $see_art->article_image,
					'artist_image' => base_url() . 'uploads/articles/' . $see_art->artist_image,
					'date' => $see_art->article_fav_created,
				);
			}
			//$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Successfully delete', 'response' => array('articlefav_data' => $article_data));
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('articleList' => $data));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function add_call_history_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('sender_id', 'user_name', 'call_duration', 'date_time', 'call_type', 'calling_type');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$new_reason = '';
		if (!empty($data["reason_for_disconnect"])) {
			$new_reason = $data["reason_for_disconnect"];
		}
		$new_receiver = '';
		if (!empty($data["receiver_id"])) {
			$new_receiver = $data["receiver_id"];
		}
		$new_receiver_contact = '';
		if (!empty($data["receiver_contact"])) {
			$new_receiver_contact = $data["receiver_contact"];
		}
		$add_array = array(
			'user_id' => $data["sender_id"],
			'user_name' => $data["user_name"],
			'call_duration' => $data["call_duration"],
			'date_time' => $data["date_time"],
			'call_type' => $data["call_type"],
			'receiver_id' => $new_receiver,
			'receiver_contact' => $new_receiver_contact,
			'calling_type' => $data["calling_type"],
			'reason_for_disconnect' => $new_reason,
			'create_date' => date('Y-m-d H:i:s')
		);
		//echo '<pre>';print_r($add_array);die;
		$insert_id = $this->common_model->addRecords("cp_call_history", $add_array);
		$add_arrays = array(
			'user_id' => $data["sender_id"],
			'user_name' => $data["user_name"],
			'call_duration' => $data["call_duration"],
			'date_time' => $data["date_time"],
			'receiver_id' => $new_receiver,
			'reason_for_disconnect' => $new_reason,
			'calling_type' => $data["calling_type"],
			'call_type' => $data["call_type"],
			'receiver_contact' => $new_receiver_contact,
		);
		if ($insert_id) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Successfully added', 'response' => array('callHistoryData' => $add_arrays));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function call_history_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$limit = 50;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$historyCount = $this->common_model->getAllwhere("cp_call_history", array("user_id" => $data['user_id']));
		$start  = ($data['page_no'] - 1) * $limit;
		$pages = ceil(count($historyCount) / $limit);
		$historyData = $this->common_model->getCallHistory($start, $limit, $data['user_id']);
		//print_r("<pre/>");
		//print_r($historyData);
		//die;
		if (!empty($historyData)) {
			foreach ($historyData as $row) {
				$user_image = $this->common_model->getsingle("cp_users", array("id" => $row["user_id"]));
				$add_array[] = array(
					'user_id' => $row["user_id"],
					'user_name' => $row["user_name"],
					'call_duration' => $row["call_duration"],
					'date_time' => $row["date_time"],
					'call_type' => $row["call_type"],
					'reason_for_disconnect' => $row["reason_for_disconnect"],
					'calling_type' => $row["calling_type"],
					'image_url' => base_url() . "uploads/profile_images/" . $user_image->profile_image,
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('callHistoryData' => $add_array));
		} else {
			$add_array = array();
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'History not found', 'response' => array('message' => 'History not found', 'callHistoryData' => $add_array));
		}
		$this->response($resp);
	}
	public function user_call_history_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('sender_id', 'receiver_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$call_detail = $this->common_model->getAllwhere('cp_call_history', array('user_id' => $data['sender_id'], 'receiver_id' => $data['receiver_id']));
		if (!empty($call_detail)) {
			foreach ($call_detail as $detail) {
				$add_arrays[] = array(
					'sender_id' => $data["sender_id"],
					'receiver_id' => $data["receiver_id"],
					'user_name' => $detail->user_name,
					'call_duration' => $detail->call_duration,
					'reason_for_disconnect' => $detail->reason_for_disconnect,
					'date_time' => $detail->date_time,
					'call_type' => $detail->call_type,
					'calling_type' => $detail->calling_type
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('callHistoryData' => $add_arrays));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	public function read_status_note_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('read_status', "note_id");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$post_data = array(
			'read_status' => $data['read_status'],
		);
		$acknowledgeId = $this->common_model->updateRecords('cp_note', $post_data, array('note_id' => $data['note_id']));
		if ($acknowledgeId) {
			$getdata = $this->common_model->getsingle("cp_note", array('note_id' => $data['note_id']));
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Note update successfully', 'response' => array("read_status" => $getdata->read_status));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/*Meditation articles category list*/
	public function MeditetionArticleCateogry_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$articleCategoryData  = $this->common_model->getAllRecordsById('cp_meditation_art_category', array('article_category_status' => 1));
		if (!empty($articleCategoryData)) {
			foreach ($articleCategoryData as $articleCategoryData_details) {
				$articleCategoryData_details1[] = array(
					'id' => $articleCategoryData_details['article_category_id'],
					'name' => $articleCategoryData_details['article_category_name'],
					'category_icon' => base_url() . 'uploads/meditation_articles/category_icon/' . $articleCategoryData_details['article_category_icon'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('category_data' => $articleCategoryData_details1));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Category not found', 'response' => array('message' => 'Category Data not found'));
		}
		$this->response($resp);
	}
	/*get user's favorite meditation videos*/
	public function  getMeditationvideosUserFav($movie_id, $user_id)
	{
		$this->db->select('*');
		$this->db->from('cp_meditaion_videos_favorite');
		$this->db->where('movie_id', $movie_id);
		$this->db->where('user_id', $user_id);
		//$this->db->where('order_id',$order_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return "1";
		}
		return "0";
	}
	// get meditation video category name
	public function getMeditationvideoCategoryName($movie_category_id)
	{
		$this->db->select('movie_category_name');
		$this->db->from('cp_meditation_videos_category');
		$this->db->where("movie_category_id", $movie_category_id);
		//$this->db->where("movie_category_status",1);
		$query = $this->db->get();
		$result = $query->row_array();
		$categoryName = $result['movie_category_name'];
		if ($categoryName) {
			return $categoryName;
		} else {
			return "NA";
		}
	}
	/*get favorite meditation videos*/
	public function  getMeditationvideoFav($movie_id)
	{
		$this->db->select('*');
		$this->db->from('cp_meditaion_videos_favorite');
		$this->db->where('movie_id', $movie_id);
		//$this->db->where('order_id',$order_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return "1";
		}
		return "0";
	}
	//Meditation vedio list API
	public function meditation_vedio_list_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$limit = 10;
		// get banner data
		$bannerCon['sorting'] = array("movie_banner_id" => "DESC");
		$bannerCon['conditions'] = array("movie_banner_status" => 1);
		$bannerCon['limit'] = $limit;
		$banner_data  = $this->Api_model->getRows('cp_meditation_videos_banner', $bannerCon);
		$bannerTotalCon['conditions'] = array("movie_banner_status" => 1);
		$banner_Total_data  = $this->Api_model->getRows('cp_meditation_videos_banner', $bannerTotalCon);
		if (empty($banner_data)) {
			$movieList['Banners'] = array();
		} else {
			$movieList['Banners']['bannerCount'] = count($banner_Total_data);
			foreach ($banner_data as $row_banner_data) {
				$movieList['Banners']['bannerList'][] = array(
					'id' => $row_banner_data['movie_banner_id'],
					'meditation_video_banner_path' => base_url() . 'uploads/meditation_videos/banners/' . $row_banner_data['movie_banner_image'],
					'meditation_video_banner_file' => base_url() . 'uploads/meditation_videos/banners/files/' . $row_banner_data['movie_banner_file']
				);
			}
		}
		// get popular music data
		$popularCon['sorting'] = array("movie_view" => "DESC");
		$popularCon['limit'] = $limit;
		$popular_movie_data  = $this->Api_model->getRows('meditation_videos', $popularCon);
		$popular_Total_data  = $this->Api_model->getRows('meditation_videos');
		if (empty($popular_movie_data)) {
			$movieList['Populars']['popularCount'] = 0;
			$movieList['Populars']['popularList'] = array();
		} else {
			$movieList['Populars']['popularCount'] = count($popular_Total_data);
			foreach ($popular_movie_data as $row_popular_data) {
				$popularFav  = $this->getMeditationvideosUserFav($row_popular_data['movie_id'], $data['user_id']);
				$popularCategoryName  = $this->getMeditationvideoCategoryName($row_popular_data['category_id']);
				$mypopu = $this->common_model->getsingle("meditation_videos", array("user_id" => $row_popular_data['user_id']));
				if ($mypopu->user_id == $data['user_id']) {
					$isedit_popular = "1";
				} else {
					$isedit_popular = "";
				}
				if (!empty($row_popular_data['movie_url'])) {
					$flag = "1";
				} else {
					$flag = "0";
				}
				$movieList['Populars']['popularList'][] = array(
					'id' => $row_popular_data['movie_id'],
					'title' => $row_popular_data['movie_title'],
					'desc' => $row_popular_data['movie_desc'],
					'artist' => $row_popular_data['movie_artist'],
					'meditation_video_image' => base_url() . 'uploads/meditation_videos/image/' . $row_popular_data['movie_image'],
					'meditation_video_file' => base_url() . 'uploads/meditation_videos/' . $row_popular_data['movie_file'],
					'meditation_video_url' => (!empty($row_popular_data['movie_url']) ? $row_popular_data['movie_url'] : ''),
					'flag' => $flag,
					'fav' => $popularFav,
					'category_name' => $popularCategoryName,
					'isEditable' => $isedit_popular
				);
			}
		}
		// get recent music data
		$recentCon['sorting'] = array("movie_id" => "DESC");
		$recentCon['limit'] = $limit;
		$recent_movie_data  = $this->Api_model->getRows('meditation_videos', $recentCon);
		$recent_Total_data  = $this->Api_model->getRows('meditation_videos');
		if (empty($recent_movie_data)) {
			$movieList['Recents']['recentCount'] = 0;
			$movieList['Recents']['recentList'] = array();
		} else {
			$movieList['Recents']['recentCount'] = count($recent_Total_data);
			foreach ($recent_movie_data as $row_recent_data) {
				$recentFav  = $this->getMeditationvideoFav($row_recent_data['movie_id']);
				$recentCategoryName  = $this->getMeditationvideoCategoryName($row_recent_data['category_id']);
				$myrece = $this->common_model->getsingle("meditation_videos", array("user_id" => $row_recent_data['user_id']));
				//$myrece = $this->common_model->getsingle("movie",array("user_id" => $row_recent_data['user_id']));
				if ($myrece->user_id == $data['user_id']) {
					$isedit_recent = "1";
				} else {
					$isedit_recent = "";
				}
				if (!empty($row_recent_data['movie_url'])) {
					$flag = "1";
				} else {
					$flag = "0";
				}
				$movieList['Recents']['recentList'][] = array(
					'id' => $row_recent_data['movie_id'],
					'title' => $row_recent_data['movie_title'],
					'desc' => $row_recent_data['movie_desc'],
					'artist' => $row_recent_data['movie_artist'],
					'meditation_video_image' => base_url() . 'uploads/meditation_videos/image/' . $row_recent_data['movie_image'],
					'meditation_video_file' => base_url() . 'uploads/meditation_videos/' . $row_recent_data['movie_file'],
					'meditation_video_url' => (!empty($row_recent_data['movie_url']) ? $row_recent_data['movie_url'] : ''),
					'flag' => $flag,
					'fav' => $recentFav,
					'category_name' => $recentCategoryName,
					'isEditable' => $isedit_recent
				);
			}
		}
		// get category data
		$categoryCon['sorting'] = array("movie_category_id" => "DESC");
		$categoryCon['limit'] = $limit;
		$categoryCon['conditions'] = array("movie_category_status" => 1);
		$category_data  = $this->Api_model->getRows('cp_meditation_videos_category', $categoryCon);
		$categoryTotalCon['conditions'] = array("movie_category_id" => 1);
		$category_Total_data  = $this->Api_model->getRows('cp_meditation_videos_category', $categoryTotalCon);
		if (empty($category_data)) {
			$movieList['Categories'] = array();
		} else {
			$movieList['Categories']['categoryCount'] = count($category_Total_data);
			foreach ($category_data as $row_category_data) {
				$movieList['Categories']['categoryList'][] = array(
					'id' => $row_category_data['movie_category_id'],
					'name' => $row_category_data['movie_category_name'],
					'category_icon' => base_url() . 'uploads/meditation_videos/category_icon/' . $row_category_data['movie_category_icon'],
				);
			}
		}
		// get my movie data
		$myMovieCon['sorting'] = array("movie_id" => "DESC");
		$myMovieCon['conditions'] = array("user_id" => $data['user_id']);
		$myMovieCon['limit'] = $limit;
		$my_movie_data  = $this->Api_model->getRows('meditation_videos', $myMovieCon);
		$myMovieTotalCon['conditions'] = array("user_id" => $data['user_id']);
		$myMovie_Total_data  = $this->Api_model->getRows('meditation_videos', $myMovieTotalCon);
		if (empty($my_movie_data)) {
			$movieList['myMovie']['myMovieCount'] = "0";
			$movieList['myMovie']['myMovieList'] = array();
		} else {
			$movieList['myMovie']['myMovieCount'] = count($myMovie_Total_data);
			foreach ($my_movie_data as $row_mymovie_data) {
				$myMovieFav  = $this->getMeditationvideoFav($row_mymovie_data['movie_id']);
				$myMovieCategoryName  = $this->getMeditationvideoCategoryName($row_mymovie_data['category_id']);
				if (!empty($row_mymovie_data['movie_url'])) {
					$flag = "1";
				} else {
					$flag = "0";
				}
				$movieList['myMovie']['myMovieList'][] = array(
					'id' => $row_mymovie_data['movie_id'],
					'title' => $row_mymovie_data['movie_title'],
					'desc' => $row_mymovie_data['movie_desc'],
					'artist' => $row_mymovie_data['movie_artist'],
					'meditation_video_image' => base_url() . 'uploads/meditation_videos/image/' . $row_mymovie_data['movie_image'],
					'meditation_video_file' => base_url() . 'uploads/meditation_videos/' . $row_mymovie_data['movie_file'],
					'meditation_video_url' => (!empty($row_mymovie_data['movie_url']) ? $row_mymovie_data['movie_url'] : ''),
					'flag' => $flag,
					'fav' => $myMovieFav,
					'category_name' => $myMovieCategoryName,
					'isEditable' => 1
				);
			}
		}
		$favMovieCon['sorting'] = array("movie_fav_id" => "DESC");
		$favMovieCon['limit'] = $limit;
		$favMovieCon['conditions'] = array("cp_meditaion_videos_favorite.user_id" => $data['user_id']);
		$fav_movie_data  = $this->Api_model->getRowsFavouriteMeditationVideos('cp_meditaion_videos_favorite', $favMovieCon);
		$favMovieTotalCon['conditions'] = array("cp_meditaion_videos_favorite.user_id" => $data['user_id']);
		$favMovie_Total_data  = $this->Api_model->getRowsFavouriteMeditationVideos('cp_meditaion_videos_favorite', $favMovieTotalCon);
		if (empty($fav_movie_data)) {
			$movieList['Favourite']['favCount'] = "0";
			$movieList['Favourite']['favList'] = array();
		} else {
			$movieList['Favourite']['favCount'] = count($favMovie_Total_data);
			foreach ($fav_movie_data as $row_favmovie_data) {
				//$favMovieCategoryName  = $this->getCategoryName($row_favmovie_data['category_id']);
				$favMovieCategoryName  = $this->getMeditationvideoCategoryName($row_favmovie_data['category_id']);
				$myfavmo = $this->common_model->getsingle("meditation_videos", array("user_id" => $row_favmovie_data['user_id']));
				if ($myfavmo->user_id == $data['user_id']) {
					$isedit_fav = "1";
				} else {
					$isedit_fav = "";
				}
				if (!empty($row_favmovie_data['movie_url'])) {
					$flag = "1";
				} else {
					$flag = "0";
				}
				$movieList['Favourite']['favList'][] = array(
					'id' => $row_favmovie_data['movie_fav_id'],
					'title' => $row_favmovie_data['movie_title'],
					'desc' => $row_favmovie_data['movie_desc'],
					'artist' => $row_favmovie_data['movie_artist'],
					'meditation_video_image' => base_url() . 'uploads/meditation_videos/image/' . $row_favmovie_data['movie_image'],
					'meditation_video_file' => base_url() . 'uploads/meditation_videos/' . $row_favmovie_data['movie_file'],
					'meditation_video_url' => (!empty($row_favmovie_data['movie_url']) ? $row_favmovie_data['movie_url'] : ''),
					'meditation_video_id' => $row_favmovie_data['movie_id'],
					'flag' => $flag,
					'category_name' => $favMovieCategoryName,
					'fav' => 1,
					"isEditable" => $isedit_fav
				);
			}
		}
		$data = array();
		$data = array_merge($movieList['Populars']['popularList'],$movieList['Recents']['recentList'],$movieList['myMovie']['myMovieList'],$movieList['Favourite']['favList']);
		if (!empty($banner_data) or !empty($popular_movie_data) or !empty($recent_movie_data) or !empty($category_data) or !empty($my_movie_data) or !empty($fav_movie_data)) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('meditation_videos_list' => $data));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Meditation Video Data not found', 'response' => array('message' => 'Meditation Video Data not found'));
			// $resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Moview Data not found','response' => array('message' => 'Movie Data not found'));
		}
		$this->response($resp);
	}
	/*Meditation articles list by category*/
	public function meditation_article_list_by_categoryid_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id', 'category_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$limit = 10;
		// get popular article data
		$popularCon['sorting'] = array("article_view" => "DESC");
		$popularCon['limit'] = $limit;
		$popularCon['conditions'] = array("category_id" => $data['category_id']);
		$popular_music_data  = $this->Api_model->getRows('cp_meditation_articles', $popularCon);
		$popularTotalCon['conditions'] = array("category_id" => $data['category_id']);
		$popular_Total_data  = $this->Api_model->getRows('cp_meditation_articles', $popularTotalCon);
		//print_r($popular_Total_data);die;
		if (empty($popular_music_data)) {
			$articleList['Populars'] = array();
		} else {
			$articleList['Populars']['popularCount'] = count($popular_Total_data);
			foreach ($popular_music_data as $row_popular_data) {
				$fav_article = $this->common_model->getsingle("cp_meditation_article_favourite", array("article_id" => $row_popular_data['article_id'], 'user_id' => $data['user_id']));
				//echo '<pre>';print_r($fav_article);
				if (!empty($fav_article)) {
					$fav = '1';
				} else {
					$fav = '0';
				}
				//echo '<pre>';print_r($popularFav);die;
				//$popularCategoryName  = $this->getCategoryName($row_popular_data['category_id']);
				$articleList['Populars']['popularList'][] = array(
					'article_id' => $row_popular_data['article_id'],
					'article_title' => $row_popular_data['article_title'],
					'article_desc' => $row_popular_data['article_desc'],
					'artist_name' => $row_popular_data['artist_name'],
					'article_image' => base_url() . 'uploads/meditation_articles/' . $row_popular_data['article_image'],
					'artist_image' => base_url() . 'uploads/meditation_articles/' . $row_popular_data['artist_image'],
					'fav' => $fav,
					'create_date' => $row_popular_data['article_created'],
					// 'category_name'=>$popularCategoryName,
					'user_id' => $row_popular_data['user_id'],
					'category_id' => $data['category_id'],
				);
			}
		}
		// get recent music data
		$recentCon['sorting'] = array("article_id" => "DESC");
		$recentCon['limit'] = $limit;
		$recentCon['conditions'] = array("category_id" => $data['category_id']);
		$recent_music_data  = $this->Api_model->getRows(' cp_meditation_articles', $recentCon);
		$recentTotalCon['conditions'] = array("category_id" => $data['category_id']);
		$recent_Total_data  = $this->Api_model->getRows(' cp_meditation_articles', $recentTotalCon);
		if (empty($recent_music_data)) {
			$articleList['Recents'] = array();
		} else {
			$articleList['Recents']['recentCount'] = count($recent_Total_data);
			foreach ($recent_music_data as $row_recent_data) {
				$refav_article = $this->common_model->getsingle("cp_meditation_article_favourite", array("article_id" => $row_recent_data['article_id'], 'user_id' => $data['user_id']));
				if (!empty($refav_article)) {
					$favre = '1';
				} else {
					$favre = '0';
				}
				//$recentFav  = $this->getMusicUserFav($row_recent_data['music_id'],$data['user_id']);
				//$recentCategoryName  = $this->getCategoryName($row_recent_data['category_id']);
				$articleList['Recents']['recentList'][] = array(
					'article_id' => $row_recent_data['article_id'],
					'article_title' => $row_recent_data['article_title'],
					'article_desc' => $row_recent_data['article_desc'],
					'artist_name' => $row_recent_data['artist_name'],
					'article_image' => base_url() . 'uploads/meditation_articles/' . $row_recent_data['article_image'],
					'artist_image' => base_url() . 'uploads/meditation_articles/' . $row_recent_data['artist_image'],
					'fav' => $favre,
					'create_date' => $row_recent_data['article_created'],
					'category_id' => $data['category_id'],
					'user_id' => $row_recent_data['user_id'],
				);
			}
		}
		// get my music data
		$myMusicCon['sorting'] = array("article_id" => "DESC");
		$myMusicCon['conditions'] = array("user_id" => $data['user_id'], "category_id" => $data['category_id']);
		$myMusicCon['limit'] = $limit;
		$my_music_data  = $this->Api_model->getRows('cp_meditation_articles', $myMusicCon);
		$myMusicTotalCon['conditions'] = array("user_id" => $data['user_id'], "category_id" => $data['category_id']);
		$myMusic_Total_data  = $this->Api_model->getRows('cp_meditation_articles', $myMusicTotalCon);
		if (empty($my_music_data)) {
			$articleList['myArticle']['myArticleCount'] = "0";
			$articleList['myArticle']['myArticleList'] = array();
		} else {
			$articleList['myArticle']['myArticleCount'] = count($myMusic_Total_data);
			foreach ($my_music_data as $row_mymusic_data) {
				$myfav_article = $this->common_model->getsingle("cp_meditation_article_favourite", array("article_id" => $row_mymusic_data['article_id'], 'user_id' => $data['user_id']));
				if (!empty($myfav_article)) {
					$favmy = '1';
				} else {
					$favmy = '0';
				}
				//$myMusicFav  = $this->getMusicUserFav($row_mymusic_data['music_id'],$data['user_id']);
				//$myMusicCategoryName  = $this->getCategoryName($row_mymusic_data['category_id']);
				$articleList['myArticle']['myArticleList'][] = array(
					'article_id' => $row_mymusic_data['article_id'],
					'article_title' => $row_mymusic_data['article_title'],
					'article_desc' => $row_mymusic_data['article_desc'],
					'artist_name' => $row_mymusic_data['artist_name'],
					'article_image' => base_url() . 'uploads/meditation_articles/' . $row_mymusic_data['article_image'],
					'artist_image' => base_url() . 'uploads/meditation_articles/' . $row_mymusic_data['artist_image'],
					'fav' => $favmy,
					'create_date' => $row_mymusic_data['article_created'],
					// 'category_name'=>$popularCategoryName,
					'category_id' => $data['category_id'],
					'user_id' => $row_mymusic_data['user_id'],
				);
			}
		}
		// get favourite music data
		$favMusicCon['sorting'] = array("article_fav_id" => "DESC");
		$favMusicCon['limit'] = $limit;
		$favMusicCon['conditions'] = array("cp_meditation_article_favourite.user_id" => $data['user_id'], "cp_meditation_articles.category_id" => $data['category_id']);
		$fav_music_data  = $this->Api_model->getRowsFavouriteMeditationArticle('cp_meditation_article_favourite', $favMusicCon);
		// print_r("<pre/>");
		//print_r($fav_music_data);
		//  die;
		$favMusicTotalCon['conditions'] = array(
			"cp_meditation_article_favourite.user_id" => $data['user_id'],
			"cp_meditation_articles.category_id" => $data['category_id']
		);
		$favMusic_Total_data  = $this->Api_model->getRowsFavouriteMeditationArticle('cp_meditation_article_favourite', $favMusicTotalCon);
		if (empty($fav_music_data)) {
			$articleList['Favourite']['favCount'] = "0";
			$articleList['Favourite']['favList'] = array();
		} else {
			$articleList['Favourite']['favCount'] = count($favMusic_Total_data);
			foreach ($fav_music_data as $row_favmusic_data) {
				$myfav_article1 = $this->common_model->getsingle("cp_meditation_article_favourite", array("article_id" => $row_favmusic_data['article_id'], 'user_id' => $data['user_id']));
				if (!empty($myfav_article1)) {
					$fav1 = '1';
				} else {
					$fav1 = '0';
				}
				//$favMusicCategoryName  = $this->getCategoryName($row_favmusic_data['category_id']);
				$articleList['Favourite']['favList'][] = array(
					'article_id' => $row_favmusic_data['article_id'],
					'article_title' => $row_favmusic_data['article_title'],
					'article_desc' => $row_favmusic_data['article_desc'],
					'artist_name' => $row_favmusic_data['artist_name'],
					'article_image' => base_url() . 'uploads/meditation_articles/' . $row_favmusic_data['article_image'],
					'artist_image' => base_url() . 'uploads/meditation_articles/' . $row_favmusic_data['artist_image'],
					'fav' => 1,
					'create_date' => $row_favmusic_data['article_created'],
					// 'category_name'=>$popularCategoryName,
					'category_id' => $data['category_id'],
					'user_id' => $row_favmusic_data['user_id'],
				);
			}
		}
		if (!empty($banner_data) or !empty($popular_music_data) or !empty($recent_music_data) or !empty($category_data) or !empty($my_music_data) or !empty($fav_music_data)) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('meditation_article_list' => $articleList));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Meditation Article Data not found', 'response' => array('message' => 'Meditation Article Data not found'));
			// $resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Music Data not found','response' => array('message' => 'Music Data not found'));
		}
		$this->response($resp);
	}
	/*New API start*/
	/*Add new vital sign*/
	public function vital_add_post()
	{
		$data['user_id'] = $this->param["user_id"];
		$data['doctor'] = $this->param["doctor"];
		$data['date'] = $this->param["date"];
		$data['test_type'] = $this->param["test_type"];
		$data['unit'] = $this->param["unit"];
		if (isset($this->param["other"])) {
			$data['other'] = $this->param["other"];
		} else {
			$data['other'] = '';
		}
		if (isset($this->param["testtype_other"])) {
			$data['testtype_other'] = $this->param["testtype_other"];
		} else {
			$data['testtype_other'] = '';
		}
		if (isset($this->param["degree_salcius"])) {
			$data['degree_salcius'] = $this->param["degree_salcius"];
		} else {
			$data['degree_salcius'] = '';
		}
		$response = array();
		if (empty($data['user_id'])) {
			$response['errorcode'] = "500001";
			$response['ok'] = 0;
			$response['message'] = "User id:Required parameter missing";
		} elseif (empty($data['doctor'])) {
			$response['errorcode'] = "500002";
			$response['ok'] = 0;
			$response['message'] = "doctor:Required parameter missing";
		} elseif (empty($data['date'])) {
			$response['errorcode'] = "500003";
			$response['ok'] = 0;
			$response['message'] = "date:Required parameter missing";
		} elseif (empty($data['test_type'])) {
			$response['errorcode'] = "500004";
			$response['ok'] = 0;
			$response['message'] = "test_type:Required parameter missing";
		} elseif (empty($data['unit'])) {
			$response['errorcode'] = "500005";
			$response['ok'] = 0;
			$response['message'] = "unit:Required parameter missing";
		} elseif ($data['doctor'] == "other" && empty($data['other'])) {
			$response['errorcode'] = "500006";
			$response['ok'] = 0;
			$response['message'] = "other:Required parameter missing";
		} elseif (!empty($data)) {
			if ($data['doctor'] == "Other" || $data['doctor'] == "other") {
				$array_otherdoc = array('other_doctor_name' => $data['other'], 'doctor_name' => $data['other'], 'doctor_created' => date("Y-m-d H:i:s"));
				$new_doc = $this->common_model->addRecords('cp_doctor', $array_otherdoc);
				$DoctorDetail = $this->common_model->getSingleRecordById('cp_doctor', array('doctor_id' => $new_doc));
				$DoctorDetail['doctor_name'];
				$doctor = $DoctorDetail['doctor_name'];
				$type = 0;
			} else {
				$doctor = $data['doctor'];
				$type = 1;
			}
			if ($data['test_type'] == "Other" || $data['test_type'] == "other") {
				$array_othertype = array('test_type' => $data['testtype_other'], 'other_test_type' => $data['testtype_other'], 'create_date' => date("Y-m-d H:i:s"));
				$new_type = $this->common_model->addRecords('cp_test_type', $array_othertype);
				$TypeDetail = $this->common_model->getSingleRecordById('cp_test_type', array('id' => $new_type));
				$TypeDetail['test_type'];
				$test_type = $new_type;
				$array_virtualtype = array('test_type' => $data['testtype_other'], 'other_test_type' => $data['testtype_other'], 'create_date' => date("Y-m-d H:i:s"), 'update_date' => date("Y-m-d H:i:s"));
				$new_virtualtype = $this->common_model->addRecords('cp_vital_test_type', $array_virtualtype);
				$unit = (!empty($data['unit']) ? $data['unit'] : '') . ' ' . (!empty($data['degree_salcius']) ? $data['degree_salcius'] : '');
				$array_unittype = array('unit' => $unit, 'test_type_id' => $new_type, 'degree' => (!empty($data['degree_salcius']) ? $data['degree_salcius'] : ''), 'create_date' => date("Y-m-d H:i:s"), 'update_date' => date("Y-m-d H:i:s"));
				$new_unittype = $this->common_model->addRecords('cp_vital_test_type_unit', $array_unittype);
			} else {
				//$test_type = $data['test_type'];
				$TypeDetail = $this->common_model->getSingleRecordById('cp_test_type', array('id' => $data['test_type']));
				$data['testtype_other'] = $TypeDetail['test_type'];
				$UnitDetail = $this->common_model->getSingleRecordById('cp_vital_test_type_unit', array('test_type_id' => $data['test_type']));
				$data['degree_salcius'] = $UnitDetail['degree'];
				$array_othertype = array('test_type' => $data['testtype_other'], 'other_test_type' => $data['testtype_other'], 'create_date' => date("Y-m-d H:i:s"));
				$new_type = $this->common_model->addRecords('cp_test_type', $array_othertype);
				$TypeDetail = $this->common_model->getSingleRecordById('cp_test_type', array('id' => $new_type));
				$TypeDetail['test_type'];
				$test_type = $new_type;
				$array_virtualtype = array('test_type' => $data['testtype_other'], 'other_test_type' => $data['testtype_other'], 'create_date' => date("Y-m-d H:i:s"), 'update_date' => date("Y-m-d H:i:s"));
				$new_virtualtype = $this->common_model->addRecords('cp_vital_test_type', $array_virtualtype);
				$unit = (!empty($data['unit']) ? $data['unit'] : '') . ' ' . (!empty($data['degree_salcius']) ? $data['degree_salcius'] : '');
				$array_unittype = array('unit' => $unit, 'test_type_id' => $new_type, 'degree' => (!empty($data['degree_salcius']) ? $data['degree_salcius'] : ''), 'create_date' => date("Y-m-d H:i:s"), 'update_date' => date("Y-m-d H:i:s"));
				$new_unittype = $this->common_model->addRecords('cp_vital_test_type_unit', $array_unittype);
			}
			/*$test_type = $data['test_type'];*/
			/*$TestTypeData = $this->common_model->getSingleRecordById('cp_vital_test_type', array('id' => $test_type));*/
			$TestTypeData = $this->common_model->getSingleRecordById('cp_test_type', array('id' => $test_type));
			if (!empty($TestTypeData)) {
				$test_typename	= $TestTypeData['test_type'];
			} else {
				$test_typename	= (!empty($TypeDetail['test_type']) ? $TypeDetail['test_type'] : '');
			}
			$TestUnitData = $this->common_model->getSingleRecordById('cp_vital_test_type_unit', array('test_type_id' => $test_type));
			if (!empty($TestUnitData)) {
				$unitName	= $TestUnitData['unit'];
			}
			if ($data['test_type'] == "Other" || $data['test_type'] == "other") {
				$unitName = $data['unit'] . ' ' . (!empty($data['degree_salcius']) ? $data['degree_salcius'] : '');
			}
			$vitaSignData = array(
				'user_id' => $data['user_id'],
				'doctor' => $doctor,
				'date' => (!empty($data['date']) ? date('Y-m-d', strtotime($data['date'])) : ''),
				'test_type' => $test_typename,
				'type' => $type,
				/*'unit'=> $data['unit'].' '.$unitName,*/
				'unit' => $data['unit'],
				//'unit'=> $unitName,
				'create_date' => date('m-d-Y'),
				'create_at' => date('Y-m-d H:i:s')
			);
			$reminderId = $this->common_model->addRecords('cp_vital_sign', $vitaSignData);
			if ($reminderId) {
				$reminderData = $this->common_model->getSingleRecordById('cp_vital_sign', array('vital_sign_id' => $reminderId));
				// if($data['doctor']=="other"){
				//   $Selectdoctor=0;
				// }else{
				//   $Selectdoctor=1;
				// }
				$reminderData1 = array(
					'id' => $reminderData['vital_sign_id'],
					'date' => (!empty($reminderData['date']) ? date('m-d-Y', strtotime($reminderData['date'])) : ''),
					'doctor' => $reminderData['doctor'],
					//'choose_doctor'=>$Selectdoctor,
					'unit' => $reminderData['unit'],
					'user_id' => $reminderData['user_id'],
					'test_type' => $reminderData['test_type'],
					'type' => $reminderData['type'],
					'create_at' => $reminderData['create_at']
					//'update_at'=> $reminderData['update_at']
				);
				$response['message'] = "Vital sign added successfully.";
				$response['ok'] = 1;
				$response['data'] = array('Vita_detail' => $reminderData1);
			}
		} else {
			$response['errorcode'] = "500007";
			$response['message'] = "Please provide the required parameters!";
			$response['ok'] = 0;
		}
		echo json_encode($response);
	}
	/*Edit vital sign*/
	public function vital_edit_post()
	{
		$data['user_id'] = $this->param["user_id"];
		$data['vital_id'] = $this->param["vital_id"];
		$data['doctor'] = $this->param["doctor"];
		$data['date'] = $this->param["date"];
		$data['test_type'] = $this->param["test_type"];
		$data['unit'] = $this->param["unit"];
		if (isset($this->param["other"])) {
			$data['other'] = $this->param["other"];
		} else {
			$data['other'] = '';
		}
		if (isset($this->param["testtype_other"])) {
			$data['testtype_other'] = $this->param["testtype_other"];
		} else {
			$data['testtype_other'] = '';
		}
		if (isset($this->param["degree_salcius"])) {
			$data['degree_salcius'] = $this->param["degree_salcius"];
		} else {
			$data['degree_salcius'] = '';
		}
		$response = array();
		if (empty($data['user_id'])) {
			$response['errorcode'] = "500001";
			$response['ok'] = 0;
			$response['message'] = "User id:Required parameter missing";
		} elseif (empty($data['vital_id'])) {
			$response['errorcode'] = "500002";
			$response['ok'] = 0;
			$response['message'] = "vital_id:Required parameter missing";
		} elseif (empty($data['doctor'])) {
			$response['errorcode'] = "500003";
			$response['ok'] = 0;
			$response['message'] = "doctor:Required parameter missing";
		} elseif (empty($data['date'])) {
			$response['errorcode'] = "500004";
			$response['ok'] = 0;
			$response['message'] = "date:Required parameter missing";
		} elseif (empty($data['test_type'])) {
			$response['errorcode'] = "500005";
			$response['ok'] = 0;
			$response['message'] = "test_type:Required parameter missing";
		} elseif (empty($data['unit'])) {
			$response['errorcode'] = "500006";
			$response['ok'] = 0;
			$response['message'] = "unit:Required parameter missing";
		} elseif ($data['doctor'] == "other" && empty($data['other'])) {
			$response['errorcode'] = "500007";
			$response['ok'] = 0;
			$response['message'] = "other:Required parameter missing";
		} elseif (!empty($data)) {
			if ($data['doctor'] == "other" || $data['doctor'] == "Other") {
				$array_otherdoc = array('other_doctor_name' => $data['other'], 'doctor_name' => $data['other'], 'doctor_created' => date("Y-m-d H:i:s"));
				$new_doc = $this->common_model->addRecords('cp_doctor', $array_otherdoc);
				$DoctorDetail = $this->common_model->getSingleRecordById('cp_doctor', array(' doctor_id' => $new_doc));
				$DoctorDetail['doctor_name'];
				$doctor = $DoctorDetail['doctor_name'];
				$type = 0;
			} else {
				$doctor = $data['doctor'];
				$type = 1;
			}
			if ($data['test_type'] == "Other" || $data['test_type'] == "other") {
				$array_othertype = array('test_type' => $data['testtype_other'], 'other_test_type' => $data['testtype_other'], 'create_date' => date("Y-m-d H:i:s"));
				$new_type = $this->common_model->addRecords('cp_test_type', $array_othertype);
				$TypeDetail = $this->common_model->getSingleRecordById('cp_test_type', array('id' => $new_type));
				$TypeDetail['test_type'];
				$test_type = $new_type;
				$array_virtualtype = array('test_type' => $data['testtype_other'], 'other_test_type' => $data['testtype_other'], 'create_date' => date("Y-m-d H:i:s"), 'update_date' => date("Y-m-d H:i:s"));
				$new_virtualtype = $this->common_model->addRecords('cp_vital_test_type', $array_virtualtype);
				$unit = (!empty($data['unit']) ? $data['unit'] : '') . ' ' . (!empty($data['degree_salcius']) ? $data['degree_salcius'] : '');
				$array_unittype = array('unit' => $unit, 'test_type_id' => $new_type, 'degree' => (!empty($data['degree_salcius']) ? $data['degree_salcius'] : ''), 'create_date' => date("Y-m-d H:i:s"), 'update_date' => date("Y-m-d H:i:s"));
				$new_unittype = $this->common_model->addRecords('cp_vital_test_type_unit', $array_unittype);
			} else {
				//$test_type = $data['test_type'];
				$TypeDetail = $this->common_model->getSingleRecordById('cp_test_type', array('id' => $data['test_type']));
				$data['testtype_other'] = $TypeDetail['test_type'];
				$UnitDetail = $this->common_model->getSingleRecordById('cp_vital_test_type_unit', array('test_type_id' => $data['test_type']));
				$data['degree_salcius'] = $UnitDetail['degree'];
				$array_othertype = array('test_type' => $data['testtype_other'], 'other_test_type' => $data['testtype_other'], 'create_date' => date("Y-m-d H:i:s"));
				$new_type = $this->common_model->addRecords('cp_test_type', $array_othertype);
				$TypeDetail = $this->common_model->getSingleRecordById('cp_test_type', array('id' => $new_type));
				$TypeDetail['test_type'];
				$test_type = $new_type;
				$array_virtualtype = array('test_type' => $data['testtype_other'], 'other_test_type' => $data['testtype_other'], 'create_date' => date("Y-m-d H:i:s"), 'update_date' => date("Y-m-d H:i:s"));
				$new_virtualtype = $this->common_model->addRecords('cp_vital_test_type', $array_virtualtype);
				$unit = (!empty($data['unit']) ? $data['unit'] : '') . ' ' . (!empty($data['degree_salcius']) ? $data['degree_salcius'] : '');
				$array_unittype = array('unit' => $unit, 'test_type_id' => $new_type, 'degree' => (!empty($data['degree_salcius']) ? $data['degree_salcius'] : ''), 'create_date' => date("Y-m-d H:i:s"), 'update_date' => date("Y-m-d H:i:s"));
				$new_unittype = $this->common_model->addRecords('cp_vital_test_type_unit', $array_unittype);
			}
			/*$test_type = $data['test_type'];*/
			/*$TestTypeData = $this->common_model->getSingleRecordById('cp_vital_test_type', array(' id' => $test_type));*/
			$TestTypeData = $this->common_model->getSingleRecordById('cp_test_type', array('id' => $test_type));
			if (!empty($TestTypeData)) {
				$test_typename	= $TestTypeData['test_type'];
			} else {
				$test_typename	= (!empty($TypeDetail['test_type']) ? $TypeDetail['test_type'] : '');
			}
			$TestUnitData = $this->common_model->getSingleRecordById('cp_vital_test_type_unit', array('test_type_id' => $test_type));
			if (!empty($TestUnitData)) {
				$unitName	= $TestUnitData['unit'];
			}
			if ($data['test_type'] == "Other" || $data['test_type'] == "other") {
				$unitName = $data['unit'] . ' ' . (!empty($data['degree_salcius']) ? $data['degree_salcius'] : '');
			}
			$vitaSignData = array(
				'user_id' => $data['user_id'],
				'doctor' => $doctor,
				'date' => (!empty($data['date']) ? date('Y-m-d', strtotime($data['date'])) : ''),
				'test_type' => $test_typename,
				'type' => $type,
				'unit' => $data['unit'],
				//'unit'=> $unitName,
				'update_date' => date('m-d-Y'),
				'update_at' => date('Y-m-d H:i:s')
			);
			$user_id = $data['user_id'];
			$vital_id = $data['vital_id'];
			$result = $this->Api_model->UpdateDataByUserId($user_id, $vital_id, $vitaSignData);
			if ($result) {
				$reminderData = $this->common_model->getSingleRecordById('cp_vital_sign', array('vital_sign_id' => $vital_id));
				// if($data['doctor']=="other"){
				//   $Selectdoctor=0;
				// }else{
				//   $Selectdoctor=1;
				// }
				$reminderData1 = array(
					'id' => $reminderData['vital_sign_id'],
					'date' => (!empty($reminderData['date']) ? date('m-d-Y', strtotime($reminderData['date'])) : ''),
					'doctor' => $reminderData['doctor'],
					//'choose_doctor'=>$Selectdoctor,
					'unit' => $reminderData['unit'],
					'user_id' => $reminderData['user_id'],
					'test_type' => $reminderData['test_type'],
					'type' => $reminderData['type'],
					//'create_at' => $reminderData['create_at']
					'update_at' => $reminderData['update_at']
				);
				$response['message'] = "Vital sign updated successfully.";
				$response['ok'] = 1;
				$response['data'] = array('Vita_detail' => $reminderData1);
			}
		} else {
			$response['errorcode'] = "500007";
			$response['message'] = "Please provide the required parameters!";
			$response['ok'] = 0;
		}
		echo json_encode($response);
	}
	
	/*Vital sign share and newadd*/
	public function vital_sign_share_post()
	{	
		
		$user_id			= isset($this->param["user_id"]) ? $this->param["user_id"] : '';
		$vital_id 			= isset($this->param["vital_id"]) ? $this->param["vital_id"] : '';
		$vital_user_email	= isset($this->param["vital_user_email"]) ? $this->param["vital_user_email"] : '';
		
		/*Start single user sent viatal data via email*/

		if(empty($user_id) && $user_id =='' || empty($vital_id)  || $vital_id =='' && empty($vital_user_email)  || $vital_user_email ==''){
			
			$VitalSignData = array();
			$response['message'] = "user_id:vital_id:vital_user_email:Required parameter missing ";
			$response['error'] = 0;
			$response['ok'] = 400;
			$response['data'] = array('Vita_detail' => $VitalSignData);
			echo (json_encode($response));
			exit;
		}else{
			if(!empty($user_id) && !empty($vital_id) && !empty($vital_user_email)){
				
				$arr_vitaldata = $this->Api_model->GetvitalDataByVitalIdUserId($user_id,$vital_id);
				if(!empty($arr_vitaldata) && $arr_vitaldata !=''){
					
					foreach ($arr_vitaldata as $k) {
						
						if(isset($k['user_id']) && $k['user_id'] !=''){
							$user_id = $k['user_id'];
						}
						if(isset($k['vital_sign_id']) && $k['vital_sign_id'] !=''){
							$vital_sign_id = $k['vital_sign_id'];
						}
						if(isset($k['vital_user_email']) && $k['vital_user_email'] !=''){
							$vital_user_email = trim($k['vital_user_email']);
						}
						if(isset($k['date']) && $k['date'] !=''){
							$date = date('m-d-Y', strtotime($k['date']));
						}
							
						$response[] = array(
							
							'vital_user_id'		 	=>	$user_id,
							'vital_vital_sign_id'	=>	$vital_sign_id,
							'vital_user_email'		=>	$vital_user_email,
							'vital_date' 			=>	$date,
							'vital_doctor'			=>	$k['doctor'],
							'vital_unit'			=>	$k['unit'],
							'vital_test_type'		=>	$k['test_type'],
							'vital_create_date'		=>	$k['create_date'],
							'vital_type'			=>	$k['type'],
							'vital_update_date'		=>	$k['update_date'],
							'vital_create_at'		=>	$k['create_at'],
							'vital_update_at'		=>	$k['update_at']
						);
						
						/*Start email sent code*/
						if (!empty($user_id) && !empty($vital_sign_id) && !empty($vital_user_email)) {
							
							$subject = "Vital Update";
							$message = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
							<html xmlns='http://www.w3.org/1999/xhtml'>
							<head>
								<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
								<title>Care Pro</title>
								<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>
							</head>
													<body style='margin: 0;font-family:'open sans';'>
							<div style='max-width: 700px;margin: 30px auto 0; background:#f4f4f4;'>
								<div style='border:none;padding:10px 0; text-align:center;background:#117bc1; text-align:center;'>
									<a href='#' target='_blank' style='display:inline-block;padding:20px 0;margin: 0 auto 0px;'>
									<img style='max-width:200px;width:100%;margin: 0 auto;' src='http://mobivdigital.com/carepro/assets/images/logo_img.png'>
									</a>
								</div>
								<div style='border:none; padding:0px 60px; max-width:580px; margin:-40px auto 0px; border-radius:0px;background:url(http://mobivdigital.com/carepro/assets/images/bg_em.jpg);background-repeat:no-repeat;'>
									<div class='temp_cont' style='width:100%; margin:0 auto;'>
										<div style='padding:15px;background: rgba(255, 255, 255, 1); border-radius:4px; margin-bottom:20px; border: 1px solid #cccccc6e;'>
										<h2 style='font-size:25px;color:#7cbe49;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
											<span style='display:block;width:100%;'>
											<img style='max-width:50px;width:100%;margin:0 0 10px;' src='http://mobivdigital.com/carepro/assets/images/confirm.png'>
											</span>
											Vital details
										</h2>
										<h3 style='font-size:20px;color:#333;text-align:center;font-weight:600;padding:0 0 10px;margin:0;'>
											Hi &nbsp;" . $k['doctor'] . ",
										</h3>
										<style>
											#customers td, #customers th {
											border:1px solid #ddd;
											padding:8px;
											}
											#customers th {
											padding-top:8px;
											padding-bottom:8px;
											text-align:left;
											background-color:#4CAF50;
											color:white;
											}
										</style>
										<table id='customers' style='width:100%; border-collapse: collapse;'>
											<tr>
												<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Vital User Id:</th>
												<td style='padding:8px;border:1px solid #ddd;'>" . $user_id . "</td>
											</tr>
											<tr>
												<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Vital Id:</th>
												<td style='padding:8px;border:1px solid #ddd;'>" . $vital_sign_id . "</td>
											</tr>
											<tr>
												<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Vital User Email:</th>
												<td style='padding:8px;border:1px solid #ddd;'>" . $vital_user_email . "</td>
											</tr>
											<tr>
												<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Vital Type:</th>
												<td style='padding:8px;border:1px solid #ddd;'>" . $k['type'] . "</td>
											</tr>
											<tr>
												<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Vital Create Date:</th>
												<td style='font-size:13px; padding:8px;border:1px solid #ddd;'>" . $k['create_date'] . "</td>
											</tr>
											<tr>
												<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Vital Update Date:</th>
												<td style='font-size:13px; padding:8px;border:1px solid #ddd;'>" . $k['update_date'] . "</td>
											</tr>
											<tr>
												<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Vital Create At:</th>
												<td style='font-size:13px; padding:8px;border:1px solid #ddd;'>" . $k['create_at'] . "</td>
											</tr>
											<tr>
												<th style='width:200px; font-size:15px; padding:8px;padding:8px; background-color:#4CAF50; border: 1px solid #ddd; text-align:left; color:#fff;'>Vital Update At:</th>
												<td style='font-size:13px; padding:8px;border:1px solid #ddd;'>" . $k['update_at'] . "</td>
											</tr>
										</table>
										</div>
										<div style='background:#2f8dccdb;padding:13px 10px 13px;color:#fff;text-align:center; border-radius:3px;margin-bottom:25px;display:inline-block;width:100%; box-sizing:border-box;'>
										<p style='font-size: 13px;margin: 0 0 8px;'>Thank You for visit</p>
										<p style='font-size: 13px;margin:0;'>
											<a style='color:#fff;font-weight:600;text-decoration:none;' href='http://mobivdigital.com/carepro/' target=''>http://carepro.com</a>
										</p>
										<div style='display:block;height: 1px;background:#fff;margin:10px 0;'></div>
										<p style='font-size: 13px;margin:0 0 8px;'>Care Pro</p>
										<p style='font-size: 13px;margin:0;'>
											<a style='color:#fff;font-weight:600;text-decoration:none;' href='javascript:void(0)'>EMAIL: info@carepro.com/</a>
										</p>
										</div>
									</div>
								</div>
							</div>
							</body>
							
							</html>";
							$chk_mail2 = new_send_mail($message, $subject,$vital_user_email,$imageArray='');
						}
						/*End email sent code*/
					}
					
					$response['message'] = "Mail Has Been Sent Successfully";
					$response['ok'] = 1;
					$response['data'] = array('Vita_detail' => $response);
					echo (json_encode($response['data']));
					exit;
				}else{
					$VitalSignData = array();
					$response['message'] = "These user_id & vital_id are not available in records";
					$response['error'] = 0;
					$response['ok'] = 400;
					$response['data'] = array('Vita_detail' => $VitalSignData);
					echo (json_encode($response));
					exit;
				}
			}
		}

		
		/*End single user sent viatal data via email*/
	}
	
	/*Vital sign list by filter by date and test type*/
	public function vital_sign_filter_list_post()
	{
		//$data = $this->param;
		$data['user_id'] = $this->param["user_id"];
		$data['from_date'] = $this->param["from_date"];
		$data['to_date'] = $this->param["to_date"];
		$data['test_type_filter'] = $this->param["test_type_filter"];
		$response = array();
		if (empty($data['user_id'])) {
			$response['errorcode'] = "500001";
			$response['ok'] = 0;
			$response['message'] = "User id:Required parameter missing";
		} elseif (!empty($data['from_date']) && empty($data['to_date'])) {
			$response['errorcode'] = "500002";
			$response['ok'] = 0;
			$response['message'] = "To date :Required parameter missing";
		} elseif (empty($data['from_date']) && !empty($data['to_date'])) {
			$response['errorcode'] = "500002";
			$response['ok'] = 0;
			$response['message'] = "From date :Required parameter missing";
		} elseif (!empty($data)) {
			$user_id = $data['user_id'];
			if (!empty($data['from_date']) && !empty($data['to_date'])) {
				$from_date = $data['from_date'];
				$to_date = $data['to_date'];
			} else {
				$from_date = '';
				$to_date = '';
			}
			if (!empty($data['test_type_filter'])) {
				$test_type_filter = $data['test_type_filter'];
			} else {
				$test_type_filter = '';
			}
			$VitalData = $this->Api_model->GetvitalDataByFilter($user_id, $from_date, $to_date, $test_type_filter);
			// echo "<pre>";
			// print_r($VitalData);
			// die;
			if (!empty($VitalData)) {
				if (empty($VitalData)) {
					$VitalSignData['VitalSignData'] = array();
				}
				$VitalSignData = array();
				foreach ($VitalData as $vital) {
					$VitalSignData[] = array(
						'id' => $vital['vital_sign_id'],
						'date' => (!empty($vital['date']) ? date('m-d-Y', strtotime($vital['date'])) : ''),
						'doctor' => $vital['doctor'],
						'unit' => $vital['unit'],
						'user_id' => $vital['user_id'],
						'test_type' => $vital['test_type'],
						'type' => $vital['type'],
						'create_at' => $vital['create_at'],
						'update_at' => $vital['update_at']
					);
				}
				$response['message'] = "Vital sign list.";
				$response['ok'] = 1;
				$response['data'] = array('Vita_detail' => $VitalSignData);
			} else {
				$VitalSignData = array();
				$response['message'] = "No data found!";
				$response['ok'] = 1;
				$response['data'] = array('Vita_detail' => $VitalSignData);
			}
		} else {
			$response['errorcode'] = "500007";
			$response['message'] = "Please provide the required parameters!";
			$response['ok'] = 0;
		}
		echo json_encode($response);
	}
	/*Delete vital*/
	public function vital_delete_post()
	{
		$data['user_id'] = $this->param["user_id"];
		$data['vital_id'] = $this->param["vital_id"];
		$response = array();
		if (empty($data['user_id'])) {
			$response['errorcode'] = "500001";
			$response['ok'] = 0;
			$response['message'] = "User id:Required parameter missing";
		} elseif (empty($data['vital_id'])) {
			$response['errorcode'] = "500002";
			$response['ok'] = 0;
			$response['message'] = "Vital Id :Required parameter missing";
		} elseif (!empty($data)) {
			$user_id = $data['user_id'];
			$vital_id = $data['vital_id'];
			$VitalData = $this->Api_model->GetvitalDataByVitalIdUserId($user_id, $vital_id);
			if (!empty($VitalData)) {
				$result = $this->Api_model->DeleteViatalById($user_id, $vital_id);
				if ($result > 0) {
					$response['message'] = "Vital sign deleted successfully.";
					$response['ok'] = 1;
				}
			} else {
				$response['message'] = "No data found with this vital id and user id!";
				$response['ok'] = 1;
			}
		} else {
			$response['errorcode'] = "500003";
			$response['message'] = "Please provide the required parameters!";
			$response['ok'] = 0;
		}
		echo json_encode($response);
	}
	/*Vital sign list by filter by date and test type*/
	public function article_filter_list_post()
	{
		$data['user_id'] = $this->param["user_id"];
		$data['category_id'] = $this->param["category_id"];
		$data['filter_by'] = $this->param["filter_by"];
		$response = array();
		if (empty($data['user_id'])) {
			$response['errorcode'] = "500001";
			$response['ok'] = 0;
			$response['message'] = "User id:Required parameter missing";
		} elseif (empty($data['category_id'])) {
			$response['errorcode'] = "500002";
			$response['ok'] = 0;
			$response['message'] = "Category id :Required parameter missing";
		} elseif (empty($data['filter_by'])) {
			$response['errorcode'] = "500002";
			$response['ok'] = 0;
			$response['message'] = "Filter by :Required parameter missing";
		} elseif (!empty($data)) {
			if ($data['filter_by'] == 'populars' || $data['filter_by'] == 'Populars') {
				$limit = 10;
				// get popular article data
				$popularCon['sorting'] = array("article_view" => "DESC");
				$popularCon['limit'] = $limit;
				$popularCon['conditions'] = array("category_id" => $data['category_id']);
				$popular_music_data  = $this->Api_model->getRows('cp_articles', $popularCon);
				$popularTotalCon['conditions'] = array("category_id" => $data['category_id']);
				$popular_Total_data  = $this->Api_model->getRows('cp_articles', $popularTotalCon);
				if (!empty($popular_music_data)) {
					if (empty($popular_music_data)) {
						$popular_music_data['Populars'] = array();
					}
					$articleList['Populars']['popularCount'] = count($popular_Total_data);
					foreach ($popular_music_data as $row_popular_data) {
						$fav_article = $this->common_model->getsingle("cp_article_favourite", array("article_id" => $row_popular_data['article_id'], 'user_id' => $data['user_id']));
						if (!empty($fav_article)) {
							$fav = '1';
						} else {
							$fav = '0';
						}
						$articleList['Populars']['popularList'][] = array(
							'article_id' => $row_popular_data['article_id'],
							'article_title' => $row_popular_data['article_title'],
							'article_desc' => $row_popular_data['article_desc'],
							'artist_name' => $row_popular_data['artist_name'],
							'article_image' => base_url() . 'uploads/articles/' . $row_popular_data['article_image'],
							'artist_image' => base_url() . 'uploads/articles/' . $row_popular_data['artist_image'],
							'fav' => $fav,
							'create_date' => $row_popular_data['article_created'],
							// 'category_name'=>$popularCategoryName,
							'user_id' => $row_popular_data['user_id'],
							'category_id' => $data['category_id'],
						);
					}
					$response['message'] = "Populars article list.";
					$response['ok'] = 1;
					$response['data'] = array('article list' => $articleList);
				} else {
					$response['message'] = "No data found!";
					$response['ok'] = 1;
					$response['data'] = array('article list' => $articleList);
				}
			} else if ($data['filter_by'] == 'recents' || $data['filter_by'] == 'Recents') {
				// echo "Recent";
				// die;
				/*test*/
				$recentCon['sorting'] = array("article_id" => "DESC");
				$recentCon['limit'] = $limit;
				$recentCon['conditions'] = array("category_id" => $data['category_id']);
				$recent_music_data  = $this->Api_model->getRows('cp_articles', $recentCon);
				$recentTotalCon['conditions'] = array("category_id" => $data['category_id']);
				$recent_Total_data  = $this->Api_model->getRows('cp_articles', $recentTotalCon);
				if (!empty($recent_music_data)) {
					if (empty($recent_music_data)) {
						$recent_music_data['Recents'] = array();
					}
					$articleList['Recents']['recentCount'] = count($recent_Total_data);
					foreach ($recent_music_data as $row_recent_data) {
						$refav_article = $this->common_model->getsingle("cp_article_favourite", array("article_id" => $row_recent_data['article_id'], 'user_id' => $data['user_id']));
						if (!empty($refav_article)) {
							$favre = '1';
						} else {
							$favre = '0';
						}
						$articleList['Recents']['recentList'][] = array(
							'article_id' => $row_recent_data['article_id'],
							'article_title' => $row_recent_data['article_title'],
							'article_desc' => $row_recent_data['article_desc'],
							'artist_name' => $row_recent_data['artist_name'],
							'article_image' => base_url() . 'uploads/articles/' . $row_recent_data['article_image'],
							'artist_image' => base_url() . 'uploads/articles/' . $row_recent_data['artist_image'],
							'fav' => $favre,
							'create_date' => $row_recent_data['article_created'],
							'category_id' => $data['category_id'],
							'user_id' => $row_recent_data['user_id'],
						);
					}
					$response['message'] = "Recents article list.";
					$response['ok'] = 1;
					$response['data'] = array('article list' => $articleList);
				} else {
					$response['message'] = "No data found!";
					$response['ok'] = 1;
					$response['data'] = array('article list' => $articleList);
				}
				/*hello*/
			} elseif ($dataata['filter_by'] == 'myArticle' || $data['filter_by'] == 'myArticle') {
				// echo "Myarticle";
				// die;
				$myMusicCon['sorting'] = array("article_id" => "DESC");
				$myMusicCon['conditions'] = array("user_id" => $data['user_id'], "category_id" => $data['category_id']);
				$myMusicCon['limit'] = $limit;
				$my_music_data  = $this->Api_model->getRows('cp_articles', $myMusicCon);
				$myMusicTotalCon['conditions'] = array("user_id" => $data['user_id'], "category_id" => $data['category_id']);
				$myMusic_Total_data  = $this->Api_model->getRows('cp_articles', $myMusicTotalCon);
				if (!empty($my_music_data)) {
					if (empty($my_music_data)) {
						$articleList['myArticle']['myArticleCount'] = "0";
						$articleList['myArticle']['myArticleList'] = array();
					}
					$articleList['myArticle']['myArticleCount'] = count($myMusic_Total_data);
					foreach ($my_music_data as $row_mymusic_data) {
						$myfav_article = $this->common_model->getsingle("cp_article_favourite", array("article_id" => $row_mymusic_data['article_id'], 'user_id' => $data['user_id']));
						if (!empty($myfav_article)) {
							$favmy = '1';
						} else {
							$favmy = '0';
						}
						$articleList['myArticle']['myArticleList'][] = array(
							'article_id' => $row_mymusic_data['article_id'],
							'article_title' => $row_mymusic_data['article_title'],
							'article_desc' => $row_mymusic_data['article_desc'],
							'artist_name' => $row_mymusic_data['artist_name'],
							'article_image' => base_url() . 'uploads/articles/' . $row_mymusic_data['article_image'],
							'artist_image' => base_url() . 'uploads/articles/' . $row_mymusic_data['artist_image'],
							'fav' => $favmy,
							'create_date' => $row_mymusic_data['article_created'],
							// 'category_name'=>$popularCategoryName,
							'category_id' => $data['category_id'],
							'user_id' => $row_mymusic_data['user_id'],
						);
					}
					$response['message'] = "My article list.";
					$response['ok'] = 1;
					$response['data'] = array('article list' => $articleList);
				} else {
					$response['message'] = "No data found!";
					$response['ok'] = 1;
					$response['data'] = array('article list' => $articleList);
				}
			} elseif ($dataata['filter_by'] == 'favourite' || $data['filter_by'] == 'Favourite') {
				// echo "Favourite";
				// die;
				$favMusicCon['sorting'] = array("article_fav_id" => "DESC");
				$favMusicCon['limit'] = $limit;
				$favMusicCon['conditions'] = array("cp_article_favourite.user_id" => $data['user_id'], "cp_articles.category_id" => $data['category_id']);
				$fav_music_data  = $this->Api_model->getRowsFavouriteArticle('cp_article_favourite', $favMusicCon);
				$favMusicTotalCon['conditions'] = array(
					"cp_article_favourite.user_id" => $data['user_id'],
					"cp_articles.category_id" => $data['category_id']
				);
				$favMusic_Total_data  = $this->Api_model->getRowsFavouriteArticle('cp_article_favourite', $favMusicTotalCon);
				if (!empty($fav_music_data)) {
					if (empty($fav_music_data)) {
						$articleList['Favourite']['favCount'] = "0";
						$articleList['Favourite']['favList'] = array();
					}
					$articleList['Favourite']['favCount'] = count($favMusic_Total_data);
					foreach ($fav_music_data as $row_favmusic_data) {
						$myfav_article1 = $this->common_model->getsingle("cp_article_favourite", array("article_id" => $row_favmusic_data['article_id'], 'user_id' => $data['user_id']));
						if (!empty($myfav_article1)) {
							$fav1 = '1';
						} else {
							$fav1 = '0';
						}
						$articleList['Favourite']['favList'][] = array(
							'article_id' => $row_favmusic_data['article_id'],
							'article_title' => $row_favmusic_data['article_title'],
							'article_desc' => $row_favmusic_data['article_desc'],
							'artist_name' => $row_favmusic_data['artist_name'],
							'article_image' => base_url() . 'uploads/articles/' . $row_favmusic_data['article_image'],
							'artist_image' => base_url() . 'uploads/articles/' . $row_favmusic_data['artist_image'],
							'fav' => 1,
							'create_date' => $row_favmusic_data['article_created'],
							// 'category_name'=>$popularCategoryName,
							'category_id' => $data['category_id'],
							'user_id' => $row_favmusic_data['user_id'],
						);
					}
					$response['message'] = "Favourite article list.";
					$response['ok'] = 1;
					$response['data'] = array('article list' => $articleList);
				} else {
					$response['message'] = "No data found!";
					$response['ok'] = 1;
					$response['data'] = array('article list' => $articleList);
				}
			}
		} else {
			$response['errorcode'] = "500007";
			$response['message'] = "Please provide the required parameters!";
			$response['ok'] = 0;
		}
		echo json_encode($response);
	}
	/*Vital sign list by filter by date and test type*/
	public function meditation_article_filter_list_post()
	{
		$data['user_id'] = $this->param["user_id"];
		$data['category_id'] = $this->param["category_id"];
		$data['filter_by'] = $this->param["filter_by"];
		$response = array();
		if (empty($data['user_id'])) {
			$response['errorcode'] = "500001";
			$response['ok'] = 0;
			$response['message'] = "User id:Required parameter missing";
		} elseif (empty($data['category_id'])) {
			$response['errorcode'] = "500002";
			$response['ok'] = 0;
			$response['message'] = "Category id :Required parameter missing";
		} elseif (empty($data['filter_by'])) {
			$response['errorcode'] = "500002";
			$response['ok'] = 0;
			$response['message'] = "Filter by :Required parameter missing";
		} elseif (!empty($data)) {
			if ($data['filter_by'] == 'populars' || $data['filter_by'] == 'Populars') {
				$limit = 10;
				// get popular article data
				$popularCon['sorting'] = array("article_view" => "DESC");
				$popularCon['limit'] = $limit;
				$popularCon['conditions'] = array("category_id" => $data['category_id']);
				$popular_music_data  = $this->Api_model->getRows('cp_meditation_articles', $popularCon);
				$popularTotalCon['conditions'] = array("category_id" => $data['category_id']);
				$popular_Total_data  = $this->Api_model->getRows('cp_meditation_articles', $popularTotalCon);
				if (!empty($popular_music_data)) {
					if (empty($popular_music_data)) {
						$popular_music_data['Populars'] = array();
					}
					$articleList['Populars']['popularCount'] = count($popular_Total_data);
					foreach ($popular_music_data as $row_popular_data) {
						$fav_article = $this->common_model->getsingle("cp_article_favourite", array("article_id" => $row_popular_data['article_id'], 'user_id' => $data['user_id']));
						if (!empty($fav_article)) {
							$fav = '1';
						} else {
							$fav = '0';
						}
						$articleList['Populars']['popularList'][] = array(
							'article_id' => $row_popular_data['article_id'],
							'article_title' => $row_popular_data['article_title'],
							'article_desc' => $row_popular_data['article_desc'],
							'artist_name' => $row_popular_data['artist_name'],
							'article_image' => base_url() . 'uploads/meditation_articles/' . $row_popular_data['article_image'],
							'artist_image' => base_url() . 'uploads/	meditation_articles/' . $row_popular_data['artist_image'],
							'fav' => $fav,
							'create_date' => $row_popular_data['article_created'],
							// 'category_name'=>$popularCategoryName,
							'user_id' => $row_popular_data['user_id'],
							'category_id' => $data['category_id'],
						);
					}
					$response['message'] = "Populars meditation article list.";
					$response['ok'] = 1;
					$response['data'] = array('meditation article list' => $articleList);
				} else {
					$response['message'] = "No data found!";
					$response['ok'] = 1;
					$response['data'] = array('meditation article list' => $articleList);
				}
			} else if ($data['filter_by'] == 'recents' || $data['filter_by'] == 'Recents') {
				$limit = 10;
				// echo "Recent";														
				// die;
				/*test*/
				$recentCon['sorting'] = array("article_id" => "DESC");
				$recentCon['limit'] = $limit;
				$recentCon['conditions'] = array("category_id" => $data['category_id']);
				$recent_music_data  = $this->Api_model->getRows(' cp_meditation_articles', $recentCon);
				$recentTotalCon['conditions'] = array("category_id" => $data['category_id']);
				$recent_Total_data  = $this->Api_model->getRows(' cp_meditation_articles', $recentTotalCon);
				if (!empty($recent_music_data)) {
					if (empty($recent_music_data)) {
						$recent_music_data['Recents'] = array();
					}
					$articleList['Recents']['recentCount'] = count($recent_Total_data);
					foreach ($recent_music_data as $row_recent_data) {
						$refav_article = $this->common_model->getsingle("cp_article_favourite", array("article_id" => $row_recent_data['article_id'], 'user_id' => $data['user_id']));
						if (!empty($refav_article)) {
							$favre = '1';
						} else {
							$favre = '0';
						}
						$articleList['Recents']['recentList'][] = array(
							'article_id' => $row_recent_data['article_id'],
							'article_title' => $row_recent_data['article_title'],
							'article_desc' => $row_recent_data['article_desc'],
							'artist_name' => $row_recent_data['artist_name'],
							'article_image' => base_url() . 'uploads/meditation_articles/' . $row_recent_data['article_image'],
							'artist_image' => base_url() . 'uploads/meditation_articles/' . $row_recent_data['artist_image'],
							'fav' => $favre,
							'create_date' => $row_recent_data['article_created'],
							'category_id' => $data['category_id'],
							'user_id' => $row_recent_data['user_id'],
						);
					}
					$response['message'] = "Recents meditation article list.";
					$response['ok'] = 1;
					$response['data'] = array('meditation article list' => $articleList);
				} else {
					$response['message'] = "No data found!";
					$response['ok'] = 1;
					$response['data'] = array('meditation article list' => $articleList);
				}
				/*hello*/
			} elseif ($dataata['filter_by'] == 'myArticle' || $data['filter_by'] == 'myArticle') {
				// echo "Myarticle";
				// die;
				$myMusicCon['sorting'] = array("article_id" => "DESC");
				$myMusicCon['conditions'] = array("user_id" => $data['user_id'], "category_id" => $data['category_id']);
				$myMusicCon['limit'] = $limit;
				$my_music_data  = $this->Api_model->getRows('cp_meditation_articles', $myMusicCon);
				$myMusicTotalCon['conditions'] = array("user_id" => $data['user_id'], "category_id" => $data['category_id']);
				$myMusic_Total_data  = $this->Api_model->getRows('cp_meditation_articles', $myMusicTotalCon);
				if (!empty($my_music_data)) {
					if (empty($my_music_data)) {
						$articleList['myArticle']['myArticleCount'] = "0";
						$articleList['myArticle']['myArticleList'] = array();
					}
					$articleList['myArticle']['myArticleCount'] = count($myMusic_Total_data);
					foreach ($my_music_data as $row_mymusic_data) {
						$myfav_article = $this->common_model->getsingle("cp_article_favourite", array("article_id" => $row_mymusic_data['article_id'], 'user_id' => $data['user_id']));
						if (!empty($myfav_article)) {
							$favmy = '1';
						} else {
							$favmy = '0';
						}
						$articleList['myArticle']['myArticleList'][] = array(
							'article_id' => $row_mymusic_data['article_id'],
							'article_title' => $row_mymusic_data['article_title'],
							'article_desc' => $row_mymusic_data['article_desc'],
							'artist_name' => $row_mymusic_data['artist_name'],
							'article_image' => base_url() . 'uploads/meditation_articles/' . $row_mymusic_data['article_image'],
							'artist_image' => base_url() . 'uploads/meditation_articles/' . $row_mymusic_data['artist_image'],
							'fav' => $favmy,
							'create_date' => $row_mymusic_data['article_created'],
							// 'category_name'=>$popularCategoryName,
							'category_id' => $data['category_id'],
							'user_id' => $row_mymusic_data['user_id'],
						);
					}
					$response['message'] = "My meditation article list.";
					$response['ok'] = 1;
					$response['data'] = array('meditation article list' => $articleList);
				} else {
					$response['message'] = "No data found!";
					$response['ok'] = 1;
					$response['data'] = array('meditation article list' => $articleList);
				}
			} elseif ($dataata['filter_by'] == 'favourite' || $data['filter_by'] == 'Favourite') {
				// echo "Favourite";
				// die;
				$favMusicCon['sorting'] = array("article_fav_id" => "DESC");
				$favMusicCon['limit'] = $limit;
				$favMusicCon['conditions'] = array("cp_meditation_article_favourite.user_id" => $data['user_id'], "cp_meditation_articles.category_id" => $data['category_id']);
				$fav_music_data  = $this->Api_model->getRowsFavouriteMeditationArticle('cp_meditation_article_favourite', $favMusicCon);
				$favMusicTotalCon['conditions'] = array(
					"cp_meditation_article_favourite.user_id" => $data['user_id'],
					"cp_meditation_articles.category_id" => $data['category_id']
				);
				$favMusic_Total_data  = $this->Api_model->getRowsFavouriteMeditationArticle('cp_meditation_article_favourite', $favMusicTotalCon);
				if (!empty($fav_music_data)) {
					if (empty($fav_music_data)) {
						$articleList['Favourite']['favCount'] = "0";
						$articleList['Favourite']['favList'] = array();
					}
					$articleList['Favourite']['favCount'] = count($favMusic_Total_data);
					foreach ($fav_music_data as $row_favmusic_data) {
						$myfav_article1 = $this->common_model->getsingle("cp_article_favourite", array("article_id" => $row_favmusic_data['article_id'], 'user_id' => $data['user_id']));
						if (!empty($myfav_article1)) {
							$fav1 = '1';
						} else {
							$fav1 = '0';
						}
						$articleList['Favourite']['favList'][] = array(
							'article_id' => $row_favmusic_data['article_id'],
							'article_title' => $row_favmusic_data['article_title'],
							'article_desc' => $row_favmusic_data['article_desc'],
							'artist_name' => $row_favmusic_data['artist_name'],
							'article_image' => base_url() . 'uploads/meditation_articles/' . $row_favmusic_data['article_image'],
							'artist_image' => base_url() . 'uploads/meditation_articles/' . $row_favmusic_data['artist_image'],
							'fav' => 1,
							'create_date' => $row_favmusic_data['article_created'],
							// 'category_name'=>$popularCategoryName,
							'category_id' => $data['category_id'],
							'user_id' => $row_favmusic_data['user_id'],
						);
					}
					$response['message'] = "Favourite meditation article list.";
					$response['ok'] = 1;
					$response['data'] = array('meditation article list' => $articleList);
				} else {
					$response['message'] = "No data found!";
					$response['ok'] = 1;
					$response['data'] = array('meditation article list' => $articleList);
				}
			}
		} else {
			$response['errorcode'] = "500007";
			$response['message'] = "Please provide the required parameters!";
			$response['ok'] = 0;
		}
		echo json_encode($response);
	}
	/*Delete multiple vitals*/
	public function delete_multivital_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('vital_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$ids = explode(',', $data['vital_id']);
		$result = $this->common_model->DeleteMultipleViatalById($ids);
		if ($result) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('vital_data' => "vital data deleted succesfully"));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'vital not found', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	/*Vital sign test type list*/
	public function vital_test_type_list_post()
	{
		$response = array();
		$VitalData = $this->Api_model->GetvitaltesttypeList();
		if (!empty($VitalData)) {
			if (empty($VitalData)) {
				$VitalSignData['VitalSignData'] = array();
			}
			foreach ($VitalData as $vital) {
				$VitalSignData[] = array(
					'id' => $vital['id'],
					'test_type' => $vital['test_type'],
					//'other_test_type'=>$vital['other_test_type'],
					'create_date' => $vital['create_date']
				);
			}
			$VitalSignData[] = array(
				'id' => 0,
				'test_type' => 'Other',
				'create_date' => date('Y-m-d H:i:s')
			);
			$response['message'] = "Vital test type list.";
			$response['ok'] = 1;
			$response['data'] = array('Vita_detail' => $VitalSignData);
		} else {
			$response['message'] = "No data found!";
			$response['ok'] = 1;
			$response['data'] = array('Vita_detail' => $VitalSignData);
		}
		echo json_encode($response);
	}
	/*New API end*/
	public function verifyuser_device_post()
	{
		$data = $this->param;
		$required_parameter = array('token');
		//$required_parameter = array('device_id','token');
		$chk_error = check_required_value($required_parameter, $data);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		} else {
			//$device_id = $data['device_id'];
			$token = $data['token'];
			// echo "<pre>";
			// echo $token;
			// echo "<br>";
			// echo $device_id;
			// die;
			//$tokenArray=array('device_id'=>$device_id,'token'=>$token);
			//$tokedetail = $this->common_model->getuserVerified('cp_token',$device_id,$token);
			$tokedetail = $this->common_model->getuserVerified('cp_token', $token);
			// echo "<pre>";
			// print_r($tokedetail);
			// die;
			if (!empty($tokedetail)) {
				$condArray = array('id' => $tokedetail[0]['user_id']);
				$usersdetail = $this->common_model->getSingleRecordById('cp_users', $condArray);
				// echo "<pre>";
				// print_r($usersdetail);
				// die;
				$user_details = array(
					'user_verification' => $usersdetail['user_verify_by_app'],
					'Otp_verification' => $usersdetail['status'],
					'token_verification' => $usersdetail['device_verify_by_token'],
					'id' => $usersdetail['id'],
					'name' => $usersdetail['name'],
					'lastname' => $usersdetail['lastname'],
					'dob' =>	$usersdetail['dob'],
					'username' => $usersdetail['username'],
					'city' => $usersdetail['city'],
					'state' => $usersdetail['state'],
					'zipcode' => $usersdetail['zipcode'],
					'company_school' => $usersdetail['company_school'],
					'email'	 => $usersdetail['email'],
					'mobile' => $usersdetail['mobile'],
					'password'	=> $usersdetail['password'],
					'profile_image'	=> $usersdetail['profile_image'],
					'device_id'	=> $usersdetail['device_id'],
					'device_type' => $usersdetail['device_type'],
					'device_token' => $usersdetail['device_token'],
					'user_role'	=> $usersdetail['user_role'],
					'act_link' => $usersdetail['act_link'],
					'status' => $usersdetail['status'],
					'Otp_verification' => $usersdetail['status'],
					//'user_verification_status'=> $usersdetail['user_verify_by_app'],
					'conversation_status' => $usersdetail['conversation_status'],
					'create_user' => $usersdetail['create_user'],
					'update_user' => $usersdetail['update_user'],
					'otp' => $usersdetail['otp'],
					'otp_expire' => $usersdetail['otp_expire'],
					'country_code' => $usersdetail['country_code'],
					'mobile_with_code' => $usersdetail['mobile_with_code'],
					'gender' => $usersdetail['gender'],
					'address' => $usersdetail['address'],
					'user_id' => $usersdetail['user_id'],
				);
				$uid = $usersdetail['id'];
				$where_condition = array('id' => $uid);
				$verificationData = array('device_verify_by_token' => 1);
				$this->common_model->updateRecords('cp_users', $verificationData, $where_condition);
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Verification successfull.', 'response' => $user_details);
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'please enter correct Token ID.', 'response' => array('message' => 'please enter correct Token ID'));
			}
		}
		$this->response($resp);
	}
	public function device_verify_post()
	{
		$data = $this->param;
		$required_parameter = array('device_id');
		$chk_error = check_required_value($required_parameter, $data);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		} else {
			$device_id = $data['device_id'];
			//$token =$data['token'];
			//$tokenArray=array('device_id'=>$device_id,'token'=>$token);
			$tokedetail = $this->common_model->getuser_device_Verified('cp_token', $device_id);
			if (!empty($tokedetail)) {
				$condArray = array('id' => $tokedetail[0]['user_id']);
				$usersdetail = $this->common_model->getSingleRecordById('cp_users', $condArray);
				// echo "<pre>";
				// print_r($usersdetail);
				// die;
				$user_details = array(
					//'id' => $usersdetail['id'],
					//'email' => $usersdetail['email'],
					// 'mobile' => $usersdetail['mobile'],
					'user_verification' => $usersdetail['user_verify_by_app'],
					'Otp_verification' => $usersdetail['status'],
					'token_verification' => $usersdetail['device_verify_by_token'],
					'id' => $usersdetail['id'],
					'name' => $usersdetail['name'],
					'lastname' => $usersdetail['lastname'],
					'dob' =>	$usersdetail['dob'],
					'username' => $usersdetail['username'],
					'city' => $usersdetail['city'],
					'state' => $usersdetail['state'],
					'zipcode' => $usersdetail['zipcode'],
					'company_school' => $usersdetail['company_school'],
					'email'	 => $usersdetail['email'],
					'mobile' => $usersdetail['mobile'],
					'password'	=> $usersdetail['password'],
					'profile_image'	=> $usersdetail['profile_image'],
					'device_id'	=> $usersdetail['device_id'],
					'device_type' => $usersdetail['device_type'],
					'device_token' => $usersdetail['device_token'],
					'user_role'	=> $usersdetail['user_role'],
					'act_link' => $usersdetail['act_link'],
					'status' => $usersdetail['status'],
					'Otp_verification' => $usersdetail['status'],
					//'user_verification_status'=> $usersdetail['user_verify_by_app'],
					'conversation_status' => $usersdetail['conversation_status'],
					'create_user' => $usersdetail['create_user'],
					'update_user' => $usersdetail['update_user'],
					'otp' => $usersdetail['otp'],
					'otp_expire' => $usersdetail['otp_expire'],
					'country_code' => $usersdetail['country_code'],
					'mobile_with_code' => $usersdetail['mobile_with_code'],
					'gender' => $usersdetail['gender'],
					'address' => $usersdetail['address'],
					'user_id' => $usersdetail['user_id'],
					// 'device_id' => $usersdetail['device_id'],
					// 'device_type' => $usersdetail['device_type'],
					// 'profile_image'=>base_url().'uploads/profile_images/'.$usersdetail['profile_image'],
				);
				if (($usersdetail['user_verify_by_app'] == 1) && ($usersdetail['status'] == 0)) {
					// OTP code start
					$mob = $usersdetail['mobile'];
					$datee = strtotime(date("h:i:sa")) + 600;
					$datee = date("Y-m-d h:i:s", $datee);
					$user_name = $usersdetail['email'];
					$otp = rand(1000, 9999);
					$uid = array('id' => $usersdetail['id']);
					$data_otp = array('otp' => $otp, 'otp_expire' => $datee);
					//$response= $this->send_sms($otp,$mob);
					$response = $this->send_sms($otp, $mob);
					$subject = 'CarePro OTP Code';
					$message = 'Your user verify OTP code:' . $otp;
					$chk_mail = new_send_mail($message, $subject, $user_name, '');
					// echo $chk_mail;
					// die;
					$otp_id = $this->common_model->updateRecords('cp_users', $data_otp, $uid);
					// OTP code end 
				}
				$uid = $usersdetail['id'];
				$where_condition = array('id' => $uid);
				$verificationData = array('device_verify' => 1);
				$this->common_model->updateRecords('cp_users', $verificationData, $where_condition);
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Device Verification successfull.', 'response' => $user_details);
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'We are unable to install Soultb app on this device, please contact Soultab app for support.', 'response' => array('message' => 'We are unable to install Soultb app on this device, please contact Soultab app for support.'));
			}
		}
		$this->response($resp);
	}
	function random_strings($length_of_string)
	{
		$str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		return substr(
			str_shuffle($str_result),
			0,
			$length_of_string
		);
	}
	public function genrate_ticket_post()
	{
		$data = $this->param;
		$required_parameter = array('user_id', 'email', 'message', 'phone', 'country_code');
		$chk_error = check_required_value($required_parameter, $data);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		} else {
			$user_id = $data['user_id'];
			$email = $data['email'];
			$message = $data['message'];
			$phone = $data['phone'];
			$country_code = $data['country_code'];
			$GenrateTickeID = $this->random_strings(11);
			$phone_no = $country_code . $phone;
			$array = array(
				'user_id' => $user_id,
				'email' => $email,
				'phone' => $phone_no,
				'devicce_id' => '',
				'genrate_ticketid' => $GenrateTickeID,
				'ticket_desc' => $message,
				'created' => date('Y-m-d H:i:s')
			);
			$userId = $this->common_model->addRecords('user_tickets', $array);
			if ($userId) {
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'your request has been sent successfully.', 'response' => 'your request has been sent successfully.');
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Somthing went wrong.', 'response' => 'Somthing went wrong.');
			}
		}
		$this->response($resp);
	}
	public function ticket_list_post()
	{
		$data = $this->param;
		$limit = 10;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $data);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		} else {
			$user_id = $data['user_id'];
			if (empty($data['page_no'])) {
				$data['page_no'] = 1;
			}
			//$tickets=$this->common_model->getAllwhereorderby('user_tickets',array('user_id'=>$user_id),'ticket_id','DESC');
			$ticketCount = $this->common_model->getAllwhere("user_tickets", array('user_id' => $user_id));
			$start  = ($data['page_no'] - 1) * $limit;
			$pages = ceil(count($ticketCount) / $limit);
			// echo $data['page_no']; 
			// die;
			$usersdetail = $this->common_model->getTicketList($start, $limit, $user_id);
			// $usersdetail  = $this->common_model->getAllwhereorderby('user_tickets',array('user_id'=>$user_id),'ticket_id','DESC');
			if (!empty($usersdetail)) {
				if (empty($usersdetail)) {
					$VitalSignData['VitalSignData'] = array();
				}
				foreach ($usersdetail as $ticket) {
					$VitalSignData[] = array(
						'ticket_id' => $ticket['ticket_id'],
						'genrate_ticketid' => $ticket['genrate_ticketid'],
						'ticket_desc' => $ticket['ticket_desc'],
						'user_id' => $ticket['user_id'],
						'created' => $ticket['created']
					);
				}
				// $result = array(
				// 	'ticket_id'=>$usersdetail['ticket_id'],
				// 	'genrate_ticketid'=>$usersdetail['genrate_ticketid'],
				// 	'ticket_desc' => $usersdetail['ticket_desc'],
				// 	'user_id' => $usersdetail['user_id'],
				// 	'created' => $usersdetail['created']
				// );
				//$userId = $this->common_model->addRecords('user_tickets', $array);
				//if(!empty($usersdetail)){
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('Ticket List' => $VitalSignData));
				// $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' =>'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'Ticket List.', 'response' =>$result);
			} else {
				$VitalSignData = array();
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Tickets not found', 'response' => array('message' => 'Tickets not found', 'Ticket List' => $VitalSignData));
				//$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Somthing went wrong.','response' => 'Somthing went wrong.');	
			}
		}
		$this->response($resp);
	}
	// Ticket conversation 
	public function ticket_conversetion_post()
	{
		$data = $this->param;
		$limit = 10;
		$required_parameter = array('user_id', 'ticket_id');
		$chk_error = check_required_value($required_parameter, $data);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		} else {
			$user_id = $data['user_id'];
			$ticket_id = $data['ticket_id'];
			if ($data['page_no'] == '') {
				$data['page_no'] = 1;
			}
			//$tickets=$this->common_model->getAllwhereorderby('user_tickets',array('user_id'=>$user_id),'ticket_id','DESC');
			$ticketCount = $this->common_model->getAllwhere("tickets_reply", array('user_id' => $user_id, 'ticket_id' => $ticket_id));
			$start  = ($data['page_no'] - 1) * $limit;
			$pages = ceil(count($ticketCount) / $limit);
			// echo $data['page_no']; 
			// die;
			//$usersdetail = $this->common_model->getTicketConversationList($start,$limit,$user_id,$ticket_id); 
			$usersdetail = $this->common_model->getTicketConversationList($user_id, $ticket_id);
			// print_r($usersdetail);
			// die;
			// $usersdetail  = $this->common_model->getAllwhereorderby('user_tickets',array('user_id'=>$user_id),'ticket_id','DESC');
			if (!empty($usersdetail)) {
				if (empty($usersdetail)) {
					$VitalSignData['VitalSignData'] = array();
				}
				foreach ($usersdetail as $ticket) {
					if (!empty($ticket['by_admin'])) {
						$replyBy = 'Admin';
					} else {
						$replyBy = 'User';
					}
					$VitalSignData[] = array(
						'reply_by' => $replyBy,
						'reply_id' => $ticket['reply_id'],
						'ticket_id' => $ticket['ticket_id'],
						'by_user' => $ticket['by_user'],
						'by_admin' => $ticket['by_admin'],
						'genrate_ticketid' => $ticket['genrate_ticketid'],
						'user_id' => $ticket['user_id'],
						'reply_desc' => $ticket['reply_desc'],
						'created' => $ticket['created'],
						// 'ticket_id'=>$ticket['ticket_id'],
						// 'genrate_ticketid'=>$ticket['genrate_ticketid'],
						// 'ticket_desc' => $ticket['ticket_desc'],
						// 'user_id' => $ticket['user_id'],
						// 'created' => $ticket['created']
					);
				}
				// $result = array(
				// 	'ticket_id'=>$usersdetail['ticket_id'],
				// 	'genrate_ticketid'=>$usersdetail['genrate_ticketid'],
				// 	'ticket_desc' => $usersdetail['ticket_desc'],
				// 	'user_id' => $usersdetail['user_id'],
				// 	'created' => $usersdetail['created']
				// );
				//$userId = $this->common_model->addRecords('user_tickets', $array);
				//if(!empty($usersdetail)){
				// $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'response' => array('Ticket conversation' => $VitalSignData));
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('Ticket conversation' => $VitalSignData));
				// $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' =>'SUCCESS','pages'=>$pages,'per_page_records'=>$limit, 'Ticket List.', 'response' =>$result);
			} else {
				$VitalSignData = array();
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Tickets not found', 'response' => array('message' => 'Tickets not found', 'Ticket List' => $VitalSignData));
				//$resp = array('status_code' => FAILURE,'status' => 'false', 'message' => 'Somthing went wrong.','response' => 'Somthing went wrong.');	
			}
		}
		$this->response($resp);
	}
	// Ticket reply 
	public function ticket_reply_post()
	{
		$data = $this->param;
		$limit = 10;
		$required_parameter = array('user_id', 'ticket_id', 'message', 'genrated_ticketid');
		$chk_error = check_required_value($required_parameter, $data);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		} else {
			$user_id = $data['user_id'];
			$ticket_id = $data['ticket_id'];
			$message = $data['message'];
			$genrated_ticketid =	$data['genrated_ticketid'];
			$result = array(
				'ticket_id' => $ticket_id,
				'by_user' => $user_id,
				'by_admin' => '',
				'genrate_ticketid' => $genrated_ticketid,
				'user_id' => $user_id,
				'reply_desc' => $message,
				'created' => date('Y-m-d i:s:m'),
			);
			$replyId = $this->common_model->addRecords('tickets_reply', $result);
			if ($replyId) {
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => 'Message sent successfully.');
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'SUCCESS', 'response' => 'Somthing went wrong.');
			}
		}
		$this->response($resp);
	}
	// Ticket reply 
	public function survey_feedback_post()
	{
		$data = $this->param;
		$limit = 10;
		$required_parameter = array('email', 'mobile', 'how_muchlike', 'no_people', 'message');
		$chk_error = check_required_value($required_parameter, $data);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		} else {
			$email = $data['email'];
			$mobile = $data['mobile'];
			$how_muchlike = $data['how_muchlike'];
			$no_people =	$data['no_people'];
			$message = $data['message'];
			$result = array(
				'email' => $email,
				'mobile' => $mobile,
				'how_muchlike' => $how_muchlike,
				'no_people' => $no_people,
				'feed_desc' => $message,
				'created' => date('Y-m-d i:s:m'),
			);
			$replyId = $this->common_model->addRecords('user_survey_feedback', $result);
			if ($replyId) {
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => 'Thanks for your feedback.');
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'SUCCESS', 'response' => 'Somthing went wrong.');
			}
		}
		$this->response($resp);
	}


	public function quik_alert_post()
	{
		$data = $this->param;
		//$limit = 10; 
		$required_parameter = array('user_id', 'message', 'alert_category');
		$chk_error = check_required_value($required_parameter, $data);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		} else {

			$user_id = $data['user_id'];
			$message = $data['message'];
			$alert_category = $data['alert_category'];
			$result = array(
				'user_id' => $user_id,
				'message' => $message,
				'category' => $alert_category,
				'read_flag' => 0,
				'created_at' => date('Y-m-d i:s:m'),
			);


			$replyId = $this->common_model->addRecords('alerts', $result);


			if ($replyId) {
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => 'Your alert sent successfully.');
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'SUCCESS', 'response' => 'Somthing went wrong.');
			}
		}
		$this->response($resp);
	}


	public function medicine_request_alert_post()
	{
		$data = $this->param;
		//$limit = 10; 
		$required_parameter = array('user_id', 'message');
		$chk_error = check_required_value($required_parameter, $data);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		} else {

			$user_id = $data['user_id'];
			$message = $data['message'];

			$result = array(
				'user_id' => $user_id,
				'message' => $message,
				'created' => date('Y-m-d i:s:m'),
			);


			$replyId = $this->common_model->addRecords('medicine_request_alerts', $result);


			if ($replyId) {
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => 'Your alert request sent successfully.');
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'SUCCESS', 'response' => 'Somthing went wrong.');
			}
		}
		$this->response($resp);
	}

	public function grocery_pickup_alert_post()
	{
		$data = $this->param;
		//$limit = 10; 
		$required_parameter = array('user_id', 'message');
		$chk_error = check_required_value($required_parameter, $data);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		} else {

			$user_id = $data['user_id'];
			$message = $data['message'];

			$result = array(
				'user_id' => $user_id,
				'message' => $message,
				'created' => date('Y-m-d i:s:m'),
			);


			$replyId = $this->common_model->addRecords('grocery_pickup_alerts', $result);


			if ($replyId) {
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => 'Your alert request sent successfully.');
			} else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'SUCCESS', 'response' => 'Somthing went wrong.');
			}
		}
		$this->response($resp);
	}


	public function make_fav_post()
	{
		$data = $this->param;
		$limit = 10;
		$required_parameter = array('user_id', 'video_id', 'fav');
		$chk_error = check_required_value($required_parameter, $data);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		} else {
			$user_id = $data['user_id'];
			$video_id = $data['video_id'];
			$fav = $data['fav'];
			if ($fav == 1) {
				$ticketCount = $this->common_model->getAllwhere("cp_meditaion_videos_favorite", array('user_id' => $user_id, 'movie_id' => $video_id));
				if ($ticketCount > 0) {
					$unfav = $this->common_model->deleteRecords('cp_meditaion_videos_favorite', array('user_id' => $user_id, 'movie_id' => $video_id));
					if ($unfav) {
						$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => 'Vedio unfavorite successfully.');
					} else {
						$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'SUCCESS', 'response' => 'Somthing went wrong.');
					}
				} else {
					$result = array(
						'user_id' => $user_id,
						'movie_id' => $video_id,
						'fav' => $fav,
						'movie_fav_created' => date('Y-m-d i:s:m'),
					);
					$replyId = $this->common_model->addRecords('cp_meditaion_videos_favorite', $result);
					if ($replyId) {
						$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => 'Vedio favorite successfully.');
					} else {
						$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'SUCCESS', 'response' => 'Somthing went wrong.');
					}
				}
			} else {
				$unfav = $this->common_model->deleteRecords('cp_meditaion_videos_favorite', array('user_id' => $user_id, 'movie_id' => $video_id));
				if ($unfav) {
					$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => 'Vedio unfavorite successfully.');
				} else {
					$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'SUCCESS', 'response' => 'Somthing went wrong.');
				}
			}
		}
		$this->response($resp);
	}
	public function sharephoto_list_post()
	{
		$data = $this->param;
		$limit = 10;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $data);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		} else {
			$user_id = $data['user_id'];
			//$ticket_id = $data['ticket_id'];
			if (empty($data['page_no'])) {
				$data['page_no'] = 1;
			}
			// share by me to others
			$where = array($user_id);
			$shareswith = $this->common_model->getAllshareby($where);
			/* if(empty($shareswith)){
																			$shareswith=array('');
																		} */
			//share by others to me
			$where = array($user_id);
			$sharesby = $this->common_model->getAllsharewith($where);
			if (empty($sharesby)) {
				$sharesby = array('');
			}
			if (!empty($shareswith)) {
				$allary = array_merge($shareswith, $sharesby);
			} else {
				$allary = $sharesby;
			}
			$sharMerge = array();
			$sharePhoto = array();
			$sharMergenew = array();
			foreach ($allary as $shara) {
				$users = explode(',', $shara->receiver_ids);
				$photos = explode(',', $shara->photo_ids);
				//s$phto = $shara->photo_ids;
				foreach ($photos as $phto) {
					$PhotoData = $this->common_model->getsingle('cp_user_photo', array('u_photo_id' => $phto));
					// echo "<pre>";
					// print_r($PhotoData);
					//echo "<br>";
					// echo $phto;
					// echo "<br>";
					if (!empty($PhotoData->u_photo_id)) {
						$sharePhoto[] = array(
							'photo_id' => $PhotoData->u_photo_id,
							'user_id' => $PhotoData->user_id,
							'photo_url' => base_url() . 'uploads/photos/' . $PhotoData->u_photo,
							'photo_status' => $PhotoData->u_photo_status,
							'photo_created' => $PhotoData->u_photo_created,
						);
					}
				}
				// echo "<pre>";
				// print_r($sharePhoto);
				// echo "<br>";
				//die; 
				if (!empty($sharePhoto)) {
					$sharMerge[] = array(
						'id' => $shara->id,
						'sender_id' => $shara->sender_id,
						'receiver_ids' => $users,
						'photos' => $sharePhoto,
						'description' => ((!empty($shara->description) ?: '')),
						'status' => $shara->status,
						'created_at' => $shara->created_at
					);
				}
			}
			// print_r($sharMerge); die;
			//$start  = ($data['page_no']-1)*$limit;
			//$pages = ceil(count($ticketCount) / $limit);
			// echo $data['page_no']; 
			// die;
			// $usersdetail = $this->common_model->getTicketConversationList($start,$limit,$user_id,$ticket_id);
			// print_r($usersdetail);
			// die;
			// $usersdetail  = $this->common_model->getAllwhereorderby('user_tickets',array('user_id'=>$user_id),'ticket_id','DESC');
			if (!empty($sharMerge)) {
				$VitalSignData['shareList'] = $sharMerge;
				// if(empty($sharMerge)){
				// 	$VitalSignData['VitalSignData'] = array();
				// }
				// foreach ($usersdetail as $ticket){
				// 	$VitalSignData[]=array(
				// 		'reply_by'=>$replyBy,
				// 		'reply_id'=>$ticket['reply_id'],
				// 		'ticket_id'=>$ticket['ticket_id'],
				// 		'by_user'=>$ticket['by_user'],
				// 		'by_admin'=>$ticket['by_admin'],
				// 		'genrate_ticketid'=>$ticket['genrate_ticketid'],
				// 		'user_id'=>$ticket['user_id'],
				// 		'reply_desc'=>$ticket['reply_desc'],
				// 		'created'=>$ticket['created'],
				// 	);
				// }
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('shareList' => $sharMerge));
			} else {
				$VitalSignData = array();
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'shareList not found', 'response' => array('message' => 'shareList not found', 'shareList' => $VitalSignData));
			}
		}
		//$myJSON = json_encode($resp);
		return $this->response($resp);
	}
	public function doctorSearchxyz_old_post()
	{
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('category_id', 'search_keyword');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$limit = 5;
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$start  = ($data['page_no'] - 1) * $limit;
		$con1['returnType'] = 'count';
		$table = "cp_doctor";
		$con1['conditions'] = array("doctor_category_id" => $data['category_id']);
		$category_count  = $this->Api_model->getRows($table, $con1);
		$pages = ceil(count($category_count) / $limit);
		$con['sorting'] = array("doctor_id" => "DESC");
		$con['limit'] = $limit;
		$con['start'] = $start;
		$con['conditions'] = array("doctor_category_id" => $data['category_id']);
		$doctorDatas  = $this->Api_model->getRows($table, $con);
		if (!empty($doctorDatas)) {
			foreach ($doctorDatas as $doctorData_details) {
				$doctorData[] = array(
					'id' => $doctorData_details['doctor_id'],
					'name' => $doctorData_details['doctor_name'],
					'fees' => $doctorData_details['doctor_fees'],
					'address' => $doctorData_details['doctor_address'],
					'contact' => $doctorData_details['doctor_mob_no'],
					'image' => base_url() . 'uploads/doctor/' . $doctorData_details['doctor_pic'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('doctor_data' => $doctorData));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Doctor not found', 'response' => array('message' => 'Doctor not found'));
		}
		$this->response($resp);
	}
	public function doctorSearch_post()
	{
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('category_id', 'search_keyword');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$limit = 5;
		if ($data['page_no'] == '') {
			$data['page_no'] = 1;
		}
		$start  = ($data['page_no'] - 1) * $limit;
		$category_count  = $this->Api_model->getDoctorSearchCount($data['category_id'], $data['search_keyword']);
		$pages = ceil(count($category_count) / $limit);
		$doctorDatas  = $this->Api_model->getDoctorSearch($data['category_id'], $data['search_keyword']);
		// echo "<pre>";
		// print_r($doctorDatas);
		// die;  
		$categoryImage = $this->getCategoryImage($data['category_id']);
		if (!empty($categoryImage)) {
			$doc_cat = base_url() . 'uploads/doctor_category/' . $categoryImage;
		} else {
			$doc_cat = base_url() . 'uploads/doctor_category/catDefault.png';
		}
		if (!empty($doctorDatas)) {
			foreach ($doctorDatas as $doctorData_details) {
				if (!empty($doctorData_details['doctor_pic'])) {
					$DocImg	=	base_url() . 'uploads/doctor_category/' . $doctorData_details['doctor_pic'];
				} else {
					$DocImg	=	base_url() . 'uploads/doctor_category/def.png';
				}
				$doctorData[] = array(
					'id' => $doctorData_details['doctor_id'],
					'name' => $doctorData_details['doctor_name'],
					'fees' => $doctorData_details['doctor_fees'],
					'address' => $doctorData_details['doctor_address'],
					'contact' => $doctorData_details['doctor_mob_no'],
					'category_name' => $this->getCategory($doctorData_details['doctor_category_id']),
					'category_id' => $doctorData_details['doctor_category_id'],
					'image' => $doc_cat,
					'doctor_image' => $DocImg,
					"doctor_email" => $doctorData_details['doctor_email'],
				);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'pages' => $pages, 'per_page_records' => $limit, 'response' => array('doctor_data' => $doctorData));
		} else {
			$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'Doctor not found', 'response' => array('message' => 'Doctor not found'));
		}
		$this->response($resp);
	}
	
	public function test_post()
	{
		//send notification
		$registrationIds[] = 'fe8jGYSztUI:APA91bEsykle8pCu9si3oJovjLjdgt646KTOkYt_Fp7XHKKtk21lxxRWqnAwcZx0WIqm_C1VRyyk4MTVl8QWNk14Oa9X_AHpkoTLpWT6nvXF3vq5hoY_JTFXe_dcaA8nuwSJB4bbOtau';
		// $registrationIds[] = 'cl1YyLt-Iis:APA91bFvQHPTCDDr_xmCXnlUNpbW-RyDZdqY5OAAP98-thxtHaQ_E_rUJBFtZbRbkPQPRoj2zlM7jrZuVdiXuCgPX_L-Z7Mis4Jwl5x7sEhlg6JoSK7PpSIHV5bkDPDR9AUY2_KPwSWr';
		$header = [
			'Authorization: Key=AAAA-vHtgpk:APA91bHYvBeAoUQrvzzGPRNS_KSy_idvKzFdA6ebK4r1gC5vJhXZssvlpVkEnooCS_k_cgEVwMXhDbuEPbGdpxcg5muNpopjPCjJ5ZHhjo7pN3WfxtdNVHuR3wAJz5Fu-xDSyXu-PQUN',
			'Content-Type: Application/json'
		];
		$msg = [
			'title' => 'survey',
			'body' => 1
		];
		$payload = [
			'registration_ids'  => $registrationIds,
			'data'        => $msg,
			'notification' => $msg
		];
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode($payload),
			CURLOPT_HTTPHEADER => $header
		));
		$response = curl_exec($curl);
		curl_close($curl);
		echo $response;
	}
	public function mydata_post()
	{
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('scraping_job_id', 'api_token');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$api_token = $data['api_token'];
		$scraping_job_id = $data['scraping_job_id'];
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.webscraper.io/api/v1/scraping-job/" . $scraping_job_id . "/csv?api_token=" . $api_token . "",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "GET",
			//CURLOPT_POSTFIELDS => json_encode( $payload ),
			//CURLOPT_HTTPHEADER => $header
		));
		$response = curl_exec($curl);
		curl_close($curl);
		echo $response;

	}
	/***************Search Yoga Video********** */
	public function search_yoga_video_post()
	{
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id','yoga_date');
		$chk_error = check_required_value($required_parameter, $object_info);
		$user_id = isset($object_info['user_id']) ? $object_info['user_id'] : '';
		$yoga_date = isset($object_info['yoga_date']) ? $object_info['yoga_date'] : '';
		$help_with = isset($object_info['help_with']) ? $object_info['help_with'] : '';
		$body_part = isset($object_info['body_part']) ? $object_info['body_part'] : '';
		$yoga_style = isset($object_info['yoga_style']) ? $object_info['yoga_style'] : '';
		$yoga_pose = isset($object_info['yoga_pose']) ? $object_info['yoga_pose'] : '';
		$yoga_duration = isset($object_info['yoga_duration']) ? $object_info['yoga_duration'] : '';
		$yoga_type = isset($object_info['yoga_type']) ? $object_info['yoga_type'] : '';
		$yoga_level = isset($object_info['yoga_level']) ? $object_info['yoga_level'] : '';
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		if (!empty($data)) {
			$data = array(
				"user_id"		=> $user_id,
				"yoga_date"		=> $yoga_date,
				"help_with"		=> $help_with,
				"body_part"		=> $body_part,
				"yoga_style"	=> $yoga_style,
				"yoga_pose"		=> $yoga_pose,
				"yoga_duration"	=> $yoga_duration,
				"yoga_type"		=> $yoga_type,
				"yoga_level"	=> $yoga_level
			);

			/*Start Search Yoga Video NAvjyoti 13-Jan-2021*/
		
			$array_yoga_videos = $this->common_model->getAllYogaRecords('yoga_videos',$data);

			if (!empty($array_yoga_videos)) {
				foreach ($array_yoga_videos as $k) {
				// _dx($k);
					$yoga_video = $k['yoga_video'];
					$favorite = $k['favorite'];
					if($favorite == '1'){
						$video_result[] = array(
							'favorite' =>$favorite,														
							'user_id'=>$k['user_id'],
							'yoga_id'=>$k['yoga_id'],								
							'title' => $k['yoga_title'],
							'description'=>$k['yoga_desc'],
							'duration'=> $k['yoga_duration'],
							'date' => $k['yoga_created'],
							'url' => base_url() . 'uploads/yoga_videos/'.$yoga_video,
							'image'=>base_url() . 'uploads/yoga_videos/image/' . $k['yoga_image']
						
						);
						
					}else{
						$video_result[] = array(
							'favorite' =>'0',
							'user_id'=>$k['user_id'],													
							'yoga_id'=>$k['yoga_id'],							
							'title' => $k['yoga_title'],
							'description'=>$k['yoga_desc'],
							'duration'=> $k['yoga_duration'],
							'date' => $k['yoga_created'],
							'url' => base_url() . 'uploads/yoga_videos/'.$yoga_video,
							'image'=>base_url() . 'uploads/yoga_videos/image/' . $k['yoga_image']
						
						);
					}
					$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('yoga_data' => $video_result));

				}
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('yoga_data' => $video_result));
			}else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'No messages matched your search');
			}

			/*End Search Yoga Video NAvjyoti 13-Jan-2021*/

			$this->response($resp);
		}
	}

	/***********Favorite List API *******/
	public function favorite_yoga_list_post()
	{
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		$user_id = isset($object_info['user_id']) ? $object_info['user_id'] : '';
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		if (!empty($data)) {
			$data = array(
				"user_id"		=> $user_id,
				"favorite"		=> '1'

			);		
	
			$array_yoga_videos = $this->common_model->jointwotable('yoga_fav_video', 'yoga_id', 'yoga_videos', 'yoga_id', array('yoga_fav_video.user_id' => $data['user_id'],'yoga_fav_video.favorite' => $data['favorite']), '*',$data);
// _dx($array_yoga_videos);
			if (!empty($array_yoga_videos)) {
			
					foreach ($array_yoga_videos as $row) {
						$favvideo[] = array(
							'favorite' 				=> $row->favorite,
							'yoga_id' 				=>$row->yoga_id,
							'user_id' 				=>$user_id,
							'title' 				=>$row->yoga_title,
							'description' 			=>$row->yoga_desc,
							'help_with' 			=>$row->help_with,
							'body_part' 			=>$row->body_part,
							'style' 				=>$row->yoga_style,
							'pose' 					=>$row->yoga_pose,
							'duration' 				=>$row->yoga_duration,
							'level' 				=>$row->yoga_level,			
							'url' => base_url() . 'uploads/yoga_videos/'.$row->yoga_video,		
							'image'					=>base_url() . 'uploads/yoga_videos/image/' . $row->yoga_image
					

						);
					}
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('yoga_data' => $favvideo));
			}else {
				$resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'No messages matched your search');
			}



			$this->response($resp);
		}
	}


	/*********Insert 0 and 1 for yoga video favorite/unfavorite ******/


	/*********Insert 0 and 1 for yoga video favorite/unfavorite ******/


	public function add_yoga_video_status_post()
	{
		$data 						= $this->param;
		$object_info				= $data;
		$required_parameter 		= array('user_id','yoga_id','favorite');	
		$user_id 					= isset($object_info['user_id']) ? $object_info['user_id'] : '';
		$yoga_id					= isset($object_info['yoga_id']) ? $object_info['yoga_id'] : '';
		$favorite					= $object_info['favorite'] == '1' ? '1' : '0';
		// _d($favorite);
		$chk_error 					= check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$required_parameter = array('user_id','yoga_id','favorite');
		/*Start new code*/
		$data = $this->common_model->getsingle("yoga_fav_video", array("user_id" => $user_id));	
		
		// _dx($data);	
		if (!empty($data) && $data->user_id == $user_id && $data->yoga_id == $yoga_id) {
			
			$update_array = array(				
				"user_id"				=> $user_id,
				"yoga_id"				=> $yoga_id,
				"favorite"				=> $favorite
			);
			$query = $this->common_model->updateRecords('yoga_fav_video', $update_array, array('user_id' => $data->user_id,'yoga_id'=>$data->yoga_id));		
			if($favorite == '0'){
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => 'Yoga Video unfavorite Successfully.');
				$this->response($resp);
			}else{
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => 'Yoga Video favorite Successfully.');
				$this->response($resp);
			}
				
		} else {
			$insert_array = array(
				"user_id"				=> $user_id,
				"yoga_id"				=> $yoga_id,
				"favorite"				=> $favorite
			);
			$query = $this->common_model->addRecords('yoga_fav_video', $insert_array);
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => 'Yoga Video Favorite Successfully.');
			$this->response($resp);
		}




	}
	// Pharmacy Pickup 
	public function get_Pharmacy_details_by_user()
	{
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		$user_id = isset($object_info['user_id']) ? $object_info['user_id'] : '';
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		if (!empty($data)) {
			$this->db->select('*');
			$this->db->from('cp_pharmacy');
			$this->db->where('user_id', $user_id);
			$query = $this->db->get();
			$result = $query->result_array();
			foreach ($result as $result_details) {
				$pharmacyArray[] = array('pharmacyId' => $result_details['id'],'pharmacyType' => $result_details['pharmacy_type'], 'button_name' => $result_details['button_name'], 'app_name' => $result_details['app_name'],'url'=> $result_details['url']);
			}
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => array('pharmacy_data' => $pharmacyArray));
		 }else {
				$resp = array('status_code' => SUCCESS, 'status' => 'false', 'message' => 'No Record Found');
		 }
		 $this->response($resp);
		
	}

	// api by ram for daily routine

	public function daily_routine_post()
	{
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array('user_id');
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}

		$check_dailyroutine = $this->common_model->getsingle("cp_dailyroutine",array("created" => date('Y-m-d'),"user_id" => $data['user_id']));

		if(!empty($check_dailyroutine)){
      	
      			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Daily routine information has already been submitted for the day', 'response' => array('dailyroutine_data' => $check_dailyroutine));
       
     	 } else {

				$post_data = array(
					'morning' => $data['morning'],
					'noon' => $data['noon'],
					'evening' => $data['evening'],
					'dinner' => $data['dinner'],
					'bedtime' => $data['bedtime'],
					'user_id' => $data['user_id'],
					'created' => date('Y-m-d'),
				);
		
				$dailyroutineId = $this->common_model->addRecords('cp_dailyroutine', $post_data);

				if ($dailyroutineId) {
					$userDetail = $this->common_model->getSingleRecordById('cp_users', array('id' => $data['user_id']));
					if($userDetail['cp_users_id'] != 0){
					   $userDetail_cp = $this->common_model->getSingleRecordById('cp_users', array('id' => $userDetail['cp_users_id']));
					   $user_name = $userDetail_cp['email'];
					} else {
						$user_name = $userDetail['email'];
					}
					
					$subject = "Daily Routine Report";

					$message = $this->emailTemp($data['user_id']);

					$chk_mail = sendEmail($user_name, $subject, $message);

					$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Dailyroutine added successfully', 'response' => array('dailyroutine_data' => $dailyroutineId));
				} else {
					$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
				}

		}
		
		$this->response($resp);
	}
	public function getCaregiverDetails_post(){
		$data = $this->param;
		$object_info = $data;
		$user_id = $data['user_id'];
		$required_parameter = array("user_id");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' =>  'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));
			$this->response($resp);
		}
		$userData = $this->common_model->getAllRecordsById('cp_users', array('cp_users_id' => $user_id));
		if(sizeof($userData) > 0){
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Caregiver Data', 'response' => $userData);
		}else {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Please add caregiver', 'response' => $userData);
		}
		$this->response($resp);
	}

	public function deleteCaregiveruser_post(){

		$data = $this->param;
		$object_info = $data;
		
		$required_parameter = array('id');
		$chk_error = check_required_value($required_parameter, $object_info);
		
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}
		$restid = $this->common_model->deleteRecords('cp_users', array('id' => $data['id']));
		if ($restid) {
			$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'caregiver user deleted successfully', 'response' => array('user_data' => "caregiver user deleted successfully"));
		} else {
			$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
		}
		$this->response($resp);
	}
	
	public function addCaregiveruser_post(){
	
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array("name", "lastname", "username", "email", "mobile", "street_name", "city", "state","country","language","gender","timezone","zipcode","password","snt_txt_msg_alrt","snt_eml_alrt","snt_daily_eml_daily_rutin_alrt","snt_eml_no_activity_in_app_alrt","snt_eml_alrm_is_missed");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}else{
			
			//post data chk
			$current_user_id =  isset($_POST['id']) ? $_POST['id'] : '';
			$name = isset($_POST['name']) ? $_POST['name'] : '';
			$lastname = isset($_POST['lastname']) ? $_POST['lastname'] : '';
			$username = isset($_POST['username']) ? $_POST['username'] : '';
			$email = isset($_POST['email']) ? $_POST['email'] : '';
			$mobile = isset($_POST['mobile']) ? $_POST['mobile'] : '';
			$street_name = isset($_POST['street_name']) ? $_POST['street_name'] : '';
			$city = isset($_POST['city']) ? $_POST['city'] : '';
			$state = isset($_POST['state']) ? $_POST['state'] : '';
			$country = isset($_POST['country']) ? $_POST['country'] : '';
			$language = isset($_POST['language']) ? $_POST['language'] : '';
			$gender = isset($_POST['gender']) ? $_POST['gender'] : '';
			$timezone = isset($_POST['timezone']) ? $_POST['timezone'] : '';
			$zipcode = isset($_POST['zipcode']) ? $_POST['zipcode'] : '';
			$password = isset($_POST['password']) ? $_POST['password'] : '';
			$password =	trim($password);
			$snt_txt_msg_alrt	=	isset($_POST['snt_txt_msg_alrt']) ? $_POST['snt_txt_msg_alrt'] : '';
			$snt_eml_alrt		=	isset($_POST['snt_eml_alrt']) ? $_POST['snt_eml_alrt'] : '';
			$snt_daily_eml_daily_rutin_alrt =	isset($_POST['snt_daily_eml_daily_rutin_alrt']) ? $_POST['snt_daily_eml_daily_rutin_alrt'] : '';
			$snt_eml_no_activity_in_app_alrt =	isset($_POST['snt_eml_no_activity_in_app_alrt']) ? $_POST['snt_eml_no_activity_in_app_alrt'] : '';
			$snt_eml_alrm_is_missed =	isset($_POST['snt_eml_alrm_is_missed']) ? $_POST['snt_eml_alrm_is_missed'] : '';
			$profile_image_name = '';
			if(!empty($_FILES['profile_image']['name']) && !empty($_FILES['profile_image']['name']) ){
				$profile_image = trim($_FILES['profile_image']['name']);
				/*$update_array = array(
					'profile_image' => md5($profile_image)
				);*/
				if(!empty($_FILES['profile_image']['name']) && $_FILES['profile_image']['name'] !=''){
					$config['file_name']     = $_FILES['profile_image']['name'];
					$config['upload_path']   = 'uploads/profile_images/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['max_size']      = '10000';
					$config['max_width']     = '0';
					$config['max_height']    = '0';
					$config['remove_spaces'] = true;
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					
					if (!$this->upload->do_upload('profile_image')) {
						$error = array('error' => $this->upload->display_errors());
						print_r($error);
						
					}else{
						
						if(!empty($exting_image) && $exting_image !=''){
							unlink('./uploads/profile_images/'.$exting_image);
						}
						
						$img = array('upload_data' => $this->upload->data());
						$profile_image_name = $img['upload_data']['file_name'];
					}
				}
			}
			//insert array for user profile
			$insert_data = array(
				'cp_users_id'  	=> $current_user_id,
				'name'			=> trim($name),
				'lastname' 		=> trim($lastname),
				'username' 		=> trim($username),
				'email' 		=> trim($email),
				'mobile' 		=> trim($mobile),
				'street_name'	=> trim($street_name),
				'city' 			=> trim($city),
				'state' 		=> trim($state),
				'country' 		=> trim($country),
				'zipcode' 		=> trim($zipcode),
				'language' 		=> trim($language),
				'gender' 		=> $this->input->post('gender'),
				'timezone' 		=> $this->input->post('timezone'),
				'password' 		=> md5($password),
				'profile_image' => $profile_image_name,
				'user_role' 	=>4,
				'create_user' 	=> date('Y-m-d H:i:s'),
				'status' 		=>1,
				'snt_txt_msg_alrt'	=> $snt_txt_msg_alrt,
				'snt_eml_alrt'			=> $snt_eml_alrt,
				'snt_daily_eml_daily_rutin_alrt' => $snt_daily_eml_daily_rutin_alrt,
				'snt_eml_no_activity_in_app_alrt'=> $snt_eml_no_activity_in_app_alrt,
				'snt_eml_alrm_is_missed'	=> $snt_eml_alrm_is_missed,
			);
			
			$insertId = $this->common_model->addRecords('cp_users', $insert_data);
			
			//insert array for setting btn
			
			if(!empty($insertId) && $insertId !=''){
				
				$setting_btn_arr = setting_btn_arr($insertId);
							
				if(!empty($setting_btn_arr) && $setting_btn_arr !=''){
					$insert_activities_record = $this->db->insert_batch('setting_butns', $setting_btn_arr);
				}

			/*start email sent code*/
			$subject ="Caregiver Profile Registration";
			$message ="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
			<html xmlns='http://www.w3.org/1999/xhtml'>
				<head>
					<link href='https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i' rel='stylesheet' />
					<link rel='stylesheet' type='text/css' href='https://www.mysoultab.com/assets/styles/caregivermail.css'>
					<title>Caregiver</title>
					<style>
					section {
						width: 40%;
						min-height: 200px;
						height: 200px;
						color: black;
						
						padding: 5%;
						padding-left: 0px;
						float: left;
						background-color: white;
				  }
				  section.two {
						width: 40%;
						min-height: 200px;
						height: 200px;
						color: black;
						
						padding: 5%;
						
						float: left;
						background-color: white;
				  }
				  section.one1{
					  width: 20%;
						min-height: 100px;
						height: 100px;
						color: black;
						
						padding: 5%;
						padding-left:0px;
						float: left;
						background-color: white;
				  }
				  section.two2{
					  width: 60%;
						min-height: 100px;
						height: 100px;
						color: black;
						
						padding: 5%;
						padding-left: 0px;
						float: left;
						background-color: white;
				  }
				  .bottom-line {
			
						border-bottom: 1px solid #000;
						padding-bottom: 20px;
				  }
				  .body {
		  
					  padding:0 !important;
					  margin:0 !important; 
					  display:block !important; 
					  min-width:100% !important; 
					  width:100% !important; 
					  background:#f4f4f4; 
					  -webkit-text-size-adjust:none;
				  }
		  
				  .td.container {
					  width:650px; 
					  min-width:650px; 
					  font-size:0pt; 
					  line-height:0pt; 
					  margin:0; 
					  font-weight:normal; 
					  padding:55px 0px;
				  }
		  
				  td.sec1{
					  padding-bottom: 20px;
				  }
		  
				  td.p30-15 {
					  padding: 25px 30px 25px 30px; 
					  background: #fff;
				  }
				  th.column-top {
					  font-size:0pt; 
					  line-height:0pt; 
					  padding:0; 
					  margin:0; 
					  font-weight:normal; 
					  vertical-align:top;
				  }
				  td.img.m-center {
					  font-size:0pt; 
					  line-height:0pt; 
					  text-align:left;
				  }
				  img.img.m-center {
					  padding-left: 230px;
				  }
				  td.h3.pb20{
					  color:#114490; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:24px; 
					  line-height:32px; 
					  text-align:center; 
					  padding-bottom:0px;
					  padding-top: 10px
				  }
				  td.text.pb20{
					  color:#000; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:14px; 
					  line-height:26px; 
					  text-align:left; 
					  padding-bottom:10px;
					  font-weight: 700;
				  }
				  td.text.pb201{
					  color:#777777; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:14px; 
					  line-height:26px; 
					  text-align:left; 
					  padding-bottom:10px;
				  }
				  td.text.pb202{
					  color:#000; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:24px; 
					  line-height:26px; 
					  text-align:left; 
					  padding-bottom:10px;
				  }
				  td.text1.pb201{
					  color:#000; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:14px; 
					  line-height:26px; 
					  text-align:center; 
					  padding-top: 5px; 
					  font-weight: 600;
				  }
				  td.text1.pb2012{
					  color:#000; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:14px; 
					  line-height:26px; 
					  text-align:center; 
					  padding-top: 5px; 
					  font-weight: 600;
					  padding-bottom: 15px;
				  }
				  td.p30-155{
					  padding: 5px 5px ;
				  }
				  td.sec2{
					  border-collapse: collapse; 
					  border-spacing: 0;
		  
				  }
				  img.sec2{
					  padding: 0px; 
					  margin: 0; 
					  outline: none; 
					  text-decoration: none; 
					  -ms-interpolation-mode: bicubic; 
					  border: 0px solid orange; 
					  border-radius:8px; 
					  display: block;
					  float:left;
					  color: #000000;
				  }
				  td.p30-151{
					  padding: 70px 0px;
				  }
				  td.text-footer1.pb10{
					  color:#999999; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:16px; 
					  line-height:20px; 
					  text-align:left; 
					  padding-bottom:0px;
				  }
				  td.text-footer1.pb20{
					  color:#999999; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:16px; 
					  line-height:20px; 
					  text-align:left; 
					  padding-bottom:10px;
				  }
				  td.p30-152{
					  padding: 0px 0px;
				  }
				  td.sec3{
					  padding-bottom: 30px;
				  }
				  td.text1.pb20{
					  color:#000; 
					  font-family:'Noto Sans', Arial,sans-serif; 
					  font-size:24px; 
					  line-height:26px; 
					  text-align:left;
				  }
				  td.sec4{
					  font-size:0pt; 
					  line-height:0pt; 
					  text-align:left;
				  }
					</style>
				</head>
   
				<body class='body'>
					<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#f4f4f4'>
						<tr>
							<td align='center' valign='top'>
							<table width='650' border='0' cellspacing='0' cellpadding='0' class='mobile-shell'>
								<tr>
									<td class='td container'>
										<!-- Header -->
										<table width='100%' border='0' cellspacing='0' cellpadding='0'>
										<tr>
											<td class='sec1'>
												<table width='100%' border='0' cellspacing='0' cellpadding='0'>
													<tr>
													<td class='p30-15'>
														<table width='100%' border='0' cellspacing='0' cellpadding='0'>
															<tr>
																<th class='column-top' width='145'>
																<table width='100%' border='0' cellspacing='0' cellpadding='0 ' class='bottom-line'>
																	<tr>
																		<td class='img m-center'><img src='https://www.mysoultab.com/assets/images/logo.png' width='100' height='50' border='0' alt='' class='img m-center'/></td>
																	</tr>
																</table>
																</th>
															</tr>
														</table>
														<table width='100%' border='0' cellspacing='0' cellpadding='0'>
															<tr>
																<td class='h3 pb20'>Welcome to SoulTab</td>
															</tr>
															<tr>
																<td class='text pb20'>Hello,
																</td>
															</tr>
															<tr>
																<td class='text pb201'>We wanted to let you know that we have received your profile information.To view your profile : <a href='#'>mysoultab.com</a>
																</td>
															</tr>
															<tr>
																<td class='text pb202'>What's next? 
																</td>
															</tr>
															<tr>
																<td class='text pb201'>Thankyou for filling out your information.Please know that your information is safe and secure with us.
																</td>
															</tr>
															<tr>
																<td class='text pb201'>Here is your caregiver information:</a>
																</td>
															</tr>
															<tr>
																<td class='text1 pb201'>User Name:  $username
																</td>
															</tr>
															<tr>
																<td class='text1 pb201'>User Password:  $password
																</td>
															</tr>
															<tr>
																<td class='text1 pb2012'>User Email: $email
																</td>
															</tr>
															<tr>
																<td class='text pb201'>To sign into your account,please visit <a href='#'>www.mysoultab.com/login</a>
																</td>
															</tr>
															<tr>
																<td class='text pb201'>Call us at 847-450-1055 if you need any help.
																</td>
															</tr>
														</table>
														<section class='one1'>
															<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																<tr>
																<td class='p30-155' bgcolor='#ffffff'>
																	<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																		<tr>
																			<td align='center' valign='top' class='sec2' style=''><img border='0' vspace='0' hspace='0' class='sec2' src='https://www.mysoultab.com/assets/images/team_two.jpg'alt='D' width='100%'>
																			</td>
																		</tr>
																	</table>
																</td>
																</tr>
															</table>
														</section>
														<section class='two2'>
															<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																<tr>
																<td class='p30-151' bgcolor='#ffffff'>
																	<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																		<tr>
																			<td class='text-footer1 pb10'>Best Regards</td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb10'>ABC</td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20'>Position</td>
																		</tr>
																	</table>
																</td>
																</tr>
															</table>
														</section>
														<!-- END Article / Title + Copy + Button -->
														<!-- Footer -->
														<section class='one'>
															<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																<tr>
																<td class='p30-152' bgcolor='#ffffff'>
																	<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																		<tr>
																			<td align='left' class='sec3'>
																			<table>
																				<tr>
																					<td class='text1 pb20'>Technical Support</td>
																				</tr>
																			</table>
																			</td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20'>Mail us at:</td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20' >info@mysoultab.com</td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20'>Web :<a href='#'>mysoultab.com</a></td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20'>Tel no : 847 4501055</td>
																		</tr>
																	</table>
																</td>
																</tr>
															</table>
														</section>
														<section class='two'>
															<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																<tr>
																<td class='p30-152' bgcolor='#ffffff'>
																	<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																		<tr>
																			<td align='left' class='sec3'>
																			<table>
																				<tr>
																					<td class='text1 pb20'>Information</td>
																				</tr>
																			</table>
																			</td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20'>Contact us</td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20'>About us </td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20'>Privacy policy</td>
																		</tr>
																		<tr>
																			<td class='text-footer1 pb20'>Terms of Use</td>
																		</tr>
																	</table>
																</td>
																</tr>
															</table>
														</section>
														<table width='100%' border='0' cellspacing='0' cellpadding='0'>
															<tr>
																<td class='p30-152'  bgcolor='#ffffff'>
																<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																	<tr>
																		<td align='center'>
																			<table border='0' cellspacing='0' cellpadding='0'>
																			<tr>
																				<td class='img' width='55' class='sec4'><a href='#' target='_blank'><img src='https://www.mysoultab.com/assets/images/t8_ico_facebook.jpg' width='38' height='38' border='0' alt='' /></a></td>
																				<td class='img' width='55' class='sec4'><a href='#' target='_blank'><img src='https://www.mysoultab.com/assets/images/t8_ico_twitter.jpg' width='38' height='38' border='0' alt='' /></a></td>
																				<td class='img' width='55' class='sec4'><a href='#' target='_blank'><img src='https://www.mysoultab.com/assets/images/t8_ico_instagram.jpg' width='38' height='38' border='0' alt='' /></a></td>
																				<td class='img' width='38' class='sec4'><a href='#' target='_blank'><img src='https://www.mysoultab.com/assets/images/t8_ico_linkedin.jpg' width='38' height='38' border='0' alt='' /></a></td>
																			</tr>
																			</table>
																		</td>
																	</tr>
																</table>
																</td>
															</tr>
														</table>
													</td>
													</tr>
												</table>
											</td>
										</tr>
										</table>
										<!-- END Header -->
										<!-- Intro -->
										<!-- END Intro -->
										<!-- Article / Title + Copy + Button -->
										<!---end footer--->
									</td>
								</tr>
							</table>
							</td>
						</tr>
					</table>
				</body>
				</html>";
			/*end email sent code*/

				$chk_mail2 = new_send_mail($message, $subject,$email,$imageArray='');
			
			}
		
				
			if (!empty($insertId) || $insertId  !='') {
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'caregiver user profile add successfully', 'response' => array('fav_data' => "user profile successfully"));
			}else{
				$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
			}
			$this->response($resp);
		}
	}

	public function updateCaregiveruser_post(){
		// $pdata = file_get_contents("php://input");
		// $data = json_decode($pdata, true);
		$data = $this->param;
		$object_info = $data;
		$required_parameter = array("id", "name","lastname", "mobile", "street_name", "city", "state", "country","language","gender","timezone","zipcode","snt_txt_msg_alrt","snt_eml_alrt","snt_eml_alrt","snt_daily_eml_daily_rutin_alrt","snt_eml_no_activity_in_app_alrt","snt_eml_alrm_is_missed");
		$chk_error = check_required_value($required_parameter, $object_info);
		if ($chk_error) {
			$resp = array('status_code' => MISSING_PARAM, 'status' => 'false', 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
			$this->response($resp);
		}else{
			$update_array = array();
			$profile_image_name = '';
			//post data chk
				$update_user_id = isset($_POST['id']) ? $_POST['id'] : '';
				$name = isset($_POST['name']) ? $_POST['name'] : '';
				$lastname = isset($_POST['lastname']) ? $_POST['lastname'] : '';
				$mobile = isset($_POST['mobile']) ? $_POST['mobile'] : '';
				$street_name = isset($_POST['street_name']) ? $_POST['street_name'] : '';
				$city = isset($_POST['city']) ? $_POST['city'] : '';
				$state = isset($_POST['state']) ? $_POST['state'] : '';
				$country =isset($_POST['country']) ? $_POST['country'] : '';
				$language = isset($_POST['language']) ? $_POST['language'] : '';
				$gender = isset($_POST['gender']) ? $_POST['gender'] : '';
				$timezone = isset($_POST['timezone']) ? $_POST['timezone'] : 1;
				$zipcode = isset($_POST['zipcode']) ? $_POST['zipcode'] : '';
				$snt_txt_msg_alrt	=	isset($_POST['snt_txt_msg_alrt']) ? $_POST['snt_txt_msg_alrt'] : 1;
				$snt_eml_alrt		=	isset($_POST['snt_eml_alrt']) ? $_POST['snt_eml_alrt'] : 1;
				$snt_daily_eml_daily_rutin_alrt =	isset($_POST['snt_daily_eml_daily_rutin_alrt']) ? $_POST['snt_daily_eml_daily_rutin_alrt'] : 1;
				$snt_eml_no_activity_in_app_alrt =	isset($_POST['snt_eml_no_activity_in_app_alrt']) ? $_POST['snt_eml_no_activity_in_app_alrt'] : 2;
				$snt_eml_alrm_is_missed =	isset($_POST['snt_eml_alrm_is_missed']) ? $_POST['snt_eml_alrm_is_missed'] : 1;
				
				if(!empty($_FILES['profile_image']['name']) && !empty($_FILES['profile_image']['name']) ){
					$profile_image = trim($_FILES['profile_image']['name']);
					/*$update_array = array(
						'profile_image' => md5($profile_image)
					);*/
					if(!empty($_FILES['profile_image']['name']) && $_FILES['profile_image']['name'] !=''){
						$config['file_name']     = $_FILES['profile_image']['name'];
						$config['upload_path']   = 'uploads/profile_images/';
						$config['allowed_types'] = 'gif|jpg|jpeg|png';
						$config['max_size']      = '10000';
						$config['max_width']     = '0';
						$config['max_height']    = '0';
						$config['remove_spaces'] = true;
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						
						if (!$this->upload->do_upload('profile_image')) {
							$error = array('error' => $this->upload->display_errors());
							print_r($error);
							
						}else{
							
							if(!empty($exting_image) && $exting_image !=''){
								unlink('./uploads/profile_images/'.$exting_image);
							}
							
							$img = array('upload_data' => $this->upload->data());
							$profile_image_name = $img['upload_data']['file_name'];
						}
					}
				}
				
				if(!empty($_POST['password']) && $_POST['password'] !='' ){
					$password = trim($_POST['password']);
					$update_array = array(
						'password' => md5($password)
					);
				}
	
				//update array user
				$update_array	= array (
							'name' 		  		=> trim($name),
							'lastname' 			=> trim($lastname),
							'mobile' 			=> trim($mobile),
							'street_name'		=> trim($street_name),
							'city' 				=> trim($city),
							'state' 			=> trim($state),
							'country' 			=> trim($country),
							'language' 			=> trim($language),
							'gender' 			=> $gender,
							'timezone'			=> $timezone,
							'zipcode'			=> $zipcode,
							'snt_txt_msg_alrt' 	=> $snt_txt_msg_alrt,
							'snt_eml_alrt' 		=> $snt_eml_alrt,
							'snt_daily_eml_daily_rutin_alrt'	=> $snt_daily_eml_daily_rutin_alrt,
							'snt_eml_no_activity_in_app_alrt'	=> $snt_eml_no_activity_in_app_alrt,
							'snt_eml_alrm_is_missed'	=> $snt_eml_alrm_is_missed,
						//	'profile_image' => $profile_image_name,
			);
			if($profile_image_name != ''){
				$update_array['profile_image'] = $profile_image_name;
			}
			$update_Id = $this->common_model->updateRecords('cp_users', $update_array, array('id' => $update_user_id));
			
			if (!empty($update_Id) && $update_Id !='') {
				$userData = $this->common_model->getSingleRecordById('cp_users', array('id' => $update_user_id));
				
				//retuns result display in json
				$users1 = array(
							'id'				=>$userData['id'],
							'cp_users_id'		=>$userData['cp_users_id'],
							'name' 		  		=>$userData['name'],
							'lastname' 			=>$userData['lastname'],
							'mobile' 			=>$userData['mobile'],
							'street_name'		=>$userData['street_name'],
							'city' 				=>$userData['city'],
							'state' 			=>$userData['state'],
							'country' 			=>$userData['country'],
							'language' 			=>$userData['language'],
							'gender' 			=>$userData['gender'],
							'timezone'			=>$userData['timezone'],
							'zipcode'			=>$userData['zipcode'],
							'snt_txt_msg_alrt' 	=>$userData['snt_txt_msg_alrt'],
							'snt_eml_alrt' 		=>$userData['snt_eml_alrt'],
							'snt_daily_eml_daily_rutin_alrt'	=> $userData['snt_daily_eml_daily_rutin_alrt'],
							'snt_eml_no_activity_in_app_alrt'	=> $userData['snt_eml_no_activity_in_app_alrt'],
							'snt_eml_alrm_is_missed'	=> $userData['snt_eml_alrm_is_missed'],
							'profile_image' => $userData['profile_image'],
				);
				
				$resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'Caregiver profile update successfully', 'response' => $users1);
			
			}else{
				$resp = array('status_code' => ERROR, 'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));
			}
			$this->response($resp);
		}
	}

	public function emailTemp($user_id)
  {
  	$check_dailyroutine = $this->common_model->getsingle("cp_dailyroutine",array("created" => date('Y-m-d'),"user_id" => $user_id));
  	if(!empty($check_dailyroutine)){
        $morning = explode(",", $check_dailyroutine->morning);
        $noon  = explode(",", $check_dailyroutine->noon);
        $evening = explode(",", $check_dailyroutine->evening);
        $notes = explode(",", $check_dailyroutine->notes);
        $dinner = explode(",", $check_dailyroutine->dinner);
        $bedtime = explode(",", $check_dailyroutine->bedtime);

     } else {
        $morning = [];
        $noon  = [];
        $evening = [];
        $notes = [];
        $dinner = [];
        $bedtime = [];
     }

     $output = '<table class="table">
    <thead>
      <tr>
        <th>Time</th>
        <th>Meal</th>
        <th>Snack</th>
        <th>Medicine</th>
        <th>Walk</th>
        <th>Yoga</th>
        <th>Meditation</th>
        <th>Nap</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>morning</td>';

     
        $output = $output.'<td><input name="morning[]" type="checkbox" value="Meal"'; 
        if(in_array("Meal", $morning)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="morning[]" type="checkbox" value="Snack"'; 
        if(in_array("Snack", $morning)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="morning[]" type="checkbox" value="Medicine"'; 
        if(in_array("Medicine", $morning)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="morning[]" type="checkbox" value="Walk"'; 
        if(in_array("Walk", $morning)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="morning[]" type="checkbox" value="Yoga"'; 
        if(in_array("Yoga", $morning)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="morning[]" type="checkbox" value="Meditation"'; 
        if(in_array("Meditation", $morning)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="morning[]" type="checkbox" value="Nap"'; 
        if(in_array("Nap", $morning)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        
        $output = $output.'</tr><tr>
        <td>noon</td>';

        $output = $output.'<td><input name="noon[]" type="checkbox" value="Meal"'; 
        if(in_array("Meal", $noon)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="noon[]" type="checkbox" value="Snack"'; 
        if(in_array("Snack", $noon)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="noon[]" type="checkbox" value="Medicine"'; 
        if(in_array("Medicine", $noon)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="noon[]" type="checkbox" value="Walk"'; 
        if(in_array("Walk", $noon)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="noon[]" type="checkbox" value="Yoga"'; 
        if(in_array("Yoga", $noon)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="noon[]" type="checkbox" value="Meditation"'; 
        if(in_array("Meditation", $noon)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="noon[]" type="checkbox" value="Nap"'; 
        if(in_array("Nap", $noon)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'</tr><tr>
        <td>evening</td>';

         $output = $output.'<td><input name="evening[]" type="checkbox" value="Meal"'; 
        if(in_array("Meal", $evening)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="evening[]" type="checkbox" value="Snack"'; 
        if(in_array("Snack", $evening)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="evening[]" type="checkbox" value="Medicine"'; 
        if(in_array("Medicine", $evening)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="evening[]" type="checkbox" value="Walk"'; 
        if(in_array("Walk", $evening)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="evening[]" type="checkbox" value="Yoga"'; 
        if(in_array("Yoga", $evening)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="evening[]" type="checkbox" value="Meditation"'; 
        if(in_array("Meditation", $evening)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="evening[]" type="checkbox" value="Nap"'; 
        if(in_array("Nap", $evening)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'</tr><tr>
        <td>dinner</td>';

         $output = $output.'<td><input name="dinner[]" type="checkbox" value="Meal"'; 
        if(in_array("Meal", $dinner)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="dinner[]" type="checkbox" value="Snack"'; 
        if(in_array("Snack", $dinner)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="dinner[]" type="checkbox" value="Medicine"'; 
        if(in_array("Medicine", $dinner)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="dinner[]" type="checkbox" value="Walk"'; 
        if(in_array("Walk", $dinner)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="dinner[]" type="checkbox" value="Yoga"'; 
        if(in_array("Yoga", $dinner)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="dinner[]" type="checkbox" value="Meditation"'; 
        if(in_array("Meditation", $dinner)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="dinner[]" type="checkbox" value="Nap"'; 
        if(in_array("Nap", $dinner)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'</tr><tr>
        <td>bedtime</td>';
        
      	$output = $output.'<td><input name="bedtime[]" type="checkbox" value="Meal"'; 
        if(in_array("Meal", $bedtime)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="bedtime[]" type="checkbox" value="Snack"'; 
        if(in_array("Snack", $bedtime)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="bedtime[]" type="checkbox" value="Medicine"'; 
        if(in_array("Medicine", $bedtime)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="bedtime[]" type="checkbox" value="Walk"'; 
        if(in_array("Walk", $bedtime)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="bedtime[]" type="checkbox" value="Yoga"'; 
        if(in_array("Yoga", $bedtime)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="bedtime[]" type="checkbox" value="Meditation"'; 
        if(in_array("Meditation", $bedtime)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'<td><input name="bedtime[]" type="checkbox" value="Nap"'; 
        if(in_array("Nap", $bedtime)){
             $output = $output."checked";
        } 
        $output = $output.'></td>';

        $output = $output.'</tr> </tbody>
  		</table>';

  		return $output;
  }

  	public function sendFax_post(){
		$data = array();
		//$userid= isset($_POST['id']) ? $_POST['id'] : '';
		$userid=878;
		//$doctor_id= isset($_POST['doctor_id']) ? $_POST['doctor_id'] : '';
		$doctor_id= 32;
		//$dr_appointment_id= isset($_POST['dr_appointment_id']) ? $_POST['dr_appointment_id'] : '';
		$dr_appointment_id=427 ;
		if(empty($userid) && $userid =='' || empty($doctor_id) && $doctor_id ==''  ||  empty($dr_appointment_id) && $dr_appointment_id ==''){
			
			$faxdetails = array();
			$response['message'] = "userid:doctor_id:dr_appointment_id:Required parameter missing ";
			$response['error'] = 0;
			$response['ok'] = 400;
			$response['data'] = array('faxdetails' => $faxdetails);
			echo (json_encode($response));
			exit;
		}else{
		
			if(!empty($userid) && $userid !=''){
				$data = $this->common_model->getsingle("cp_users", array("id" => $userid));
				$username = $data->username;
				$mobile = $data->mobile;

				$data = array(
					'username' => trim($username),
					'mobile' => trim($mobile),
				);
			}
				
			if(!empty($dr_appointment_id) && $dr_appointment_id !=''){
				$data = $this->common_model->getsingle("doctor_appointments", array("dr_appointment_id" => $dr_appointment_id));
				
				$dr_appointment_date_id = $data->dr_appointment_date_id;
				$dr_appointment_time_id = $data->dr_appointment_time_id;
				
				$data = array(
					'dr_appointment_date_id' => $dr_appointment_date_id,
					'dr_appointment_time_id' => $dr_appointment_time_id,
				);
			}
			
			if(!empty($doctor_id) && $doctor_id !=''){
				$data = $this->common_model->getsingle("cp_doctor", array("doctor_id" => $doctor_id));	
				$doctor_name = $data->doctor_name;
				$fax_num = $data->fax_num;

				$data = array(
					'doctor_name' => trim($doctor_name),
					'fax_num' => trim($fax_num),
				);
			}	
			if(!empty($userid) && $userid !='' && !empty($dr_appointment_id) && $dr_appointment_id !='' && !empty($doctor_id) && $doctor_id !=''){
				
				$data =array(
					'username'				 => $username,
					'mobile' 				 => $mobile,
					'dr_appointment_date_id' => $dr_appointment_date_id,
					'dr_appointment_time_id' => $dr_appointment_time_id,
					'doctor_name' 			 => $doctor_name,
					'fax_num'				 => $fax_num,
				);

				$response = $data;

				$rand= rand(0,10000);
				$output_pdf_name = $username.''.$rand.'_'.$dr_appointment_date_id.'_'.$dr_appointment_time_id;
				$pdfFilePath = APPPATH."output_pdf_name.pdf";
				
				$this->load->library('m_pdf');
				$mpdf = new m_pdf('bn', 'Legal');
				
				$obj = $mpdf->load();
				$obj->SetImportUse();
				$obj->AddPage('L');
				$html = $this->load->view('ViwewFileLocation', $data,true); 
				$obj->WriteHTML($html);
				$obj->Output($pdfFilePath,"F");
				require_once(APPPATH . '/third_party/interfax/vendor/autoload.php');
				$interfax = new Interfax\Client(['username' => 'jeet.singh', 'password' => '!nt3rFax65']);
				// check balance of $interfax->getBalance();
				try {
					//$fax = $interfax->deliver(['faxNumber' => '+12242035560', 'file' => $pdfFilePath]);
					//$fax = $interfax->deliver(['faxNumber' => $fax_num, 'file' => $pdfFilePath]);
				} catch (Interfax\Exception\RequestException $e) {
					echo $e->getMessage();
					// contains text detail that is available
					echo $e->getStatusCode();
					// the http status code that was received
					throw $e->getWrappedException();
					// The underlying Guzzle exception that was caught by the Interfax Client.
				}
				//$obj->Output($pdfFilePath, "f");

				$response['message'] = "sendFax Successfully";
				$response['ok'] = 1;
				$response['data'] = array('sendFax' => $response);
				echo (json_encode($response['data']));
				exit;
				
			}else{
				
				$faxdetails = array();
				$response['message'] = "userid:doctor_id:dr_appointment_id:Required parameter missing ";
				$response['error'] = 0;
				$response['ok'] = 400;
				$response['data'] = array('faxdetails' => $faxdetails);
				echo (json_encode($response));
				exit;
			}
		}
	}

	public function missedalarm_post(){

		$data = array();

		$user_id = isset($this->param["id"]) ? $this->param["id"] : '';
		if(empty($user_id) && $user_id ==''){
			
		}else{
			$data = $this->common_model->getsingle("cp_users", array("id" => $user_id));

			if(!empty($data) && $data !='' ){

			$user_id = $data->id;
			$snt_eml_alrm_is_missed = $data->snt_eml_alrm_is_missed;
			$email = $data->email;
			$update_user = $data->update_user;
			
			if(!empty($snt_eml_alrm_is_missed) && $snt_eml_alrm_is_missed !='' && $snt_eml_alrm_is_missed ==1 || $snt_eml_alrm_is_missed =='1'){

			/*start email sent code*/
			$subject ="Medicine alram";
			$message ="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
			<html xmlns='http://www.w3.org/1999/xhtml'>
			<head>
				<link href='https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i' rel='stylesheet' />
				<link rel='stylesheet' type='text/css' href='https://www.mysoultab.com/assets/styles/caregivermail.css'>
				<title></title>
				<style>
				section {
					width: 40%;
					min-height: 200px;
					height: 200px;
					color: black;
					
					padding: 5%;
					padding-left: 0px;
					float: left;
					background-color: white;
			}
			section.two {
					width: 40%;
					min-height: 200px;
					height: 200px;
					color: black;
					
					padding: 5%;
					
					float: left;
					background-color: white;
			}
			section.one1{
				width: 20%;
					min-height: 100px;
					height: 100px;
					color: black;
					
					padding: 5%;
					padding-left:0px;
					float: left;
					background-color: white;
			}
			section.two2{
				width: 60%;
					min-height: 100px;
					height: 100px;
					color: black;
					
					padding: 5%;
					padding-left: 0px;
					float: left;
					background-color: white;
			}
			.bottom-line {

					border-bottom: 1px solid #000;
					padding-bottom: 20px;
			}
			.body {

				padding:0 !important;
				margin:0 !important; 
				display:block !important; 
				min-width:100% !important; 
				width:100% !important; 
				background:#f4f4f4; 
				-webkit-text-size-adjust:none;
			}

			.td.container {
				width:650px; 
				min-width:650px; 
				font-size:0pt; 
				line-height:0pt; 
				margin:0; 
				font-weight:normal; 
				padding:55px 0px;
			}

			td.sec1{
				padding-bottom: 20px;
			}

			td.p30-15 {
				padding: 25px 30px 25px 30px; 
				background: #fff;
			}
			th.column-top {
				font-size:0pt; 
				line-height:0pt; 
				padding:0; 
				margin:0; 
				font-weight:normal; 
				vertical-align:top;
			}
			td.img.m-center {
				font-size:0pt; 
				line-height:0pt; 
				text-align:left;
			}
			img.img.m-center {
				padding-left: 230px;
			}
			td.h3.pb20{
				color:#114490; 
				font-family:'Noto Sans', Arial,sans-serif; 
				font-size:24px; 
				line-height:32px; 
				text-align:center; 
				padding-bottom:0px;
				padding-top: 10px
			}
			td.text.pb20{
				color:#000; 
				font-family:'Noto Sans', Arial,sans-serif; 
				font-size:14px; 
				line-height:26px; 
				text-align:left; 
				padding-bottom:10px;
				font-weight: 700;
			}
			td.text.pb201{
				color:#777777; 
				font-family:'Noto Sans', Arial,sans-serif; 
				font-size:14px; 
				line-height:26px; 
				text-align:left; 
				padding-bottom:10px;
			}
			td.text.pb202{
				color:#000; 
				font-family:'Noto Sans', Arial,sans-serif; 
				font-size:24px; 
				line-height:26px; 
				text-align:left; 
				padding-bottom:10px;
			}
			td.text1.pb201{
				color:#000; 
				font-family:'Noto Sans', Arial,sans-serif; 
				font-size:14px; 
				line-height:26px; 
				text-align:center; 
				padding-top: 5px; 
				font-weight: 600;
			}
			td.text1.pb2012{
				color:#000; 
				font-family:'Noto Sans', Arial,sans-serif; 
				font-size:14px; 
				line-height:26px; 
				text-align:center; 
				padding-top: 5px; 
				font-weight: 600;
				padding-bottom: 15px;
			}
			td.p30-155{
				padding: 5px 5px ;
			}
			td.sec2{
				border-collapse: collapse; 
				border-spacing: 0;

			}
			img.sec2{
				padding: 0px; 
				margin: 0; 
				outline: none; 
				text-decoration: none; 
				-ms-interpolation-mode: bicubic; 
				border: 0px solid orange; 
				border-radius:8px; 
				display: block;
				float:left;
				color: #000000;
			}
			td.p30-151{
				padding: 70px 0px;
			}
			td.text-footer1.pb10{
				color:#999999; 
				font-family:'Noto Sans', Arial,sans-serif; 
				font-size:16px; 
				line-height:20px; 
				text-align:left; 
				padding-bottom:0px;
			}
			td.text-footer1.pb20{
				color:#999999; 
				font-family:'Noto Sans', Arial,sans-serif; 
				font-size:16px; 
				line-height:20px; 
				text-align:left; 
				padding-bottom:10px;
			}
			td.p30-152{
				padding: 0px 0px;
			}
			td.sec3{
				padding-bottom: 30px;
			}
			td.text1.pb20{
				color:#000; 
				font-family:'Noto Sans', Arial,sans-serif; 
				font-size:24px; 
				line-height:26px; 
				text-align:left;
			}
			td.sec4{
				font-size:0pt; 
				line-height:0pt; 
				text-align:left;
			}
				</style>
			</head>

			<body class='body'>
				<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#f4f4f4'>
					<tr>
						<td align='center' valign='top'>
						<table width='650' border='0' cellspacing='0' cellpadding='0' class='mobile-shell'>
							<tr>
								<td class='td container'>
									<!-- Header -->
									<table width='100%' border='0' cellspacing='0' cellpadding='0'>
									<tr>
										<td class='sec1'>
											<table width='100%' border='0' cellspacing='0' cellpadding='0'>
												<tr>
												<td class='p30-15'>
													<table width='100%' border='0' cellspacing='0' cellpadding='0'>
														<tr>
															<th class='column-top' width='145'>
															<table width='100%' border='0' cellspacing='0' cellpadding='0 ' class='bottom-line'>
																<tr>
																	<td class='img m-center'><img src='https://www.mysoultab.com/assets/images/logo.png' width='100' height='50' border='0' alt='' class='img m-center'/></td>
																</tr>
															</table>
															</th>
														</tr>
													</table>
													<table width='100%' border='0' cellspacing='0' cellpadding='0'>
														<tr>
															<td class='h3 pb20'>Welcome to SoulTab</td>
														</tr>
														<tr>
															<td class='text pb20'>Hello,
															</td>
														</tr>
														<tr>
															<td class='text pb201'>We wanted to let you know that we have received your profile information.To view your profile : <a href='#'>mysoultab.com</a>
															</td>
														</tr>
														<tr>
															<td class='text pb202'>What's next? 
															</td>
														</tr>
														<tr>
															<td class='text pb201'>Thankyou for filling out your information.Please know that your information is safe and secure with us.
															</td>
														</tr>
														<tr>
															<td class='text pb201'>Here is your missed alarm information:</a>
															</td>
														</tr>
														<tr>
														<td class='text1 pb201'>Title: Missed Medicine Alram
														</td>
														</tr>
														<tr>
															<td class='text1 pb201'>Time:  $update_user
															</td>
														</tr>
														<tr>
															<td class='text1 pb201'>alert mail id:  $email
															</td>
														</tr>
														<tr>
															<td class='text1 pb2012'>you missed medicine taken
															</td>
														</tr>
														<tr>
															<td class='text pb201'>To sign into your account,please visit <a href='#'>www.mysoultab.com/login</a>
															</td>
														</tr>
														<tr>
															<td class='text pb201'>Call us at 847-450-1055 if you need any help.
															</td>
														</tr>
													</table>
													<section class='one1'>
														<table width='100%' border='0' cellspacing='0' cellpadding='0'>
															<tr>
															<td class='p30-155' bgcolor='#ffffff'>
																<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																	<tr>
																		<td align='center' valign='top' class='sec2' style=''><img border='0' vspace='0' hspace='0' class='sec2' src='https://www.mysoultab.com/assets/images/team_two.jpg'alt='D' width='100%'>
																		</td>
																	</tr>
																</table>
															</td>
															</tr>
														</table>
													</section>
													<section class='two2'>
														<table width='100%' border='0' cellspacing='0' cellpadding='0'>
															<tr>
															<td class='p30-151' bgcolor='#ffffff'>
																<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																	<tr>
																		<td class='text-footer1 pb10'>Best Regards</td>
																	</tr>
																	<tr>
																		<td class='text-footer1 pb10'>ABC</td>
																	</tr>
																	<tr>
																		<td class='text-footer1 pb20'>Position</td>
																	</tr>
																</table>
															</td>
															</tr>
														</table>
													</section>
													<!-- END Article / Title + Copy + Button -->
													<!-- Footer -->
													<section class='one'>
														<table width='100%' border='0' cellspacing='0' cellpadding='0'>
															<tr>
															<td class='p30-152' bgcolor='#ffffff'>
																<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																	<tr>
																		<td align='left' class='sec3'>
																		<table>
																			<tr>
																				<td class='text1 pb20'>Technical Support</td>
																			</tr>
																		</table>
																		</td>
																	</tr>
																	<tr>
																		<td class='text-footer1 pb20'>Mail us at:</td>
																	</tr>
																	<tr>
																		<td class='text-footer1 pb20' >info@mysoultab.com</td>
																	</tr>
																	<tr>
																		<td class='text-footer1 pb20'>Web :<a href='#'>mysoultab.com</a></td>
																	</tr>
																	<tr>
																		<td class='text-footer1 pb20'>Tel no : 847 4501055</td>
																	</tr>
																</table>
															</td>
															</tr>
														</table>
													</section>
													<section class='two'>
														<table width='100%' border='0' cellspacing='0' cellpadding='0'>
															<tr>
															<td class='p30-152' bgcolor='#ffffff'>
																<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																	<tr>
																		<td align='left' class='sec3'>
																		<table>
																			<tr>
																				<td class='text1 pb20'>Information</td>
																			</tr>
																		</table>
																		</td>
																	</tr>
																	<tr>
																		<td class='text-footer1 pb20'>Contact us</td>
																	</tr>
																	<tr>
																		<td class='text-footer1 pb20'>About us </td>
																	</tr>
																	<tr>
																		<td class='text-footer1 pb20'>Privacy policy</td>
																	</tr>
																	<tr>
																		<td class='text-footer1 pb20'>Terms of Use</td>
																	</tr>
																</table>
															</td>
															</tr>
														</table>
													</section>
													<table width='100%' border='0' cellspacing='0' cellpadding='0'>
														<tr>
															<td class='p30-152'  bgcolor='#ffffff'>
															<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																<tr>
																	<td align='center'>
																		<table border='0' cellspacing='0' cellpadding='0'>
																		<tr>
																			<td class='img' width='55' class='sec4'><a href='#' target='_blank'><img src='https://www.mysoultab.com/assets/images/t8_ico_facebook.jpg' width='38' height='38' border='0' alt='' /></a></td>
																			<td class='img' width='55' class='sec4'><a href='#' target='_blank'><img src='https://www.mysoultab.com/assets/images/t8_ico_twitter.jpg' width='38' height='38' border='0' alt='' /></a></td>
																			<td class='img' width='55' class='sec4'><a href='#' target='_blank'><img src='https://www.mysoultab.com/assets/images/t8_ico_instagram.jpg' width='38' height='38' border='0' alt='' /></a></td>
																			<td class='img' width='38' class='sec4'><a href='#' target='_blank'><img src='https://www.mysoultab.com/assets/images/t8_ico_linkedin.jpg' width='38' height='38' border='0' alt='' /></a></td>
																		</tr>
																		</table>
																	</td>
																</tr>
															</table>
															</td>
														</tr>
													</table>
												</td>
												</tr>
											</table>
										</td>
									</tr>
									</table>
									<!-- END Header -->
									<!-- Intro -->
									<!-- END Intro -->
									<!-- Article / Title + Copy + Button -->
									<!---end footer--->
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
			</body>
			</html>";
			/*end email sent code*/

			$chk_mail2 = new_send_mail($message,$subject,$email,$imageArray='');

				
				$medicinearr = array(
					'id' => $user_id,
					'snt_eml_alrm_is_missed' => $snt_eml_alrm_is_missed,
					'email' => $email,
					'update_user' => $update_user,
				);

				$response['message'] = "medicine alram missed alert sent Successfully";
				$response['ok'] = 1;
				$response['data'] = array('medicine_alram' => $medicinearr);
				echo (json_encode($response));
				exit;

				}else{
					$response[] = array();
					$response['message'] = "id:user has not missed medicine taken";
					$response['error'] = 0;
					$response['ok'] = 400;
					$response['data'] = array('medicine_alram' => $response);
					echo (json_encode($response));
					exit;
				}

			}
			
		}
	}

}
