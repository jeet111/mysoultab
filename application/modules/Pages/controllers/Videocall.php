<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Videocall extends MX_Controller {
  public function __construct() {
    parent:: __construct();
    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->library('table');
    $this->load->library('pagination');
    $this->load->model('common_model');
    $this->load->model('common_model_new');
    $this->load->model('Common_model_new');
    $this->load->helper(array('common_helper'));
    $this->load->helper(array('url'));
    $this->load->helper(array('form'));
  

    not_login();
  }

  public function videocall(){ 
    $checkLogin = $this->session->userdata('logged_in');
    $user_id = $this->session->userdata('logged_in')['id'];

    if(!empty($checkLogin)) 
    {
      $data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'sprituality',"user_id"=>$user_id));
      if($this->session->userdata('logged_in')['user_role'] == "2"){
        $data['userDetail_cp'] = $this->common_model_new->getAllwhere('cp_users', array('cp_users_id' => $this->session->userdata('logged_in')['id']));
      }else if($this->session->userdata('logged_in')['user_role'] == "4"){
        $data['userDetail_cp'] = $this->common_model->getParentDetail();
      }
      $data['callrecorddata'] = $this->common_model_new->getAllwhere('cp_video_call_record', array('user_id' => $this->session->userdata('logged_in')['id']));
      $this->template->set('title', 'Video call');
      // $this->template->load('user_dashboard_layout', 'contents', 'sprituality_view',$data); 
      $this->template->load('new_user_dashboard_layout', 'contents', 'videocall',$data); 
      
    }else{
      redirect('login');
    }
  }

  public function addvideocallrecord()
  {
    if(count($_POST)>0){
        $array =array(
           'user_id' => $this->session->userdata('logged_in')['id'],
           'name' => $this->input->post('name'),
           'date_recieved' => $this->input->post('date_recieved'),
           'status'=>$this->input->post('status'),
           'duration' => $this->input->post('duration'),
           'created' => date('Y-m-d H:i:s'));
      
       $insert = $this->Common_model_new->insertData('cp_video_call_record',$array);
       if($insert){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        $this->session->set_flashdata('success', "Video call record added successfully");
        echo json_encode(array("statusCode"=>200,"msg"=>"Video call record added successfully"));
        die();
      }

    }
  }
}
?>
