<div class="container-fluid">
	<div class="dash-counter users-main my-sts-table">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<div class="status-cng"></div>
						<?php if ($this->session->flashdata('success')) { ?>
							<div class="alert alert-success message">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<?php echo $this->session->flashdata('success'); ?></div>
							<?php } ?>

							<a style="float: right;" class="cancel-btn btn" href="javascript:window.history.back()">
								Back
							</a>

							<?php //echo "<pre>"; print_r($shedule_times); echo "</pre>"; ?>

							<table class="table table-hover tab_comn" id="sampleTable">
								<thead>
									<tr>
										<th>S no.</th>
										<th>Schedule Dates</th>
										<th>Schedule Times</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i = 1;
									foreach ($shedule_times as $shedule) {

										?>
										<tr>
											<td><?php echo $i++; ?></td>
											<td><?php echo date('d-m-Y',strtotime($shedule['avdate_date'])) ; ?></td>
											<td><?php echo $shedule['avtime_text']; ?></td>
											<td>

												<div class="link-del-view">

													<div class="tooltip-2">

														<a href="admin/edit_schedule_time/<?php echo $shedule['avtime_id']; ?>/<?php echo $this->uri->segment(3); ?>/<?php echo $this->uri->segment(4); ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>
														<span class="tooltiptext">Edit</span>

													</div>


													<div class="tooltip-2">

														<a href="admin/delete_shedule_time/<?php echo $shedule['avtime_id']; ?>" onclick="return confirm('Delete this shedule date ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

													</div>


												</div>


											</td>

										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



