<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Internet extends MX_Controller {



	public function __construct() {

		parent:: __construct();

		$this->load->library('session');

		$this->load->library('form_validation');

		$this->load->model('activities_model');

		$this->load->model('Common_model_new');

		$this->load->model('common_model_new');

		$this->load->model('movie_model');

		$this->load->helper(array('common_helper'));

		$this->load->helper(array('url'));

		$this->load->helper(array('form'));

		not_login();

	}



   public function internet_dashboard(){
    $data['menuactive'] = '';
    $checkLogin = $this->session->userdata('logged_in');
		
    if(!empty($checkLogin))
    {
      $data['menuactive'] = $this->uri->segment(1);
     
      unset($_POST);
			
			
      $data['app_list'] = $this->common_model_new->getAllwhere("app_list",array('status' => 1,'use_for'=>"internet"));
      $data['internet_list'] = $this->common_model_new->getAllwhere("cp_internet",array('user_id'=>$this->session->userdata('logged_in')['id']));
			$data['InternetArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'Internet',"user_id"=>$this->session->userdata('logged_in')['id']));
   
			$this->template->set('title', 'Dashboard Game');

      $this->template->load('new_user_dashboard_layout', 'contents', 'internet', $data);
    } 
    else{

      redirect('login');
  
    } 
  }

  public function add_internet()
  {
    if(count($_POST)>0){
        if($_POST['type']==1){
      
        
        $array =array(
           'user_id' => $this->session->userdata('logged_in')['id'],
           'internet_type' => $this->input->post('internet_type'),
           'button_name' => $this->input->post('button_name'),
           'url'=>$this->input->post('url'),
           'app_name' => $this->input->post('app_name'),
           'created' => date('Y-m-d H:i:s'));
       }

       $check_internet = $this->Common_model_new->getsingle("cp_internet",array("button_name" => $this->input->post('button_name'),"user_id" => $this->session->userdata('logged_in')['id']));
      if(!empty($check_internet)){
        unset($_POST);
        $this->session->set_flashdata('success', "Internet already added");
        echo json_encode(array("statusCode"=>200,"msg"=>"internet already added"));
        die();
      }

       $insert = $this->Common_model_new->insertData('cp_internet',$array);
       if($insert){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        $this->session->set_flashdata('success', "Internet added successfully");
        echo json_encode(array("statusCode"=>200,"msg"=>"internet added successfully"));
        die();
      }

    }
  }

  public function edit_internet()
  {
    if(count($_POST)>0){
        if($_POST['type']==2){
        $internet_id = $this->input->post('id');
        
        $array =array(
           'user_id' => $this->session->userdata('logged_in')['id'],
           'internet_type' => $this->input->post('internet_type'),
           'button_name' => $this->input->post('button_name'),
           'url'=>$this->input->post('url'),
           'app_name' => $this->input->post('app_name'),
           'created' => date('Y-m-d H:i:s'));
       }

       $check_internet = $this->Common_model_new->allexcludeone("cp_internet",array("button_name" => $this->input->post('button_name'),"user_id" => $this->session->userdata('logged_in')['id']),$internet_id);
      if(!empty($check_internet)){
        unset($_POST);
        $this->session->set_flashdata('success', "Internet already added");
        echo json_encode(array("statusCode"=>200,"msg"=>"internet already added"));
        die();
      }

       $update = $this->Common_model_new->updateData('cp_internet',$array,array('id' => $internet_id));
       if($update){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        $this->session->set_flashdata('success', "Internet updated successfully");
        echo json_encode(array("statusCode"=>200,"msg"=>"internet updated successfully"));
        die();
      }

    }
  }

  public function delete_internet()
  {
    if(count($_POST)>0){
        if($_POST['type']==3){
        $internet_id = $this->input->post('id');

       $update = $this->Common_model_new->delete('cp_internet',array('id' => $internet_id));
       if($update){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        echo json_encode(array("statusCode"=>200,"msg"=>"internet deleted successfully"));
        die();
      }

    }
	}
	


	
}

	



}
