
				
				<?php  if($category_search){ 	foreach ($category_search as $key => $value) {
				 ?>
				<div class="col-lg-3 col-sm-6 col-md-3 col-xs-12">
					<article class="themetechmount-box themetechmount-box-team themetechmount-teambox-view-overlay">
						<div class="themetechmount-post-item">
							<div class="themetechmount-team-image-box">
								<span class="themetechmount-item-thumbnail">
									<span class="themetechmount-item-thumbnail-inner">
										<img src="<?php echo base_url();?>uploads/doctor_category/<?php echo $value['category_image']?>" class="attachment-themetechmount-img-teamoverlay size-themetechmount-img-teamoverlay wp-post-image" alt="">
									</span>
								</span>
								<div class="themetechmount-overlay">
									<div class="themetechmount-icon-box themetechmount-box-link">
									</div>
								</div>
							</div>
							<div class="themetechmount-box-content">
								<div class="themetechmount-box-title">
									<h4>
										<a href="<?php echo base_url()?>view_doctor/<?php echo $value['doctor_id'];  ?>"><?php 	echo $value['doctor_name']; ?></a>
									</h4>
								</div>
								<div class="themetechmount-team-position">
								<?php echo $value['doctor_category']; ?>
								</div>
							</div>
						</div>
					</article>
				</div>
			<?php 	}} else{ ?>
				<div class="photo-list-empty">
		    	<span class="blnk_photo"><i class="fa fa-picture-o" aria-hidden="true"></i></span>
		    	<h4>Doctors not found</h4>
		    </div>

			<?php } ?>
			
