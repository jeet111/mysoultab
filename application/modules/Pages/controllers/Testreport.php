<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Testreport extends MX_Controller {
  public function __construct() {
    parent:: __construct();
    $this->load->library('session');
    $this->load->library('form_validation');
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');       
    $this->load->model('testreport_model');
    $this->load->model('Common_model_new');
    $this->load->helper(array('common_helper'));
    $this->load->helper(array('url'));
    $this->load->helper(array('form'));      
  }
  /*Created by 95 for show all the test report by user*/
  public function testreport_list()
  {
    $data['menuactive'] = $this->uri->segment(1);
    $checkLogin = $this->session->userdata('logged_in');
    if(!empty($checkLogin))
    {
      $data['TestReportData']  = $this->testreport_model->getAllTestReports($this->session->userdata('logged_in')['id']);
      $data['btnsetingArray'] = $this->Common_model_new->getAllwhere("setting_butns",array("btnname" =>'Test Report','user_id'=>$checkLogin['id']));
      $this->template->set('title', 'Test Report');
      $this->template->load('new_user_dashboard_layout', 'contents', 'testreport_list',$data);
    }else{
     redirect('login'); 
   }
 }
 /*Created by 95 for delete multiple test reports*/
 public function testreports_all_del()
 {
  $bodytype = $this->input->post("bodytype");
  $explode = explode(',',$bodytype);
  foreach($explode as $del){
    $this->testreport_model->delete("cp_test_report",array("id" => $del));
  }
  echo '1';
}
/*Created by 95 for single testreport delete*/
public function delete_testreport()
{
  $delete_id = $this->uri->segment('2');
  $this->testreport_model->delete("cp_test_report",array("id" => $delete_id));
  $this->session->set_flashdata('success', 'Successfully deleted');
  redirect('testreport_list'); 
}
/*Created by 95 for add new test report*/
public function add_testreport()
{
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  $other_doctor_name=$this->input->post('other_doctor_name');
  $other_testreport_name=$this->input->post('other_testreport_name');
  $other_doctor_email_address=$this->input->post('other_doctor_email_address');
  $other_doctor_fax_number=$this->input->post('other_doctor_fax_number');
  $other_doctor_address=$this->input->post('other_doctor_address');

  $other_doctor_phone_number=$this->input->post('other_doctor_phone_number');


  if(!empty($checkLogin))
  {
    // $this->form_validation->set_rules('report_title', 'Report title', 'required');
    // $this->form_validation->set_rules('doctor', 'Doctor', 'required');
    // $this->form_validation->set_rules('test_type', 'Test type', 'required');
    $this->form_validation->set_rules('report_description', 'Report Description', 'required');
    if ($this->form_validation->run() == TRUE) 
    {
      if(isset($_POST['submit'])){
        //Check whether user upload picture
        if(!empty($_FILES['picture']['name'])){
          $config['upload_path'] = 'uploads/test_report/';
          $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|txt|doc|docx';
          $config['file_name'] = $_FILES['picture']['name'];
                //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('picture')){
            $uploadData = $this->upload->data();
            $picture = $uploadData['file_name'];
          }else{
            $picture = '';
          }
        }else{
          $picture = '';
        }
        if(!empty($other_doctor_name) && !empty( $other_testreport_name)){
          $insert1 = $this->testreport_model->insertData('cp_doctor',array('other_doctor_name'=>$other_doctor_name,'doctor_email'=>$other_doctor_email_address,'fax_num'=>$other_doctor_fax_number,'doctor_address'=>$other_doctor_address,'doctor_mob_no'=>$other_doctor_phone_number));
          $doctor_id = $this->db->insert_id();
          $insert2 = $this->testreport_model->insertData('cp_test_type',array('other_test_type'=>$other_testreport_name));
          $report_id = $this->db->insert_id();
          $data=array(
            'doctor_id'=> $doctor_id,
            'test_report_title'=>$this->input->post('report_title'),
            'other_doctor_name'=>$this->input->post('other_doctor_name'),

            'other_testreport_name'=>$this->input->post('other_testreport_name'),
            'user_id'=>$this->session->userdata('logged_in')['id'],
            'test_type_id'=>$report_id,
            'image'=>$picture,
            'description'=>$this->input->post('report_description'),
            'create_date'=>date('Y-m-d H:i:s'),
          );
          $insert = $this->testreport_model->insertData('cp_test_report',$data);
          if($insert){
            $this->session->set_flashdata('success','Test report added successfully');
            redirect('testreport_list');
          }
        }else{
          $data=array(
            'doctor_id'=>$this->input->post('doctor'),
            'test_report_title'=>$this->input->post('report_title'),
            'user_id'=>$this->session->userdata('logged_in')['id'],
            'test_type_id'=>$this->input->post('test_type'),
            'image'=>$picture,
            'description'=>$this->input->post('report_description'),
            'create_date'=>date('Y-m-d H:i:s'),
          );
          $insert = $this->testreport_model->insertData('cp_test_report',$data);
          if($insert){
            $this->session->set_flashdata('success','Test report added successfully');
            redirect('testreport_list');
          }
        }
    
      }
    }
    //$data['reportData'] = $this->testreport_model->getTestReport($report_id);
    $data['Testtypes']  = $this->testreport_model->getAllor('cp_test_type','test_type','asc');
    $data['Doctors']  = $this->testreport_model->getAllor('cp_doctor','doctor_name','asc');
    $this->template->set('title', 'Test Report');
    $this->template->load('new_user_dashboard_layout', 'contents', 'add_testreport',$data);
  }else{
   redirect('login'); 
 }
}
/*Created by 95 for edit test report by user*/
public function edit_testreport($report_id)
{
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    if(isset($_POST['submit'])){
      $this->form_validation->set_rules('report_title', 'Report title', 'required');
      $this->form_validation->set_rules('doctor', 'Doctor', 'required');
      $this->form_validation->set_rules('test_type', 'Test type', 'required');
      $this->form_validation->set_rules('report_description', 'Report Description', 'required');
      if ($this->form_validation->run() == TRUE) 
      {
  //Check whether user upload picture
        if(!empty($_FILES['picture']['name'])){
          $config['upload_path'] = 'uploads/test_report/';
          $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|txt|doc|docx';
          $config['file_name'] = $_FILES['picture']['name'];
                //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('picture')){
            $uploadData = $this->upload->data();
            $picture = $uploadData['file_name'];
          }else{
            $picture = '';
          }
          $data=array(
            'doctor_id'=>$this->input->post('doctor'),
            'test_report_title'=>$this->input->post('report_title'),
            'user_id'=>$this->session->userdata('logged_in')['id'],
            'test_type_id'=>$this->input->post('test_type'),
            'image'=>$picture,
            'description'=>$this->input->post('report_description'),
            'create_date'=>date('Y-m-d H:i:s'),
          );
          $this->testreport_model->updateData('cp_test_report',$data,array('id' => $report_id));
          $this->session->set_flashdata('success','Test report updated successfully');
          redirect('testreport_list');
        }else{
          $picture = '';
        }
        $data=array(
          'doctor_id'=>$this->input->post('doctor'),
          'test_report_title'=>$this->input->post('report_title'),
          'user_id'=>$this->session->userdata('logged_in')['id'],
          'test_type_id'=>$this->input->post('test_type'),
          'description'=>$this->input->post('report_description'),
          'create_date'=>date('Y-m-d H:i:s'),
        );
        $insert= $this->testreport_model->updateData('cp_test_report',$data,array('id' => $report_id));
       //$insert = $this->testreport_model->insertData('cp_test_report',$data);
        if($insert){
          $this->session->set_flashdata('success','Test report updated successfully');
          redirect('testreport_list');
        }
      }
    }
    $data['reportData'] = $this->testreport_model->getTestReport($report_id);
    $data['Testtypes']  = $this->testreport_model->getAllor('cp_test_type','test_type','asc');
    $data['Doctors']  = $this->testreport_model->getAllor('cp_doctor','doctor_name','asc');
    $this->template->set('title', 'Test Report');
    $this->template->load('new_user_dashboard_layout', 'contents', 'edit_testreport',$data);
  }else{
   redirect('login'); 
 }
}
public function view_testreport($testReportId='')
{
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
   $data['TestReportView']  = $this->testreport_model->getSingleTestReport($testReportId,$this->session->userdata('logged_in')['id']);
   $this->template->set('title', 'Test Report');
   $this->template->load('new_user_dashboard_layout', 'contents', 'view_testreport',$data);
 }else{
   redirect('login'); 
 }
}
/*Created by 95 for send test report to caregiver and doctor*/
public function send_testreport($report_id)
{
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    if($_POST['submit']){
      //$this->form_validation->set_rules('doctor','Doctor', 'required');
      //$this->form_validation->set_rules('caregiver','Caregiver', 'required');
      $this->form_validation->set_rules('remark','Remark', 'required');
      if ($this->form_validation->run() == TRUE) 
      {
       $caregiver_id = $this->input->post('caregiver');
       $caregiverDetail = $this->testreport_model->getsingle('cp_users',array('id'=>$caregiver_id));
       $caregiverEmail = $caregiverDetail->email;
       // echo "<pre>";
       // print_r($caregiverDetail->email);
       // die;
       // $doctor_id = $this->input->post('doctor');
       // $doctorDetail = $this->testreport_model->getsingle('cp_doctor',array('doctor_id'=>$doctor_id));
       // $doctorEmail = $doctorDetail->doctor_email;
       $data=array(
        'test_id'=>$this->input->post('testReport_id'),
        'doc_id'=>'',
        'caregiver_id'=>$this->input->post('caregiver'),
        'description'=>$this->input->post('remark'),
        'user_id'=>$this->session->userdata('logged_in')['id'],
        'send_test_report_created'=>date('Y-m-d H:i:s'),
      );
       $insertId = $this->testreport_model->insertData('send_test_report', $data);
       if($insertId){
        $result = $this->testreport_model->getsingle('cp_test_report',array('id'=>$report_id));
       //  if(!empty($doctorEmail)){
       //   $message = "This is test report for doctor.";
       //   $subject = "Test Report Doctor.";
       //   $attachment1[]= base_url().'uploads/test_report/'.$result->image;
       //   $status = new_send_mail($message,$subject,$doctorEmail,$attachment1); 
       // }
        if(!empty($caregiverEmail)){
          $message="This is test report for caregiver.";    
          $subject = "Test Report Caregiver.";
          $attachment[]= base_url().'uploads/test_report/'.$result->image;
          $status = new_send_mail($message,$subject,$caregiverEmail,$attachment);   
        }
        if($status){
          $this->session->set_flashdata('success','Test report send successfully');
          redirect('testreport_list');
        }
      }
// $insert = $this->testreport_model->updateData('cp_test_report',$data,array('id' => $report_id));
    }
  }
  $data['bestData'] = $this->testreport_model->getTestReportData($report_id);
  $data['Doctors']  = $this->testreport_model->getAllor('cp_doctor','doctor_name','asc');
  $data['caregivers']  = $this->testreport_model->getAllCaregiver();
  $this->template->set('title', 'Test Report');
  $this->template->load('new_user_dashboard_layout', 'contents', 'send_testreport',$data);
}else{
 redirect('login');            
}
}  
}
?>