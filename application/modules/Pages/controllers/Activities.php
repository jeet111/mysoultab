<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Activities extends MX_Controller {
	public function __construct() {
		parent:: __construct();
		$this->load->library('session');
		$this->load->library('form_validation');      
		$this->load->model('activities_model');
		$this->load->model('Common_model_new');
		$this->load->helper(array('common_helper'));
		$this->load->helper(array('url'));
		$this->load->helper(array('form'));      
	}
	/*Created by 95 for show all the activities*/
	public function show_activities()
	{
		$data['menuactive'] = $this->uri->segment(1);
		$checkLogin = $this->session->userdata('logged_in');
		if(!empty($checkLogin))
		{
			$ReminderData  = $this->activities_model->getAllwhereorderby("cp_reminder",array("user_id" => $this->session->userdata('logged_in')['id']),'reminder_id','desc');
			if(!empty($ReminderData)){
				$data['ReminderData']=$ReminderData;
			}else{
				$data['ReminderData']='';
			}
			$reminder_data = $this->activities_model->getAllreminders("cp_reminder",array("user_id" => $this->session->userdata('logged_in')['id']));
			$month = $this->session->userdata("hidden_activity");
			$year = $this->session->userdata("hidden_activity_year");
			$start_date = "01-".$month."-".$year;
			$start_time = strtotime($start_date);
			$end_time = strtotime("+15 years", $start_time);
			/* ------------------for year--------------------*/
			for($i =0; $i <= 20 ;$i++)
			{
				if(!empty($reminder_data)){
					foreach($reminder_data as $reminders){
						$explode_year = explode("-",$reminders['reminder_date']);
						$years = $explode_year[0] + $i;
						if($reminders['repeat'] == 'Yearly'){
							$explode_reminder_date = explode('-',$reminders['reminder_date']);		   
							if($explode_reminder_date[0] <= $years){ 
								$explodereminder = explode('-',$reminders['reminder_date']);
								$newdate = $years.'-'.$explodereminder[1].'-'.$explodereminder[2];
								$datesfgd[] = array('date'=>$newdate);
							}else{ 
							}
						}
					}
				}
			}
			/*--------------------for daily-----------------------------*/
			if(!empty($reminder_data)){
				foreach($reminder_data as $reminders){
					if($reminders['repeat'] == 'Daily'){
						$start_date_daily = $reminders['reminder_date'];
						$start_time_daily = strtotime($start_date_daily);
						$end_time_daily = strtotime("+15 years", $start_time_daily);
						for($d=$start_time_daily; $d<$end_time_daily; $d+=86400)
						{
							$list = date('Y-m-d', $d);
							if($reminders['reminder_date'] <= $list){
								$dates[] = array('date'=>$list,'id' => $reminders['reminder_id']);
							}else{
							}
						}
					}
				}	
			}
			/*-----------------------for weekly-----------------------*/
			if(!empty($reminder_data)){
				foreach($reminder_data as $reminders){  
					if($reminders['repeat'] == 'Weekly'){ 	
						$start_date_week = $reminders['reminder_date'];
						$start_time_week = strtotime($start_date_week);
						$end_time_week = strtotime("+15 years", $start_time_week);
						for($w=$start_time_week; $w<$end_time_week; $w+=604800)
						{ 
							$list_week = date('Y-m-d', $w);
							if($reminders['reminder_date'] <= $list_week){
								$dates_week[] = array('date'=>$list_week,'id' => $reminders['reminder_id']);
							}else{
							}
						}
					}
				}		
			}
		//echo '<pre>';print_r($dates_week);die;		
			/*--------------------for monthly-----------------------------*/
			for($k =0; $k <= 48 ;$k++)
			{ 
				if(!empty($reminder_data)){  
					foreach($reminder_data as $reminders){
						$explode_year = explode("-",$reminders['reminder_date']);
						$year = $explode_year[0];
						$years = $explode_year[0] + $k;
						$years_month = $explode_year[1] + $k;
						if($reminders['repeat'] == 'Monthly'){
							$explode_reminder_month_date = explode('-',$reminders['reminder_date']);		   
							if($explode_reminder_month_date[1] <= $years_month){ 
								$exploderemindermonth = explode('-',$reminders['reminder_date']);
								if($years_month == 1){
									$test = '01';
									$test_year = $year;
								}if($years_month == 2){
									$test = '02';
									$test_year = $year;
								}if($years_month == 3){
									$test = '03';
									$test_year = $year;
								}if($years_month == 4){
									$test = '04';
									$test_year = $year;
								}if($years_month == 5){
									$test = '05';
									$test_year = $year;
								}if($years_month == 6){
									$test = '06';
									$test_year = $year;
								}if($years_month == 7){
									$test = '07';
									$test_year = $year;
								}if($years_month == 8){
									$test = '08';
									$test_year = $year;
								}if($years_month == 9){
									$test = '09';
									$test_year = $year;
								}if($years_month == 10){
									$test = '10';
									$test_year = $year;
								}if($years_month == 11){
									$test = '11';
									$test_year = $year;
								}if($years_month == 12){
									$test = '12';
									$test_year = $year;
								}if($years_month == 13){ 
									$test = '01';	
									$test_year = $year+1;				 
								}if($years_month == 14){
									$test = '02';
									$test_year = $year+1;	
								}if($years_month == 15){
									$test = '03';
									$test_year = $year+1;	
								}if($years_month == 16){
									$test = '04';
									$test_year = $year+1;	
								}if($years_month == 17){
									$test = '05';$test_year = $year+1;	
								}if($years_month == 18){
									$test = '06';
									$test_year = $year+1;	
								}if($years_month == 19){
									$test = '07';$test_year = $year+1;	
								}if($years_month == 20){
									$test = '08';$test_year = $year+1;	
								}if($years_month == 21){
									$test = '09';$test_year = $year+1;	
								}if($years_month == 22){
									$test = '10';$test_year = $year+1;	
								}if($years_month == 23){
									$test = '11';$test_year = $year+1;	
								}if($years_month == 24){
									$test = '12';$test_year = $year+1;	
								}if($years_month == 25){
									$test = '01';$test_year = $year+2;	
								}if($years_month == 26){
									$test = '02';$test_year = $year+2;	
								}if($years_month == 27){
									$test = '03';$test_year = $year+2;	
								}if($years_month == 28){
									$test = '04';$test_year = $year+2;	
								}if($years_month == 29){
									$test = '05';$test_year = $year+2;	
								}if($years_month == 30){
									$test = '06';$test_year = $year+2;	
								}if($years_month == 31){
									$test = '07';$test_year = $year+2;	
								}if($years_month == 32){
									$test = '08';$test_year = $year+2;	
								}if($years_month == 33){
									$test = '09';$test_year = $year+2;	
								}if($years_month == 34){
									$test = '10';$test_year = $year+2;	
								}if($years_month == 35){
									$test = '11';$test_year = $year+2;	
								}if($years_month == 36){
									$test = '12';$test_year = $year+2;	
								}if($years_month == 37){
									$test = '01';$test_year = $year+3;	
								}if($years_month == 38){
									$test = '02';$test_year = $year+3;
								}if($years_month == 39){
									$test = '03';$test_year = $year+3;
								}if($years_month == 40){
									$test = '04';$test_year = $year+3;
								}if($years_month == 41){
									$test = '05';$test_year = $year+3;
								}if($years_month == 42){
									$test = '06';$test_year = $year+3;
								}if($years_month == 43){
									$test = '07';$test_year = $year+3;
								}if($years_month == 44){
									$test = '08';$test_year = $year+3;
								}if($years_month == 45){
									$test = '09';$test_year = $year+3;
								}if($years_month == 46){
									$test = '10';$test_year = $year+3;
								}if($years_month == 47){
									$test = '11';$test_year = $year+3;
								}if($years_month == 48){
									$test = '12';$test_year = $year+3;
								}if($years_month == 49){
									$test = '01';$test_year = $year+4;
								}if($years_month == 50){
									$test = '02';$test_year = $year+4;
								}
								if($years_month == 51){
									$test = '03';$test_year = $year+4;
								}
								if($years_month == 52){
									$test = '04';$test_year = $year+4;
								}
								if($years_month == 53){
									$test = '05';$test_year = $year+4;			
								}if($years_month == 54){
									$test = '06';$test_year = $year+4;
								}
								if($years_month == 55){
									$test = '07';$test_year = $year+4;
								}
								if($years_month == 56){
									$test = '08';$test_year = $year+4;
								}
								$newdatemonth = $test_year.'-'.$test.'-'.$exploderemindermonth[2];
								$datemonth[] = array('date'=>$newdatemonth);
							}else{
							}
						}
					}
				}
			}
 		//echo '<pre>';print_r($datemonth);die;
			$c='';
			if(!empty($datesfgd) && empty($dates) && empty($datemonth) && empty($dates_week)){
				$c = $datesfgd;
			}elseif(empty($datesfgd) && !empty($dates) && empty($datemonth) && empty($dates_week)){
				$c = $dates;
			}elseif(!empty($datesfgd) && !empty($dates) && empty($datemonth) && empty($dates_week)){
				$c = array_merge($datesfgd,$dates);
			}elseif(empty($datesfgd) && empty($dates) && !empty($datemonth) && empty($dates_week)){
				$c = $datemonth;
			}elseif(empty($datesfgd) && !empty($dates) && !empty($datemonth) && empty($dates_week)){
				$c = array_merge($dates,$datemonth);
			}elseif(!empty($datesfgd) && empty($dates) && !empty($datemonth) && empty($dates_week)){
				$c = array_merge($datesfgd,$datemonth);
			}elseif(!empty($datesfgd) && !empty($dates) && !empty($datemonth) && empty($dates_week)){
				$c = array_merge($datesfgd,$datemonth,$dates);
			}elseif(empty($datesfgd) && empty($dates) && empty($datemonth) && !empty($dates_week)){
				$c = $dates_week;
			}elseif(empty($datesfgd) && !empty($dates) && !empty($datemonth) && !empty($dates_week)){
				$c = array_merge($dates,$datemonth,$dates_week);
			}elseif(!empty($datesfgd) && empty($dates) && !empty($datemonth) && !empty($dates_week)){
				$c = array_merge($datesfgd,$datemonth,$dates_week);
			}elseif(!empty($datesfgd) && !empty($dates) && empty($datemonth) && !empty($dates_week)){
				$c = array_merge($datesfgd,$dates_week,$dates);
			}elseif(!empty($datesfgd) && !empty($dates) && !empty($datemonth) && !empty($dates_week)){
				$c = array_merge($datesfgd,$dates,$datemonth,$dates_week);
			}elseif(!empty($datesfgd) && empty($dates) && empty($datemonth) && !empty($dates_week)){
				$c = array_merge($datesfgd,$dates_week);
			}elseif(empty($datesfgd) && !empty($dates) && empty($datemonth) && !empty($dates_week)){
				$c = array_merge($dates,$dates_week);
			}elseif(empty($datesfgd) && empty($dates) && !empty($datemonth) && !empty($dates_week)){
				$c = array_merge($datemonth,$dates_week);
			}	
	//echo '<pre>';print_r($c);die;	
			$dates1 = $c;	  
			$appointment_data = $this->activities_model->getAllappointmets("doctor_appointments", "dr_appointment_date_id", "dr_available_date", "avdate_id",array("doctor_appointments.user_id" => $this->session->userdata('logged_in')['id']),"*","dr_available_date.avdate_date","dr_appointment_id","desc");
			$appointment_data_new = $this->Common_model_new->jointwotablenn("doctor_appointments", "dr_appointment_date_id", "dr_available_date", "avdate_id",array("dr_available_date.avdate_date" => date('Y-m-d'),"doctor_appointments.user_id" => $this->session->userdata('logged_in')['id']),"*","dr_appointment_id","desc");
			$reminder_data_new = $this->Common_model_new->getAllwhereorderby("cp_reminder",array("user_id" => $this->session->userdata('logged_in')['id'],"reminder_date"=> date('Y-m-d')),"reminder_id","desc");
			$medicine_data = $this->Common_model_new->getAllwhereorderby("medicine_schedule",array("user_id" => $this->session->userdata('logged_in')['id']),"medicine_id","desc");
	//echo '<pre>';print_r($medicine_data);die;
			foreach($medicine_data as $medi){
				$medicine_date = date('Y-m-d',strtotime($medi->medicine_create_date ."+$medi->medicine_take_days days"));
				$test = $medi->medicine_create_date;
				$explode = explode(' ',$test);
				$dates = $this->getDatesFromRange($explode[0], $medicine_date);
				$todayD = date('Y-m-d');
				$vals=array();
				$medi_id=array();
				$avail_dates=array();
				foreach($dates as $dd){
					if($dd == $todayD){  
						$vals[] = "1";
						$avail_dates[] = $dd;
						$medi_id[] = $medi->medicine_id;
					}else{
						$vals[] = "2";
					}
				}
			}
			$merged_array = array_map(null, $avail_dates, $medi_id);
			foreach($merged_array as $merging){
				$test_array[] = $this->Common_model_new->getsingle("medicine_schedule",array("medicine_id" => $merging[1]));
			}
			if (in_array("1", $vals)) {
				$medicine_data1 = $test_array;
			}else{
				$medicine_data1 = "";
			}
			$data['reminder_data'] = $reminder_data_new;
			$data['appointment_data'] = $appointment_data_new;
			$data['medicine_data'] = $medicine_data1;
			foreach ($appointment_data as $value) {
				$dates2[]= array('date'=>$value['avdate_date']);
			}
			$medicine_schedule = $this->Common_model_new->getAllwhere("medicine_schedule",array("user_id" => $this->session->userdata('logged_in')['id']));
			foreach ($medicine_schedule as $med_value) {
				$test = $med_value->medicine_create_date;
				$explode = explode(' ',$test);	
				$medicine_date = date('Y-m-d',strtotime($med_value->medicine_create_date ."+$med_value->medicine_take_days days"));
				$datesw = $this->getDatesFromRange($explode[0], $medicine_date);
				foreach($datesw as $dd){
					$dates3[]= array('date'=>$dd);
				}
			}
	//echo '<pre>'; print_r($dates3);die;
			if(!empty($dates2) && !empty($dates1) && !empty($dates3)){
				$dates = array_merge($dates2,$dates1,$dates3);
			}else if(!empty($dates2) && empty($dates1) && empty($dates3)){
				$dates = $dates2;
			}else if(!empty($dates1) && empty($dates2) && empty($dates3)){
				$dates = $dates1;
			}else if(empty($dates1) && empty($dates2) && !empty($dates3)){
				$dates = $dates1;
			}else if(empty($dates1) && !empty($dates2) && !empty($dates3)){
				$dates = array_merge($dates2,$dates3);
			}else if(!empty($dates1) && !empty($dates2) && empty($dates3)){
				$dates = array_merge($dates1,$dates2);
			}else if(!empty($dates1) && empty($dates2) && !empty($dates3)){
				$dates = array_merge($dates1,$dates3);
			}
			$data['alldates']=$dates;
			$data['btnsetingArray'] = $this->Common_model_new->getAllwhere("setting_butns",array("btnname" =>'all_activities'));
			$this->template->set('title', 'All Activities');
			$this->template->load('user_dashboard_layout_activities', 'contents', 'activities_list',$data);
		}else{
			redirect('login'); 
		}
	}
	public function ajax_activities()  
	{
		$available_date = $this->input->post("date");
		$reminder_data = $this->activities_model->getAllreminders("cp_reminder",array("user_id" => $this->session->userdata('logged_in')['id']));
	//echo '<pre>';print_r($reminder_data);die;
		$exp = explode("-",$available_date); 
		$month = $exp[1];
		$year = $exp[0];
		$start_date = "01-".$month."-".$year;
		$start_time = strtotime($start_date);
		$end_time = strtotime("+15 years", $start_time);
		/* ------------------for year--------------------*/
		for($i =0; $i <= 20 ;$i++)
		{
			$years = $year + $i;
			foreach($reminder_data as $reminders){
				if($reminders['repeat'] == 'Yearly'){
					$explode_reminder_date = explode('-',$reminders['reminder_date']);		   
					if($explode_reminder_date[0] <= $years){ 
						$explodereminder = explode('-',$reminders['reminder_date']);
						$newdate = $years.'-'.$explodereminder[1].'-'.$explodereminder[2];
						$datesfgd[] = array('date'=>$newdate,'id' => $reminders['reminder_id']);
					}else{ 
					}
				}
			}
		}
		/*--------------------for daily-----------------------------*/
		foreach($reminder_data as $reminders){
			if($reminders['repeat'] == 'Daily'){
				$start_date_daily = $reminders['reminder_date'];
				$start_time_daily = strtotime($start_date_daily);
				for($d=$start_time_daily; $d<$end_time; $d+=86400)
				{
					$list = date('Y-m-d', $d);
					if($reminders['reminder_date'] <= $list){
						$dates[] = array('date'=>$list,'id' => $reminders['reminder_id']);
					}else{
					}
				}
			}
		}	
		/*-----------------------for weekly-----------------------*/
		foreach($reminder_data as $reminders){  
			if($reminders['repeat'] == 'Weekly'){ 	
				$start_date_week = $reminders['reminder_date'];
				$start_time_week = strtotime($start_date_week);
				for($w=$start_time_week; $w<$end_time; $w+=604800)
				{
					$list_week = date('Y-m-d', $w);
					if($reminders['reminder_date'] <= $list_week){
						$dates_week[] = array('date'=>$list_week,'id' => $reminders['reminder_id']);
					}else{
					}
				}
			}
		}		
		//echo '<pre>';print_r($dates_week);die;		
		/*--------------------for monthly-----------------------------*/
		for($k =0; $k <= 48 ;$k++)
		{ 
			$years_month = $month + $k;
			$years = $year + $k;
			foreach($reminder_data as $reminders){
				if($reminders['repeat'] == 'Monthly'){ 
					$explode_reminder_month_date = explode('-',$reminders['reminder_date']);		   
					if($explode_reminder_month_date[1] <= $years_month){ 
						$exploderemindermonth = explode('-',$reminders['reminder_date']);
						if($years_month == 1){
							$test = '01';
							$test_year = $year;
						}if($years_month == 2){
							$test = '02';
							$test_year = $year;
						}if($years_month == 3){
							$test = '03';
							$test_year = $year;
						}if($years_month == 4){
							$test = '04';
							$test_year = $year;
						}if($years_month == 5){
							$test = '05';
							$test_year = $year;
						}if($years_month == 6){
							$test = '06';
							$test_year = $year;
						}if($years_month == 7){
							$test = '07';
							$test_year = $year;
						}if($years_month == 8){
							$test = '08';
							$test_year = $year;
						}if($years_month == 9){
							$test = '09';
							$test_year = $year;
						}if($years_month == 10){
							$test = '10';
							$test_year = $year;
						}if($years_month == 11){
							$test = '11';
							$test_year = $year;
						}if($years_month == 12){
							$test = '12';
							$test_year = $year;
						}if($years_month == 13){ 
							$test = '01';	
							$test_year = $year+1;				 
						}if($years_month == 14){
							$test = '02';
							$test_year = $year+1;	
						}if($years_month == 15){
							$test = '03';
							$test_year = $year+1;	
						}if($years_month == 16){
							$test = '04';
							$test_year = $year+1;	
						}if($years_month == 17){
							$test = '05';$test_year = $year+1;	
						}if($years_month == 18){
							$test = '06';
							$test_year = $year+1;	
						}if($years_month == 19){
							$test = '07';$test_year = $year+1;	
						}if($years_month == 20){
							$test = '08';$test_year = $year+1;	
						}if($years_month == 21){
							$test = '09';$test_year = $year+1;	
						}if($years_month == 22){
							$test = '10';$test_year = $year+1;	
						}if($years_month == 23){
							$test = '11';$test_year = $year+1;	
						}if($years_month == 24){
							$test = '12';$test_year = $year+1;	
						}if($years_month == 25){
							$test = '01';$test_year = $year+2;	
						}if($years_month == 26){
							$test = '02';$test_year = $year+2;	
						}if($years_month == 27){
							$test = '03';$test_year = $year+2;	
						}if($years_month == 28){
							$test = '04';$test_year = $year+2;	
						}if($years_month == 29){
							$test = '05';$test_year = $year+2;	
						}if($years_month == 30){
							$test = '06';$test_year = $year+2;	
						}if($years_month == 31){
							$test = '07';$test_year = $year+2;	
						}if($years_month == 32){
							$test = '08';$test_year = $year+2;	
						}if($years_month == 33){
							$test = '09';$test_year = $year+2;	
						}if($years_month == 34){
							$test = '10';$test_year = $year+2;	
						}if($years_month == 35){
							$test = '11';$test_year = $year+2;	
						}if($years_month == 36){
							$test = '12';$test_year = $year+2;	
						}if($years_month == 37){
							$test = '01';$test_year = $year+3;	
						}if($years_month == 38){
							$test = '02';$test_year = $year+3;
						}if($years_month == 39){
							$test = '03';$test_year = $year+3;
						}if($years_month == 40){
							$test = '04';$test_year = $year+3;
						}if($years_month == 41){
							$test = '05';$test_year = $year+3;
						}if($years_month == 42){
							$test = '06';$test_year = $year+3;
						}if($years_month == 43){
							$test = '07';$test_year = $year+3;
						}if($years_month == 44){
							$test = '08';$test_year = $year+3;
						}if($years_month == 45){
							$test = '09';$test_year = $year+3;
						}if($years_month == 46){
							$test = '10';$test_year = $year+3;
						}if($years_month == 47){
							$test = '11';$test_year = $year+3;
						}if($years_month == 48){
							$test = '12';$test_year = $year+3;
						}if($years_month == 49){
							$test = '01';$test_year = $year+4;
						}if($years_month == 50){
							$test = '02';$test_year = $year+4;
						}
						if($years_month == 51){
							$test = '03';$test_year = $year+4;
						}
						if($years_month == 52){
							$test = '04';$test_year = $year+4;
						}
						if($years_month == 53){
							$test = '05';$test_year = $year+4;			
						}if($years_month == 54){
							$test = '06';$test_year = $year+4;
						}
						if($years_month == 55){
							$test = '07';$test_year = $year+4;
						}
						if($years_month == 56){
							$test = '08';$test_year = $year+4;
						}
						$newdatemonth = $test_year.'-'.$test.'-'.$exploderemindermonth[2];
						$datemonth[] = array('date'=>$newdatemonth,'id' => $reminders['reminder_id']);
					}else{
					}
				}
			}
		}
 		//echo '<pre>';print_r($datemonth);die;
		if(!empty($datesfgd) && empty($dates) && empty($datemonth) && empty($dates_week)){
			$c = $datesfgd;
		}elseif(empty($datesfgd) && !empty($dates) && empty($datemonth) && empty($dates_week)){
			$c = $dates;
		}elseif(!empty($datesfgd) && !empty($dates) && empty($datemonth) && empty($dates_week)){
			$c = array_merge($datesfgd,$dates);
		}elseif(empty($datesfgd) && empty($dates) && !empty($datemonth) && empty($dates_week)){
			$c = $datemonth;
		}elseif(empty($datesfgd) && !empty($dates) && !empty($datemonth) && empty($dates_week)){
			$c = array_merge($dates,$datemonth);
		}elseif(!empty($datesfgd) && empty($dates) && !empty($datemonth) && empty($dates_week)){
			$c = array_merge($datesfgd,$datemonth);
		}elseif(!empty($datesfgd) && !empty($dates) && !empty($datemonth) && empty($dates_week)){
			$c = array_merge($datesfgd,$datemonth,$dates);
		}elseif(empty($datesfgd) && empty($dates) && empty($datemonth) && !empty($dates_week)){
			$c = $dates_week;
		}elseif(empty($datesfgd) && !empty($dates) && !empty($datemonth) && !empty($dates_week)){
			$c = array_merge($dates,$datemonth,$dates_week);
		}elseif(!empty($datesfgd) && empty($dates) && !empty($datemonth) && !empty($dates_week)){
			$c = array_merge($datesfgd,$datemonth,$dates_week);
		}elseif(!empty($datesfgd) && !empty($dates) && empty($datemonth) && !empty($dates_week)){
			$c = array_merge($datesfgd,$dates_week,$dates);
		}elseif(!empty($datesfgd) && !empty($dates) && !empty($datemonth) && !empty($dates_week)){
			$c = array_merge($datesfgd,$datemonth,$dates,$dates_week);
		}elseif(!empty($datesfgd) && empty($dates) && empty($datemonth) && !empty($dates_week)){
			$c = array_merge($datesfgd,$dates_week);
		}elseif(empty($datesfgd) && !empty($dates) && empty($datemonth) && !empty($dates_week)){
			$c = array_merge($dates,$dates_week);
		}elseif(empty($datesfgd) && empty($dates) && !empty($datemonth) && !empty($dates_week)){
			$c = array_merge($datemonth,$dates_week);
		}	
	//echo '<pre>';print_r($c);die;
		foreach($c as $merging_reminder){
			if($merging_reminder['date'] == $available_date){
				$test_array_reminder[] = $this->Common_model_new->getsingle("cp_reminder",array("user_id" => $this->session->userdata('logged_in')['id'],"reminder_id"=> $merging_reminder['id']));
			}
		}
		$appointment_data = $this->Common_model_new->jointwotablenn("doctor_appointments", "dr_appointment_date_id", "dr_available_date", "avdate_id",array("dr_available_date.avdate_date" => $available_date,"doctor_appointments.user_id" => $this->session->userdata('logged_in')['id']),"*","dr_appointment_id","desc");
		$medicine_data = $this->Common_model_new->getAllwhereorderby("medicine_schedule",array("user_id" => $this->session->userdata('logged_in')['id']),"medicine_id","desc");
		foreach($medicine_data as $medi){
			$medicine_date = date('Y-m-d',strtotime($medi->medicine_create_date ."+$medi->medicine_take_days days"));
			$test = $medi->medicine_create_date;
			$explode = explode(' ',$test);
			$dates = $this->getDatesFromRange($explode[0], $medicine_date);
			foreach($dates as $dd){
				if($dd == $available_date){  
					$vals[] = "1";
					$avail_dates[] = $dd;
					$medi_id[] = $medi->medicine_id;
				}else{
					$vals[] = "2";
				}
			}
		}
		$merged_array = array_map(null, $avail_dates, $medi_id);
		foreach($merged_array as $merging){
			$test_array[] = $this->Common_model_new->getsingle("medicine_schedule",array("medicine_id" => $merging[1]));
		}
		if (in_array("1", $vals)) {
			$medicine_data1 = $test_array;
		}else{
			$medicine_data1 = "";
		}
		$list = $this->load->view('ajax_activities', array('reminder_data' => $test_array_reminder,'appointment_data' => $appointment_data,'medicine_data' => $medicine_data1,'news_date' => $available_date), true);
		$this->output->set_content_type('application/json');
		$return = array('success' => true, 'list' => $list);
		echo json_encode($return);
	}
	public function view_reminder_activities()
	{
		$reminder_id = $this->input->post("reminder_id");
		$reminder_data = $this->Common_model_new->getsingle("cp_reminder",array("reminder_id" => $reminder_id));
		$data = ' <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title"> Activities Info </h4>
		</div>
		<div class="modal-body">
		<ul class="common_remind_ul">
		<li>
		<div class="remind_cls">Reminder Title :</div>
		<div class="remind_new_cls">'.$reminder_data->reminder_title.'</div></li>
		<li>
		<div class="remind_cls">Reminder Date :</div>
		<div class="remind_new_cls">'.$reminder_data->reminder_date.'</div>
		</li>
		<li>
		<div class="remind_cls">Reminder Time :</div>
		<div class="remind_new_cls">'.$reminder_data->reminder_time.'</div></li>
		</ul>
		</div>';
		echo $data;
	}
	public function view_apointment_activities()
	{
		$appointment_id = $this->input->post("appointment_id");
		$appointment_data = $this->Common_model_new->getAppointmentActivities($appointment_id);
   //echo '<pre>';print_r($appointment_data);die;
		if($appointment_data->doctor_pic){
			$image = base_url()."uploads/doctor/".$appointment_data->doctor_pic;
		}else{
			$image = base_url()."uploads/doctor/default.png";  
		}
		$data = '<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title"> Activities Information </h4>
		</div>
		<div class="row">				
		<div class="col-xs-4 col-sm-4 col-md-4">
		<div class="pro_img">
		<img src='.$image.' class="" alt="">
		</div>
		</div>
		<div class="col-xs-8 col-sm-8 col-md-8">
		<div class="bx_drDetail">						
		<div class="bx_nam">
		<h2 class="tm_member">'.$appointment_data->doctor_name.'</h2>
		<h5 class="tm_position">';
		$categories = explode(',',$appointment_data->doctor_category_id); 
		$catArray=array();						
		foreach ($categories as $category_id) {
			$catArray[]= $this->Common_model_new->getCategory1($category_id);
								//$category_name .= $doctors->doctor_name.',';
		}
								//print_r($catArray);
		$data .= implode(',',$catArray);
		$data .= '</h5>
		</div>
		</div>					
		</div>				
		</div>
		<ul class="tm-team-details-list">
		<li>
		<div class="tm-team-list-title"><i class="fa fa-money" aria-hidden="true"></i> Fees :</div>
		<div class="tm-team-list-value">';
		$data .= $appointment_data->doctor_fees;
		$data .= '</div>
		</li>
		<li>
		<div class="tm-team-list-title"><i class="fa fa-phone"></i> Contact No :</div>
		<div class="tm-team-list-value">';
		$data .= $appointment_data->doctor_mob_no;
		$data .= '</div>
		</li>
		<li>
		<div class="tm-team-list-title"><i class="fa fa-map-marker"></i> Address :</div>
		<div class="tm-team-list-value">';
		$data .= $appointment_data->doctor_address; 
		$data .= '</div>
		</li>
		<li>
		<div class="tm-team-list-title"><i class="fa fa-calendar"></i> Date :</div>
		<div class="tm-team-list-value">
		'.$appointment_data->avdate_date.'</div></li>
		<li><div class="tm-team-list-title"><i class="fa fa-clock-o"></i> Time Slot:</div>
		<div class="tm-team-list-value">'.$appointment_data->avtime_day_slot.
		'</div>
		</li>
		<li><div class="tm-team-list-title"><i class="fa fa-clock-o"></i> Time :</div>
		<div class="tm-team-list-value">'.$appointment_data->avtime_text.
		'</div>
		</li>
		</ul>';
		echo $data;
	}
	function getDatesFromRange($start, $end, $format = 'Y-m-d') {
		$array = array();
		$interval = new DateInterval('P1D');
		$realEnd = new DateTime($end);
		$realEnd->modify('-1 day');
		$realEnd->add($interval);
		$period = new DatePeriod(new DateTime($start), $interval, $realEnd);
		foreach($period as $date) { 
			$array[] = $date->format($format); 
		}
		return $array;
	}
	public function view_medicine_activities()
	{
		$medicine_id = $this->input->post("medicine_id");
		$medicine_data = $this->Common_model_new->getsingle("medicine_schedule",array("medicine_id" => $medicine_id));
		$data = ' <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title"> Activities Info </h4>
		</div>
		<div class="modal-body">
		<ul class="common_remind_ul">
		<li>
		<div class="remind_cls">Medicine Name :</div>
		<div class="remind_new_cls">'.$medicine_data->medicine_name.'</div></li>
		<li>
		<div class="remind_cls">Medicine Create Date :</div>
		<div class="remind_new_cls">'.$medicine_data->medicine_create_date.'</div>
		</li>
		<li>
		<div class="remind_cls">Medicine Time :</div>
		<div class="remind_new_cls">'.$medicine_data->medicine_time.'</div></li>
		</ul>
		</div>';
		echo $data;
	}
	public function showmonth()
	{
		$month = $this->session->userdata("hidden_activity");
		$year = $this->session->userdata("hidden_activity_year");
		$newmonth = $month+1;
		$this->session->unset_userdata('hidden_activity');
		$this->session->unset_userdata('hidden_activity_year');
		$this->session->set_userdata("hidden_activity",$newmonth);
		$newyear = $year;
		$this->session->set_userdata("hidden_activity_year",$newyear);
		$this->session->userdata("hidden_activity");
	}
	public function previousshowmonth()
	{
		$month = $this->input->post("month");
		echo $month-1;
	}
}