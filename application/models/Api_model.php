<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Api_model extends CI_Model

{

    function getRows($table,$params = array()){







        $this->db->select('*');

        $this->db->from($table);

        //$this->db->join('gc_users', 'gc_report_ads.user_id = gc_users.user_id','left');

        if(array_key_exists("conditions",$params)){

            foreach ($params['conditions'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("filter",$params)){

            foreach ($params['filter'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("like",$params)){

            foreach ($params['like'] as $key => $value) {

                $this->db->like($key,$value);

            }

        }

        // $order_mode = "ASC";

        if(array_key_exists("sorting",$params)){

            foreach($params['sorting'] as $key => $value) {

                $this->db->order_by($key, $value);

                // $this->session->set_userdata('order_by',$value);

            }

        }

        if(array_key_exists("id",$params)){

            $this->db->where('id',$params['id']);

            $query = $this->db->get();



            $result = $query->row_array();

        }else{

            //set start and limit



            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit'],$params['start']);



            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit']);

            }

            $query = $this->db->get();



            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){

                $result = $query->num_rows();

            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){

                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;

            }else{

                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

            }

        }





        return $result;

    }

    function getRowsRestaurant($table,$params = array()){



        $this->db->select('*');

        $this->db->from($table);

        $this->db->join('cp_restaurant', 'cp_fav_restaurant.restaurant_id = cp_restaurant.rest_id');

        if(array_key_exists("conditions",$params)){

            foreach ($params['conditions'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("filter",$params)){

            foreach ($params['filter'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("like",$params)){

            foreach ($params['like'] as $key => $value) {

                $this->db->like($key,$value);

            }

        }

        // $order_mode = "ASC";

        if(array_key_exists("sorting",$params)){

            foreach($params['sorting'] as $key => $value) {

                $this->db->order_by($key, $value);

                // $this->session->set_userdata('order_by',$value);

            }

        }

        if(array_key_exists("id",$params)){

            $this->db->where('id',$params['id']);

            $query = $this->db->get();



            $result = $query->row_array();

        }else{

            //set start and limit



            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit'],$params['start']);



            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit']);

            }

            $query = $this->db->get();



            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){

                $result = $query->num_rows();

            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){

                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;

            }else{

                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

            }

        }





        return $result;

    }



    function getRowsRestaurantnew($table,$params = array()){



        $this->db->select('*');

        $this->db->from($table);



        if(array_key_exists("conditions",$params)){

            foreach ($params['conditions'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("filter",$params)){

            foreach ($params['filter'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("like",$params)){

            foreach ($params['like'] as $key => $value) {

                $this->db->like($key,$value);

            }

        }

        // $order_mode = "ASC";

        if(array_key_exists("sorting",$params)){

            foreach($params['sorting'] as $key => $value) {

                $this->db->order_by($key, $value);

                // $this->session->set_userdata('order_by',$value);

            }

        }

        if(array_key_exists("id",$params)){

            $this->db->where('id',$params['id']);

            $query = $this->db->get();



            $result = $query->row_array();

        }else{

            //set start and limit



            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit'],$params['start']);



            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit']);

            }

            $query = $this->db->get();



            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){

                $result = $query->num_rows();

            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){

                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;

            }else{

                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

            }

        }





        return $result;

    }



    function getRowsBank($table,$params = array()){



        $this->db->select('*');

        $this->db->from($table);

        $this->db->join('cp_bank', 'cp_fav_banks.bank_id = cp_bank.bank_id');

        if(array_key_exists("conditions",$params)){

            foreach ($params['conditions'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("filter",$params)){

            foreach ($params['filter'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("like",$params)){

            foreach ($params['like'] as $key => $value) {

                $this->db->like($key,$value);

            }

        }

        // $order_mode = "ASC";

        if(array_key_exists("sorting",$params)){

            foreach($params['sorting'] as $key => $value) {

                $this->db->order_by($key, $value);

                // $this->session->set_userdata('order_by',$value);

            }

        }

        if(array_key_exists("id",$params)){

            $this->db->where('id',$params['id']);

            $query = $this->db->get();



            $result = $query->row_array();

        }else{

            //set start and limit



            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit'],$params['start']);



            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit']);

            }

            $query = $this->db->get();



            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){

                $result = $query->num_rows();

            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){

                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;

            }else{

                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

            }

        }





        return $result;

    }



    function getRowsBanknew($table,$params = array()){



        $this->db->select('*');

        $this->db->from($table);

        //$this->db->join('cp_bank', 'cp_fav_banks.bank_id = cp_bank.bank_id');

        if(array_key_exists("conditions",$params)){

            foreach ($params['conditions'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("filter",$params)){

            foreach ($params['filter'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("like",$params)){

            foreach ($params['like'] as $key => $value) {

                $this->db->like($key,$value);

            }

        }

        // $order_mode = "ASC";

        if(array_key_exists("sorting",$params)){

            foreach($params['sorting'] as $key => $value) {

                $this->db->order_by($key, $value);

                // $this->session->set_userdata('order_by',$value);

            }

        }

        if(array_key_exists("id",$params)){

            $this->db->where('id',$params['id']);

            $query = $this->db->get();



            $result = $query->row_array();

        }else{

            //set start and limit



            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit'],$params['start']);



            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit']);

            }

            $query = $this->db->get();



            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){

                $result = $query->num_rows();

            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){

                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;

            }else{

                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

            }

        }





        return $result;

    }





    function getRowsGrocery($table,$params = array()){



        $this->db->select('*');

        $this->db->from($table);

        $this->db->join('cp_grocery', 'cp_fav_grocery.grocery_id = cp_grocery.grocery_id');

        if(array_key_exists("conditions",$params)){

            foreach ($params['conditions'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("filter",$params)){

            foreach ($params['filter'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("like",$params)){

            foreach ($params['like'] as $key => $value) {

                $this->db->like($key,$value);

            }

        }

        // $order_mode = "ASC";

        if(array_key_exists("sorting",$params)){

            foreach($params['sorting'] as $key => $value) {

                $this->db->order_by($key, $value);

                // $this->session->set_userdata('order_by',$value);

            }

        }

        if(array_key_exists("id",$params)){

            $this->db->where('id',$params['id']);

            $query = $this->db->get();



            $result = $query->row_array();

        }else{

            //set start and limit



            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit'],$params['start']);



            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit']);

            }

            $query = $this->db->get();



            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){

                $result = $query->num_rows();

            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){

                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;

            }else{

                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

            }

        }





        return $result;

    }



    function getRowsGrocerynew($table,$params = array()){



        $this->db->select('*');

        $this->db->from($table);

       //$this->db->join('cp_grocery', 'cp_fav_grocery.grocery_id = cp_grocery.grocery_id');

        if(array_key_exists("conditions",$params)){

            foreach ($params['conditions'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("filter",$params)){

            foreach ($params['filter'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("like",$params)){

            foreach ($params['like'] as $key => $value) {

                $this->db->like($key,$value);

            }

        }

        // $order_mode = "ASC";

        if(array_key_exists("sorting",$params)){

            foreach($params['sorting'] as $key => $value) {

                $this->db->order_by($key, $value);

                // $this->session->set_userdata('order_by',$value);

            }

        }

        if(array_key_exists("id",$params)){

            $this->db->where('id',$params['id']);

            $query = $this->db->get();



            $result = $query->row_array();

        }else{

            //set start and limit



            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit'],$params['start']);



            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit']);

            }

            $query = $this->db->get();



            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){

                $result = $query->num_rows();

            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){

                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;

            }else{

                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

            }

        }





        return $result;

    }



    function getRowsFavouriteMusic($table,$params = array()){



        $this->db->select('*');

        $this->db->from($table);

        $this->db->join('music', 'cp_music_favorite.music_id = music.music_id');

        if(array_key_exists("conditions",$params)){

            foreach ($params['conditions'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("filter",$params)){

            foreach ($params['filter'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("like",$params)){

            foreach ($params['like'] as $key => $value) {

                $this->db->like($key,$value);

            }

        }

        // $order_mode = "ASC";

        if(array_key_exists("sorting",$params)){

            foreach($params['sorting'] as $key => $value) {

                $this->db->order_by($key, $value);

                // $this->session->set_userdata('order_by',$value);

            }

        }

        if(array_key_exists("id",$params)){

            $this->db->where('id',$params['id']);

            $query = $this->db->get();



            $result = $query->row_array();

        }else{

            //set start and limit



            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit'],$params['start']);



            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit']);

            }

            $query = $this->db->get();



            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){

                $result = $query->num_rows();

            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){

                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;

            }else{

                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

            }

        }





        return $result;

    }

    function getRowsFavouriteMovie($table,$params = array()){



        $this->db->select('*');

        $this->db->from($table);

        $this->db->join('movie', 'cp_movie_favorite.movie_id = movie.movie_id');

        if(array_key_exists("conditions",$params)){

            foreach ($params['conditions'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("filter",$params)){

            foreach ($params['filter'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("like",$params)){

            foreach ($params['like'] as $key => $value) {

                $this->db->like($key,$value);

            }

        }

        // $order_mode = "ASC";

        if(array_key_exists("sorting",$params)){

            foreach($params['sorting'] as $key => $value) {

                $this->db->order_by($key, $value);

                // $this->session->set_userdata('order_by',$value);

            }

        }

        if(array_key_exists("id",$params)){

            $this->db->where('id',$params['id']);

            $query = $this->db->get();



            $result = $query->row_array();

        }else{

            //set start and limit



            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit'],$params['start']);



            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit']);

            }

            $query = $this->db->get();



            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){

                $result = $query->num_rows();

            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){

                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;

            }else{

                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

            }

        }





        return $result;

    }





    function getRowsFavouriteMeditationVideos($table,$params = array()){



        $this->db->select('*');

        $this->db->from($table);

        $this->db->join('meditation_videos', 'cp_meditaion_videos_favorite.movie_id = meditation_videos.movie_id');

        if(array_key_exists("conditions",$params)){

            foreach ($params['conditions'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("filter",$params)){

            foreach ($params['filter'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("like",$params)){

            foreach ($params['like'] as $key => $value) {

                $this->db->like($key,$value);

            }

        }

        // $order_mode = "ASC";

        if(array_key_exists("sorting",$params)){

            foreach($params['sorting'] as $key => $value) {

                $this->db->order_by($key, $value);

                // $this->session->set_userdata('order_by',$value);

            }

        }

        if(array_key_exists("id",$params)){

            $this->db->where('id',$params['id']);

            $query = $this->db->get();



            $result = $query->row_array();

        }else{

            //set start and limit



            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit'],$params['start']);



            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit']);

            }

            $query = $this->db->get();



            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){

                $result = $query->num_rows();

            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){

                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;

            }else{

                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

            }

        }





        return $result;

    }



    function getRowsFavouriteArticle($table,$params = array()){



        $this->db->select('*');

        $this->db->from($table);

        $this->db->join('cp_articles', 'cp_article_favourite.article_id = cp_articles.article_id');

        if(array_key_exists("conditions",$params)){

            foreach ($params['conditions'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("filter",$params)){

            foreach ($params['filter'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("like",$params)){

            foreach ($params['like'] as $key => $value) {

                $this->db->like($key,$value);

            }

        }

        // $order_mode = "ASC";

        if(array_key_exists("sorting",$params)){

            foreach($params['sorting'] as $key => $value) {

                $this->db->order_by($key, $value);

                // $this->session->set_userdata('order_by',$value);

            }

        }

        if(array_key_exists("id",$params)){

            $this->db->where('id',$params['id']);

            $query = $this->db->get();



            $result = $query->row_array();

        }else{

            //set start and limit



            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit'],$params['start']);



            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit']);

            }

            $query = $this->db->get();



            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){

                $result = $query->num_rows();

            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){

                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;

            }else{

                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

            }

        }





        return $result;

    }







    function getRowsFavouriteMeditationArticle($table,$params = array()){



        $this->db->select('*');

        $this->db->from($table);

        $this->db->join('cp_meditation_articles', 'cp_meditation_article_favourite.article_id = cp_meditation_articles.article_id');

        if(array_key_exists("conditions",$params)){

            foreach ($params['conditions'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("filter",$params)){

            foreach ($params['filter'] as $key => $value) {

                $this->db->where($key,$value);

            }

        }

        if(array_key_exists("like",$params)){

            foreach ($params['like'] as $key => $value) {

                $this->db->like($key,$value);

            }

        }

        // $order_mode = "ASC";

        if(array_key_exists("sorting",$params)){

            foreach($params['sorting'] as $key => $value) {

                $this->db->order_by($key, $value);

                // $this->session->set_userdata('order_by',$value);

            }

        }

        if(array_key_exists("id",$params)){

            $this->db->where('id',$params['id']);

            $query = $this->db->get();



            $result = $query->row_array();

        }else{

            //set start and limit



            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit'],$params['start']);



            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){

                $this->db->limit($params['limit']);

            }

            $query = $this->db->get();



            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){

                $result = $query->num_rows();

            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){

                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;

            }else{

                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

            }

        }





        return $result;

    }

    function getDoctorSpeciality($category_id){



        $where = "FIND_IN_SET('" . $category_id . "', dc_id)";
    
        $this->db->select('*');
    
        $this->db->from('cp_doctor_categories');
    
        $this->db->where($where);
    
        //$this->db->where("doctor_name LIKE '%$searchKeyword'");
    
        //$this->db->like('doctor_name',"$search_keyword"); 
    
        //$this->db->limit($limit,$start);
    
        $query = $this->db->get();
    
    
    
        // echo $this->db->last_query();
    
        // die;
    
    
    
    
    
        $result = $query->result_array();
    
        return $result;
    }

    function getAppointments($drAppId){



     $this->db->select('*');

     $this->db->from('doctor_appointments');

     $this->db->join('dr_available_date', 'doctor_appointments.dr_appointment_date_id = dr_available_date.avdate_id','left');

     $this->db->join('dr_available_time', 'doctor_appointments.dr_appointment_time_id = dr_available_time.avtime_id','left');

     $this->db->where('dr_appointment_id',$drAppId);

     $query = $this->db->get();


       // echo $this->db->last_query();
       // die;

     $result = $query->result_array();

     return $result;



 }

 // function getUserAppointments($user_id,$date){



 //     $this->db->select('*');

 //     $this->db->from('doctor_appointments');

 //     $this->db->join('dr_available_date', 'doctor_appointments.dr_appointment_date_id = dr_available_date.avdate_id','left');

 //     $this->db->join('dr_available_time', 'doctor_appointments.dr_appointment_time_id = dr_available_time.avtime_id','left');

 //     $this->db->where('user_id',$user_id);

 //     if($date){

 //         $this->db->where('avdate_date',$date);

 //     }

 //     $query = $this->db->get();

 //     $result = $query->result_array();

 //     return $result;



 // }



 function getUserAppointments($user_id,$date){



     $this->db->select('*');

     $this->db->from('doctor_appointments');

     $this->db->join('dr_available_date', 'doctor_appointments.dr_appointment_date_id = dr_available_date.avdate_id','left');

     $this->db->join('dr_available_time', 'doctor_appointments.dr_appointment_time_id = dr_available_time.avtime_id','left');

     $this->db->where('user_id',$user_id);

     //$this->db->where('avdate_date>=',date('Y-m-d'));

     if($date){

         $this->db->where('avdate_date',$date);

     }

     $query = $this->db->get();

     $result = $query->result_array();

     return $result;



 }





 function getDoctorListCount($category_id=""){



    // $where = "FIND_IN_SET('".$category_id."', doctor_category_id)";

    $this->db->select('*');

    $this->db->from('cp_doctor');

    //$this->db->like('doctor_name', 'GREGORY');

       //$this->db->where_in('doctor_category_id',$category_id);

    // $this->db->where($where);

    

    $query = $this->db->get();

    $result = $query->result_array();

    return $result;

}





function getDoctorSearchCount($search_keyword){



    // $where = "FIND_IN_SET('".$category_id."', doctor_category_id)";

    $this->db->select('*');

    $this->db->from('cp_doctor');

    //$this->db->like('doctor_name', 'GREGORY');

       //$this->db->where_in('doctor_category_id',$category_id);

    // $this->db->where($where);

    $this->db->like('doctor_name',"$search_keyword");

    $query = $this->db->get();

    $result = $query->result_array();

    return $result;

}





function getDoctorList($category_id=""){



    // echo $category_id;

    // echo "<br>";

    // echo $search_keyword;

    // die; 



    // $where = "FIND_IN_SET('".$category_id."', doctor_category_id)";

    $this->db->select('*');

    $this->db->from('cp_doctor');

    // $this->db->where($where);

   //$this->db->where("doctor_name LIKE '%$searchKeyword'");

    //$this->db->like('doctor_name',"$search_keyword"); 

       //$this->db->limit($limit,$start);

    $query = $this->db->get();



    // echo $this->db->last_query();

    // die;





    $result = $query->result_array();

    return $result;

}





function getDoctorSearch($search_keyword){



    // $where = "FIND_IN_SET('".$category_id."', doctor_category_id)";

    $this->db->select('*');

    $this->db->from('cp_doctor');

    // $this->db->where($where);

   //$this->db->where("doctor_name LIKE '%$searchKeyword'");

    $this->db->like('doctor_name',"$search_keyword"); 

       //$this->db->limit($limit,$start);

    $query = $this->db->get();



    // echo $this->db->last_query();

    // die;





    $result = $query->result_array();

    return $result;

}

   // function getUserReminders($user_id,$date){



   //     $this->db->select('*');

   //     $this->db->from('cp_reminder');

   //     //$this->db->join('dr_available_date', 'doctor_appointments.dr_appointment_date_id = dr_available_date.avdate_id','left');

   //     //$this->db->join('dr_available_time', 'doctor_appointments.dr_appointment_time_id = dr_available_time.avtime_id','left');

   //     $this->db->where('user_id',$user_id);

   //     if($date){

   //         $this->db->where('reminder_date',$date);

   //     }

   //     $query = $this->db->get();

   //     $result = $query->result_array();

   //     return $result;



   // }





function getUserReminders($user_id,$date){



 $this->db->select('*');

 $this->db->from('cp_reminder');

       //$this->db->join('dr_available_date', 'doctor_appointments.dr_appointment_date_id = dr_available_date.avdate_id','left');

       //$this->db->join('dr_available_time', 'doctor_appointments.dr_appointment_time_id = dr_available_time.avtime_id','left');

 $this->db->where('user_id',$user_id);

     //$this->db->where('reminder_date >=',date('Y-m-d'));



 if($date){

     $this->db->where('reminder_date',$date);

 }

 $query = $this->db->get();



     // echo $this->db->last_query();

     // die;



 $result = $query->result_array();

 return $result;



}





function viewAppointments($appointments_id){



 $this->db->select('*');

 $this->db->from('doctor_appointments');

 $this->db->join('cp_doctor', 'doctor_appointments.doctor_id = cp_doctor.doctor_id','left');

 $this->db->join('dr_available_date', 'doctor_appointments.dr_appointment_date_id = dr_available_date.avdate_id','left');

 $this->db->join('dr_available_time', 'doctor_appointments.dr_appointment_time_id = dr_available_time.avtime_id','left');

 $this->db->where('dr_appointment_id',$appointments_id);

 $query = $this->db->get();

 $result = $query->result_array();

 return $result;



}

   // function showAppointments($user_id){



   //     $this->db->select('*');

   //     $this->db->from('doctor_appointments');

   //     $this->db->join('cp_doctor', 'doctor_appointments.doctor_id = cp_doctor.doctor_id','left');

   //     $this->db->join('dr_available_date', 'doctor_appointments.dr_appointment_date_id = dr_available_date.avdate_id','left');

   //     $this->db->join('dr_available_time', 'doctor_appointments.dr_appointment_time_id = dr_available_time.avtime_id','left');

   //     $this->db->where('user_id',$user_id);

   //     $query = $this->db->get();

   //     $result = $query->result_array();

   //     return $result;



   // }









function showAppointments($user_id){



 $this->db->select('*');

 $this->db->from('doctor_appointments');

 $this->db->join('cp_doctor', 'doctor_appointments.doctor_id = cp_doctor.doctor_id','left');

 $this->db->join('dr_available_date', 'doctor_appointments.dr_appointment_date_id = dr_available_date.avdate_id','left');

 $this->db->join('dr_available_time', 'doctor_appointments.dr_appointment_time_id = dr_available_time.avtime_id','left');

 $this->db->where('user_id',$user_id);

 $query = $this->db->get();

 $result = $query->result_array();

 return $result;



}



public function getAllor($table,$order_id,$order_by,$where="")

{

  $this->db->select('*');

  $this->db->order_by($order_id,$order_by);

  if (!empty($where)) {
      $this->db->where('date',date('Y-m-d'));
  }
  

  $q = $this->db->get($table);

  $num_rows = $q->num_rows();

  if($num_rows >0)

  {

     foreach($q->result() as $row)

     {

        $data[] = $row;

    }

    $q->free_result();

    return $data;

}

}





public function getAllUsers($table,$order_id,$order_by,$where_cond)

{

  $this->db->select('*');

  $this->db->order_by($order_id,$order_by);

  $this->db->where($where_cond);

  $q = $this->db->get($table);

  $num_rows = $q->num_rows();

  if($num_rows >0)

  {

     foreach($q->result() as $row)

     {

        $data[] = $row;

    }

    $q->free_result();

    return $data;

}

}







function testlist($user_id){



 $this->db->select('cp_test_report.*,cp_doctor.*,cp_test_type.id as type_id,cp_test_type.test_type as test_type_name');

 $this->db->from('cp_test_report');

 $this->db->join('cp_doctor', 'cp_test_report.doctor_id = cp_doctor.doctor_id','left');

 $this->db->join('cp_test_type', 'cp_test_report.test_type_id = cp_test_type.id','left');

       //$this->db->join('dr_available_date', 'doctor_appointments.dr_appointment_date_id = dr_available_date.avdate_id','left');

       //$this->db->join('dr_available_time', 'doctor_appointments.dr_appointment_time_id = dr_available_time.avtime_id','left');

 $this->db->where('cp_test_report.user_id',$user_id);

 $this->db->order_by("cp_test_type.id","desc");

 $query = $this->db->get();

 $result = $query->result_array();

 return $result;

}

function acknowledgeList($user_id){



 $sql= "SELECT acknowledge.acknowledge_id as acknowledge_id,acknowledge.acknowledge_desc as descp ,acknowledge.acknowledge_title as title,CONCAT(`u1`.`medicine_name`) as medicine,

 CONCAT(`u2`.`medicine_name`) as medicine2,

 CONCAT(`u3`.`medicine_name`) as medicine3,

 pharma_company.name as pharma_name ,

 acknowledge.user_id as user_id ,

 acknowledge.name as users_name,

 acknowledge.lastname as lastname,

 acknowledge.dob as dob,

 acknowledge.selected_user as selected_user,

 acknowledge.send_date as send_date,

 acknowledge.send_time as send_time,

 acknowledge.acknowledge_created as acknowledge_created,

 acknowledge.rx_number as rx_number,

 acknowledge.supply_days as supply_days,

 acknowledge.supply2_days as supply2_days,  

 acknowledge.supply3_days as supply3_days    

 FROM `acknowledge`

 LEFT JOIN `medicine_schedule` as `u1` ON (`u1`.`medicine_id`=`acknowledge`.`medicine_id`)

 LEFT JOIN `medicine_schedule` as `u2` ON (`u2`.`medicine_id`=`acknowledge`.`medicine2_id`)

 LEFT JOIN `medicine_schedule` as `u3` ON (`u3`.`medicine_id`=`acknowledge`.`medicine3_id`)

 LEFT JOIN `pharma_company` ON (`pharma_company`.`id`=`acknowledge`.`pharma_company_id`) where acknowledge.user_id=$user_id";



 $result= $this->db->query($sql)->result_array();

 // echo $this->db->last_query();

 // die;   

 return $result;







    // $this->db->select('*');

    // $this->db->from('acknowledge');

    //    //$this->db->where('acknowledge.user_id',$user_id);

    // $this->db->join('medicine_schedule', 'acknowledge.medicine_id = medicine_schedule.medicine_id','left');

    // // $this->db->join('medicine_schedule', 'acknowledge.medicine2_id = medicine_schedule.medicine_id','left');

    // // $this->db->join('medicine_schedule', 'acknowledge.medicine3_id = medicine_schedule.medicine_id','left');

    // $this->db->join('pharma_company', 'acknowledge.pharma_company_id =  pharma_company.id','left');

    // $this->db->where('acknowledge.user_id',$user_id);

    // $this->db->order_by("acknowledge.acknowledge_id","desc");

    // $query = $this->db->get();

    // $result = $query->result_array();

    // return $result;



}



function pharmaList(){

 $this->db->select('*');

 $this->db->from('pharma_company');

       //$this->db->join('medicine_schedule', 'acknowledge.medicine_id = medicine_schedule.medicine_id','left');

       //$this->db->join('pharma_company', 'acknowledge.pharma_company_id =  pharma_company.id','left');

 $query = $this->db->get();

 $result = $query->result_array();

 return $result;



}



function newpharmaList(){

 $this->db->select('*');

 $this->db->from('pharma_company');

       //$this->db->join('medicine_schedule', 'acknowledge.medicine_id = medicine_schedule.medicine_id','left');

       //$this->db->join('pharma_company', 'acknowledge.pharma_company_id =  pharma_company.id','left');

 $this->db->order_by("name","asc");

 $query = $this->db->get();

 $result = $query->result_array();

 return $result;



}

function noteList(){

 $this->db->select('*');

 $this->db->from('cp_note');

       //$this->db->join('medicine_schedule', 'acknowledge.medicine_id = medicine_schedule.medicine_id','left');

       //$this->db->join('pharma_company', 'acknowledge.pharma_company_id =  pharma_company.id','left');

 $query = $this->db->get();

 $result = $query->result_array();

 return $result;



}

function articleList(){

 $this->db->select('*');

 $this->db->from('cp_articles');

       //$this->db->join('medicine_schedule', 'acknowledge.medicine_id = medicine_schedule.medicine_id','left');

       //$this->db->join('pharma_company', 'acknowledge.pharma_company_id =  pharma_company.id','left');

 $query = $this->db->get();

 $result = $query->result_array();

 return $result;



}

function favarticleList(){

 $this->db->select('*');

 $this->db->from('cp_article_favourite');

 $this->db->join('cp_articles', 'cp_article_favourite.article_id = cp_articles.article_id');

       //$this->db->join('pharma_company', 'acknowledge.pharma_company_id =  pharma_company.id','left');

 $query = $this->db->get();

 $result = $query->result_array();

 return $result;

}

function musicList(){

 $this->db->select('*');

 $this->db->from('music');

       //$this->db->join('medicine_schedule', 'acknowledge.medicine_id = medicine_schedule.medicine_id','left');

       //$this->db->join('pharma_company', 'acknowledge.pharma_company_id =  pharma_company.id','left');

 $query = $this->db->get();

 $result = $query->result_array();

 return $result;



}

function favMusicList($user_id){

 $this->db->select('*');

 $this->db->from('cp_music_favorite');

 $this->db->where('cp_music_favorite.user_id',$user_id);

 $this->db->join('music', 'cp_music_favorite.music_id = music.music_id');

       //$this->db->join('pharma_company', 'acknowledge.pharma_company_id =  pharma_company.id','left');

 $query = $this->db->get();

 $result = $query->result_array();

 return $result;



}

function movieList(){

 $this->db->select('*');

 $this->db->from('movie');

       //$this->db->join('medicine_schedule', 'acknowledge.medicine_id = medicine_schedule.medicine_id','left');

       //$this->db->join('pharma_company', 'acknowledge.pharma_company_id =  pharma_company.id','left');

 $query = $this->db->get();

 $result = $query->result_array();

 return $result;



}

function favMovieList($user_id){

 $this->db->select('*');

 $this->db->from('cp_movie_favorite');

 $this->db->where('cp_movie_favorite.user_id',$user_id);

 $this->db->join('movie', 'cp_movie_favorite.movie_id = movie.movie_id');

       //$this->db->join('pharma_company', 'acknowledge.pharma_company_id =  pharma_company.id','left');

 $query = $this->db->get();

 $result = $query->result_array();

 return $result;



}

function caregiverList(){

 $this->db->select('*');

 $this->db->from('cp_users');

 $this->db->where('cp_users.user_role',2);

 $this->db->where('cp_users.status',1);

 $query = $this->db->get();

 $result = $query->result_array();

 return $result;



}

function pickupList(){

 $this->db->select('*');

 $this->db->from('picup_medicine');

 $this->db->join('medicine_schedule', 'picup_medicine.medicine_id = medicine_schedule.medicine_id','left');

 $this->db->join('cp_doctor', 'picup_medicine.doctor_id =   cp_doctor.doctor_id','left');

 $query = $this->db->get();

 $result = $query->result_array();

 return $result;



}

function contactList($user_id){

 $this->db->select('*');

 $this->db->from('cp_user_contact');

 $this->db->where('contact_user_id',$user_id);

       //$this->db->join('medicine_schedule', 'picup_medicine.medicine_id = medicine_schedule.medicine_id','left');

      // $this->db->join('cp_doctor', 'picup_medicine.doctor_id =   cp_doctor.doctor_id','left');

 $query = $this->db->get();

 $result = $query->result_array();

 return $result;

}

function view_all_music($type){

    if($type == 1){

        $order_id = "music_view";

    }else if($type == 2){

        $order_id = "music_id";

    }

    $this->db->select('*');

    $this->db->from('music');

    $this->db->order_by($order_id,'desc');

    $query = $this->db->get();

    $result = $query->result_array();

    return $result;

}

function view_all_movie($type){

    if($type == 1){

        $order_id = "movie_view";

    }else if($type == 2){

        $order_id = "movie_id";

    }

    $this->db->select('*');

    $this->db->from('movie');

    $this->db->order_by($order_id,'desc');

    $query = $this->db->get();

    $result = $query->result_array();

    return $result;



}





/*Get vital data by userid*/

public function GetvitalDataByUserId($user_id)

{

   $this->db->select('*');

   $this->db->from('cp_vital_sign');

   $this->db->order_by('vital_sign_id','desc');

   $this->db->where('user_id',$user_id);

   $query = $this->db->get();

   $result = $query->result_array();

   return $result;

}



/*Get vital test type list*/

public function GetvitaltesttypeList()

{

   $this->db->select('*');

   /*$this->db->from('cp_vital_test_type');*/

   $this->db->from('cp_test_type');

   $this->db->order_by('id','desc');
   $this->db->group_by('test_type');

   //$this->db->where('user_id',$user_id);

   $query = $this->db->get();

   $result = $query->result_array();

   return $result;

}



/*Update vital data by userid*/

public function UpdateDataByUserId($user_id,$vital_id,$data)

{

    $this->db->where('user_id', $user_id);

    $this->db->where('vital_sign_id', $vital_id);

    $res = $this->db->update('cp_vital_sign',$data);

    // echo $this->db->last_query();

    // die;

    return $res;

}



public function UpdatePopAlertByUserId($id,$data)

{

    $this->db->where('id', $id);

    $res = $this->db->update('survey_popalert',$data);

    // echo $this->db->last_query();

    // die;

    return $res;

}







/*Get vital data by filter date and test type*/

public function GetvitalDataByFilter($user_id,$from_date,$to_date,$test_type_filter)

{

    if(!empty($from_date) && !empty($to_date)){

        $from_date = date('Y-m-d',strtotime($from_date));

        $to_date = date('Y-m-d',strtotime($to_date));
    }else{
        $from_date='';
        $to_date='';
    }

    // if (!empty($from_date) && !empty($to_date)) {

    //     $from_arr = explode('-', $from_date);

    //     $to_arr = explode('-', $to_date);

    //     $from_date = $from_arr[2].'-'.$from_arr[0].'-'.$from_arr[1];

    //     $to_date = $to_arr[2].'-'.$to_arr[0].'-'.$to_arr[1];

    // }





    // echo $from_date;

    // echo "<br>";

    // echo $to_date;

    // die;





    //$sql = 'SELECT * FROM `cp_vital_sign` WHERE `user_id` = "'.$user_id.'" AND DATE(`date`) >= DATE('.$from_date.') AND DATE(`date`) <= DATE('.$to_date.') ORDER BY `vital_sign_id` DESC';



    //$query = $this->db->query($sql);





    $this->db->select('*');

    $this->db->from('cp_vital_sign');

    $this->db->order_by('vital_sign_id','desc');

    $this->db->where('user_id',$user_id);

    if (!empty($from_date) && !empty($to_date)) {




        $this->db->where('date >=', $from_date);

        $this->db->where('date <=', $to_date);

    }

    if (!empty($test_type_filter) && $test_type_filter != 'All' && $test_type_filter != 'all') {

        $this->db->where('test_type',$test_type_filter);

    }

    $query = $this->db->get();

    $result = $query->result_array();    

    //echo $this->db->last_query();

    //print_r($this->db->last_query());

    //die;

    return $result;



    /*if(empty($from_date) && empty($to_date) && empty($test_type_filter)){

        $this->db->select('*');

        $this->db->from('cp_vital_sign');

        $this->db->order_by('vital_sign_id','desc');

        $this->db->where('user_id',$user_id);

        $query = $this->db->get();

        $result = $query->result_array();

        return $result;

    }elseif(!empty($from_date) && !empty($to_date) && empty($test_type_filter)){

        $this->db->select('*');

        $this->db->from('cp_vital_sign');

        $this->db->order_by('vital_sign_id','desc');

        $this->db->where('user_id',$user_id);

        $this->db->where('date >=', $from_date);

        $this->db->where('date <=', $to_date);

        $query = $this->db->get();

        $result = $query->result_array();

        return $result;

    }else{

        $this->db->select('*');

        $this->db->from('cp_vital_sign');

        $this->db->order_by('create_at','desc');

        $this->db->where('user_id',$user_id);

        $this->db->where('test_type',$test_type_filter);

        $query = $this->db->get();

        $result = $query->result_array();

        return $result;

    }*/

}



/*Delete vital sign*/

public function DeleteViatalById($user_id,$vital_id)

{

    $this->db->where('user_id', $user_id);

    $this->db->where('vital_sign_id', $vital_id);

    $this->db->delete('cp_vital_sign');

    $result = $this->db->affected_rows();

    return $result;

}





/*Get vital data by userid*/

public function GetvitalDataByVitalIdUserId($user_id,$vital_id)

{

   $this->db->select('*');

   $this->db->from('cp_vital_sign');

   $this->db->where('user_id',$user_id);

   $this->db->where('vital_sign_id',$vital_id);

   $query = $this->db->get();

   $result = $query->result_array();

   return $result;

}





}