<!DOCTYPE html>
<html lang="en">
<head>
<title>Health Care</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="<?php echo base_url();?>assets/user_dashboard/img/favicon.ico" rel="icon">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/bootstrap-4.1.2/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/costom.css">
<link href="<?php echo base_url();?>assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugins/OwlCarousel2-2.3.4/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugins/OwlCarousel2-2.3.4/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugins/OwlCarousel2-2.3.4/animate.css">
<!-- <link href="plugins/jquery-datepicker/jquery-ui.css" rel="stylesheet" type="text/css"> -->
<!-- <link href="plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css"> -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/main_styles.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/responsive.css">

<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">

</head>
<body>

<div class="super_container">
    
    <!-- Header -->
    <header class="header">
        <div class="header_content d-flex flex-row align-items-center justify-content-start">
            <div class="logo"><a href="<?php echo base_url(); ?>">Health Care</a></div>
            <div class="ml-auto d-flex flex-row align-items-center justify-content-start">
                <nav class="main_nav">
                    <ul class="d-flex flex-row align-items-start justify-content-start">
                        <li <?php if($menuactive == ''){
                            echo 'class="active"';
                        }?>><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li><a href="javascript:void(0);">Features</a></li>
                        <li><a href="about-us">About</a></li>
                        <li><a href="contact">Contact</a></li>
                    </ul>
                </nav>

                <?php 
                $username = $this->session->userdata['logged_in']['username'];

                 ?>

                <?php 
                if(isset($username)){ 
                    ?>

                <div class="book_button"><a href="signup">
                    <a href="logout" class="btn-flat">Sign out</a>
                </div>

                <?php 
                }else{ 
                ?>
                <div class="book_button"><a href="signup">
                    <i class="fa fa-lock"></i> Sign Up</a>
                </div>
                
                <?php 
            } 
            ?>
                
                <!-- <div class="book_button"><a href="signup"><i class="fa fa-lock"></i> Sign Up</a></div>
                 -->

                <!-- Hamburger Menu -->
                <div class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></div>
            </div>
        </div>
    </header>

    <!-- Menu -->

    <div class="menu trans_400 d-flex flex-column align-items-end justify-content-start">
        <div class="menu_close"><i class="fa fa-times" aria-hidden="true"></i></div>
        <div class="menu_content">
            <nav class="menu_nav text-right">
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li><a href="#">Features</a></li>
                    <li><a href="about.html">About</a></li>
                    <li><a href="contact.html">Contact</a></li>
                </ul>
            </nav>
        </div>
        <div class="menu_extra">
            <div class="menu_book text-right"><a href="#">Book online</a></div>
            <div class="menu_phone d-flex flex-row align-items-center justify-content-center">
                <img src="assets/images/phone-2.png" alt="">
                <span>0183-12345678</span>
            </div>
        </div>
    </div>