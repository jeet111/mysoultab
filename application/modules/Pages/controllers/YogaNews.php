<?php defined('BASEPATH') OR exit('No direct script access allowed');



class YogaNews extends MX_Controller {



	public function __construct() {

		parent:: __construct();

		$this->load->library('session');

		$this->load->library('form_validation');

		$this->load->model('activities_model');

		$this->load->model('Common_model_new');

		$this->load->model('common_model_new');

		$this->load->model('movie_model');

		$this->load->helper(array('common_helper'));

		$this->load->helper(array('url'));

		$this->load->helper(array('form'));

		not_login();

	}






    public function yoga_news(){
      $data['menuactive'] = '';
      $checkLogin = $this->session->userdata('logged_in');
  
      if(!empty($checkLogin))
      {
        $data['menuactive'] = $this->uri->segment(1);
       
        unset($_POST);
        $user_id = $this->session->userdata('logged_in')['id'];
        $data['app_list'] = $this->common_model_new->getAllwhere("app_list",array('status' => 1,'use_for'=>"Yoga News"));
        $data['yoganews_list'] = $this->common_model_new->getAllwhere("yoga_news",array('user_id'=>$this->session->userdata('logged_in')['id']));
      
      
        $this->template->set('title', 'Yoga News');
  
        $this->template->load('new_user_dashboard_layout', 'contents', 'yoga_news', $data);
      } 
    }  
  public function add_yoga_news()
  {
    if(count($_POST)>0){
        if($_POST['type']==1){
      
        
        $array =array(
           'user_id' => $this->session->userdata('logged_in')['id'],
           'social_type' => $this->input->post('yoganews_type'),
           'button_name' => $this->input->post('button_name'),
           'url'=>$this->input->post('url'),
           'app_name' => $this->input->post('app_name'),
           'created' => date('Y-m-d H:i:s'));
       }
// _dx($array);
       $check_transportation = $this->Common_model_new->getsingle("yoga_news",array("button_name" => $this->input->post('button_name'),"user_id" => $this->session->userdata('logged_in')['id']));
      if(!empty($check_transportation)){
        unset($_POST);
        $this->session->set_flashdata('success', "Yoga News already added");
        echo json_encode(array("statusCode"=>200,"msg"=>"Yoga News already added"));
        die();
      }
       $insert = $this->Common_model_new->insertData('yoga_news',$array);
       if($insert){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        $this->session->set_flashdata('success', "Yoga News added successfully");
        echo json_encode(array("statusCode"=>200,"msg"=>"Yoga News added successfully"));
        die();
      }

    }
  }
  public function edit_yoganews()
  {
    if(count($_POST)>0){
        if($_POST['type']==2){
        $news_id = $this->input->post('id');
        
        $array =array(
          'app_id'=>$this->input->post('appname_add_yoganews'),
           'user_id' => $this->session->userdata('logged_in')['id'],
           'social_type' => $this->input->post('yoganews_type'),
           'button_name' => $this->input->post('button_name'),
           'url'=>$this->input->post('url'),
           'app_name' => $this->input->post('app_name'),
           'type'=>'32',
           'created'=> date('Y-m-d H:i:s'));
       }
// _dx($array);
       $check_news = $this->Common_model_new->allexcludeone("yoga_news",array("button_name" => $this->input->post('button_name'),"user_id" => $this->session->userdata('logged_in')['id']),$news_id);
      // _dx($check_news);
      if(!empty($check_news)){
        unset($_POST);
        $this->session->set_flashdata('success', "Yoga News already added");
        echo json_encode(array("statusCode"=>200,"msg"=>"Yoga News already added"));
        die();
      }
       $update = $this->Common_model_new->updateData('yoga_news',$array,array('id' => $news_id));
       if($update){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        $this->session->set_flashdata('success', "Yoga News updated successfully");
        echo json_encode(array("statusCode"=>200,"msg"=>"Yoga News updated successfully"));
        die();
      }

    }
  }
  public function delete_yoganews()
  {
    if(count($_POST)>0){
        if($_POST['type']==3){
        $yoga_newsID = $this->input->post('id');

       $update = $this->Common_model_new->delete('yoga_news',array('id' => $yoga_newsID));
       if($update){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        echo json_encode(array("statusCode"=>200,"msg"=>"Yoga News deleted successfully"));
        die();
      }

    }
  }
}

}
?>
