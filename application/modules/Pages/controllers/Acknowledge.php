<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Acknowledge extends MX_Controller {



  public function __construct() {

    parent:: __construct();

    $this->load->library('session');

    $this->load->library('form_validation');

    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');       

    $this->load->model('medicine_model');

    $this->load->model('Common_model_new');

    $this->load->helper(array('common_helper'));

    $this->load->helper(array('url'));

    $this->load->helper(array('form'));      

  }









  

  /*Created by 95 for send acknowledge*/

  public function send_acknowledge()  

  {



    //die('asdfsd');

    $data['menuactive'] = $this->uri->segment(1);

    $checkLogin = $this->session->userdata('logged_in');

    if(!empty($checkLogin))

    {

      // $data['medicineData']  = $this->medicine_model->jointwotablenn('medicine_schedule','doctor_id', 'cp_doctor','doctor_id',$where=array('user_id'=>$this->session->userdata('logged_in')['id']),'*','medicine_id','desc');

      $this->template->set('title', 'Acknowledge');

      $this->template->load('user_dashboard_layout', 'contents', 'send_acknowledge');

    }else{

     redirect('login');   

   }

 }





 /*Created by 95 for show acknowledge list*/

 public function acknowledge_list()

 {

  $data['menuactive'] = $this->uri->segment(1);

  $checkLogin = $this->session->userdata('logged_in');

  if(!empty($checkLogin))

  {

    $data['acknowledge_list'] = $this->Common_model_new->jointhreetable("acknowledge", "pharma_company_id", "pharma_company", "id","medicine_schedule","medicine_id","medicine_id",$where=array('acknowledge.user_id'=>$this->session->userdata('logged_in')['id']),"*","acknowledge_id","desc");

	     //echo $this->db->last_query();die;



    $data['btnsetingArray'] = $this->Common_model_new->getAllwhere("setting_butns",array("btnname" =>'acknowledge_list'));

    $this->template->set('title', 'Acknowledge');

    $this->template->load('user_dashboard_layout', 'contents', 'acknowledge_list',$data);

  }else{

   redirect('login'); 

 }

}



public function add_acknowledge()

{ 

  $data['menuactive'] = $this->uri->segment(1);

  $checkLogin = $this->session->userdata('logged_in');

  if(!empty($checkLogin))

  {



    $this->form_validation->set_rules('acknowledge_title', 'Acknowledge Title', 'trim|required');



    $this->form_validation->set_rules('acknowledge_desc', 'Acknowledge Description', 'trim|required');

    $this->form_validation->set_rules('pharma_company_id', 'Pharma Company', 'trim|required');

    $this->form_validation->set_rules('medicine_id', 'Medicine', 'trim|required');



    $acknowledge_title = $this->input->post('acknowledge_title');

    $acknowledge_desc = $this->input->post('acknowledge_desc');

    $pharma_company_id = $this->input->post('pharma_company_id');

    $medicine_id = $this->input->post('medicine_id');



    $data['pharma_company'] = $this->Common_model_new->getAllor("pharma_company","name","asc");

    $data['medicine_list'] = $this->Common_model_new->getAllorwhere("medicine_schedule",array("user_id" => $checkLogin['id']),"medicine_name","asc");



    if($this->form_validation->run() == true)

    {

      $array = array(

        'acknowledge_title' => $acknowledge_title,

        'acknowledge_desc' => $acknowledge_desc,

        'pharma_company_id' => $pharma_company_id,

        'medicine_id' => $medicine_id,

        'user_id'=>$this->session->userdata('logged_in')['id'],

        'acknowledge_created' => date('Y-m-d H:i:s')

      );

      $this->Common_model_new->insertdata("acknowledge",$array);

      $this->session->set_flashdata('success', 'Successfully added');

      redirect('acknowledge_list'); 		  

    }



    $this->template->set('title', 'Acknowledge');

    $this->template->load('user_dashboard_layout', 'contents', 'add_acknowledge',$data);



  }else{

    redirect('login'); 

  }

}



public function edit_acknowledge()

{

	$data['menuactive'] = $this->uri->segment(1);

  $checkLogin = $this->session->userdata('logged_in');

  if(!empty($checkLogin))

  {



    $this->form_validation->set_rules('acknowledge_title', 'Acknowledge Title', 'trim|required');

    $this->form_validation->set_rules('acknowledge_desc', 'Acknowledge Description', 'trim|required');

    $this->form_validation->set_rules('pharma_company_id', 'Pharma Company', 'trim|required');

    $this->form_validation->set_rules('medicine_id', 'Medicine', 'trim|required');



    $segment = $this->uri->segment("2");

    $acknowledge_title = $this->input->post('acknowledge_title');

    $acknowledge_desc = $this->input->post('acknowledge_desc');

    $pharma_company_id = $this->input->post('pharma_company_id');

    $medicine_id = $this->input->post('medicine_id');



    $data['pharma_company'] = $this->Common_model_new->getAllor("pharma_company","name","asc");

    $data['medicine_list'] = $this->Common_model_new->getAllorwhere("medicine_schedule",array("user_id" => $checkLogin['id']),"medicine_name","asc");



    $data['singleData'] = $this->Common_model_new->jointhreetablenn("acknowledge", "pharma_company_id", "pharma_company", "id","medicine_schedule","medicine_id","medicine_id",array("acknowledge.acknowledge_id" => $segment),"*");



    if($this->form_validation->run() == true)

    {

      $array = array(



        'acknowledge_title' => $acknowledge_title,

        'acknowledge_desc' => $acknowledge_desc,

        'pharma_company_id' => $pharma_company_id,

        'medicine_id' => $medicine_id,

        'acknowledge_created' => date('Y-m-d H:i:s')

      );

      $this->Common_model_new->updateData("acknowledge",$array,array("acknowledge_id" => $segment));

      $this->session->set_flashdata('success', 'Successfully updated');

      redirect('acknowledge_list'); 		  

    }



    $this->template->set('title', 'Acknowledge');

    $this->template->load('user_dashboard_layout', 'contents', 'add_acknowledge',$data);



  }else{

    redirect('login'); 

  }

}



public function delete_acknowledge()

{

  $checkLogin = $this->session->userdata('logged_in');

  $segment = $this->uri->segment("2");

  if(!empty($checkLogin))

  {

    $this->Common_model_new->delete("acknowledge",array("acknowledge_id" => $segment));

    $this->session->set_flashdata('success', 'Successfully deleted');

    redirect('acknowledge_list');



  }

}



public function delete_checkboxes()

{ 

	$data['menuactive'] = $this->uri->segment(1);

  $checkLogin = $this->session->userdata('logged_in');

  if(!empty($checkLogin))

  {

    $explode = explode(',',$_POST["bodytype"]);

    

    foreach($explode as $id)

    {

      $where_condition = array('acknowledge_id' => $id);

      $upd = $this->Common_model_new->delete('acknowledge',$where_condition);

      //if($upd){

      echo 'success';

     // }

    }}else{

     redirect('login'); 



   }

 }



 /*Created by 95 on 27-2-2019 for show acknowledge detail*/

 public function acknowledge_detail()

 {

  $data['menuactive'] = $this->uri->segment(1);

  

  $checkLogin = $this->session->userdata('logged_in');

  if(!empty($checkLogin))

  {

    $segment = $this->uri->segment("2");



    $data['acknowledge_detail']	= $this->Common_model_new->jointhreetablenn("acknowledge", "pharma_company_id", "pharma_company", "id","medicine_schedule","medicine_id","medicine_id",array("acknowledge.acknowledge_id" => $segment),"*");



    $this->template->set('title', 'Acknowledge');

    $this->template->load('user_dashboard_layout', 'contents', 'acknowledge_detail',$data);

  }else{

   redirect('login'); 

 }

}





}