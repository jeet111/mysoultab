<div class="bx_user">
	<div class="add-btn">
		<a href="<?php echo base_url(); ?>admin/add/caregiver">Add User</a>
	</div>
</div>
<div class="container-fluid">
	<div class="dash-counter users-main my-sts-table">
		<div class="row">
			<div class="col-md-12">
				<div class="">
					<div class="card-body">
						<div class="status-cng"></div>
						<?php
						if($this->session->flashdata('success')) {
							$message = $this->session->flashdata('success');
							?>
							<div class="<?php echo $message['class'] ?>"><?php echo $message['message']; ?></div>
						<?php } ?>
						<div id="deletemsg"></div>
						<table class="table table-hover tab_comn" id="sampleTable">
							<thead>
								<tr>
									<th>S no.</th>
									<th>Username</th>
									<th>Email</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($users_data as $key => $user) {?>
									<tr>
										<td><?php echo ($key+1); ?></td>
										<td><?php echo $user['username']; ?></td>
										<td><?php echo $user['email']; ?></td>
										<td>
											<?php if($user['status']==1){ ?>
												<?php echo "<button type='' class='btn ybt btn-xs' disabled>Active</button>"; ?>
											<?php }else{ ?>
												<?php echo "<button type='' class='btn xbt btn-xs'  disabled>Inactive</button>"; ?>
											<?php } ?>
										</td>
										<td>
											<div class="link-del-view">
												<div class="tooltip-2">
													<a href="admin/viewcaregiver/caregiver/<?php echo $user['id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
													<span class="tooltiptext">View</span>
												</div>
												<div class="tooltip-2">
													<a href="admin/editcare/caregiver/<?php echo $user['id']; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>
													<span class="tooltiptext">Edit</span>
												</div>
												<div class="tooltip-2">
													<a href="javascript:void(0);" data-id="<?php echo $user['id']; ?>" id="deletemusiccat_<?php echo $user['id']; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>
													<!-- <a href="javascript:void(0)" onclick="deleteStatus(<?php echo $user['id']; ?>,'admin/delete/user')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span> -->
												</div>
												<?php if($user['status'] == 1) { ?>
													<div class="tooltip-2"><a href="javascript:void(0);" onclick="return updateStatus(<?php echo $user['id']; ?>, 'admin/user/status');"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
														<span class="tooltiptext">Active</span>
													</div>
												<?php }else { ?>
													<div class="tooltip-2"><a href="javascript:void(0);" onclick="return updateStatus(<?php echo $user['id']; ?>, 'admin/user/status');"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
														<span class="tooltiptext">Deactive</span>
													</div>
												<?php } ?>
											</div>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).on('click','[id^="deletemusiccat_"]',function(){
		var delete_music = $(this).attr('data-id');
		if(confirm("Are you sure, you want to delete this user ?")){
			$.ajax({
				type: "POST",
				url: '<?php echo base_url(); ?>Admin/Admin_user/delete_persone/',
				data: 'delete_music='+delete_music,
            //dataType: "json",
            success: function(data)
            {
            	if(data==1){
            		setTimeout(function(){
            			$("#deletemsg").html("<div class='alert alert-success'>Deleted Successfully.</div>");
            			location.reload();
            		}, 1000);
            	}
            }
        });
			$(this).parents(".card_one").animate({ backgroundColor: "#fbc7c7" }, "fast")
			.animate({ opacity: "hide" }, "slow");
		}
	});
</script>