<?php

if (isset($staff_permission[3]['add_permission']) == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn">

			<a href="<?php echo base_url(); ?>admin/doctor-category/Add">Add Specialties</a>

		</div>

	</div>

	<?php

} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn">

			<a href="<?php echo base_url(); ?>admin/doctor-category/Add">Add Specialties</a>

		</div>

	</div>

	<?php

}

?>





<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="row">

			<div class="col-md-12">

				<div class="">

					<div class="card-body">

						<div class="status-cng"></div>

						<div id="msg_show"></div>

						<?php

						if($this->session->flashdata('success_msg')) {

							$message = $this->session->flashdata('success_msg');

							?>



							<div class="alert alert-success">

								<button type="button" class="close" data-dismiss="alert">&times;</button>

								<?php echo $message; ?>

							</div>

							<?php

						}else if($this->session->flashdata('error_msg')){

							?>

							<div class="alert alert-danger">

								<button type="button" class="close" data-dismiss="alert">&times;</button>

								<strong><?php echo $this->session->flashdata('error_msg'); ?></strong>

							</div>

							<?php

						}

						?>



						<div id="deletemsg"></div>



						<!-- <div class="add-btn"><a href="<?php echo base_url(); ?>admin/doctor-category/Add">Add Specialties</a></div> -->

						<table class="table table-hover tab_comn" id="sampleTable">

							<thead>

								<tr>

									<th>S no.</th>

									<th>Category Name</th>

									<!-- <th>Doctor Name</th> -->

									<th>Category Icon</th>

									<th>Creat Date</th>

									<th>Action</th>

								</tr>

							</thead>

							<tbody>

								<?php  foreach ($dc_data as $key => $dc) {



									$doctors_list = $this->login_model->getwhereIns($dc->dc_id);



									$doctor_name = '';

									if(!empty($doctors_list)){
										foreach($doctors_list as $doctors){

											$doctor_name .= $doctors->doctor_name.',';

										}
									}

									$rtrim = rtrim($doctor_name,',');





									?>

									<tr>

										<td><?php echo ($key+1); ?></td>

										<td><?php echo ucfirst($dc->dc_name); ?></td>

										<!-- <td><?php if($rtrim){ echo $rtrim; }else{ echo 'NA'; } ?></td> -->







										<td class="cat-logo">



											<?php if(!empty($dc->dc_icon)){ ?>

												

												<img src="<?php echo base_url('uploads/doctor_category/'.$dc->dc_icon) ?>" height="50px" width="100px">

											<?php }else{ ?>	



												<img src="<?php echo base_url('uploads/doctor_category/default.png') ?>" height="50px" width="100px">

											<?php } ?>	

										</td>



										<td><?php echo date('d-m-Y',strtotime($dc->dc_created)); ?></td>

										<td>

											<div class="link-del-view">

												<?php

												if (isset($staff_permission[3]['view_permission']) == 1) {

													?>

													<div class="tooltip-2">

														<a href="admin/doctor-category/viewdoctor_category/<?php echo $dc->dc_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

														<span class="tooltiptext">View</span>

													</div>

													<?php

												} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

													?>

													<div class="tooltip-2">

														<a href="admin/doctor-category/viewdoctor_category/<?php echo $dc->dc_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

														<span class="tooltiptext">View</span>

													</div>

													<?php

												}

												?>

												

												<?php

												if (isset($staff_permission[3]['edit_permission']) == 1) {

													?>

													<div class="tooltip-2">

														<a href="admin/doctor-category/edit/<?php echo $dc->dc_id; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>

														<span class="tooltiptext">Edit</span>

													</div>

													<?php if($dc->dc_status == 1) { ?>

														<div class="tooltip-2"><a data-new="<?php echo $dc->dc_id; ?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>

															<span class="tooltiptext">Active</span>

														</div>

													<?php }else { ?>

														<div class="tooltip-2"><a data-new="<?php echo $dc->dc_id; ?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>

															<span class="tooltiptext">Deactive</span>

														</div>

													<?php } ?>

													<?php

												} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

													?>

													<div class="tooltip-2">

														<a href="admin/doctor-category/edit/<?php echo $dc->dc_id; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>

														<span class="tooltiptext">Edit</span>

													</div>

													<?php if($dc->dc_status == 1) { ?>

														<div class="tooltip-2"><a data-new="<?php echo $dc->dc_id; ?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>

															<span class="tooltiptext">Active</span>

														</div>

													<?php }else { ?>

														<div class="tooltip-2"><a data-new="<?php echo $dc->dc_id; ?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>

															<span class="tooltiptext">Deactive</span>

														</div>

													<?php } ?>

													<?php

												}

												?>

												

												<?php

												if (isset($staff_permission[3]['delete_permission']) == 1) {

													?>

													<div class="tooltip-2">

														<a href="javascript:void(0);" data-id="<?php echo $dc->dc_id; ?>" id="deletemusiccat_<?php echo $dc->dc_id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

														<!-- <a href="javascript:void(0)" onclick="deleteStatus(<?php echo $dc->dc_id; ?>,'admin/doctor-category/delete')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span> -->

													</div>

													<?php

												} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

													?>

													<div class="tooltip-2">

														<a href="javascript:void(0);" data-id="<?php echo $dc->dc_id; ?>" id="deletemusiccat_<?php echo $dc->dc_id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

														<!-- <a href="javascript:void(0)" onclick="deleteStatus(<?php echo $dc->dc_id; ?>,'admin/doctor-category/delete')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span> -->

													</div>

													<?php

												}

												?>

												

											</div>



										</div>

									</td>

								</tr>

							<?php } ?>

						</tbody>

					</table>

				</div>

			</div>

		</div>

	</div>

</div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<script>

	$(document).on('click','.updateStatus',function(){

		var update_status = $(this).attr('data-new');

		$.ajax({



			url: '<?php echo base_url();?>Admin/Doctor/change_category_status',

			type: "POST",

			data: {'update_status':update_status},

			dataType: "json",

			success:function(data){

			  //alert('test');

			  setTimeout(function(){

			  	$("#msg_show").html("<div class='alert alert-success'>"+data.status+"</div>");

			  	location.reload();



			  }, 1000);



			}

		});

	});







	$(document).on('click','[id^="deletemusiccat_"]',function(){

		var delete_music = $(this).attr('data-id');

		if(confirm("Are you sure, you want to delete this category ?")){

			$.ajax({

				type: "POST",

				url: '<?php echo base_url(); ?>Admin/Admin_user/deletedoctor_category/',

				data: 'delete_music='+delete_music,

            //dataType: "json",

            success: function(data)

            {

            	if(data==1){

            		setTimeout(function(){

            			$("#deletemsg").html("<div class='alert alert-success'>Deleted Successfully.</div>");

            			location.reload();



            		}, 1000);

            	}

            }

        });

			$(this).parents(".card_one").animate({ backgroundColor: "#fbc7c7" }, "fast")

			.animate({ opacity: "hide" }, "slow");

		}

	});

</script>