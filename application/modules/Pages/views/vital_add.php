<style>
	form#id_add_vital .form-group p {
		color: #ff0000;
		padding-top: 6px;
		float: left;

		width: 100%;
	}
	.doctorDiv .col-sm-6 {
    padding: 0;
}
	.coupon_checkbox h3 {
		color: #000;
		font-size: 14px;
		margin-top: 0;
		margin-bottom: 20px;
	}

	.coupon_checkbox input {
		margin-right: 6px !important;
	}

	.text_heading {
		border-bottom: none !important;
		margin: 0 0 14px 0;
	}

	label {
		font-weight: 400;
		margin-bottom: 10px;
		color: #000;
	}

	input.btn.btn-primary {
		margin: 0 !important;
	}
</style>
<div class="text_heading">
	<?php if ($this->session->flashdata('success_msg')) { ?>
		<div class="container box">
			<div class="alert alert-success message" id="success_msg">
				<button type="button" class="close" data-dismiss="alert">x</button>
				<p align="center"><?php echo $this->session->flashdata('success_msg'); ?></p>
			</div>
		</div>
	<?php } ?>

	<!--- Error Message --->
	<?php if ($this->session->flashdata('faild_msg')) { ?>
		<div class="container box">
			<div class="alert alert-danger message" id="faild_msg">
				<button type="button" class="close" data-dismiss="alert">x</button>
				<p align="center"><?php echo $this->session->flashdata('faild_msg'); ?></p>
			</div>
		</div>
	<?php } ?>

	<h3 class="text-center"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Add Vital</h3>
	<div class="rem_add"><a href="javascript:window.history.back()" class="btn btn-primary">Vital List</a></div>
</div>

<div class="content photo-list">
	<div class="photo-listMain">
		<!--- Success Message --->
		<form id="id_add_vital" enctype="multipart/form-data" method="post" class="frm_add" autocomplete="off">

			<div class="row">
				<div class="col-sm-6 col-md-6">
					<div class="bootstrap-timepicker">
						<div class="form-group">
							<label>Vital Add:<span class="red_star">*</span></label>
							<div class="input-group date shedule" id="datepickers2">
								<input type="text" class="form-control" readonly="" id="vital_add_date" name="vital_add_date" value="" placeholder="Select your vital add date">
								<?php echo form_error('vital_add_date'); ?>
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-6">
					<div class="form-group">
						<label>Checkup Type<font style="color:red;">*</font>:</label>
						<select name="test_type" type="text" class="form-control" id="test_type">
							<option value="">Select</option>
							<?php
							if (!empty($test_type_arr) && $test_type_arr != '') {
								foreach ($test_type_arr as $k) {
							?>
									<option value="<?php echo $k['test_type']; ?>" id="<?php echo $k['id']; ?>"><?php echo $k['test_type']; ?></option>
							<?php }
							} ?>
						</select>
						<?php echo form_error('test_type'); ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 col-md-6">
					<div class="form-group">
						<label>Ur Doctor.<font style="color:red;">*</font>:</label>
						<select name="doctor" type="text" id="doctor" class="form-control">
							<option value="">Select</option>
							<?php
							if (!empty($doctor_arr) && $doctor_arr != '') {
								foreach ($doctor_arr as $key) {
							?>
									<option value="<?php echo $key['doctor_name']; ?>" id="<?php echo $key['doctor_id']; ?>"><?php echo $key['doctor_name']; ?></option>
							<?php }
							} ?>
						</select>
						<?php echo form_error('doctor'); ?>
					</div>
				</div>
				<div class="col-sm-6 col-md-6">
					<div class="form-group">
						<label>Checkup Type Unit<font style="color:red;">*</font>:</label>
						<input type="text" class="form-control" id="unit" name="unit" value="" placeholder="Enter unit type">
						<?php echo form_error('unit'); ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-md-6">
					<div class="coupon_checkbox">

						<h3><input class="doctorName" type="checkbox" name="doctorName" value="1" onchange="DoctorvalueChanged()" />Select Other Doctor</h3>
						<div class="doctorDiv">


							<div class="form-group">
								<label>if your Doctor name is not listed please enter your Doctor name here</label>
								<input type="text" name="other_doctor_name" class="form-control" id="other_doctor_name" placeholder="Doctor Name" value="" maxlength="100">
								<?php echo form_error('other_doctor_name'); ?>
							</div>
							<div class="col-sm-6 col-md-6">
              <div class="form-group">
                <label>Email Address</label>
                <input type="text" name="other_doctor_email_address" class="form-control" id="other_doctor_email_address" placeholder="Email Address" value="" maxlength="100">
                <?php echo form_error('other_doctor_email_address'); ?>
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="form-group">
                <label>Fax Number</label>
                <input type="text" name="other_doctor_fax_number" class="form-control" id="other_doctor_fax_number" placeholder="Fax Number" value="" maxlength="100">
				<?php echo form_error('other_doctor_fax_number'); ?>

              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="form-group">
                <label>Address</label>
                <input type="text" name="other_doctor_address" class="form-control" id="other_doctor_address" placeholder="Address" value="" maxlength="100">
				<?php echo form_error('other_doctor_address'); ?>

              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="form-group">
                <label>Phone Number</label>
                <input type="text" name="other_doctor_phone_number" class="form-control" id="other_doctor_phone_number" placeholder="Phone Number" value="" maxlength="100">
				<?php echo form_error('other_doctor_phone_number'); ?>

              </div>
            </div>
						</div>
					</div>
				</div>
				
				<div class="col-sm-6 col-md-6">
					<div class="coupon_checkbox">

						<h3><input class="coupon_question" type="checkbox" name="coupon_question" value="1" onchange="valueChanged()" />Select Other Test Report</h3>
						<div class="answer">


							<div class="form-group">
								<label>if your test name is not listed please enter your Test name here</label>
								<input type="text" name="other_test_report" class="form-control" id="other_test_report" placeholder="Test Name" value="" maxlength="100">
								<?php echo form_error('other_test_report'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input type="submit" name="submit" class="btn btn-primary" value="submit">
				<input type="reset" name="cancel" class="btn btn-primary" value="Cancel" onclick="window.location='<?php echo base_url(); ?>vital_signs';">
			</div>
		</form>
	</div>
</div>

<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery-3.5.1.js"></script>

<script type="text/javascript">
	$(".answer").hide();
	$(".doctorDiv").hide();

	function valueChanged() {
		if ($('.coupon_question').is(":checked")) {
			$(".answer").show();
			$("#test_type").prop('disabled', 'disabled');
			$('input[type="text"]').attr('required', 'required');

		} else {
			$(".answer").hide();
			$("#test_type").removeAttr("disabled");

		}
	}

	function DoctorvalueChanged() {
		if ($('.doctorName').is(":checked")) {
			$(".doctorDiv").show();
			$("#doctor").prop('disabled', 'disabled');
			$('input[type="text"]').attr('required', 'required');

		} else {
			$(".doctorDiv").hide();
			$("#doctor").removeAttr("disabled");

		}
	}
	$('#add_vital').click(function() {
		var url = '<?php echo base_url() . "vital_add" ?>';
		window.location.href = url;
		return false;
	});

	$(document).ready(function() {

		setTimeout(function() {
			$('#success_msg').fadeOut('fast');
		}, 2500);

		$('#id_add_vital').validate({
			rules: {
				vital_add_date: 'required',
				test_type: 'required',
				doctor: 'required',
				unit: 'required',
			},

			messages: {
				vital_add_date: 'Please select a vital add date.',
				test_type: 'Please select a checkup Type.',
				doctor: 'Please select a doctor.',
				unit: 'Please enter unit yype',
			}
		});
	});

	$(function() {
		$('#datepickers2').datetimepicker({
			ignoreReadonly: true,
			format: 'YYYY-MM-DD',
			maxDate: moment()
		});
		$('#vital_add_date').datetimepicker({
			ignoreReadonly: true,
			format: 'YYYY-MM-DD',
			maxDate: moment()
		});
	});
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>
