<aside class="right-side">    

  <section class="content-header no-margin">

    <h1 class="text-center"><i class="fa fa-tasks" aria-hidden="true"></i> All Activities </h1>

  </section>  

  <section class="content weather-bg news_section">
    <!-- setting start -->
    <div class="col-md-12">

      <div id="msg_show"></div>

      <div class="col-md-10">
        <h3>All Activities</h3>

        <?php $current_user_id = $_SESSION['logged_in']['id'];
         foreach($btnsetingArray as $res){
          $userid = $res->user_id;
          if($current_user_id == $userid){
            if($res->settings == 1) { ?>
            <div class="tooltip-2"><a data-btn="<?php echo 'all_activities'?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
              <span class="tooltiptext">Active</span>
            </div>
          <?php }else { ?>
            <div class="tooltip-2 "><a data-btn="<?php echo 'all_activities'?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
              <span class="tooltiptext">Deactive</span>
            </div>
          <?php } ?>
        <?php }
        } ?>
      </div>
    </section>

    <?php /* ?>
    <section class="content weather-bg news_section">

      <!-- setting start -->
      <div class="col-md-12">
        <div class="col-md-10">
          <h3>All Activities</h3>
          <?php if(!empty($btnsetingArray)){ ?>
            <?php if($btnsetingArray[0]->settings=='1'){ ?>

             <label><input type="radio" name="btnshow" value='1' checked="checked">Now All Activities app showing on the mysoultab APP<br></label>

           <?php }else{ ?>

             <label> <input type="radio" name="btnshow" value='1' checked="checked">Now All Activities app not showing on the mysoultab APP </label>

           <?php } ?>

         <?php } ?> 
       </div>
       <div class="col-md-2 delete-email">
        <button class="btn btn-primary chk">Settings</button>
      </div> 
      <div id="para" style="display: none "> 



        <form id="submit_form" method="post" action="<?php echo base_url()?>savsettings "> 
          <div class="col-md-10">
            <label> <input type="checkbox" name="butn_seting" id="butn_seting" value="">Please check if want to show the All Activities button in mysoultab App </label>
            <input type="hidden" name="btn" value="all_activities">
          </div>
          <div class="col-md-2">
           <input class="btn btn-primary chkdd" type="submit" name="submit" value="submit" >
         </div>
       </form>
       <div id="response"></div>
     </div>

     <!-- </div> -->

     <!-- End -->
   </section>

   <?php */ ?>

   <input type="hidden" name="hidden_activity" id="hidden_activity" value="<?php echo $this->session->userdata("hidden_activity"); ?>" />

   <input type="hidden" name="hidden_activity_year" id="hidden_activity_year" value="<?php echo $this->session->userdata("hidden_activity_year"); ?>" />	



   <section class="content photo-list">

    <div class="photo-listMain activity_ajax_main">

     <div id="shedule_dates"></div>









     <ul class="activity_show">

      <?php 

      if($reminder_data || $appointment_data){

       foreach($reminder_data as $reminder){ ?>

         <li>



          <div class="activity_show_list">

            <a href="javascript:void(0);" id="reminder_id_<?php echo $reminder->reminder_id; ?>" data-remider="<?php echo $reminder->reminder_id; ?>"><?php echo '<b>Reminder: </b>'.$reminder->reminder_title; ?></a>

          </div>

          <div class="activity_show_list_second">

            <?php echo $reminder->reminder_date; ?>

          </div>

        </li>

        <?php 

      } 

      foreach($medicine_data as $medicine){ ?>

       <li>



        <div class="activity_show_list">

          <a href="javascript:void(0);" id="medicine_id_<?php echo $medicine->medicine_id; ?>" data-medicine="<?php echo $medicine->medicine_id; ?>"> <?php echo '<b>Medicine: </b>'.$medicine->medicine_name; ?></a>

        </div>

        <div class="activity_show_list_second">

          <?php echo date('Y-m-d'); ?>

        </div>

      </li>

      <?php 

    } 		

    foreach($appointment_data as $appointment){

     ?>

     <li>



      <div class="activity_show_list">

        <a href="javascript:void(0);" id="appointment_id_<?php echo $appointment->dr_appointment_id; ?>" data-appointment="<?php echo $appointment->dr_appointment_id; ?>"> <?php echo '<b>Appointment</b>'; ?></a>

      </div>

      <div class="activity_show_list_second">

        <?php echo $appointment->avdate_date; ?>

      </div>

    </li>

  <?php }}else{ ?>

   <div class="photo-list-empty">

     <span class="blnk_photo"><i class="fa fa-picture-o" aria-hidden="true"></i></span>

     <h4>Activities not found</h4>

   </div>

 <?php } ?>

</ul>







</div>



</section>

</aside>



<!----------------------Activities Info Modal------------------- -->



<div class="modal fade" id="reminder_info_modal" tabindex="-1" role="dialog" aria-hidden="true">

  <div class="modal-dialog">

    <div class="modal-content">

      <div id="getCode">

        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

          <h4 class="modal-title"> Activities Information </h4>

        </div>

        <div id="addmsg" style="text-align: center;"></div>





        <div class="modal-body">

          <div class="form-group">

            <div class="input-group">

              <span class="input-group-addon">TO:</span>

              <input name="email_to" id="email_to" type="email" class="form-control" placeholder="Enter your email">

            </div>

            <p id="emailerror" style="color: red"></p>

            <p id="valideemailerror" style="color: red"></p>

          </div>



          <div class="form-group">

            <div class="input-group">

              <span class="input-group-addon">Subject:</span>

              <input name="subject" id="subject" type="text" class="form-control" placeholder="Enter your subject">

            </div>

            <p id="subjecterror" style="color: red"></p>

          </div>



          <div class="form-group">

            <textarea name="message" id="email_message" name="message" class="form-control" placeholder="Enter your description" style="height: 120px;"></textarea>



            <p id="emailmsgerror" style="color: red"></p>

          </div>

          <div class="form-group">

            <!-- <input class="form-control" type="file" name="attachment_file" id="upload_file" /> -->



            <input type="hidden" name="new_hidden" id="new_hidden" value="" >

            <div class="btn btn-success btn-file">

              <i class="fa fa-paperclip"></i> Attachment





              <input class="form-control" type="file" multiple name="attachment_file[]" id="upload_file" onchange="showMyImage(this)" />





            </div>

            <div id="progress-wrp"><div class="progress-bar"></div ><div class="status">0%</div></div>

            <div id="testing"></div>



            <!-- <p class="help-block">Max. 32MB</p>-->

          </div>



        </div>

        <div class="modal-footer clearfix">



          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Discard</button>



          <button type="submit" class="btn btn-primary pull-left form-submit" name="submit" id="sendemail1">

            <i class="fa fa-envelope"></i> Send Message</button>

          </div>

          <div id='loadingmessage' style='display:none'>

            <img src='assets/img/loading.gif'/>

          </div>



        </div>

      </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

  </div>



  <!-----------------------------------appointment info modal------------------ -->





  <div class="modal fade" id="appointment_info_modal" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog">

      <div class="modal-content">

        <div id="getCodenew" class="newcodecls">

          <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

            <h4 class="modal-title"> Activities Information </h4>

          </div>

          <div id="addmsg" style="text-align: center;"></div>



          <div class="modal-body">

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon">TO:</span>

                <input name="email_to" id="email_to" type="email" class="form-control" placeholder="Enter your email">

              </div>

              <p id="emailerror" style="color: red"></p>

              <p id="valideemailerror" style="color: red"></p>

            </div>



            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon">Subject:</span>

                <input name="subject" id="subject" type="text" class="form-control" placeholder="Enter your subject">

              </div>

              <p id="subjecterror" style="color: red"></p>

            </div>



            <div class="form-group">

              <textarea name="message" id="email_message" name="message" class="form-control" placeholder="Enter your description" style="height: 120px;"></textarea>



              <p id="emailmsgerror" style="color: red"></p>

            </div>

            <div class="form-group">

              <!-- <input class="form-control" type="file" name="attachment_file" id="upload_file" /> -->



              <input type="hidden" name="new_hidden" id="new_hidden" value="" >

              <div class="btn btn-success btn-file">

                <i class="fa fa-paperclip"></i> Attachment





                <input class="form-control" type="file" multiple name="attachment_file[]" id="upload_file" onchange="showMyImage(this)" />





              </div>

              <div id="progress-wrp"><div class="progress-bar"></div ><div class="status">0%</div></div>

              <div id="testing"></div>



              <!-- <p class="help-block">Max. 32MB</p>-->

            </div>



          </div>

          <div class="modal-footer clearfix">



            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Discard</button>



            <button type="submit" class="btn btn-primary pull-left form-submit" name="submit" id="sendemail1">

              <i class="fa fa-envelope"></i> Send Message</button>

            </div>

            <div id='loadingmessage' style='display:none'>

              <img src='assets/img/loading.gif'/>

            </div>



          </div>

        </div><!-- /.modal-content -->

      </div><!-- /.modal-dialog -->

    </div>





    <!-------------------------------medicine data-------------- -->



    <div class="modal fade" id="medicine_info_modal" tabindex="-1" role="dialog" aria-hidden="true">

      <div class="modal-dialog">

        <div class="modal-content">

          <div id="getCodemedicine">

            <div class="modal-header">

              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

              <h4 class="modal-title"> Activities Information </h4>

            </div>

            <div id="addmsg" style="text-align: center;"></div>

            

            <div class="modal-body">

              <div class="form-group">

                <div class="input-group">

                  <span class="input-group-addon">TO:</span>

                  <input name="email_to" id="email_to" type="email" class="form-control" placeholder="Enter your email">

                </div>

                <p id="emailerror" style="color: red"></p>

                <p id="valideemailerror" style="color: red"></p>

              </div>



              <div class="form-group">

                <div class="input-group">

                  <span class="input-group-addon">Subject:</span>

                  <input name="subject" id="subject" type="text" class="form-control" placeholder="Enter your subject">

                </div>

                <p id="subjecterror" style="color: red"></p>

              </div>



              <div class="form-group">

                <textarea name="message" id="email_message" name="message" class="form-control" placeholder="Enter your description" style="height: 120px;"></textarea>



                <p id="emailmsgerror" style="color: red"></p>

              </div>

              <div class="form-group">

                <!-- <input class="form-control" type="file" name="attachment_file" id="upload_file" /> -->



                <input type="hidden" name="new_hidden" id="new_hidden" value="" >

                <div class="btn btn-success btn-file">

                  <i class="fa fa-paperclip"></i> Attachment





                  <input class="form-control" type="file" multiple name="attachment_file[]" id="upload_file" onchange="showMyImage(this)" />





                </div>

                <div id="progress-wrp"><div class="progress-bar"></div ><div class="status">0%</div></div>

                <div id="testing"></div>



                <!-- <p class="help-block">Max. 32MB</p>-->

              </div>



            </div>

            <div class="modal-footer clearfix">



              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Discard</button>



              <button type="submit" class="btn btn-primary pull-left form-submit" name="submit" id="sendemail1">

                <i class="fa fa-envelope"></i> Send Message</button>

              </div>

              <div id='loadingmessage' style='display:none'>

                <img src='assets/img/loading.gif'/>

              </div>



            </div>

          </div><!-- /.modal-content -->

        </div><!-- /.modal-dialog -->

      </div>







      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.css">

      <script src="<?php echo base_url(); ?>assets/js/jquery-1.12.4.js"></script>

      <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>





      <!--Added by 95 on 5-2-2019 For Doctor category for validation-->

      <script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>









      <script>

       jQuery(document).ready(function() {

	//$(function () { 



    var eventDates = {};

    <?php 

    foreach ($alldates as $eventDate) {

      $dat = date('m/d/Y',strtotime($eventDate['date']));

      ?>eventDates[new Date('<?php echo $dat; ?>')] = new Date('<?php echo $dat; ?>').toString();<?php

    }

    ?>



    $('#shedule_dates').datepicker({

     dateFormat: 'yy-mm-dd', 

     onSelect: function (date) { 		 

      show_activities(date);  

    },

    beforeShowDay: function(date){

      var highlight = eventDates[date];

      if( highlight ) {

       return [true, "event", highlight];

     } else {

       return [true, '', ''];

     }

   }

 });



    $(document).on('click','.ui-icon-circle-triangle-e',function(){	

      var month = $("#hidden_activity").val();

      var year = $('#hidden_activity_year').val();

      $.ajax({

        url: "<?php echo base_url("Pages/Activities/showmonth"); ?>",

        type:"POST",

        data: "month="+month+'&year='+year,



        success:function(data){

         $("#activity_ajax_main").val(data);

         $("#hidden_activity").val(data.newmonth);

         $("#hidden_activity_year").val(data.newyear);

       }

     });

    });



    $(document).on('click','.ui-icon-circle-triangle-w',function(){	

      var month = $("#hidden_activity").val();

      $.ajax({

        url: "<?php echo base_url("Pages/Activities/previousshowmonth"); ?>",

        type:"POST",

        data: "month="+month,

       // dataType: "json",		

       success:function(data){

         $("#hidden_activity").val(data);

       }

     });

    });

  });



       function show_activities(date)

       {



        $.ajax({

          url: "<?php echo base_url("Pages/Activities/ajax_activities"); ?>",

          type:"POST",

          data: "date="+date,	

          dataType: "json",  		

          success:function(data){

            $(".activity_show").html(data.list);	

          }

        });

      }



      $(document).on('click','[id^="reminder_id_"]',function(){	   

        var reminder_id = $(this).attr('data-remider');

        $.ajax({

          url: "<?php echo base_url("Pages/Activities/view_reminder_activities"); ?>",

          type:"POST",

          data: "reminder_id="+reminder_id,	       	

          success:function(response){

            $("#getCode").html(response);

            $("#reminder_info_modal").modal('show'); 	

          }

        });

      });



      $(document).on('click','[id^="appointment_id_"]',function(){	   

        var appointment_id = $(this).attr('data-appointment');

        $.ajax({

          url: "<?php echo base_url("Pages/Activities/view_apointment_activities"); ?>",

          type:"POST",

          data: "appointment_id="+appointment_id,	       	

          success:function(response){

            $("#getCodenew").html(response);

            $("#appointment_info_modal").modal('show'); 	

          }

        });

      });



      $(document).on('click','[id^="medicine_id_"]',function(){	   

        var medicine_id = $(this).attr('data-medicine');

        $.ajax({

          url: "<?php echo base_url("Pages/Activities/view_medicine_activities"); ?>",

          type:"POST",

          data: "medicine_id="+medicine_id,	       	

          success:function(response){

            $("#getCodemedicine").html(response);

            $("#medicine_info_modal").modal('show'); 	

          }

        });

      });

    </script>  





    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>



    <script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>


    <script type="text/javascript">
      $(document).on('click','.updateStatus',function(){
        var update_status = $(this).attr('data-btn');
        var update_st = $(this).attr('data-st');
    //alert(update_status+'-'+update_st);
    $.ajax({
      url: '<?php echo base_url();?>Pages/pages/change_button_status',
      type: "POST",
      data: {'btn':update_status},
      dataType: "json",
      success:function(data){
        setTimeout(function(){
          $("#msg_show").html("<div class='alert alert-success'>"+data.status+"</div>");
          location.reload();

        }, 1500);
      }
    }); 
  });
</script>


<style>

  * {

    margin: 0 auto;

    padding: 0;

  }



  body {

    background-color: #F2F2F2;

  }



  .container {

    margin: 0 auto;

    width: 920px;

    padding: 50px 20px;

    background-color: #fff;

  }



  h3 {

    text-align: center;

  }



  #calendar {

    margin-top: 40px;

  }



  .event a {

    background-color: #42B373 !important;

    background-image :none !important;

    color: #ffffff !important;

  }

</style>