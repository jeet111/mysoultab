<aside class="right-side">
  <section class="content-header no-margin">
    <h1 class="text-center">
      <!-- <i class="fa fa-newspaper-o"></i>  -->
      <i class="fa fa-phone-square" aria-hidden="true"></i>
      Contact Detail
    <div class="back_reminder"><a href="javascript:window.history.back()" class="btn btn-danger">Back</a></div>
    </h1>
  </section>
  <?php //echo "<pre>"; print_r($UserContact); echo "</pre>"; ?>
  <section class="content photo-list">
    <div class="con_detailMain">
      <div class="row">       
      <div class="col-md-offset-3 col-md-5">
          <div class="bx_drDetail">
            <div class="bx_nam">          
          <div class="bx_usr">
              <div class="usr_img">         
                 <?php if($UserContact->user_contact_image==''){ ?>                       
                  <img  id='output' src="<?php echo base_url().'uploads/profile_images/user.png' ?>">
                <?php }else{ ?>
                <img  id='output' src="<?php echo base_url().'uploads/contact/'.$UserContact->user_contact_image; ?>">

              <?php } ?> 
                </div>
                <h2 class="tm_member"><?php echo $UserContact->contact_name; ?></h2>
          </div>

              
              <div class="tm_position">
                <form>      
                    <input type="text"  name="code" value="<?php echo $UserContact->contact_mobile_number; ?>" maxlength="10" class="display form-control"  />
                    <table id="keypad" cellpadding="5" cellspacing="3">
                    <tr>
                        <td onclick="addCode('1');">1</td>
                          <td onclick="addCode('2');">2</td>
                          <td onclick="addCode('3');">3</td>
                      </tr>
                      <tr>
                        <td onclick="addCode('4');">4</td>
                          <td onclick="addCode('5');">5</td>
                          <td onclick="addCode('6');">6</td>
                      </tr>
                      <tr>
                        <td onclick="addCode('7');">7</td>
                          <td onclick="addCode('8');">8</td>
                          <td onclick="addCode('9');">9</td>
                      </tr>
                      <tr>
                        <td ></td>
                          <td onclick="addCode('0');">0</td>
                          <td ></td>                              
                      </tr>
                  </table>
                  </form>                   
                </div>
            </div>
            <ul class="tm-team-details-list">
              <li>                
                <div class="tm-team-list-value">
                  <div class="bx_apoi">
                    <button type="button" class="btn_apoi" name="submit">
                      <i class="fa fa-phone" aria-hidden="true"></i></button>
                  </div> 
                </div>
                <div class="tm-team-list-title">Voice Call</div>
              </li>
              <li>                
                <div class="tm-team-list-value">
                  <div class="bx_apoi">
                    <button type="button" class="btn_apoi" name="submit" id="appointment">
                      <i class="fa fa-video-camera" aria-hidden="true"></i></button>
                  </div> 
                </div>
                <div class="tm-team-list-title">Video Call</div>
              </li>
              <li>                
                <div class="tm-team-list-value">
                  <div class="bx_apoi">
                    <button type="button" class="btn_apoi" name="submit" id="appointment">
                     <i class="fa fa-comments-o" aria-hidden="true"></i></button>
                  </div> 
                </div>
                <div class="tm-team-list-title">Chat</div>
              </li>
            </ul>          
          </div>          
        </div>        
      </div>
    </div>    
  </div>
</section>
</aside>


<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/user_dashboard/css/jquery.numpad.css"> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script src="<?php echo base_url();?>assets/user_dashboard/js/jquery.numpad.js"></script> -->

<!-- <script type="text/javascript">
      $(document).ready(function(){
        $('#text-basic').numpad();
        
      });
    </script> -->



<script type="text/javascript">
  function addCode(key){
    //alert(key);
  var code = document.forms[0].code;
  if(code.value.length < 10){
    code.value = code.value + key;
  }else{
    alert('Do not enter more then 10 digits');
  }
  
}
</script>


<style>

#keypad{        
        margin-top:-1px;
      }

#keypad tr td {
    vertical-align:middle;
    text-align:center;
    border:1px solid #cccccc;
    font-size:18px;
    font-weight:500;
    max-width:33%;
    height:40px;
    cursor:pointer;
    background-color:#ffffff;
    color:#000000;
  }
#keypad tr td:hover {
     background-color: #cccccc;
    color: #000000;
  }
.tm_position {
    max-width: 300px;
    margin: 0px auto;
}
.tm_position table#keypad {
    width: 100%;
}
ul.tm-team-details-list {
    list-style: none;
    padding: 0px;
    margin: 0px;
    border-top: 1px solid #cccccc;
    padding-top: 25px;
    margin-top: 20px;
    display: inline-block;
    width: 100%;
    text-align: center;
  }
.con_detailMain .tm-team-details-list > li {
    float: none;
    display: inline-block;
    width: 30%;
  }
#message {
  text-align:center; 
  color:#009900; 
  font-size:14px; 
  font-weight:bold; 
  display:none;
  }
.tm_position .display.form-control {
    height: 45px;
        font-size: 20px !important;
  }
ul.tm-team-details-list {
    border-top: 1px solid #cccccc;
  }
.con_detailMain .tm-team-details-list li {
    box-shadow: 0px 0px 1px #949494;  
  }

</style>


    

    


     