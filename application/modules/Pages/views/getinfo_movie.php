
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.css">
<aside class="right-side new-music mus_list">
	<!--<div class="searchbar-wrap">
		<input autocomplete="off" class="form-control" id="" name="" placeholder="Search Movie" type="text">
	</div>-->
	<section class="doc_cate">
		<div class="row">
			<div class="col-md-9">
				<div class="bx_leftScrollmain mCustomScrollbar">
					<div class="bx_leftScroll">
						<div class="bx_viewAll">
							<div class="inside-pageinfo-wrap">
								<div class="bx_moviMain" id="test">
									<video  controls autoplay style="width:100%; height:auto;" >
										<source id="moivedata" data-myval="" src="<?php echo base_url(); ?>uploads/movie/<?php echo $getinfo_movie->movie_file; ?>" type="video/mp4">
											Sorry, your browser doesn't support the video element.
										</video>
									</div>
									<div class="bx_getInfo">
										<div class="inside-pageinfo-wrap">
											<div class="inside-pageinfo-lhs">
												<div class="image-cont colimg ">
													<img id="" src="<?php echo base_url(); ?>uploads/movie/image/<?php echo $getinfo_movie->movie_image; ?>" alt="" class="mCS_img_loaded">
												</div>
											</div>
											<div class="inside-pageinfo-rhs">
												<ul class="info_like">
													<li id="re"><a href="javascript:void(0);" id="favourite_info_music" data-segment="<?php echo base64_decode($this->uri->segment('2')); ?>"><?php if($check_info_movie){ ?><i class="fa fa-heart remove" aria-hidden="true"></i><?php }else{ ?><i class="fa fa-heart-o remove"></i><?php } ?></a></li>
													<li style="display:none" id="renew"><a href="javascript:void(0);" id="favourite_info_music" data-segment="<?php echo base64_decode($this->uri->segment('2')); ?>"><i class="fa fa-heart" aria-hidden="true"></i></a></li>
													<li style="display:none" id="renew2"><a href="javascript:void(0);" id="favourite_info_music" data-segment="<?php echo base64_decode($this->uri->segment('2')); ?>"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
												</ul>
												<div class="pageinfo-top">
													<div class="pageinfo-top-lhs">
														<h1><?php echo $getinfo_movie->movie_title; ?></h1>
														<div class="myplaylist_totalsong"><?php echo $getinfo_movie->movie_artist; ?></div>

													</div>
													<div class="pageinfo-top-rhs">
														<span class="icon-but-overflow"></span>
													</div>
												</div>
												<div class="pageinfo-bottom">
													<button onclick="getId('<?php echo $getinfo_movie->movie_id; ?>')" id="play_now_<?php echo $getinfo_movie->movie_id; ?>" data-music="85"><i class="fa fa-play"></i> Play</button>
													<button id="download_m_<?php echo $getinfo_movie->movie_id; ?>" data-download-movie="<?php echo $getinfo_movie->movie_id; ?>" class="white-button"><i class="fa fa-download"></i> Download</button>
												</div>
											</div>
										</div>
									</div>

									<div class="cont-wrap">
										<ul class="getinfolist">
											<li>
												<p>Name:</p>
												<p><?php echo $getinfo_movie->movie_title; ?></p>
											</li>

											<li>
												<p>Description:</p>
												<p><?php echo $getinfo_movie->movie_desc; ?></p>
											</li>
										</ul>
									</div>


								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 mrg_non">
					<div class="bx_right bx_movRight">
						<div class="up_box">
							<h5 class="pull-left">You may also like-</h5>
							<div class="back-info">
								<a href="javascript:window.history.back()"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</a>
							</div>
						</div>
						<div class="listing_sng mCustomScrollbar">
							<ul class="list_songs">
								<?php 
								/*if(!empty($like_movie)){
									foreach($like_movie as $movie_cat){ */
										/*if($movie_cat->movie_id != $movie_id){*/
											?>
											<li>
												<div class="queue-movie">
													<div class="mov_qu">


														<!-- <a href="javascript:void(0);" onclick="getId('<?php echo $like_movie->playmovie_id; ?>')"> -->

															<a href="<?php echo base_url(); ?>getinfo_movie/<?php echo $this->uri->segment(2); ?>" >


																<img alt="" src="<?php echo base_url(); ?>uploads/movie/image/<?php echo $like_movie->playmovie_image; ?>">
																<span class="iconplay"><i class="fa fa-play-circle"></i></span>
															</a>

															<!-- </a> -->
														</div>
														<div class="nam_view">
															<h5><a href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($like_movie->movie_id) ?>" ><?php echo $like_movie->playmovie_title; ?></a></h5>
															<?php if($like_movie->playmovie_views != 0){ ?><span><?php echo $like_movie->playmovie_views; ?> Views</span><?php } ?>
														</div>
													</div>
												</li>
											<?php //}}} ?>


										</ul>
									</div>
								</div>
							</div>
						</div>
					</section>
				</aside>


				<script src="<?php echo base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>

				<script>
					(function($){
						$(window).on("load",function(){

							$("a[rel='load-content']").click(function(e){
								e.preventDefault();
								var url=$(this).attr("href");
								$.get(url,function(data){
						$(".content .mCSB_container").append(data); //load new content inside .mCSB_container
						//scroll-to appended content
						$(".content").mCustomScrollbar("scrollTo","h2:last");
					});
							});

							$(".content").delegate("a[href='top']","click",function(e){
								e.preventDefault();
								$(".content").mCustomScrollbar("scrollTo",$(this).attr("href"));
							});

						});
					})(jQuery);

					function getId(movie_id) {
						$.ajax({
							type: "POST",
							url: '<?php echo base_url(); ?>Pages/Movie/playMov/',
							data: {'movie_id': movie_id},
            //dataType: "json",
            success: function(data)
            {
            	$("#test").html(data);
            }
        });



					}

					$(document).on('click','[id^="download_m_"]',function(){
						var movie_id = $(this).attr("data-download-movie");
						window.location = "<?php echo base_url(); ?>Pages/Movie/download_movie/"+movie_id;

					});

					$(document).on('click','#favourite_info_music',function(){
						var movie_id = $(this).attr('data-segment');
						$.ajax({
							type:'POST',
							url: "<?php echo base_url().'Pages/Movie/favourite_movie'; ?>",
							data: 'movie_id=' + movie_id,
							success:function(data){
								$("#re").hide();
								if(data == '0'){
									$("#renew").hide();
									$("#renew2").show();
									$("#msgs").html('<div class="alert alert-success">'+'Successfully unfavourite'+'</div>');
									setTimeout(function(){
										$("#msgs").html("");
									}, 1500);

								}else{
									$("#msgs").html('<div class="alert alert-success">'+'Successfully favourite'+'</div>');
									setTimeout(function(){
										$("#msgs").html("");
									}, 1500);
									$("#renew2").hide();
									$("#renew").show();
								}

							}
						});
					});
				</script>
				<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>
