<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Alerts extends MX_Controller {
	
	public function __construct() {
		parent:: __construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('table');
		$this->load->library('pagination');
		$this->load->model('common_model');
		$this->load->model('common_model_new');
		$this->load->model('Common_model_new');
		$this->load->helper(array('common_helper'));
		$this->load->helper(array('url'));
		$this->load->helper(array('form'));
		not_login();
	}


	public function getall_alerts_list(){

		$checkLogin = $this->session->userdata('logged_in');
		
		if(!empty($checkLogin) && $checkLogin !=''){
						
			$user_id = isset($checkLogin['id']) ? $checkLogin['id'] : "";
			
			$alerts_arr = $this->common_model_new->getAllwhereorderby("alerts",array("user_id" => $user_id),'id','DESC');
			
			if(!empty($alerts_arr) && $alerts_arr !=''){
				
				$result = obj_to_array($alerts_arr);

				$result = array_map("unserialize", array_unique(array_map("serialize", $result)));
			
			
				//all status cnt
				$result = array_values(array_filter($result));
				
				foreach($result as $key=>$val){
					
					$result[$key]['total_read_flag'] = count($result);
					$read_flag = array_count_values(array_column($result, "read_flag"));
					
					foreach($read_flag as $k=>$v){
						
						switch ($k) {
						  case '1':
							$result[$key]['cnt_read_flag'] 	 = isset($v) ? $v : '0';
							break;
						  case '0':
							$result[$key]['cnt_unread_flag'] = isset($v) ? $v : '0';
							break;
						  default:
						}
						
						$cnt_read_flag 	= isset($result[$key]['cnt_read_flag']) ? $result[$key]['cnt_read_flag'] : '0';
						$cnt_unread_flag = isset($result[$key]['cnt_unread_flag']) ? $result[$key]['cnt_unread_flag'] : '0';
								
						if($result[$key]['total_read_flag'] == $cnt_unread_flag){
							$result[$key]['cnt_read_flag'] ='0';
						}
						
						if($result[$key]['total_read_flag'] == $cnt_read_flag){
							$result[$key]['cnt_unread_flag'] ='0';
						}
					}
				}
				$result['alerts_array'] = $result;
			}else{
				$result['alerts_array'] ='';
			}
			
			$this->template->set('title','Alerts');
			$this->template->load('new_user_dashboard_layout','contents','alerts',$result);
		}else{
			redirect('login');
		}
	}
	
	public function update_status(){
   
		$alert_id = isset($_POST['id']) ? $_POST['id'] : "";
			  
		if (!empty($alert_id) && $alert_id !=''){

			$data = $this->common_model_new->getsingle("alerts",array("id" => $alert_id)); 
		
			$read_flag = ($data->read_flag == '1' ? '0' : '1');
			
			$condition	= array('id' =>$data->id);
			$array 		= array('read_flag' => $read_flag);
			
			$update_login = $this->common_model->updateRecords('alerts', $array, $condition);
				
			if ($update_login) {
				echo 1;
			} else {
				echo 0;
			}
			
		}else{
			redirect('login');
		}
	}	 
}
  
?>
