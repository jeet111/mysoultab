     <aside class="right-side new-music mus_list">
     	<div class="searchbar-wrap">
     		<div class="src_sbmit">
     			<input autocomplete="off" class="form-control" id="music_search" name="music_search" placeholder="Search Music" value="<?php echo htmlspecialchars($this->uri->segment('2')); ?>" type="text">
     			<button type="submit" id="btn_music" value="Submit"><i class="fa fa-search"></i></button>				
     		</div>
     		<a href="<?php echo base_url(); ?>add_music" class="add_music_btn" ><i class="fa fa-music" aria-hidden="true"></i>Add Music</a>
     	</div>
     	<section class="content-header no-margin">
     	</section>

     	<section class="content weather-bg news_section">
     		<!-- setting start -->
     		<div class="col-md-12">

     			<div id="msg_show"></div>

     			<div class="col-md-10">
     				<h3>Music</h3>

     				<?php //echo "<pre>"; print_r($btnsetingArray); ?>

     				<?php $current_user_id = $_SESSION['logged_in']['id'];
         foreach($btnsetingArray as $res){
          $userid = $res->user_id;
          if($current_user_id == $userid){
            if($res->settings == 1) { ?>
     						<div class="tooltip-2"><a data-btn="<?php echo 'music'?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
     							<span class="tooltiptext">Active</span>
     						</div>
     					<?php }else { ?>
     						<div class="tooltip-2 "><a data-btn="<?php echo 'music'?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
     							<span class="tooltiptext">Deactive</span>
     						</div>
     					<?php } ?>
					 <?php }
					 } ?>
     			</div>
     		</section>

     		<section class="doc_cate">
     			<div class="row">
     				<div class="col-md-9">
     					<div class="bx_leftScrollmain mCustomScrollbar">
     						<div class="bx_leftScroll">
     							<div id="bx_slid">
     								<div class="main_sld">
     									<div class="sdl">
     										<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
     											<!-- <ol class="carousel-indicators"> -->
     												<?php 
						  	//$j=0;
						  	//foreach ($music_banner as $banner) { ?>
						  		<!--  <li data-target="#carousel-example-generic" data-slide-to="<?php //echo $j; ?>" class="active"></li> -->
						  		<?php //$j++;}?>
						  		<!-- </ol>						  -->
						  		<div class="carousel-inner" role="listbox">
						  			<?php 
						  			$i=0;
						  			foreach ($music_banner as $banner) { ?>
						  				<?php if($i==0){ ?>
						  					<div class="item active">
						  						<img src="<?php echo base_url().'uploads/music/banners/'.$banner->music_banner_image;?>">						     
						  					</div>
						  				<?php }else{ ?>
						  					<div class="item ">
						  						<img src="<?php echo base_url().'uploads/music/banners/'.$banner->music_banner_image;?>">						     
						  					</div>
						  				<?php } ?>
						  				<?php $i++;} ?>
						  			</div>						
						  			<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						  				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						  				<span class="sr-only">Previous</span>
						  			</a>
						  			<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						  				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						  				<span class="sr-only">Next</span>
						  			</a>
						  		</div>
						  	</div>
						  </div>
						</div>
						<div id="combine_tab">		
							<div class="widget-header">
								<h2>Populars</h2>
								<div class="wid-header-rhs">
									<?php if(count($popular_count_array) > 10){ ?>
										<a href="<?php echo base_url(); ?>view_all_music/popular_music"> View all </a>
									<?php } ?>
								</div>
							</div>
							<div class="demo">
								<div class="item">                        
								</div>
								<div class="item">
									<ul id="content-slider1" class="content-slider">
										<?php foreach ($popular_array as $popular) { ?>
											<li>
												<div class="rail-thumb">
													<a class="dropdown-item" href="javascript:void(0);" id="play_now3_<?php echo $popular->music_id; ?>" data-music="<?php echo $popular->music_id; ?>"><img src="<?php echo base_url().'uploads/music/image/'.$popular->music_image; ?>"></a>
													<span class="playbutton">
														<a class="dropdown-item" href="javascript:void(0);" id="play_now3_<?php echo $popular->music_id; ?>" data-music="<?php echo $popular->music_id; ?>"><i class="fa fa-play-circle"></i></a>
													</span>			    			
												</div>
												<div class="rail-content" title="">
													<a title="<?php echo $popular->music_title;  ?>" href="<?php echo base_url(); ?>getinfo_music/<?php echo base64_encode($popular->music_id); ?>"><?php echo $popular->music_title;  ?> </a>
													<div class="btn-group new-music-btn">
														<a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
														<div class="dropdown-menu">	
															<a class="dropdown-item" href="javascript:void(0);" id="play_now3_<?php echo $popular->music_id; ?>" data-music="<?php echo $popular->music_id; ?>">Play Now</a>
															<a class="dropdown-item download_music_<?php echo $popular->music_id; ?>" href="<?php echo base_url(); ?>Pages/Music/download_music/<?php echo $popular->music_id; ?>" data-music-id="<?php echo $popular->music_id; ?>">Download</a>
															<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_music/<?php echo base64_encode($popular->music_id); ?>">Get info</a>
														</div>
													</div>			    			
												</div>
											</li>
										<?php } ?>
									</ul>
								</div>
							</div>
							<div class="widget-header">
								<h2>Recents</h2>
								<div class="wid-header-rhs">
									<?php if(count($popular_count_array) > 10){ ?>
										<a href="<?php echo base_url(); ?>view_all_music/recent_music"> View all </a>
									<?php } ?>
								</div>
							</div>
							<ul id="content-slider2" class="content-slider">
								<?php foreach ($recent_array as $recent) { ?>
									<li>
										<div class="rail-thumb">
											<a class="dropdown-item" href="javascript:void(0);" id="play_now1_<?php echo $recent->music_id; ?>" data-music="<?php echo $recent->music_id; ?>"><img src="<?php echo base_url().'uploads/music/image/'.$recent->music_image; ?>"></a>
											<span class="playbutton">
												<a class="dropdown-item" href="javascript:void(0);" id="play_now1_<?php echo $recent->music_id; ?>" data-music="<?php echo $recent->music_id; ?>"><i class="fa fa-play-circle"></i></a>
											</span>			    			
										</div>
										<div class="rail-content" title="">
											<a title="<?php echo $recent->music_title;  ?>" href="<?php echo base_url(); ?>getinfo_music/<?php echo base64_encode($recent->music_id); ?>"> <?php echo $recent->music_title;  ?> </a>
											<div class="btn-group new-music-btn">
												<a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="javascript:void(0);" id="play_now1_<?php echo $recent->music_id; ?>" data-music="<?php echo $recent->music_id; ?>">Play Now</a>
													<a class="dropdown-item download_music_<?php echo $recent->music_id; ?>" href="<?php echo base_url(); ?>Pages/Music/download_music/<?php echo $recent->music_id; ?>" data-music-id="<?php echo $recent->music_id; ?>">Download</a>
													<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_music/<?php echo base64_encode($recent->music_id); ?>">Get info</a>
												</div>
											</div>			    			
										</div>
									</li>
								<?php } ?>
							</ul>
							<div class="widget-header">
								<h2>Categories</h2>
								<div class="wid-header-rhs">
									<?php if(count($category_count_array) > 10){ ?>
										<a href="<?php echo base_url(); ?>view_all_categories"> View all </a>
									<?php } ?>
								</div>
							</div>
							<ul id="content-slider3" class="content-slider">
								<?php foreach ($category_array as $category) { ?>
									<li>
										<div class="rail-thumb">
											<a href="<?php echo base_url(); ?>view_all_music/<?php echo $category->music_category_id; ?>"><img src="<?php echo base_url().'uploads/music/category_icon/'.$category->music_category_icon; ?>"></a>		    			
										</div>
										<div class="rail-content" title="">
											<a title="<?php echo $category->music_category_name;  ?>" href="<?php echo base_url(); ?>view_all_music/<?php echo $category->music_category_id; ?>"> <?php echo $category->music_category_name;  ?> </a>
										</div>
									</li>                      
								<?php } ?>
							</ul>
							<!-------------------------for favorite section------------ -->
							<?php if(!empty($favorite_music)){ ?>
								<div class="widget-header">
									<h2>Favorite Music</h2>
									<div class="wid-header-rhs">
										<?php if(count($favorite_count_array) > 10){ ?>
											<a href="<?php echo base_url(); ?>view_all_music/favorite_music"> View all </a>
										<?php } ?>
									</div>
								</div>
								<ul id="content-slider4" class="content-slider">
									<?php foreach ($favorite_music as $favorite) { 
										?>
										<li>
											<div class="rail-thumb">
												<a class="dropdown-item" href="javascript:void(0);" id="play_now1_<?php echo $favorite->music_id; ?>" data-music="<?php echo $favorite->music_id; ?>"><img src="<?php echo base_url().'uploads/music/image/'.$favorite->music_image; ?>"></a>
												<span class="playbutton">
													<a class="dropdown-item" href="javascript:void(0);" id="play_now1_<?php echo $favorite->music_id; ?>" data-music="<?php echo $favorite->music_id; ?>"><i class="fa fa-play-circle"></i></a>
												</span>			    			
											</div>
											<div class="rail-content" title="">
												<a title="<?php echo $favorite->music_title;  ?>" href="<?php echo base_url(); ?>getinfo_music/<?php echo base64_encode($favorite->music_id); ?>"><?php echo $favorite->music_title;  ?> </a>
												<div class="btn-group new-music-btn">
													<a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
													<div class="dropdown-menu">
														<a class="dropdown-item" href="javascript:void(0);" id="play_now1_<?php echo $favorite->music_id; ?>" data-music="<?php echo $favorite->music_id; ?>">Play Now</a>
														<a class="dropdown-item download_music_<?php echo $favorite->music_id; ?>" href="<?php echo base_url(); ?>Pages/Music/download_music/<?php echo $favorite->music_id; ?>"  data-music-id="<?php echo $favorite->music_id; ?>">Download</a>
														<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_music/<?php echo base64_encode($favorite->music_id); ?>">Get info</a>
													</div>
												</div>			    			
											</div>
										</li>
									<?php } ?>
								</ul>
							<?php } ?>
							<!------------------------------------------------------ -->
							<div class="mymusic-slider">
								<?php if(!empty($mymusic)){ ?>
									<div class="widget-header">
										<h2>My Music</h2>
										<div class="wid-header-rhs">
											<?php if(count($mymusic_count_array) > 10){ ?>
												<a href="<?php echo base_url(); ?>view_all_music/my_music"> View all </a>
											<?php } ?>
										</div>
									</div>
									<ul id="content-slider5" class="content-slider">
										<?php foreach ($mymusic as $music) { ?>                                
											<li class="test">
												<div class="rail-thumb">
													<a class="dropdown-item play_now_<?php echo $music->music_id; ?>" href="javascript:void(0);" id="play_now_<?php echo $music->music_id; ?>" data-music="<?php echo $music->music_id; ?>"><img src="<?php echo base_url().'uploads/music/image/'.$music->music_image; ?>"></a>
													<span class="playbutton">
														<a class="dropdown-item play_now_<?php echo $music->music_id; ?>" href="javascript:void(0);" id="play_now_<?php echo $music->music_id; ?>" data-music="<?php echo $music->music_id; ?>"><i class="fa fa-play-circle"></i></a>
													</span>			    			
												</div>
												<div class="rail-content" title="">
													<a title="<?php echo $music->music_title;  ?>" href="<?php echo base_url(); ?>getinfo_music/<?php echo base64_encode($music->music_id); ?>"><?php echo $music->music_title;  ?> </a>
													<div class="btn-group new-music-btn">
														<a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
														<div class="dropdown-menu music_dropdown">
															<a class="dropdown-item play_now_<?php echo $music->music_id; ?>" href="javascript:void(0);" id="play_now_<?php echo $music->music_id; ?>" data-music="<?php echo $music->music_id; ?>">Play Now</a>
															<a class="dropdown-item download_music_<?php echo $music->music_id; ?>" href="<?php echo base_url(); ?>Pages/Music/download_music/<?php echo $music->music_id; ?>" data-music-id="<?php echo $recent->music_id; ?>">Download</a>
															<a class="dropdown-item" href="javascript:void(0);" id="remove_mymusic_<?php echo $music->music_id; ?>" data-mymusic="<?php echo $music->music_id; ?>" >Remove</a>
															<a class="dropdown-item" href="<?php echo base_url(); ?>edit_music/<?php echo base64_encode($music->music_id); ?>">Edit</a>																		
															<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_music/<?php echo base64_encode($music->music_id); ?>">Get info</a>
														</div>
													</div>			    			
												</div>
											</li>
										<?php } ?>
									</ul>
								<?php } ?>
							</div></div>
						</div>
					</div>
				</div>
				<div class="col-md-3 mrg_non">
					<div class="bx_right" id="upload_music">	
						<?php if(!empty($playlist_music)){ ?>
							<div class="queue-header">
								<div class="hd_que">
									<h5 class="pull-left">Queue</h5>
								</div>
								<div class="main_chk">
									<div class="bx_chkAll">
										<span class="select-input">
											<input class="chk_bx" id="checkall" type="checkbox">
											<span> Select All</span>
										</span>
									</div>
									<div class="bx_rgt">
										<a href="javascript:void(0);" class="clear-queue-popover btn_delete" data-id="" id="btn_delete"> Remove</a>
									</div>
								</div>
							</div>
							<div class="listing_sng mCustomScrollbar">
								<ul class="list_songs playlist1" id="playlist">
									<?php
									if(!empty($playlist_music)){
										foreach($playlist_music as $playlist){ ?>
											<li class="active test_play">
												<div class="select-input_one"><input type="checkbox" name="sub_chk[]" value="<?php echo $playlist->music_playlist_id; ?>" data-id="<?php echo $playlist->music_playlist_id; ?>" class="sub_chk" ></div>
												<a href="<?php echo base_url(); ?>uploads/music/<?php echo $playlist->music_file; ?>"><div class="queue-songlist-lhs">
													<div class="queue-songlist-img">
														<div class="song-current">
															<img class="click_image_change_<?php echo $playlist->music_id; ?>" data-image-change="<?php echo $playlist->music_id; ?>" alt="" src="<?php echo base_url(); ?>uploads/music/image/<?php echo $playlist->music_image; ?>">
														</div>
													</div>
													<div class="queue-songlist-info">
														<p class="click_image_change_<?php echo $playlist->music_id; ?>" data-image-change="<?php echo $playlist->music_id; ?>"><?php echo $playlist->music_title; ?></p>
														<span class="click_image_change_<?php echo $playlist->music_id; ?>" data-image-change="<?php echo $playlist->music_id; ?>"><?php echo $playlist->music_artist; ?></span>
													</div>
												</div></a>
												<div class="queue-songlist-rhs queue-play-list">
													<div class="btn-group new-music-btn">
														<button class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
														<div class="dropdown-menu">
															<button class="dropdown-item" href="javascript:void(0);" id="remove_play_now_<?php echo $playlist->music_playlist_id; ?>" data-music-remove="<?php echo $playlist->music_playlist_id; ?>">Remove</button>
															<button class="dropdown-item  download_music_<?php echo $playlist->music_id; ?>" href="<?php echo base_url(); ?>Pages/Music/download_music/<?php echo $playlist->music_id; ?>" id="testi_<?php echo $playlist->music_id; ?>" data-music-id="<?php echo $playlist->music_id; ?>" >Download</button>
															<button class="dropdown-item" href="javascript:void(0);" id="getinfo_m_<?php echo $playlist->music_id ?>" data-getinfo="<?php echo base64_encode($playlist->music_id); ?>">Get info</button>
														</div>
													</div>
												</div>
											</li>
										<?php } ?>
									<?php }else{ ?>	
										<div class="music_norecord">No record found</div> 
									<?php } ?>
								</ul>
							</div>
							<div class="bx_mus">
								<div class="ply_sng">
									<div class="sng-crnt">
										<?php if($single_playlist_music->music_image){ ?>
											<img alt="" src="<?php echo base_url(); ?>uploads/music/image/<?php echo $single_playlist_music->music_image; ?>">
										<?php }else{ ?>
											<img src="<?php echo base_url().'uploads/music/defualt_music.png' ?>">
										<?php } ?>
									</div>
									<div class="sng-txt">
										<h4><?php echo $single_playlist_music->music_title; ?></h4>
										<p><?php echo $single_playlist_music->music_artist; ?></p>
									</div>
								</div>
								<audio id="audio" preload="auto" tabindex="0" controls="" type="audio/mpeg">
									<source type="audio/mp3" id="audiotest" src="<?php echo base_url(); ?>uploads/music/<?php echo $single_playlist_music->music_file; ?>">
										Sorry, your browser does not support HTML5 audio.
									</audio>
								</div>
							<?php }else{ ?>
								<div class="play-list-empty">
									<span class="blnk_queue"><i class="fa fa-music" aria-hidden="true"></i></span>
									<h4>Your queue is empty</h4>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</section>
		</aside>
		<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<!-- 
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->
	<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightslider.css"/>
	<script src="<?php echo base_url(); ?>assets/js/lightslider.js"></script> 
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.css">
	<script src="<?php echo base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript">
		$(document).on('click','.updateStatus',function(){
			var update_status = $(this).attr('data-btn');
			var update_st = $(this).attr('data-st');
    //alert(update_status+'-'+update_st);
    $.ajax({
    	url: '<?php echo base_url();?>Pages/pages/change_button_status',
    	type: "POST",
    	data: {'btn':update_status},
    	dataType: "json",
    	success:function(data){
    		setTimeout(function(){
    			$("#msg_show").html("<div class='alert alert-success'>"+data.status+"</div>");
    			location.reload();

    		}, 1500);
    	}
    }); 
});

</script>
<script>
	<?php if(!empty($playlist_music)){ ?>
		var audio;
		var playlist;
		var tracks;
		var current;
		init();
		function init(){
			current = 0;
			audio = $('audio');
			playlist = $('.playlist1');
			tracks = playlist.find('li a');
			len = tracks.length - 1;
			audio[0].volume = .10;
			audio[0].play();
			playlist.find('a').click(function(e){ 
				e.preventDefault();
				link = $(this);
				current = link.parent().index();
				run(link, audio[0]);
			});
			audio[0].addEventListener('ended',function(e){
				if(current == len){
					current = 0;
					link = playlist.find('a')[0];
				}else{
					current++;
					link = playlist.find('a')[current];    
				}
				run($(link),audio[0]);
			});
		}
		function run(link, player){
			player.src = link.attr('href');
			par = link.parent();
			par.addClass('active').siblings().removeClass('active');
			audio[0].load();
			audio[0].play();
		}
	<?php } ?>
	$(document).ready(function() {
		$(".content-slider").lightSlider({
			loop:true,
			keyPress:true,
			responsive: [
			{
				breakpoint:1200,
				settings: {
					item:4,
				}                   
			},
			{
				breakpoint:1100,
				settings: {
					item:3,
				}
			},
			{
				breakpoint:520,
				settings: {
					item:2,
				}
			},
			{
				breakpoint:360,
				settings: {
					item:1,
				}
			}
			],
		});			
		$('#image-gallery').lightSlider({
			gallery:true,
			item:1,
			thumbItem:9,
			slideMargin: 0,    
			speed:500,
			auto:true,
			loop:true,
			onSliderLoad: function() {
				$('#image-gallery').removeClass('cS-hidden');
			}  
		});
	});
//jQuery('#checkall').on('click', function(e) {
	$(document).on('click','#checkall',function(){
		if($(this).is(':checked',true))  
		{ 
			$(".sub_chk").prop('checked', true); 
		}  
		else  
		{ 
			$(".sub_chk").prop('checked',false);  
		}  
	});
   //jQuery('.sub_chk').on('click',function(){
   	$(document).on('click','#checkall',function(){   
   		if($('.sub_chk:checked').length == $('.sub_chk').length){
   			$('#checkall').prop('checked',true);
   		}else{
   			$('#checkall').prop('checked',false);
   		}
   	});
   	$(document).on('click','[id^="testi_"]',function(){ 
   		var music_id = $(this).attr("data-music-id");
   		window.location = "<?php echo base_url(); ?>Pages/Music/download_music/"+music_id;
   	});
   	$(document).on('click','[class^="download_music_"]',function(){ alert("dsafdsf");
   		var music_id = $(this).attr("data-music-id");
   		window.location = "<?php echo base_url(); ?>Pages/Music/download_music/"+music_id;
   	});
   	$(document).on('click','.btn_delete', function(e){
   		var bodytype = $('input[name="sub_chk[]"]:checked').map(function () {  
   			return this.value;
   		}).get().join(",");
   		if(bodytype != ''){
   			if(confirm("Are you sure you want to remove this audio?")){	
   				$.ajax({
   					url: '<?php echo base_url(); ?>Pages/Music/delete_checkboxes',
   					type: 'POST',
   					data: {bodytype:bodytype},
   					dataType: "json",
                success: function(data) { //alert('swati');
                setTimeout(function(){
                	$("#expire_msg").html("<div class='alert alert-success'>Successfully deleted !</div>");
                	$("#upload_music").load(location.href + " #upload_music");
            //location.reload();
//window.location.href = email_url+"email_list";
}, 1000);
            }
        });
   			}
   		}else{
   			alert("Please select atleast one audio.");
   		}
   	});
   	$(document).on('click','[id^="play_now_"]',function(){ 
   		var music_id = $(this).attr("data-music");
   		$.ajax({
   			url:"<?php echo base_url().'play_music'?>",
   			method:"POST",
   			data:{music_id:music_id},
   			success:function(data)
   			{
   				$("#upload_music").show();
   				$("#upload_music").html(data);
   			}
   		})
   	});  
   	$(document).on('click','[id^="play_now1_"]',function(){ 
   		var music_id = $(this).attr("data-music");
   		$.ajax({
   			url:"<?php echo base_url().'play_music'?>",
   			method:"POST",
   			data:{music_id:music_id},
   			success:function(data)
   			{
   				$("#upload_music").show();
   				$("#upload_music").html(data);
   			}
   		})
   	});  
   	$(document).on('click','[id^="play_now3_"]',function(){ 
   		var music_id = $(this).attr("data-music");
   		$.ajax({
   			url:"<?php echo base_url().'play_music'?>",
   			method:"POST",
   			data:{music_id:music_id},
   			success:function(data)
   			{
   				$("#upload_music").show();
   				$("#upload_music").html(data);
   			}
   		})
   	}); 
   	$(document).on('click','[id^="remove_play_now_"]',function(){
   		var music_id = $(this).attr("data-music-remove");
   		if(confirm("Are you sure you want to remove this audio?")){
   			$.ajax({
   				url:"<?php echo base_url().'Pages/Music/remove_play_music'?>",
   				method:"POST",
   				data:{music_id:music_id},
   				success:function(data)
   				{ 
   					if(data == 2){
   						$("#upload_music").html("<div class='play-list-empty'>Your Queue is empty</div>");
		  //$('.play-list-empty_display').html();
		}
	}
})
   			$(this).parents(".test_play").animate({ backgroundColor: "#fbc7c7" }, "fast")
   			.animate({ opacity: "hide" }, "slow");
   		}
   	});
   	$(document).on('click','[id^="remove_mymusic_"]',function(){ 
   		var music_id = $(this).attr("data-mymusic");
   		if(confirm("Are you sure you want to remove this audio?")){
   			$.ajax({
   				url:"<?php echo base_url().'Pages/Music/remove_my_music'?>",
   				method:"POST",
   				data:{music_id:music_id},
   				success:function(data)
   				{ 
   					if(data == 2){
   						$(".mymusic-slider").fadeOut();
   					}
	//
}
})
   			$(this).parents(".test").animate({ backgroundColor: "#fbc7c7" }, "fast")
   			.animate({ opacity: "hide" }, "slow");
   		}
   	}); 
   	$(document).on('click','[class^="click_image_change_"]',function(){
   		var music_id = $(this).attr("data-image-change");
   		$.ajax({
   			url:"<?php echo base_url().'Pages/Music/image_change'?>",
   			method:"POST",
   			data:{music_id:music_id},
   			success:function(data)
   			{ 
   				$('.ply_sng').html(data);
   			}
   		})
   	});
   	$(document).on('click','[id^="getinfo_m_"]',function(){
   		var music_id = $(this).attr("data-getinfo");
   		window.location = "<?php echo base_url(); ?>getinfo_music/"+music_id;
   	});
   </script>
   <script>
   	(function($){
   		$(window).on("load",function(){
   			$("a[rel='load-content']").click(function(e){
   				e.preventDefault();
   				var url=$(this).attr("href");
   				$.get(url,function(data){
						$(".content .mCSB_container").append(data); //load new content inside .mCSB_container
						//scroll-to appended content 
						$(".content").mCustomScrollbar("scrollTo","h2:last");
					});
   			});
   			$(".content").delegate("a[href='top']","click",function(e){
   				e.preventDefault();
   				$(".content").mCustomScrollbar("scrollTo",$(this).attr("href"));
   			});
   		});
   	})(jQuery);
	/*$(document).on('keyup','#music_search',function(e){  
	$(this).append('<div id="video_loader" class="loading_class"><img id="loading_img" style="width:20px" src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif" /></div>');
		var music_search = $("#music_search").val();
	   $.ajax
		({
			url: "<?php echo base_url(); ?>Pages/Music/search_music_list",
			type: "POST",             
			data: 'music_search='+music_search, 
			//dataType: "json",			        
			success: function(response)   
			{
				//$('.bx_leftScroll').html(response);
				$('#combine_tab').html(response);
			}
	   });
	}); */	
	$(document).on('click','#btn_music',function(e){ 
		var music_search = $("#music_search").val();
		window.location = "<?php echo base_url(); ?>music_list/"+music_search;	
	}); 	      
</script>
<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>