<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MyMusic extends MX_Controller {

  public function __construct() {
      parent:: __construct();
      $this->load->library('session');
      $this->load->library('form_validation');      
	  $this->load->model('activities_model');
	  $this->load->model('Common_model_new');
	  $this->load->model('mymusic_model');
      $this->load->helper(array('common_helper'));
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));  
not_login();	  
  }
  
  
  public function mymusic_list()
  {


  	//die('oewiuroiuweor');

	  $data['menuactive'] = $this->uri->segment(1);
	 $checkLogin = $this->session->userdata('logged_in');
	 
	   if(!empty($checkLogin)){
		    $data['page'] = 0;
			$pagecon['per_page'] = 20;
	    $music_list1 = $this->mymusic_model->MyMusic_list($pagecon['per_page'],$data['page']);
		$data['music_list'] = $music_list1['rows'];
		$data['count_total'] = $music_list1['num_rows'];
	    $this->template->set('title', 'My Music');
        $this->template->load('user_dashboard_layout', 'contents', 'mymusic.php',$data);
	   }
	     
  }




 public function get_offset()
  {
	  $data['offset'] = $this->input->post('offset');
	  $pagecon['per_page'] = 20;
	  $music_list1 = $this->mymusic_model->MyMusic_list($pagecon['per_page'],$data['offset']);
	  //echo '<pre>';print_r($music_list1);die;
	  if (!empty($music_list1['rows'])) {
                $data['music_list'] = $music_list1['rows'];
                $data['count_total'] = $music_list1['num_rows'];
            } else {
                $data['music_list'] = '';
                $data['count_total'] = '';
            }
		 $this->load->view('ajax_mymusicvalue', $data);
  }



  /*Created by 95 for show all the activities*/
  public function mymusic_detail()
	{ 
  $data['menuactive'] = $this->uri->segment(1);

   $checkLogin = $this->session->userdata('logged_in');
    if(!empty($checkLogin))
    {
		$segment = $this->uri->segment('2');
      $data['detail_music']  = $this->Common_model_new->getsingle("music",array("music_id" => $segment));


	  $data['check_music'] = $this->Common_model_new->getsingle("cp_music_favorite",array("music_id" => $segment,"user_id" => $checkLogin['id']));

      $this->template->set('title', 'My music');
      $this->template->load('user_dashboard_layout_music', 'contents', 'detail_my_music',$data);
    }else{

     redirect('login'); 
    }

}

public function favorite_music()
{
	$music_id = $this->input->post("music_id");
	  $checkLogin = $this->session->userdata('logged_in');
	  $check_music = $this->Common_model_new->getsingle("cp_music_favorite",array("music_id" => $music_id,"user_id" => $checkLogin['id']));
	  if(!empty($check_music)){
		  $this->Common_model_new->delete("cp_music_favorite",array("music_id" => $music_id,"user_id" => $checkLogin['id']));
		  echo '0';
	  }else{
		  $array = array("user_id" => $checkLogin["id"],"music_id" => $music_id,"fav" => 1,"music_fav_created" => date("Y-m-d H:i:s"));
		  $this->Common_model_new->insertData("cp_music_favorite",$array);
		  echo '1';
	  }
}

 public function delete_mymusic()
  {
	   $segment = $this->uri->segment("2");
	   $checkLogin = $this->session->userdata('logged_in');
	   //$this->common_model_new->updateData("cp_articles",array("deleted_user_id" => $checkLogin['id']),array("article_id" => $segment));
	   $this->Common_model_new->insertData('cp_music_trash',array('user_id' => $checkLogin['id'],'music_id' => $segment,'music_trash_date' => date('Y-m-d H:i:s')));
	   
	    $this->session->set_flashdata('success', 'Successfuly deleted');
		redirect("mymusic_list");
  }

 
  
  
  
  
  
  public function movie_list()
  {
	   $data['menuactive'] = $this->uri->segment(1);
	  $checkLogin = $this->session->userdata('logged_in');
	 
	   
	    $this->template->set('title', 'All Movie');
        $this->template->load('user_dashboard_layout', 'contents', 'movie',$data);
	   }
	   
	public function movie_detail()
  {
	   $data['menuactive'] = $this->uri->segment(1);
	  $checkLogin = $this->session->userdata('logged_in');
	 
	   
	    $this->template->set('title', 'Movie Detail');
        $this->template->load('user_dashboard_layout', 'contents', 'movie_detail',$data);
	   }   
 
 public function favorite_music_detail()
 {
     $data['menuactive'] = $this->uri->segment(1);
	 $checkLogin = $this->session->userdata('logged_in');
	 if(!empty($checkLogin))
    {
		$segment = $this->uri->segment('2');
      $data['detail_music']  = $this->Common_model_new->getsingle("music",array("music_id" => $segment));
	  $data['check_music'] = $this->Common_model_new->getsingle("cp_music_favorite",array("music_id" => $segment,"user_id" => $checkLogin['id']));
		
	  $this->template->set('title', 'All Activities');
      $this->template->load('user_dashboard_layout_music', 'contents', 'detail_favorite_music',$data);
	}else{
		redirect('login'); 
	}
 } 
 
 public function search_music_list()
  {
	  $music_title = $this->input->post('music_title');
	  $data['page'] = 0;
	  $pagecon['per_page'] = 2;
	  $allmusic = $this->mymusic_model->getmymusicsearch($music_title,$pagecon['per_page'],$data['page']);
	  
	  $data['music_list'] = $allmusic['rows'];
	  $data['count_total'] = $allmusic['num_rows'];
		
	  $this->load->view("ajax_search_mymusic",$data);
	  
  }
  
  
}