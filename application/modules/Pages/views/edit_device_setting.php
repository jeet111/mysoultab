<style>

.addNote a, .btn.btn-primary {
    max-width: 142px !important;
}
.text_heading {
    border-bottom: none;
}
</style>
<div class="text_heading">
<?php if ($this->session->flashdata('success')) { ?>
	<div class="alert alert-success message" id="success">
		<button type="button" class="close" data-dismiss="alert">x</button>
<?php echo $this->session->flashdata('success'); ?>
	</div>
<?php } ?>

	<div id="msg_show"></div>

    <h3 class="text-center"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Edit Tablet Setting</h3>
	<?php if(!empty($btnsetingArray['device_id']) && $btnsetingArray['device_id'] !=''){ ?>
  	<div class="back_reminder">
		<button class="btn btn-primary" id="back_device_page" align="center">Device details</button>
	</div>
  <?php } ?>
	<div class="tooltip-2">
		<h2>Device Kiosk Mode 
		<?php $device_id = $btnsetingArray['device_id'];?>
						<a href="javascript:void(0)" onclick="update_status('<?php echo $device_id; ?>');">
						<?php if($btnsetingArray['device_kiosk_mode'] == 1) { ?>
						<i class="fa fa-toggle-on" aria-hidden="true"></i><?php
							}else{
							?>
						<i class="fa fa-toggle-off" aria-hidden="true"></i>
						<?php } ?>
            </h2>
	</div>

	</div>

<script src="<?php echo base_url(); ?>assets/js/jquery.min4.js"></script>
<script>
	$('#back_device_page').click(function() {
		var url = '<?php echo base_url()."devices" ?>';
		window.location.href = url;
		return false;
	});

	function update_status(device_id) {

	var device_id = $('input[name="device_id"]').val();
	var url = "<?php echo base_url().'Pages/devices/update_status'; ?>";

	$.ajax({
		type: 'POST',
		url: url,
		data: {device_id:device_id},
		
		success: function(resultData) {
			if (parseInt(resultData) === 1) {
				var kiosk_mode="Kiosk mode has been updated successfully!";
				setTimeout(function(){
					$("#msg_show").html("<div class='alert alert-success' align='center'>"+kiosk_mode+"</div>");
					location.reload();
				}, 1250);
			} else {
				alert("Some internal issue occured!");
			}
		},
		error: function (errorData) {
			console.log(errorData);
			//alert("Some internal issue occured!");
		}
	});
	}
</script>
