<style>
    .text_heading {
        border-bottom: 0;
    }
</style>
      
        <div class=" text_heading">
        <div id="msg_show"></div>
            <h3><i class='fas fa-atom' aria-hidden="true"></i> Spirituality</h3>
            <div class="tooltip-2">
            <h2>Display Spirituality button on Mobile App

<?php
 if(is_array($btnsetingArray)){

foreach ($btnsetingArray as $res) {
  if ($res->settings == 1) { ?>
    <label class="switch ">
      <input type="checkbox" checked data-btn="<?php echo 'Sprituality' ?>" class="updateStatus">
      <span class="slider round"></span>
    </label>

<?php } else { ?>
<label class="switch space">
<input type="checkbox" data-btn="<?php echo 'Sprituality' ?>" class="updateStatus">
<span class="slider round"></span>
</label>


<?php } ?>
<?php }
 }
?>
</h2>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script type="text/javascript">
    $(document).on('click', '.updateStatus', function() {
        var update_status = $(this).attr('data-btn');
        var update_st = $(this).attr('data-st');
        //alert(update_status+'-'+update_st);
        $.ajax({
            url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
            type: "POST",
            data: {
                'btn': update_status
            },
            dataType: "json",
            success: function(data) {
                    $("#msg_show").html("<div class='alert alert-success'>" + data.status + "</div>");
                 
            }
        });
    });
</script>