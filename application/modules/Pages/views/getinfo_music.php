<aside class="right-side new-music mus_list">
   	<div class="searchbar-wrap">
		<!--<input autocomplete="off" class="form-control" id="" name="" placeholder="Search Music" type="text"> -->
		<a href="<?php echo base_url(); ?>add_music" class="add_music_btn" ><i class="fa fa-music" aria-hidden="true"></i>Add Music</a>
	</div>
	<section class="content-header no-margin">
		</section>
		<section class="doc_cate">
			<div class="row">
			
			<div class="col-md-9">
			
				<div class="bx_leftScrollmain mCustomScrollbar">
				<div id="msgs"></div>
				<div class="bx_leftScroll get_main">
					<div class="bx_getInfo">
						<div class="inside-pageinfo-wrap">
							<div class="inside-pageinfo-lhs">
								<div class="image-cont colimg ">
									<img id="" src="<?php echo base_url(); ?>uploads/music/image/<?php echo $getinfo_music->music_image;  ?>" alt="" class="mCS_img_loaded">											
										<!--<span class="icon_play"><i class="fa fa-play-circle"></i></span>-->
								</div>
							</div>
							<div class="inside-pageinfo-rhs">
							
							<ul class="info_like">
                <li id="re"><a href="javascript:void(0);" id="favourite_info_music" data-segment="<?php echo base64_decode($this->uri->segment('2')); ?>"><?php if($check_info_music){ ?><i class="fa fa-heart remove" aria-hidden="true"></i><?php }else{ ?><i class="fa fa-heart-o remove"></i><?php } ?></a></li>
				<li style="display:none" id="renew"><a href="javascript:void(0);" id="favourite_info_music" data-segment="<?php echo base64_decode($this->uri->segment('2')); ?>"><i class="fa fa-heart" aria-hidden="true"></i></a></li>
				<li style="display:none" id="renew2"><a href="javascript:void(0);" id="favourite_info_music" data-segment="<?php echo base64_decode($this->uri->segment('2')); ?>"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                
              </ul>
								<div class="pageinfo-top">
									<div class="pageinfo-top-lhs">
										<h1><?php echo $getinfo_music->music_title; ?></h1>
										<div class="myplaylist_totalsong"><?php echo $getinfo_music->music_artist; ?></div>
										
									</div>
									<div class="pageinfo-top-rhs">
										<span class="icon-but-overflow"></span>
									</div>
								</div>
								<div class="pageinfo-bottom">
									<button onclick="playAudio()" class="test1" id="play_now_<?php echo $getinfo_music->music_id; ?>" data-music="<?php echo $getinfo_music->music_id; ?>"><i class="fa fa-play"></i> Play</button>
									
									<button style="display:none" onclick="pauseAudio()" class="test2" id="play_now_<?php echo $getinfo_music->music_id; ?>" data-music="<?php echo $getinfo_music->music_id; ?>"><i class="fa fa-play"></i> Play</button>

									<button id="download_m_<?php echo $getinfo_music->music_id; ?>" data-download-music="<?php echo $getinfo_music->music_id; ?>" class="white-button"><i class="fa fa-download"></i> Download</button>
									
									<div class="back-info"><a href="javascript:window.history.back()"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</a></div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="cont-wrap">
						<ul class="getinfolist">
							<li>
								<p>Album :</p>
								<p><?php echo $getinfo_music->music_title; ?></p>
							</li>
							<li>
								<p>Singer :</p>
								<p>
									<?php echo $getinfo_music->music_artist; ?>									
								</p>
							</li>
							<li >
								<p>Description :</p>
								<p><!---->
									<a title="" href="#"><?php echo $getinfo_music->music_desc; ?></a>							
								</p>
							</li>
							
						</ul>
					</div>
					<div class="bx_related">
						<h3>You may also like</h3>
						<ul class="list_sld">
  			    	<?php 
					foreach($like_music as $music_li){ 
					if($music_li->music_id != $music_id){
					?>
					    	<li>
					    		<div class="rail-thumb">
					    			<a class="dropdown-item" href="javascript:void(0);" id="play_now2q_<?php echo $music_li->music_id; ?>" data-music="<?php echo $music_li->music_id; ?>"><img alt="" title="" src="<?php echo base_url(); ?>uploads/music/image/<?php echo $music_li->music_image; ?>" class="mCS_img_loaded"></a>
					    			<span class="playbutton">
									<a class="dropdown-item" href="javascript:void(0);" id="play_nowcirle_<?php echo $music_li->music_id; ?>" data-music="<?php echo $music_li->music_id; ?>">
									<i class="fa fa-play-circle"></i></a>
									</span>			
					    		</div>
					    		
                         <div class="rail-content" title="">
			    			<a href="<?php echo base_url(); ?>getinfo_music/<?php echo base64_encode($music_li->music_id); ?>"><?php echo $music_li->music_title; ?></a>
		    				<div class="btn-group new-music-btn">
									<a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
								  <div class="dropdown-menu">	
									<a class="dropdown-item" href="javascript:void(0);" id="play_now2_<?php echo $music_li->music_id; ?>" data-music="<?php echo $music_li->music_id; ?>">Play Now</a>
									<a class="dropdown-item" href="<?php echo base_url(); ?>Pages/Music/download_music/<?php echo $music_li->music_id; ?>" id="like_download_<?php echo $music_li->music_id; ?>" data-music-like="<?php echo $music_li->music_id; ?>">Download</a>
									
									<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_music/<?php echo base64_encode($music_li->music_id); ?>">Get info</a>
								  </div>
							</div>			    			
			    		</div>
					    	</li>
					<?php }} ?>
					    						    	
					    </ul>
					</div>
				</div>
			</div>
		</div>
	


	<div class="col-md-3 mrg_non">
				<div class="bx_right" id="upload_music">
<?php if(!empty($playlist_music)){ ?>					
					<div class="queue-header">
						<div class="hd_que">
							<h5 class="pull-left">Queue</h5>
						</div>
						<div class="main_chk">
							<div class="bx_chkAll">
								<span class="select-input">
									<input class="chk_bx" id="checkall" type="checkbox">
									<span> Select All</span>
								</span>
							</div>
							<div class="bx_rgt">
								
								<a href="javascript:void(0);" class="clear-queue-popover btn_delete" data-id="" id="btn_delete"> Remove</a>
							</div>
						</div>
					</div>


					<div class="listing_sng mCustomScrollbar">
						<ul class="list_songs playlist1" id="playlist">
							<?php
                         if(!empty($playlist_music)){
							foreach($playlist_music as $playlist){ ?>
							<li class="active test_play">
								<div class="select-input_one"><input type="checkbox" name="sub_chk[]" value="<?php echo $playlist->music_playlist_id; ?>" data-id="<?php echo $playlist->music_playlist_id; ?>" class="sub_chk" ></div>
								<a href="<?php echo base_url(); ?>uploads/music/<?php echo $playlist->music_file; ?>"><div class="queue-songlist-lhs">
									<div class="queue-songlist-img">
										<div class="song-current">
											<img class="click_image_change_<?php echo $playlist->music_id; ?>" data-image-change="<?php echo $playlist->music_id; ?>" alt="" src="<?php echo base_url(); ?>uploads/music/image/<?php echo $playlist->music_image; ?>">
										</div>
									</div>
									<div class="queue-songlist-info">
										<p class="click_image_change_<?php echo $playlist->music_id; ?>" data-image-change="<?php echo $playlist->music_id; ?>"><?php echo $playlist->music_title; ?></p>
										<span class="click_image_change_<?php echo $playlist->music_id; ?>" data-image-change="<?php echo $playlist->music_id; ?>"><?php echo $playlist->music_artist; ?></span>
									</div>
								</div></a>
								<div class="queue-songlist-rhs queue-play-list">
									<div class="btn-group new-music-btn">
										<button class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
										<div class="dropdown-menu">
											
											<button class="dropdown-item" id="remove_play_now_<?php echo $playlist->music_playlist_id; ?>" data-music-remove="<?php echo $playlist->music_playlist_id; ?>">Remove</button>
											
											<button class="dropdown-item" id="testi_<?php echo $playlist->music_id; ?>" data-music-id="<?php echo $playlist->music_id; ?>" >Download</button>
											
											<button class="dropdown-item" id="getinfo_m_<?php echo $playlist->music_id ?>" data-getinfo="<?php echo base64_encode($playlist->music_id); ?>" >Get info</button>
											
										</div>
									</div>
								</div>
							</li>
							
						<?php } ?>

						 <?php }else{ ?>	
						<div class="music_norecord">No record found</div> 
						 <?php } ?>
						</ul>
					</div>


					<div class="bx_mus">
						<div class="ply_sng">
							<div class="sng-crnt">
								<?php if($single_playlist_music->music_image){ ?>
								<img alt="" src="<?php echo base_url(); ?>uploads/music/image/<?php echo $single_playlist_music->music_image; ?>">
							<?php }else{ ?>
							<img src="<?php echo base_url().'uploads/music/defualt_music.png' ?>">
							<?php } ?>
							</div>
							<div class="sng-txt">
								<h4><?php echo $single_playlist_music->music_title; ?></h4>
								<p><?php echo $single_playlist_music->music_artist; ?></p>
							</div>
						</div>

						<audio id="audio" class="player_audio" preload="auto" tabindex="0" controls="" type="audio/mpeg">
        <source type="audio/mp3" id="audiotest" src="<?php echo base_url(); ?>uploads/music/<?php echo $single_playlist_music->music_file; ?>">
        Sorry, your browser does not support HTML5 audio.
    </audio>
						
					</div>
                <?php }else{ ?>
		     <div class="play-list-empty">
		    	<span class="blnk_queue"><i class="fa fa-music" aria-hidden="true"></i></span>
		    	<h4>Your queue is empty</h4>
		    </div>
		   <?php } ?>
				</div>
			</div>
			</div>

	
		</section>









     </aside>



  

     <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->

<!-- 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->
<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightslider.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightslider.js"></script> 
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.css">
<script src="<?php echo base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script>
<?php if(!empty($playlist_music)){ ?>
var audio;
var playlist;
var tracks;
var current;

init();
function init(){
    current = 0;
    audio = $('audio');
    playlist = $('.playlist1');
    tracks = playlist.find('li a');
    len = tracks.length - 1;
    audio[0].volume = .10;
    audio[0].play();
    playlist.find('a').click(function(e){ 
        e.preventDefault();
        link = $(this);
        current = link.parent().index();
        run(link, audio[0]);
    });
    audio[0].addEventListener('ended',function(e){
       
        if(current == len){
            current = 0;
            link = playlist.find('a')[0];
        }else{
			 current++;
            link = playlist.find('a')[current];    
        }
        run($(link),audio[0]);
    });
}

function run(link, player){
        player.src = link.attr('href');
		
        par = link.parent();
        par.addClass('active').siblings().removeClass('active');
        audio[0].load();
        audio[0].play();
}
<?php } ?>

//jQuery('#checkall').on('click', function(e) {
	  $(document).on('click','#checkall',function(){
        if($(this).is(':checked',true))  
        { 
            $(".sub_chk").prop('checked', true); 
        }  
        else  
        { 
            $(".sub_chk").prop('checked',false);  
        }  
    });
   //jQuery('.sub_chk').on('click',function(){
	  $(document).on('click','#checkall',function(){   
        if($('.sub_chk:checked').length == $('.sub_chk').length){
            $('#checkall').prop('checked',true);
        }else{
            $('#checkall').prop('checked',false);
        }
    });
	
	
	$(document).on('click','.btn_delete', function(e){
		
		var bodytype = $('input[name="sub_chk[]"]:checked').map(function () {  
        return this.value;
        }).get().join(",");
		if(bodytype != ''){
	if(confirm("Are you sure you want to remove this audio?")){	
		$.ajax({
                url: '<?php echo base_url(); ?>Pages/Music/delete_checkboxes',
                type: 'POST',
                data: {bodytype:bodytype},
				dataType: "json",
                success: function(data) { //alert('swati');
                    
					
					 setTimeout(function(){
            $("#expire_msg").html("<div class='alert alert-success'>Successfully deleted !</div>");
			
			$("#upload_music").load(location.href + " #upload_music");
            //location.reload();
//window.location.href = email_url+"email_list";
}, 1000);
                }
            });
			
			}
		}else{
			alert("Please select atleast one audio.");
		}
		
		
  });

$(document).on('click','[id^="play_now_"]',function(){ 

var music_id = $(this).attr("data-music");
$.ajax({
    url:"<?php echo base_url().'play_music'?>",
    method:"POST",
    data:{music_id:music_id},
    success:function(data)
    {
       $("#upload_music").show();
        $("#upload_music").html(data);
    }
   })

 });  

$(document).on('click','[id^="play_now2_"]',function(){ 

var music_id = $(this).attr("data-music");
$.ajax({
    url:"<?php echo base_url().'play_music'?>",
    method:"POST",
    data:{music_id:music_id},
    success:function(data)
    {
       $("#upload_music").show();
        $("#upload_music").html(data);
    }
   })

 }); 
 
 
 $(document).on('click','[id^="play_now2q_"]',function(){ 

var music_id = $(this).attr("data-music");
$.ajax({
    url:"<?php echo base_url().'play_music'?>",
    method:"POST",
    data:{music_id:music_id},
    success:function(data)
    {
       $("#upload_music").show();
        $("#upload_music").html(data);
    }
   })

 }); 
 
 
  $(document).on('click','[id^="play_nowcirle_"]',function(){ 

var music_id = $(this).attr("data-music");
$.ajax({
    url:"<?php echo base_url().'play_music'?>",
    method:"POST",
    data:{music_id:music_id},
    success:function(data)
    {
       $("#upload_music").show();
        $("#upload_music").html(data);
    }
   })

 });
 
 $(document).on('click','#favourite_info_music',function(){
	   var music_id = $(this).attr('data-segment');
	   $.ajax({
            type:'POST',
            url: "<?php echo base_url().'Pages/Music/favourite_music'; ?>",
            data: 'music_id=' + music_id,            
            success:function(data){	
			$("#re").hide();
               if(data == '0'){
                  $("#renew").hide();		   
                  $("#renew2").show();
				$("#msgs").html('<div class="alert alert-success">'+'Successfully unfavourite'+'</div>');
                setTimeout(function(){
                      $("#msgs").html("");
                     }, 1500);  
				  
			   }else{
				   $("#msgs").html('<div class="alert alert-success">'+'Successfully favourite'+'</div>');
                setTimeout(function(){
                      $("#msgs").html("");
                     }, 1500);  
		          $("#renew2").hide();		   
                  $("#renew").show();
			   }			   
				
            }
        });
	}); 
 
 
$(document).on('click','[id^="remove_play_now_"]',function(){
	
 var music_id = $(this).attr("data-music-remove");
 
 if(confirm("Are you sure you want to remove this audio?")){
        
$.ajax({
    url:"<?php echo base_url().'Pages/Music/remove_play_music'?>",
    method:"POST",
    data:{music_id:music_id},
    success:function(data)
    {
	  /*if(data == 2){
		   $("#upload_music").html("<div class='play-list-empty'>Your Queue is empty</div>");
		 
	   }*/
	    setTimeout(function(){
            $("#expire_msg").html("<div class='alert alert-success'>Successfully deleted !</div>");
			
			$("#upload_music").load(location.href + " #upload_music");
            
}, 1000);
    }
	
   })
   
   
}

});

 
 $(document).on('click','[class^="click_image_change_"]',function(){
   	  var music_id = $(this).attr("data-image-change");
   
$.ajax({
    url:"<?php echo base_url().'Pages/Music/image_change'?>",
    method:"POST",
    data:{music_id:music_id},
    success:function(data)
    { 
	   $('.ply_sng').html(data);
    }
	
   })
 });
</script>

<script>
		(function($){
			$(window).on("load",function(){
				
				$("a[rel='load-content']").click(function(e){
					e.preventDefault();
					var url=$(this).attr("href");
					$.get(url,function(data){
						$(".content .mCSB_container").append(data); //load new content inside .mCSB_container
						//scroll-to appended content 
						$(".content").mCustomScrollbar("scrollTo","h2:last");
					});
				});
				
				$(".content").delegate("a[href='top']","click",function(e){
					e.preventDefault();
					$(".content").mCustomScrollbar("scrollTo",$(this).attr("href"));
				});
			});
		})(jQuery);
		
		
$(document).on('click','[id^="download_music_"]',function(){
	 var music_id = $(this).attr("data-music-id");
window.location = "<?php echo base_url(); ?>Pages/Music/download_music/"+music_id;

 });	



$(document).on('click','[id^="download_m_"]',function(){
	 var music_id = $(this).attr("data-download-music");
window.location = "<?php echo base_url(); ?>Pages/Music/download_music/"+music_id;

 });


$(document).on('click','[id^="like_download_"]',function(){
	 var music_id = $(this).attr("data-music-like");
window.location = "<?php echo base_url(); ?>Pages/Music/download_music/"+music_id;

 });
	
$(document).on('click','[id^="getinfo_m_"]',function(){
	 var music_id = $(this).attr("data-getinfo");
window.location = "<?php echo base_url(); ?>getinfo_music/"+music_id;

 });
 
 	$(document).on('click','[id^="testi_"]',function(){ 
	 var music_id = $(this).attr("data-music-id");
window.location = "<?php echo base_url(); ?>Pages/Music/download_music/"+music_id;
	});
	
	
	
	var x = document.getElementById("audio"); 

function playAudio() { 
 $(".test1").hide();
  $(".test2").show();
  x.play(); 
 
} 

function pauseAudio() { 
$(".test2").hide();
  $(".test1").show();
  x.pause(); 
   
} 
</script>

<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>
