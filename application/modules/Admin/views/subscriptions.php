<div class="container-fluid">

	<div class="dash-counter">

		<div class="Schedule_main_one">

			<div class="users-main">

				<?php echo "<h2>Subscriptions</h2>"; ?>



				<div class="btn_topBack">

					<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">

						Back

					</a>

				</div>



				<?php

				if (!empty($activities_msg)) {

					?>

					<div class="col-md-12">

						<div class="alert alert-success alert-dismissible">

						  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

						  <strong>Success!</strong> <?php echo $activities_msg; ?>

						</div>

					</div>

					<?php

				}

				?>

				



				<form class="form-horizontal" action="" method="post" enctype="multipart/form-data" name="AddUsers" id="subscriptionForm">

					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for=""> Email:<span class="field_req">*</span></label>

							<div class="col-md-8">

								<select class="form-control" name="emails[]" multiple id="emails">

									<?php

									if (!empty($subscription_list)) 

									{

										foreach ($subscription_list as $key => $value) 

										{

											?>

											<option value="<?php echo $value->email; ?>">

												<?php echo $value->email; ?>

											</option>

											<?php

										}

									}

									?>

								</select>

								<?php echo form_error('emails'); ?>

							</div>

						</div>

					</div>

					

					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for=""> Subject:<span class="field_req">*</span></label>

							<div class="col-md-8">

								<input type="text" class="form-control" placeholder="Subject" name="subject" value="<?php echo set_value('subject'); ?>" id="subject">

								<?php echo form_error('subject'); ?>

							</div>

						</div>

					</div>



					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for=""> Title:<span class="field_req">*</span></label>

							<div class="col-md-8">

								<input type="text" class="form-control" placeholder="Title" name="title" value="<?php echo set_value('title'); ?>" id="title">

								<?php echo form_error('title'); ?>

							</div>

						</div>

					</div>	



					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for=""> Message:<span class="field_req">*</span></label>

							<div class="col-md-8">

								<textarea name="message" placeholder="Message" class="form-control"><?php echo set_value('message'); ?></textarea>

								<?php echo form_error('message'); ?>

							</div>

						</div>

					</div>



					<?php

					if (isset($staff_permission[16]['add_permission']) == 1 || isset($staff_permission[16]['edit_permission']) == 1) {

						?>

						<div class="col-md-8">

							<div class="bnt_sub">

								<input type="submit" class="btn btn-default up_but" class="form-control" name="btnSend" value="Submit">

								<!-- <a class="cancel-btn btn" href="javascript:window.history.back()">

		                     Back

		                  </a> -->

			            </div>

			         </div>

						<?php

					} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

						?>

						<div class="col-md-8">

							<div class="bnt_sub">

								<input type="submit" class="btn btn-default up_but" class="form-control" name="btnSend" value="Submit">

								<!-- <a class="cancel-btn btn" href="javascript:window.history.back()">

		                     Back

		                  </a> -->

			            </div>

			         </div>

						<?php

					}

					?>

					

        </div>

    </div>

</form>

</div>

</div>

</div>

</div>

<!--Added by 95 on 5-2-2019 For Doctor category for validation-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>

<script type="text/javascript">

	$(document).ready(function(){

		$('#subscriptionForm').validate({

			rules: {

				"emails[]":{

					required: true

				},

				subject:{

					required: true

				},

				title:{

					required: true

				},

				message:{

					required: true

				}

			},

			messages: {

				"emails[]":{

					required: 'Please select email.'

				},

				subject:{

					required: 'Please enter subject.'

				},

				title:{

					required: 'Please enter title.'

				},

				message:{

					required: 'Please enter message.'

				}

			}

		});

	});

</script>