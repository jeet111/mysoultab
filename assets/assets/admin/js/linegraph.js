$(document).ready(function(){
	var reurl = $('.baseurl').val();
	$.ajax({
		url : reurl+"Admin/Admin_role/analytics_chart",
		type : "GET",
		dataType:'json',
		success : function(data){
			//console.log(data);

			var total = [];
			var id = [];
			var userid = [];
			var amount = [];
			for(var i in data) {
				//total.push("Date: " + data[i].date);
				total.push(data[i].date);
				id.push(data[i].id);
				userid.push(data[i].userid);
				amount.push(data[i].amount);
			}

			var chartdata = {
				labels: total,
				datasets: [
					/*{
						label: "date",
						fill: false,
						//lineTension: 0.1,
						backgroundColor: "rgba(59, 89, 152, 0.75)",
						borderColor: "rgba(59, 89, 152, 1)",
						pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
						pointHoverBorderColor: "rgba(59, 89, 152, 1)",
						data: date
					},*/
					{
						label: "users",
						fill: false,
						//lineTension: 0.1,
						backgroundColor: "rgba(29, 202, 255, 0.75)",
						borderColor: "rgba(29, 202, 255, 1)",
						pointHoverBackgroundColor: "rgba(29, 202, 255, 1)",
						pointHoverBorderColor: "rgba(29, 202, 255, 1)",
						data: userid
					},
					{
						label: "amount",
						fill: false,
						//lineTension: 0.1,
						backgroundColor: "rgba(211, 72, 54, 0.75)",
						borderColor: "rgba(211, 72, 54, 1)",
						pointHoverBackgroundColor: "rgba(211, 72, 54, 1)",
						pointHoverBorderColor: "rgba(211, 72, 54, 1)",
						data: amount
					}
				]
			};

			var ctx = $("#mycanvas");

			var LineGraph = new Chart(ctx, {
				type: 'line',
				data: chartdata
			});
		},
		error : function(data) {

		}
	});
});