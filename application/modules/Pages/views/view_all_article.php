

<aside class="right-side new-music artic_list newarticlelist">

	<div class="searchbar-wrap">

		<div class="src_sbmit">



			<form name="article_frm" id="article_frm">

				<input autocomplete="off" class="form-control" id="article_search" name="article_search" placeholder="Search Article" value="" type="text">

				<button type="submit" id="btn_music" value="Submit"><i class="fa fa-search"></i></button>				



			</form>

		</div>	

		

	</div>



	<?php $type=''; ?>
	<section class="content-header no-margin">



		<?php if($type=='popular'){ ?>



			<h1 class="text-center"><i class="fa fa-list"></i> All Popular Articles <div class="rem_add">

			<?php }elseif($type=='recent'){ ?>

				<h1 class="text-center"><i class="fa fa-list"></i> All Latest Articles <div class="rem_add">

				<?php }elseif($type=='my'){ ?>

					<h1 class="text-center"><i class="fa fa-list"></i> All My Articles <div class="rem_add">

					<?php }elseif($type=='favorite'){ ?>

						<h1 class="text-center"><i class="fa fa-list"></i> All Favorite Articles <div class="rem_add">

						<?php }else{ ?>

							<h1 class="text-center">

								<!-- <i class="fa fa-file-image-o" aria-hidden="true"></i> -->

								<i class="fa fa-list" aria-hidden="true"></i>

								All Article Categories <div class="rem_add">

								<?php } ?>



								<a href="<?php echo base_url();?>add_article" class="btn btn-primary">Add</a>



								<!-- <a href="javascript:window.history.back()" class="btn btn-primary">Back</a> -->



							</div></h1>

						</section>


						<section class="content weather-bg news_section">
							<!-- setting start -->
							<div class="col-md-12">

								<div id="msg_show"></div>

								<div class="col-md-10">
									<h3>All Article Categories</h3>

									<?php $current_user_id = $_SESSION['logged_in']['id'];
									if (is_array($btnsetingArray) || is_object($btnsetingArray)){
         foreach($btnsetingArray as $res){
          $userid = $res->user_id;
          if($current_user_id == $userid){
            if($res->settings == 1) { ?>
											<div class="tooltip-2"><a data-btn="<?php echo 'all_article_categories'?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
												<span class="tooltiptext">Active</span> 
											</div>
										<?php }else { ?>
											<div class="tooltip-2 "><a data-btn="<?php echo 'all_article_categories'?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
												<span class="tooltiptext">Deactive</span>
											</div>
										<?php } ?>
									<?php }
									} 
								}?>
								</div>
							</section>


							<?php /* ?>

							<section class="content weather-bg news_section">

								<!-- setting start -->
								<div class="col-md-12">
									<div class="col-md-10">
										<h3>All Article Categories</h3>
										<?php if(!empty($btnsetingArray)){ ?>
											<?php if($btnsetingArray[0]->settings=='1'){ ?>

												<label><input type="radio" name="btnshow" value='1' checked="checked">Now All Article Categories showing on the mysoultab APP<br></label>

											<?php }else{ ?>

												<label> <input type="radio" name="btnshow" value='1' checked="checked">Now All Article Categories not showing on the mysoultab APP </label>

											<?php } ?>

										<?php } ?> 
									</div>
									<div class="col-md-2 delete-email">
										<button class="btn btn-primary chk">Settings</button>
									</div> 
									<div id="para" style="display: none "> 



										<form id="submit_form" method="post" action="<?php echo base_url()?>savsettings"> 
											<div class="col-md-10">
												<label> <input type="checkbox" name="butn_seting" id="butn_seting" value="">Please check if want to show the All Article Categories button in mysoultab App </label>
												<input type="hidden" name="btn" value="all_article_categories">
											</div>
											<div class="col-md-2">
												<input class="btn btn-primary chkdd" type="submit" name="submit" value="submit" >
											</div>
										</form>
										<div id="response"></div>
									</div>

									<!-- </div> -->

									<!-- End -->
								</section>

								<?php */ ?>


								<?php if($type=='popular'){ ?>   





									<section class="doc_cate">

										<div class="row">

											<div class="col-md-12">

												<div class="bx_leftScrollmain mCustomScrollbar">

													<div class="bx_leftScroll">

														<div id="combine_tab">						    

															<div class="box_viewFull">

																<div class="row">



																	<?php foreach ($popular_array as $popular) { ?>



																		<div class="col-sm-6 col-md-4">	    	

																			<div class="rail-thumb">

																				<a class="dropdown-item" href="<?php echo base_url();?>read_article/<?php echo base64_encode($popular->article_id); ?>/<?php echo $this->uri->segment(3); ?>">



																					<img src="<?php echo base_url().'uploads/articles/'.$popular->article_image;?>" class="mCS_img_loaded">





																				</a>

																			</div>

																			<div class="rail-content" title="">

																				<a href="<?php echo base_url();?>read_article/<?php echo base64_encode($popular->article_id); ?>/<?php echo $this->uri->segment(3); ?>"><?php echo $popular->article_title; ?></a>						    				

																			</div>					                

																		</div>



																	<?php } ?>



																</div>

															</div>

														</div>						

													</div>

												</div>

											</div>					

										</div>	

									</section>











								<?php }elseif($type=='recent'){ ?>



									<section class="doc_cate">

										<div class="row">

											<div class="col-md-12">

												<div class="bx_leftScrollmain mCustomScrollbar">

													<div class="bx_leftScroll">

														<div id="combine_tab">						    

															<div class="box_viewFull">

																<div class="row">



																	<?php foreach ($recent_array as $recent) { ?>



																		<div class="col-sm-6 col-md-4">	    	

																			<div class="rail-thumb">

																				<a class="dropdown-item" href="<?php echo base_url();?>read_article/<?php echo base64_encode($recent->article_id); ?>/<?php echo $this->uri->segment(3); ?>">



																					<img src="<?php echo base_url().'uploads/articles/'.$recent->article_image;?>" class="mCS_img_loaded">





																				</a>

																			</div>

																			<div class="rail-content" title="">

																				<a href="<?php echo base_url();?>read_article/<?php echo base64_encode($recent->article_id); ?>/<?php echo $this->uri->segment(3); ?>"><?php echo $recent->article_title; ?></a>						    				

																			</div>					                

																		</div>



																	<?php } ?>



																</div>

															</div>

														</div>						

													</div>

												</div>

											</div>					

										</div>	

									</section>



								<?php }elseif($type=='my'){ ?>



									<section class="doc_cate">

										<div class="row">

											<div class="col-md-12">

												<div class="bx_leftScrollmain mCustomScrollbar">

													<div class="bx_leftScroll">

														<div id="combine_tab">						    

															<div class="box_viewFull">

																<div class="row">



																	<?php foreach ($UserArticleData as $my) { ?>



																		<div class="col-sm-6 col-md-4">	    	

																			<div class="rail-thumb">

																				<a class="dropdown-item" href="<?php echo base_url();?>read_article/<?php echo base64_encode($my->article_id); ?>/<?php echo $this->uri->segment(3); ?>">



																					<img src="<?php echo base_url().'uploads/articles/'.$my->article_image;?>" class="mCS_img_loaded">





																				</a>

																			</div>

																			<div class="rail-content" title="">

																				<a href="<?php echo base_url();?>read_article/<?php echo base64_encode($my->article_id); ?>/<?php echo $this->uri->segment(3); ?>"><?php echo $my->article_title; ?></a>						    				

																			</div>					                

																		</div>



																	<?php } ?>



																</div>

															</div>

														</div>						

													</div>

												</div>

											</div>					

										</div>	

									</section>





								<?php }elseif($type=='favorite'){ ?>



									<section class="doc_cate">

										<div class="row">

											<div class="col-md-12">

												<div class="bx_leftScrollmain mCustomScrollbar">

													<div class="bx_leftScroll">

														<div id="combine_tab">						    

															<div class="box_viewFull">

																<div class="row">



																	<?php foreach ($UserFavArticl as $Favourite) { ?>



																		<div class="col-sm-6 col-md-4">	    	

																			<div class="rail-thumb">

																				<a class="dropdown-item" href="<?php echo base_url();?>read_article/<?php echo base64_encode($Favourite->article_id); ?>/<?php echo $this->uri->segment(3); ?>">



																					<img src="<?php echo base_url().'uploads/articles/'.$Favourite->article_image;?>" class="mCS_img_loaded">





																				</a>

																			</div>

																			<div class="rail-content" title="">

																				<a href="<?php echo base_url();?>read_article/<?php echo base64_encode($Favourite->article_id); ?>/<?php echo $this->uri->segment(3); ?>"><?php echo $Favourite->article_title; ?></a>						    				

																			</div>					                

																		</div>



																	<?php } ?>



																</div>

															</div>

														</div>						

													</div>

												</div>

											</div>					

										</div>	

									</section>



								<?php }else{ ?>



									<section class="doc_cate">

										<div class="row">

											<div class="col-md-12">

												<div class="bx_leftScrollmain mCustomScrollbar">

													<div class="bx_leftScroll">

														<div id="combine_tab">						    

															<div class="box_viewFull">

																<div class="row">


																	<?php if(!empty($category_array)){ ?>
																		<?php foreach ($category_array as $category) { ?>



																			<div class="col-sm-6 col-md-4">	    	

																				<div class="rail-thumb">

																					<a class="dropdown-item" href="<?php echo base_url();?>category_articles/<?php echo base64_encode($category->article_category_id); ?>">



																						<img src="<?php echo base_url().'uploads/articles/category_icon/'.$category->article_category_icon;?>" class="mCS_img_loaded">





																					</a>

																				</div>

																				<div class="rail-content" title="">

																					<a href="<?php echo base_url();?>category_articles/<?php echo base64_encode($category->article_category_id); ?>">

																						<?php echo $category->article_category_name; ?>



																					</a>						    				

																				</div>					                

																			</div>



																		<?php } ?>

																	<?php } ?>


																</div>

															</div>

														</div>						

													</div>

												</div>

											</div>					

										</div>	

									</section>



								<?php } ?>

							</aside>		





							<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->

							<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->



							<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightslider.css"/>

							<script src="<?php echo base_url(); ?>assets/js/lightslider.js"></script> 

							<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.css">

							<script src="<?php echo base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>



							<script>	

								$(document).ready(function() {

									$(".content-slider").lightSlider({

										loop:true,

										keyPress:true

									});

									$('#image-gallery').lightSlider({

										gallery:true,

										item:1,

										thumbItem:9,

										slideMargin: 0,    

										speed:500,

										auto:true,

										loop:true,

										onSliderLoad: function() {

											$('#image-gallery').removeClass('cS-hidden');

										}  

									});

								});





								$(document).on('click','#btn_music',function(e){ 

									var article_search = $("#article_search").val();

									window.location = "<?php echo base_url(); ?>articles/"+article_search;	

								});





								$('form').submit(function(){

									return false;

								});

// javascript

document.getElementById('article_frm').onsubmit = function() {

	return false;

}

$('#article_search').keypress(function(e) {

	if (e.keyCode == $.ui.keyCode.ENTER) {

		e.preventDefault();

		var article_search = $("#article_search").val();





		window.location = "<?php echo base_url(); ?>articles/"+article_search;



	}

});





</script>

<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).on('click','.updateStatus',function(){
		var update_status = $(this).attr('data-btn');
		var update_st = $(this).attr('data-st');
    //alert(update_status+'-'+update_st);
    $.ajax({
    	url: '<?php echo base_url();?>Pages/pages/change_button_status',
    	type: "POST",
    	data: {'btn':update_status},
    	dataType: "json",
    	success:function(data){
    		setTimeout(function(){
    			$("#msg_show").html("<div class='alert alert-success'>"+data.status+"</div>");
    			location.reload();

    		}, 1500);
    	}
    }); 
});
</script>