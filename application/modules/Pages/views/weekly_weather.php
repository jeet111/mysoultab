<aside class="right-side weather-box">

     <section class="content-header no-margin">

          <h1 class="text-center"><i class="fa fa-cloud" aria-hidden="true"></i> Weekly Weather <div class="rem_add"><a href="<?php echo base_url();?>dashboard" class="btn btn-primary">Back</a></div></h1>

     </section>
     <section>

          <section class="content weather-bg news_section">
              <!-- setting start -->
              <div class="col-md-12">

                <div id="msg_show"></div>

                <div class="col-md-10">
                  <h3>Weekly Weather
                  </h3>

                  <?php 
         foreach($btnsetingArray as $res){
            if($res->settings == 1) { ?>
                      <div class="tooltip-2"><a data-btn="<?php echo 'weather'?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                        <span class="tooltiptext">Active</span>
                   </div>
              <?php }else { ?>
                 <div class="tooltip-2 "><a data-btn="<?php echo 'weather'?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
                   <span class="tooltiptext">Deactive</span>
              </div>
         <?php } ?>
    <?php }
     ?>
</div>
</section>


          <?php /* ?>
       <section class="content weather-bg news_section">
          <!-- setting start -->
          <div class="col-md-12">
               <div class="col-md-10">
                    <h3>News</h3>
                    <?php if(!empty($btnsetingArray)){ ?>
                         <?php if($btnsetingArray[0]->settings=='1'){ ?>

                              <label><input type="radio" name="btnshow" value='1' checked="checked">Now social app showing on the mysoultab APP<br></label>

                         <?php }else{ ?>

                              <label> <input type="radio" name="btnshow" value='1' checked="checked">Now social app not showing on the mysoultab APP </label>

                         <?php } ?>

                    <?php } ?> 
               </div>
               <div class="col-md-2 delete-email">
                    <button class="btn btn-primary chk">Settings</button>
               </div> 
               <div id="para" style="display: none "> 



                    <form id="submit_form" method="post" action="<?php echo base_url()?>savsettings "> 
                         <div class="col-md-10">
                              <label> <input type="checkbox" name="butn_seting" id="butn_seting" value="">Please check if want to show the social button in mysoultab App </label>
                              <input type="hidden" name="btn" value="weather">
                         </div>
                         <div class="col-md-2">
                              <input class="btn btn-primary chkdd" type="submit" name="submit" value="submit" >
                         </div>
                    </form>
                    <div id="response"></div>
               </div>

               <!-- </div> -->

               <!-- End -->
          </section>

          <?php */ ?>

          <section class="content weather-bg">



               <?php

          //$dayOfWeek=array();
               $previous_date='';
               foreach ($weather['list'] as $key) {

                    $dt_txt = $key['dt_txt'];

                    $date = new DateTime($dt_txt);

                    $current_date = $date->format('Y-m-d');

                    if ($current_date != $previous_date) {

          // echo "<pre>";

          // print_r($key);

          // echo "</pre>";

                         $ctemp_main =  $key['main']['temp'];

                         $current_ctemp_main = number_format((float)$ctemp_main, 0, '.', '');

                         if(isset($key['weather'][0]['id'])){

                              $cod = $key['weather'][0]['id'];

                         }

          // echo $current_ctemp_main;

          // echo "<br/>";

          //Convert the date string into a unix timestamp.

                         $unixTimestamp = strtotime($current_date);

          //Get the day of the week using PHP's date function.

                         $dayOfWeek = date("l", $unixTimestamp);

          //Print out the day that our date fell on.

          //echo $date . ' fell on a ' . $dayOfWeek;

          //}            

                    //$code=$cod;

                         echo  "<table >";

                         echo   "<tr>";

                         echo     "<th>".$dayOfWeek."</th>";

                         echo   "</tr>";

                         echo   "<tr>";

                         echo    "<td>".$current_ctemp_main." &#8451; ";

                         switch($cod){

                              /*Group 2xx: Thunderstorm:*/

                              /*========================*/

                              case '200':

                              echo 'Thunderstorm with light rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';

                              break;

                              case '201':

                              echo 'Thunderstorm with rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';

                              break;

                              case '202':

                              echo 'Thunderstorm with heavy rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';

                              break;

                              case '210':

                              echo 'Light thunderstorm.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';

                              break;

                              case '211':

                              echo 'Thunderstorm.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';

                              break;

                              case '212':

                              echo 'Heavy thunderstorm.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';

                              break;

                              case '221':

                              echo 'Ragged thunderstorm.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';

                              break;

                              case '230':

                              echo 'Thunderstorm with light drizzle.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';

                              break;

                              case '231':

                              echo 'Thunderstorm with drizzle'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';

                              break;

                              case '232':

                              echo 'Thunderstorm with heavy drizzle.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/11d.png ">';

                              break;

                              /*Group 3xx: Drizzle:*/

                              /*===================*/

                              case '300':

                              echo 'Light intensity drizzle.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';

                              break;

                              case '301':

                              echo 'Drizzle.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';

                              break;

                              case '302':

                              echo 'Heavy intensity drizzle.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';

                              break;

                              case '310':

                              echo 'Light intensity drizzle rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';

                              break;

                              case '311':

                              echo 'Drizzle rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';

                              break;

                              case '312':

                              echo 'Heavy intensity drizzle rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';

                              break;

                              case '313':

                              echo 'Shower rain and drizzle.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';

                              break;

                              case '314':

                              echo 'Heavy shower rain and drizzle.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';

                              break;

                              case '321':

                              echo 'Shower drizzle.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';

                              break;

                              /*Group 5xx: Rain:*/

                              /*=================*/

                              case'500':

                              echo 'Light rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/10d.png ">';

                              break;

                              case'501':

                              echo 'Moderate rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/10d.png ">';

                              break;

                              case'502':

                              echo 'Heavy intensity rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/10d.png ">';

                              break;

                              case'503':

                              echo 'Very heavy rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/10d.png ">';

                              break;

                              case'504':

                              echo 'Extreme rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/10d.png ">';

                              break;

                              case'511':

                              echo 'Freezing rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';

                              break;

                              case'520':

                              echo 'Light intensity shower rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';

                              break;

                              case'521':

                              echo 'Shower rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';

                              break;

                              case'522':

                              echo 'Heavy intensity shower rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';

                              break;

                              case'531':

                              echo 'Ragged shower rain.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/9d.png ">';

                              break;

                              /*Group 6xx: Snow:*/

                              /*==================*/

                              case'600':

                              echo 'Light snow.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';

                              break;

                              case'601':

                              echo 'Snow.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';

                              break;

                              case'602':

                              echo 'Heavy snow.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';

                              break;

                              case'611':

                              echo 'Sleet.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';

                              break;

                              case'612':

                              echo 'Shower sleet.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';

                              break;

                              case'615':

                              echo 'Light rain and snow.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';

                              break;

                              case'616':

                              echo 'Rain and snow.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';

                              break;

                              case'620':

                              echo 'Light shower snow.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';

                              break;

                              case'621':

                              echo 'Shower snow.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';

                              break;

                              case'622':

                              echo 'Heavy shower snow.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/13d.png ">';

                              break;

                              /*Group 7xx: Atmosphere:*/

                              /*======================*/

                              case'701':

                              echo 'Mist.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';

                              break;

                              case'711':

                              echo 'Smoke.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';

                              break;

                              case'721':

                              echo 'Haze.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';

                              break;

                              case'731':

                              echo 'Sand, dust whirls.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';

                              break;

                              case'741':

                              echo 'Fog.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';

                              break;

                              case'751':

                              echo 'Sand.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';

                              break;

                              case'761':

                              echo 'Dust.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';

                              break;

                              case'762':

                              echo 'Volcanic ash.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';

                              break;

                              case'771':

                              echo 'Squalls.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';

                              break;

                              case'781':

                              echo 'Tornado.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/50d.png ">';

                              break;

                              /*Group 800: Clear:*/

                              /*===================*/

                              case'800':

                              echo 'Clear sky.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/1d.png ">';

                              break;

                              /*Group 80x: Clouds:*/

                              /*===================*/

                              case'801':

                              echo 'Few clouds.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/2d.png ">';

                              break;

                              case'802':

                              echo 'Scattered clouds.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/3d.png ">';

                              break;

                              case'803':

                              echo 'Broken clouds.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/4d.png ">';

                              break;

                              case'804':

                              echo 'Overcast clouds.'.'<br>'.'<img src="'.base_url().'assets/user_dashboard/img/weather/4d.png ">';

                              break;

                         }

                         "</td>";

                         echo  "</tr>";

                         echo "</table>";

                    }

                    $previous_date = $current_date;

               }

               ?>

          </section>



     </aside>

     <script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

     <script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>

     <script type="text/javascript">
          $(document).on('click','.updateStatus',function(){
           var update_status = $(this).attr('data-btn');
           var update_st = $(this).attr('data-st');
    //alert(update_status+'-'+update_st);
    $.ajax({
         url: '<?php echo base_url();?>Pages/pages/change_button_status',
         type: "POST",
         data: {'btn':update_status},
         dataType: "json",
         success:function(data){
            setTimeout(function(){
               $("#msg_show").html("<div class='alert alert-success'>"+data.status+"</div>");
               location.reload();

          }, 1500);
       }
  }); 
});
</script>


<style>

     table, th, td {

          border: 1px solid black;

          border-collapse: collapse;

     }

     th, td {

          padding: 5px;

          text-align: left;

     }

</style>