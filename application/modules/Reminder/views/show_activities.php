<div id="loader" class="lds-dual-ring hidden overlay"></div>

<style type="text/css">
    .todaysDate {
        background: #1f569e !important;
        color: #fff;
    }

    /*Hidden class for adding and removing*/
    .lds-dual-ring.hidden {
        display: none;
    }

    /*Add an overlay to the entire page blocking any further presses to buttons or other elements.*/
    .overlay {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100vh;
        background: rgba(255, 255, 255, 0.8);
        z-index: 999;
        opacity: 1;
        transition: all 0.5s;
    }

    /*Spinner Styles*/
    .lds-dual-ring {
        display: inline-block;
        width: 100%;
        height: 100%;
    }

    .lds-dual-ring:after {
        content: " ";
        display: block;
        width: 64px;
        height: 64px;
        margin: 20% auto;
        border-radius: 50%;
        border: 6px solid #000;
        border-color: #000 transparent #000 transparent;
        animation: lds-dual-ring 1.2s linear infinite;
        top: 50% !important;
    }

    @keyframes lds-dual-ring {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }

    .text_heading {
        border-bottom: 0;
        margin-bottom: 0;
    }

    /* .overlay{
    display: none;
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    z-index: 999;
    background: rgba(255,255,255,0.8) url("<?php echo base_url() ?>/uploads/loading-gif-png-5.gif") center no-repeat;
}
/* Turn off scrollbar when body element has the loading class */
    /* body.loading{
    overflow: hidden;   
} */
    /* Make spinner image visible when body element has the loading class */
    /* body.loading .overlay{
    display: block;
}  */
    #responseImage {
        height: 200px;
        max-width: 500px;
        width: 100%;
        margin: 0 auto;
        text-align: center;
    }

    #responseImage img {
        height: 100%;
    }

    .addNote a,
    .btn.btn-primary {
        margin: 0 0 14px 0 !important;
    }

    .news_section table th:hover,
    .news_section table th.active {
        background: #1f569e;
        border-color: #1f569e;
        color: #fff;
        cursor: pointer;
    }



    /* Blue */
    @media only screen and (max-width: 960px) {
        .calendar {
            width: 100%;
            margin: 0px;
            margin: 0px;
            box-sizing: border-box;
            position: relative;
            left: 0px;
        }
    }

    table {
        width: 100%;
    }

    th,
    td {
        padding: 15px;
        text-align: left;
    }

    section.content.weather-bg.news_section {
        float: left;
        width: 100%;
    }
</style>
<div class="text_heading">
    <div id="msg_show"></div>

    <h3><i class="fa fa-tasks" aria-hidden="true"></i> All Activities List</h3>
    <div class="tooltip-2">
        <h2>Display Activities button on Mobile App
            <?php
            if (!empty($btnsetingArray) && $btnsetingArray != '') {
                foreach ($btnsetingArray as $res) {
                    if ($res->settings == 1) { ?>
                        <label class="switch ">
                            <input type="checkbox" checked data-btn="<?php echo 'Calendar' ?>" class="updateStatus">
                            <span class="slider round"></span>
                        </label>

                    <?php } else { ?>
                        <label class="switch space">
                            <input type="checkbox" data-btn="<?php echo 'Calendar' ?>" class="updateStatus">
                            <span class="slider round"></span>
                        </label>

                    <?php } ?>
                <?php     } ?>
            <?php } else { ?>
                <label class="switch space">
                    <input type="checkbox" data-btn="<?php echo 'Calendar' ?>" class="updateStatus">
                    <span class="slider round"></span>
                </label>
            <?php } ?>
        </h2>
    </div>
    <div class="rem_add"><a href="<?php echo base_url(); ?>Reminder/add_reminder" class="btn btn-primary">Add</a></div>
</div>

<section class="content weather-bg news_section">
    <div class="calendar" id="calendar">
        <div class="calendar-btn month-btn" onclick="$('#months').toggle('fast')">
            <span id="curMonth"></span>
            <div id="months" class="months dropdown"></div>
        </div>
        <div class="calendar-btn year-btn" onclick="$('#years').toggle('fast')">
            <span id="curYear"></span>
            <div id="years" class="years dropdown"></div>
        </div>
        <div class="clear"></div>
        <div class="calendar-dates">
            <div class="days">
                <div class="day label">SUN</div>
                <div class="day label">MON</div>
                <div class="day label">TUE</div>
                <div class="day label">WED</div>
                <div class="day label">THUR</div>
                <div class="day label">FRI</div>
                <div class="day label">SAT</div>
                <div class="clear"></div>
            </div>
            <div id="calendarDays" class="days">
            </div>
        </div>
    </div>

    <table border="1" id="heading_click" class="editField">
        <tr>
            <th id="daily">Daily</th>
            <th id="weekly">Weekly</th>
            <th id="monthly">Monthly</th>
        </tr>

    </table>

    <div id="showeventdata">
    </div>
    </table>
</section>
</aside>
<script type="text/javascript">
    $(document).on('click', '.updateStatus', function() {
        var update_status = $(this).attr('data-btn');
        var update_st = $(this).attr('data-st');
        //alert(update_status+'-'+update_st);
        $.ajax({
            url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
            type: "POST",
            data: {
                'btn': update_status
            },
            dataType: "json",
            // beforeSend: function () { // Before we send the request, remove the .hidden class from the spinner and default to inline-block.
            //     $('#loader').removeClass('hidden')
            // },
            success: function(data) {
                $("#msg_show").html("<div class='alert alert-success' id='hide_msg'>" + data.status + "</div>");
                setTimeout(function() {
                    $('#hide_msg').fadeOut('fast');
                }, 2500);
            },
            //  complete: function () { // Set our complete callback, adding the .hidden class and hiding the spinner.
            //     $('#loader').addClass('hidden')
            // },
        });
    });
</script>
<script type="text/javascript">
    $("#heading_click th").click(function() {
        $("#heading_click th").removeClass("active");
        $(this).addClass("active");



    });
    $(document).on("click", "#years .dropdown-item", function() {
        var year_dropdown = $(this).text();
        $("#myTable").html("");
        $.ajax({
            url: '<?php echo base_url(); ?>Reminder/yearwisedata',
            type: "POST",
            data: {
                year_dropdown: year_dropdown,
            },

            timeout: 5000,
            success: function(result) {
                //   alert(response);
                $("#showeventdata").html(result);

            }
        });
    });
    $(document).on("click", ".day", function() {
        var dateon = $(this).attr("data-data");

        $(".day,.active1").removeClass('active1');
        $(this).addClass("active1");
        $("#myTable").html("");
        $.ajax({
            url: '<?php echo base_url(); ?>Reminder/search_datewise_result',
            type: "POST",
            data: {
                dateon: dateon,
            },


            timeout: 5000,
            success: function(result) {
                $("#showeventdata").html(result);

            },

        });

    });
</script>
<script type="text/javascript">
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];


    var startYear = 2000;
    var endYear = 2020;
    var month = 1;
    var year = 0;

    function loadCalendarMonths() {
        for (var i = 0; i < months.length; i++) {
            var doc = document.createElement("div");
            doc.innerHTML = months[i];
            doc.setAttribute("data-value", i + 1);
            // doc.name= i+1;
            doc.classList.add("dropdown-item");
            doc.onclick = (function() {
                var selectedMonth = i;

                return function() {
                    month = selectedMonth;
                    // alert(month);            
                    document.getElementById("curMonth").innerHTML = months[selectedMonth];

                    loadCalendarDays();
                    return months[monthNumber - 1];

                }

            })();
            document.getElementById("months").appendChild(doc);
        }
        $("#months .dropdown-item").on('click', function() {
            var selected = $(this).attr("data-value");
            $.ajax({
                url: "<?php echo base_url() ?>Reminder/get_monthly_events",
                data: {
                    'monthValue': selected
                },
                type: "post",
                timeout: 5000,
                success: function(result) {
                    $("#showeventdata").html(result);
                },
            });
        });
    }

    function loadCalendarYears() {
        document.getElementById("years").innerHTML = "";
        for (var i = startYear; i <= endYear; i++) {
            var doc = document.createElement("div");
            doc.innerHTML = i;
            doc.classList.add("dropdown-item");
            doc.onclick = (function() {
                var selectedYear = i;
                return function() {
                    year = selectedYear;
                    // alert(year);

                    document.getElementById("curYear").innerHTML = year;
                    loadCalendarDays();
                    return year;
                }
            })();
            document.getElementById("years").appendChild(doc);
        }
    }
        function loadCalendarDays() {
        var cuday = new Date();
        var cudmonth = cuday.getMonth() + 1;
        var cudday = cuday.getDate();
        var output = cuday.getFullYear() + '-' + (('' + cudmonth).length < 2 ? '0' : '') + cudmonth + '-' + (('' + cudday).length < 2 ? '0' : '') + cudday;
        // alert(output);
        document.getElementById("calendarDays").innerHTML = "";
        var tmpDate = new Date(year, month, 0);
        var num = daysInMonth(month, year);
        var dayofweek = tmpDate.getDay(); // find where to start calendar day of week
        for (var i = 0; i <= dayofweek; i++) {
            var d = document.createElement("div");
            d.classList.add("day");
            d.classList.add("blank");
            document.getElementById("calendarDays").appendChild(d);
        }
        var dateCurrent = new Date();
        var strDate = dateCurrent.getFullYear() + "-" + (dateCurrent.getMonth() + 1) + "-" + dateCurrent.getDate();
        for (var i = 0; i < num; i++) {
            var tmp = i + 1;
            var d = document.createElement("div");
            var monthnew = month + 1;
            var daystemp = i + 1;
            var tempdate123 = year + "-" + monthnew + "-" + daystemp;
            // alert(strDate);
            d.id = "calendarday_" + i;
            d.className = "day";
            d.dataset.data = tempdate123;
            d.innerHTML = tmp;
            if (strDate == tempdate123) {
                jQuery(d).addClass('todaysDate');
            }
            //var tmpDate123 = new Date(year, month, tmp);
            $.ajax({
                type: "POST",
                beforeSend: function(request) {
                    request.setRequestHeader("votive", "123456");
                },
                //    url: "<?php base_url() ?>/application/controllers/api/Users/activity_details",
                data: "user_id=878&date=" + tempdate123,
                processData: false,

                timeout: 5000,
                success: function(msg) {
                    if (msg.message == "SUCCESS") {
                        //    console.log(msg);
                        var day = parseInt(msg.response.Activities.reminders[0].date.split("-")[2]) - 1;
                        $("#calendarday_" + day).css({
                            "border-color": "blue",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                        //   console.log(day);
                    }
                }
            });
            document.getElementById("calendarDays").appendChild(d);
        }
        var clear = document.createElement("div");
        clear.className = "clear";
        document.getElementById("calendarDays").appendChild(clear);
    }

    function daysInMonth(month, year) {
        var d = new Date(year, month + 1, 0);
        return d.getDate();
    }
    window.addEventListener('load', function() {
        var date = new Date();
        month = date.getMonth();
        year = date.getFullYear();
        document.getElementById("curMonth").innerHTML = months[month];
        document.getElementById("curYear").innerHTML = year;
        loadCalendarMonths();
        loadCalendarYears();
        loadCalendarDays();
    });
</script>

<script type="text/javascript">
    $("#daily").click(function() {
        $("#showeventdata").load();

        var dayId = jQuery('#daily').text();
    //    alert(dayId);
        $.ajax({
            url: "<?php echo base_url() ?>Reminder/get_weekly",
            data: {
                'getdata': dayId
            },
            type: "post",
            timeout: 5000,
            success: function(result) {
                $("#showeventdata").html(result);
            },
        });
    });

    $("#weekly").click(function() {
        $("#showeventdata").load();

        var weekId = jQuery('#weekly').text();

    //    alert(weekId);

        $.ajax({
            url: "<?php echo base_url() ?>Reminder/get_weekly",
            data: {
                'getdata': weekId
            },
            type: "post",

            timeout: 5000,
            success: function(result) {
                $("#showeventdata").html(result);
            },
        });
    });

    $("#monthly").click(function() {
        $("#showeventdata").load();

        var monthId = jQuery('#monthly').text();
        // alert(monthId);

        $.ajax({
            url: "<?php echo base_url() ?>Reminder/get_weekly",
            data: {
                'getdata': monthId
            },
            type: "post",

            timeout: 5000,
            success: function(result) {
                $("#showeventdata").html(result);
            },
        });
        });
        });
    });

    $("document").ready(function() {
        setTimeout(function() {
            $("#daily").trigger('click');
        }, 10);
    });
</script>
