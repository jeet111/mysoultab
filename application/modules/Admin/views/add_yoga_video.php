<style>
	.red {
		color: red;
	}

	.red1 {
		color: red;
	}

	#progress-wrp {
		border: 1px solid #0099CC;
		padding: 1px;
		position: relative;
		border-radius: 3px;
		margin: 10px;
		text-align: left;
		background: #fff;
		box-shadow: inset 1px 3px 6px rgba(0, 0, 0, 0.12);
	}

	#progress-wrp .progress-bar {
		height: 20px;
		border-radius: 3px;
		background-color: #f39ac7;
		width: 0;
		box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);
	}

	.Schedule_main_one label.error {
		font-size: 14px !important;
	}

	.Schedule_main_one label.error {
		bottom: -15px !important;
	}

	#progress-wrp .status {
		top: 3px;
		left: 50%;
		position: absolute;
		display: inline-block;
		color: #000000;
	}

	div#video_loader {
		position: fixed;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		background-color: #0a0a0aa6;
		z-index: 999999;
		text-align: center;
		padding-top: 23%;
	}

	div#video_loader img#loading_img {
		width: 70px !important;
	}
</style>

<div class="container-fluid">
	<div class="dash-counter">
		<div class="Schedule_main_one">
			<div class="users-main">
				<?php echo "<h2>Add Yoga video</h2>"; ?>
				<input type="hidden" name="yoga_id" id="yoga_id" value="<?php echo $this->uri->segment('3'); ?>">
				<div class="btn_topBack">
					<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">
						Back
					</a>
				</div>
				<div id="image_msg"></div>
				<form class="form-horizontal add_movie_form" action="" method="post" enctype="multipart/form-data" name="AddUsers" id="movie-form">
					<input type="hidden" name="user_id" id="user_id" value="<?php echo $this->session->userdata('logged_ins')['id']; ?>">
					<div id="loders"></div>
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Title:<span class="field_req">*</span></label>
							<div class="col-md-8">
								<input type="text" placeholder="Yoga Video Title" class="form-control" name="yoga_title" id="username" value="<?php echo set_value('article_title'); ?>">
								<?php echo form_error('yoga_title'); ?>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Description:<span class="field_req">*</span></label>
							<div class="col-md-8">
								<textarea name="yoga_desc" id="" class="form-control" placeholder="Yoga Video Description" value="<?php echo set_value('yoga_desc'); ?>"></textarea>
								<?php echo form_error('yoga_desc'); ?>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Level:<span class="field_req">*</span></label>
							<div class="col-md-8">
								<select name="yoga_level" class="form-control">
									<option value="">Select</option>
									<option value="Easy">Easy</option>
									<option value="Moderate">Moderate</option>
									<option value="Challenging">Challenging</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Type:<span class="field_req">*</span></label>
							<div class="col-md-8">
								<select name="yoga_type" class="form-control">
									<option value="">Select</option>
									<option value="Morning">Morning</option>
									<option value="Afternoon">Afternoon</option>
									<option value="Evening">Evening</option>
								</select>
								<?php echo form_error('movie_type'); ?>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Help with</label>
							<div class="col-md-8">
								<select name="body_part" class="form-control">
									<option value="">Select</option>
									<option value="Arms">Arms</option>
									<option value="Back">Back</option>
									<option value="Neck">Neck</option>
									<option value="Head & Neck">Head & Neck</option>
								</select>
								<?php echo form_error('movie_artist'); ?>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Yoga Style</label>
							<div class="col-md-8">
								<select name="yoga_style" class="form-control">
									<option value="">Select</option>
									<option value="Restorative">Restorative</option>
									<option value="Energizing">Energizing</option>
								</select>
								<?php echo form_error('movie_artist'); ?>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Yoga Pose</label>
							<div class="col-md-8">
								<select name="yoga_pose" class="form-control">
									<option value="">Select</option>
									<option value="Stand">Stand</option>
									<option value="Chair">Chair</option>
									<option value="Mix">Mix</option>
									<option value="floor">Floor</option>
								</select>
								<?php echo form_error('movie_artist'); ?>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Youtube URL</label>
							<div class="col-md-8">
								<input type="text" placeholder="Yoga Video Title" class="form-control" name="yoga_url" id="yoga_url">
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Upload Video</label>
							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-btn">
										<input type="file" id="video_upload" class="form-control" name="video_upload" name="video_upload" onchange="loadFile1(event)">
										<?php echo form_error('upload_video'); ?>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Duration</label>
							<div class="col-md-8">
								<select name="yoga_duration" class="form-control">
									<option value="">Select</option>
									<option value="3 Pose">3 Pose</option>
									<option value="5 Pose">5 Pose</option>
									<option value="8 Pose">8 Pose</option>

								</select>
								<?php
								// $start = "00:00"; 
								// $end = "00:30";

								// $tStart = strtotime($start);
								// $tEnd = strtotime($end);
								// $tNow = $tStart;
								// echo '<select name="yoga_duration" class="form-control" >';
								// while ($tNow <= $tEnd) {
								// 	echo '<option value="' . date("i:s", $tNow) . '">' . date("i:s", $tNow) . '</option>';
								// 	$tNow = strtotime('+1 minutes', $tNow);
								// }
								// echo '</select>';
								?>
								<!-- </select> -->

								<?php //echo form_error('movie_artist'); 
								?>

							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-md-4" for=""> Image:<span class="field_req">*</span></label>
							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-btn">
										<input type="file" id="profile_images" class="form-control" name="pictureFile" onchange="loadFile(event)" required>
										<?php echo form_error('upload_image'); ?>
									</span>
									<div id="flupmsgs"></div>
								</div>
								<div>
									<span id="demo" style="color: red"></span>
								</div>
								<img id='img-upload' src="">
								<div class="bnt_sub">
									<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit">

								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
<script>
	$(".add_movie_form").on('submit', (function(e) {

		e.preventDefault();
		var isvalidate = $(".add_movie_form").valid();
		if (isvalidate) {
			$("#loders").append('<img id="loading_img" style="width:20px" src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif" />');
			$.ajax({
				url: "<?php echo base_url(); ?>Admin/Yoga_video/ajax_yoga_add",
				type: "POST",
				data: new FormData(this),
				dataType: "json",
				contentType: false,
				cache: false,
				processData: false,
				success: function(data) {
					$("#loading_img").hide();
					if (data.code == 1) {
						$("#loading_img").hide();
						$("#sub").prop('disabled', true);
						$("#image_msg").html('<div class="alert alert-success">' + data.message + '<div>');
						setTimeout(function() {
							$("#image_msg").html("");
							window.location.href = '<?php echo base_url(); ?>admin/yoga_video_list';
						}, 1000);
					} else {
						$("#loading_img").hide();
						$("#image_msg").html('<div class="alert alert-danger">' + data.message + '<div>');
					}
				}
			});
		}
	}));

	function loadFile(event) {
		var pcFile = $('#profile_images').val().split('\\').pop();
		var pcExt = pcFile.split('.').pop();
		var output = document.getElementById('img-upload');
		if (pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG" || pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF") {

			$('#img-upload').show();

			output.src = URL.createObjectURL(event.target.files[0]);

			$('#flupmsgs').html('');

			$('.red1').html('');

			$('#demo').html('');

		} else {

			$('#demo').html('');

			$('#flupmsgs').html('<div class="red1">Please select only Image file.</div>');

			$('#img-upload').hide();

		}

	};





	function loadFile1(event) {
		var pcFile = $('#video_upload').val().split('\\').pop();
		var pcExt = pcFile.split('.').pop();
		var output = document.getElementById('img-uploads');
		if (pcExt == "mp4" || pcExt == "3gp" || pcExt == "avi" || pcExt == "wmv") {
			$('#img-uploads').show();
			$('.red').html('');
			$('#flupmsg').html('');
			$('#demo1').html('');
		} else {
			$('#demo1').html('');
			$('#dfds').hide();
			$('#flupmsg').html('<div class="red">Please select only video file.</div>');
			$('#img-uploads').hide();
		}
	};
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$.validator.addMethod("regx", function(value, element, regexpr) {
			return regexpr.test(value);
		}, "Please upload valid extension mp3 file.");
		$('#movie-form').validate({
			rules: {
				yoga_title: 'required',
				yoga_desc: 'required',
				yoga_level: 'required',
				yoga_type: 'required',
				upload_video: 'required',
				upload_image: 'required'
			},
			messages: {
				yoga_title: 'Please enter video title.',
				yoga_desc: 'Please enter video description.',
				yoga_level: 'Please select a level name.',
				yoga_type: 'Please select a video type.',
				upload_video: 'Please select a video',
				upload_image: 'Please select a image',
			}
		});
	});

	/*Created by 95 on 6-2-2019 For Image validation*/

	function showFileSize() {
		var img = document.getElementById('profile_imagess').value;
		var img1 = document.getElementById('profile_images').value;
		if (img === '' || img === null) {
			document.getElementById("demo1").innerHTML = "Please choose a video file.";
		} else {
			document.getElementById("demo1").innerHTML = " ";
		}
		if (img1 === '' || img1 === null) {
			document.getElementById("demo").innerHTML = "Please choose a Image file.";
		} else {
			document.getElementById("demo").innerHTML = " ";
		}
	}
	$(document).ready(function() {
		var progress_bar_id = '#progress-wrp';
		$('#profile_imagess').change(function(e) {
			var pcFile = $('#profile_imagess').val().split('\\').pop();
			var pcExt = pcFile.split('.').pop();
			var output = document.getElementById('img-uploads');
			if (pcExt == "mp4" || pcExt == "3gp" || pcExt == "avi" || pcExt == "wmv") {
				$('#progress-wrp').show();
				$(progress_bar_id + " .progress-bar").css("width", "0%");
				$(progress_bar_id + " .status").text("0%");
				var file = this.files[0];
				var form = new FormData();
				form.append('360_image_upload', file);
				$.ajax({
					url: "<?php echo base_url(); ?>Admin/Movie/image_upload",
					type: "POST",
					cache: false,
					contentType: false,
					processData: false,
					data: form,
					dataType: "json",
					xhr: function() {
						//upload Progress
						var xhr = $.ajaxSettings.xhr();
						if (xhr.upload) {
							xhr.upload.addEventListener('progress', function(event) {
								var percent = 0;
								var position = event.loaded || event.position;
								var total = event.total;
								if (event.lengthComputable) {
									percent = Math.ceil(position / total * 100);
								}

								//update progressbar
								$(progress_bar_id + " .progress-bar").css("width", +percent + "%");
								$(progress_bar_id + " .status").text(percent + "%");
							}, true);
						}
						return xhr;
					},

					success: function(data) {
						$("#progress-wrp").hide();
						$("#dfds").show();
						var fileInput = document.getElementById('profile_imagess');
						var fileUrl = window.URL.createObjectURL(fileInput.files[0]);
						$("#dfds").attr("src", fileUrl);
					}
				});
			}
		});
	});
	$(document).ready(function() {
		$('#img-upload').hide();
	});
</script>