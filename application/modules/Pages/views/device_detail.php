
 <style>
table#devicatable td, table#devicatable th {
    font-size: 14px;
    color: #000;
    text-transform: capitalize;
}
.btn.btn-primary {
    margin: 4px 0 22px !important;
    max-width: 188px !important;
}
 </style> <div class=" text_heading">
  <h3 class="text-center"><i class="" aria-hidden="true"></i> Tablet Details </h3>
  <div class="tooltip-2">  </div>

  <?php if(!empty($btnsetingArray)){ ?>
  	<div class="back_reminder">
		<button class="btn btn-primary" id="edit_setting" align="center">Edit Tablet Settings</button>
	</div>
  <?php } ?>
	<section class="photo-listMain reminder_listMain">

	  	<table id="devicatable"  class="table table-hover tab_comn">
		  <?php if(!empty($btnsetingArray)){ ?>
		  
			 <?php $device_id = $btnsetingArray['device_id'];?>
			 <thead>
			<tr>
			  <th scope="col">Device ID</th>
			  <th scope="col">Last Known Location</th>
			  <th scope="col">Battery Percentage(%)</th>
			  <th scope="col">Last Online</th>
			  <th scope="col">OS Version</th>
			  <th scope="col">App Version</th>
			  <th scope="col">Connection Type</th>
			  <th scope="col">Signal Strength</th>
			  </tr>
			 </thead>
			  <tr>
			  <td scope="row" align="center" data-label="Device ID"><?php echo $btnsetingArray['device_unique_id']; ?></td>
		
			  <td align="center" data-label="Last Known Location"><?php echo $btnsetingArray['device_last_known_location']; ?></td>			
			  <td align="center" data-label="Battery Percentage(%)"><?php echo $btnsetingArray['device_battery_percentage']; ?></td>			
			  <td data-label="Last Online" ><?php echo $btnsetingArray['device_last_online']; ?></td>
			
			  <td align="center" data-label="OS Version"><?php echo $btnsetingArray['device_os_version']; ?></td>
			
			  <td align="center" data-label="App Version"><?php echo $btnsetingArray['device_app_version']; ?></td>
			
			  <td align="center" data-label="Connection Type"><?php echo $btnsetingArray['device_connection_type']; ?></td>
			
			  <td align="center" data-label="Signal Strength"><?php echo $btnsetingArray['device_signal_strength']; ?></td>
			</tr>
			
		<?php } else { ?>
		 <h5 align="center">No Data Found!</h5>
        <?php } ?>
		  
		</table>
   </section>
  </div>

<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script>
	$(document).ready( function () {
    $('#devicatable').DataTable();
} );

	$('#edit_setting').click(function() {
		
		var device_id = "<?php echo $device_id ?>";
		var url = '<?php echo base_url()."edit_device_setting/" ?>';
		
		window.location.href = url+device_id;
		return false;
	});

</script>
