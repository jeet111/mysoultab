<body onload="initialize()">

	<aside class="right-side">    

		<section class="content-header no-margin">

			<h1 class="text-center"><i class="fa fa-heart"></i> Favorite Grocery <a href="javascript:void(0)" class="search-open"><i class="fa fa-search"></i></a>

			</h1>

			<div class="searching">         

				<div class="search-inline">

					<form method="post" action="" name="grocery_search" id="grocery_search">

						<input type="text" class="form-control" placeholder="Search Grocery.." name="grocery_title" id="grocery_title">

						<button type="submit" id="search_sub"><i class="fa fa-search"></i></button>

						<a href="javascript:void(0)" class="search-close">

							<i class="fa fa-times"></i>

						</a>

					</form>

				</div>

			</div>

		</section>

		<section class="content weather-bg news_section">
			
			<!-- setting start -->
			<div class="col-md-12">
				<div class="col-md-10">
					<h3>Favorite Grocery</h3>
					<?php if(!empty($btnsetingArray)){ ?>
						<?php if($btnsetingArray[0]->settings=='1'){ ?>

							<label><input type="radio" name="btnshow" value='1' checked="checked">Now Favorite Grocery showing on the mysoultab APP<br></label>

						<?php }else{ ?>

							<label> <input type="radio" name="btnshow" value='1' checked="checked">Now Favorite Grocery not showing on the mysoultab APP </label>

						<?php } ?>

					<?php } ?> 
				</div>
				<div class="col-md-2 delete-email">
					<button class="btn btn-primary chk">Settings</button>
				</div> 
				<div id="para" style="display: none "> 



					<form id="submit_form" method="post" action="<?php echo base_url()?>savsettings "> 
						<div class="col-md-10">
							<label> <input type="checkbox" name="butn_seting" id="butn_seting" value="">Please check if want to show the Favorite Grocery button in mysoultab App </label>
							<input type="hidden" name="btn" value="favorite_grocery">
						</div>
						<div class="col-md-2">
							<input class="btn btn-primary chkdd" type="submit" name="submit" value="submit" >
						</div>
					</form>
					<div id="response"></div>
				</div>

				<!-- </div> -->

				<!-- End -->
			</section>


			<div id="fav_message"></div>

			<input type="hidden" name="get_lat" id="get_lat" value="" >

			<input type="hidden" name="get_long" id="get_long" value="" >

			<section class="content photo-list">

				<div class="photo-listMain main_restaurant" id="main_grocery">

					<div class="row">

						<div class="col-md-12"><h5 class="sr_rest"><?php if(!empty($favorite_grocery)){ ?><?php echo $count_total; ?> Grocery <?php } ?></h5></div>                

					</div>

					<div class="row" id="main-cntdiv">

						<?php 

						$image = '';

						$i=1;

						if(!empty($favorite_grocery)){ 

							foreach($favorite_grocery as $grocery){

								

								$image = $grocery['fav_grocery_image'];

								$fav = $this->common_model->getSingleRecordById('cp_fav_grocery',array('fav_grocery_place_id'=> $grocery['fav_grocery_place_id'],'user_id' => $this->session->userdata('logged_in')["id"]));

								

								?>

								<div class="col-sm-6 col-md-4 loadData" id="myData_<?php echo $i;?>">    

									<div class="bx_rest">

										<div class="bx_mig">

											<img src='<?php echo $image; ?>' class="" alt="">

											<span id="re_<?php echo $grocery['fav_grocery_place_id']; ?>"> <a href="javascript:void(0);" class="fav_none_<?php echo $grocery['fav_grocery_place_id']; ?>" id="favorite_grocery_<?php echo $grocery['fav_grocery_place_id']; ?>" data-grocery="<?php echo $grocery['fav_grocery_place_id']; ?>"><?php if($fav['fav_grocery_place_id']){ ?><i class="fa fa-heart"></i><?php }else{ ?><i class="fa fa-heart-o remove"></i><?php } ?></a></span>

											

										</div>

										<div class="bx_name">

											

											<h4><a href="<?php echo base_url() ?>view_favorite_grocery/<?php echo $grocery['fav_grocery_id']; ?>" ><?php echo $grocery['fav_grocery_name']; ?></a></h4>

											<ul class="abs">

												<?php for($i=1;$i<=$grocery['fav_grocery_rating'];$i++) { ?>

													

													<li><i class="fa fa-star" aria-hidden="true"></i></li>

												<?php } 

												if (strpos($grocery['fav_grocery_rating'],'.')) { ?>

													

													<li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>

													<?php $i++; } ?>

													<?php while ($i<=5) { ?>

														<li><i class="fa fa-star-o" aria-hidden="true"></i></li>

														<?php $i++; } 



														?>

														

													</ul>

												</div>                            

											</div>

										</div>

									<?php }}else{ ?>

										<div class="photo-list-empty">

											<span class="blnk_photo"><i class="fa fa-picture-o" aria-hidden="true"></i></span>

											<h4>Grocery not found</h4>

										</div>

									<?php } ?> 

								</div> 

								

							</div>

						</section>

					</aside>

					<script src="<?php echo base_url(); ?>assets/js/jquery.min2.js"></script>

					<script>

  /*var geocoder;



  if (navigator.geolocation) {

    navigator.geolocation.getCurrentPosition(successFunction, errorFunction);

} 

//Get the latitude and the longitude;

function successFunction(position) {

    var lat = position.coords.latitude;

	$("#get_lat").val(lat);

	

    var lng = position.coords.longitude;

	$("#get_long").val(lng);

   

}



function errorFunction(){

  //  alert("Geocoder failed");

}

*/





$.getJSON('https://ipinfo.io/geo', function(response) { 

	var loc = response.loc.split(',');

	var coords = {

		latitude: loc[0],

		longitude: loc[1]

	};

	$("#get_lat").val(loc[0]);

	$("#get_long").val(loc[1]);

	

});





function initialize()

{

	var lat = $("#get_lat").val();

	var longt = $("#get_long").val();

	$.ajax({							   

		type:'POST',			    

		url: '<?php echo base_url(); ?>Pages/session_value',

		data: 'lat='+lat+'&longt='+longt,           

		dataType:'json',

		

		success:function(data){

			

		}  



	});

}









</script>



<script>

	$(document).ready(function (e) 

	{ 

		$(window).scroll(function()

		{ 

			scrollMore();

		});

		

	});



	function scrollMore()

	{

		if($(window).scrollTop() == ($(document).height() - $(window).height()))

		{ 

	//$(window).unbind("scroll");

	var records = '<?php echo $count_total;?>';

	

	var offset = $('[id^="myData_"]').length;

	//  alert(offset);

	if(records != offset)

	{ 

		$('#main-cntdiv').append('<img id="loader_img" style="width:50px" src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif" />');

		loadMoreData(offset);

	}

} 

}



function loadMoreData(offset)

{

	var like = $("#grocery_title").val();

	if(like){

		var like_val = like;

	}else{

		var like_val = '';

	}

	

	$.ajax({

		type: 'post',

		async:false,

		url: '<?php echo base_url(); ?>Pages/Grocery/get_fav_ajax_offset',

		data: 'offset='+offset+'&like_val='+like_val,

		success: function(data)

		{	

			



			//alert(data);



			$('#main-cntdiv').append(data);

			$('#loader_img').remove();

		},

		error: function(data)

		{

			alert("ajax error occured�"+data);

		}

	}).done(function()

	{	

		getliveurl();

		$(window).bind("scroll",function()

		{

			

			scrollMore();	

		});

	});

}



$(document).on('keyup','#grocery_title',function(e){ 

 // $( "#music_title" ).keypress(function( e ) {

 	e.preventDefault();

 	var grocery_title = $("#grocery_title").val();

 	

 	$.ajax

 	({

 		url: "<?php echo base_url(); ?>Pages/Grocery/search_fav_grocery",

 		type: "POST",             

 		data: 'grocery_title='+grocery_title, 

			//dataType: "json",			        

			success: function(response)   

			{

				$('#main_grocery').html(response);

			}

			

		});         

 	

 }); 



$(document).on('click','[id^="favorite_grocery_"]',function(){	

	var grocery_id = $(this).attr('data-grocery');

	$.ajax({

		type:'POST',

		url: "<?php echo base_url().'Pages/Grocery/favorite_grocery'; ?>",

		data: 'grocery_id='+grocery_id,            

		success:function(data){	

			

			setTimeout(function(){

				$("#fav_message").html("<div class='alert alert-success'>Unfavorite successfully !</div>");

				window.location.href = '<?php echo base_url(); ?>favorite_grocery';

			}, 1000);

			

		}

	});

});		



</script>

<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>









<script>

	var sp = document.querySelector('.search-open');

	var searchbar = document.querySelector('.search-inline');

	var shclose = document.querySelector('.search-close');

	function changeClass() {

		searchbar.classList.add('search-visible');

	}

	function closesearch() {

		searchbar.classList.remove('search-visible');

	}

	sp.addEventListener('click', changeClass);

	shclose.addEventListener('click', closesearch);

	

</script>

