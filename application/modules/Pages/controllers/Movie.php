<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Movie extends MX_Controller {



	public function __construct() {

		parent:: __construct();

		$this->load->library('session');

		$this->load->library('form_validation');

		$this->load->model('activities_model');

		$this->load->model('Common_model_new');

		$this->load->model('common_model_new');

		$this->load->model('movie_model');

		$this->load->helper(array('common_helper'));

		$this->load->helper(array('url'));

		$this->load->helper(array('form'));

		not_login();

	}


	public function movie_list()

	{

		$user_id = $this->session->userdata('logged_in')['id'];


		$data['menuactive'] = $this->uri->segment(1);

		$checkLogin = $this->session->userdata('logged_in');



		$data['movie_banner'] = $this->movie_model->getAllBanner();

		$popularData = $this->movie_model->getAllPopular();

		if(!empty($popularData)){

			$data['popular_array']=$popularData;

		}else{
			$data['popular_array']=array();
		}

		// echo "<pre>";
		// print_r($data['popular_array']);
		// die;

		$recent_array  = $this->movie_model->getAllRecent();

		if(!empty($recent_array)){

			$data['recent_array']=$recent_array;

		}else{
			$data['recent_array']=array();
		}


		$category_array = $this->movie_model->getAllCategory();

		if(!empty($category_array)){

			$data['category_array']=$category_array;

		}else{
			$data['category_array']=array();
		}


		$mymovie = $this->movie_model->getAllMyMovie();

		if(!empty($mymovie)){

			$data['mymovie']=$mymovie;

		}else{
			$data['mymovie']=array();
		}

		$UserFavMovie  = $this->movie_model->getAllMyFavMovie();

		if(!empty($UserFavMovie)){

			$data['UserFavMovie']=$UserFavMovie;

		}else{
			$data['UserFavMovie']=array();
		}


		$PlayMovie  = $this->movie_model->getAllPlayMovie();

		if(!empty($PlayMovie)){

			$data['PlayMovie']=$PlayMovie;

		}else{
			$data['PlayMovie']=array();
		}



		$data['popular_count_array'] = $this->Common_model_new->getAll("movie");





		$data['category_count_array'] = $this->Common_model_new->getAllwhere("cp_movie_category",array("movie_category_status" => 1));



		$data['mymovie_count_array'] = $this->Common_model_new->getAllwhere("movie",array("user_id" => $checkLogin['id']));



		$data['favorite_count_array'] = $this->Common_model_new->getAllwhere("cp_movie_favorite",array("user_id" => $checkLogin['id']));


		$data['movieArray'] = $this->Common_model_new->getAllwhere("setting_butns",array("btnname" =>'movies'));


		$this->template->set('title', 'All Movie');

		$this->template->load('new_user_dashboard_layout', 'contents', 'movie',$data);



	}







	/*Created by 95 for show all the activities*/

	public function detail_movie()

	{

		$data['menuactive'] = $this->uri->segment(1);

		$checkLogin = $this->session->userdata('logged_in');

		if(!empty($checkLogin))

		{

			$segment = $this->uri->segment('2');

			$data['detail_movie']  = $this->movie_model->getsingle("movie",array("movie_id" => $segment));

			$data['check_movie'] = $this->movie_model->getsingle("cp_movie_favorite",array("movie_id" => $segment,"user_id" => $checkLogin['id']));

			$this->template->set('title', 'All Activities');

			$this->template->load('user_dashboard_layout_movie', 'contents', 'detail_movie',$data);

		}else{



			redirect('login');

		}



	}



	public function favorite_movie()

	{

		$movie_id = $this->input->post("movie_id");

		$checkLogin = $this->session->userdata('logged_in');

		$check_movie = $this->movie_model->getsingle("cp_movie_favorite",array("movie_id" => $movie_id,"user_id" => $checkLogin['id']));

		if(!empty($check_movie)){

			$this->movie_model->delete("cp_movie_favorite",array("movie_id" => $movie_id,"user_id" => $checkLogin['id']));

			echo '0';

		}else{

			$array = array("user_id" => $checkLogin["id"],"movie_id" => $movie_id,"fav" => 1,"movie_fav_created" => date("Y-m-d H:i:s"));

			$this->movie_model->insertData("cp_movie_favorite",$array);

			echo '1';

		}

	}



 // public function delete_movie()

 //  {

	//    $segment = $this->uri->segment("2");

	//    $checkLogin = $this->session->userdata('logged_in');

	//    //$this->common_model_new->updateData("cp_articles",array("deleted_user_id" => $checkLogin['id']),array("article_id" => $segment));

	//    $this->movie_model->insertData('cp_movie_trash',array('user_id' => $checkLogin['id'],'movie_id' => $segment,'movie_trash_date' => date('Y-m-d H:i:s')));



	//     $this->session->set_flashdata('success', 'Successfuly deleted');

	// 	redirect("movie_list");

 //  }





	public function get_offsets()

	{

		$data['offset'] = $this->input->post('offset');

		$like_val = $this->input->post('like_val');





		if(!empty($like_val)){



			$pagecon['per_page'] = 20;



			$music_list1 = $this->Common_model_new->getsearchdatamovie($like_val,$pagecon['per_page'],$data['offset']);



			if (!empty($music_list1['rows'])) {

				$data['movie_list'] = $music_list1['rows'];

				$data['count_total'] = $music_list1['num_rows'];

			} else {

				$data['movie_list'] = '';

				$data['count_total'] = '';

			}

		}else{

			$pagecon['per_page'] = 20;

			$music_list1 = $this->movie_model->movie_listing($pagecon['per_page'],$data['offset']);

	  //echo '<pre>';print_r($music_list1);die;

			if (!empty($music_list1['rows'])) {

				$data['movie_list'] = $music_list1['rows'];

				$data['count_total'] = $music_list1['num_rows'];

			} else {

				$data['movie_list'] = '';

				$data['count_total'] = '';

			}

		}





		$this->load->view('ajax_movievalue', $data);

	}



  // public function movie_lists()

  // {

	 //   $data['menuactive'] = $this->uri->segment(1);

	 //  $checkLogin = $this->session->userdata('logged_in');





	 //    $this->template->set('title', 'All Movie');

  //       $this->template->load('user_dashboard_layout', 'contents', 'movie',$data);

	 //   }



	public function movie_detail()

	{

		$data['menuactive'] = $this->uri->segment(1);

		$checkLogin = $this->session->userdata('logged_in');





		$this->template->set('title', 'Movie Detail');

		$this->template->load('user_dashboard_layout', 'contents', 'movie_detail',$data);

	}









	public function movie_category()

	{

		$movie_value = $this->input->post("movie_value");



		$data['page'] = 0;

		$pagecon['per_page'] = 20;



		if($movie_value == '1' || $movie_value == '2'){

			$movie_list1 = $this->Common_model_new->movie_listing_category($movie_value,$pagecon['per_page'],$data['page']);



		}else{

			$movie_list1 = $this->Common_model_new->movie_listing($pagecon['per_page'],$data['page']);



		}





		if (!empty($movie_list1['rows'])) {

			$data['movie_list'] = $movie_list1['rows'];

			$data['count_total'] = $movie_list1['num_rows'];

		} else {

			$data['movie_list'] = '';

			$data['count_total'] = '';

		}

		$this->load->view("ajax_search_category_movie",$data);

	}





	public function get_search_offset()

	{

		$data['offset'] = $this->input->post('offset');

		$pagecon['per_page'] = 20;

		$movie_value = $this->input->post('movie_value');



		if($movie_value == '1' || $movie_value == '2'){

			$movie_list1 = $this->Common_model_new->movie_listing_category($movie_value,$pagecon['per_page'],$data['offset']);

		}else{

			$movie_list1 = $this->Common_model_new->movie_listing($pagecon['per_page'],$data['offset']);

		}

		if (!empty($movie_list1['rows'])) {

			$data['movie_list'] = $movie_list1['rows'];

			$data['count_total'] = $movie_list1['num_rows'];

		} else {

			$data['movie_list'] = '';

			$data['count_total'] = '';

		}

		$this->load->view('ajax_movie_value', $data);

	}







	// public function add_movie()

	// {



	// 	//die('pweirpiwepripwe');



	// 	$data['menuactive'] = $this->uri->segment(1);

	// 	$data['category_name'] = $this->Common_model_new->getAllwhereorderby("cp_movie_category",array("movie_category_status" => 1),"movie_category_name","asc");





	//  //echo "<pre>";print_r( $data['category_name']);die;





	// 	$this->template->set('title', 'Add Movie');

	// 	$this->template->load('user_dashboard_layout', 'contents', 'add_movie',$data);

	// }



	public function image_upload1()

	{

		$config1['upload_path'] = 'uploads/movie/';

		$config1['allowed_types'] = 'mp4';



           //$data['max_size'] = '5000';

		$config1['encrypt_name'] = true;



		$this->load->library('upload', $config1);





		if ($this->upload->do_upload('picture')) {

			$attachment_data = array('upload_data' => $this->upload->data());

			$uploadfile = $attachment_data['upload_data']['file_name'];

			$return = array("code" => 1);

		}

		echo json_encode($return);

	}



	public function ajax_movie_add()

	{



		$checkLogin = $this->session->userdata('logged_in');



		if ($_FILES["picture"]["error"] > 0 && $_FILES["pictureFile"]["error"] > 0)

		{

			$return = array('code' => 2,'message' => 'File loading error!');

		}else{



			$config1['upload_path'] = 'uploads/movie/';

			$config1['allowed_types'] = 'mp4';



           //$data['max_size'] = '5000';

			$config1['encrypt_name'] = true;



			$this->load->library('upload', $config1);



			if ($this->upload->do_upload('picture')) {

				$attachment_data = array('upload_data' => $this->upload->data());

				$uploadfile = $attachment_data['upload_data']['file_name'];

			// $return = array('code' => 1,'message' => 'Successfully added');

			}else{

				$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));

			}





			$config2['upload_path'] = 'uploads/movie/image/';

			$config2['allowed_types'] = 'jpg|png|gif|jpeg';

           //$data['max_size'] = '5000';

			$config2['encrypt_name'] = true;



			$this->upload->initialize($config2);





			if ($this->upload->do_upload('pictureFile')) {

				$attachment_data_image = array('upload_data' => $this->upload->data());

				$picturefile = $attachment_data_image['upload_data']['file_name'];

      //  $return = array('code' => 1,'message' => 'Successfully added');



				/*For image resize*/

				$this->load->library('image_lib');

				$configer =  array(

					'image_library'   => 'gd2',

					'source_image'    =>  $attachment_data_image['upload_data']['full_path'],

					'maintain_ratio'  =>  TRUE,

					'width'           =>  150,

					'height'          =>  150,

				);

				$this->image_lib->clear();

				$this->image_lib->initialize($configer);

				$this->image_lib->resize();



			}else{

				$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));



			}





			if($return['code'] != 2){

				$array = array(

					'movie_title' => $this->input->post('movie_title'),

					'movie_desc' => $this->input->post('movie_description'),

					'movie_artist' => $this->input->post('movie_artist'),

          //'movie_type' => $this->input->post('movie_type'),

					'movie_file' => $uploadfile,

					'movie_image' =>  $picturefile,

					'user_id' => $checkLogin['id'],

					'category_id' => $this->input->post('movie_type'),

          // 'music_image' => $pictureFile,

					'movie_created' =>date('Y-m-d H:i:s')

				);













				$this->Common_model_new->insertData("movie",$array);



				$return = array('code' => 1,'message' => 'Successfully added');

			}

		}

		echo json_encode($return);

	}





	/*====================*/





	// public function edit_movie()

	// {

	// 	$data['menuactive'] = $this->uri->segment(1);

	// 	$music_id = $this->uri->segment("2");



	// 	$checkLogin = $this->session->userdata('logged_in');



	// 	$data['category_name'] = $this->Common_model_new->getAllwhereorderby("cp_movie_category",array("movie_category_status" => 1),"movie_category_name","asc");



	// 	$data['singleData'] = $this->Common_model_new->getsingle("movie",array("user_id" => $checkLogin['id'],"movie_id" => $music_id));



	// 	$this->template->set('title', 'Edit Movie');

	// 	$this->template->load('user_dashboard_layout', 'contents', 'edit_movie',$data);

	// }



	public function ajax_movie_edit()

	{

		$music_id = $this->input->post('movie_id');

		$checkLogin = $this->session->userdata('logged_in');



		$singleData = $this->Common_model_new->getsingle("movie",array("movie_id" => $music_id));





		$config1['upload_path'] = 'uploads/movie/';

		$config1['allowed_types'] = 'mp4';



           //$data['max_size'] = '5000';

		$config1['encrypt_name'] = true;



		$this->load->library('upload', $config1);



		if(!empty($_FILES["picture"]["name"])){

			if ($this->upload->do_upload('picture')) {

				$attachment_data = array('upload_data' => $this->upload->data());

				$uploadfile = $attachment_data['upload_data']['file_name'];

			// $return = array('code' => 1,'message' => 'Successfully added');

			}else{

				$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));

			}

		}



		$config2['upload_path'] = 'uploads/movie/image/';

		$config2['allowed_types'] = 'jpg|png|gif|jpeg';

           //$data['max_size'] = '5000';

		$config2['encrypt_name'] = true;



		$this->upload->initialize($config2);



		if(!empty($_FILES["pictureFile"]["name"])){

			if ($this->upload->do_upload('pictureFile')) {

				$attachment_data_image = array('upload_data' => $this->upload->data());

				$picturefile = $attachment_data_image['upload_data']['file_name'];

      //  $return = array('code' => 1,'message' => 'Successfully added');



				/*For image resize*/

				$this->load->library('image_lib');

				$configer =  array(

					'image_library'   => 'gd2',

					'source_image'    =>  $attachment_data_image['upload_data']['full_path'],

					'maintain_ratio'  =>  TRUE,

					'width'           =>  150,

					'height'          =>  150,

				);

				$this->image_lib->clear();

				$this->image_lib->initialize($configer);

				$this->image_lib->resize();





			}else{

				$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));



			}

		}



		if(!empty($picturefile)){

			$picfile = $picturefile;

		}else{

			$picfile = $singleData->movie_image;

		}



		if(!empty($uploadfile)){

			$upfile = $uploadfile;

		}else{

			$upfile = $singleData->movie_file;

		}



		if(!empty($upfile) && !empty($picfile)){

			if($return['code'] != 2){

				$array = array(

					'movie_title' => $this->input->post('movie_title'),

					'movie_desc' => $this->input->post('movie_description'),

					'movie_artist' => $this->input->post('movie_artist'),

          //'movie_type' => $this->input->post('movie_type'),

					'movie_file' => $upfile,

					'movie_image' =>  $picfile,

					'user_id' => $checkLogin['id'],

					'category_id' => $this->input->post('movie_type'),

          // 'music_image' => $pictureFile,

					'movie_created' =>date('Y-m-d H:i:s')

				);

				$this->Common_model_new->updateData("movie",$array,array('movie_id' => $this->input->post("movie_id")));



				$return = array('code' => 1,'message' => 'Successfully updated');

			}

		}





		echo json_encode($return);

	}









	// public function delete_movie()

	// {



	// 	$segment = $this->uri->segment("2");

	// 	$checkLogin = $this->session->userdata('logged_in');

	// 	$delete_movie = $this->input->post("delete_movie");



	// 	$image_name = $this->common_model_new->getAllwhereorderby('cp_movie_favorite', array('movie_id' => $delete_movie,'user_id'=>$checkLogin['id']),'movie_fav_id','DESC');



	// 	foreach ($image_name as $image) {

	// 		$this->common_model_new->delete("cp_movie_favorite", array('movie_fav_id'=> $image->movie_fav_id));

	// 	}



	// 	$movieDetail =$this->common_model_new->getsingle("movie",array("movie_id" => $delete_movie));



	// 	$movieImagePath=getcwd().'/uploads/movie/image/';

	// 	$movieImage=$movieDetail->movie_image;

	// 	unlink($movieImagePath.$movieImage);





	// 	$movieFilePath=getcwd().'/uploads/movie/';

	// 	$movieFile=$movieDetail->movie_file;

	// 	unlink($movieFilePath.$movieFile);



	// 	$this->common_model_new->delete('movie',array('movie_id' =>$delete_movie));



	// }







	public function view_allmovies($viewtype='')

	{



		$data['menuactive'] = $this->uri->segment(1);

		$checkLogin = $this->session->userdata('logged_in');



		if($viewtype=='popular'){





			$data['type']='popular';





			/*==============*/



			$play_movies  = $this->movie_model->getsingle("play_movies",array("user_id" => $this->session->userdata('logged_in')['id']));



			if(!empty($play_movies)){



				$data['popular_array'] = $this->movie_model->getAllthePopular();



				$playdata = array(

					'user_id'=>$this->session->userdata('logged_in')['id'],

					'playmovie_file'=>$data['popular_array'][0]->movie_file,

					'playmovie_image'=>$data['popular_array'][0]->movie_image,

					'playmovie_title'=>$data['popular_array'][0]->movie_title,

					'playmovie_desc'=>$data['popular_array'][0]->movie_desc,

					'playmovie_artist'=>$data['popular_array'][0]->movie_artist,

					'playmovie_views'=>$data['popular_array'][0]->movie_view,

					'movie_id'=>$data['popular_array'][0]->movie_id,



				);





				$this->movie_model->updateData("play_movies",$playdata,array('user_id'=>$this->session->userdata('logged_in')['id']));



			}else{



				$data['popular_array'] = $this->movie_model->getAllthePopular();



				$playdata = array(

					'user_id'=>$this->session->userdata('logged_in')['id'],

					'playmovie_file'=>$data['popular_array'][0]->movie_file,

					'playmovie_image'=>$data['popular_array'][0]->movie_image,

					'playmovie_title'=>$data['popular_array'][0]->movie_title,

					'playmovie_desc'=>$data['popular_array'][0]->movie_desc,

					'playmovie_artist'=>$data['popular_array'][0]->movie_artist,

					'playmovie_views'=>$data['popular_array'][0]->movie_view,

					'movie_id'=>$data['popular_array'][0]->movie_id,

				);



				$this->movie_model->insertData("play_movies",$playdata);

			}



			/*==============*/



			$data['popular_array'] = $this->movie_model->getAllthePopular();

			$data['PlayMovie']  = $this->movie_model->getAllPlayMovie();

			$this->template->set('title', 'All Movies');

			$this->template->load('user_dashboard_layout_movie2', 'contents', 'view_allmovies',$data);



		}elseif($viewtype=='recent'){

	 		//die('ioiuouoiu');

			$data['type']='recent';



			/*===========*/



			$play_movies  = $this->movie_model->getsingle("play_movies",array("user_id" => $this->session->userdata('logged_in')['id']));



			if(!empty($play_movies)){



				$data['recent_array']  = $this->movie_model->getAlltheRecent();



				$playdata = array(

					'user_id'=>$this->session->userdata('logged_in')['id'],

					'playmovie_file'=>$data['recent_array'][0]->movie_file,

					'playmovie_image'=>$data['recent_array'][0]->movie_image,

					'playmovie_title'=>$data['recent_array'][0]->movie_title,

					'playmovie_desc'=>$data['recent_array'][0]->movie_desc,

					'playmovie_artist'=>$data['recent_array'][0]->movie_artist,

					'playmovie_views'=>$data['recent_array'][0]->movie_view,

					'movie_id'=>$data['recent_array'][0]->movie_id,



				);





				$this->movie_model->updateData("play_movies",$playdata,array('user_id'=>$this->session->userdata('logged_in')['id']));



			}else{



				$data['recent_array']  = $this->movie_model->getAlltheRecent();



				$playdata = array(

					'user_id'=>$this->session->userdata('logged_in')['id'],

					'playmovie_file'=>$data['recent_array'][0]->movie_file,

					'playmovie_image'=>$data['recent_array'][0]->movie_image,

					'playmovie_title'=>$data['recent_array'][0]->movie_title,

					'playmovie_desc'=>$data['recent_array'][0]->movie_desc,

					'playmovie_artist'=>$data['recent_array'][0]->movie_artist,

					'playmovie_views'=>$data['recent_array'][0]->movie_view,

					'movie_id'=>$data['recent_array'][0]->movie_id,

				);



				$this->movie_model->insertData("play_movies",$playdata);

			}



			/*=============*/







			$data['recent_array']  = $this->movie_model->getAlltheRecent();

			$data['PlayMovie']  = $this->movie_model->getAllPlayMovie();

			$this->template->set('title', 'All Movies');

			$this->template->load('user_dashboard_layout_movie2', 'contents', 'view_allmovies',$data);





		}elseif($viewtype=='my'){



			$data['type']='my';





			/*===========*/



			$play_movies  = $this->movie_model->getsingle("play_movies",array("user_id" => $this->session->userdata('logged_in')['id']));



			if(!empty($play_movies)){



				$data['UserMovieData']  = $this->movie_model->getAlltheMyMovie();



				$playdata = array(

					'user_id'=>$this->session->userdata('logged_in')['id'],

					'playmovie_file'=>$data['UserMovieData'][0]->movie_file,

					'playmovie_image'=>$data['UserMovieData'][0]->movie_image,

					'playmovie_title'=>$data['UserMovieData'][0]->movie_title,

					'playmovie_desc'=>$data['UserMovieData'][0]->movie_desc,

					'playmovie_artist'=>$data['UserMovieData'][0]->movie_artist,

					'playmovie_views'=>$data['UserMovieData'][0]->movie_view,

					'movie_id'=>$data['UserMovieData'][0]->movie_id,



				);





				$this->movie_model->updateData("play_movies",$playdata,array('user_id'=>$this->session->userdata('logged_in')['id']));



			}else{



				$data['UserMovieData']  = $this->movie_model->getAlltheMyMovie();



				$playdata = array(

					'user_id'=>$this->session->userdata('logged_in')['id'],

					'playmovie_file'=>$data['UserMovieData'][0]->movie_file,

					'playmovie_image'=>$data['UserMovieData'][0]->movie_image,

					'playmovie_title'=>$data['UserMovieData'][0]->movie_title,

					'playmovie_desc'=>$data['UserMovieData'][0]->movie_desc,

					'playmovie_artist'=>$data['UserMovieData'][0]->movie_artist,

					'playmovie_views'=>$data['UserMovieData'][0]->movie_view,

					'movie_id'=>$data['UserMovieData'][0]->movie_id,

				);



				$this->movie_model->insertData("play_movies",$playdata);

			}



			/*=============*/











			$data['UserMovieData']  = $this->movie_model->getAlltheMyMovie();

			$data['PlayMovie']  = $this->movie_model->getAllPlayMovie();

			$this->template->set('title', 'All Movies');

			$this->template->load('user_dashboard_layout_movie2', 'contents', 'view_allmovies',$data);



		}elseif ($viewtype=='favorite'){



			$data['type']='favorite';





			/*===========*/



			$play_movies  = $this->movie_model->getsingle("play_movies",array("user_id" => $this->session->userdata('logged_in')['id']));



			if(!empty($play_movies)){



				$data['UserFavMovie']  = $this->movie_model->getAlltheMyFavMovie();



				$playdata = array(

					'user_id'=>$this->session->userdata('logged_in')['id'],

					'playmovie_file'=>$data['UserFavMovie'][0]->movie_file,

					'playmovie_image'=>$data['UserFavMovie'][0]->movie_image,

					'playmovie_title'=>$data['UserFavMovie'][0]->movie_title,

					'playmovie_desc'=>$data['UserFavMovie'][0]->movie_desc,

					'playmovie_artist'=>$data['UserFavMovie'][0]->movie_artist,

					'playmovie_views'=>$data['UserFavMovie'][0]->movie_view,

					'movie_id'=>$data['UserFavMovie'][0]->movie_id,



				);





				$this->movie_model->updateData("play_movies",$playdata,array('user_id'=>$this->session->userdata('logged_in')['id']));



			}else{



				$data['UserFavMovie']  = $this->movie_model->getAlltheMyFavMovie();



				$playdata = array(

					'user_id'=>$this->session->userdata('logged_in')['id'],

					'playmovie_file'=>$data['UserFavMovie'][0]->movie_file,

					'playmovie_image'=>$data['UserFavMovie'][0]->movie_image,

					'playmovie_title'=>$data['UserFavMovie'][0]->movie_title,

					'playmovie_desc'=>$data['UserFavMovie'][0]->movie_desc,

					'playmovie_artist'=>$data['UserFavMovie'][0]->movie_artist,

					'playmovie_views'=>$data['UserFavMovie'][0]->movie_view,

					'movie_id'=>$data['UserFavMovie'][0]->movie_id,

				);



				$this->movie_model->insertData("play_movies",$playdata);

			}



			/*=============*/













			$data['UserFavMovie']  = $this->movie_model->getAlltheMyFavMovie();

			$data['PlayMovie']  = $this->movie_model->getAllPlayMovie();

			$this->template->set('title', 'All Movies');

			$this->template->load('user_dashboard_layout_movie2', 'contents', 'view_allmovies',$data);

		}











	}



	public function Paythismovie($movie_id='')

	{







		$movie_id = $this->input->post('movie_id');



		$total_view = $this->movie_model->GetViewsCount($movie_id);



		$total = $total_view+1;



		$viewData=array(

			'movie_view'=>$total,

		);



		$this->movie_model->updateData("movie",$viewData,array('movie_id'=>$movie_id));

		$detail_movie  = $this->movie_model->getsingle("movie",array("movie_id" => $movie_id));







		$play_movies  = $this->movie_model->getsingle("play_movies",array("user_id" => $this->session->userdata('logged_in')['id']));





		if(!empty($play_movies)){



			$data=array(

				'user_id'=>$this->session->userdata('logged_in')['id'],

				'playmovie_file'=>$detail_movie->movie_file,

				'playmovie_image'=>$detail_movie->movie_image,

				'movie_id'=>$detail_movie->movie_id,

				'playmovie_title'=>$detail_movie->movie_title,

				'playmovie_desc'=>$detail_movie->movie_desc,

				'playmovie_artist'=>$detail_movie->movie_artist,

				'playmovie_views'=>$detail_movie->movie_view,

			);



			$this->movie_model->updateData("play_movies",$data,array('user_id'=>$this->session->userdata('logged_in')['id']));

		  //echo '1';



			$play_movies  = $this->movie_model->getsingle("play_movies",array("user_id" => $this->session->userdata('logged_in')['id']));



			$new_data['play_movie1'] = $play_movies;

		  //$new_data['play_movie1'] = '';



			$this->load->view('play_movie',$new_data);









		}else{

			$data=array(

				'user_id'=>$this->session->userdata('logged_in')['id'],

				'playmovie_file'=>$detail_movie->movie_file,

				'playmovie_image'=>$detail_movie->movie_image,

				'movie_id'=>$detail_movie->movie_id,

				'playmovie_title'=>$detail_movie->movie_title,

				'playmovie_desc'=>$detail_movie->movie_desc,

				'playmovie_artist'=>$detail_movie->movie_artist,

				'playmovie_views'=>$detail_movie->movie_view,

			);



			$this->movie_model->insertData("play_movies",$data);



			$play_movies  = $this->movie_model->getsingle("play_movies",array("user_id" => $this->session->userdata('logged_in')['id']));



			$new_data['play_movie1'] = $play_movies;

        //$new_data['play_movie1'] = '';

			$this->load->view('play_movie',$new_data);



		  //echo '1';





		}







	}







	public function Paymovie($movie_id='',$type='')

	{



		$type = $this->input->post('type');

		$movie_id = $this->input->post('movie_id');



		$total_view = $this->movie_model->GetViewsCount($movie_id);



		$total = $total_view+1;



		$viewData=array(

			'movie_view'=>$total,

		);



		$this->movie_model->updateData("movie",$viewData,array('movie_id'=>$movie_id));







		$detail_movie  = $this->movie_model->getsingle("movie",array("movie_id" => $movie_id));







		$play_movies  = $this->movie_model->getsingle("play_movies",array("user_id" => $this->session->userdata('logged_in')['id']));





		if(!empty($play_movies)){



			$data=array(

				'user_id'=>$this->session->userdata('logged_in')['id'],

				'playmovie_file'=>$detail_movie->movie_file,

				'playmovie_image'=>$detail_movie->movie_image,

				'movie_id'=>$detail_movie->movie_id,

				'playmovie_title'=>$detail_movie->movie_title,

				'playmovie_desc'=>$detail_movie->movie_desc,

				'playmovie_artist'=>$detail_movie->movie_artist,

				'playmovie_views'=>$detail_movie->movie_view,



			);



			$this->movie_model->updateData("play_movies",$data,array('user_id'=>$this->session->userdata('logged_in')['id']));

		  //echo '1';



			$play_movies  = $this->movie_model->getsingle("play_movies",array("user_id" => $this->session->userdata('logged_in')['id']));



			$new_data['play_movie1'] = $play_movies;





			if($type=='popular'){



				$new_data['type']='popular';

				$new_data['popular_array'] = $this->movie_model->getAllthePopular();

			}elseif ($type=='recent'){

				$new_data['type']='recent';

				$new_data['recent_array']  = $this->movie_model->getAlltheRecent();

			}elseif ($type=='my'){

				$new_data['type']='my';

				$new_data['UserMovieData']  = $this->movie_model->getAlltheMyMovie();



			}elseif ($type=='favorite'){

				$new_data['type']='favorite';

				$new_data['UserFavMovie']  = $this->movie_model->getAlltheMyFavMovie();



			}else{

				$category_id = base64_decode($type);



				$new_data['MoviesBycategories']= $this->movie_model->getAllwhereorderby("movie",array("category_id" =>$category_id),"movie_id","DESC");





			}





			$this->load->view('play_moviebycategory',$new_data);





		}else{

			$data=array(

				'user_id'=>$this->session->userdata('logged_in')['id'],

				'playmovie_file'=>$detail_movie->movie_file,

				'playmovie_image'=>$detail_movie->movie_image,

				'movie_id'=>$detail_movie->movie_id,

				'playmovie_title'=>$detail_movie->movie_title,

				'playmovie_desc'=>$detail_movie->movie_desc,

				'playmovie_artist'=>$detail_movie->movie_artist,

				'playmovie_views'=>$detail_movie->movie_view,

			);



			$this->movie_model->insertData("play_movies",$data);



			$play_movies  = $this->movie_model->getsingle("play_movies",array("user_id" => $this->session->userdata('logged_in')['id']));



			$new_data['play_movie1'] = $play_movies;





			if($type=='popular'){



				$new_data['type']='popular';

				$new_data['popular_array'] = $this->movie_model->getAllthePopular();

			}elseif ($type=='recent'){

				$new_data['type']='recent';

				$new_data['recent_array']  = $this->movie_model->getAlltheRecent();

			}elseif ($type=='my'){

				$new_data['type']='my';

				$new_data['UserMovieData']  = $this->movie_model->getAlltheMyMovie();



			}elseif ($type=='favorite'){

				$new_data['type']='favorite';

				$new_data['UserFavMovie']  = $this->movie_model->getAlltheMyFavMovie();



			}else{

				$category_id = base64_decode($type);



				$new_data['MoviesBycategories']= $this->movie_model->getAllwhereorderby("movie",array("category_id" =>$category_id),"movie_id","DESC");





			}





			$this->load->view('play_moviebycategory',$new_data);



		  //echo '1';





		}







	}





	public function download_movie()

	{

		$movie_id =  $this->uri->segment(4);

		//$music_id = $this->input->post("music_id");

		$this->load->helper('download');

		$movie = $this->movie_model->getsingle("movie",array("movie_id" => $movie_id));



		$mp4 = base_url().'uploads/movie/'.$movie->movie_file;





		$fileName = $movie->movie_file;

		$path = 'uploads/movie/';

		$file = $path.$fileName;



		force_download($file, NULL);





	}







	public function view_All_Categories()

	{



		$data['menuactive'] = $this->uri->segment(1);



		$checkLogin = $this->session->userdata('logged_in');



		$data['category_array'] = $this->movie_model->getAlltheCategory();

		if(!empty($category_array)){
			$data['category_array'] =$category_array;
		}else{
			$data['category_array']=array();
		}


		$PlayMovie  = $this->movie_model->getAllPlayMovie();

		if(!empty($PlayMovie)){
			$data['PlayMovie'] =$PlayMovie;
		}else{
			$data['PlayMovie']=array();
		}

		$this->template->set('title', 'All Movies');

		$this->template->load('user_dashboard_layout_movie2', 'contents', 'view_allMovies_categories',$data);



	}







	public function movies_by_categories($category_id='')

	{

		$category_id	= base64_decode($category_id);



		$data['menuactive'] = $this->uri->segment(1);



		$checkLogin = $this->session->userdata('logged_in');





		/*===========*/



		$play_movies  = $this->movie_model->getsingle("play_movies",array("user_id" => $this->session->userdata('logged_in')['id']));



		if(!empty($play_movies)){



			$data['MoviesBycategories']= $this->movie_model->getAllwhereorderby("movie",array("category_id" =>$category_id),"movie_id","DESC");





			$playdata = array(

				'user_id'=>$this->session->userdata('logged_in')['id'],

				'playmovie_file'=>$data['MoviesBycategories'][0]->movie_file,

				'playmovie_image'=>$data['MoviesBycategories'][0]->movie_image,

				'playmovie_title'=>$data['MoviesBycategories'][0]->movie_title,

				'playmovie_desc'=>$data['MoviesBycategories'][0]->movie_desc,

				'playmovie_artist'=>$data['MoviesBycategories'][0]->movie_artist,

				'playmovie_views'=>$data['MoviesBycategories'][0]->movie_view,

				'movie_id'=>$data['MoviesBycategories'][0]->movie_id,



			);





			$this->movie_model->updateData("play_movies",$playdata,array('user_id'=>$this->session->userdata('logged_in')['id']));



		}else{



			$data['MoviesBycategories']= $this->movie_model->getAllwhereorderby("movie",array("category_id" =>$category_id),"movie_id","DESC");



			$playdata = array(

				'user_id'=>$this->session->userdata('logged_in')['id'],

				'playmovie_file'=>$data['MoviesBycategories'][0]->movie_file,

				'playmovie_image'=>$data['MoviesBycategories'][0]->movie_image,

				'playmovie_title'=>$data['MoviesBycategories'][0]->movie_title,

				'playmovie_desc'=>$data['MoviesBycategories'][0]->movie_desc,

				'playmovie_artist'=>$data['MoviesBycategories'][0]->movie_artist,

				'playmovie_views'=>$data['MoviesBycategories'][0]->movie_view,

				'movie_id'=>$data['MoviesBycategories'][0]->movie_id,

			);



			$this->movie_model->insertData("play_movies",$playdata);

		}



		/*=============*/







		$data['MoviesBycategories']= $this->movie_model->getAllwhereorderby("movie",array("category_id" =>$category_id),"movie_id","DESC");



		$data['PlayMovie']  = $this->movie_model->getAllPlayMovie();

		$this->template->set('title', 'All Movies');

		$this->template->load('user_dashboard_layout_movie2', 'contents', 'view_allmovies',$data);



	}







	public function search_newmovie_list()

	{

		$data['menuactive'] = $this->uri->segment(1);

		$checkLogin = $this->session->userdata('logged_in');

		$movie_search = str_replace('%20', ' ', $this->uri->segment("2"));

		$data['movie_search'] = $movie_search;



		$data['movie_banner'] = $this->movie_model->getAllBanner();

		$data['popular_array'] = $this->movie_model->getAllPopularlike($movie_search);

		$data['recent_array']  = $this->movie_model->getAllRecentlike($movie_search);

		$data['category_array'] = $this->movie_model->getAllCategorylike($movie_search);

		$data['mymovie'] = $this->movie_model->getAllMyMovielike($movie_search,$checkLogin['id']);

		$data['UserFavMovie']  = $this->movie_model->getfavlimitsearch($movie_search,$checkLogin['id']);



		$data['PlayMovie']  = $this->movie_model->getAllPlayMovie();



		$data['category_array_limit'] = $this->movie_model->getAllCategorylikeli($movie_search);





		$data['UserFavMovieli']  = $this->Common_model_new->getfavlimitsearchli($movie_search,$checkLogin['id']);





		$this->template->set('title', 'All Movie');

		$this->template->load('user_dashboard_layout_movie1', 'contents', 'movie_combine_search',$data);

	}



	public function playMov($movie_id='')

	{







		$movie_id = $this->input->post('movie_id');

		$detail_movie  = $this->movie_model->getsingle("movie",array("movie_id" => $movie_id));







		$play_movies  = $this->movie_model->getsingle("play_movies",array("user_id" => $this->session->userdata('logged_in')['id']));





		if(!empty($play_movies)){



			$data=array(

				'user_id'=>$this->session->userdata('logged_in')['id'],

				'playmovie_file'=>$detail_movie->movie_file,

				'playmovie_image'=>$detail_movie->movie_image,

				'movie_id'=>$detail_movie->movie_id,

				'playmovie_title'=>$detail_movie->movie_title,

				'playmovie_desc'=>$detail_movie->movie_desc,

				'playmovie_artist'=>$detail_movie->movie_artist,

			);



			$this->movie_model->updateData("play_movies",$data,array('user_id'=>$this->session->userdata('logged_in')['id']));

		  //echo '1';



			$play_movies  = $this->movie_model->getsingle("play_movies",array("user_id" => $this->session->userdata('logged_in')['id']));



			$new_data['play_movie1'] = $play_movies;





			$this->load->view('getinfo_play_movie',$new_data);









		}else{

			$data=array(

				'user_id'=>$this->session->userdata('logged_in')['id'],

				'playmovie_file'=>$detail_movie->movie_file,

				'playmovie_image'=>$detail_movie->movie_image,

				'movie_id'=>$detail_movie->movie_id,

				'playmovie_title'=>$detail_movie->movie_title,

				'playmovie_desc'=>$detail_movie->movie_desc,

				'playmovie_artist'=>$detail_movie->movie_artist,

			);



			$this->movie_model->insertData("play_movies",$data);



			$play_movies  = $this->movie_model->getsingle("play_movies",array("user_id" => $this->session->userdata('logged_in')['id']));



			$new_data['play_movie1'] = $play_movies;

			$this->load->view('getinfo_play_movie',$new_data);



		  //echo '1';





		}







	}



	public function getinfo_movie()

	{

		$data['menuactive'] = $this->uri->segment(1);

		$checkLogin = $this->session->userdata('logged_in');

		$segment = base64_decode($this->uri->segment("2"));



		$play_movie = $this->Common_model_new->getsingle("movie",array("movie_id" => $segment));

		

		$check_play_movie = $this->Common_model_new->getsingle("play_movies",array("user_id" => $checkLogin['id']));

		

		$data['like_movie'] = $check_play_movie;

		

		$data_play_movie=array(

			'user_id'=>$this->session->userdata('logged_in')['id'],

			'playmovie_file'=>$play_movie->movie_file,

			'playmovie_image'=>$play_movie->movie_image,

			'movie_id'=>$play_movie->movie_id,

			'playmovie_title'=>$play_movie->movie_title,

			'playmovie_desc'=>$play_movie->movie_desc,

			'playmovie_artist'=>$play_movie->movie_artist,

			'playmovie_views'=>$play_movie->movie_view,

		);

		

		if(!empty($check_play_movie)){ 

			$this->Common_model_new->updateData('play_movies',$data_play_movie,array("user_id" => $checkLogin['id']));

		}else{

			$this->Common_model_new->insertData('play_movies',$data_play_movie);

		}



		



		$data['movie_id'] = $segment;





		/*for update views*/

		$type = $this->input->post('type');

		$movie_id =  $segment;   //$this->input->post('movie_id');



		$total_view = $this->movie_model->GetViewsCount($movie_id);



		$total = $total_view+1;



		$viewData=array(

			'movie_view'=>$total,

		);



		$this->movie_model->updateData("movie",$viewData,array('movie_id'=>$movie_id));



		/*for update views end*/





		$data['getinfo_movie'] = $this->movie_model->getsingle("movie",array("movie_id" => $segment));



		$data['check_info_movie'] = $this->common_model_new->getsingle("cp_movie_favorite",array("movie_id" => $segment,"user_id" => $checkLogin['id']));



		//$data['like_movie'] = $this->Common_model_new->jointwotablenn("movie", "category_id", "cp_movie_category", "movie_category_id",array("cp_movie_category.movie_category_id" => $data['getinfo_movie']->category_id,"cp_movie_category.user_id"),"*","cp_movie_category.movie_category_id","desc");

		

		

		$this->template->set('title', 'Detail Movie');

		$this->template->load('user_dashboard_layout_movie1', 'contents', 'getinfo_movie',$data);

	}



	public function all_movie_search($viewtype='')

	{

		$data['menuactive'] = $this->uri->segment(1);

		$checkLogin = $this->session->userdata('logged_in');



		$segment = $this->uri->segment("2");

		$segmentsec = $this->uri->segment("3");

		$segment2 = str_replace("%20"," ",$segmentsec);

		$data['segment'] = $segment;

		$data['segment2'] = $segment2;



		if($viewtype=='popular'){





			$data['type']='popular';

			$data['popular_array'] = $this->movie_model->getAllthePopularlike($segment2);



			$data['PlayMovie']  = $this->movie_model->getAllPlayMovie();

			$this->template->set('title', 'All Movies');

			$this->template->load('user_dashboard_layout_movie2', 'contents', 'view_allmovies_search',$data);



		}elseif($viewtype=='recent'){

	 		//die('ioiuouoiu');

			$data['type']='recent';

			$data['recent_array']  = $this->movie_model->getAlltheRecentlike($segment2);

			$data['PlayMovie']  = $this->movie_model->getAllPlayMovie();

			$this->template->set('title', 'All Movies');

			$this->template->load('user_dashboard_layout_movie2', 'contents', 'view_allmovies_search',$data);





		}elseif($viewtype=='my'){



			$data['type']='my';

			$data['UserMovieData']  = $this->movie_model->getAlltheMyMovielike($segment2,$checkLogin['id']);

			$data['PlayMovie']  = $this->movie_model->getAllPlayMovie();

			$this->template->set('title', 'All Movies');

			$this->template->load('user_dashboard_layout_movie2', 'contents', 'view_allmovies_search',$data);



		}elseif ($viewtype=='favorite'){



			$data['type']='favorite';

			$data['UserFavMovie']  = $this->movie_model->getAlltheMyFavMovielike($segment2,$checkLogin['id']);

			$data['PlayMovie']  = $this->movie_model->getAllPlayMovie();

			$this->template->set('title', 'All Movies');

			$this->template->load('user_dashboard_layout_movie2', 'contents', 'view_allmovies_search',$data);

		}

	}



	public function new_movies_by_categories()

	{

		$data['menuactive'] = $this->uri->segment(1);

		$like_data = $this->uri->segment("4");





		$data['movie_search'] = $like_data;



		$data['category_search'] = $this->Common_model_new->getsearchdatamoviecat($like_data);



		$this->template->set('title', 'All Movies');

		$this->template->load('user_dashboard_layout_movie2', 'contents', 'view_allMovies_categories_search',$data);

	}



	public function favourite_movie()

	{

		$movie_id = $this->input->post("movie_id");

		$checkLogin = $this->session->userdata('logged_in');

		$check_info_music = $this->common_model_new->getsingle("cp_movie_favorite",array("movie_id" => $movie_id,"user_id" => $checkLogin['id']));



		if(!empty($check_info_music)){

			$this->Common_model_new->delete("cp_movie_favorite",array("movie_id" => $movie_id,"user_id" => $checkLogin['id']));

			echo '0';

		}else{

			$array = array("user_id" => $checkLogin["id"],"movie_id" => $movie_id,"fav" => 1,"movie_fav_created" => date("Y-m-d H:i:s"));

			$this->Common_model_new->insertData("cp_movie_favorite",$array);

			echo '1';

		}



	}

	public function Movie_dashboard(){
    $data['menuactive'] = '';
    $checkLogin = $this->session->userdata('logged_in');
$user_id = $checkLogin['id'];
    if(!empty($checkLogin))
    {
      $data['menuactive'] = $this->uri->segment(1);
     
      unset($_POST);
      
      $data['app_list'] = $this->common_model_new->getAllwhere("app_list",array('status' => 1,'use_for'=>"Movie"));
	  $data['movieArray'] = $this->Common_model_new->getAllwhere("setting_butns",array("btnname" =>'Movies',"user_id"=>$user_id));

      $data['Movie_list'] = $this->common_model_new->getAllwhere("cp_movie",array('user_id'=>$this->session->userdata('logged_in')['id']));
      $this->template->set('title', 'Movie');

      $this->template->load('new_user_dashboard_layout', 'contents', 'movie', $data);
    } 
    else{

      redirect('login');
  
    } 
  }

  public function add_Movie()
  {
    if(count($_POST)>0){
        if($_POST['type']==1){
      
        
        $array =array(
           'user_id' => $this->session->userdata('logged_in')['id'],
           'Movie_type' => $this->input->post('Movie_type'),
           'button_name' => $this->input->post('button_name'),
           'url'=>$this->input->post('url'),
           'app_name' => $this->input->post('app_name'),
           'created' => date('Y-m-d H:i:s'));
       }

       $check_movie = $this->Common_model_new->getsingle("cp_movie",array("button_name" => $this->input->post('button_name'),"user_id" => $this->session->userdata('logged_in')['id']));
      if(!empty($check_movie)){
        unset($_POST);
        echo json_encode(array("statusCode"=>200,"msg"=>"movie already added"));
        die();
      }
       $insert = $this->movie_model->insertData('cp_movie',$array);
       if($insert){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        echo json_encode(array("statusCode"=>200,"msg"=>"Movie added successfully"));
        die();
      }

    }
  }

  public function edit_Movie()
  {
    if(count($_POST)>0){
        if($_POST['type']==2){
        $Movie_id = $this->input->post('id');
        
        $array =array(
           'user_id' => $this->session->userdata('logged_in')['id'],
           'Movie_type' => $this->input->post('Movie_type'),
           'button_name' => $this->input->post('button_name'),
           'url'=>$this->input->post('url'),
           'app_name' => $this->input->post('app_name'),
           'created' => date('Y-m-d H:i:s'));
       }

        $check_movie = $this->Common_model_new->allexcludeone("cp_movie",array("button_name" => $this->input->post('button_name'),"user_id" => $this->session->userdata('logged_in')['id']),$Movie_id);
      if(!empty($check_movie)){
        unset($_POST);
        echo json_encode(array("statusCode"=>200,"msg"=>"movie already added"));
        die();
      }

       $update = $this->movie_model->updateData('cp_movie',$array,array('id' => $Movie_id));
       if($update){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        echo json_encode(array("statusCode"=>200,"msg"=>"Movie updated successfully"));
        die();
      }

    }
  }

  public function delete_Movie()
  {
    if(count($_POST)>0){
        if($_POST['type']==3){
        $Movie_id = $this->input->post('id');

       $update = $this->movie_model->delete('cp_movie',array('id' => $Movie_id));
       if($update){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        echo json_encode(array("statusCode"=>200,"msg"=>"Movie deleted successfully"));
        die();
      }

    }
  }
}



}