<aside class="right-side doc_cat_cls">

	<section class="content-header no-margin">

		<h1 class="text-center"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Doctors List 

			<div  class="back_reminder"><a href="javascript:window.history.back()" class="btn btn-danger">Back</a></div>

			<a href="javascript:void(0)" class="search-open"><i class="fa fa-search"></i></a>

		</h1>

		<div class="searching">			

			<div class="search-inline">

				<form method="post" action="" name="doctor_cat_search" id="doctor_cat_search">

					<input type="text" class="form-control" placeholder="Doctor Name" name="doctor_cat" id="doctor_cat">

					<input type="hidden" name="hidden_cat_id" id="hidden_cat_id" value="<?php echo $this->uri->segment(2); ?>" />

					<button type="submit" id="search_sub"><i class="fa fa-search"></i></button>

					<a href="javascript:void(0)" class="search-close">

						<i class="fa fa-times"></i>

					</a>

				</form>

			</div>

		</div>                  		

	</section>

	<section class="doc_cate">

		<div class="doc_cateMidd doc_listMidd">

			<div class="row" id="main-cntdiv">

				

				<?php  if($alldoctors){ 	foreach ($alldoctors as $key => $value) {

					?>

					<div class="col-lg-3 col-sm-6 col-md-3 col-xs-12">

						<article class="themetechmount-box themetechmount-box-team themetechmount-teambox-view-overlay">

							<div class="themetechmount-post-item">

								<div class="themetechmount-team-image-box">

									<span class="themetechmount-item-thumbnail">

										<span class="themetechmount-item-thumbnail-inner">



											<?php if(!empty($value['image'])){ ?>



												<img src="<?php echo base_url();?>uploads/doctor_category/<?php echo $value['image']?>" class="attachment-themetechmount-img-teamoverlay size-themetechmount-img-teamoverlay wp-post-image" alt="">

											<?php }else{ ?>

												<img src="<?php echo base_url();?>uploads/doctor_category/doctor_default.png" class="attachment-themetechmount-img-teamoverlay size-themetechmount-img-teamoverlay wp-post-image" alt="">

											<?php } ?>

										</span>

									</span>

									<div class="themetechmount-overlay">

										<div class="themetechmount-icon-box themetechmount-box-link">

										</div>

									</div>

								</div>

								<div class="themetechmount-box-content">

									<div class="themetechmount-box-title">

										<h4>

											<a href="<?php echo base_url()?>view_doctor/<?php echo $value['id'];  ?>"><?php 	echo $value['doctor_name']; ?></a>

										</h4>

									</div>

									<div class="themetechmount-team-position">

										<?php 	

								/*foreach (explode(",", $value['Category_name']) as $cat_name) {

									$newcat = $cat_name.',';*/

									?>



									<?php 



									echo $value['Category_name'];

								//echo $cat_name;

								//echo ",";

									

									?>



									<?php 	

								//} 

									?>

								</div>

							</div>

						</div>

					</article>

				</div>

			<?php 	}} else{ ?>

				<div class="photo-list-empty">

					<span class="blnk_photo"><i class="fa fa-picture-o" aria-hidden="true"></i></span>

					<h4>Doctors not found</h4>

				</div>



			<?php } ?>

			

		</div>

		

	</section>

</aside>

<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>



<script src="<?php echo base_url();?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>



<script>

	

	

	$(document).on('click','#search_sub',function(e){

		e.preventDefault();

		var category_id = "<?php echo $this->uri->segment('2'); ?>";

		var doctor_name = $("#doctor_cat").val();

		$.ajax

		({

			url: "<?php echo base_url(); ?>Pages/search_doctor_category",

			type: "POST",             

			data: 'category_id='+category_id+'&doctor_name='+doctor_name, 

			dataType: "json",			        

			success: function(response)   

			{

				$('#main-cntdiv').html(response.list);

			}

			

		});

		

	}); 

</script>

<script>

	var sp = document.querySelector('.search-open');

	var searchbar = document.querySelector('.search-inline');

	var shclose = document.querySelector('.search-close');

	function changeClass() {

		searchbar.classList.add('search-visible');

	}

	function closesearch() {

		searchbar.classList.remove('search-visible');

	}

	sp.addEventListener('click', changeClass);

	shclose.addEventListener('click', closesearch);

	

</script>



