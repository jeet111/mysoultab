<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <?php if($this->uri->segment(1)==''){ ?>
    <title>Best Easy To Use Tablet For Seniors</title>
  <?php }elseif ($this->uri->segment(1)=='pricing') { ?> 
    <title>SoulTab Details & Pricing</title>
  <?php }elseif ($this->uri->segment(1)=='features') { ?>
    <title>SoulTab Senior Tablet Features</title>
  <?php }elseif ($this->uri->segment(1)=='faqs') { ?>
    <title>FAQ's About SoulTab Senior Tablet</title>
  <?php }elseif ($this->uri->segment(1)=='member-stories') { ?>
    <title>Best Tablets For Seniors | Member Stories</title>
  <?php }elseif ($this->uri->segment(1)=='about-us') { ?>
    <title>About SoulTab</title>
  <?php }else { ?>
    <title>Contact Soultab Support</title>
  <?php } ?>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/bootstrap.min.css">
  <!-- <link rel="stylesheet" type="text/css" href="assets/styles/bootstrap-4.1.2/bootstrap.min.css"> -->
  <!-- <link rel="stylesheet" type="text/css" href="css/font-awesome.css"> -->
  <link href="<?php echo base_url();?>assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- <link rel="stylesheet" type="text/css" href="css/master.css"> -->
  <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/main_styles.css"> -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/master.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/new_changes.css">
  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/owl.carousel.css">
  <!-- <link rel="stylesheet" type="text/css" href="assets/plugins/OwlCarousel2-2.3.4/owl.carousel.css"> -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/owl.theme.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/animate.css">
  <link href="<?php echo base_url();?>assets/user_dashboard/img/logo.png" rel="icon">
  <link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/responsive.css" rel="stylesheet">

  <!-- <link rel="stylesheet" type="text/css" href="assets/plugins/OwlCarousel2-2.3.4/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/OwlCarousel2-2.3.4/animate.css"> -->      
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
  </head>
  <body class="home_page ">
    <article class="slider-arti">    
      <header id="main_header_id">
        <div class="header-main ">
          <nav class="navbar navbar-default  navbar-static-top menu-header " id="nav-top">
            <div class="container-fluid he_cont">
              <div class="navbar-header logo_s">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url();?>assets/images/logo.png"></a>
              </div>          
              <div class="menu_s collapse navbar-collapse text-right" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav main-nav">
                  <?php $menuactive=''; ?>
                  <li <?php if($this->uri->segment(1)==''){ $active='active';}else{ $active=''; } ?> class="<?php echo $active; ?>"><a href="<?php echo base_url(); ?>">Home</a></li>
                  <!-- <li><a href="#">Product Details</a></li> -->
                  <li <?php if($this->uri->segment(1)=='pricing'){ $active='active';}else{ $active=''; } ?> class="<?php echo $active; ?>"><a href="<?php echo base_url(); ?>pricing">Pricing</a></li>
                  <li <?php if($this->uri->segment(1)=='features'){ $active='active';}else{ $active=''; } ?> class="<?php echo $active; ?>"><a href="<?php echo base_url(); ?>features">Features</a></li>
                  <li <?php if($this->uri->segment(1)=='about-us'){ $active='active';}else{ $active=''; } ?> class="<?php echo $active; ?>"><a href="<?php echo base_url(); ?>about-us">About Us</a></li>
                  <li <?php if($this->uri->segment(1)=='faqs'){ $active='active';}else{ $active=''; } ?> class="<?php echo $active; ?>"><a href="<?php echo base_url(); ?>faqs">FAQ's</a></li>
                  <li <?php if($this->uri->segment(1)=='member-stories'){ $active='active';}else{ $active=''; } ?> class="<?php echo $active; ?>"><a href="<?php echo base_url(); ?>member-stories">Member memories</a></li>
                  <li <?php if($this->uri->segment(1)=='login'){ $active='active';}else{ $active=''; } ?> class="<?php echo $active; ?>"><a href="<?php echo base_url(); ?>login">Login</a></li>
                </ul>
              </div>
              <div class="top_lange">
               <a href="<?php echo base_url(); ?>buynow">BUY Now</a>
             </div>
           </div>
         </nav>
       </div>
     </header>  