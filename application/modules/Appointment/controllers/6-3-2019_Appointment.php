<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment extends MX_Controller {

  public function __construct() {
      parent:: __construct();
      $this->load->library('session');
      $this->load->library('form_validation'); 
       $this->form_validation->set_error_delimiters('<div class="error">', '</div>');    
	  $this->load->model('Common_model_new');
      $this->load->helper(array('common_helper'));
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));      
  }
  
  public function appointment_list()
  {
	  if($this->session->userdata('logged_in')){
		   $data = array();
      $data['menuactive'] = $this->uri->segment(1);
	 // $data['reminder_list'] = $this->Common_model_new->getAllwhereorderby("cp_reminder",array("user_id" => $this->session->userdata('logged_in')['id']),'reminder_id','desc');  
	  
	  
	  $this->template->set('title', 'Appointment');
      $this->template->load('user_dashboard_layout', 'contents', 'appointment_list', $data);
	   }else{
		   redirect('login');
	   }
  }


  public function edit_appointment($id)
  {

    if($this->session->userdata('logged_in')){
       $data = array();
      $data['menuactive'] = $this->uri->segment(1);
   
    
    $this->template->set('title', 'Appointment');
      $this->template->load('user_dashboard_layout', 'contents', 'edit_appointment', $data);
     }else{
       redirect('login');
     }
  }


}