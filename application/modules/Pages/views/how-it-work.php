	<div class="aboutMain">
		<div class="ban_box wow slideInDown animated">
			<div class="container">
				<div class="ben_hed">
					<h2>How It work</h2>
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					  	<li class="breadcrumb-item"><a href="#">Home</a></li>
					    <li class="breadcrumb-item active" aria-current="page">About Us</li>
					  </ol>
					</nav>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="main_about wow slideInDown animated">
				<h2 class="hd_services">How It works</h2>
			</div>
		</div>
		<section class="about-cont-main">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="acm-txt wow slideInLeft animated">
                        <div class="acm-head">
                            <h2>Find Caregiver</h2>
                        </div>
                        <div class="acm-txt-sub">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel enim feugiat gravida Phasellus.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel enim feugiat gravida. Phasellus velit risus, euismod a lacus consectetur  et.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel enim feugiat gravida. Phasellus velit risus, euismod a lacus et.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel enim feugiat gravida. Phasellus velit risus.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="about-cont-sub">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="about-img">
                        <img src="assets/images/about-2.jpg">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="acm-txt wow slideInRight animated">
                        <div class="acm-head">
                            <h2>Choose the best for you</h2>
                        </div>
                        <div class="acm-txt-sub">
                            <p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit:</strong></p>
                            <ul>
                                <li><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel enim feugiat gravida. Phasellus velit risus, euismod a lacus et.</p></li>
                                <li><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel enim feugiat gravida. Phasellus velit risus, euismod a lacus et.</p></li>
                                <li><p>We commit to delivering results in a reliable, cost effective and timely manner.</p></li>
                                <li><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel enim feugiat gravida. Phasellus velit risus.</p></li>
                                <li><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel.</p></li>
                                <li><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel enim feugiat gravida. Phasellus velit risus, euismod a lacus et.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel enim feugiat gravida.</p></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="firm-value wow fadeInDown animated">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="acm-txt">
                        <div class="acm-head">
                            <h2><strong>FIRM</strong>VALUES</h2>
                        </div>
                        <div class="acm-txt-sub">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel enim feugiat gravida:</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="fv-cont">
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="fv-sub">
                                    <h3>Excellence</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel enim feugiat gravida. Phasellus velit</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="fv-sub">
                                    <h3>Determination</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel enim feugiat gravida.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="fv-sub">
                                    <h3>Professionalism</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="fv-sub">
                                    <h3>Equality</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing. Pellentesque fermentum massa</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

	<div class="container">

		<div class="main_about wow slideInDown animated">
			<h2 class="hd_services">Everything you need to support you in<br>
			your efforts as a family caregiver</h2>

			<div class="row">
				<div class="col-md-3">						
					<div class="sc_services_item">
						<a href="#" class="icon_item"><i class="fa fa-wheelchair"></i></a>
						<div class="item_content">
							<h4 class="item_title"><a href="#">Home care</a></h4>
							<div class="item_description">
								<p>Solution for families that need an extra level of care</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3">						
					<div class="sc_services_item">
						<a href="#" class="icon_item"><i class="fa fa-file-text-o"></i></a>			
						<div class="item_content">
							<h4 class="item_title"><a href="#">Care Services</a></h4>
							<div class="item_description">
								<p>From companionship services to hospice care support</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3">						
					<div class="sc_services_item">
						<a href="#" class="icon_item"><i class="fa fa-heartbeat"></i></a>			
						<div class="item_content">
							<h4 class="item_title"><a href="#">Medical Escort</a></h4>
							<div class="item_description">
								<p>We provide transportation services you can rely on</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3">						
					<div class="sc_services_item">
						<a href="#" class="icon_item"><i class="fa fa-medkit"></i></a>			
						<div class="item_content">
							<h4 class="item_title"><a href="#">Caregiver Jobs</a></h4>
							<div class="item_description">
								<p>We are looking for caring individuals to join our teams</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="bx_ourMission">
		<div class="container">
			<div class="row">
				<div class="col-md-6 wow slideInLeft animated">	
					<div class="mission_main">
						<h2>Our Mission</h2>
						<p>At Kindly Care we believe dignity and respect are two of the most valuable commodities in the world. There is no higher praise for us than the smile of a happy patient, the thanks of a relieved client, or the revitalized embrace of an engaged resident.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="bx_ourTeam">
		<div class="container">
			<h2 class="hd_services wow slideInDown animated">Our Brilliant Team</h2>	

			<div class="row">
				<div class="col-md-3 wow slideInLeft animated">
					<div class="sc_team_item">
						<div class="sc_team_item_avatar">
							<img class="wp-post-image" alt="" src="assets/images/team1.jpg">
							<div class="sc_team_item_hover">
								<div class="sc_team_item_socials">
									<div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
										<div class="sc_socials_item">
											<a href="#" target="_blank" class="social_icons social_facebook"><i class="fa fa-facebook"></i></a>
										</div>
										<div class="sc_socials_item">
											<a href="#" target="_blank" class="social_icons social_twitter"><i class="fa fa-twitter"></i></a>
										</div>
										<div class="sc_socials_item">
											<a href="#" target="_blank" class="social_icons social_gplus"><i class="fa fa-google-plus"></i></a>
										</div>
										<div class="sc_socials_item">
											<a href="#" target="_blank" class="social_icons social_linkedin"><i class="fa fa-linkedin"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="sc_team_item_info">
							<h5 class="sc_team_item_title"><a href="http://kindlycare.ancorathemes.com/team/roger-golden/">Roger Golden</a></h5>
							<div class="sc_team_item_position">Manager</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 wow slideInLeft animated">
					<div class="sc_team_item">
						<div class="sc_team_item_avatar">
							<img class="wp-post-image" alt="" src="assets/images/team2.jpg">
							<div class="sc_team_item_hover">
								<div class="sc_team_item_socials">
									<div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
										<div class="sc_socials_item">
											<a href="#" target="_blank" class="social_icons social_facebook"><i class="fa fa-facebook"></i></a>
										</div>
										<div class="sc_socials_item">
											<a href="#" target="_blank" class="social_icons social_twitter"><i class="fa fa-twitter"></i></a>
										</div>
										<div class="sc_socials_item">
											<a href="#" target="_blank" class="social_icons social_gplus"><i class="fa fa-google-plus"></i></a>
										</div>
										<div class="sc_socials_item">
											<a href="#" target="_blank" class="social_icons social_linkedin"><i class="fa fa-linkedin"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="sc_team_item_info">
							<h5 class="sc_team_item_title"><a href="http://kindlycare.ancorathemes.com/team/roger-golden/">Jane French</a></h5>
							<div class="sc_team_item_position">Hostess</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 wow slideInRight animated">
					<div class="sc_team_item">
						<div class="sc_team_item_avatar">
							<img class="wp-post-image" alt="" src="assets/images/team3.jpg">
							<div class="sc_team_item_hover">
								<div class="sc_team_item_socials">
									<div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
										<div class="sc_socials_item">
											<a href="#" target="_blank" class="social_icons social_facebook"><i class="fa fa-facebook"></i></a>
										</div>
										<div class="sc_socials_item">
											<a href="#" target="_blank" class="social_icons social_twitter"><i class="fa fa-twitter"></i></a>
										</div>
										<div class="sc_socials_item">
											<a href="#" target="_blank" class="social_icons social_gplus"><i class="fa fa-google-plus"></i></a>
										</div>
										<div class="sc_socials_item">
											<a href="#" target="_blank" class="social_icons social_linkedin"><i class="fa fa-linkedin"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="sc_team_item_info">
							<h5 class="sc_team_item_title"><a href="http://kindlycare.ancorathemes.com/team/roger-golden/">Susan McDowell</a></h5>
							<div class="sc_team_item_position">Hostess</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 wow slideInRight animated">
					<div class="sc_team_item">
						<div class="sc_team_item_avatar">
							<img class="wp-post-image" alt="" src="assets/images/team4.jpg">
							<div class="sc_team_item_hover">
								<div class="sc_team_item_socials">
									<div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
										<div class="sc_socials_item">
											<a href="#" target="_blank" class="social_icons social_facebook"><i class="fa fa-facebook"></i></a>
										</div>
										<div class="sc_socials_item">
											<a href="#" target="_blank" class="social_icons social_twitter"><i class="fa fa-twitter"></i></a>
										</div>
										<div class="sc_socials_item">
											<a href="#" target="_blank" class="social_icons social_gplus"><i class="fa fa-google-plus"></i></a>
										</div>
										<div class="sc_socials_item">
											<a href="#" target="_blank" class="social_icons social_linkedin"><i class="fa fa-linkedin"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="sc_team_item_info">
							<h5 class="sc_team_item_title"><a href="http://kindlycare.ancorathemes.com/team/roger-golden/">Aleesha Floyd</a></h5>
							<div class="sc_team_item_position">Hostess</div>
						</div>
					</div>
				</div>
				<div class="sc_team_button"><a href="#" class="sc_button">View all members</a></div>
			</div>
		</div>
	</div>

	<div class="bx_ourGat wow slideInDown animated">
		<div class="container">
			<div class="main_get">
				<div class="action_info">
					<h2 class="action_title">We've been there. We understand.</h2>
					<div class="action_descr">Everything we do is driven by our universal mission: to enhance the lives of aging adults and their families.</div>
				</div>
				<div class="sc_call_to_action_buttons">
					<div class="sc_call_to_action_button">
						<a href="#" class="sc_button">Get FREE consultation now!</a>
					</div>
				</div>
			</div>
		</div>
	</div>