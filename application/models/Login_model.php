<?php



class Login_model extends CI_Model {



  public function __construct() {

    $this->userTbl = 'cp_users';

  }



  function login($email, $password)

  {  


   $this->db-> select('*');

   $this->db-> from('cp_users');

   $this->db-> where('email', $email);

   $this->db-> where('password', $password);

       //$this -> db -> where('userrole', '1');

       //$this -> db -> limit(1);

   $query = $this ->db->get();

   if($query -> num_rows() == 1)

   {

    return $query->result();

  }

  else

  {

   return false;

 }

}





function admin_login($email, $password)

{

   $this->db->select('*');

   $this->db->from('cp_users');

   $this->db->where("(email = '".$email."' AND password = '".$password."' AND user_role = '1') OR (email = '".$email."' AND password = '".$password."' AND user_role = '5')");



   /*$this->db->where('email', $email);

   $this->db->where('password', $password);

   $this->db->where('user_role', '1');

   $this->db->or_where('user_role','5');*/

   //$this -> db -> limit(1);

   $query = $this ->db->get();

   if($query -> num_rows() == 1)

   {

      return $query->result();

   }

   else

   {

      return false;

   }

}



function insertrecords($table,$data)

{

 $this->db->insert($table,$data);

 return $this->db->insert_id();

}



function getAllRecords($table)

{

  $query = $this->db->get($table);

  return $query->result_array();

}



function getAllRecordsById($table,$conditions)

{

  $query = $this->db->get_where($table,$conditions);

  return $query->result_array();

}



function getRecordsByOrdering($table,$column_name='',$order_by='DESC',$conditions)

{

  $query = $this->db->order_by($column_name, $order_by)->get_where($table, $conditions);

  return $query->result_array();

}



function getSingleRecordById($table,$conditions)

{

  $query = $this->db->get_where($table,$conditions);

  return $query->row_array();

}



function updateRecords($table, $post_data, $where_condition)

{

  $this->db->where($where_condition);

  $query = $this->db->update($table, $post_data);

  return $query;

}



function updateRecordsUser($table, $post_data, $where_condition)

{

  $this->db->where($where_condition);

  $query = $this->db->update($table, $post_data);



  $where = array('user_id'=>$where_condition['id']);

  $para = array("order_status"=>3);

  $this->db->update("gb_orders", $para, $where);



  $where_trip = array('traveler_id'=>$where_condition['id']);

  $para_trip = array("trip_status"=>3);

  $this->db->update("gb_traveler_trips", $para_trip, $where_trip);



  return $query;

}



function deleteRecords($table,$where_condition)

{

  $this->db->where($where_condition);

  return $this->db->delete($table);

}



  /*

  * get rows from the users table

  */

  function getRows($table,$params = array()){

    $this->db->select('*');

    $this->db->from($table);

    //fetch data by conditions

    //$this->db->where("status !=", 3);



    if(array_key_exists("conditions",$params)){

      foreach ($params['conditions'] as $key => $value) {

        $this->db->where($key,$value);

      }

    }



    if(array_key_exists("filter",$params)){

      foreach ($params['filter'] as $key => $value) {

        $this->db->like($key,$value);

      }

    }



    $order_mode = "DESC";

    if(array_key_exists("sorting",$params)){

      foreach ($params['sorting'] as $key => $value) {

        $this->db->order_by($value, $order_mode);

        $this->session->set_userdata('order_by',$value);

      }

    } else {

      //$this->db->order_by('create_user', 'DESC');

    }



    if(array_key_exists("id",$params)){

      $this->db->where('id',$params['id']);

      $query = $this->db->get();

      $result = $query->row_array();

    }else{

        //set start and limit

      if(array_key_exists("start",$params) && array_key_exists("limit",$params)){

        $this->db->limit($params['limit'],$params['start']);

      } elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){

        $this->db->limit($params['limit']);

      }

      $query = $this->db->get();

      if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){

        $result = $query->num_rows();

      }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){

        $result = ($query->num_rows() > 0)?$query->row_array():FALSE;

      }else{

        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

      }

    }

    //echo $this->db->last_query(); die();

    return $result;

  }



  /*

  * get single row from the table

  */

  function getRow($table,$where) {

    $this->db->select('*');

    $this->db->from($table);

    $this->db->where($where);

    $query = $this->db->get();

    return $query->row();

  }



  /*

  * activate the users

  */

  public function activateUser($str) {

    $this->db->select('*');

    $this->db->from($this->userTbl);

    $this->db->where('act_link', $str);

    $this->db->where('status', 1);

    $query = $this->db->get();

    $rowcount = $query->num_rows();

    if($rowcount >= 1) {

      return -101;

    } else {

      $Data = array('status'=> 1);

      $this->db->where('act_link', $str);

      $this->db->where('status', 0);

      $val =  $this->db->update($this->userTbl, $Data);

      $this->db->select('*');

      $this->db->from($this->userTbl);

      $this->db->where('act_link', $str);

      $this->db->where('status', 1);

      $query = $this->db->get();

      $rowcount = $query->num_rows();

      return $rowcount;

    }

  }



  /*

  * get rows from the rating table

  */

  function get_ratings() {

    $query="*,

    (SELECT gb_users.firstname FROM gb_users WHERE gb_rating.buyer_id=gb_users.id ) AS buyer_firstname,

    (SELECT gb_users.lastname FROM gb_users WHERE gb_rating.buyer_id=gb_users.id ) AS buyer_lastname,

    (SELECT gb_users.firstname FROM gb_users WHERE gb_rating.traveler_id=gb_users.id ) AS traveler_firstname,

    (SELECT gb_users.lastname FROM gb_users WHERE gb_rating.traveler_id=gb_users.id ) AS traveler_lastname";

    $query_run=$this->db->select($query);

    $query_run->from('gb_rating');

    $query_run->where('rating_status !=', 3);

    $query_run->order_by('r_id', 'DESC');

    //echo $this->db->last_query();die();

    $query = $query_run->get();

    $result = $query->result_array();

    return $result;

  }



  /*

  * get rows from the notification table

  */

  function get_notifications() {

    $this->db->select('noti.*, ord.o_order_number, user.firstname, user.lastname');

    $this->db->from('gb_notifications noti');

    $this->db->join('gb_orders ord', 'ord.o_id=noti.noti_orderid', 'left');

    $this->db->join('gb_users user', 'user.id=noti.noti_userid', 'left');

    $this->db->where('noti_status !=', 3);

    $this->db->group_by('noti.noti_orderid');

    $this->db->order_by('noti.created_at', 'DESC');

    $query = $this->db->get();

    if($query->num_rows() != 0) {

      return $query->result_array();

    } else {

      return false;

    }

  }



  /*

  * get rows from the notification table

  */

  function get_notificationsById($where) {

    $this->db->select('noti.*, ord.o_order_number, user.firstname, user.lastname');

    $this->db->from('gb_notifications noti');

    $this->db->join('gb_orders ord', 'ord.o_id=noti.noti_orderid', 'left');

    $this->db->join('gb_users user', 'user.id=noti.noti_userid', 'left');

    $this->db->where('noti_status !=', 3);

    $this->db->where($where);

    $this->db->order_by('noti.created_at', 'DESC');

    $query = $this->db->get();

    if($query->num_rows() != 0) {

      return $query->result_array();

    } else {

      return false;

    }

  }



  /*

  * get rows from the message table

  */

  function get_messages() {

    $query="*,

    (SELECT gb_users.firstname FROM gb_users WHERE gb_messages.sender_id=gb_users.id ) AS sender_firstname,

    (SELECT gb_users.lastname FROM gb_users WHERE gb_messages.sender_id=gb_users.id ) AS sender_lastname,

    (SELECT gb_users.firstname FROM gb_users WHERE gb_messages.receiver_id=gb_users.id ) AS receiver_firstname,

    (SELECT gb_users.lastname FROM gb_users WHERE gb_messages.receiver_id=gb_users.id ) AS receiver_lastname";

    $query_run=$this->db->select($query);

    $query_run->from('gb_messages');

    $query_run->where('message_status !=', 3);

    $query_run->order_by('id', 'DESC');

    //echo $this->db->last_query();die();

    $query = $query_run->get();

    $result = $query->result_array();

    return $result;

  }



  /*

  * get rows from the message table

  */

  function get_messagesById($where) {

    $query="*,

    (SELECT gb_users.firstname FROM gb_users WHERE gb_messages.sender_id=gb_users.id ) AS sender_firstname,

    (SELECT gb_users.lastname FROM gb_users WHERE gb_messages.sender_id=gb_users.id ) AS sender_lastname,

    (SELECT gb_users.firstname FROM gb_users WHERE gb_messages.receiver_id=gb_users.id ) AS receiver_firstname,

    (SELECT gb_users.lastname FROM gb_users WHERE gb_messages.receiver_id=gb_users.id ) AS receiver_lastname";

    $query_run=$this->db->select($query);

    $query_run->from('gb_messages');

    $query_run->where('message_status !=', 3);

    $query_run->where($where);

    $query_run->order_by('id', 'DESC');

    //echo $this->db->last_query();die();

    $query = $query_run->get();

    $result = $query->result_array();

    return $result;

  }



  /*

  * get rows from the rating table

  */

  function get_orders() {

    $query="*,

    (SELECT gb_users.firstname FROM gb_users WHERE gb_orders.user_id=gb_users.id ) AS buyer_firstname,

    (SELECT gb_users.lastname FROM gb_users WHERE gb_orders.user_id=gb_users.id ) AS buyer_lastname,

    (SELECT gb_users.firstname FROM gb_users WHERE gb_orders.traveler_id=gb_users.id ) AS traveler_firstname,

    (SELECT gb_users.lastname FROM gb_users WHERE gb_orders.traveler_id=gb_users.id ) AS traveler_lastname";

    $query_run=$this->db->select($query);

    $query_run->from('gb_orders');

    $query_run->where('order_status !=', 3);

    $query_run->order_by('o_id', 'DESC');

    //echo $this->db->last_query();die();

    $query = $query_run->get();

    $result = $query->result_array();

    return $result;

  }



  /*

  * get rows from the transaction table

  */

  function get_transactions() {

    $query="*,

    (SELECT gb_users.firstname FROM gb_users WHERE gb_transcations.buyer_id=gb_users.id ) AS buyer_firstname,

    (SELECT gb_users.lastname FROM gb_users WHERE gb_transcations.buyer_id=gb_users.id ) AS buyer_lastname,

    (SELECT gb_users.firstname FROM gb_users WHERE gb_transcations.traveler_id=gb_users.id ) AS traveler_firstname,

    (SELECT gb_users.lastname FROM gb_users WHERE gb_transcations.traveler_id=gb_users.id ) AS traveler_lastname,

    (SELECT gb_orders.o_order_number FROM gb_orders WHERE gb_transcations.order_id=gb_orders.o_id ) AS order_number";

    $query_run=$this->db->select($query);

    $query_run->from('gb_transcations');

    $query_run->where('payment_status !=', 3);

    $query_run->order_by('payment_date', 'DESC');

    //echo $this->db->last_query();die();

    $query = $query_run->get();

    $result = $query->result_array();

    return $result;

  }



  /*

  * get rows from the transaction table

  */

  function get_transactionsById($where) {

    $query="*,

    (SELECT gb_users.firstname FROM gb_users WHERE gb_transcations.buyer_id=gb_users.id ) AS buyer_firstname,

    (SELECT gb_users.lastname FROM gb_users WHERE gb_transcations.buyer_id=gb_users.id ) AS buyer_lastname,

    (SELECT gb_users.firstname FROM gb_users WHERE gb_transcations.traveler_id=gb_users.id ) AS traveler_firstname,

    (SELECT gb_users.lastname FROM gb_users WHERE gb_transcations.traveler_id=gb_users.id ) AS traveler_lastname,

    (SELECT gb_orders.o_order_number FROM gb_orders WHERE gb_transcations.order_id=gb_orders.o_id ) AS order_number";

    $query_run=$this->db->select($query);

    $query_run->from('gb_transcations');

    $query_run->where('payment_status !=', 3);

    $query_run->where($where);

    //echo $this->db->last_query();die();

    $query = $query_run->get();

    $result = $query->result_array();

    return $result;

  }



  /*

  * get row from the rating table

  */

  function getRatingById($where) {

    $query="*,

    (SELECT gb_users.firstname FROM gb_users WHERE gb_rating.buyer_id=gb_users.id ) AS buyer_firstname,

    (SELECT gb_users.lastname FROM gb_users WHERE gb_rating.buyer_id=gb_users.id ) AS buyer_lastname,

    (SELECT gb_users.firstname FROM gb_users WHERE gb_rating.traveler_id=gb_users.id ) AS traveler_firstname,

    (SELECT gb_users.lastname FROM gb_users WHERE gb_rating.traveler_id=gb_users.id ) AS traveler_lastname";

    $query_run=$this->db->select($query);

    $query_run->from('gb_rating');

    $query_run->where('rating_status !=', 3);

    $query_run->where($where);

    $query = $query_run->get();

    //echo $query_run->last_query();die();

    $result = $query->row_array();

    return $result;

  }



  /*

  * get rows from the traveler trip table

  */

  function get_trips() {

    $query="*,

    (SELECT gb_users.firstname FROM gb_users WHERE gb_traveler_trips.traveler_id=gb_users.id ) AS traveler_firstname,

    (SELECT gb_users.lastname FROM gb_users WHERE gb_traveler_trips.traveler_id=gb_users.id ) AS traveler_lastname,

    (SELECT gb_transport.transport_name FROM gb_transport WHERE gb_traveler_trips.travel_mode=gb_transport.transport_id ) AS transport_name,

    (SELECT gb_transport.transport_image FROM gb_transport WHERE gb_traveler_trips.travel_mode=gb_transport.transport_id ) AS transport_image";

    $query_run=$this->db->select($query);

    $query_run->from('gb_traveler_trips');

    $query_run->where('trip_status !=', 3);

    $query_run->order_by('trip_id', 'DESC');

    //echo $this->db->last_query();die();

    $query = $query_run->get();

    $result = $query->result_array();

    return $result;

  }



  /*

  * get row from the traveler trip table

  */

  function get_tripsById($where) {

    $query="*,

    (SELECT gb_users.firstname FROM gb_users WHERE gb_traveler_trips.traveler_id=gb_users.id ) AS traveler_firstname,

    (SELECT gb_users.lastname FROM gb_users WHERE gb_traveler_trips.traveler_id=gb_users.id ) AS traveler_lastname,

    (SELECT gb_transport.transport_name FROM gb_transport WHERE gb_traveler_trips.travel_mode=gb_transport.transport_id ) AS transport_name,

    (SELECT gb_transport.transport_image FROM gb_transport WHERE gb_traveler_trips.travel_mode=gb_transport.transport_id ) AS transport_image";

    $query_run=$this->db->select($query);

    $query_run->from('gb_traveler_trips');

    $query_run->where('trip_status !=', 3);

    $query_run->where($where);

    //echo $this->db->last_query();die();

    $query = $query_run->get();

    $result = $query->row_array();

    return $result;

  }



  /**

  *  GET ALL DATA FROM ADDRESS

  */

  function get_address_data($address){

    $address = str_replace(",", "", $address);

    $address = str_replace(" ", "+", $address);

    $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";

    $result = file_get_contents("$url");

    $json = json_decode($result);



    foreach ($json->results as $result) {

      foreach($result->address_components as $addressPart) {

        if ((in_array('locality', $addressPart->types)) && (in_array('political', $addressPart->types)))

          $city = $addressPart->long_name;

        else if ((in_array('administrative_area_level_1', $addressPart->types)) && (in_array('political', $addressPart->types)))

          $state = $addressPart->long_name;

        else if ((in_array('country', $addressPart->types)) && (in_array('political', $addressPart->types)))

          $country = $addressPart->long_name;

      }

    }



    if(($city != '') && ($state != '') && ($country != ''))

      $address = $city.', '.$state.', '.$country;

    else if (($city != '') && ($state != ''))

      $address = $city.', '.$state;

    else if (($state != '') && ($country != ''))

      $address = $state.', '.$country;

    else if ($country != '')

      $address = $country;



    return $address;

  }





  /*Created by 95 For Edit/update Doctor Category */

  function update_category($data='', $id='')

  {

    $this->db->where('dc_id',$id);

    $query = $this->db->update('cp_doctor_categories',$data);

    return $query;

  }



  function getwhereIns($cat_id)

  {



	  //$q=$this->db->query('SELECT * FROM `cp_doctor` WHERE '.$cat_id.' IN(`'. doctor_category_id .'`)');



   $this->db->select('*');



   $this->db->where("FIND_IN_SET('$cat_id',cp_doctor.doctor_category_id) !=", 0);

   $q = $this->db->get('cp_doctor');





   $num_rows = $q->num_rows();

   if($num_rows >0)

   {

     foreach($q->result() as $row)

     {

      $data[] = $row;

    }

    $q->free_result();

    return $data;

  }

}



public function getAllwhere($table,$where)

{

  $this->db->select('*');

  $q = $this->db->get_where($table,$where);

  $num_rows = $q->num_rows();

  if($num_rows >0)

  {

   foreach($q->result() as $row)

   {

    $data[] = $row;

  }

  $q->free_result();

  return $data;

}

}



  function addRecords($table,$post_data)

  {

    $this->db->insert($table,$post_data);

    return $this->db->insert_id();

  }



  public function updateData($table,$data,$where)

  {

    $this->db->update($table,$data,$where);

    return $this->db->affected_rows();

  }

  /**
   * Get compieance data
   */

public function getComplieanceData($user_id) {
		$data = array();
		if($user_id != ''){
		  $result = $this->db->select('count(user_id) as count, type')
			  ->from('missed_alarm_compliance')
			  ->group_by('user_id')
			  ->where("DATE_FORMAT(created_at,'%d')", date('d'))
			  ->get()->result();
		if(!empty($result)){
			$data['daily'] = $result[0];
		}

		  $result = $this->db->select('count(user_id) as count, type')
			  ->from('missed_alarm_compliance')
			  ->group_by('user_id')
			  ->where("WEEK(created_at)", date("W", strtotime(date('Y-m-d'))))
			  ->get()->result();
		  if(!empty($result)){
			  $data['weekly'] = $result[0];
		  }
		  $result = $this->db->select('count(user_id) as count, type')
			  ->from('missed_alarm_compliance')
			  ->group_by('user_id')
			  ->where("DATE_FORMAT(created_at,'%m')", date('m'))
			  ->get()->result();

		  if(!empty($result)){
			  $data['monthly'] = $result[0];
		  }
		}

	  return $data;
  }



}

?>
