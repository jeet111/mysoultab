<style>
	.red {

		color: red;

	}

	.imageurl, .videoUrl {
    position: relative;
    float: left;
    width: 100%;
    margin: 0;
    color: green;
}
	span.input-group-btn.imgbtn,
	.videobtn {
		position: relative;
	}

	.red1 {

		color: red;

	}

	#progress-wrp {

		border: 1px solid #0099CC;

		padding: 1px;

		position: relative;

		border-radius: 3px;

		margin: 10px;

		text-align: left;

		background: #fff;

		box-shadow: inset 1px 3px 6px rgba(0, 0, 0, 0.12);

	}

	#progress-wrp .progress-bar {

		height: 20px;

		border-radius: 3px;

		background-color: #f39ac7;

		width: 0;

		box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);

	}

	#progress-wrp .status {

		top: 3px;

		left: 50%;

		position: absolute;

		display: inline-block;

		color: #000000;

	}
</style>

<div class="container-fluid">

	<div class="dash-counter">

		<div class="Schedule_main_one">

			<div class="users-main">

				<?php

				echo "<h2>Edit Yoga video</h2>";

				?>

				<div id="image_msg"></div>





				<div class="btn_topBack">

					<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">

						Back

					</a>

				</div>

				<?php //print_r( $editMovie);
				?>

				<form class="form-horizontal add_movie_form" action="" method="post" enctype="multipart/form-data" name="AddUsers" id="movie-form">


					<div id="loders"></div>

					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for=""> Title:<span class="field_req">*</span></label>

							<div class="col-md-8">


								<input type="text" placeholder="Yoga video Title" class="form-control" name="yoga_title" id="username" value="<?php echo $editYoga->yoga_title; ?>">

								<?php echo form_error('yoga_title'); ?>

							</div>

						</div>

					</div>

					<input type="hidden" name="yoga_id" id="yoga_id" value="<?php echo $this->uri->segment('3'); ?>">

					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for=""> Description:<span class="field_req">*</span></label>

							<div class="col-md-8">


								<textarea name="yoga_desc" id="" class="form-control" placeholder="Yoga Video Description" value="<?php echo set_value('yoga_desc'); ?>"><?php echo $editYoga->yoga_desc; ?></textarea>


								<?php echo form_error('yoga_desc'); ?>

							</div>

						</div>

					</div>


					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for=""> Level:<span class="field_req">*</span></label>

							<div class="col-md-8">

								<select name="yoga_level" class="form-control">

									<option value="Easy" <?php if ($editYoga->yoga_level == 'Easy') {
																echo 'selected=="selected"';
															} ?>>Easy</option>
									<option value="Moderate" <?php if ($editYoga->yoga_level == 'Moderate') {
																	echo 'selected=="selected"';
																} ?>>Moderate</option>
									<option value="Challenging" <?php if ($editYoga->yoga_level == 'Challenging') {
																	echo 'selected=="selected"';
																} ?>>Challenging</option>

								</select>

							</div>

						</div>

					</div>


					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for=""> Type:<span class="field_req">*</span></label>

							<div class="col-md-8">





								<select name="yoga_type" class="form-control">

									<option value="Morning" <?php if ($editYoga->yoga_type == 'Morning') {
																echo 'selected=="selected"';
															} ?>>Morning</option>
									<option value="Afternoon" <?php if ($editYoga->yoga_type == 'Afternoon') {
																	echo 'selected=="selected"';
																} ?>>Afternoon</option>

									<option value="Evening" <?php if ($editYoga->yoga_type == 'Evening') {
																echo 'selected=="selected"';
															} ?>>Evening</option>

								</select>



							</div>

						</div>

					</div>

					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for=""> Duration</label>

							<div class="col-md-8">

								<select name="yoga_duration" class="form-control" >
								<option value="3 Pose" <?php if ($editYoga->yoga_duration == '3 Pose') {
																echo 'selected=="selected"';
															} ?>>3 Pose</option>
									<option value="5 Pose" <?php if ($editYoga->yoga_duration == '5 Pose') {
																	echo 'selected=="selected"';
																} ?>>5 Pose</option>

									<option value="8 Pose" <?php if ($editYoga->yoga_duration == '8 Pose') {
																echo 'selected=="selected"';
															} ?>>8 Pose</option>
								
							</select>
									<?php
									// $start = "00:00"; //you can write here 00:00:00 but not need to it
									// $end = "00:30";

									// $tStart = strtotime($start);
									// $tEnd = strtotime($end);
									// $tNow = $tStart;
									
									?>
									<!-- <select name="yoga_duration" class="form-control"> -->
										<?php
										// while ($tNow <= $tEnd) { ?>
											<!-- <option value="<?php //echo date("i:s", $tNow); ?>" <?php //if ($editYoga->yoga_duration == date("i:s", $tNow)) { -->
																								//	echo 'selected=="selected"';
																							//	} ?>><?php //echo date("i:s", $tNow); ?>
											</option>
										 <?php //$tNow = strtotime('+1 minutes', $tNow); -->
										// }

										?>
									</select> -->


							</div>

						</div>

					</div>



					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for=""> Help with</label>

							<div class="col-md-8">

								<select name="body_part" class="form-control">


									<option value="Arms" <?php if ($editYoga->body_part == 'Arms') {
																echo 'selected=="selected"';
															} ?>>Arms</option>
									<option value="Head" <?php if ($editYoga->body_part == 'Back') {
																echo 'selected=="selected"';
															} ?>>Back</option>
									<option value="Neck" <?php if ($editYoga->body_part == 'Neck') {
																echo 'selected=="selected"';
															} ?>>Neck</option>
									<option value="Head & Neck" <?php if ($editYoga->body_part == 'Head & Neck') {
																	echo 'selected=="selected"';
																} ?>>Head & Neck</option>






								</select>



							</div>

						</div>

					</div>
					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for="">Yoga Style</label>

							<div class="col-md-8">

								<select name="yoga_style" class="form-control">


									<option value="Restorative" <?php if ($editYoga->yoga_style == 'Restorative') {
																	echo 'selected=="selected"';
																} ?>>Restorative</option>
									<option value="Energizing" <?php if ($editYoga->yoga_style == 'Energizing') {
																	echo 'selected=="selected"';
																} ?>>Energizing</option>

								</select>

							</div>

						</div>

					</div>
					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for="">Yoga Pose</label>

							<div class="col-md-8">




								<select name="yoga_pose" class="form-control">
									<option value='Stand' <?php if ($editYoga->yoga_pose == 'Stand') {
																echo 'selected=="selected"';
															} ?>>Stand</option>

									<option value='Chair' <?php if ($editYoga->yoga_pose == 'Chair') {
																echo 'selected=="selected"';
															} ?>>Chair</option>

									<option value='Mix' <?php if ($editYoga->yoga_pose == 'Mix') {
															echo 'selected=="selected"';
														} ?>>Mix</option>

									<option value='Floor' <?php if ($editYoga->yoga_pose == 'Floor') {
																echo 'selected=="selected"';
															} ?>>Floor</option>
								</select>
							</div>

						</div>

					</div>
					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for=""> Upload Video</label>

							<div class="col-md-8">

								<div class="input-group">

									<span class="input-group-btn videobtn">


										<input type="file" id="video_upload" class="form-control" name="video_upload" name="video_upload" onchange="loadFile1(event)" value="<?php if (!empty($editYoga->yoga_video)) {
																																													echo $editYoga->yoga_video;
																																												} ?>">
										<h5 class="videoUrl">
											<?php echo $editYoga->yoga_video; ?>
									</span>
									<?php echo form_error('upload_video'); ?>

									</span>

								</div>
								<?php if (!empty($editYoga->yoga_video)) { ?>
									<video width="320" height="240" controls>
										<source src="<?php echo base_url(); ?>uploads/yoga_videos/<?php echo $editYoga->yoga_video; ?>" type="video/mp4">

									</video>
								<?php } ?>

							</div>

						</div>

					</div>
					<!-- start -->
					<div class="col-md-8">

		<div class="form-group">

			<label class="control-label col-md-4" for="">Youtube URL</label>

			<div class="col-md-8">

				<div class="input-group">

					<span class="input-group-btn">


					<input type="url" class="form-control" name="yoga_url" value="<?php echo $editYoga->yoga_url; ?>">

					</span>




			</div>



		</div>

	</div>

</div>
					<!-- end -->


					<div class="col-md-8">

						<div class="form-group">

							<label class="control-label col-md-4" for=""> Image</label>

							<div class="col-md-8">

								<div class="input-group">

									<span class="input-group-btn imgbtn">

										<!-- <span class="btn btn-default btn-files"> -->

										<input type="file" id="profile_images" class="form-control" name="pictureFile" onchange="loadFile(event)" value="<?php echo base_url() . 'uploads/yoga_videos/image/' . $editYoga->yoga_image ?>">

										<h5 class="imageurl">
											<?php echo $editYoga->yoga_image; ?>

										</h5>

									</span>

									<div id="flupmsgs"></div>

								</div>

								<div>

									<span id="demo" style="color: red"></span>

								</div>





								<?php if ($editYoga->yoga_image == '') {
								?>

									<img id='img-upload' src="<?php echo base_url() . 'uploads/yoga_videos/image/defualt_movie.png' ?>">

								<?php } else { ?>

									<img id='img-upload' src="<?php echo base_url() . 'uploads/yoga_videos/image/' . $editYoga->yoga_image ?>">



								<?php } ?>

								<div class="bnt_sub">

									<input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit">





								</div>

							</div>

						</div>

					</div>







				</form>

			</div>

		</div>

	</div>

</div>



<!--Added by 95 on 5-2-2019 For Doctor category for validation-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

<script>
	$(".add_movie_form").on('submit', (function(e)

		{

			e.preventDefault();

			var isvalidate = $(".add_movie_form").valid();

			if (isvalidate)

			{

				$("#loders").append('<img id="loading_img" style="width:20px" src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif" />');





				$.ajax({

					url: "<?php echo base_url(); ?>Admin/Yoga_video/ajax_yoga_edit",

					type: "POST",

					data: new FormData(this),

					dataType: "json",

					contentType: false,

					cache: false,

					processData: false,

					success: function(data) {

						$("#loading_img").hide();

						if (data.code == 1) {

							$("#loading_img").hide();

							$("#sub").prop('disabled', true);

							$("#image_msg").html('<div class="alert alert-success">' + data.message + '<div>');

							setTimeout(function() {

								$("#image_msg").html("");

								window.location.href = '<?php echo base_url(); ?>admin/yoga_video_list';

							}, 1000);

						}
					}

				});

			}

		}));

	function loadFile(event) {

		var pcFile = $('#profile_images').val().split('\\').pop();

		var pcExt = pcFile.split('.').pop();

		var output = document.getElementById('img-upload');

		if (pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"

			||
			pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF") {

			$('#img-upload').show();

			output.src = URL.createObjectURL(event.target.files[0]);

			$('#flupmsgs').html('');

		} else {

			$('#flupmsgs').html('Please select only Image file.');

			$('#img-upload').hide();

		}

	};





	function loadFile1(event) {

		var pcFile = $('#profile_imagess').val().split('\\').pop();

		var pcExt = pcFile.split('.').pop();

		var output = document.getElementById('img-uploads');

		if (pcExt == "mp4" || pcExt == "3gp" || pcExt == "avi" || pcExt == "wmv") {

			$('#img-uploads').show();

			output.src = URL.createObjectURL(event.target.files[0]);

			$('#flupmsg').html('');

		} else {

			$('#flupmsg').html('Please select only vedio file.');

			$('#img-uploads').hide();

		}

	};
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$.validator.addMethod("regx", function(value, element, regexpr) {
			return regexpr.test(value);
		}, "Please upload valid extension vedio formate file.");

		$('#movie-form').validate({

			rules: {

				yoga_title: 'required',
				yoga_desc: 'required',
				yoga_level: 'required',
				yoga_type: 'required',
			},
			messages: {
				yoga_title: 'Please enter video title.',
				yoga_desc: 'Please enter video description.',
			yoga_level: 'Please select a level name.',
				yoga_type: 'Please select a video type.'
			}
		});

	});
	$(document).ready(function() {
		var progress_bar_id = '#progress-wrp';
		$('#profile_imagess').change(function(e) {
			var pcFile = $('#profile_imagess').val().split('\\').pop();
			var pcExt = pcFile.split('.').pop();
			var output = document.getElementById('img-uploads');
			if (pcExt == "mp4" || pcExt == "3gp" || pcExt == "avi" || pcExt == "wmv") {
				$('#flupmsg').hide();
				$('#progress-wrp').show();
				$(progress_bar_id + " .progress-bar").css("width", "0%");
				$(progress_bar_id + " .status").text("0%");
				// var file = this.files[0];

				var file = this.files[0];
				var form = new FormData();
				form.append('360_image_upload', file);
				$.ajax({
					url: "<?php echo base_url(); ?>Admin/Movie/image_upload",
					type: "POST",
					cache: false,
					contentType: false,
					processData: false,
					data: form,
					dataType: "json",
					xhr: function() {

						//upload Progress

						var xhr = $.ajaxSettings.xhr();
						if (xhr.upload) {
							xhr.upload.addEventListener('progress', function(event) {
								var percent = 0;
								var position = event.loaded || event.position;
								var total = event.total;
								if (event.lengthComputable) {
									percent = Math.ceil(position / total * 100);
								}

								//update progressbar

								$(progress_bar_id + " .progress-bar").css("width", +percent + "%");
								$(progress_bar_id + " .status").text(percent + "%");

							}, true);
						}
						return xhr;					},

					success: function(data) {
						$("#progress-wrp").hide();
						$("#dfds").show();
						var fileInput = document.getElementById('profile_imagess');
						var fileUrl = window.URL.createObjectURL(fileInput.files[0]);
						$("#dfds").attr("src", fileUrl);

					}

				});

			}

		});

	});
</script>