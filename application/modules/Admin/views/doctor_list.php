<?php

if (isset($staff_permission[3]['add_permission']) == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_doctor">Add Doctor</a></div>

	</div>

	<?php

} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

	?>

	<div class="bx_user">

		<div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_doctor">Add Doctor</a></div>

	</div>

	<?php

}

?>







<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="row">

			<div class="col-md-12">

				<div class="">

					<div class="card-body">

						<div class="status-cng"></div>

						<?php if ($this->session->flashdata('success')) { ?>

							<div class="alert alert-success message">

								<button type="button" class="close" data-dismiss="alert">x</button>

								<?php echo $this->session->flashdata('success'); ?></div>

							<?php } ?>



							<!-- <div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_doctor">Add Doctor</a></div> -->

							<table class="table table-hover tab_comn" id="sampleTable">

								<thead>

									<tr>

										<th>S no.</th>

										<th>Doctor Name</th>

										<th>Address</th>

										<th>Mobile No</th>

										<th>Action</th>

									</tr>

								</thead>

								<tbody>

									<?php 

									$i = 1;

									foreach ($doctor_list as $list) {



										?>

										<tr>

											<td><?php echo $i++; ?></td>

											<td><?php echo $list->doctor_name; ?></td>

											<td><?php echo $list->doctor_address; ?></td>

											<td><?php echo $list->doctor_mob_no; ?></td>



											<td>

												<div class="link-del-view">

													<?php

													if (isset($staff_permission[3]['view_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/view_doctor/<?php echo $list->doctor_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

															<span class="tooltiptext">View</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/view_doctor/<?php echo $list->doctor_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

															<span class="tooltiptext">View</span>

														</div>

														<?php

													}

													?>



													<?php

													if (isset($staff_permission[3]['edit_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/edit_doctor/<?php echo $list->doctor_id; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>

															<span class="tooltiptext">Edit</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/edit_doctor/<?php echo $list->doctor_id; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>

															<span class="tooltiptext">Edit</span>

														</div>

														<?php

													}

													?>



													<?php

													if (isset($staff_permission[3]['delete_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/delete_doctor/<?php echo $list->doctor_id; ?>" onclick="return confirm('Are you sure you want to delete this doctor ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/delete_doctor/<?php echo $list->doctor_id; ?>" onclick="return confirm('Are you sure you want to delete this doctor ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

														</div>

														<?php

													}

													?>



													<?php

													if (isset($staff_permission[3]['add_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/add_shedule/<?php echo $list->doctor_id; ?>"><i class="fa fa-clock-o" aria-hidden="true"></i></a><span class="tooltiptext">Add Schedule</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/add_shedule/<?php echo $list->doctor_id; ?>"><i class="fa fa-clock-o" aria-hidden="true"></i></a><span class="tooltiptext">Add Schedule</span>

														</div>

														<?php

													}

													?>



												</div>



											</div>

										</td>

									</tr>

								<?php } ?>

							</tbody>

						</table>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>