<footer class="main_footer">	
	<a href="#" class="scrollToTop"><i class="fa fa-angle-up" aria-hidden="true"></i></a>   
	<div class="container">
		<div class="row">
			<div class="f_middle wow fadeInDown animated">
				<div class="nes_let">
					<h1>Subscribe to news letter</h1>
					<form action="" id="addressform">
						<input type="text" placeholder="Enter your email address." name="subs_email">
						<button type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
					</form>
				</div>
				<div class="foot_m">
					<div class="col-md-4 col-sm-4">
						<!-- class="f_heading" -->
						<div >
							<h3><b><?php echo getfooter_content()->description_heading ; ?></b></h3>
						</div>
						<div class="f_heading footer-add">
							<p><?php echo getfooter_content()->footer_desc ; ?></p>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="f_heading footer-add se_tab">
							<div class="social-demo">
								<p><?php echo getfooter_content()->contact_email ; ?></p>
								<p><b>Contact</b>      <span><?php echo getfooter_content()->contact_phone ; ?></span>   </p>
								<ul>                  
									<li><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
									<li><a href="https://www.linkedin.com/company/soultab"><i class="fa fa-linkedin"></i></a></li>
									<!-- <li><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li> -->
									<!-- <li><a href=" https://www.instagram.com/"><i class="fa fa-instagram"></i></a></li> -->
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="f_heading last_tb">
							<ul>
								<li><a href="<?php echo base_url(); ?>contact-us">Contact Us</a></li> 
								<li><a href="<?php echo base_url(); ?>about-us">About us</a></li>
								<!-- <li><a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a></li> -->
								<li><a href="<?php echo base_url(); ?>term-of-use">Terms of Use</a></li>
								<li><a href="<?php echo base_url(); ?>survey">Feedback</a></li>
							</ul></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="f_follow text-center"><?php echo getfooter_content()->rights_reserved ; ?></div>
					</div>
				</div>
			</div>
		</div>

		<style>
			*{
				font-family: Myriad Pro;   
				/*text-align: left;*/
			}
			.cookie-banner {
				margin: 0;
				/* height: 85px; */
				max-width: 100%;
				border-radius: 0px;
				background-color: #797A7E;
				position: fixed;
				bottom: 0;    width: 100%;
			}
			.cookie-banner h1 {
    color: #fff;
    font-size: 15px;
    padding: 4px 20px 11px;
    margin: 0;
    box-sizing: border-box;
    line-height: 22px;
}
			a{
				text-decoration: none;
				color: #fff;	
			}
			div#cookie-banner a {
				text-decoration: underline;
			}
			div#cookie-banner a:hover{
				color:#eee !important;
			}
			.button_agr {
    border: 0.5px solid #000;
    padding: 9px 0;
    border-radius: 0px;
    /* float: left; */
    margin: 0 10px 13px 14px;
    cursor: pointer;
    background-color: #ffffff;
    max-width: 144px !important;
    width: 100% !important;
    text-align: center;
    box-sizing: border-box;
    background: #fff!important;
    font-size: 17px;
    border: none !important;
    border-radius: 0;
    color: #797A7E;
}
div#cookie-banner h3 {
    color: #fff;
    margin: 0;
    padding-left: 18px;
    padding-bottom: 0;
    line-height: normal;
    padding-top: 15px;
}
			.button_agr:hover{
				color: #797A7E;
				background-color: #0A99FA;
				border: 0.5px solid #FFF;
			}
			#x {
    position: absolute;
    background: transparent !important;
    color: white;
    top: 7px;
    right: 20px;
    box-shadow: none !important;
    border: 0 !important;
    /* left: 18px; */
}
			@media(max-width:1024px){
				.cookie-banner h1 {

					font-size: 12px;
				}
			}
			@media (max-width: 812px){
				.cookie-banner h1 {
					font-size: 10px;
				}
				#x {
					right: 61px;
				}
			}
			@media (max-width: 667px){
				.cookie-banner h1 {
					font-size: 8px !important;
				}
			}

		</style>

		<?php $ter= get_cookie('soulTab');  ?>

		<?php if(empty($ter)){ ?>

			<div class="cookie-banner" id="cookie-banner">
			<h3>Cookie Consent</h3>
				<h1>By continuing to browser or by clicking 'Accept', you agree to the storing of cookies on your device to enhance your site experience and for analytical purposes. To learn more about how we use  the cookies, please see our <a href="#">cookie policy</a>.
				</h1>

				<div class="button_agr" onclick="AgreeCookie()">Accept and close</div>
				<button id = "x"  onclick="hide('cookie-banner')">
					X
				</button>
			</div>




			<!-- <div class="cookie-banner">
				<h1>We are using cookies for increasing
					your web experience on our website.
					Please give <br> the authorization as accept these 
					cookies and remove this permanently.
					<a href="#">Read More</a> 
				</h1>

				<div class="button_dis" onclick="myFunction()">Disgree</div>
				<div class="button_agr" onclick="AgreeCookie()">Agree</div>
			</div> -->
			<!-- <div class="cookie-banner" >
				<h1>We are using cookies for increasing
					your web experience on our website.
					Please give the authorization  <br> as accept these 
					cookies and remove this permanently.
					
				</h1>

				<div class="button" onclick="AgreeCookie()">Agree</div>
				<div class="button" onclick="myFunction()">Not Agree</div>
			</div> -->

			<!-- <div class="cookie-banner col-lg-12">
				<p>
					By using our website, you agree to our 
					<button onclick="AgreeCookie()">Agree</button>&nbsp;&nbsp;
					<button onclick="myFunction()">Not Agree</button>
				</p>

			</div> -->
		<?php } ?>
  <!-- <div class="container text-center bt_pop">
  <div class="row">
        <div class="round hollow text-center po_ro_pop">
        <a href="#" id="addClass"><span class="glyphicon glyphicon-comment"></span> Open in chat </a>
        </div>
        <hr>
  </div>
</div> -->
  <!--
  <div class="popup-box chat-popup" id="qnimate">
         <div class="popup-head">
        <div class="popup-head-left pull-left"><img src="http://bootsnipp.com/img/avatars/team_thre.jpg" alt="iamgurdeeposahan"> Gurdeep Osahan</div>
            <div class="popup-head-right pull-right">
            <div class="btn-group">
                      <button class="chat-header-button" data-toggle="dropdown" type="button" aria-expanded="false">
                     <i class="glyphicon glyphicon-cog"></i> </button>
                    <ul role="menu" class="dropdown-menu pull-right">
                    <li><a href="#">Media</a></li>
                    <li><a href="#">Block</a></li>
                    <li><a href="#">Clear Chat</a></li>
                    <li><a href="#">Email Chat</a></li>
                    </ul>
            </div>
            <button data-widget="remove" id="removeClass" class="chat-header-button pull-right" type="button"><i class="glyphicon glyphicon-off"></i></button>
                      </div>
                  </div> -->
      <!-- <div class="popup-messages">
       <div class="direct-chat-messages">
       <div class="chat-box-single-line">
       <abbr class="timestamp">October 8th, 2015</abbr>
          </div>
          <div class="direct-chat-msg doted-border">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left">Osahan</span>
                      </div>
                      <img alt="message user image" src="http://bootsnipp.com/img/avatars/bcf1c0d13e5500875fdd5a7e8ad9752ee16e7462.jpg" class="direct-chat-img">
                      <div class="direct-chat-text">
                        Hey bro, how’s everything going ?
                      </div>
            <div class="direct-chat-info clearfix">
                        <span class="direct-chat-timestamp pull-right">3.36 PM</span>
                      </div>
            <div class="direct-chat-info clearfix">
            <span class="direct-chat-img-reply-small pull-left">
            </span>
            </div></div>
          <div class="chat-box-single-line">
            <abbr class="timestamp">October 9th, 2015</abbr>
          </div>
      		<div class="direct-chat-msg doted-border">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left">Osahan</span>
                      </div>
                      <img alt="iamgurdeeposahan" src="http://bootsnipp.com/img/avatars/bcf1c0d13e5500875fdd5a7e8ad9752ee16e7462.jpg" class="direct-chat-img">
                      <div class="direct-chat-text">
                        Hey bro, how’s everything going ?
                      </div>
            <div class="direct-chat-info clearfix">
                        <span class="direct-chat-timestamp pull-right">3.36 PM</span>
                      </div>
            <div class="direct-chat-info clearfix">
              <img alt="iamgurdeeposahan" src="http://bootsnipp.com/img/avatars/bcf1c0d13e5500875fdd5a7e8ad9752ee16e7462.jpg" class="direct-chat-img big-round">
            <span class="direct-chat-reply-name">Singh</span>
            </div>
                    </div>
            </div></div>
		      <div class="popup-messages-footer">
		      <textarea id="status_message" placeholder="Type a message..." rows="10" cols="40" name="message"></textarea>
		      <div class="btn-footer">
		      <button class="bg_none"><i class="glyphicon glyphicon-film"></i> </button>
		      <button class="bg_none"><i class="glyphicon glyphicon-camera"></i> </button>
		            <button class="bg_none"><i class="glyphicon glyphicon-paperclip"></i> </button>
		      <button class="bg_none pull-right"><i class="glyphicon glyphicon-thumbs-up"></i> </button>
		      </div>
		      </div>
		  </div> -->
		</footer>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

		<script src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/styles/bootstrap-4.1.2/popper.js"></script>
		<!-- <script src="assets/styles/bootstrap-4.1.2/bootstrap.min.js"></script> -->
		<script src="<?php echo base_url(); ?>assets/plugins/greensock/TweenMax.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/greensock/TimelineMax.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/scrollmagic/ScrollMagic.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/greensock/animation.gsap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/greensock/ScrollToPlugin.min.js"></script>
		<!-- <script src="assets/plugins/OwlCarousel2-2.3.4/owl.carousel.js"></script> -->
		<script src="<?php echo base_url(); ?>assets/plugins/easing/easing.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/progressbar/progressbar.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/parallax-js-master/parallax.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/jquery-datepicker/jquery-ui.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/colorbox/jquery.colorbox-min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
		<!-- <script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script> -->
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/wow.min.js"></script> 
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
		<script>
			function hide(target) {
				document.getElementById(target).style.display = 'none';
			}
			function myFunction() {
				
				$(".cookie-banner").hide();
			}

			function AgreeCookie() {
				$.ajax({
					url: 'tempcookie.php',            
					method: 'POST',
					type: 'json',
					success: function (response) {
						if(response=='1'){
							$(".cookie-banner").hide();
						}
					},
					error: function (error) {
						console.log(error);
					}
				});
			}
		</script>

		<script>
			wow = new WOW(
			{
				animateClass: 'animated',
				offset: 100
			}
			);
			wow.init();
		</script>
		<script>
			$(document).ready(function() {
				
				
				$("#owl-demo").owlCarousel({
					navigation : true,
					slideSpeed : 300,
					paginationSpeed : 400,
					singleItem : true,
					pagination : false,
					autoPlay:false
	    //autoplayHoverPause:true
	});
			});
			$(document).ready(function() {
				$("#student-demo").owlCarousel({
					items : 5,
					itemsDesktop : [1180,4],
					itemsDesktopSmall : [800,3],
					itemsTablet: [600,2],
					itemsMobile : [450,1], 
					lazyLoad : true,
					navigation : false,
					pagination : false,
					autoPlay:true,
					autoplayHoverPause:true
				});
			});
		</script>
		<script type="text/javascript">
			jQuery(window).scroll(function() {   
				if (jQuery(window).scrollTop() >=0) { 
					if(jQuery('#nav-top').hasClass( "navi" ))
					{
	  		//jQuery('#header').removeClass("fixed-theme");
	  	}
	  	else
	  	{
	  		jQuery('#nav-top').addClass("navi");
	  	}
	  }
	  else{
	  	jQuery('#nav-top').removeClass("navi");
	  }
	});
</script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">		

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
<script type="text/javascript">
	$('#addressform').validate({
		rules: {
			//usrfeedback: "required",
			subs_email:{
				required:true,
				email:true,
			},
			// mobile:{
			// 	number:true,
			// 	required:true,
			// },
		},
		messages: {
			subs_email:{
				required:"Please enter email.",
				email:"Please enter valide email.",
			},
			// mobile:{
			// 	number:"Please enter valide mobile no.",
			// 	required:"Please enter mobile no.",
			// },
			//usrfeedback:"Please enter your feedback.",
		},
		submitHandler: function(form){
			$.ajax({
				url: '<?php echo base_url();?>Pages/Login/NewsSubs',
				type: 'post',
				data: $(form).serialize(),
				success: function(data) {
						//returnData = JSON.parse(data);
						console.log(data);
						if(data==1){
                    //console.log(response);
                    //alert('Thanks for your feedback..!');
                    //sweetAlert("Success", "Thanks for your feedback..!", "success");
                    setTimeout(function() {
                    	swal({
                    		title: "Success",
                    		text: "Thanks for Subscribe our news letter.!",
                    		type: "success"
                    	}, function() {
                    		window.location = "<?php echo base_url();?>";
                    	});
                    }, 1000);
                    //location.href = "<?php echo base_url();?>"
                    // $('#success_message').fadeIn().html('<div class="alert alert-success message"><button type="button" class="close" data-dismiss="alert">x</button>Message sent successfully</div>');
                    // setTimeout(function() {
                    // 	$('#success_message').fadeOut();
                    // }, 5000 );
                }else if(data==0){
                	alert("Please try again somthing went wrong.");
                }
            }            
        });
		}
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.navbar a.dropdown-toggle').on('click', function(e) {
			var $el = $(this);
			var $parent = $(this).offsetParent(".dropdown-menu");
			$(this).parent("li").toggleClass('open');
			if(!$parent.parent().hasClass('nav')) {
				$el.next().css({"top": $el[0].offsetTop, "left": $parent.outerWidth() - 4});
			}
			$('.nav li.open').not($(this).parents("li")).removeClass("open");
			return false;
		});
	});
</script>
<script>
	$(document).ready(function() {
		$("#owl-demo-1").owlCarousel({
			autoPlay: 3000,
			items : 1,
			itemsDesktop : [1199,1],
			itemsDesktopSmall : [979,1],
			navigation:true,
			navigationText: [
			"<i class='fa fa-chevron-left'></i>",
			"<i class='fa fa-chevron-right'></i>"
		});
	});
</script>
<script>
	$(document).ready(function(){
    //Check to see if the window is top if not then display button
    $(window).scroll(function(){
    	if ($(this).scrollTop() > 100) {
    		$('.scrollToTop').fadeIn();
    	} else {
    		$('.scrollToTop').fadeOut();
    	}
    });
    //Click event to scroll to top
    $('.scrollToTop').click(function(){
    	$('html, body').animate({scrollTop : 0},800);
    	return false;
    });
});
</script>
<script type="text/javascript">
	$(window).scroll(function() {
		if ($(this).scrollTop() > 100) {
			$('.back-to-top').fadeIn('slow');
		} else {
			$('.back-to-top').fadeOut('slow');
		}
	});
</script>
<script>
	$(document).ready(function(){
		$('li> a').click(function() {
			$('li').removeClass();
			$(this).parent().addClass('active');
		});
	});
</script>
<script>
	$(document).ready(function() { 
		$('.post-like').click(function(){
			$(".post-like img").attr('src',"assets/images/ic_imoji_disappoint.png");
			return false;
		});
		$('.post-like_one').click(function(){
			$(".post-like_one img").attr('src',"assets/images/ic_imoji_poor.png");
			return false;
		});
		$('.post-like_two').click(function(){
			$(".post-like_two img").attr('src',"assets/images/ic_imojinotimpress.png");
			return false;
		});
		$('.post-like_three').click(function(){
			$(".post-like_three img").attr('src',"assets/images/ic_imoji_impressed.png");
			return false;
		});
		$('.post-like_four').click(function(){
			$(".post-like_four img").attr('src',"assets/images/ic_imojiperfect.png");
			return false;
		});
	});
</script>
<!-- <script type="text/javascript">
   $(function(){
$("#addClass").click(function () {
          $('#qnimate').addClass('popup-box-on');
            });
            $("#removeClass").click(function () {
          $('#qnimate').removeClass('popup-box-on');
            });
  })
</script> -->
<script src="<?php echo base_url(); ?>assets/js/jquery-accordion.js"></script>
<script>
	$(".faq").accordion({
		questionClass: '.header',
		answerClass: '.content',
		itemClass: '.faqitem'
	});
</script>
<style>
/*body, html, .main {
  height: 100%;
  }*/
/*section {
  min-height: 100%;
  }*/
</style>
</body>
<!-- Start of LiveChat (www.livechatinc.com) code -->
<script>
	window.__lc = window.__lc || {};
	window.__lc.license = 12224370;
	;(function(n,t,c){function i(n){return e._h?e._h.apply(null,n):e._q.push(n)}var e={_q:[],_h:null,_v:"2.0",on:function(){i(["on",c.call(arguments)])},once:function(){i(["once",c.call(arguments)])},off:function(){i(["off",c.call(arguments)])},get:function(){if(!e._h)throw new Error("[LiveChatWidget] You can't use getters before load.");return i(["get",c.call(arguments)])},call:function(){i(["call",c.call(arguments)])},init:function(){var n=t.createElement("script");n.async=!0,n.type="text/javascript",n.src="https://cdn.livechatinc.com/tracking.js",t.head.appendChild(n)}};!n.__lc.asyncInit&&e.init(),n.LiveChatWidget=n.LiveChatWidget||e}(window,document,[].slice))
</script>
<noscript><a href="https://www.livechatinc.com/chat-with/12224370/" rel="nofollow">Chat with us</a>, powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a></noscript>
<!-- End of LiveChat code -->
</html>