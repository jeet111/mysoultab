<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Music extends MX_Controller {
	public function __construct() {
		parent:: __construct();
		$this->load->library('session');
		$this->load->library('form_validation');      
		$this->load->model('activities_model');
		$this->load->model('Common_model_new');
		$this->load->model('common_model_new');
		$this->load->model('Music_model');
		$this->load->helper(array('common_helper'));
		$this->load->helper(array('url'));
		$this->load->helper(array('form'));
		not_login();      
	}


	public function detail_music()
	{ 
		$data['menuactive'] = $this->uri->segment(1);
		$checkLogin = $this->session->userdata('logged_in');
		if(!empty($checkLogin))
		{
			$segment = $this->uri->segment('2');
			$data['detail_music']  = $this->Common_model_new->getsingle("music",array("music_id" => $segment));
			$data['check_music'] = $this->Common_model_new->getsingle("cp_music_favorite",array("music_id" => $segment,"user_id" => $checkLogin['id']));
	 // echo '<pre>';print_r($data['detail_music']);die;

			$this->template->set('title', 'All Activities');
			$this->template->load('user_dashboard_layout_music1', 'contents', 'detail_music',$data);
		}else{
			redirect('login'); 
		}
	}
	public function favorite_music()
	{
		$music_id = $this->input->post("music_id");
		$checkLogin = $this->session->userdata('logged_in');
		$check_music = $this->Common_model_new->getsingle("cp_music_favorite",array("music_id" => $music_id,"user_id" => $checkLogin['id']));
		if(!empty($check_music)){
			$this->Common_model_new->delete("cp_music_favorite",array("music_id" => $music_id,"user_id" => $checkLogin['id']));
			echo '0';
		}else{
			$array = array("user_id" => $checkLogin["id"],"music_id" => $music_id,"fav" => 1,"music_fav_created" => date("Y-m-d H:i:s"));
			$this->Common_model_new->insertData("cp_music_favorite",$array);
			echo '1';
		}
	}

	public function delete_checkboxes()
	{
		$delete_check = $this->input->post("bodytype");
		$explode = explode(',',$delete_check);
		foreach($explode as $delete_id){
			$this->Common_model_new->delete('cp_music_playlist',array('music_playlist_id' => $delete_id));	
		}
		$error['check'] = "100";
		echo json_encode($error);
	}
	public function music_list()
	{ 
		$user_id = $this->session->userdata('logged_in')['id'];

		$data['menuactive'] = $this->uri->segment(1);
		$checkLogin = $this->session->userdata('logged_in');
		$data['music_banner'] = $this->Common_model_new->getAllBanner();
		$data['popular_array'] = $this->Common_model_new->getAllPopular();	
		$data['recent_array']  = $this->Common_model_new->getAllRecent();
		$data['category_array'] = $this->Common_model_new->getAllCategory();
		$data['mymusic']  = $this->Common_model_new->getAllMyMusic();
		$data['playlist_music'] = $this->Common_model_new->jointwotablenn("cp_music_playlist", "music_id", "music", "music_id",array("cp_music_playlist.user_id" => $checkLogin['id']),"*","cp_music_playlist.music_playlist_id","desc");
		$data['favorite_music'] = $this->Common_model_new->jointwotablenn("cp_music_favorite", "music_id", "music", "music_id",array("cp_music_favorite.user_id" => $checkLogin['id']),"*","cp_music_favorite.music_fav_id","desc");
		$data['popular_count_array'] = $this->Common_model_new->getAll("music");
		$data['category_count_array'] = $this->Common_model_new->getAllwhere("cp_music_category",array("music_category_status" => 1));
		$data['mymusic_count_array'] = $this->Common_model_new->getAllwhere("music",array("user_id" => $checkLogin['id']));
		$data['favorite_count_array'] = $this->Common_model_new->getAllwhere("cp_music_favorite",array("user_id" => $checkLogin['id']));

		$data['single_playlist_music'] = $this->Common_model_new->getsingleorderlimit1("cp_music_playlist", "music_id", "music", "music_id","","*","cp_music_playlist.music_playlist_id","desc");
		$this->template->set('title', 'Music');
		$this->template->load('new_user_dashboard_layout', 'contents', 'music',$data);
	}
	public function search_newmusic_list()
	{
		$checkLogin = $this->session->userdata('logged_in'); 
		$data['music_banner'] = $this->Common_model_new->getAllBanner();
		//$music_search = $this->input->post("music_search");
		$music_search1 = preg_replace('/[^A-Za-z0-9\-]/', '', $this->uri->segment("2"));
		$music_search = str_replace('%20', ' ', $this->uri->segment("2"));
		$data['music_search'] = $music_search;
		$data['popular_array'] = $this->Common_model_new->getlikepopular($music_search);
		//echo $this->db->last_query();die;
        //print_r($data['popular_array']);die;	
		$data['recent_array']  = $this->Common_model_new->getlikerecent($music_search);
		$data['category_array'] = $this->Common_model_new->getlikecategory($music_search);
		$data['mymusic']  = $this->Common_model_new->getlikemymusic($music_search,$checkLogin['id']);
		$data['playlist_music'] = $this->Common_model_new->jointwotablenn("cp_music_playlist", "music_id", "music", "music_id",array("cp_music_playlist.user_id" => $checkLogin['id']),"*","cp_music_playlist.music_playlist_id","desc");
		//$data['favorite_music'] = $this->Common_model_new->jointwotablenn("cp_music_favorite", "music_id", "music", "music_id",array("cp_music_favorite.user_id" => $checkLogin['id']),"*","cp_music_favorite.music_fav_id","desc");
		$data['favorite_music'] = $this->Common_model_new->getfavlimitsearch($music_search,$checkLogin['id']);
		//echo $this->db->last_query();die;
		//echo '<pre>';print_r($data['popular_array']);die;
		$data['single_playlist_music'] = $this->Common_model_new->getsingleorderlimit1("cp_music_playlist", "music_id", "music", "music_id","","*","cp_music_playlist.music_playlist_id","desc");
		$this->template->set('title', 'Music');
		$this->template->load('user_dashboard_layout_music1', 'contents', 'music_combine_search',$data);
	}
	public function music_list1()
	{
  	  //$data['menuactive'] = $this->uri->segment(1);
	   //$checkLogin = $this->session->userdata('logged_in');
	 // $data['my_music_list'] = $this->Common_model_new->getAllwhereorderbylimit('music',array("user_id" => $checkLogin['id']),'music_id','desc',5);
  	  //$this->template->load('user_dashboard_layout_music1', 'contents', 'music',$data);
		$this->template->load('user_dashboard_layout_music1', 'contents', 'music1');
	}
	public function slider(){
  	//$this->template->load('contents', 'slider');
		$this->load->view('slider');
	}
	public function get_offset()
	{
		$data['offset'] = $this->input->post('offset');
		$like_val = $this->input->post('like_val');
		if(!empty($like_val)){
			$pagecon['per_page'] = 20;
			$allmusic = $this->Common_model_new->getsearchdatamusic($like_val,$pagecon['per_page'],$data['offset']);
			$data['music_list'] = $allmusic['rows'];
			$data['count_total'] = $allmusic['num_rows'];  
		}else{
			$pagecon['per_page'] = 20;
			$music_list1 = $this->Common_model_new->music_listing($pagecon['per_page'],$data['offset']);
	  //echo '<pre>';print_r($music_list1);die;
			if (!empty($music_list1['rows'])) {
				$data['music_list'] = $music_list1['rows'];
				$data['count_total'] = $music_list1['num_rows'];
			} else {
				$data['music_list'] = '';
				$data['count_total'] = '';
			}
		}
		$this->load->view('ajax_value', $data);
	}
	public function movie_list()
	{
		//die('jggjkfgj');
		$data['menuactive'] = $this->uri->segment(1);
		$checkLogin = $this->session->userdata('logged_in');
		$data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'movies'));
		$this->template->set('title', 'All Movie');
		$this->template->load('user_dashboard_layout', 'contents', 'movie',$data);
	}
	public function movie_detail()
	{
		$data['menuactive'] = $this->uri->segment(1);
		$checkLogin = $this->session->userdata('logged_in');
		$this->template->set('title', 'Movie Detail');
		$this->template->load('user_dashboard_layout', 'contents', 'movie_detail',$data);
	} 
	public function music_category()
	{
		$music_value = $this->input->post("music_value");
		$data['page'] = 0;
		$pagecon['per_page'] = 20;
		if($music_value == '1' || $music_value == '2'){		 
			$music_list1 = $this->Common_model_new->music_listing_category($music_value,$pagecon['per_page'],$data['page']);		
		}else{
			$music_list1 = $this->Common_model_new->music_listing($pagecon['per_page'],$data['page']);  
		}
		if (!empty($music_list1['rows'])) {
			$data['music_list'] = $music_list1['rows'];
			$data['count_total'] = $music_list1['num_rows'];
		} else {
			$data['music_list'] = '';
			$data['count_total'] = '';
		}
		$this->load->view("ajax_search_category_music",$data);
	}
	public function get_search_offset()
	{
		$data['offset'] = $this->input->post('offset');
		$pagecon['per_page'] = 20;
		$music_value = $this->input->post('music_value');
		if($music_value == '1' || $music_value == '2'){		 
			$music_list1 = $this->Common_model_new->music_listing_category($music_value,$pagecon['per_page'],$data['offset']);		
		}else{
			$music_list1 = $this->Common_model_new->music_listing($pagecon['per_page'],$data['offset']);  
		}
		if (!empty($music_list1['rows'])) {
			$data['music_list'] = $music_list1['rows'];
			$data['count_total'] = $music_list1['num_rows'];
		} else {
			$data['music_list'] = '';
			$data['count_total'] = '';
		}
		$this->load->view('ajax_value', $data);
	}
	// public function add_music()
	// {
	// 	$data['menuactive'] = $this->uri->segment(1);
	// 	$data['category_name'] = $this->Common_model_new->getAllwhereorderby("cp_music_category",array("music_category_status" => 1),"music_category_name","asc");
	// 	$this->template->set('title', 'Movie Detail');
	// 	$this->template->load('user_dashboard_layout', 'contents', 'add_music',$data);
	// }	
	public function image_upload()
	{
		$config1['upload_path'] = 'uploads/music/';
		$config1['allowed_types'] = 'mp3';
           //$data['max_size'] = '5000';
		$config1['encrypt_name'] = true;
		$this->load->library('upload', $config1);
		if ($this->upload->do_upload('picture')) {
			$attachment_data = array('upload_data' => $this->upload->data());
			$uploadfile = $attachment_data['upload_data']['file_name'];
			$return = array("code" => 1);
		}
		echo json_encode($return);
	}
	public function ajax_music_add()
	{
		$checkLogin = $this->session->userdata('logged_in');
		if ($_FILES["picture"]["error"] > 0 && $_FILES["pictureFile"]["error"] > 0)
		{
			$return = array('code' => 2,'message' => 'File loading error!');
		}else{ 
			$config1['upload_path'] = 'uploads/music/';
			$config1['allowed_types'] = 'mp3';
           //$data['max_size'] = '5000';
			$config1['encrypt_name'] = true;
			$this->load->library('upload', $config1);
			if ($this->upload->do_upload('picture')) {  
				$attachment_data = array('upload_data' => $this->upload->data());
				$uploadfile = $attachment_data['upload_data']['file_name'];
			// $return = array('code' => 1,'message' => 'Successfully added');	
			}else{ 
			//$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
			}
			$config2['upload_path'] = 'uploads/music/image/';
			$config2['allowed_types'] = 'jpg|png|gif|jpeg';
           //$data['max_size'] = '5000';
			$config2['encrypt_name'] = true;
			$this->upload->initialize($config2);
			if ($this->upload->do_upload('pictureFile')) {
				$attachment_data_image = array('upload_data' => $this->upload->data());
				$picturefile = $attachment_data_image['upload_data']['file_name'];
      //  $return = array('code' => 1,'message' => 'Successfully added');
				/*For image resize*/
				$this->load->library('image_lib');
				$configer =  array(
					'image_library'   => 'gd2',
					'source_image'    =>  $attachment_data_image['upload_data']['full_path'],
					'maintain_ratio'  =>  TRUE,
					'width'           =>  150,
					'height'          =>  150,
				);
				$this->image_lib->clear();
				$this->image_lib->initialize($configer);
				$this->image_lib->resize();		   
			}else{ 
			//$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
			}
			if($return['code'] != 2){ 
				$array = array(
					'music_title' => $this->input->post('music_title'),
					'music_desc' => $this->input->post('music_description'),
					'music_artist' => $this->input->post('music_artist'),
					'music_type' => $this->input->post('music_type'),
					'music_file' => $uploadfile,
					'music_image' =>  $picturefile,
					'user_id' => $checkLogin['id'],
					'category_id' => $this->input->post('music_type'),
          // 'music_image' => $pictureFile,
					'music_created' =>date('Y-m-d H:i:s')
				);
				$this->Common_model_new->insertData("music",$array);
				$return = array('code' => 1,'message' => 'Successfully added');
			}
		}
		echo json_encode($return);	
	}
	// public function edit_music()
	// {
	// 	$data['menuactive'] = $this->uri->segment(1);
	// 	$music_id = base64_decode($this->uri->segment("2"));
	// 	$checkLogin = $this->session->userdata('logged_in');	
	// 	$data['category_name'] = $this->Common_model_new->getAllwhereorderby("cp_music_category",array("music_category_status" => 1),"music_category_name","asc");
	// 	$data['singleData'] = $this->Common_model_new->getsingle("music",array("user_id" => $checkLogin['id'],"music_id" => $music_id));
	// 	$this->template->set('title', 'Music Detail');
	// 	$this->template->load('user_dashboard_layout', 'contents', 'edit_music',$data);
	// }
	public function ajax_music_edit()
	{
		$music_id = $this->input->post('music_id');
		$checkLogin = $this->session->userdata('logged_in');
		$singleData = $this->Common_model_new->getsingle("music",array("music_id" => $music_id));
		$config1['upload_path'] = 'uploads/music/';
		$config1['allowed_types'] = 'mp3';
           //$data['max_size'] = '5000';
		$config1['encrypt_name'] = true;
		$this->load->library('upload', $config1);
		if(!empty($_FILES["picture"]["name"])){ 
			if ($this->upload->do_upload('picture')) { 
				$attachment_data = array('upload_data' => $this->upload->data());
				$uploadfile = $attachment_data['upload_data']['file_name'];
			// $return = array('code' => 1,'message' => 'Successfully added');	
			}else{ 
				$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
			}
		}
		$config2['upload_path'] = 'uploads/music/image/';
		$config2['allowed_types'] = 'jpg|png|gif|jpeg';
           //$data['max_size'] = '5000';
		$config2['encrypt_name'] = true;
		$this->upload->initialize($config2);
		if(!empty($_FILES["pictureFile"]["name"])){ 
			if ($this->upload->do_upload('pictureFile')) {
				$attachment_data_image = array('upload_data' => $this->upload->data());
				$picturefile = $attachment_data_image['upload_data']['file_name'];
      //  $return = array('code' => 1,'message' => 'Successfully added');	
				/*For image resize*/
				$this->load->library('image_lib');
				$configer =  array(
					'image_library'   => 'gd2',
					'source_image'    =>  $attachment_data_image['upload_data']['full_path'],
					'maintain_ratio'  =>  TRUE,
					'width'           =>  150,
					'height'          =>  150,
				);
				$this->image_lib->clear();
				$this->image_lib->initialize($configer);
				$this->image_lib->resize();
			}else{ 
				$return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
			}
		}
		if(!empty($picturefile)){ 
			$picfile = $picturefile;
		}else{ 
			$picfile = $singleData->music_image;
		}
		if(!empty($uploadfile)){ 
			$upfile = $uploadfile;
		}else{
			$upfile = $singleData->music_file;
		}
		if(!empty($upfile) && !empty($picfile)){
			if($return['code'] != 2){ 
				$array = array(
					'music_title' => $this->input->post('music_title'),
					'music_desc' => $this->input->post('music_description'),
					'music_artist' => $this->input->post('music_artist'),
					'music_type' => $this->input->post('music_type'),
					'music_file' => $upfile,
					'music_image' =>  $picfile,
					'user_id' => $checkLogin['id'],
					'category_id' => $this->input->post('music_type'),
          // 'music_image' => $pictureFile,
					'music_created' =>date('Y-m-d H:i:s')
				);
				$this->Common_model_new->updateData("music",$array,array('music_id' => $this->input->post("music_id")));
				$return = array('code' => 1,'message' => 'Successfully updated');
			}
		}
		echo json_encode($return);
	}
	/*public function play_music()
	{
		$music_id = $this->input->post("music_id");
		$data['play_music'] = $this->Common_model_new->getsingle("music",array("music_id" => $music_id));
		$this->load->view('play_music',$data);
	}*/
	public function download_music()
	{
		$music_id =  $this->uri->segment(4);
		//$music_id = $this->input->post("music_id");
		$this->load->helper('download');
		$music = $this->Common_model_new->getsingle("music",array("music_id" => $music_id));
		$mp3 = base_url().'uploads/music/'.$music->music_file;
		$fileName = $music->music_file;
		$path = 'uploads/music/';
		$file = $path.$fileName;
		force_download($file, NULL); 
	}
	public function play_music()
	{ 
		$music_id = $this->input->post("music_id");
		$checkLogin = $this->session->userdata('logged_in');
		$data['play_music'] = $this->Common_model_new->getsingle("music",array("music_id" => $music_id));
		$data['check_music'] = $this->Common_model_new->getsingle("cp_music_playlist",array("music_id" => $music_id,"user_id" => $checkLogin['id']));
		if(empty($data['check_music'])){
			$array = array(
				'music_id' => $music_id,
				'user_id' => $checkLogin['id'],
				'music_playlist_create_date' => date("Y-m-d H:i:s")
			);
			$this->Common_model_new->insertData("cp_music_playlist",$array);
		}
		if(!empty($data['play_music'])){
			$count = $data['play_music']->music_view + 1;
		}else{
			$count = 1;
		}
		$view_count_array = array(
			'music_view' => $count,
		);
		$this->Common_model_new->updateData('music',$view_count_array,array("music_id" => $music_id));
		$data['playlist_music'] = $this->Common_model_new->jointwotablenn("cp_music_playlist", "music_id", "music", "music_id",array("cp_music_playlist.user_id" => $checkLogin['id']),"*","cp_music_playlist.music_playlist_id","desc");
		$this->load->view('play_music',$data);
	}
	public function music_newdesign()
	{
		$data['menuactive'] = $this->uri->segment(1);
		$this->template->set('title', 'Music');
		$this->template->load('user_dashboard_layout_music1', 'contents', 'music-newdesign',$data);
	}
	public function view_all_music_newdesign()
	{
		$data['menuactive'] = $this->uri->segment(1);
		$this->template->set('title', 'Music');
		$this->template->load('user_dashboard_layout_music1', 'contents', 'view_all_music_newdesign',$data);
	}
	public function view_all_music()
	{
		$data['menuactive'] = $this->uri->segment(1);
		$checkLogin = $this->session->userdata('logged_in');
		$data['playlist_music'] = $this->Common_model_new->jointwotablenn("cp_music_playlist", "music_id", "music", "music_id",array("cp_music_playlist.user_id" => $checkLogin['id']),"*","cp_music_playlist.music_playlist_id","desc");
		$data['single_playlist_music'] = $this->Common_model_new->getsingleorderlimit1("cp_music_playlist", "music_id", "music", "music_id","","*","cp_music_playlist.music_playlist_id","desc");
		// echo $this->uri->segment("2");
		// die;		
		$segment = $this->uri->segment("2");
		$data['segment'] = $segment;
		if($segment == 'my_music'){
			$data['single_music_image'] = $this->Common_model_new->getsinglelimit1("music",array("user_id" => $checkLogin['id']),"music_id","desc");
          //echo $this->db->last_query();die;
			$data['view_all_music'] = $this->Common_model_new->getAllwhereorderby("music",array("user_id" => $checkLogin['id']),'music_id','desc');
		// $this->calculateFileSize('uploads/music/Kalimba.mp3');
		}
		if($segment == 'favorite_music'){
			$data['single_music_image'] = $this->Common_model_new->jointwotablenn("cp_music_favorite", "music_id", "music", "music_id",array("cp_music_favorite.user_id" => $checkLogin['id']),"*","cp_music_favorite.music_fav_id","desc");
			$data['view_all_music'] = $this->Common_model_new->jointwotablenn("cp_music_favorite", "music_id", "music", "music_id",array("cp_music_favorite.user_id" => $checkLogin['id']),"*","cp_music_favorite.music_fav_id","desc");
		// $this->calculateFileSize('uploads/music/Kalimba.mp3');
		}
		if($segment == 'popular_music'){
			$data['single_music_image'] = $this->Common_model_new->getsinglelimit1wh("music","music_view","desc");
			$data['view_all_music'] = $this->Common_model_new->getAllor("music","music_view","desc");
		}
		if($segment == 'recent_music'){
			$data['single_music_image'] = $this->Common_model_new->getsinglelimit1wh("music","music_id","desc");		
			$data['view_all_music'] = $this->Common_model_new->getAllor("music","music_id","desc");;
		}
		if($segment != 'recent_music' && $segment != 'popular_music' && $segment != 'my_music' && $segment != 'favorite_music'){  
			$data['single_music_image'] = $this->Common_model_new->getsinglelimit1("music",array("category_id" => $segment),"music_id","desc");	
			$data['view_all_music'] = $this->Common_model_new->getAllwhereorderby("music",array("category_id" => $segment),'music_id','desc');
			$data['view_cate_name'] = $this->Common_model_new->getsingle("cp_music_category",array("music_category_id" => $segment));
		//echo '<pre>';print_r($data['view_all_music']);die;
		}
		$this->template->set('title', 'Music');
		$this->template->load('user_dashboard_layout_music1', 'contents', 'view_all_music',$data);
	}
	public function view_all_categories()
	{
		$data['menuactive'] = $this->uri->segment(1);
		$checkLogin = $this->session->userdata('logged_in');
		$data['view_all_category'] = $this->Common_model_new->getAllwhereorderby("cp_music_category",array("music_category_status" => 1),"music_category_id","DESC");
		$data['playlist_music'] = $this->Common_model_new->jointwotablenn("cp_music_playlist", "music_id", "music", "music_id",array("cp_music_playlist.user_id" => $checkLogin['id']),"*","cp_music_playlist.music_playlist_id","desc");
		$data['single_playlist_music'] = $this->Common_model_new->getsingleorderlimit1("cp_music_playlist", "music_id", "music", "music_id","","*","cp_music_playlist.music_playlist_id","desc");
		$this->template->set('title', 'Music Detail');
		$this->template->load('user_dashboard_layout_music1', 'contents', 'view_all_categories',$data);
	}
	public function remove_my_music()
	{
		$music_id = $this->input->post("music_id");
		$checkLogin = $this->session->userdata('logged_in');
		$this->Common_model_new->delete("music",array("music_id" => $music_id,"user_id" => $checkLogin["id"]));
		$this->Common_model_new->delete("cp_music_playlist",array("music_id" => $music_id,"user_id" => $checkLogin["id"]));
		$data['check_music'] = $this->Common_model_new->getAllMyMusic();
		if(!empty($data['check_music'])){
			echo '1';
		}else{
			echo '2';
		}
		//$this->load->view("ajax_remove_my_music",$data);
	}
// public function remove_play_music()
// {
// 	$play_id = $this->input->post("music_id");
// 	$checkLogin = $this->session->userdata('logged_in');
// 	$this->Common_model_new->delete("cp_music_playlist",array("music_playlist_id" => $play_id));
// }
	public function remove_play_music()
	{
		$play_id = $this->input->post("music_id");
		$checkLogin = $this->session->userdata('logged_in');
		$this->Common_model_new->delete("cp_music_playlist",array("music_playlist_id" => $play_id));
		$data['check_music'] = $this->Common_model_new->getAllwhere("cp_music_playlist",array("user_id" => $checkLogin['id']));
//print_r($data['check_music']);die;
		if(!empty($data['check_music'])){
			echo '1';
		}else{
			echo '2';
		}	
	}
	public function image_change()
	{
		$music_id = $this->input->post("music_id");
		$change_image_array = $this->Common_model_new->getsingle("music",array("music_id" => $music_id));
		$src = base_url().'uploads/music/image/'.$change_image_array->music_image;
		$change_image = '<div class="sng-crnt">
		<img alt="" src="'.$src.'">
		</div>
		<div class="sng-txt">
		<h4>'.$change_image_array->music_title.'</h4>
		<p>'.$change_image_array->music_artist.'</p>
		</div>';
		echo $change_image;
	}
	public function getinfo_music()
	{
		$data['menuactive'] = $this->uri->segment(1);
		$music_id = base64_decode($this->uri->segment("2"));
		$data['music_id'] = $music_id;
		$checkLogin = $this->session->userdata('logged_in'); 
		$data['getinfo_music'] = $this->Common_model_new->getsingle("music",array("music_id" => $music_id));
		$data['check_info_music'] = $this->common_model_new->getsingle("cp_music_favorite",array("music_id" => $music_id,"user_id" => $checkLogin['id']));
		$data['like_music'] = $this->Common_model_new->jointwotablenn("music", "category_id", "cp_music_category", "music_category_id",array("cp_music_category.music_category_id" => $data['getinfo_music']->category_id,"cp_music_category.user_id"),"*","cp_music_category.music_category_id","desc");
		$data['playlist_music'] = $this->Common_model_new->jointwotablenn("cp_music_playlist", "music_id", "music", "music_id",array("cp_music_playlist.user_id" => $checkLogin['id']),"*","cp_music_playlist.music_playlist_id","desc");
		$data['single_playlist_music'] = $this->Common_model_new->getsingleorderlimit1("cp_music_playlist", "music_id", "music", "music_id","","*","cp_music_playlist.music_playlist_id","desc");
		$this->template->set('title', 'Music');
		$this->template->load('user_dashboard_layout_music1', 'contents', 'getinfo_music',$data);
	}
	public function favourite_music()
	{
		$music_id = $this->input->post("music_id");
		$checkLogin = $this->session->userdata('logged_in');
		$check_info_music = $this->common_model_new->getsingle("cp_music_favorite",array("music_id" => $music_id,"user_id" => $checkLogin['id']));
		if(!empty($check_info_music)){
			$this->Common_model_new->delete("cp_music_favorite",array("music_id" => $music_id,"user_id" => $checkLogin['id']));
			echo '0';
		}else{
			$array = array("user_id" => $checkLogin["id"],"music_id" => $music_id,"fav" => 1,"music_fav_created" => date("Y-m-d H:i:s"));
			$this->Common_model_new->insertData("cp_music_favorite",$array);
			echo '1';
		}
	}
	public function view_all_music_like()
	{
		$segment = $this->uri->segment("2");
		$segmentsec = $this->uri->segment("3");
		$segment2 = str_replace("%20"," ",$segmentsec);
		$data['menuactive'] = $this->uri->segment(1);
		$checkLogin = $this->session->userdata('logged_in');
		$data['playlist_music'] = $this->Common_model_new->jointwotablenn("cp_music_playlist", "music_id", "music", "music_id",array("cp_music_playlist.user_id" => $checkLogin['id']),"*","cp_music_playlist.music_playlist_id","desc");
		$data['single_playlist_music'] = $this->Common_model_new->getsingleorderlimit1("cp_music_playlist", "music_id", "music", "music_id","","*","cp_music_playlist.music_playlist_id","desc");
		$data['segment'] = $segment;
		if($segment == 'my_music'){
			$data['single_music_image'] = $this->Common_model_new->getsinglelimit1("music",array("user_id" => $checkLogin['id']),"music_id","desc");
          //echo $this->db->last_query();die;
			$data['view_all_music'] = $this->Common_model_new->getlikemymmusic($segment2,$checkLogin['id']);
		// $this->calculateFileSize('uploads/music/Kalimba.mp3');
		}
		if($segment == 'popular_music'){
			$data['single_music_image'] = $this->Common_model_new->getsinglelimit1wh("music","music_view","desc");
			$data['view_all_music'] = $this->Common_model_new->getlikepopmusic($segment2);
		}
		if($segment == 'recent_music'){
			$data['single_music_image'] = $this->Common_model_new->getsinglelimit1wh("music","music_id","desc");		
			$data['view_all_music'] = $this->Common_model_new->getlikerecmusic($segment2);
			$data['view_cate_name'] = $this->Common_model_new->getsingle("cp_music_category",array("music_category_id" => $segment));
		}
		$this->template->set('title', 'Music');
		$this->template->load('user_dashboard_layout_music1', 'contents', 'view_all_music_like',$data);
	}
	public function view_all_categories_search()
	{
		$segmentvalue = $this->uri->segment("2");
		$segment2 = str_replace("%20"," ",$segmentvalue);
		$checkLogin = $this->session->userdata('logged_in');
		$data["view_all_category"] = $this->Common_model_new->getcatsearch($segment2);
		$data['segment2'] = $segment2;
		$data['playlist_music'] = $this->Common_model_new->jointwotablenn("cp_music_playlist", "music_id", "music", "music_id",array("cp_music_playlist.user_id" => $checkLogin['id']),"*","cp_music_playlist.music_playlist_id","desc");
		$data['single_playlist_music'] = $this->Common_model_new->getsingleorderlimit1("cp_music_playlist", "music_id", "music", "music_id","","*","cp_music_playlist.music_playlist_id","desc");
		$this->template->set('title', 'Category Detail');
		$this->template->load('user_dashboard_layout_music1', 'contents', 'view_all_categories_search',$data);
	}
	public function view_all_music_search()
	{
		$segment = $this->uri->segment("2");
		$segmentsec = $this->uri->segment("3");
		$segment2 = str_replace("%20"," ",$segmentsec);
		$data['segment2'] = $segment2;
		$data['menuactive'] = $this->uri->segment(1);
		$checkLogin = $this->session->userdata('logged_in');
		$data['playlist_music'] = $this->Common_model_new->jointwotablenn("cp_music_playlist", "music_id", "music", "music_id",array("cp_music_playlist.user_id" => $checkLogin['id']),"*","cp_music_playlist.music_playlist_id","desc");
		$data['single_playlist_music'] = $this->Common_model_new->getsingleorderlimit1("cp_music_playlist", "music_id", "music", "music_id","","*","cp_music_playlist.music_playlist_id","desc");
		$data['segment'] = $segment;
		if($segment == 'my_music'){ 
			$data['single_music_image'] = $this->Common_model_new->getsinglelimit1("music",array("user_id" => $checkLogin['id']),"music_id","desc");
          //echo $this->db->last_query();die;
			$data['view_all_music'] = $this->Common_model_new->getlikemymmusic($segment2,$checkLogin['id']);
		// $this->calculateFileSize('uploads/music/Kalimba.mp3');
		}
		if($segment == 'popular_music'){ 
			$data['single_music_image'] = $this->Common_model_new->getsinglelimit1wh("music","music_view","desc");
			$data['view_all_music'] = $this->Common_model_new->getlikepopmusic($segment2);
		}
		if($segment == 'favorite_music'){ 
			$data['single_music_image'] = $this->Common_model_new->getlikefavmusic($segment2,$checkLogin['id']);
			$data['view_all_music'] = $this->Common_model_new->getlikefavmusic($segment2,$checkLogin['id']);
		}
		if($segment == 'recent_music'){ 
			$data['single_music_image'] = $this->Common_model_new->getsinglelimit1wh("music","music_id","desc");		
			$data['view_all_music'] = $this->Common_model_new->getlikerecmusic($segment2);
		}
		if($segment != 'recent_music' && $segment != 'popular_music' && $segment != 'my_music' && $segment != 'favorite_music'){  
			$data['single_music_image'] = $this->Common_model_new->getsinglelimit1("music",array("category_id" => $segment),"music_id","desc");	
			$data['view_all_music'] = $this->Common_model_new->getlikecat($segment2,$segment);
		//echo $this->db->last_query();die;
		//echo '<pre>';print_r($data['view_all_music']);die;
		}
		$this->template->set('title', 'Music');
		$this->template->load('user_dashboard_layout_music1', 'contents', 'view_all_music_like',$data);
	}	
	public function add_to_playlist()
	{
		$bodytype = $this->input->post("bodytype");
		$explode = explode(",",$bodytype);
		$checkLogin = $this->session->userdata('logged_in');
		foreach($explode as $music){
			$check_table = $this->Common_model_new->getsingle("cp_music_playlist",array("music_id" => $music,"user_id" => $checkLogin['id']));
			if($check_table->music_id != $music){
				$insert = $this->Common_model_new->insertData("cp_music_playlist",array("music_id" => $music,"user_id" => $checkLogin['id']));
			}
		}
	}

	public function music_dashboard(){
    $data['menuactive'] = '';
    $checkLogin = $this->session->userdata('logged_in');
	$user_id = $checkLogin['id'];
    if(!empty($checkLogin))
    {
      $data['menuactive'] = $this->uri->segment(1);
     
      unset($_POST);
	  $data['musicArray'] = $this->Music_model->getAllwhere("setting_butns",array("btnname" =>'Music',"user_id"=>$user_id));

      $data['app_list'] = $this->common_model_new->getAllwhere("app_list",array('status' => 1,'use_for'=>"music"));
      $data['music_list'] = $this->common_model_new->getAllwhere("cp_music",array('user_id'=>$this->session->userdata('logged_in')['id']));
      $this->template->set('title', 'Dashboard Game');

      $this->template->load('new_user_dashboard_layout', 'contents', 'music', $data);
    } 
    else{

      redirect('login');
  
    } 
  }

  public function add_music()
  {
    if(count($_POST)>0){
        if($_POST['type']==1){
      
        
        $array =array(
           'user_id' => $this->session->userdata('logged_in')['id'],
           'music_type' => $this->input->post('music_type'),
           'button_name' => $this->input->post('button_name'),
           'url'=>$this->input->post('url'),
           'app_name' => $this->input->post('app_name'),
           'created' => date('Y-m-d H:i:s'));
       }

      $check_music = $this->Common_model_new->getsingle("cp_music",array("button_name" => $this->input->post('button_name'),"user_id" => $this->session->userdata('logged_in')['id']));
      if(!empty($check_music)){
      	unset($_POST);
      	$this->session->set_flashdata('success', "Music already added");
        echo json_encode(array("statusCode"=>200,"msg"=>"music already added"));
        die();
      }
       $insert = $this->Common_model_new->insertData('cp_music',$array);
       if($insert){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        $this->session->set_flashdata('success', "Music added successfully");
        echo json_encode(array("statusCode"=>200,"msg"=>"music added successfully"));
        die();
      }

    }
  }

  public function edit_music()
  {
    if(count($_POST)>0){
        if($_POST['type']==2){
        $music_id = $this->input->post('id');
        
        $array =array(
           'user_id' => $this->session->userdata('logged_in')['id'],
           'music_type' => $this->input->post('music_type'),
           'button_name' => $this->input->post('button_name'),
           'url'=>$this->input->post('url'),
           'app_name' => $this->input->post('app_name'),
           'created' => date('Y-m-d H:i:s'));
       }

       $check_music = $this->Common_model_new->allexcludeone("cp_music",array("button_name" => $this->input->post('button_name'),"user_id" => $this->session->userdata('logged_in')['id']),$music_id);
      if(!empty($check_music)){
      	unset($_POST);
      	$this->session->set_flashdata('success', "Music already added");
        echo json_encode(array("statusCode"=>200,"msg"=>"music already added"));
        die();
      }

       $update = $this->Common_model_new->updateData('cp_music',$array,array('id' => $music_id));
       if($update){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        $this->session->set_flashdata('success', "Music updated successfully");
        echo json_encode(array("statusCode"=>200,"msg"=>"music updated successfully"));
        die();
      }

    }
  }

  public function delete_music()
  {
    if(count($_POST)>0){
        if($_POST['type']==3){
        $music_id = $this->input->post('id');

       $update = $this->Common_model_new->delete('cp_music',array('id' => $music_id));
       if($update){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        echo json_encode(array("statusCode"=>200,"msg"=>"music deleted successfully"));
        die();
      }

    }
  }
}
} 