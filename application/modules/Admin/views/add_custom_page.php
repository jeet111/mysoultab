<div class="container-fluid">

    <div class="dash-counter">

        <div class="Schedule_main_one">

            <div class="users-main">



                <h2><?php if ($this->uri->segment('3')) { ?>Edit Feature <?php } else { ?>Add New Page <?php } ?></h2>



                <div class="btn_topBack">

                    <a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">

                        Back

                    </a>

                </div>



                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" name="pageNewName" id="pageNewName">

                    <?php if ($this->session->flashdata('success')) { ?>

                        <div class="alert alert-success message">

                            <button type="button" class="close" data-dismiss="alert">x</button>

                            <?php echo $this->session->flashdata('success'); ?>
                        </div>

                    <?php } ?>

                    <div class="row">

                        <div class="col-md-12">

                            <div class="form-group">

                                <label class="control-label col-md-4" for="">Page Name:<span class="field_req">*</span></label>

                                <div class="col-md-8">

                                    <input type="text" placeholder="Page Name" class="form-control" name="page_name" id="page_name" value="">

                                    <?php echo form_error('page_name'); ?>



                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-12">

                            <div class="form-group">

                                <label class="control-label col-md-4" for="">Page Description:<span class="field_req">*</span></label>

                                <div class="col-md-8">


                                    <textarea class="form-control" id="editor1" name="editor1" data-sample-short></textarea>



                                    <?php echo form_error('editor1'); ?>



                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-sm-12">

                                    <input type="submit" class="btn btn-default up_but" id="postdata" class="form-control" name="submit" value="Submit">


                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            </div>











        </div>

    </div>

</div>


<!--Added by 95 on 5-2-2019 For Doctor category for validation-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#pageNewName").validate({

            rules: {

                editor1: {

                    required: true,

                    minlength: 5,

                    maxlength: 30,

                    lettersonly: true

                },





            },

            messages: {



                editor1: {

                    required: "Enter your message 3-20 characters",

                }





            },



        });



    });
</script>



<script src="https://cdn.ckeditor.com/4.14.0/standard-all/ckeditor.js"></script>


<script type="text/javascript">
    $(document).ready(function() {

        $("#pageNewName").validate({

            rules: {

                editor1: "required",

                page_name: "required",




            },

            messages: {


                page_name: "Plesase enter feature name.",

                editor1: "Plesase enter feature description.",

            }



        });



    });



    var editor1 = CKEDITOR.replace('editor1', {

        allowedContent: true,

        extraAllowedContent: 'div',

        height: 460

    });

    editor1.on('instanceReady', function() {

        // Output self-closing tags the HTML4 way, like <br>.

        this.dataProcessor.writer.selfClosingEnd = '>';



        // Use line breaks for block elements, tables, and lists.

        var dtd = CKEDITOR.dtd;

        for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {

            this.dataProcessor.writer.setRules(e, {

                indent: true,

                breakBeforeOpen: true,

                breakAfterOpen: true,

                breakBeforeClose: true,

                breakAfterClose: true

            });

        }

        // Start in source mode.

        this.setMode('source');

    });
</script>