  <?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  class Opetion_management extends MX_Controller {
    private $staff_permission = array();
    private $access_permission = true;
    public function __construct(){   
      parent:: __construct();
      $this->load->library('session');
      $this->load->library('form_validation');
      $this->load->model('pharma_model');
      $this->load->model('login_model');
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));
      $user_id = $this->session->userdata('logged_ins')['id'];
      if(empty($user_id)) {
        redirect('admin/login');
      }

      if ($this->session->userdata('logged_ins')['user_role'] == 5) {
        $uid = $this->session->userdata('logged_ins')['id'];
        $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
        if (!empty($permission_list)) 
        {
         foreach ($permission_list as $k => $v) 
         {
          $this->staff_permission[$v['permission_id']] = $v;
        }
      }
    }
  }

  public function permission_access($permission_id=9)
  {
    if ($this->session->userdata('logged_ins')['user_role'] != 1) 
    {
     if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
     {
      $this->access_permission = false;
    }
  }
}

/*Created by 95 for show all movie*/
public function social_option_list()
{

  $data['menuactive'] = $this->uri->segment(2);
  $data['appData'] = $this->pharma_model->getAllwhereorderby('app_list',array('status'=>1),'name', 'ACE');


  $data['staff_permission'] = $this->staff_permission;

  $this->permission_access(9);
  if ($this->access_permission) {
   $this->template->set('title', 'Option management');
   $this->template->load('admin_dashboard_layout', 'contents', 'app_list', $data);
 } else {
   $this->template->set('title', 'Admin Panel - Staff');
   $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
 }
}


/*Created by 95 for delete pharmacompany*/
public function delete_app($company_id='')
{
  $this->pharma_model->delete('app_list',array('id' =>$company_id));
  $this->session->set_flashdata('success', 'Deleted successfully.');
  redirect('admin/app_list');
}


/*Created by 95 for add pharmacompany*/
public function add_app()
{


  $data['menuactive'] = $this->uri->segment(2);
  $this->form_validation->set_rules('app_name','Company Name','required');
  $this->form_validation->set_rules('url','Company Email','required');
  
	$data['use_for'] = array(	
		'Game'							=> 'Game',		
		'Email'             => 'Email',
		'Social'            => 'Social',
		'Transportation'    => 'Transportation',
		'Pharmacy'          => 'Pharmacy',
		'Music'             => 'Music',
		'Movie'             => 'Movie',
		'Weather'           => 'Weather',
		'Internet'          => 'Internet',
		'News'              => 'News',
		'Yoga News'         => 'Yoga News',
		'Spirituality News' => 'Spirituality News',
		//'Call'              => 'Call'
	);

  if ($this->form_validation->run() == TRUE) 
  {  
    if($_POST['submit']){


      //Check whether user upload picture
      if(!empty($_FILES['icon']['name'])){
        $config['upload_path'] = 'uploads/social/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['icon']['name'];
                //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('icon')){
          $uploadData = $this->upload->data();
          $picture = $uploadData['file_name'];
          $iconurl = base_url().'uploads/social/'.$picture;
          
        }else{
          $iconurl = '';

        }
      }else{
        $iconurl = '';

      }

			

      $data = array(
        'name' => $this->input->post('app_name'),
        'url' => $this->input->post('url'),
        'use_for' => $this->input->post('use_for'),
        'icon' => $iconurl,
        'created' =>date('Y-m-d H:i:s')
      );

      $this->pharma_model->insertData('app_list',$data);
      $this->session->set_flashdata('success', 'App added successfully.');
      redirect('admin/app_list');
    }
  }

  $data['staff_permission'] = $this->staff_permission;

  $this->permission_access(9);
  if ($this->access_permission) {
    $this->template->set('title', 'Option Management');
    $this->template->load('admin_dashboard_layout', 'contents', 'add_app',$data);
  } else {
   $this->template->set('title', 'Admin Panel - Staff');
   $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
 }
}



public function generateRandomString($length = 12) {
 $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
 $charactersLength = strlen($characters);
 $randomString = '';
 for ($i = 0; $i < $length; $i++) {
   $randomString .= $characters[rand(0, $charactersLength - 1)];
 }
 return $randomString;
}


/*Created by 95 for edit movie*/
public function edit_app($company_id='')
{
 $data['menuactive'] = $this->uri->segment(2);
 $this->form_validation->set_rules('app_name','Company Name','required');
 $this->form_validation->set_rules('url','Company Email','required');

	$data['use_for'] = array(	
		'Game'							=> 'Game',		
		'Email'             => 'Email',
		'Social'            => 'Social',
		'Transportation'    => 'Transportation',
		'Pharmacy'          => 'Pharmacy',
		'Music'             => 'Music',
		'Movie'             => 'Movie',
		'Weather'           => 'Weather',
		'Internet'          => 'Internet',
		'News'              => 'News',
		'Yoga News'         => 'Yoga News',
		'Spirituality News' => 'Spirituality News',
		//'Call'              => 'Call'
	);

 if ($this->form_validation->run() == TRUE) 
 {  
   if($_POST['submit']){

    //Check whether user upload picture
    if(!empty($_FILES['icon']['name'])){
      $config['upload_path'] = 'uploads/social/';
      $config['allowed_types'] = 'jpg|jpeg|png|gif';
      $config['file_name'] = $_FILES['icon']['name'];
                //Load upload library and initialize configuration
      $this->load->library('upload',$config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('icon')){
        $uploadData = $this->upload->data();
        $picture = $uploadData['file_name'];
        $iconurl = base_url().'uploads/social/'.$picture;

      }else{
        $iconurl = '';

      }
    }else{
      $iconurl = '';

    }

    if(!empty($iconurl)){
			
      $data = array(
        'name' => $this->input->post('app_name'),
        'url' => $this->input->post('url'),
        'use_for' => $this->input->post('use_for'),
        'icon' => $iconurl,
        'created' =>date('Y-m-d H:i:s')
      );
    }else{
      $data = array(
        'name' => $this->input->post('app_name'),
        'url' => $this->input->post('url'),
        'use_for' => $this->input->post('use_for'),
      //'icon' => $iconurl,
        'created' =>date('Y-m-d H:i:s')
      );
    }

    
    $this->pharma_model->updateData('app_list',$data,array('id'=>$company_id));
    $this->session->set_flashdata('success', 'App updated successfully.');
    redirect('admin/app_list');
  }
}

$use_for= array();

$data['editApp']=$this->pharma_model->getsingle('app_list',array('id'=>$company_id));

$data['staff_permission'] = $this->staff_permission;

$this->permission_access(9);
if ($this->access_permission) { 
  $this->template->set('title', 'Option Management');
  $this->template->load('admin_dashboard_layout','contents','edit_app',$data);
} else {
  $this->template->set('title', 'Admin Panel - Staff');
  $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
}
}


public function view_pharmacompany($company_id='')
{
 $data['menuactive'] = $this->uri->segment(2);
 $data['viewPhamra']=$this->pharma_model->getsingle('pharma_company',array('id'=>$company_id));
 
 $data['staff_permission'] = $this->staff_permission;

 $this->permission_access(9);
 if ($this->access_permission) {
  $this->template->set('title', 'Pharma Company');
  $this->template->load('admin_dashboard_layout', 'contents', 'view_pharma', $data);
} else {
  $this->template->set('title', 'Admin Panel - Staff');
  $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
}
}



}
