<style>
  .text_heading {
    border-bottom: 0;
  }

  .btn.btn-primary {
    margin: 0px 0 14px !important;
}


</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<script>
	var userEmail = "<?php echo $this->session->userdata('logged_in')['email']; ?>";
</script>
<script src="<?php echo base_url(); ?>assets/new_user_dashboard/js/sinch.min.js"></script>

    <div class=" text_heading">
    <div id="msg_show"></div>
  
      <h3><i class="fa fa-phone-square" aria-hidden="true"></i> Call</h3>
      <div class="tooltip-2">
        <h2>Display call button on Mobile App

          <?php
          foreach ($btnsetingArray as $res) {
            if ($res->settings == 1) { ?>
              <label class="switch ">
                <input type="checkbox" checked data-btn="<?php echo 'call' ?>" class="updateStatus">
                <span class="slider round"></span>
              </label>


      </div>
    <?php } else { ?>
      <label class="switch space">
        <input type="checkbox" data-btn="<?php echo 'call' ?>" class="updateStatus">
        <span class="slider round"></span>
      </label>


    <?php } ?>
  <?php }
  ?>
  </h2>

  <div class="rem_add"><a href="<?php echo base_url(); ?>" class="btn btn-primary">Start Call</a></div>

    </div>

      <section class="content photo-list">

      <div class="photo-listMain reminder_listMain">

         

          <div class="table-responsive tb_swt">



              <table class="table table-hover table-bordered table-mailbox send-user-mail tab_medi" id="sampleTable">

                  <thead>

                      <tr>

                          <th> <input type="checkbox" id="checkall" name="">

<a href="javascript:void(0);" name="btn_delete" id="btn_delete" class="btn_delete"> <i aria-hidden="true" class="fa fa-trash-o"></i></a> Delete All

</th>
                          <th>Description</th>

                          <th>Date Received</th>

                          <th>Time Received</th>

                          <!-- <th>Medicine Time</th> -->

                          <th>Status</th>

                          <th>Action</th>

                      </tr>

                  </thead>

                  <tbody>

                      <tr>
                        <td></td>
                        <td>Pick Up Grocery</td>
                        <td>12-19-2020</td>
                        <td>7:40 PM</td>
                        <td>DISMISS</td>
                        <td></td>
                      </tr>



                  </tbody>

              </table>

          </div>
    </div>
    <div id="auth" class="col-md-6 hide">
            <h2>Login or register</h2>
            <form id="userForm">
                <input id="username" placeholder="username"><br>
                <input id="password" type="password" placeholder="password"><br>
                <button id="loginUser">Login</button>
                <button id="createUser">Create</button>
            </form>
        </div>
        
    <div id="call" class="col-md-6 hide">
        <h2>Make a call</h2>
        <input type="text" id="phoneNumber" />
        <button id="callNumber" class="btn btn-primary">Call</button>
        <button id="hangupCall" class="btn btn-primary" style="display: none">Hangup</button>
        <audio id="incoming" autoplay></audio>
        <audio id="ringtone" src="ringtone.wav" loop ></audio>
        <div id="status"></div>
    </div>
    <div class="col-md-12" id="error"></div>
   
   </section>
    </div>
      </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script type="text/javascript">
         $(document).on('click', '.updateStatus', function() {
    var update_status = $(this).attr('data-btn');
    var update_st = $(this).attr('data-st');
    //alert(update_status+'-'+update_st);
    $.ajax({
      url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
      type: "POST",
      data: {
        'btn': update_status
      },
      dataType: "json",
      success: function(data) {
        $("#msg_show").html("<div class='alert alert-success'>" + data.status + "</div>");

     
      }
    });
        //2. Set up sinchClient
        var sinchClient = new SinchClient({
            applicationKey: 'bbc9fe1c-0017-4f8e-8228-3d14cd29e83d',
            applicationSecret: '6Q3oGPpRb0eCk5j3aX+60Q==', //WARNING: This is insecure, only for demo easy/instant sign-in where we don't care about user identification
            capabilities: { calling: true },
            onLogMessage: function (message) {
                console.log(message);
            },
        });
        //3. Create user and start sinch for that user
        $('button#createUser').on('click', function (event) {
            event.preventDefault();
            clearError();
            $('button#loginUser').attr('disabled', true);
            $('button#createUser').attr('disabled', true);
            var signUpObj = {};
            signUpObj.username = $('input#username').val();
            signUpObj.password = $('input#password').val();
            //Use Sinch SDK to create a new user
            sinchClient.newUser(signUpObj, function (ticket) {
                //On success, start the client
                sinchClient.start(ticket, function () {
                    //On success, show the UI
                    showUI();
                }).fail(handleError);
            }).fail(handleError);
        });
        // 4 Login user and and start client
        $('button#loginUser').on('click', function (event) {
            event.preventDefault();
            clearError();
            $('button#loginUser').attr('disabled', true);
            $('button#createUser').attr('disabled', true);
            var signInObj = {};
            signInObj.username = 'bbc9fe1c-0017-4f8e-8228-3d14cd29e83d';
	          signInObj.password = '6Q3oGPpRb0eCk5j3aX+60Q==';
	
            //signInObj.username = $('input#username').val();
            //signInObj.password = $('input#password').val();

            //Use Sinch SDK to authenticate a user
            sinchClient.start(signInObj, function () {
                //On success, show the UI
                showUI();
            }).fail(handleError);
        });

        var callClient;
        var call;
        $('#callNumber').click(function (event) {
            event.preventDefault();
            callClient = sinchClient.getCallClient();
            $('#callNumber').attr('disabled', 'disabled');
            $('#phoneNumber').attr('disabled', 'disabled');
            $("#hangupCall").show();

            call = callClient.callPhoneNumber($('#phoneNumber').val());
            call.addEventListener(callListeners);
        });

        $('#hangupCall').click(function (event) {
            event.preventDefault();
            call && call.hangup();
        });
        var callListeners = {
            //1. start playing sound and display status
            onCallProgressing: function (call) {
                $('#ringtone').prop("currentTime", 0);
                $('#ringtone').trigger("play");
                $('#status').append('<div id="stats">Ringing...</div>');
            },
            //2. Call is pickedup (or voicemail)
            onCallEstablished: function (call) {
                $('audio#incoming').attr('src', call.incomingStreamURL);
                $('audio#ringtone').trigger("pause");

                //Report call stats
                var callDetails = call.getDetails();
                $('#status').append('<div id="stats">Answered at: ' + new Date(callDetails.establishedTime) + '</div>');
            },
            //3. Call ended, stop all sounds, and revert back
            onCallEnded: function (call) {
                // you need to make sure the ringback is not playing incase the call ended from progress (no answer)
                $('audio#ringtone').trigger("pause");
                $('audio#incoming').attr('src', '');
                $('#callNumber').removeAttr('disabled');
                $('input#phoneNumber').removeAttr('disabled');
                $("#hangupCall").hide();
                //Report call stats
                var callDetails = call.getDetails();
                $('#status').append('<div id="stats">Ended: ' + new Date(callDetails.endedTime) + '</div>');
                $('#status').append('<div id="stats">Duration (s): ' + callDetails.duration + '</div>');
                $('#status').append('<div id="stats">End cause: ' + callDetails.endCause + '</div>');
                if (call.error) {
                    $('div#callLog').append('<div id="stats">Failure message: ' + call.error.message + '</div>');
                }
            }
        };

        var handleError = function (error) {
            //Enable buttons
            $('button#createUser').prop('disabled', false);
            $('button#loginUser').prop('disabled', false);
            //Show error
            $('#error').text(error.message);
            $('#error').show();
        }
        var clearError = function () {
            $('#error').text('');
            $('#error').show();
        }

        var showUI = function () {
            $('#auth').hide();
            $('#call').show();
        }
        $('button#logOut').click();
        $('button#loginUser').click();
      });
    </script>