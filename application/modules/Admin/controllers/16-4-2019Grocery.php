<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Grocery extends MX_Controller {

  public function __construct(){   

    parent:: __construct();

    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->model('Common_model');
    $this->load->helper(array('url'));
    $this->load->helper(array('form'));
	$this->load->library("pagination");

    $user_id = $this->session->userdata('logged_in')['id'];
    if(empty($user_id)) {
      redirect('admin/login');
    }

  }
  
  public function favorite_grocery_list()
  {
	   $data['menuactive'] = $this->uri->segment(2);
	   $data['favorite_grocery_data'] = $this->Common_model->jointwotablenn('cp_fav_grocery', 'grocery_id', 'cp_grocery', 'grocery_id','','*','fav_grocery_id','desc');
	
	   $this->template->set('title', 'Grocery Management');

       $this->template->load('admin_dashboard_layout', 'contents' , 'favorite_grocery_list', $data);
  }
  
  public function view_grocery()
  {
	  $data['menuactive'] = $this->uri->segment(2);
	  
	  $segment = $this->uri->segment('3');
	  
	  $data['single_grocery'] = $this->Common_model->getsingle("cp_grocery",array("grocery_id" => $segment));
	  $data['username'] = $this->Common_model->getsingle("cp_users",array("id" => $data['single_grocery']->user_id)); 
	
	  $this->template->set('title', 'View grocery');
      $this->template->load('admin_dashboard_layout', 'contents' , 'view_grocery', $data);
	  
  }
  
   public function delete_fav_grocery()
  {
	  $data['menuactive'] = $this->uri->segment(2);
	  $segment = $this->uri->segment('3');
	  $this->Common_model->delete("cp_fav_grocery",array("fav_grocery_id" =>$segment));
	  $this->session->set_flashdata('success', 'Successfully deleted');
      redirect('admin/favorite_grocery_list'); 
  }
  
}