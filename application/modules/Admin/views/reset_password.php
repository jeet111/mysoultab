<div class="main_admin_sec">
  <div class="main_admin_sec_inner">
    <div class="admin_log_sec">
          <a class="Admin_log_logo" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/logo-icon.png" style="width:100px; height: 100px; margin-left: 135px; text-align: center;"></a>
          <h2>Reset Password</h2>
          <div id="results"></div>
          <form  name="resetPass" id="resetPass" method="post">
            <input type="hidden" name="actCode" id="actCode" value="<?php echo $actkey; ?>">            
            <div class="form-group">
              <input type="password" class="form-control" name="new_password" id="new_password" placeholder="New Password">
            </div>
            
            <div class="form-group">
              <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm Password">
            </div>
            
            <div class="form-group">
              
              <button type="submit" class="chnge-mail btn btn-default upps">Update password</button>
            </div>
          </form>
    </div>
  </div>
</div>