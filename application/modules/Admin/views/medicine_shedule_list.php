<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="row">

			<div class="col-md-12">

				<div class="">

					<div class="card-body">

						<div class="status-cng"></div>

						<?php if ($this->session->flashdata('success')) { ?>

							<div class="alert alert-success message">

								<button type="button" class="close" data-dismiss="alert">x</button>

								<?php echo $this->session->flashdata('success'); ?></div>

							<?php } ?>

							<?php  //echo "<pre>"; print_r($medicineShedule); echo "</pre>"; ?>

							<table class="table table-hover tab_comn" id="sampleTable">

								<thead>

									<tr>

										<th>S no.</th>

										<th>User Name</th>

										<th>Doctor Name</th>

										<th>Medicine Name</th>

										<th>Medicine Time</th>

										<th>Medicine Take Days</th>

										<th>Action</th>

									</tr>

								</thead>

								<tbody>

									<?php

									$i = 1;

									foreach ($medicineShedule as $shedule) {

										?>

										<tr>

											<td><?php echo $i++; ?></td>

											<td><?php echo $shedule['username']; ?></td>

											<td><?php echo $shedule['doctor_name']; ?></td>



											<td><?php echo $shedule['medicine_name']; ?></td>

											<td><?php echo $shedule['medicine_time']; ?></td>



											<td><?php echo $shedule['medicine_take_days']; ?></td>



											<td>

												<div class="link-del-view">



													<?php

													if (isset($staff_permission[3]['view_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/view_medicine_schedule/<?php echo $shedule['medicine_id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

															<span class="tooltiptext">View</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/view_medicine_schedule/<?php echo $shedule['medicine_id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

															<span class="tooltiptext">View</span>

														</div>

														<?php

													}

													?>

													

													<?php

													if (isset($staff_permission[3]['delete_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/delete_medicine/<?php echo $shedule['medicine_id']; ?>" onclick="return confirm('Delete this medicine shedule ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/delete_medicine/<?php echo $shedule['medicine_id']; ?>" onclick="return confirm('Delete this medicine shedule ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

														</div>

														<?php

													}

													?>

													



												</div>

											</td>

										</tr>

									<?php } ?>

								</tbody>

							</table>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>