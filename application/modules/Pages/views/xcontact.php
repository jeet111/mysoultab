<div class="aboutMain wow slideInDown animated">

		<div class="ban_box">

			<div class="container">

				<div class="ben_hed">

					<h2>Conatct Us</h2>

					<nav aria-label="breadcrumb">

					  <ol class="breadcrumb">

					  	<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>

					    <li class="breadcrumb-item active" aria-current="page">Conatct Us</li>

					  </ol>

					</nav>

				</div>

			</div>

		</div>

	</div>

<div class="contact_form">

<div class="container">

    	<div class="row">

        	<div class="contact_form_main">

            	<div class="col-md-7 col-sm-6">

                	<div class="contact_form_heading">

                    	<h3 class="about_are wow slideInLeft animated">Get in touch with us</h3>                    

                    </div>

                	<div class="contact_form_section wow slideInLeft animated">

                        <div id="deletemsg" style="text-align: center;"></div>

                    	<form id="admin_contact">

                        	<div class="row">

                            	<div class="col-md-6">	

                                	<div class="input-filed">

            							<input type="text" name="fullname" value="<?php echo set_value('email'); ?>" placeholder="Full Name" required="">

            							<img src="assets/images/name.png" class="icon">

                                        <td class="error"><?php echo form_error('fullname'); ?></td>

						            </div>

                                </div>

                                <div class="col-md-6">

                                	<div class="input-filed">

            							<input type="email" name="email" value="<?php echo set_value('email'); ?>" placeholder="Email Address" required="">

            							<img src="assets/images/message.png" class="icon">

                                        <td class="error"><?php echo form_error('email'); ?></td>

        						    </div>

                                </div>   

                            </div>

                            <div class="form-group input-filed text_areaicon">

                                <textarea class="form-control" rows="5" id="comment" name

                                ="comment" placeholder="Comment" required=""><?php echo set_value('email'); ?></textarea>

    	                        <img src="assets/images/icon-comments.png" class="icon">

                                <td class="error"><?php echo form_error('comment'); ?>

                            </div>                            

                            <input type="submit" class="sc_button" name="submit" id="contact">

                        </form>                       

                    </div>

                </div>                

                <div class="col-md-1 col-sm-1">

                	<div class="or_heading">

                	<h3>OR</h3>

                    </div>

                </div>                

                <div class="col-md-4 col-sm-5">

                	<div class="contact_details wow slideInRight animated">

                        <h3 class="about_are wow slideInRight animated">Contact Details</h3>

                        <ul class="info">

                            <li>

                                <div class="icon"><i class="fa fa-map-marker"></i></div>

                                <div class="content">

                                    <p>123 Main Street, St. NW Ste,</p>

                                    <p>1 Washington, DC,USA.</p>

                                </div>

                            </li>    

                            <li>

                                <div class="icon"><i class="fa fa-phone"></i></div>

                                <div class="content">

                                    <p>123 456 789 000</p>

                                    <p>123 456 789 000</p>                                    

                                </div>

                            </li>

                            <li>

                                <div class="icon"><i class="fa fa-envelope"></i></div>

                                <div class="content">

                                    <p>support@gmail.com</p>

                                    <p>www.carepro.com</p>

                                </div>

                            </li>

                        </ul>

                    </div>                    

                    <div class="contact_02_social_media wow slideInRight animated"> 

                        <ul>

                        	<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>

                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>

                            <li><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>

                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>

                        </ul>

                   </div>

                </div>

                

                

            </div>

        </div>

    </div>



</div>



<section class="contact-map-main"> 

	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d52760.752952685536!2d-73.98099442675944!3d40.721011979147356!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1517043097434" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

</section>





<script src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.js"></script>







<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script> 

  



<script type="text/javascript">

$('#admin_contact').validate({

            rules: {

                fullname: "required",

                email: "required",

                comment: "required",

             },

        

            messages: {

                fullname: "Please enter your fullname.",

                email: "Please enter your email.",

                comment: "Please enter your message.",

            },



    submitHandler: function(form) {

          $.ajax({

            type: 'post',

            url: '<?php echo base_url();?>Pages/Login/contact_us',

            data: $('form').serialize(),



            beforeSend: function() { 

              $("#contact").prop('disabled', true); // disable button

              //console.log('ooo');

            },

            success: function (data) {

              if(data==1){

                $("#deletemsg").html('<div class="alert alert-success">'+'Message sent successfully.'+'</div>');

                setTimeout(function(){

                      $("#deletemsg").html("");

                     }, 2000);

                $("#contact").prop('disabled', false); // enable button

              }else{

                $("#deletemsg").html('<div class="alert alert-danger">'+'Please try again.'+'</div>');

                setTimeout(function(){

                      $("#deletemsg").html("");

                     }, 2000);

                $("#contact").prop('disabled', false); // enable button

              }

            }

          });

    }

});

</script>