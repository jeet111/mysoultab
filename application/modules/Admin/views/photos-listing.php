<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="row">

			<div class="col-md-12">

				<div class="">

					<div class="card-body">

						<div class="status-cng"></div>

						<?php

						if($this->session->flashdata('success')) {

							$message = $this->session->flashdata('success');

							?>

							<div class="<?php echo $message['class'] ?>"><?php echo $message['message']; ?></div>

						<?php } ?>





						<div id="deletemsg"></div>

						<!-- <div class="add-btn"><a href="admin/add/user">Add User</a></div> -->

						<table class="table table-hover tab_comn" id="sampleTable">

							<thead>

								<tr>

									<th>S no.</th>

									<th>Username</th>

									<th>Photo</th>

									<th>Status</th>

									<th>Action</th>

								</tr>

							</thead>

							<tbody>

								<?php foreach ($users_data as $key => $user) {?>

									<tr>

										<td><?php echo ($key+1); ?></td>

										<?php $data['users'] = $this->login_model->getSingleRecordById('cp_users',array('id' =>$user['user_id']));

										?>

										<td><?php echo $data['users']['username']; ?></td>

										<td><img height="50px" width="50px" src="<?php echo base_url().'uploads/photos/'.$user['u_photo']; ?>"></td>

										<td>

											<?php if($user['u_photo_status']==1){ ?>

												<?php echo "<button type='' class='btn ybt btn-xs'  disabled>Active</button>"; ?>

											<?php }else{ ?>

												<?php echo "<button type='' class='btn xbt btn-xs'  disabled>Inactive</button>"; ?>

											<?php } ?>

										</td>

										<td>

											<div class="link-del-view">

												<?php

												if (isset($staff_permission[2]['view_permission']) == 1) {

													?>

													<div class="tooltip-2">

														<a href="admin/view_photo/photo/<?php echo $user['u_photo_id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

														<span class="tooltiptext">View</span>

													</div>

													<?php

												} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

													?>

													<div class="tooltip-2">

														<a href="admin/view_photo/photo/<?php echo $user['u_photo_id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

														<span class="tooltiptext">View</span>

													</div>

													<?php

												}

												?>

												

												<!-- <div class="tooltip-2">

													<a href="admin/editphoto/<?php echo $user['u_photo_id']; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>

													<span class="tooltiptext">Edit</span>

												</div> -->



												<?php

												if (isset($staff_permission[2]['delete_permission']) == 1) {

													?>

													<div class="tooltip-2">

														<a href="javascript:void(0);" data-id="<?php echo $user['u_photo_id']; ?>" id="deletemoviecat_<?php echo $user['u_photo_id']; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

														<!-- <a href="javascript:void(0)" onclick="deleteStatus(<?php echo $user['u_photo_id']; ?>,'admin/delete/photo')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span> -->

													</div>

													<?php

												} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

													?>

													<div class="tooltip-2">

														<a href="javascript:void(0);" data-id="<?php echo $user['u_photo_id']; ?>" id="deletemoviecat_<?php echo $user['u_photo_id']; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

														<!-- <a href="javascript:void(0)" onclick="deleteStatus(<?php echo $user['u_photo_id']; ?>,'admin/delete/photo')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span> -->

													</div>

													<?php

												}

												?>

												

												<?php

												if (isset($staff_permission[2]['edit_permission']) == 1) {

													if($user['u_photo_status'] == 1) { ?>

														<div class="tooltip-2"><a href="javascript:void(0);" onclick="return updateStatus(<?php echo $user['u_photo_id']; ?>, 'admin/photo/status');"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>

															<span class="tooltiptext">Active</span>

														</div>

													<?php }else { ?>

														<div class="tooltip-2"><a href="javascript:void(0);" onclick="return updateStatus(<?php echo $user['u_photo_id']; ?>, 'admin/photo/status');"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>

															<span class="tooltiptext">Deactive</span>

														</div>

													<?php } 

												} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

													if($user['u_photo_status'] == 1) { ?>

														<div class="tooltip-2"><a href="javascript:void(0);" onclick="return updateStatus(<?php echo $user['u_photo_id']; ?>, 'admin/photo/status');"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>

															<span class="tooltiptext">Active</span>

														</div>

													<?php }else { ?>

														<div class="tooltip-2"><a href="javascript:void(0);" onclick="return updateStatus(<?php echo $user['u_photo_id']; ?>, 'admin/photo/status');"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>

															<span class="tooltiptext">Deactive</span>

														</div>

													<?php } 

												}

												?>

												

											</div>

										</td>

									</tr>

								<?php } ?>

							</tbody>

						</table>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>





<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>





<script type="text/javascript">

	$(document).on('click','[id^="deletemoviecat_"]',function(){







		var delete_photo = $(this).attr('data-id');

		if(confirm("Are you sure, you want to delete this photo ?")){

			$.ajax({

				type: "POST",

				url: '<?php echo base_url(); ?>Admin/Admin_user/deleteuserphotoRecords/',

				data: 'id='+delete_photo,

            //dataType: "json",

            success: function(data)

            {





            	setTimeout(function(){

            		$("#deletemsg").html("<div class='alert alert-success'>Deleted Successfully.</div>");

            		location.reload();

//window.location.href = email_url+"email_list";

}, 1000);



            }

        });

			$(this).parents(".card_one").animate({ backgroundColor: "#fbc7c7" }, "fast")

			.animate({ opacity: "hide" }, "slow");

		}

	});

</script>