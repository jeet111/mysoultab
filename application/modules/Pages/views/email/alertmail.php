<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<!--[if gte mso 9]>
	<xml>
		<o:OfficeDocumentSettings>
		<o:AllowPNG/>
		<o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="format-detection" content="date=no" />
	<meta name="format-detection" content="address=no" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="x-apple-disable-message-reformatting" />
    <!--[if !mso]><!-->
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/new_user_dashboard/css/mail.css" rel="stylesheet">
    <!--<![endif]-->
	<title>Alert Template</title>
	
	

	
</head>
<body class="body">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f4f4f4">
		<tr>
			<td align="center" valign="top">
				<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
					<tr>
						<td class="td container">
							<!-- Header -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="sec1">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="p30-15">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<th class="column-top" width="145">
																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bottom-line">
																	<tr>
																		<td class="img m-center"><img src="<?php echo base_url(); ?>/assets/images/logo.png" width="100" height="50" border="0" alt="" class="img m-center"/></td>
																	</tr>
																</table>
															</th>
															
														</tr>
												<!--------main body----->
													</table>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="h3 pb20">Welcome to SoulTab</td>
														</tr>
														<tr>
															<td class="text pb20">Hello <?php echo $data['caregiver']; ?>,
															 
															</td>
														</tr>
														<tr>
															<td class="text pb201">As subscribed, we are sending you e mail notification with following alert.
															 
															</td>
														</tr>
														<tr>
															<td class="text2 pb20">Here is profile information:
															 
															</td>
														</tr>
													</table>
														<section class="two22">
    													<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td class="textt pb201">Sender :
															 
																</td>
															</tr>
															
															<tr>
																<td class="textt pb20">Message :
															 
																</td>
															</tr>
															<tr>
																<td class="textt pb2012">Date sent :
															 
																</td>
															</tr>
														</table>
 							 						</section>

  													<section class="t2">
    													<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td class="texttt pb201"> <?php echo $data['username']; ?>
															 
																</td>
															</tr>
															<tr>
																<td class="texttt pb20"><?php echo $data['message']; ?>
															 
																</td>
															</tr>
															<tr>
																<td class="texttt pb2012"><?php echo $data['date']; ?>
															 
																</td>
															</tr>
														</table>
  													</section>
  													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														
														<tr>
															<td class="text pb202" style="padding-top: 30px">What's next? 
															 
															</td>
														</tr>
														
														<tr>
															<td class="text pb201">To sign into your account,please visit <a href="#">www.mysoultab.com/login</a>
															</td>
														</tr>
													</table>

                                            <!------- end main body---------->


                                            <!-----------regards section---->
													<section class="one1">
    													<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td class="p30-155" bgcolor="#ffffff">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td align="center" valign="top" class="sec2"><img border="0" vspace="0" hspace="0" class="sec2" src="https://www.mysoultab.com/assets/images/team_two.jpg"alt="D" width="100%">
																			</td>
																		</tr>
											
																	</table>
																</td>
															</tr>
														</table>
 							 						</section>

  													<section class="two2">
    							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="p30-151" bgcolor="#ffffff">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											
											<tr>
												<td class="text-footer1 pb10">Best Regards</td>
											</tr>
											<tr>
												<td class="text-footer1 pb10">ABC</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20">Position</td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table>
  							</section>
							<!-- END Article / Title + Copy + Button -->

							
							<!-- Footer -->
							 <section class="one">
    							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="p30-152" bgcolor="#ffffff">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="left" class="sec3">
													<table>
														<tr>
															<td class="text12 pb20">Technical Support</td>
															
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20">Mail us at:</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20" >info@mysoultab.com</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20">Web :<a href="#">mysoultab.com</a></td>
											</tr>
											<tr>
												<td class="text-footer1 pb20">Tel no : 847 4501055</td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table>
 							 </section>

  							<section class="two">
    							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="p30-152" bgcolor="#ffffff">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="left" class="sec3">
													<table>
														<tr>
															<td class="text12 pb20">Information</td>
															
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20">Contact us</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20">About us </td>
											</tr>
											<tr>
												<td class="text-footer1 pb20">Privacy policy</td>
											</tr>
											<tr>
												<td class="text-footer1 pb20">Terms of Use</td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table>
  							</section>


  							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="p30-152"  bgcolor="#ffffff">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="img" width="55" class="sec4"><a href="#" target="_blank"><img src="<?php echo base_url(); ?>/images/t8_ico_facebook.jpg" width="38" height="38" border="0" alt="" /></a></td>
															<td class="img" width="55" class="sec4"><a href="#" target="_blank"><img src="<?php echo base_url(); ?>/images/t8_ico_twitter.jpg" width="38" height="38" border="0" alt="" /></a></td>
															<td class="img" width="55" class="sec4"><a href="#" target="_blank"><img src="<?php echo base_url(); ?>/images/t8_ico_instagram.jpg" width="38" height="38" border="0" alt="" /></a></td>
															<td class="img" width="38" class="sec4"><a href="#" target="_blank"><img src="<?php echo base_url(); ?>/images/t8_ico_linkedin.jpg" width="38" height="38" border="0" alt="" /></a></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<!-- END footer -->

						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>




</body>
</html>
