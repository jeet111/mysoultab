<style>
    tbody tr td:last-child {
        text-align: left;
    }
    .rem_add_button {
        width: 100%;
        float: left;
        display: block;
        text-align: center;
        margin-top: 13px;
    }
     .btn-primary:hover {
        color: #000 !important;
    }
    .serv.col-md-3 {
        text-align: center;
        box-shadow: rgb(100 100 111 / 10%) 0px 7px 29px 0px;
        padding: 14px 0;
        margin-top: 20px;
    }
    .rem_add_button a {
        font-size: 19px;
        margin-right: 10px;
        margin-top: 20px;
    }
    
    table#sampleTable {
        table-layout: fixed;
        text-align: left;
    }
    td.lcol a i {
        font-style: normal;
    }
    td.lcol a {
        margin-right: 5px;
    }
    .tab_medi td a.edit_btn {
        background: #1f569e;
        color: #fff;
        padding: 10px 17px;
        max-width: 139px;
        width: 100%;
        border-radius: 41px;
    }
    .serv.col-md-3 h4 {
        font-size: 22px;
        color: #000;
    }
    .rem_add {
        margin-bottom: 14px;
        margin-top: 8px;
    }
</style>





<div class="text_heading">
    <div id="msg_show"></div>

    <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success message" id="success">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php } ?>
    <h3 class="text-center">
        <i class="fa fa-plus-square-o" aria-hidden="true"></i> Social
    </h3>
    <?php if (!empty($SocialArray) && $SocialArray != '') { ?>
        <div class="tooltip-2">
            <h2>Display Social button on Mobile App
                <?php
                foreach ($SocialArray as $res) {
                    if ($res->settings == 1) { ?>
                        <label class="switch ">
                            <input type="checkbox" checked data-btn="<?php echo 'Social' ?>" class="updateStatus">
                            <span class="slider round"></span>
                        </label>
                    <?php } else { ?>
                        <label class="switch ">
                            <input type="checkbox" data-btn="<?php echo 'Social' ?>" class="updateStatus">
                            <span class="slider round"></span>
                        </label>
                    <?php } ?>
                <?php } ?>
            </h2>
        </div>
    <?php } ?>
    <div class="rem_add">
        <a href="#addsocialmyModal" class="btn button btn-primary" data-toggle="modal">Add Social</i> </a>
    </div>
</div>


</h2>

</div>

</div>

<div class="row websiteAppClass">

<?php
if (!empty($social_list)) {
    foreach ($social_list as $key => $value) {
        // echo "<pre>";
        // print_r($value);
?>
        <div class="serv col-md-3 bg-info">
            <h4>
                <?php echo $value->button_name; ?>

            </h4>

            <?php
            if ($value->social_type == "Website") {
            ?>

                <img class="game website" src="https://www.google.com/s2/favicons?domain=<?php echo $value->url; ?>" data-url="<?php echo $value->url; ?>">
            <?php } else {
                $app_list_data = $this->common_model_new->getAllwhere("app_list", array('id' => $value->app_name));
            ?>
                <img class="game app" src="<?php echo $app_list_data[0]->icon; ?>">

            <?php } ?>
            <div class="rem_add_button">
                <a href="#editsocialmyModal" class="model-content edit edit_btn" data-toggle="modal">
                    <i class="material-icons update" data-toggle="tooltip" data-socialid="<?php echo $value->id; ?>" data-socialid="<?php echo $value->social_type; ?>" data-url="<?php echo $value->url; ?>" data-app_name="<?php echo $value->app_name; ?>" data-button_name="<?php echo $value->button_name; ?>" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></i>
                </a>
                <a href="#deletesocialmyModal" class="model-content delete" data-id="<?php echo $value->id; ?>
                                            " data-toggle="modal"><i class="fa fa-trash" aria-hidden="true"></i></a>

            </div>
        </div>

    <?php }
} else { ?>
        <div class="nodataContainer"><h4 class="nodataFound">No Data Found!</h4></div>
<?php } ?>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() {
            $('#success').fadeOut('fast');
        }, 2500);
    });

    $(document).on('click', '.updateStatus', function() {
        var update_status = $(this).attr('data-btn');
        var update_st = $(this).attr('data-st');
        //alert(update_status+'-'+update_st);
        $.ajax({
            url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
            type: "POST",
            data: {
                'btn': update_status
            },
            dataType: "json",
            success: function(data) {
                $("#msg_show").html("<div class='alert alert-success' id='hide_msg'>" + data.status + "</div>");
                setTimeout(function() {
                    $('#hide_msg').fadeOut('fast');
                }, 2500);
            }
        });
    });
</script>