<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Weather extends MX_Controller {



	public function __construct() {

		parent:: __construct();

		$this->load->library('session');

		$this->load->library('form_validation');

		$this->load->model('activities_model');

		$this->load->model('Common_model_new');

		$this->load->model('common_model_new');

		$this->load->model('movie_model');

		$this->load->helper(array('common_helper'));

		$this->load->helper(array('url'));

		$this->load->helper(array('form'));

		not_login();

	}


	public function weather_dashboard(){
    $data['menuactive'] = '';
    $checkLogin = $this->session->userdata('logged_in');
    $user_id = $this->session->userdata('logged_in')['id'];

    if(!empty($checkLogin))
    {
      $data['menuactive'] = $this->uri->segment(1);
     
      unset($_POST);
      $data['weatherArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'Weather',"user_id"=>$user_id));

      $data['app_list'] = $this->common_model_new->getAllwhere("app_list",array('status' => 1,'use_for'=>"weather"));
      $data['weather_list'] = $this->common_model_new->getAllwhere("cp_weather",array('user_id'=>$this->session->userdata('logged_in')['id']));
      $this->template->set('title', 'Dashboard Game');

      $this->template->load('new_user_dashboard_layout', 'contents', 'weather', $data);
    } 
    else{

      redirect('login');
  
    } 
  }

  public function add_weather()
  {
    if(count($_POST)>0){
        if($_POST['type']==1){
      
        
        $array =array(
           'user_id' => $this->session->userdata('logged_in')['id'],
           'weather_type' => $this->input->post('weather_type'),
           'button_name' => $this->input->post('button_name'),
           'url'=>$this->input->post('url'),
           'app_name' => $this->input->post('app_name'),
           'created' => date('Y-m-d H:i:s'));
       }
      
      $check_weather = $this->Common_model_new->getsingle("cp_weather",array("button_name" => $this->input->post('button_name'),"user_id" => $this->session->userdata('logged_in')['id']));
      if(!empty($check_weather)){
      	unset($_POST);
        $this->session->set_flashdata('success', "Weather already added");
        echo json_encode(array("statusCode"=>200,"msg"=>"weather already added"));
        die();
      }

       $insert = $this->Common_model_new->insertData('cp_weather',$array);
       if($insert){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        $this->session->set_flashdata('success', "Weather added successfully");
        echo json_encode(array("statusCode"=>200,"msg"=>"weather added successfully"));
        die();
      }

    }
  }

  public function edit_weather()
  {
    if(count($_POST)>0){
        if($_POST['type']==2){
        $weather_id = $this->input->post('id');
        
        $array =array(
           'user_id' => $this->session->userdata('logged_in')['id'],
           'weather_type' => $this->input->post('weather_type'),
           'button_name' => $this->input->post('button_name'),
           'url'=>$this->input->post('url'),
           'app_name' => $this->input->post('app_name'),
           'created' => date('Y-m-d H:i:s'));
       }

       $check_weather = $this->Common_model_new->allexcludeone("cp_weather",array("button_name" => $this->input->post('button_name'),"user_id" => $this->session->userdata('logged_in')['id']),$weather_id);
      if(!empty($check_weather)){
      	unset($_POST);
        $this->session->set_flashdata('success', "Weather already added");
        echo json_encode(array("statusCode"=>200,"msg"=>"weather already added"));
        die();
      }

       $update = $this->Common_model_new->updateData('cp_weather',$array,array('id' => $weather_id));
       if($update){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        $this->session->set_flashdata('success', "Weather updated successfully");
        echo json_encode(array("statusCode"=>200,"msg"=>"weather updated successfully"));
        die();
      }

    }
  }

  public function delete_weather()
  {
    if(count($_POST)>0){
        if($_POST['type']==3){
        $weather_id = $this->input->post('id');

       $update = $this->Common_model_new->delete('cp_weather',array('id' => $weather_id));
       if($update){
        //$this->session->set_flashdata('success','');
        unset($_POST);
        echo json_encode(array("statusCode"=>200,"msg"=>"weather deleted successfully"));
        die();
      }

    }
  }
}





	



}
?>