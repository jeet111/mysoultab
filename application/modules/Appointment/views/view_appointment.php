<aside class="right-side">
  <section class="content-header no-margin">
    <h1 class="text-center"><i class="fa fa-file-text" aria-hidden="true"></i> Appointment Detail
      <div class="back_reminder"><a href="<?php echo base_url(); ?>appointment_list" class="btn btn-danger">Back</a></div>
    </h1>
  </section>

  <?php //echo "<pre>"; print_r($TestReportView); echo "</pre>"; ?>
  <section class="content photo-list">
    <div class="photo-listMain">
	    <form id="detail_acknowledge" method="post" class="frm_add">
        <div class="row">
          <div class="col-md-6">                        
            <div class="form-group">
              <div class="bx_med">Doctor Name:</div>
              <div class="bx_meSec"><?php echo $singleData->doctor_name; ?></div>  
            </div>                                  
          </div>
          <div class="col-md-6">                        
            <div class="form-group">
              <div class="bx_med">Doctor Category:</div>
              <div class="bx_meSec"><?php echo $category_name; ?></div>  
            </div>                                  
          </div>


        </div>


        <div class="row">
          
          <div class="col-md-6">                        
            <div class="form-group">
              <div class="bx_med">Appointment Date :</div>
              <div class="bx_meSec"><?php echo date('j F Y',strtotime($singleData->avdate_date)); ?></div>
            </div>                                  
          </div>

          <div class="col-md-6">
            <div class="form-group">
                <div class="bx_med">Appointment Time:</div>
                <div class="bx_meSec"><?php echo $singleData->avtime_text; ?></div>
            </div>              
          </div>


        </div>
        <div class="row">
          
          <div class="col-md-6">
            <div class="form-group">
              <div class="bx_med">Picture:</div>
              <div class="bx_meSec">


                <img height="50px" width="50px"  src="<?php echo base_url().'uploads/doctor/'.$singleData->doctor_pic; ?>">

                
                </div>
            </div>
          </div>
        </div>
       <!-- <div class="row">
          
          <div class="col-md-6">
              <div class="form-group">
                  <div class="bx_med">Acknowledge Status:</div>
                  <div class="bx_meSec">Done</div>
              </div>
          </div>
        </div>-->
        <div class="box-footer">
            <a href="<?php echo base_url();?>edit_appointment/<?php echo $singleData->dr_appointment_id;?>" class="btn btn-primary" >Edit</a>            
			
        </div>
      </form>           
    </div>
  </section>
</aside>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>


