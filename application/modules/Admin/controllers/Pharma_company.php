  <?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  class Pharma_company extends MX_Controller {
    private $staff_permission = array();
    private $access_permission = true;
    public function __construct(){   
      parent:: __construct();
      $this->load->library('session');
      $this->load->library('form_validation');
      $this->load->model('pharma_model');
      $this->load->model('login_model');
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));
      $user_id = $this->session->userdata('logged_ins')['id'];
      if(empty($user_id)) {
        redirect('admin/login');
      }

      if ($this->session->userdata('logged_ins')['user_role'] == 5) {
        $uid = $this->session->userdata('logged_ins')['id'];
        $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
        if (!empty($permission_list)) 
        {
           foreach ($permission_list as $k => $v) 
           {
              $this->staff_permission[$v['permission_id']] = $v;
           }
        }
      }
    }

    public function permission_access($permission_id=9)
    {
      if ($this->session->userdata('logged_ins')['user_role'] != 1) 
      {
         if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
         {
            $this->access_permission = false;
         }
      }
    }

    /*Created by 95 for show all movie*/
    public function pharmacompany_list()
    {
     $data['menuactive'] = $this->uri->segment(2);
     $data['pharmaData'] = $this->pharma_model->getAllpharmacompany();
     
     $data['staff_permission'] = $this->staff_permission;
     
     $this->permission_access(9);
     if ($this->access_permission) {
       $this->template->set('title', 'Pharma Company');
       $this->template->load('admin_dashboard_layout', 'contents', 'pharmacompany_list', $data);
     } else {
       $this->template->set('title', 'Admin Panel - Staff');
       $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }


   /*Created by 95 for delete pharmacompany*/
   public function delete_pharmacompany($company_id='')
   {
    $this->pharma_model->delete('pharma_company',array('id' =>$company_id));
    $this->session->set_flashdata('success', 'Deleted successfully.');
    redirect('admin/pharmacompany_list');
  }


  /*Created by 95 for add pharmacompany*/
  public function add_pharmacompany()
  {
   $data['menuactive'] = $this->uri->segment(2);
   $this->form_validation->set_rules('company_name','Company Name','required');
   $this->form_validation->set_rules('company_email','Company Email','required');
   $this->form_validation->set_rules('company_contact','Company Contact','required');
   $this->form_validation->set_rules('company_address','Company Address','required');
   if ($this->form_validation->run() == TRUE) 
   {  
    if($_POST['submit']){

      $data = array(
        'name' => $this->input->post('company_name'),
        'email' => $this->input->post('company_email'),
        'contact_no' => $this->input->post('company_contact'),
        'address' => $this->input->post('company_address'),
        'created' =>date('Y-m-d H:i:s')
      );

      $this->pharma_model->insertData('pharma_company',$data);
      $this->session->set_flashdata('success', 'Pharma company added successfully.');
      redirect('admin/pharmacompany_list');
    }
  }
  
  $data['staff_permission'] = $this->staff_permission;
  
   $this->permission_access(9);
   if ($this->access_permission) {
     $this->template->set('title', 'Pharma Company');
     $this->template->load('admin_dashboard_layout', 'contents', 'add_pharma',$data);
   } else {
     $this->template->set('title', 'Admin Panel - Staff');
     $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
   }
}



public function generateRandomString($length = 12) {
 $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
 $charactersLength = strlen($characters);
 $randomString = '';
 for ($i = 0; $i < $length; $i++) {
   $randomString .= $characters[rand(0, $charactersLength - 1)];
 }
 return $randomString;
}


/*Created by 95 for edit movie*/
public function edit_pharmacompany($company_id='')
{
 $data['menuactive'] = $this->uri->segment(2);
 $this->form_validation->set_rules('company_name','Company Name','required');
 $this->form_validation->set_rules('company_email','Company Email','required');
 $this->form_validation->set_rules('company_contact','Company Contact','required');
 $this->form_validation->set_rules('company_address','Company Address','required');
 if ($this->form_validation->run() == TRUE) 
 {  
   if($_POST['submit']){
    $data = array(
      'name' => $this->input->post('company_name'),
      'email' => $this->input->post('company_email'),
      'contact_no' => $this->input->post('company_contact'),
      'address' => $this->input->post('company_address'),
      'created' =>date('Y-m-d H:i:s')
    );
    $this->pharma_model->updateData('pharma_company',$data,array('id'=>$company_id));
    $this->session->set_flashdata('success', 'Company updated successfully.');
    redirect('admin/pharmacompany_list');
  }
}
$data['editPhamra']=$this->pharma_model->getsingle('pharma_company',array('id'=>$company_id));

$data['staff_permission'] = $this->staff_permission;
    
    $this->permission_access(9);
    if ($this->access_permission) {
      $this->template->set('title', 'Pharma Company');
      $this->template->load('admin_dashboard_layout','contents','edit_pharma',$data);
    } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
    }
}


public function view_pharmacompany($company_id='')
{
 $data['menuactive'] = $this->uri->segment(2);
 $data['viewPhamra']=$this->pharma_model->getsingle('pharma_company',array('id'=>$company_id));
 
 $data['staff_permission'] = $this->staff_permission;
  
  $this->permission_access(9);
  if ($this->access_permission) {
    $this->template->set('title', 'Pharma Company');
    $this->template->load('admin_dashboard_layout', 'contents', 'view_pharma', $data);
  } else {
    $this->template->set('title', 'Admin Panel - Staff');
    $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
  }
}



}