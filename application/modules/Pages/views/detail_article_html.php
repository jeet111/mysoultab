<aside class="right-side">
  <section class="content-header no-margin">
    <h1 class="text-center"><i class="fa fa-newspaper-o"></i> Article
    <div class="back_reminder"><a href="#" class="btn btn-danger">Back</a></div>
    </h1>
  </section>
  <section class="content photo-list">
    <div class="articl_main">
      <div class="row">
        <div class="col-md-8">
          <div class="bx_artLeft">
            <div class="art_img">
              <img src='<?php echo base_url(); ?>/uploads/articles/artic_img.jpg' class="" alt="">
            </div>
            <div class="bx_minImg">
              <div class="min_img">
                <img src="http://votivelaravel.in/carepro/uploads/doctor/happydoctor.jpg" class="" alt="">
              </div>
              <div class="min_txt">
                <h5>Amanda Morris</h5>
                <span>13 March 8:15 AM</span>
              </div>
              <ul class="list_like">
                <li><a href="#"><i class="fa fa-heart-o"></i></a></li>
                <li><a href="#"><i class="fa fa-trash-o"></i></a></li>
              </ul>

            </div>
            <h4>Great hopes make great men</h4>
            <p>Quisque in ornare mollis urna, a pharetra lectus bibendum et. Aenean sodales cursus nulla, faucibus tempor nibh porta id. Pellentesque non nibh eros. Nunc lectus lacus, interdum et consequat et, varius sit amet ipsum. Fusce convallis, lorem sit amet bibendum accumsan, mark example consectetursup adipisicingsub nec mattis dui nisi a lacus. Fusce non nisl pretium tellus eleifend tincidunt id id lorem. In hac habitasse platea dictumst. Pellentesque orci libero, fringilla et ultrices ut, cursus eu ipsum. Aenean bibendum dui quis pellentesque dictum. Cras aliquet risus id nisi feugiat vulputate.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore a ut magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do est tempor incididunt ut labore et dolore magna ex est aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris. Phasellus ornare sem quis dui aliquet facilisis. Sed ipsum eros, ultricies at turpis eu, mattis dictum risus.</p>
          </div>
        </div>
        <div class="col-md-4">
          <div class="">
            <div class="magmax_Posts">
              <h2 class="mag-title">Popular</h2>        
              <div class="magmax-popular-post">
              <ul class="list_popu">
                <li class="single-item">
                  <div class="popular-post-wrapper">
                    <div class="post-thumb-wrapper">
                      <a class="post-thumb" href="#">
                        <img src="http://demo.themexpert.com/wordpress/magmax/wp-content/uploads/2018/04/constitution-gavel-and-handcuffs-PQBC5FA-150x150.jpg" class="attachment-thumbnail" alt="">
                      </a>
                    </div>                    
                    <div class="bx_txt">
                      <div class="bx_pop">
                        <h4><a href="#">Nisl sagittis vestibulum Nullam nulla eros ultricies sit</a></h4>
                        <div class="popular_time">Apr 5, 2018</div>
                      </div>                      
                    </div>
                  </div>
                </li>
                <li class="single-item">
                  <div class="popular-post-wrapper">
                    <div class="post-thumb-wrapper">
                      <a class="post-thumb" href="#">
                        <img src="http://demo.themexpert.com/wordpress/magmax/wp-content/uploads/2018/04/constitution-gavel-and-handcuffs-PQBC5FA-150x150.jpg" class="attachment-thumbnail" alt="">
                      </a>
                    </div>                    
                    <div class="bx_txt">
                      <div class="bx_pop">
                        <h4><a href="#">Nisl sagittis vestibulum Nullam nulla eros ultricies sit</a></h4>
                        <div class="popular_time">Apr 5, 2018</div>
                      </div>                      
                    </div>
                  </div>
                </li>
                <li class="single-item">
                  <div class="popular-post-wrapper">
                    <div class="post-thumb-wrapper">
                      <a class="post-thumb" href="#">
                        <img src="http://demo.themexpert.com/wordpress/magmax/wp-content/uploads/2018/04/constitution-gavel-and-handcuffs-PQBC5FA-150x150.jpg" class="attachment-thumbnail" alt="">
                      </a>
                    </div>                    
                    <div class="bx_txt">
                      <div class="bx_pop">
                        <h4><a href="#">Nisl sagittis vestibulum Nullam nulla eros ultricies sit</a></h4>
                        <div class="popular_time">Apr 5, 2018</div>
                      </div>                      
                    </div>
                  </div>
                </li>
                <li class="single-item">
                  <div class="popular-post-wrapper">
                    <div class="post-thumb-wrapper">
                      <a class="post-thumb" href="#">
                        <img src="http://demo.themexpert.com/wordpress/magmax/wp-content/uploads/2018/04/constitution-gavel-and-handcuffs-PQBC5FA-150x150.jpg" class="attachment-thumbnail" alt="">
                      </a>
                    </div>                    
                    <div class="bx_txt">
                      <div class="bx_pop">
                        <h4><a href="#">Nisl sagittis vestibulum Nullam nulla eros ultricies sit</a></h4>
                        <div class="popular_time">Apr 5, 2018</div>
                      </div>                      
                    </div>
                  </div>
                </li>
              </ul>
            </div>            
          </div>

          <div class="magmax_Posts">
              <h2 class="mag-title">Popular</h2>        
              <div class="magmax-popular-post">
              <ul class="list_popu">
                <li class="single-item">
                  <div class="popular-post-wrapper">
                    <div class="post-thumb-wrapper">
                      <a class="post-thumb" href="#">
                        <img src="http://demo.themexpert.com/wordpress/magmax/wp-content/uploads/2018/04/constitution-gavel-and-handcuffs-PQBC5FA-150x150.jpg" class="attachment-thumbnail" alt="">
                      </a>
                    </div>                    
                    <div class="bx_txt">
                      <div class="bx_pop">
                        <h4><a href="#">Nisl sagittis vestibulum Nullam nulla eros ultricies sit</a></h4>
                        <div class="popular_time">Apr 5, 2018</div>
                      </div>                      
                    </div>
                  </div>
                </li>
                <li class="single-item">
                  <div class="popular-post-wrapper">
                    <div class="post-thumb-wrapper">
                      <a class="post-thumb" href="#">
                        <img src="http://demo.themexpert.com/wordpress/magmax/wp-content/uploads/2018/04/constitution-gavel-and-handcuffs-PQBC5FA-150x150.jpg" class="attachment-thumbnail" alt="">
                      </a>
                    </div>                    
                    <div class="bx_txt">
                      <div class="bx_pop">
                        <h4><a href="#">Nisl sagittis vestibulum Nullam nulla eros ultricies sit</a></h4>
                        <div class="popular_time">Apr 5, 2018</div>
                      </div>                      
                    </div>
                  </div>
                </li>
                <li class="single-item">
                  <div class="popular-post-wrapper">
                    <div class="post-thumb-wrapper">
                      <a class="post-thumb" href="#">
                        <img src="http://demo.themexpert.com/wordpress/magmax/wp-content/uploads/2018/04/constitution-gavel-and-handcuffs-PQBC5FA-150x150.jpg" class="attachment-thumbnail" alt="">
                      </a>
                    </div>                    
                    <div class="bx_txt">
                      <div class="bx_pop">
                        <h4><a href="#">Nisl sagittis vestibulum Nullam nulla eros ultricies sit</a></h4>
                        <div class="popular_time">Apr 5, 2018</div>
                      </div>                      
                    </div>
                  </div>
                </li>
                <li class="single-item">
                  <div class="popular-post-wrapper">
                    <div class="post-thumb-wrapper">
                      <a class="post-thumb" href="#">
                        <img src="http://demo.themexpert.com/wordpress/magmax/wp-content/uploads/2018/04/constitution-gavel-and-handcuffs-PQBC5FA-150x150.jpg" class="attachment-thumbnail" alt="">
                      </a>
                    </div>                    
                    <div class="bx_txt">
                      <div class="bx_pop">
                        <h4><a href="#">Nisl sagittis vestibulum Nullam nulla eros ultricies sit</a></h4>
                        <div class="popular_time">Apr 5, 2018</div>
                      </div>                      
                    </div>
                  </div>
                </li>
              </ul>
            </div>            
          </div>
        </div>
      </div>
    </div>
    
  </div>
</section>
</aside>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="assets/admin/js/jquery.validate.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>