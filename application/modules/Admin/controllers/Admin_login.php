<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_login extends MX_Controller {

  public function __construct(){

    parent:: __construct();

    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->model('login_model');
    $this->load->helper(array('url'));
    $this->load->helper(array('form'));

    $user_id = $this->session->userdata('logged_ins')['id'];
    if(!empty($user_id))
    {
      redirect('admin/dashboard');
    }

  }


  /*

   * Admin Dashboard

  */
  public function login() {

    //$data[]=array();

    $this->template->set('title', 'Admin Login');
    $this->template->load('admin_login_layout', 'contents' , 'login_page');
  }

  public function signin() {

    $this->form_validation->set_rules('user_email', 'email', 'trim|required');
    $this->form_validation->set_rules('user_password', 'password', 'trim|required');
    $data['error'] = "";

    if ($this->form_validation->run() == FALSE) {
      $this->template->set('title', 'Admin Login');
      $this->template->load('admin_login_layout', 'contents' , 'login_page',$data);
    } else {
      //Go to private area
      $email = $this->input->post('user_email');
      $password = md5($this->input->post('user_password'));
      $result = $this->login_model->admin_login($email, $password);
      
      if ($result) {
        $sess_array = array();
        $this->session->set_userdata('isUserLoggedIn',TRUE);
        foreach ($result as $row) {

          $sess_array = array(
            'id' => $row->id,
            'email' => $row->email,
            'user_role' => $row->user_role
          );
          $esss = $this->session->set_userdata('logged_ins', $sess_array);
        }
        $user_role = $result[0]->user_role;
        if($user_role==1)
        {
          redirect('admin/dashboard');
        }
      } else {
        $error = base64_encode('Invalid access or password');
        redirect('admin/login?error='.$error);
      }
    }
  }

  /*
  * Admin forget password
  */
  public function adminForgetPassword(){
    $data = array();
    $data['error_msg']="";
    $data['success_msg'] = "";
    if($this->input->post('passwordSubmit')) {
      $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email');
      $email = $this->input->post('user_email');
      if ($this->form_validation->run() == true) {
        //$con['returnType'] = 'single';
        $con['conditions'] = array(
          'email'=> $email,
          'status' => '1'
        );
        $checkUser = $this->login_model->getRows('cp_users',$con);
        if($checkUser && $checkUser[0]['user_role'] == 1){ //customer
          $reset_link = $this->generateRandomString();
          $where_condition = array('id' => $checkUser[0]['id']);
          $upd = $this->login_model->updateRecords('cp_users',array('act_link' => $reset_link),$where_condition);
          if($upd >=1)
            $message = "Please click on the below link to reset your password.<br><a href='".base_url()."reset/password/".$reset_link."'> Reset Password</a><br>";
          //$row3 =  $this->Admin_model->get_data('email_templates',array('eid'=>3));
          $subject = "Care Pro Forgot password";
          //$message = $row3[0]->content;
          //$message = str_replace("{msginner}",$msginner,$message);
          //$message = str_replace("{base_url}",base_url(),$message);
          //$message = str_replace("{email}",$email,$message);
          //$message = str_replace("{temppass}",$temppass,$message);
          sendEmail('votivephppushpendra@gmail.com', $subject, $message);
          //echo $x;
          $data['success_msg'] = 'Please check your email and reset your password.';
        } else {
          $data['error_msg'] = '<font color="red"><center>Wrong email, please try again.</center></font>';
        }
      }
    }
    $this->template->set('title', 'Forgot Password');
    $this->template->load('admin_login_layout', 'contents' , 'admin_forgot_password', $data);
  }

  public function AdminResetPassword() {
    $data = array();
    $data['actkey'] = $this->uri->segment(3);
    $con['conditions'] = array(
      'act_link'=>$data['actkey']
    );
    $con['returnType'] = 'count';
    $active_key = $this->login_model->getRows('cp_users',$con);
    //echo "<pre>";print_r($active_key);die();
    if($active_key > 0) {
      $this->template->set('title', 'Reset Password');
      $this->template->load('admin_login_layout', 'contents' , 'reset_password', $data);
    }
  }

  /*
  * Update User password
  */
  public function ResetAdminPassword(){

    $actCode = $this->input->post("actCode");
    $new = $this->input->post("new_password");

    $con['conditions'] = array("act_link"=>$actCode);
    $user = $this->login_model->getRows('cp_users',$con);
    //echo "<pre>";print_r($user[0]);die();

    if($user[0]['id'] > 0 && $user[0]['role_id']==1){
      $userid = $user[0]['id'];
      $data = array("password"=>md5($new));

      $where_condition = array('id' => $userid);
      $check = $this->login_model->updateRecords('cp_users',$data,$where_condition);
      $cont = '<div class="alert alert-success" role="alert"><strong>Success!</strong> Password updated successfully.<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a></div>';
      echo json_encode(array("status"=>200, "msg"=>$cont, "role"=>$user[0]['role_id']));
    }elseif($user[0]['id'] > 0 && $user[0]['role_id']!=1){
      $userid = $user[0]['id'];
      $data = array("password"=>md5($new));

      $where_condition = array('id' => $userid);
      $check = $this->login_model->updateRecords('cp_users',$data,$where_condition);
      $cont = '<div class="alert alert-success" role="alert"><strong>Success!</strong> Password updated successfully.<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a></div>';
      echo json_encode(array("status"=>200, "msg"=>$cont, "role"=>$user[0]['role_id']));
    }else{
      $cont = '<div class="alert alert-danger" role="alert"><strong>Sorry!</strong> Invalid access.<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a></div>';
      echo json_encode(array("status"=>1, "msg"=>$cont));
    }
  }

  public function generateRandomString($length = 12) {
    // $length = 12;
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

}
?>