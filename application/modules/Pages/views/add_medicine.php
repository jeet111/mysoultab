<style>
    .text_heading {
        PADDING-BOTTOM: 20PX;
        border: 0;
    }

    .form-group>label {
        color: #000;
    }

    .btn.btn-primary,
    .back_reminder a {
        max-width: 152px !important;
    }

    label.error {
        font-size: 12px;
        color: #ff0000 !important;
        padding-top: 3px;
    }
</style>
<div class=" text_heading">

    <h3><i class="fa fa-plus-square-o" aria-hidden="true"></i> Add Medicine</H3>

    <div class="back_reminder"><a href="javascript:window.history.back()" class="btn btn-primary">Back</a></div>

</div>

<section class="content photo-list">

    <div class="photo-listMain">



        <?php if ($this->session->flashdata('success')) { ?>

            <div class="alert alert-success message">

                <button type="button" class="close" data-dismiss="alert">x</button>

                <?php echo $this->session->flashdata('success'); ?>
            </div>

        <?php } ?>



        <form role="form" class="frm_add" id="add_medicine" method="post">

            <div class="row">

                <div class="col-sm-6 col-md-6">

                    <div class="form-group">

                        <label for="">Medicine Name<font style="color:red;">*</font></label>

                        <input type="text" name="medicine_name" class="form-control" id="medicine_name" placeholder="Medicine Name">

                        <?php echo form_error('medicine_name'); ?>

                    </div>

                </div>

                <div class="col-sm-6 col-md-6">

                    <div class="form-group">

                        <label for="">How Many Days Will Take Medicine<font style="color:red;">*</font></label>

                        <input type="number" min="1" name="no_of_days" class="form-control" id="number_days" placeholder="Number of Days" onKeyPress="if(this.value.length==3) return false;">

                        <?php echo form_error('no_of_days'); ?>

                    </div>

                </div>



                <?php if (empty($segment)) { ?>

                    <div class="col-sm-6 col-md-6">

                        <div class="form-group">

                            <label for="">Doctor Name<font style="color:red;">*</font></label>

                            <select name="doctor_name" class="form-control">

                                <option value="">Select Doctor</option>

                                <?php foreach ($Doctors as $doctor) { ?>

                                    <option value="<?php echo $doctor->doctor_id; ?>" <?php if (!empty($segment)) {
                                                                                            if ($segment == $doctor->doctor_id) {
                                                                                                echo 'selected';
                                                                                            }
                                                                                        } ?>><?php echo $doctor->doctor_name; ?> </option>



                                <?php } ?>

                            </select>

                            <?php echo form_error('doctor_name'); ?>



                        </div>

                    </div>

                <?php } ?>
<div class="col-sm-6 col-md-6 ct-flex">

                    <div class="form-group">

                        <label for="">Start Date</label>
                          <?php $date = date('d-m-Y'); ?>
                        <input type="text"  readonly name="start_date" class="form-control" value="<?php echo $date; ?>">
                    </div>
                    
                    <div class="form-group">

                        <label for="">End Date</label>

                        <input type="text" readonly  id="end_date_medicine"  name="end_date" class="form-control" >
                       
                    </div>

                </div>

                <div class="col-sm-6 col-md-6">

                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea name="medicine_description" id="medicine_description" class="form-control" placeholder="Add Medicine Description"></textarea>

                    </div>

                      <div class="form-group">

                        <label for="">Reminder</label>

                        <input type="checkbox" name="remind" value="1" checked data-toggle="toggle" data-style="ios">

                    </div>

                </div>

   <div class="col-sm-6 col-md-6">

                    <div class="form-group">

                        <label for="">Time To Take Medicine<font style="color:red;">*</font></label>

                        <!-- <input type="text" name="medicine_time" class="form-control" id="" placeholder="Morning or evening"> -->



                        <select name="medicine_time[]" id="medicine_time" class="form-control" multiple="multiple">

                            <option value="06:00 AM">06:00 AM</option>

                            <option value="07:00 AM">07:00 AM</option>

                            <option value="08:00 AM">08:00 AM</option>

                            <option value="09:00 AM">09:00 AM</option>

                            <option value="10:00 AM">10:00 AM</option>

                            <option value="11:00 AM">11:00 AM</option>

                            <option value="12:00 PM">12:00 PM</option>

                            <option value="01:00 PM">01:00 PM</option>

                            <option value="02:00 PM">02:00 PM</option>

                            <option value="03:00 PM">03:00 PM</option>

                            <option value="04:00 PM">04:00 PM</option>

                            <option value="05:00 PM">05:00 PM</option>

                            <option value="06:00 PM">06:00 PM</option>

                            <option value="07:00 PM">07:00 PM</option>

                            <option value="08:00 PM">08:00 PM</option>

                            <option value="09:00 PM">09:00 PM</option>

                            <option value="10:00 PM">10:00 PM</option>

                            <option value="11:00 PM">11:00 PM</option>



                        </select>



                        <?php echo form_error('medicine_time'); ?>

                    </div>

                </div>

            </div>

           

            <div class="box-footer">

                <button type="submit" name="msubmit" class="btn btn-primary"><i class="fa fa-plus"></i> Add Medicine</button>
                <input type="reset" name="cancel" class="btn btn-primary" value="Cancel" onclick="window.location='<?php echo base_url(); ?>medicine_list';">


                <!-- <button type="submit" class="btn btn-primary"><i class="fa fa-clock-o"></i> Set Reminder</button> -->

            </div>

        </form>

    </div>

</section>

</aside>

<script type="text/javascript">

    $("#number_days").bind('keyup mouseup', function () {
var add_days = $(this).val();
 var date = new Date();
    date.setDate(date.getDate() + +add_days);

var dd = date.getDate();
var mm = date.getMonth()+1;
var yyyy = date.getFullYear();

var future_days = dd+'-'+mm+'-'+yyyy;

$("#end_date_medicine").val(future_days);


     
});
    $('.datepicker').datepicker({

        format: 'mm/dd/yyyy',

        startDate: '-3d'

    });

    $(function() {

        $('#datetimepicker3').datetimepicker({

            pickDate: false

        });

    });
</script>



<!--Added by 95 on 26-2-2019 For Doctor category for validation-->

<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

<script type="text/javascript">
    $(document).ready(function() {

        $("#medicine_name").on('keypress', function() {
            if ($(this).val().length > 20) {
                alert("Max 100 character limit");
                return false;
            }
        });


        $("#add_medicine").validate({

            rules: {

                medicine_name: {
                    required: true,
                    minlength: 2,
                    maxlength: 100,
                },

                no_of_days: "required",

                doctor_name: "required",

                "medicine_time[]": "required",

            },



            messages: {

                medicine_name: {
                    required: "Please enter medicine name.",
                    minlength: "Please enter at least 2 characters.",
                    maxlength: "Max. 100 characters are allowed.",
                },

                no_of_days: "Please enter number of days.",

                doctor_name: "Please select a doctor.",

                "medicine_time[]": "Please select at least one medicine time.",

            },

        });





        $('#medicine_time').multiselect({

            includeSelectAllOption: true,

            allSelectedText: 'No option left ...',

            maxHeight: 200

        });



    });
</script>


<!-- Added by 95 for medicine reminder toggle button start -->

<link href="<?php echo base_url(); ?>assets/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="<?php echo base_url(); ?>assets/js/bootstrap-toggle.min.js"></script>

<style>
    .toggle.ios,
    .toggle-on.ios,
    .toggle-off.ios {
        border-radius: 20px;
    }

    .toggle.ios .toggle-handle {
        border-radius: 20px;
    }
</style>

<!-- Added by 95 for medicine reminder toggle button end -->
<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>
