<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Grocery_model extends CI_Model{
    function __construct() {
        
    }
	
		public function insertData($table,$datainsert)
	{
		$this->db->insert($table,$datainsert);
		return $this->db->insert_id();
	}
	
	public function updateData($table,$data,$where)
	{
	   $this->db->update($table,$data,$where);
	   return $this->db->affected_rows();
	}
	
	public function delete($table,$where)
	{
	   $this->db->delete($table,$where);
	   return;
	}
	
	
	public function getsingle($table,$where)
	{
		$q = $this->db->get_where($table,$where);
		return $q->row();
	}

    
    
   public function getAllwhereorderby($table,$where,$order_id,$order_by)
	{
		$this->db->select('*');
		$this->db->order_by($order_id,$order_by);
		$q = $this->db->get_where($table,$where);	
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	
	public function getAll($table)
	{
		$this->db->select('*');		
		$q = $this->db->get($table);		
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	
	public function getAllorderby($table,$order_id,$order_by)
	{
		$this->db->select('*');	
        $this->db->order_by($order_id,$order_by);		
		$q = $this->db->get($table);		
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	
	
	
	public function getAllwhere($table,$where)
	{
		$this->db->select('*');
		$q = $this->db->get_where($table,$where);
		$num_rows = $q->num_rows();
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	
	function jointwotablenn($table, $field_first, $table1, $field_second,$where='',$field,$order_id,$order_by) {

        $this->db->select($field);
	   $this->db->order_by( $order_id, $order_by);
        $this->db->from("$table");
        $this->db->join("$table1", "$table1.$field_second = $table.$field_first"); 
        if($where !=''){
        $this->db->where($where); 
        }
        $q = $this->db->get();
        if($q->num_rows() > 0) {
            foreach($q->result() as $rows) {
                $data[] = $rows;
            }
            $q->free_result();
            return $data;
        }
    }
      
   
/*Created by 95 for show all Grocery */
    public function getAllgrocery()
    {
    	 $this->db->select('*');
      	 $this->db->from('cp_grocery');
      	 $this->db->join('cp_users','cp_users.id=cp_grocery.user_id','left');
      	 $this->db->order_by('grocery_id','desc');
         $query = $this->db->get();
         return $query->result();
    }



/*Created by 95 for show single Grocery */
    public function getGrocery($Grocery_id='')
    {
    	 $this->db->select('*');
      	 $this->db->from('cp_grocery');
      	 $this->db->join('cp_users','cp_users.id=cp_grocery.user_id','left');
      	 $this->db->where('grocery_id',$Grocery_id);
         $query = $this->db->get();
         return $query->result();
    }





/*Created by 95 for show all favourite Grocery */
public function getAllfavourite_Grocery_list(){
		 $this->db->select('*');
      	 $this->db->from('cp_fav_grocery');
      	 $this->db->join('cp_users','cp_users.id=cp_fav_grocery.user_id','left');
      	 $this->db->order_by('fav_grocery_id','desc');
         $query = $this->db->get();
         return $query->result();
}


/*Created by 95 for show single favourite Grocery */
public function getFavourite_Grocery($fav_grocery_id=''){
		 $this->db->select('*');
      	 $this->db->from('cp_fav_grocery');
      	 $this->db->join('cp_users','cp_users.id=cp_fav_grocery.user_id','left');
      	 $this->db->where('fav_grocery_id',$fav_grocery_id);
         $query = $this->db->get();
         return $query->result();
}


}