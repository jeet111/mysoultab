<!-- header  -->

<!DOCTYPE html>

<html lang="en">

<head>

   <meta charset="utf-8">

   <meta http-equiv="X-UA-Compatible" content="IE=edge">

   <meta name="viewport" content="width=device-width, initial-scale=1">

   <meta name="description" content="" />

   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

   <link rel="icon" href="assets/admin/img/favicon.ico" type="image/ico" sizes="16x16">

   <title><?php echo !empty($title) ? $title : "Admin Panel"; ?></title>

   <base href="<?php echo base_url(); ?>">

   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/admin_master.css" />

   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/bootstrap-responsive.min.css" />

   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/bootstrap.min.css" />
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/bootstrap.min.css" />
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/newinternal.css" />

   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/multiselect/bootstrap-multiselect.css">

   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/datepicker.css" />

   <!-- <link rel="stylesheet" href="assets/admin/css/jquery.timepicker.min.css" /> -->

   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/timepicki.css">

   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/font-awesome.css" />

   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/jquery-customselect.css" />

   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/matrix-style.css" />

   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/matrix-media.css" />

   <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">

</head>

<body>

   <div id="header">

      <h1><a href="<?php echo base_url(); ?>admin/dashboard">Care Pro</a></h1>

   </div>

   <div id="user-nav" class="navbar navbar-inverse">

      <ul class="nav">

         <li class="dropdown" id="profile-messages">

            <a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle">

               <i class="icon icon-user"></i>

               <?php

               // $user = $this->session->userdata("user_id"); 
               ?>

               <span class="text">Welcome <?php //echo customerdata($user); 
                                          ?></span><b class="caret"></b>

            </a>

            <ul class="dropdown-menu">

               <li>

                  <a href="admin/adminprofile"><i class="icon-user"></i> My Profile</a>

               </li>

               <li>

                  <a href="admin/adminedit"><i class="icon-user"></i> Edit Profile</a>

               </li>

               <li>

                  <a href="admin/changepassword"><i class="icon-user"></i> Change Password</a>

               </li>

            </ul>

         </li>

         <li class="">

            <a title="" href="admin/logout">

               <i class="icon icon-share-alt"></i> <span class="text">Logout</span>

            </a>

         </li>

      </ul>

   </div>

   <div id="sidebar">

      <?php

      /*echo "<pre>";print_r($staff_permission);*/

      ?>

      <a href="admin/dashboard" class="visible-phone"><i class="fa fa-dot-circle-o"></i> Dashboard</a>

      <ul>

         <?php
         if (isset($staff_permission[1]['view_permission']) == 1 || isset($staff_permission[1]['add_permission']) == 1 || isset($staff_permission[1]['edit_permission']) == 1 || isset($staff_permission[1]['delete_permission']) == 1) {
         ?>

            <li class="<?php echo ($menuactive == 'dashboard') ? 'active' : ''; ?>">

               <a href="admin/dashboard">

                  <i class="fa fa-fw fa-dashboard"></i>

                  <span>Dashboard </span>

               </a>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>
            <?php $menuactive = ''; ?>
            <?php $menuactives = ''; ?>

            <li class="<?php echo ($menuactive == 'dashboard') ? 'active' : ''; ?>">

               <a href="admin/dashboard">

                  <i class="fa fa-fw fa-dashboard"></i>

                  <span>Dashboard </span>

               </a>

            </li>

         <?php

         }

         ?>


         <?php

         if (isset($staff_permission[14]['view_permission']) == 1 || isset($staff_permission[14]['add_permission']) == 1 || isset($staff_permission[14]['edit_permission']) == 1 || isset($staff_permission[14]['delete_permission']) == 1) {

         ?>

            <!-- Sprituality module start -->

            <li class="submenu <?php echo ($menuactive == 'Articles' || $menuactive == 'add_meditation_article' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'meditation_articles_list' || $menuactive == 'music_list' || $menuactive == 'view_music' || $menuactive == 'edit_music' || $menuactive == 'movie_list' || $menuactive == 'view_movie' || $menuactive == 'edit_movie' || $menuactive == 'music_banner_list' || $menuactive == 'view_music_banner' || $menuactive == 'meditation_video_category_list' || $menuactive == 'music_category_list' || $menuactive == 'edit_music_banner' || $menuactive == 'movie_banner_list' || $menuactive == 'view_movie_banner' || $menuactive == 'edit_movie_banner' || $menuactive == 'add_movie_banner' || $menuactive == 'articles_list' || $menuactive == 'view_article' || $menuactive == 'edit_article' || $menuactive == 'add_article' || $menuactive == 'article_category_list' || $menuactive == 'add_article_category' || $menuactive == 'edit_article_category' || $menuactive == 'view_article_category' || $menuactive == 'view_music_category' || $menuactive == 'edit_music_category' || $menuactive == 'add_music_banner' || $menuactive == 'add_movie' || $menuactive == 'add_movie_category' || $menuactive == 'add_music' || $menuactive == 'add_music_category' || $menuactive == 'edit_movie_category' || $menuactive == 'view_movie_category' || $menuactive == 'meditation_article_category_list' || $menuactive == 'add_meditation_article_category' || $menuactive == 'edit_meditation_article_category' || $menuactive == 'view_meditation_article_category' || $menuactive == 'edit_meditation_article' || $menuactive == 'add_meditation_video_category' || $menuactive == 'edit_meditation_vedio_category' || $menuactive == 'view_meditation_vedio_category' || $menuactive == 'meditation_video_list' || $menuactive == 'add_meditation_video' || $menuactive == 'edit_meditation_video' || $menuactive == 'view_meditation_video' || $menuactive == 'meditation_video_banner_list' || $menuactive == 'add_meditation_video_banner' || $menuactive == 'edit_meditation_video_banner' || $menuactive == 'delete_meditation_video_banner' || $menuactive == 'view_meditation_video_banner' || $menuactive == 'view_meditation_article') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-book" aria-hidden="true"></i> -->

                  <i class="fa fa-empire" aria-hidden="true"></i>

                  <!-- <i class="fa fa-television" aria-hidden="true"></i> -->

                  <span>Yoga</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i>Meditation articles </span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'meditation_articles_list' || $menuactive == 'view_article' || $menuactive == 'edit_meditation_article' || $menuactive == 'add_meditation_article' || $menuactive == 'view_meditation_article') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/meditation_articles_list"><i class="fa fa-angle-double-right"></i> <span>Meditation articles List</span></a>

                        </li>

                        <!--  <li class="<?php echo ($menuactive == 'article_category_list' || $menuactive == 'add_article_category' || $menuactive == 'edit_article_category' || $menuactive == 'view_article_category') ? 'active' : ''; ?>" >

                  <a href="<?php echo base_url(); ?>admin/article_category_list"><i class="fa fa-dot-circle-o"></i> <span>Article Category List</span></a>

               </li> -->

                        <li <?php if ($menuactive == 'meditation_article_category_list' || $menuactive == 'add_meditation_article_category' || $menuactive == 'edit_meditation_article_category' || $menuactive == 'view_meditation_article_category') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/meditation_article_category_list"><i class="fa fa-angle-double-right"></i>Meditaion article category List</a>

                        </li>

                        <!-- <li <?php if ($menuactive == 'music_banner_list' || $menuactive == 'view_music_banner' || $menuactive == 'edit_music_banner' || $menuactive == 'edit_music_category') {
                                    echo 'class="active"';
                                 } ?>>

                  <a href="<?php echo base_url(); ?>admin/music_banner_list"><i class="fa fa-angle-double-right"></i> Music Banner List</a>

               </li> -->

                     </ul>

                  </li>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-film" aria-hidden="true"></i> Meditaion Videos</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'meditation_video_list' || $menuactive == 'view_movie' || $menuactive == 'edit_movie' || $menuactive == 'add_meditation_video' || $menuactive == 'edit_meditation_video' || $menuactive == 'view_meditation_video') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/meditation_video_list"><i class="fa fa-angle-double-right"></i> <span>Meditaion Video List</span></a>

                        </li>

                        <li <?php if ($menuactive == 'meditation_video_category_list' || $menuactive == 'add_meditation_video_category' || $menuactive == 'edit_meditation_vedio_category' || $menuactive == 'add_movie_category' || $menuactive == 'view_movie_category' || $menuactive == 'view_meditation_vedio_category') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/meditation_video_category_list"><i class="fa fa-angle-double-right"></i>Meditaion Video Category List</a>

                        </li>

                        <li <?php if ($menuactive == 'meditation_video_banner_list' || $menuactive == 'add_meditation_video_banner' || $menuactive == 'edit_meditation_video_banner' || $menuactive == 'delete_meditation_video_banner' || $menuactive == 'view_meditation_video_banner') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/meditation_video_banner_list"><i class="fa fa-angle-double-right"></i>Meditaion Video Banner List</a>

                        </li>

                     </ul>

                  </li>

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <!-- Sprituality module start -->
				<li class="submenu <?php echo ($menuactive == 'Articles' || $menuactive == 'add_meditation_article' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'meditation_articles_list' || $menuactive == 'music_list' || $menuactive == 'view_music' || $menuactive == 'edit_music' || $menuactive == 'movie_list' || $menuactive == 'view_movie' || $menuactive == 'edit_movie' || $menuactive == 'music_banner_list' || $menuactive == 'view_music_banner' || $menuactive == 'meditation_video_category_list' || $menuactive == 'music_category_list' || $menuactive == 'edit_music_banner' || $menuactive == 'movie_banner_list' || $menuactive == 'view_movie_banner' || $menuactive == 'edit_movie_banner' || $menuactive == 'add_movie_banner' || $menuactive == 'articles_list' || $menuactive == 'view_article' || $menuactive == 'edit_article' || $menuactive == 'add_article' || $menuactive == 'article_category_list' || $menuactive == 'add_article_category' || $menuactive == 'edit_article_category' || $menuactive == 'view_article_category' || $menuactive == 'view_music_category' || $menuactive == 'edit_music_category' || $menuactive == 'add_music_banner' || $menuactive == 'add_movie' || $menuactive == 'add_movie_category' || $menuactive == 'add_music' || $menuactive == 'add_music_category' || $menuactive == 'edit_movie_category' || $menuactive == 'view_movie_category' || $menuactive == 'meditation_article_category_list' || $menuactive == 'add_meditation_article_category' || $menuactive == 'edit_meditation_article_category' || $menuactive == 'view_meditation_article_category' || $menuactive == 'edit_meditation_article' || $menuactive == 'add_meditation_video_category' || $menuactive == 'edit_meditation_vedio_category' || $menuactive == 'view_meditation_vedio_category' || $menuactive == 'yoga_video_list' || $menuactive == 'meditation_video_list' || $menuactive == 'add_meditation_video' || $menuactive == 'edit_meditation_video' || $menuactive == 'view_meditation_video' || $menuactive == 'meditation_video_banner_list' || $menuactive == 'add_meditation_video_banner' || $menuactive == 'edit_meditation_video_banner' || $menuactive == 'delete_meditation_video_banner' || $menuactive == 'view_meditation_video_banner' || $menuactive == 'view_meditation_article') ? 'open' : ''; ?>">
					
               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-book" aria-hidden="true"></i> -->

                  <i class="fa fa-empire" aria-hidden="true"></i>

                  <!-- <i class="fa fa-television" aria-hidden="true"></i> -->

                  <span>Yoga</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>



                  <li>

                     <span class="submenu-cls"><i class="fa fa-film" aria-hidden="true"></i> Yoga Videos</span>

                     <ul class="frd_swtChild">
							
                        <li <?php 
										$user_role = isset($this->session->userdata('logged_ins')['user_role']) ? $this->session->userdata('logged_ins')['user_role'] : "";
										if ($menuactive == 'yoga_video_list' || $user_role == 1 || $user_role == 2) {
                                 echo 'class="active"';
                              }
										?>>
										
                           <a href="<?php echo base_url(); ?>admin/yoga_video_list"><i class="fa fa-angle-double-right"></i> <span>Yoga Video List</span></a>

                        </li>



                     </ul>

                  </li>

               </ul>

            </li>

         <?php

         }

         ?>




         <?php
			
			if (isset($staff_permission[2]['view_permission']) == 1 || isset($staff_permission[2]['add_permission']) == 1 || isset($staff_permission[2]['edit_permission']) == 1 || isset($staff_permission[2]['delete_permission']) == 1) {

         ?>

            <?php $menuactive_add = ''; ?>
            <li class="submenu <?php echo ($menuactive == 'users' || $menuactive == 'add' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'family_member' || $menuactive == 'addfamily_member' || $menuactive_add == 'editfamily' || $menuactive == 'viewfamily_member'  || $menuactive == 'caregiver' || $menuactive == 'addcaregiver' || $menuactive == 'editcare' || $menuactive == 'viewcaregiver' || $menuactive == 'contacts_list' || $menuactive == 'note_list' || $menuactive == 'view_note' || $menuactive_add == 'user' || $menuactive_add == 'family' || $menuactive_add == 'caregiver' || $menuactive == 'userphoto' || $menuactive == 'view_photo' || $menuactive == 'email_list' || $menuactives == 'email' || $menuactives == 'user' || $menuactive == 'view_person' || $menuactive_add == 'add') ? 'open' : ''; ?>">

               <a href="#">

                  <i class="fa fa-fw fa-user-circle-o"></i>

                  <span>Users Management</span>

                  <span class="label label-important">

                     <b class="caret"></b>

                  </span>

               </a>

               <ul>
					   <li class="<?php echo ($menuactive == 'users' || $menuactive == 'add' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive_add == 'user' || $menuactive == 'user'  || $menuactive == 'view_person') ? 'active' : ''; ?>">

                     <a href="admin/users/list"><i class="fa fa-dot-circle-o"></i> <span>Person Listing</span></a>

                  </li>

                  <?php /* ?> 

                     <li class="<?php echo ($menuactive=='family_member' || $menuactive=='addfamily_member' || $menuactive_add=='editfamily' || $menuactive=='viewfamily_member' || $menuactive_add == 'family' || $menuactive_add=='add')?'active':'';?>" >

                        <a href="admin/family_member/list"><i class="fa fa-dot-circle-o"></i> <span>Family Member List</span></a>

                     </li>

                     <li class="<?php echo ($menuactive=='caregiver' || $menuactive=='addcaregiver' || $menuactive=='editcare' || $menuactive=='viewcaregiver' || $menuactive_add == 'caregiver')?'active':'';?>" >

                        <a href="admin/caregiver/list"><i class="fa fa-dot-circle-o"></i> <span>Care Giver List</span></a>

                     </li>

                     <?php */ ?>
                  <!-- <li class="<?php echo ($menuactive == 'contacts_list') ? 'active' : ''; ?>" >

                        <a href="<?php echo base_url(); ?>admin/contacts_list"><i class="fa fa-dot-circle-o"></i> <span>Contact List</span></a>

                     </li> -->



                  <li class="<?php echo ($menuactive == 'note_list' || $menuactive == 'view_note') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/note_list"><i class="fa fa-dot-circle-o"></i> <span>Note List</span></a>

                  </li>

                  <li class="<?php echo ($menuactive == 'userphoto' || $menuactive == 'view_photo') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/userphoto/list"> <i class="fa fa-fw fa-photo"></i> <span>Photo Gallery </span></a>

                  </li>

                  <li class="<?php echo ($menuactive == 'email_list' || $menuactives == 'email') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/email_list/list"> <i class="fa fa-envelope"></i> <span>Emails</span></a>

                  </li>

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>
            <?php $menuactive = ''; ?>
            <?php $menuactives = ''; ?>
            <?php $menuactive_add = ''; ?>
            <li class="submenu <?php echo ($menuactive == 'users' || $menuactive == 'add' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'family_member' || $menuactive == 'addfamily_member' || $menuactive_add == 'editfamily' || $menuactive == 'viewfamily_member'  || $menuactive == 'caregiver' || $menuactive == 'addcaregiver' || $menuactive == 'editcare' || $menuactive == 'viewcaregiver' || $menuactive == 'contacts_list' || $menuactive == 'note_list' || $menuactive == 'view_note' || $menuactive_add == 'user' || $menuactive_add == 'family' || $menuactive_add == 'caregiver' || $menuactive == 'userphoto' || $menuactive == 'view_photo' || $menuactive == 'email_list' || $menuactives == 'email' || $menuactives == 'user' || $menuactive == 'view_person' || $menuactive_add == 'add') ? 'open' : ''; ?>">

               <a href="#">

                  <i class="fa fa-fw fa-user-circle-o"></i>

                  <span>Users Management</span>

                  <span class="label label-important">

                     <b class="caret"></b>

                  </span>

               </a>

               <ul>

                  <li class="<?php echo ($menuactive == 'users' || $menuactive == 'add' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive_add == 'user' || $menuactive == 'user'  || $menuactive == 'view_person') ? 'active' : ''; ?>">

                     <a href="admin/users/list"><i class="fa fa-dot-circle-o"></i> <span>Person Listing</span></a>

                  </li>

                  <?php /* ?> 

                     <li class="<?php echo ($menuactive=='family_member' || $menuactive=='addfamily_member' || $menuactive_add=='editfamily' || $menuactive=='viewfamily_member' || $menuactive_add == 'family' || $menuactive_add=='add')?'active':'';?>" >

                        <a href="admin/family_member/list"><i class="fa fa-dot-circle-o"></i> <span>Family Member List</span></a>

                     </li>

                     <li class="<?php echo ($menuactive=='caregiver' || $menuactive=='addcaregiver' || $menuactive=='editcare' || $menuactive=='viewcaregiver' || $menuactive_add == 'caregiver')?'active':'';?>" >

                        <a href="admin/caregiver/list"><i class="fa fa-dot-circle-o"></i> <span>Care Giver List</span></a>

                     </li>

                     <li class="<?php echo ($menuactive=='contacts_list')?'active':'';?>" >

                        <a href="<?php echo base_url(); ?>admin/contacts_list"><i class="fa fa-dot-circle-o"></i> <span>Contact List</span></a>

                     </li>

                     <?php */ ?>

                  <li class="<?php echo ($menuactive == 'note_list' || $menuactive == 'view_note') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/note_list"><i class="fa fa-dot-circle-o"></i> <span>Note List</span></a>

                  </li>

                  <li class="<?php echo ($menuactive == 'userphoto' || $menuactive == 'view_photo') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/userphoto/list"> <i class="fa fa-fw fa-photo"></i> <span>Photo Gallery </span></a>

                  </li>

                  <li class="<?php echo ($menuactive == 'email_list' || $menuactives == 'email') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/email_list/list"> <i class="fa fa-envelope"></i> <span>Emails</span></a>

                  </li>

               </ul>

            </li>

         <?php

         }

         ?>



         <?php

         if (isset($staff_permission[3]['view_permission']) == 1 || isset($staff_permission[3]['add_permission']) == 1 || isset($staff_permission[3]['edit_permission']) == 1 || isset($staff_permission[3]['delete_permission']) == 1) {

         ?>

            <!-- Doctor Management Created by 95 on 4-2-2019 Start -->

            <li class="submenu <?php echo ($menuactive == 'doctor-category' || $menuactive == 'Add' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'list' || $menuactive == 'add_doctor' || $menuactive == 'doctor_list' || $menuactive == 'doctor_schedules' || $menuactive == 'doctor_appointmensts' || $menuactive == 'view_doctor' || $menuactive == 'edit_doctor' || $menuactive == 'view_schedule_times' || $menuactive == 'edit_schedule_time' || $menuactive == 'view_appointment' || $menuactive == 'add_shedule' || $menuactive == 'medicine_schedules' || $menuactive == 'view_medicine_schedule') ? 'open' : ''; ?>">

               <a href="#">

                  <i class="fa fa-user-md" aria-hidden="true"></i>

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <span>Doctor Management</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <?php //echo $this->uri->segment(3); 
                  ?>

                  <li class="<?php echo ($menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'doctor_list' || $menuactive == 'view_doctor' || $menuactive == 'edit_doctor' || $menuactive == 'add_doctor' || $menuactive == 'add_shedule') ? 'active' : ''; ?>">

                     <a href="admin/doctor_list"><i class="fa fa-dot-circle-o"></i> <span>Doctor List</span></a>

                  </li>

                  <li class="<?php echo ($menuactive == 'doctor-category' || $menuactive == 'edit' || $menuactive == 'Add' || $menuactive == 'view' || $menuactive == 'list') ? 'active' : ''; ?>">

                     <a href="admin/doctor-category/list"><i class="fa fa-dot-circle-o"></i> <span>Doctor Specialties List</span></a>

                  </li>

                  <li class="<?php echo ($menuactive == 'doctor_schedules' || $menuactive == 'view_schedule_times' || $menuactive == 'edit_schedule_time') ? 'active' : ''; ?>">

                     <a href="admin/doctor_schedules"><i class="fa fa-dot-circle-o"></i> <span>Doctor Schedule List</span></a>

                  </li>

                  <li class="<?php echo ($menuactive == 'doctor_appointmensts' || $menuactive == 'view_appointment') ? 'active' : ''; ?>">

                     <a href="admin/doctor_appointmensts"><i class="fa fa-dot-circle-o"></i> <span>Doctor Appointments</span></a>

                  </li>

                  <li class="<?php echo ($menuactive == 'medicine_schedules' || $menuactive == 'view_medicine_schedule') ? 'active' : ''; ?>">

                     <a href="admin/medicine_schedules"><i class="fa fa-dot-circle-o"></i> <span>Medicine Schedule List</span></a>

                  </li>

                  <!-- <li class="<?php echo ($menuactive == 'Add') ? 'active' : ''; ?>" >

                        <a href="admin/doctor-category/Add">

                        

                          <i class="fa fa-dot-circle-o"></i>

                          <span>Add Specialties</span></a>

                       </li> -->

                  <!-- <li class="<?php echo ($menuactive == 'doctor-category' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'add_doctor') ? 'active' : ''; ?>" >

                        <a href="admin/add_doctor"><i class="fa fa-dot-circle-o"></i> <span>Add Doctor</span></a>

                     </li> -->

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <!-- Doctor Management Created by 95 on 4-2-2019 Start -->

            <li class="submenu <?php echo ($menuactive == 'doctor-category' || $menuactive == 'Add' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'list' || $menuactive == 'add_doctor' || $menuactive == 'doctor_list' || $menuactive == 'doctor_schedules' || $menuactive == 'doctor_appointmensts' || $menuactive == 'view_doctor' || $menuactive == 'edit_doctor' || $menuactive == 'view_schedule_times' || $menuactive == 'edit_schedule_time' || $menuactive == 'view_appointment' || $menuactive == 'add_shedule' || $menuactive == 'medicine_schedules' || $menuactive == 'view_medicine_schedule') ? 'open' : ''; ?>">

               <a href="#">

                  <i class="fa fa-user-md" aria-hidden="true"></i>

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <span>Doctor Management</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <?php //echo $this->uri->segment(3); 
                  ?>

                  <li class="<?php echo ($menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'doctor_list' || $menuactive == 'view_doctor' || $menuactive == 'edit_doctor' || $menuactive == 'add_doctor' || $menuactive == 'add_shedule') ? 'active' : ''; ?>">

                     <a href="admin/doctor_list"><i class="fa fa-dot-circle-o"></i> <span>Doctor List</span></a>

                  </li>

                  <li class="<?php echo ($menuactive == 'doctor-category' || $menuactive == 'edit' || $menuactive == 'Add' || $menuactive == 'view' || $menuactive == 'list') ? 'active' : ''; ?>">

                     <a href="admin/doctor-category/list"><i class="fa fa-dot-circle-o"></i> <span>Doctor Specialties List</span></a>

                  </li>

                  <li class="<?php echo ($menuactive == 'doctor_schedules' || $menuactive == 'view_schedule_times' || $menuactive == 'edit_schedule_time') ? 'active' : ''; ?>">

                     <a href="admin/doctor_schedules"><i class="fa fa-dot-circle-o"></i> <span>Doctor Schedule List</span></a>

                  </li>

                  <li class="<?php echo ($menuactive == 'doctor_appointmensts' || $menuactive == 'view_appointment') ? 'active' : ''; ?>">

                     <a href="admin/doctor_appointmensts"><i class="fa fa-dot-circle-o"></i> <span>Doctor Appointments</span></a>

                  </li>

                  <li class="<?php echo ($menuactive == 'medicine_schedules' || $menuactive == 'view_medicine_schedule') ? 'active' : ''; ?>">

                     <a href="admin/medicine_schedules"><i class="fa fa-dot-circle-o"></i> <span>Medicine Schedule List</span></a>

                  </li>

                  <!-- <li class="<?php echo ($menuactive == 'Add') ? 'active' : ''; ?>" >

                        <a href="admin/doctor-category/Add">

                        

                          <i class="fa fa-dot-circle-o"></i>

                          <span>Add Specialties</span></a>

                       </li> -->

                  <!-- <li class="<?php echo ($menuactive == 'doctor-category' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'add_doctor') ? 'active' : ''; ?>" >

                        <a href="admin/add_doctor"><i class="fa fa-dot-circle-o"></i> <span>Add Doctor</span></a>

                     </li> -->

               </ul>

            </li>

         <?php

         }

         ?>



         <!-- Doctor Management Created by 95 on 4-2-2019 End -->

         <!-- <li class="<?php echo ($menuactive == 'userphoto') ? 'active' : ''; ?>" >

               <a href="admin/userphoto/list"> <i class="fa fa-fw fa-photo"></i> <span>Photo Gallery </span></a>

               </li>

               <li class="<?php echo ($menuactive == 'email_list') ? 'active' : ''; ?>" >

               <a href="admin/email_list/list"> <i class="fa fa-envelope"></i> <span>Emails</span></a>

            </li> -->



         <?php

         if (isset($staff_permission[4]['view_permission']) == 1 || isset($staff_permission[4]['add_permission']) == 1 || isset($staff_permission[4]['edit_permission']) == 1 || isset($staff_permission[4]['delete_permission']) == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'Test_type' || $menuactive == 'add_test_type' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'test_type_list' || $menuactive == 'edit_test_type') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <i class="fa fa-file-text-o" aria-hidden="true"></i>

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <span>Test Type</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li class="<?php echo ($menuactive == 'test_type_list' || $menuactive == 'edit_test_type' || $menuactive == 'add_test_type') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/test_type_list"><i class="fa fa-dot-circle-o"></i> <span>Test Type List</span></a>

                  </li>

                  <!-- <li class="<?php echo ($menuactive == 'add_test_type') ? 'active' : ''; ?>" >

                        <a href="admin/add_test_type"><i class="fa fa-dot-circle-o"></i> <span>Add test type</span></a>

                     </li> -->

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'Test_type' || $menuactive == 'add_test_type' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'test_type_list' || $menuactive == 'edit_test_type') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <i class="fa fa-file-text-o" aria-hidden="true"></i>

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <span>Test Type</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li class="<?php echo ($menuactive == 'test_type_list' || $menuactive == 'edit_test_type' || $menuactive == 'add_test_type') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/test_type_list"><i class="fa fa-dot-circle-o"></i> <span>Test Type List</span></a>

                  </li>

                  <!-- <li class="<?php echo ($menuactive == 'add_test_type') ? 'active' : ''; ?>" >

                        <a href="admin/add_test_type"><i class="fa fa-dot-circle-o"></i> <span>Add test type</span></a>

                     </li> -->

               </ul>

            </li>

         <?php

         }

         ?>



         <?php

         if (isset($staff_permission[5]['view_permission']) == 1 || isset($staff_permission[5]['add_permission']) == 1 || isset($staff_permission[5]['edit_permission']) == 1 || isset($staff_permission[5]['delete_permission']) == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'staff' || $menuactive == 'add_staff' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'staff_list' || $menuactive == 'edit_staff') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <i class="fa fa-file-text-o" aria-hidden="true"></i>

                  <span>Staff Maagement</span>

                  <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li class="<?php echo ($menuactive == 'staff_list' || $menuactive == 'edit_staff' || $menuactive == 'add_staff') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/staff_list"><i class="fa fa-dot-circle-o"></i> <span>Staff List</span></a>

                  </li>

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'staff' || $menuactive == 'add_staff' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'staff_list' || $menuactive == 'edit_staff') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <i class="fa fa-file-text-o" aria-hidden="true"></i>

                  <span>Staff Maagement</span>

                  <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li class="<?php echo ($menuactive == 'staff_list' || $menuactive == 'edit_staff' || $menuactive == 'add_staff') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/staff_list"><i class="fa fa-dot-circle-o"></i> <span>Staff List</span></a>

                  </li>

               </ul>

            </li>

         <?php

         }

         ?>



         <?php

         if (isset($staff_permission[6]['view_permission']) == 1 || isset($staff_permission[6]['add_permission']) == 1 || isset($staff_permission[6]['edit_permission']) == 1 || isset($staff_permission[6]['delete_permission']) == 1) {

         ?>

            <!-- Vital module start -->

            <li class="submenu <?php echo ($menuactive == 'Vital_sign' || $menuactive == 'add_vital_sign' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'vital_sign_list' || $menuactive == 'edit_vital_sign') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <i class="fa fa-file-text-o" aria-hidden="true"></i>

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <span>Vital Sign Test Type</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li class="<?php echo ($menuactive == 'vital_sign_list' || $menuactive == 'edit_vital_sign' || $menuactive == 'add_vital_sign') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/vital_sign_list"><i class="fa fa-dot-circle-o"></i> <span>Vital Sign Test Type List</span></a>

                  </li>

                  <!-- <li class="<?php echo ($menuactive == 'add_test_type') ? 'active' : ''; ?>" >

                        <a href="admin/add_test_type"><i class="fa fa-dot-circle-o"></i> <span>Add test type</span></a>

                     </li> -->

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <!-- Vital module start -->

            <li class="submenu <?php echo ($menuactive == 'Vital_sign' || $menuactive == 'add_vital_sign' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'vital_sign_list' || $menuactive == 'edit_vital_sign') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <i class="fa fa-file-text-o" aria-hidden="true"></i>

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <span>Vital Sign Test Type</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li class="<?php echo ($menuactive == 'vital_sign_list' || $menuactive == 'edit_vital_sign' || $menuactive == 'add_vital_sign') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/vital_sign_list"><i class="fa fa-dot-circle-o"></i> <span>Vital Sign Test Type List</span></a>

                  </li>

                  <!-- <li class="<?php echo ($menuactive == 'add_test_type') ? 'active' : ''; ?>" >

                        <a href="admin/add_test_type"><i class="fa fa-dot-circle-o"></i> <span>Add test type</span></a>

                     </li> -->

               </ul>

            </li>

         <?php

         }

         ?>





         <?php /* ?>

            <!-- Tab section start -->



            <?php

            if ($staff_permission[6]['view_permission'] == 1 || $staff_permission[6]['add_permission'] == 1 || $staff_permission[6]['edit_permission'] == 1 || $staff_permission[6]['delete_permission'] == 1) {

               ?>

               <!-- Vital module start -->

               <li class="submenu <?php echo ($menuactive=='edit_feature_category' || $menuactive=='feature_category_list' || $menuactive=='add_feature_category' || $menuactive=='edit_feature_category' ||$menuactive=='feature_list' || $menuactive=='add_feature' || $menuactive=='edit_feature')?'open':'';?>">

                  <a href="javascript:void(0);">

                     <i class="fa fa-file-text-o" aria-hidden="true"></i>

                     <!-- <i class="fa fa-fw fa-user-circle-o"></i> --> 

                     <span>Tab Features</span> <span class="label label-important"><b class="caret"></b></span>

                  </a>

                  <ul>

                     <li class="<?php echo ($menuactive=='feature_category_list'  || $menuactive=='add_feature_category' || $menuactive=='edit_feature_category')?'active':'';?>" >

                        <a href="<?php echo base_url(); ?>admin/feature_category_list"><i class="fa fa-dot-circle-o"></i> <span>Feature category list</span></a>

                     </li>

                     <li class="<?php echo ($menuactive=='feature_list' || $menuactive=='add_feature' || $menuactive=='edit_feature')?'active':'';?>" >

                        <a href="admin/feature_list"><i class="fa fa-dot-circle-o"></i> <span>Feature list</span></a>

                     </li>

                  </ul>

               </li>

               <?php                              

            } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

               ?>

               <!-- Vital module start -->

               <li class="submenu <?php echo ($menuactive=='edit_feature_category' || $menuactive=='feature_category_list' || $menuactive=='add_feature_category' || $menuactive=='edit_feature_category' ||$menuactive=='feature_list' || $menuactive=='add_feature' || $menuactive=='edit_feature')?'open':'';?>">

                  <a href="javascript:void(0);">

                     <i class="fa fa-file-text-o" aria-hidden="true"></i>

                     <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                     <span>Tab Features</span> <span class="label label-important"><b class="caret"></b></span>

                  </a>

                  <ul>

                     <li class="<?php echo ($menuactive=='feature_category_list'  || $menuactive=='add_feature_category' || $menuactive=='edit_feature_category')?'active':'';?>" >

                        <a href="<?php echo base_url(); ?>admin/feature_category_list"><i class="fa fa-dot-circle-o"></i> <span>Feature category list</span></a>

                     </li>

                     <li class="<?php echo ($menuactive=='feature_list' || $menuactive=='add_feature' || $menuactive=='edit_feature')?'active':'';?>" >

                        <a href="admin/feature_list"><i class="fa fa-dot-circle-o"></i> <span>Feature list</span></a>

                     </li>

                  </ul>

               </li>

               <?php

            }

            ?>





            <?php */ ?>



         <!-- Tab section end -->



         <!-- Mail template section start -->



         <?php

         if (isset($staff_permission[6]['view_permission']) == 1 || isset($staff_permission[6]['add_permission']) == 1 || isset($staff_permission[6]['edit_permission']) == 1 || isset($staff_permission[6]['delete_permission']) == 1) {

         ?>

            <!-- Vital module start -->

            <li class="submenu <?php echo ($menuactive == 'mail_templates' || $menuactive == 'edit_template') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <i class="fa fa-file-text-o" aria-hidden="true"></i>

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <span>Mail templates</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li class="<?php echo ($menuactive == 'mail_templates' || $menuactive == 'edit_template') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/mail_templates"><i class="fa fa-dot-circle-o"></i> <span>Template List</span></a>

                  </li>

                  <!-- <li class="<?php echo ($menuactive == 'feature_list' || $menuactive == 'add_feature' || $menuactive == 'edit_feature') ? 'active' : ''; ?>" >

                        <a href="admin/feature_list"><i class="fa fa-dot-circle-o"></i> <span>Feature list</span></a>

                     </li> -->

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <!-- Vital module start -->

            <li class="submenu <?php echo ($menuactive == 'mail_templates' || $menuactive == 'edit_template' || $menuactive == 'view_template') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <i class="fa fa-file-text-o" aria-hidden="true"></i>

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <span>Mail templates</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li class="<?php echo ($menuactive == 'mail_templates'  || $menuactive == 'edit_template' || $menuactive == 'view_template') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/mail_templates"><i class="fa fa-dot-circle-o"></i> <span>Template List</span></a>

                  </li>

                  <!-- <li class="<?php echo ($menuactive == 'feature_list' || $menuactive == 'add_feature' || $menuactive == 'edit_feature') ? 'active' : ''; ?>" >

                        <a href="admin/feature_list"><i class="fa fa-dot-circle-o"></i> <span>Feature list</span></a>

                     </li> -->

               </ul>

            </li>

         <?php

         }

         ?>









         <!-- Mail template section end -->

         <!-- New CMS -->

         <?php

         if (isset($staff_permission[13]['view_permission']) == 1 || isset($staff_permission[13]['add_permission']) == 1 || isset($staff_permission[13]['edit_permission']) == 1 || isset($staff_permission[13]['delete_permission']) == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'pages_list'  || $menuactive == 'edit_page' || $menuactive == 'edit_feature_category' || $menuactive == 'feature_category_list' || $menuactive == 'add_feature_category' || $menuactive == 'feature_list' || $menuactive == 'add_feature' || $menuactive == 'edit_feature' || $menuactive == 'about-us' || $menuactive == 'faq_list' || $menuactive == 'add_faq' || $menuactive == 'testimonial_list' || $menuactive == 'add_testimonial' || $menuactive == 'add_story' || $menuactive == 'story_list' || $menuactive == 'edit_story' || $menuactive == 'add_faq' || $menuactive == 'footer_content_list' || $menuactive == 'edit_footer_content') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-book" aria-hidden="true"></i> -->

                  <i class="fa fa-edit" aria-hidden="true"></i>

                  <span>CMS</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i> Pages</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'pages_list'  || $menuactive == 'edit_page') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/pages_list"><i class="fa fa-angle-double-right"></i> <span>Pages</span></a>

                        </li>



                     </ul>

                  </li>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i> Tab Features</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'feature_category_list' || $menuactive == 'add_feature_category' || $menuactive == 'edit_feature_category') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/feature_category_list"><i class="fa fa-angle-double-right"></i> <span>Feature category list</span></a>

                        </li>

                        <li <?php if ($menuactive == 'feature_list' || $menuactive == 'add_feature' || $menuactive == 'edit_feature') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/feature_list"><i class="fa fa-angle-double-right"></i> Feature list</a>

                        </li>



                     </ul>

                  </li>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i> About us</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'about-us') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/about-us"><i class="fa fa-angle-double-right"></i> <span>About us</span></a>

                        </li>



                     </ul>

                  </li>



                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i> FAQ</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'faq_list' || $menuactive == 'add_faq') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/faq_list"><i class="fa fa-angle-double-right"></i> <span>FAQ</span></a>

                        </li>



                     </ul>

                  </li>



                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i>Testimonial</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'testimonial_list' || $menuactive == 'add_testimonial') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/testimonial_list"><i class="fa fa-angle-double-right"></i> <span>Testimonial</span></a>

                        </li>



                     </ul>

                  </li>



                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i>Member stories</span>

                     <ul class="frd_swtChild">







                        <li <?php if ($menuactive == 'add_story' || $menuactive == 'story_list' || $menuactive == 'edit_story') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/story_list"><i class="fa fa-angle-double-right"></i> <span>Member story list</span></a>

                        </li>



                     </ul>

                  </li>





                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i>Footer</span>

                     <ul class="frd_swtChild">







                        <li <?php if ($menuactive == 'edit_footer_content' || $menuactive == 'footer_content_list') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/footer_content_list"><i class="fa fa-angle-double-right"></i> <span>Footer content</span></a>

                        </li>



                     </ul>

                  </li>

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'pages_list'  || $menuactive == 'edit_page' || $menuactive == 'edit_feature_category' || $menuactive == 'feature_category_list' || $menuactive == 'add_feature_category' || $menuactive == 'feature_list' || $menuactive == 'add_feature' || $menuactive == 'edit_feature' || $menuactive == 'about-us' || $menuactive == 'faq_list' || $menuactive == 'add_faq' || $menuactive == 'testimonial_list' || $menuactive == 'add_testimonial' || $menuactive == 'add_story' || $menuactive == 'story_list' || $menuactive == 'edit_story' || $menuactive == 'add_faq' || $menuactive == 'footer_content_list' || $menuactive == 'edit_footer_content') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-book" aria-hidden="true"></i> -->

                  <i class="fa fa-edit" aria-hidden="true"></i>

                  <span>CMS</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i> Pages</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'pages_list'  || $menuactive == 'edit_page') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/pages_list"><i class="fa fa-angle-double-right"></i> <span>Pages</span></a>

                        </li>



                     </ul>

                  </li>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i> Tab Features</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'feature_category_list' || $menuactive == 'add_feature_category' || $menuactive == 'edit_feature_category') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/feature_category_list"><i class="fa fa-angle-double-right"></i> <span>Feature category list</span></a>

                        </li>

                        <li <?php if ($menuactive == 'feature_list' || $menuactive == 'add_feature' || $menuactive == 'edit_feature') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/feature_list"><i class="fa fa-angle-double-right"></i> Feature list</a>

                        </li>



                     </ul>

                  </li>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i> About us</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'about-us') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/about-us"><i class="fa fa-angle-double-right"></i> <span>About us</span></a>

                        </li>



                     </ul>

                  </li>



                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i> FAQ</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'faq_list' || $menuactive == 'add_faq') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/faq_list"><i class="fa fa-angle-double-right"></i> <span>FAQ</span></a>

                        </li>



                     </ul>

                  </li>



                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i>Testimonial</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'testimonial_list' || $menuactive == 'add_testimonial') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/testimonial_list"><i class="fa fa-angle-double-right"></i> <span>Testimonial</span></a>

                        </li>



                     </ul>

                  </li>



                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i>Member stories</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'add_story' || $menuactive == 'story_list' || $menuactive == 'edit_story') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/story_list"><i class="fa fa-angle-double-right"></i> <span>Member story list</span></a>

                        </li>



                     </ul>

                  </li>







                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i>Footer</span>

                     <ul class="frd_swtChild">







                        <li <?php if ($menuactive == 'edit_footer_content' || $menuactive == 'footer_content_list') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/footer_content_list"><i class="fa fa-angle-double-right"></i> <span>Footer content</span></a>

                        </li>



                     </ul>

                  </li>

               </ul>

            </li>

         <?php

         }

         ?>

         <!-- CMS END -->

         <!-- Static pages section start -->





         <?php /* ?>

            <?php

            if ($staff_permission[6]['view_permission'] == 1 || $staff_permission[6]['add_permission'] == 1 || $staff_permission[6]['edit_permission'] == 1 || $staff_permission[6]['delete_permission'] == 1) {

               ?>

               <!-- Vital module start -->

               <li class="submenu <?php echo ($menuactive=='pages_list'  ||$menuactive=='edit_page' )?'open':'';?>">

                  <a href="javascript:void(0);">

                     <i class="fa fa-file-text-o" aria-hidden="true"></i>

                     <!-- <i class="fa fa-fw fa-user-circle-o"></i> --> 

                     <span>CMS</span> <span class="label label-important"><b class="caret"></b></span>

                  </a>

                  <ul>

                     <li class="<?php echo ($menuactive=='pages_list'  ||$menuactive=='edit_page' )?'active':'';?>" >

                        <a href="<?php echo base_url(); ?>admin/pages_list"><i class="fa fa-dot-circle-o"></i> <span>Pages</span></a>

                     </li>

                     <!-- <li class="<?php echo ($menuactive=='feature_list' || $menuactive=='add_feature' || $menuactive=='edit_feature')?'active':'';?>" >

                        <a href="admin/feature_list"><i class="fa fa-dot-circle-o"></i> <span>Feature list</span></a>

                     </li> -->

                  </ul>

               </li>

               <?php                              

            } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

               ?>

               <!-- Vital module start -->

               <li class="submenu <?php echo ($menuactive=='pages_list'  ||$menuactive=='edit_page' )?'open':'';?>">

                  <a href="javascript:void(0);">

                     <i class="fa fa-file-text-o" aria-hidden="true"></i>

                     <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                     <span>CMS</span> <span class="label label-important"><b class="caret"></b></span>

                  </a>

                  <ul>

                     <li class="<?php echo ($menuactive=='pages_list'  ||$menuactive=='edit_page' )?'active':'';?>" >

                        <a href="<?php echo base_url(); ?>admin/pages_list"><i class="fa fa-dot-circle-o"></i> <span>Pages</span></a>

                     </li>

                     <!-- <li class="<?php echo ($menuactive=='feature_list' || $menuactive=='add_feature' || $menuactive=='edit_feature')?'active':'';?>" >

                        <a href="admin/feature_list"><i class="fa fa-dot-circle-o"></i> <span>Feature list</span></a>

                     </li> -->

                  </ul>

               </li>

               <?php

            }

            ?>





            <?php */ ?>



         <!-- Static pages section end -->







         <!-- Story section start -->



         <?php /* ?>

            <?php

            if ($staff_permission[6]['view_permission'] == 1 || $staff_permission[6]['add_permission'] == 1 || $staff_permission[6]['edit_permission'] == 1 || $staff_permission[6]['delete_permission'] == 1) {

               ?>

               <!-- Vital module start -->

               <li class="submenu <?php echo ($menuactive=='add_story' || $menuactive=='story_list' || $menuactive=='edit_story')?'open':'';?>">

                  <a href="javascript:void(0);">

                     <i class="fa fa-file-text-o" aria-hidden="true"></i>

                     <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                     <span>Member Stories</span> <span class="label label-important"><b class="caret"></b></span>

                  </a>

                  <ul>

                     <li class="<?php echo ($menuactive=='add_story' || $menuactive=='story_list' || $menuactive=='edit_story')?'active':'';?>" >

                        <a href="<?php echo base_url(); ?>admin/feature_category_list"><i class="fa fa-dot-circle-o"></i> <span>Member story list</span></a>

                     </li>

                     <!-- <li class="<?php echo ($menuactive=='feature_list')?'active':'';?>" >

                        <a href="admin/feature_list"><i class="fa fa-dot-circle-o"></i> <span>Feature list</span></a>

                     </li> -->

                  </ul>

               </li>

               <?php                              

            } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

               ?>

               <!-- Vital module start -->

               <li class="submenu <?php echo ($menuactive=='add_story' || $menuactive=='story_list' || $menuactive=='edit_story')?'open':'';?>">

                  <a href="javascript:void(0);">

                     <i class="fa fa-file-text-o" aria-hidden="true"></i>

                     <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                     <span>Member stories</span> <span class="label label-important"><b class="caret"></b></span>

                  </a>

                  <ul>

                     <li class="<?php echo ($menuactive=='story_list'  || $menuactive=='add_story' || $menuactive=='edit_story')?'active':'';?>" >

                        <a href="<?php echo base_url(); ?>admin/story_list"><i class="fa fa-dot-circle-o"></i> <span>Member story list</span></a>

                     </li>

                     <!-- <li class="<?php echo ($menuactive=='feature_list' || $menuactive=='add_feature' )?'active':'';?>" >

                        <a href="admin/feature_list"><i class="fa fa-dot-circle-o"></i> <span>Feature list</span></a>

                     </li> -->

                  </ul>

               </li>

               <?php

            }

            ?>



            <?php */ ?>





         <!-- Story section end -->













         <!-- Vital module end -->

         <!-- Vital module start -->



         <?php

         if (isset($staff_permission[7]['view_permission']) == 1 || isset($staff_permission[7]['add_permission']) == 1 || isset($staff_permission[7]['edit_permission']) == 1 || isset($staff_permission[7]['delete_permission']) == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'vital_sign' || $menuactive == 'edit' || $menuactive == 'view_vital_sign') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <i class="fa fa-file-text-o" aria-hidden="true"></i>

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <span>Vital Sign</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li class="<?php echo ($menuactive == 'vital_sign' || $menuactive == 'view_vital_sign') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/vital_sign"><i class="fa fa-dot-circle-o"></i> <span>Vital Sign List</span></a>

                  </li>

                  <!-- <li class="<?php echo ($menuactive == 'add_test_type') ? 'active' : ''; ?>" >

                        <a href="admin/add_test_type"><i class="fa fa-dot-circle-o"></i> <span>Add test type</span></a>

                     </li> -->

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'vital_sign' || $menuactive == 'edit' || $menuactive == 'view_vital_sign') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <i class="fa fa-file-text-o" aria-hidden="true"></i>

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <span>Vital Sign</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li class="<?php echo ($menuactive == 'vital_sign' || $menuactive == 'view_vital_sign') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/vital_sign"><i class="fa fa-dot-circle-o"></i> <span>Vital Sign List</span></a>

                  </li>

                  <!-- <li class="<?php echo ($menuactive == 'add_test_type') ? 'active' : ''; ?>" >

                        <a href="admin/add_test_type"><i class="fa fa-dot-circle-o"></i> <span>Add test type</span></a>

                     </li> -->

               </ul>

            </li>

         <?php

         }

         ?>

         <!-- front management start -->





         <?php

         if (isset($staff_permission[7]['view_permission']) == 1 || isset($staff_permission[7]['add_permission']) == 1 || isset($staff_permission[7]['edit_permission']) == 1 || isset($staff_permission[7]['delete_permission']) == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'app_list' || $menuactive == 'add_app' || $menuactive == 'edit_app' || $menuactive == 'view_app') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <i class="fa fa-file-text-o" aria-hidden="true"></i>

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <span>Option management</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li class="<?php echo ($menuactive == 'app_list' || $menuactive == 'add_app' || $menuactive == 'edit_app' || $menuactive == 'view_app') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/app_list"><i class="fa fa-dot-circle-o"></i> <span>Social management</span></a>

                  </li>

                  <!-- <li class="<?php echo ($menuactive == 'add_test_type') ? 'active' : ''; ?>" >

                        <a href="admin/add_test_type"><i class="fa fa-dot-circle-o"></i> <span>Add test type</span></a>

                     </li> -->

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'app_list' || $menuactive == 'add_app' || $menuactive == 'edit_app' || $menuactive == 'view_app') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <i class="fa fa-file-text-o" aria-hidden="true"></i>

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <span>Option management</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li class="<?php echo ($menuactive == 'app_list' || $menuactive == 'add_app' || $menuactive == 'edit_app' || $menuactive == 'view_app') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/app_list"><i class="fa fa-dot-circle-o"></i> <span>Social management</span></a>

                  </li>

                  <!-- <li class="<?php echo ($menuactive == 'add_test_type') ? 'active' : ''; ?>" >

                        <a href="admin/add_test_type"><i class="fa fa-dot-circle-o"></i> <span>Add test type</span></a>

                     </li> -->

               </ul>

            </li>

         <?php

         }

         ?>


         <!-- End -->

         <?php

         if (isset($staff_permission[8]['view_permission']) == 1 || isset($staff_permission[8]['add_permission']) == 1 || isset($staff_permission[8]['edit_permission']) == 1 || isset($staff_permission[8]['delete_permission']) == 1) {

         ?>

            <!-- End -->

            <li class="<?php echo ($menuactive == 'acknowledge_list' || $menuactive == 'view_acknowledge') ? 'active' : ''; ?>">

               <a href="admin/acknowledge_list"> <i class="fa fa-file" aria-hidden="true"></i> <span>Acknowledge</span></a>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <!-- End -->

            <li class="<?php echo ($menuactive == 'acknowledge_list' || $menuactive == 'view_acknowledge') ? 'active' : ''; ?>">

               <a href="admin/acknowledge_list"> <i class="fa fa-file" aria-hidden="true"></i> <span>Acknowledge</span></a>

            </li>

         <?php

         }

         ?>



         <?php /* ?>



            <?php

            if ($staff_permission[8]['view_permission'] == 1 || $staff_permission[8]['add_permission'] == 1 || $staff_permission[8]['edit_permission'] == 1 || $staff_permission[8]['delete_permission'] == 1) {

               ?>

               <!-- End -->

               <li class="<?php echo ($menuactive=='about-us')?'active':'';?>" >

                  <a href="admin/about-us"> <i class="fa fa-file" aria-hidden="true"></i> <span>About us</span></a>

               </li>

               <?php                  

            } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

               ?>

               <!-- End -->

               <li class="<?php echo ($menuactive=='about-us')?'active':'';?>" >

                  <a href="admin/about-us"> <i class="fa fa-file" aria-hidden="true"></i> <span>About us</span></a>

               </li>

               <?php

            }

            ?>



            <?php */ ?>









         <?php

         if (isset($staff_permission[16]['view_permission']) == 1 || isset($staff_permission[16]['add_permission']) == 1 || isset($staff_permission[16]['edit_permission']) == 1 || isset($staff_permission[16]['delete_permission']) == 1) {

         ?>

            <!-- End -->

            <li class="<?php echo ($this->uri->segment(2) == 'subscriptions') ? 'active' : ''; ?>">

               <a href="admin/subscriptions">

                  <i class="fa fa-file" aria-hidden="true"></i>

                  <span>Subscription</span>

               </a>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <!-- End -->

            <li class="<?php echo ($menuactive == 'subscriptions') ? 'active' : ''; ?>">

               <a href="admin/subscriptions">

                  <i class="fa fa-file" aria-hidden="true"></i>

                  <span>Subscription</span>

               </a>

            </li>

         <?php

         }

         ?>

         <?php //echo "<pre>"; print_r($data);  
         ?>

         <!-- News letters -->

         <?php

         if (isset($staff_permission[16]['view_permission']) == 1 || isset($staff_permission[16]['add_permission']) == 1 || isset($staff_permission[16]['edit_permission']) == 1 || isset($staff_permission[16]['delete_permission']) == 1) {

         ?>



            <li class="<?php echo ($menuactive == 'news-letter-subscribers') ? 'active' : ''; ?>">

               <a href="admin/news-letter-subscribers">

                  <i class="fa fa-file" aria-hidden="true"></i>

                  <span>News letter Subscribers </span>

               </a>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>



            <li class="<?php echo ($menuactive == 'news-letter-subscribers') ? 'active' : ''; ?>">

               <a href="admin/news-letter-subscribers">

                  <i class="fa fa-file" aria-hidden="true"></i>

                  <span>News letter Subscribers </span>

               </a>

            </li>

         <?php

         }

         ?>

         <?php /* ?>

        <?php

        if ($staff_permission[17]['view_permission'] == 1 || $staff_permission[17]['add_permission'] == 1 || $staff_permission[17]['edit_permission'] == 1 || $staff_permission[17]['delete_permission'] == 1) {

         ?>

         <!-- End -->

         <li class="<?php echo ($menuactive=='faq_list' || $menuactive=='add_faq') ? 'active' : '';?>" >

            <a href="admin/faq_list">

              <i class="fa fa-file" aria-hidden="true"></i> 

              <span>FAQ</span>

           </a>

        </li>

        <?php

     } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

      ?>

      <!-- End -->

      <li class="<?php echo ($menuactive=='faq_list' || $menuactive=='add_faq') ? 'active' : '';?>" >

         <a href="admin/faq_list">

           <i class="fa fa-file" aria-hidden="true"></i> 

           <span>FAQ</span>

        </a>

     </li>

     <?php

  }

  ?>

  <?php */ ?>

         <?php

         if (isset($staff_permission[19]['view_permission']) == 1 || isset($staff_permission[19]['add_permission']) == 1 || isset($staff_permission[19]['edit_permission']) == 1 || isset($staff_permission[19]['delete_permission']) == 1) {

         ?>

            <!-- End -->

            <li class="<?php echo ($menuactive == 'plan_list' || $menuactive == 'add_plan'  || $menuactive == 'edit_plan') ? 'active' : ''; ?>">

               <a href="admin/plan_list">

                  <i class="fa fa-file" aria-hidden="true"></i>

                  <span>Subscription Plan</span>

               </a>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <!-- End -->

            <li class="<?php echo ($menuactive == 'plan_list' || $menuactive == 'add_plan' || $menuactive == 'edit_plan') ? 'active' : ''; ?>">

               <a href="admin/plan_list">

                  <i class="fa fa-file" aria-hidden="true"></i>

                  <span>Subscription Plan</span>

               </a>

            </li>

         <?php

         }

         ?>



         <?php /* ?>

<?php

if ($staff_permission[18]['view_permission'] == 1 || $staff_permission[18]['add_permission'] == 1 || $staff_permission[18]['edit_permission'] == 1 || $staff_permission[18]['delete_permission'] == 1) {

   ?>

   <!-- End -->

   <li class="<?php echo ($menuactive=='testimonial_list' || $menuactive=='add_testimonial') ? 'active' : '';?>" >

      <a href="admin/testimonial_list">

        <i class="fa fa-file" aria-hidden="true"></i> 

        <span>Testimonial</span>

     </a>

  </li>

  <?php

} elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

   ?>

   <!-- End -->

   <li class="<?php echo ($menuactive=='testimonial_list' || $menuactive=='add_testimonial') ? 'active' : '';?>" >

      <a href="admin/testimonial_list">

        <i class="fa fa-file" aria-hidden="true"></i> 

        <span>Testimonial</span>

     </a>

  </li>

  <?php

}

?>

<?php */ ?>

         <!-- <li class="submenu <?php echo ($menuactive == 'Music' || $menuactive == 'add_music' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'music_list') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                 <i class="fa fa-music" aria-hidden="true"></i>

               

                 <span>Music</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

               

               

               

               <li class="<?php echo ($menuactive == 'music_list') ? 'active' : ''; ?>" >

                     <a href="<?php echo base_url(); ?>admin/music_list"><i class="fa fa-dot-circle-o"></i> <span>Music List</span></a>

                   </li>

               

               </ul>

               

            </li> -->

         <!-- <li class="submenu <?php echo ($menuactive == 'Movie' || $menuactive == 'add_movie' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'movie_list') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                 <i class="fa fa-film" aria-hidden="true"></i>

               

                 <span>Movie</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

               

               

               <li class="<?php echo ($menuactive == 'movie_list') ? 'active' : ''; ?>" >

                     <a href="<?php echo base_url(); ?>admin/movie_list"><i class="fa fa-dot-circle-o"></i> <span>Movie List</span></a>

                   </li>

               

               </ul>

               

            </li> -->



         <?php

         if (isset($staff_permission[9]['view_permission']) == 1 || isset($staff_permission[9]['add_permission']) == 1 || isset($staff_permission[9]['edit_permission']) == 1 || isset($staff_permission[9]['delete_permission']) == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'Pharma_company' || $menuactive == 'add_pharmacompany' || $menuactive == 'edit' || $menuactive == 'pharmacompany_list' || $menuactive == 'view_pharmacompany' || $menuactive == 'edit_pharmacompany') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-film" aria-hidden="true"></i> -->

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <i class="fa fa-medkit" aria-hidden="true"></i>

                  <span>Pharma Company</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <!-- <li class="<?php echo ($menuactive == 'add_pharmacompany') ? 'active' : ''; ?>" >

                        <a href="admin/add_pharmacompany"><i class="fa fa-dot-circle-o"></i> <span>Add Pharma Company</span></a>

                     </li> -->

                  <li class="<?php echo ($menuactive == 'pharmacompany_list' || $menuactive == 'add_pharmacompany' || $menuactive == 'view_pharmacompany' || $menuactive == 'edit_pharmacompany') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/pharmacompany_list"><i class="fa fa-dot-circle-o"></i> <span>Pharma Company List</span></a>

                  </li>

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'Pharma_company' || $menuactive == 'add_pharmacompany' || $menuactive == 'edit' || $menuactive == 'pharmacompany_list' || $menuactive == 'view_pharmacompany' || $menuactive == 'edit_pharmacompany') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-film" aria-hidden="true"></i> -->

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <i class="fa fa-medkit" aria-hidden="true"></i>

                  <span>Pharma Company</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <!-- <li class="<?php echo ($menuactive == 'add_pharmacompany') ? 'active' : ''; ?>" >

                        <a href="admin/add_pharmacompany"><i class="fa fa-dot-circle-o"></i> <span>Add Pharma Company</span></a>

                     </li> -->

                  <li class="<?php echo ($menuactive == 'pharmacompany_list' || $menuactive == 'add_pharmacompany' || $menuactive == 'view_pharmacompany' || $menuactive == 'edit_pharmacompany') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/pharmacompany_list"><i class="fa fa-dot-circle-o"></i> <span>Pharma Company List</span></a>

                  </li>

               </ul>

            </li>

         <?php

         }

         ?>



         <?php

         if (isset($staff_permission[10]['view_permission']) == 1 || isset($staff_permission[10]['add_permission']) == 1 || isset($staff_permission[10]['edit_permission']) == 1 || isset($staff_permission[10]['delete_permission']) == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'banks' || $menuactive == 'bank_list' || $menuactive == 'favourite_banks' || $menuactive == 'view' || $menuactive == 'view_favourite_bank') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-film" aria-hidden="true"></i> -->

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <i class="fa fa-university" aria-hidden="true"></i>

                  <span>Banks</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <?php /* ?>

                     <li class="<?php echo ($menuactive=='bank_list')?'active':'';?>" >

                        <a href="admin/bank_list"><i class="fa fa-dot-circle-o"></i> <span>Bank List</span></a>

                     </li>

                     <?php */ ?>

                  <li class="<?php echo ($menuactive == 'favourite_banks' || $menuactive == 'view_favourite_bank') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/favourite_banks"><i class="fa fa-dot-circle-o"></i> <span>Favourite Banks</span></a>

                  </li>

                  <?php /* ?>

                     <li class="<?php echo ($menuactive=='unit_list' ||  $menuactive=='set_unit')?'active':'';?>" >

                        <a href="admin/unit_list"><i class="fa fa-dot-circle-o"></i> <span>Units</span></a>

                     </li>

                     <?php */ ?>

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'banks' || $menuactive == 'bank_list' || $menuactive == 'favourite_banks' || $menuactive == 'view' || $menuactive == 'view_favourite_bank') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-film" aria-hidden="true"></i> -->

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <i class="fa fa-university" aria-hidden="true"></i>

                  <span>Banks</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <?php /* ?>

                     <li class="<?php echo ($menuactive=='bank_list')?'active':'';?>" >

                        <a href="admin/bank_list"><i class="fa fa-dot-circle-o"></i> <span>Bank List</span></a>

                     </li>

                     <?php */ ?>

                  <li class="<?php echo ($menuactive == 'favourite_banks' || $menuactive == 'view_favourite_bank') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/favourite_banks"><i class="fa fa-dot-circle-o"></i> <span>Favourite Banks</span></a>

                  </li>

                  <?php /* ?>

                     <li class="<?php echo ($menuactive=='unit_list' ||  $menuactive=='set_unit')?'active':'';?>" >

                        <a href="admin/unit_list"><i class="fa fa-dot-circle-o"></i> <span>Units</span></a>

                     </li>

                     <?php */ ?>

               </ul>

            </li>

         <?php

         }

         ?>



         <!-- <li class="submenu <?php echo ($menuactive == 'Contact' || $menuactive == 'contacts_list') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                 <i class="fa fa-address-book" aria-hidden="true"></i>

               

                 <span>User's contact</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

               

               <li class="<?php echo ($menuactive == 'contacts_list') ? 'active' : ''; ?>" >

                     <a href="<?php echo base_url(); ?>admin/contacts_list"><i class="fa fa-dot-circle-o"></i> <span>Contact List</span></a>

                   </li>

               

               </ul>

               

            </li> -->



         <?php

         if (isset($staff_permission[11]['view_permission']) == 1 || isset($staff_permission[11]['add_permission']) == 1 || isset($staff_permission[11]['edit_permission']) == 1 || isset($staff_permission[11]['delete_permission']) == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'restaurant_list' || $menuactive == 'favorite_restaurant_list' || $menuactive == 'view_restaurant') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-address-book" aria-hidden="true"></i> -->

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <i class="fa fa-cutlery" aria-hidden="true"></i>

                  <span>Restaurants</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <?php /* ?> 

                     <li class="<?php echo ($menuactive=='restaurant_list')?'active':'';?>" >

                        <a href="<?php echo base_url(); ?>admin/restaurant_list"><i class="fa fa-dot-circle-o"></i> <span>Restaurant List</span></a>

                     </li>

                     <?php */ ?>

                  <li class="<?php echo ($menuactive == 'favorite_restaurant_list') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/favorite_restaurant_list"><i class="fa fa-dot-circle-o"></i> <span>Favorite restaurant </span></a>

                  </li>

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'restaurant_list' || $menuactive == 'favorite_restaurant_list' || $menuactive == 'view_restaurant') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-address-book" aria-hidden="true"></i> -->

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <i class="fa fa-cutlery" aria-hidden="true"></i>

                  <span>Restaurants</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <?php /* ?> 

                     <li class="<?php echo ($menuactive=='restaurant_list')?'active':'';?>" >

                        <a href="<?php echo base_url(); ?>admin/restaurant_list"><i class="fa fa-dot-circle-o"></i> <span>Restaurant List</span></a>

                     </li>

                     <?php */ ?>

                  <li class="<?php echo ($menuactive == 'favorite_restaurant_list') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/favorite_restaurant_list"><i class="fa fa-dot-circle-o"></i> <span>Favorite restaurant </span></a>

                  </li>

               </ul>

            </li>

         <?php

         }

         ?>





         <!-- <li class="submenu <?php echo ($menuactive == 'grocery_list' || $menuactive == 'favorite_grocery_list' || $menuactive == 'view_grocery') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                 <i class="fa fa-address-book" aria-hidden="true"></i>

               

                 <span>Grocery</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

               

               <?php  ?><li class="<?php echo ($menuactive == 'grocery_list') ? 'active' : ''; ?>" >

                     <a href="<?php echo base_url(); ?>admin/restaurant_list"><i class="fa fa-dot-circle-o"></i> <span>Restaurant List</span></a>

               </li><?php  ?>

               <li class="<?php echo ($menuactive == 'favorite_grocery_list') ? 'active' : ''; ?>" >

                     <a href="<?php echo base_url(); ?>admin/favorite_grocery_list"><i class="fa fa-dot-circle-o"></i> <span>Favorite grocery </span></a>

               </li>

               

               </ul>

               

            </li> -->



         <?php

         if (isset($staff_permission[12]['view_permission']) == 1 || isset($staff_permission[12]['add_permission']) == 1 || isset($staff_permission[12]['edit_permission']) == 1 || isset($staff_permission[12]['delete_permission']) == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'grocery' || $menuactive == 'grocery_list' || $menuactive == 'favourite_grocery' || $menuactive == 'view_grocery' || $menuactive == 'view_favourite_grocery') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-film" aria-hidden="true"></i> -->

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>

                  <span>Grocerys</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <!--<li class="<?php //echo ($menuactive=='grocery_list' ||$menuactive=='view_grocery')?'active':'';
                                 ?>" >

                        <a href="<?php //echo base_url(); 
                                 ?>admin/grocery_list"><i class="fa fa-dot-circle-o"></i> <span>Grocery List</span></a>

                     </li>-->

                  <li class="<?php echo ($menuactive == 'favourite_grocery' || $menuactive == 'view_favourite_grocery') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/favourite_grocery"><i class="fa fa-dot-circle-o"></i> <span>Favourite Grocery</span></a>

                  </li>

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'grocery' || $menuactive == 'grocery_list' || $menuactive == 'favourite_grocery' || $menuactive == 'view_grocery' || $menuactive == 'view_favourite_grocery') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-film" aria-hidden="true"></i> -->

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>

                  <span>Grocerys</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <!--<li class="<?php //echo ($menuactive=='grocery_list' ||$menuactive=='view_grocery')?'active':'';
                                 ?>" >

                        <a href="<?php //echo base_url(); 
                                 ?>admin/grocery_list"><i class="fa fa-dot-circle-o"></i> <span>Grocery List</span></a>

                     </li>-->

                  <li class="<?php echo ($menuactive == 'favourite_grocery' || $menuactive == 'view_favourite_grocery') ? 'active' : ''; ?>">

                     <a href="<?php echo base_url(); ?>admin/favourite_grocery"><i class="fa fa-dot-circle-o"></i> <span>Favourite Grocery</span></a>

                  </li>

               </ul>

            </li>

         <?php

         }

         ?>



         <!-- <li class="submenu <?php echo ($menuactive == 'note' || $menuactive == 'note_list'  || $menuactive == 'view_note') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                 <i class="fa fa-film" aria-hidden="true"></i>

               

                 <span>Note</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

               

               

                 <li class="<?php echo ($menuactive == 'note_list' || $menuactive == 'view_note') ? 'active' : ''; ?>" >

                     <a href="<?php echo base_url(); ?>admin/note_list"><i class="fa fa-dot-circle-o"></i> <span>Note List</span></a>

                   </li>

               

               

               </ul>

               

            </li> -->



         <?php

         if (isset($staff_permission[13]['view_permission']) == 1 || isset($staff_permission[13]['add_permission']) == 1 || isset($staff_permission[13]['edit_permission']) == 1 || isset($staff_permission[13]['delete_permission']) == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'Articles' || $menuactive == 'add_article' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'articles_list' || $menuactive == 'music_list' || $menuactive == 'view_music' || $menuactive == 'edit_music' || $menuactive == 'movie_list' || $menuactive == 'view_movie' || $menuactive == 'edit_movie' || $menuactive == 'music_banner_list' || $menuactive == 'view_music_banner' || $menuactive == 'movie_category_list' || $menuactive == 'music_category_list' || $menuactive == 'edit_music_banner' || $menuactive == 'movie_banner_list' || $menuactive == 'view_movie_banner' || $menuactive == 'edit_movie_banner' || $menuactive == 'add_movie_banner' || $menuactive == 'articles_list' || $menuactive == 'view_article' || $menuactive == 'edit_article' || $menuactive == 'add_article' || $menuactive == 'article_category_list' || $menuactive == 'add_article_category' || $menuactive == 'edit_article_category' || $menuactive == 'view_article_category' || $menuactive == 'view_music_category' || $menuactive == 'edit_music_category' || $menuactive == 'add_music_banner' || $menuactive == 'add_movie' || $menuactive == 'add_movie_category' || $menuactive == 'add_music' || $menuactive == 'add_music_category' || $menuactive == 'edit_movie_category' || $menuactive == 'view_movie_category') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-book" aria-hidden="true"></i> -->

                  <i class="fa fa-television" aria-hidden="true"></i>

                  <span>Entertainment</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i> Articles List</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'articles_list' || $menuactive == 'view_article' || $menuactive == 'edit_article' || $menuactive == 'add_article') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/articles_list"><i class="fa fa-angle-double-right"></i> <span>Articles List</span></a>

                        </li>

                        <!--  <li class="<?php echo ($menuactive == 'article_category_list' || $menuactive == 'add_article_category' || $menuactive == 'edit_article_category' || $menuactive == 'view_article_category') ? 'active' : ''; ?>" >

                              <a href="<?php echo base_url(); ?>admin/article_category_list"><i class="fa fa-dot-circle-o"></i> <span>Article Category List</span></a>

                           </li> -->

                        <li <?php if ($menuactive == 'article_category_list' || $menuactive == 'add_article_category' || $menuactive == 'edit_article_category' || $menuactive == 'view_article_category') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/article_category_list"><i class="fa fa-angle-double-right"></i> Article Category List</a>

                        </li>

                        <!-- <li <?php if ($menuactive == 'music_banner_list' || $menuactive == 'view_music_banner' || $menuactive == 'edit_music_banner' || $menuactive == 'edit_music_category') {
                                    echo 'class="active"';
                                 } ?>>

                              <a href="<?php echo base_url(); ?>admin/music_banner_list"><i class="fa fa-angle-double-right"></i> Music Banner List</a>

                           </li> -->

                     </ul>

                  </li>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-music" aria-hidden="true"></i> Music</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'music_list' || $menuactive == 'view_music' || $menuactive == 'edit_music' || $menuactive == 'add_music') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/music_list"><i class="fa fa-angle-double-right"></i> <span>Music List</span></a>

                        </li>

                        <li <?php if ($menuactive == 'music_category_list' || $menuactive == 'add_music_category' || $menuactive == 'edit_music_category' || $menuactive == 'view_music_category' || $menuactive == 'edit_music_category') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/music_category_list"><i class="fa fa-angle-double-right"></i> Music Category List</a>

                        </li>

                        <li <?php if ($menuactive == 'music_banner_list' || $menuactive == 'view_music_banner' || $menuactive == 'edit_music_banner' || $menuactive == 'add_music_banner') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/music_banner_list"><i class="fa fa-angle-double-right"></i> Music Banner List</a>

                        </li>

                     </ul>

                  </li>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-film" aria-hidden="true"></i> Movie</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'movie_list' || $menuactive == 'view_movie' || $menuactive == 'edit_movie' || $menuactive == 'add_movie') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/movie_list"><i class="fa fa-angle-double-right"></i> <span>Movie List</span></a>

                        </li>

                        <li <?php if ($menuactive == 'movie_category_list' || $menuactive == 'add_movie_category' || $menuactive == 'edit_movie_category' || $menuactive == 'add_movie_category' || $menuactive == 'view_movie_category') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/movie_category_list"><i class="fa fa-angle-double-right"></i> Movie Category List</a>

                        </li>

                        <li <?php if ($menuactive == 'movie_banner_list' || $menuactive == 'view_movie_banner' || $menuactive == 'edit_movie_banner' || $menuactive == 'add_movie_banner') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/movie_banner_list"><i class="fa fa-angle-double-right"></i> Movie Banner List</a>

                        </li>

                     </ul>

                  </li>

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <li class="submenu <?php echo ($menuactive == 'Articles' || $menuactive == 'add_article' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'articles_list' || $menuactive == 'music_list' || $menuactive == 'view_music' || $menuactive == 'edit_music' || $menuactive == 'movie_list' || $menuactive == 'view_movie' || $menuactive == 'edit_movie' || $menuactive == 'music_banner_list' || $menuactive == 'view_music_banner' || $menuactive == 'movie_category_list' || $menuactive == 'music_category_list' || $menuactive == 'edit_music_banner' || $menuactive == 'movie_banner_list' || $menuactive == 'view_movie_banner' || $menuactive == 'edit_movie_banner' || $menuactive == 'add_movie_banner' || $menuactive == 'articles_list' || $menuactive == 'view_article' || $menuactive == 'edit_article' || $menuactive == 'add_article' || $menuactive == 'article_category_list' || $menuactive == 'add_article_category' || $menuactive == 'edit_article_category' || $menuactive == 'view_article_category' || $menuactive == 'view_music_category' || $menuactive == 'edit_music_category' || $menuactive == 'add_music_banner' || $menuactive == 'add_movie' || $menuactive == 'add_movie_category' || $menuactive == 'add_music' || $menuactive == 'add_music_category' || $menuactive == 'edit_movie_category' || $menuactive == 'view_movie_category') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-book" aria-hidden="true"></i> -->

                  <i class="fa fa-television" aria-hidden="true"></i>

                  <span>Entertainment</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i> Articles List</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'articles_list' || $menuactive == 'view_article' || $menuactive == 'edit_article' || $menuactive == 'add_article') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/articles_list"><i class="fa fa-angle-double-right"></i> <span>Articles List</span></a>

                        </li>

                        <!--  <li class="<?php echo ($menuactive == 'article_category_list' || $menuactive == 'add_article_category' || $menuactive == 'edit_article_category' || $menuactive == 'view_article_category') ? 'active' : ''; ?>" >

                              <a href="<?php echo base_url(); ?>admin/article_category_list"><i class="fa fa-dot-circle-o"></i> <span>Article Category List</span></a>

                           </li> -->

                        <li <?php if ($menuactive == 'article_category_list' || $menuactive == 'add_article_category' || $menuactive == 'edit_article_category' || $menuactive == 'view_article_category') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/article_category_list"><i class="fa fa-angle-double-right"></i> Article Category List</a>

                        </li>

                        <!-- <li <?php if ($menuactive == 'music_banner_list' || $menuactive == 'view_music_banner' || $menuactive == 'edit_music_banner' || $menuactive == 'edit_music_category') {
                                    echo 'class="active"';
                                 } ?>>

                              <a href="<?php echo base_url(); ?>admin/music_banner_list"><i class="fa fa-angle-double-right"></i> Music Banner List</a>

                           </li> -->

                     </ul>

                  </li>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-music" aria-hidden="true"></i> Music</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'music_list' || $menuactive == 'view_music' || $menuactive == 'edit_music' || $menuactive == 'add_music') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/music_list"><i class="fa fa-angle-double-right"></i> <span>Music List</span></a>

                        </li>

                        <li <?php if ($menuactive == 'music_category_list' || $menuactive == 'add_music_category' || $menuactive == 'edit_music_category' || $menuactive == 'view_music_category' || $menuactive == 'edit_music_category') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/music_category_list"><i class="fa fa-angle-double-right"></i> Music Category List</a>

                        </li>

                        <li <?php if ($menuactive == 'music_banner_list' || $menuactive == 'view_music_banner' || $menuactive == 'edit_music_banner' || $menuactive == 'add_music_banner') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/music_banner_list"><i class="fa fa-angle-double-right"></i> Music Banner List</a>

                        </li>

                     </ul>

                  </li>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-film" aria-hidden="true"></i> Movie</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'movie_list' || $menuactive == 'view_movie' || $menuactive == 'edit_movie' || $menuactive == 'add_movie') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/movie_list"><i class="fa fa-angle-double-right"></i> <span>Movie List</span></a>

                        </li>

                        <li <?php if ($menuactive == 'movie_category_list' || $menuactive == 'add_movie_category' || $menuactive == 'edit_movie_category' || $menuactive == 'add_movie_category' || $menuactive == 'view_movie_category') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/movie_category_list"><i class="fa fa-angle-double-right"></i> Movie Category List</a>

                        </li>

                        <li <?php if ($menuactive == 'movie_banner_list' || $menuactive == 'view_movie_banner' || $menuactive == 'edit_movie_banner' || $menuactive == 'add_movie_banner') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/movie_banner_list"><i class="fa fa-angle-double-right"></i> Movie Banner List</a>

                        </li>

                     </ul>

                  </li>

               </ul>

            </li>

         <?php

         }

         ?>



         <?php

         if (isset($staff_permission[14]['view_permission']) == 1 || isset($staff_permission[14]['add_permission']) == 1 || isset($staff_permission[14]['edit_permission']) == 1 || isset($staff_permission[14]['delete_permission']) == 1) {

         ?>

            <!-- Sprituality module start -->

            <li class="submenu <?php echo ($menuactive == 'Articles' || $menuactive == 'add_meditation_article' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'meditation_articles_list' || $menuactive == 'music_list' || $menuactive == 'view_music' || $menuactive == 'edit_music' || $menuactive == 'movie_list' || $menuactive == 'view_movie' || $menuactive == 'edit_movie' || $menuactive == 'music_banner_list' || $menuactive == 'view_music_banner' || $menuactive == 'meditation_video_category_list' || $menuactive == 'music_category_list' || $menuactive == 'edit_music_banner' || $menuactive == 'movie_banner_list' || $menuactive == 'view_movie_banner' || $menuactive == 'edit_movie_banner' || $menuactive == 'add_movie_banner' || $menuactive == 'articles_list' || $menuactive == 'view_article' || $menuactive == 'edit_article' || $menuactive == 'add_article' || $menuactive == 'article_category_list' || $menuactive == 'add_article_category' || $menuactive == 'edit_article_category' || $menuactive == 'view_article_category' || $menuactive == 'view_music_category' || $menuactive == 'edit_music_category' || $menuactive == 'add_music_banner' || $menuactive == 'add_movie' || $menuactive == 'add_movie_category' || $menuactive == 'add_music' || $menuactive == 'add_music_category' || $menuactive == 'edit_movie_category' || $menuactive == 'view_movie_category' || $menuactive == 'meditation_article_category_list' || $menuactive == 'add_meditation_article_category' || $menuactive == 'edit_meditation_article_category' || $menuactive == 'view_meditation_article_category' || $menuactive == 'edit_meditation_article' || $menuactive == 'add_meditation_video_category' || $menuactive == 'edit_meditation_vedio_category' || $menuactive == 'view_meditation_vedio_category' || $menuactive == 'meditation_video_list' || $menuactive == 'add_meditation_video' || $menuactive == 'edit_meditation_video' || $menuactive == 'view_meditation_video' || $menuactive == 'meditation_video_banner_list' || $menuactive == 'add_meditation_video_banner' || $menuactive == 'edit_meditation_video_banner' || $menuactive == 'delete_meditation_video_banner' || $menuactive == 'view_meditation_video_banner' || $menuactive == 'view_meditation_article') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-book" aria-hidden="true"></i> -->

                  <i class="fa fa-empire" aria-hidden="true"></i>

                  <!-- <i class="fa fa-television" aria-hidden="true"></i> -->

                  <span>Spirituality</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i>Meditation articles </span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'meditation_articles_list' || $menuactive == 'view_article' || $menuactive == 'edit_meditation_article' || $menuactive == 'add_meditation_article' || $menuactive == 'view_meditation_article') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/meditation_articles_list"><i class="fa fa-angle-double-right"></i> <span>Meditation articles List</span></a>

                        </li>

                        <!--  <li class="<?php echo ($menuactive == 'article_category_list' || $menuactive == 'add_article_category' || $menuactive == 'edit_article_category' || $menuactive == 'view_article_category') ? 'active' : ''; ?>" >

                              <a href="<?php echo base_url(); ?>admin/article_category_list"><i class="fa fa-dot-circle-o"></i> <span>Article Category List</span></a>

                           </li> -->

                        <li <?php if ($menuactive == 'meditation_article_category_list' || $menuactive == 'add_meditation_article_category' || $menuactive == 'edit_meditation_article_category' || $menuactive == 'view_meditation_article_category') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/meditation_article_category_list"><i class="fa fa-angle-double-right"></i>Meditaion article category List</a>

                        </li>

                        <!-- <li <?php if ($menuactive == 'music_banner_list' || $menuactive == 'view_music_banner' || $menuactive == 'edit_music_banner' || $menuactive == 'edit_music_category') {
                                    echo 'class="active"';
                                 } ?>>

                              <a href="<?php echo base_url(); ?>admin/music_banner_list"><i class="fa fa-angle-double-right"></i> Music Banner List</a>

                           </li> -->

                     </ul>

                  </li>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-film" aria-hidden="true"></i> Meditaion Videos</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'meditation_video_list' || $menuactive == 'view_movie' || $menuactive == 'edit_movie' || $menuactive == 'add_meditation_video' || $menuactive == 'edit_meditation_video' || $menuactive == 'view_meditation_video') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/meditation_video_list"><i class="fa fa-angle-double-right"></i> <span>Meditaion Video List</span></a>

                        </li>

                        <li <?php if ($menuactive == 'meditation_video_category_list' || $menuactive == 'add_meditation_video_category' || $menuactive == 'edit_meditation_vedio_category' || $menuactive == 'add_movie_category' || $menuactive == 'view_movie_category' || $menuactive == 'view_meditation_vedio_category') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/meditation_video_category_list"><i class="fa fa-angle-double-right"></i>Meditaion Video Category List</a>

                        </li>

                        <li <?php if ($menuactive == 'meditation_video_banner_list' || $menuactive == 'add_meditation_video_banner' || $menuactive == 'edit_meditation_video_banner' || $menuactive == 'delete_meditation_video_banner' || $menuactive == 'view_meditation_video_banner') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/meditation_video_banner_list"><i class="fa fa-angle-double-right"></i>Meditaion Video Banner List</a>

                        </li>

                     </ul>

                  </li>

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <!-- Sprituality module start -->

            <li class="submenu <?php echo ($menuactive == 'Articles' || $menuactive == 'add_meditation_article' || $menuactive == 'edit' || $menuactive == 'view' || $menuactive == 'meditation_articles_list' || $menuactive == 'music_list' || $menuactive == 'view_music' || $menuactive == 'edit_music' || $menuactive == 'movie_list' || $menuactive == 'view_movie' || $menuactive == 'edit_movie' || $menuactive == 'music_banner_list' || $menuactive == 'view_music_banner' || $menuactive == 'meditation_video_category_list' || $menuactive == 'music_category_list' || $menuactive == 'edit_music_banner' || $menuactive == 'movie_banner_list' || $menuactive == 'view_movie_banner' || $menuactive == 'edit_movie_banner' || $menuactive == 'add_movie_banner' || $menuactive == 'articles_list' || $menuactive == 'view_article' || $menuactive == 'edit_article' || $menuactive == 'add_article' || $menuactive == 'article_category_list' || $menuactive == 'add_article_category' || $menuactive == 'edit_article_category' || $menuactive == 'view_article_category' || $menuactive == 'view_music_category' || $menuactive == 'edit_music_category' || $menuactive == 'add_music_banner' || $menuactive == 'add_movie' || $menuactive == 'add_movie_category' || $menuactive == 'add_music' || $menuactive == 'add_music_category' || $menuactive == 'edit_movie_category' || $menuactive == 'view_movie_category' || $menuactive == 'meditation_article_category_list' || $menuactive == 'add_meditation_article_category' || $menuactive == 'edit_meditation_article_category' || $menuactive == 'view_meditation_article_category' || $menuactive == 'edit_meditation_article' || $menuactive == 'add_meditation_video_category' || $menuactive == 'edit_meditation_vedio_category' || $menuactive == 'view_meditation_vedio_category' || $menuactive == 'meditation_video_list' || $menuactive == 'add_meditation_video' || $menuactive == 'edit_meditation_video' || $menuactive == 'view_meditation_video' || $menuactive == 'meditation_video_banner_list' || $menuactive == 'add_meditation_video_banner' || $menuactive == 'edit_meditation_video_banner' || $menuactive == 'delete_meditation_video_banner' || $menuactive == 'view_meditation_video_banner' || $menuactive == 'view_meditation_article') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-book" aria-hidden="true"></i> -->

                  <i class="fa fa-empire" aria-hidden="true"></i>

                  <!-- <i class="fa fa-television" aria-hidden="true"></i> -->

                  <span>Spirituality</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-dot-circle-o"></i>Meditation articles </span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'meditation_articles_list' || $menuactive == 'view_article' || $menuactive == 'edit_meditation_article' || $menuactive == 'add_meditation_article' || $menuactive == 'view_meditation_article') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/meditation_articles_list"><i class="fa fa-angle-double-right"></i> <span>Meditation articles List</span></a>

                        </li>

                        <!--  <li class="<?php echo ($menuactive == 'article_category_list' || $menuactive == 'add_article_category' || $menuactive == 'edit_article_category' || $menuactive == 'view_article_category') ? 'active' : ''; ?>" >

                              <a href="<?php echo base_url(); ?>admin/article_category_list"><i class="fa fa-dot-circle-o"></i> <span>Article Category List</span></a>

                           </li> -->

                        <li <?php if ($menuactive == 'meditation_article_category_list' || $menuactive == 'add_meditation_article_category' || $menuactive == 'edit_meditation_article_category' || $menuactive == 'view_meditation_article_category') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/meditation_article_category_list"><i class="fa fa-angle-double-right"></i>Meditaion article category List</a>

                        </li>

                        <!-- <li <?php if ($menuactive == 'music_banner_list' || $menuactive == 'view_music_banner' || $menuactive == 'edit_music_banner' || $menuactive == 'edit_music_category') {
                                    echo 'class="active"';
                                 } ?>>

                              <a href="<?php echo base_url(); ?>admin/music_banner_list"><i class="fa fa-angle-double-right"></i> Music Banner List</a>

                           </li> -->

                     </ul>

                  </li>

                  <li>

                     <span class="submenu-cls"><i class="fa fa-film" aria-hidden="true"></i> Meditaion Videos</span>

                     <ul class="frd_swtChild">

                        <li <?php if ($menuactive == 'meditation_video_list' || $menuactive == 'view_movie' || $menuactive == 'edit_movie' || $menuactive == 'add_meditation_video' || $menuactive == 'edit_meditation_video' || $menuactive == 'view_meditation_video') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/meditation_video_list"><i class="fa fa-angle-double-right"></i> <span>Meditaion Video List</span></a>

                        </li>

                        <li <?php if ($menuactive == 'meditation_video_category_list' || $menuactive == 'add_meditation_video_category' || $menuactive == 'edit_meditation_vedio_category' || $menuactive == 'add_movie_category' || $menuactive == 'view_movie_category' || $menuactive == 'view_meditation_vedio_category') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/meditation_video_category_list"><i class="fa fa-angle-double-right"></i>Meditaion Video Category List</a>

                        </li>

                        <li <?php if ($menuactive == 'meditation_video_banner_list' || $menuactive == 'add_meditation_video_banner' || $menuactive == 'edit_meditation_video_banner' || $menuactive == 'delete_meditation_video_banner' || $menuactive == 'view_meditation_video_banner') {
                                 echo 'class="active"';
                              } ?>>

                           <a href="<?php echo base_url(); ?>admin/meditation_video_banner_list"><i class="fa fa-angle-double-right"></i>Meditaion Video Banner List</a>

                        </li>

                     </ul>

                  </li>

               </ul>

            </li>

         <?php

         }

         ?>



         <?php

         if (isset($staff_permission[15]['view_permission']) == 1 || isset($staff_permission[15]['add_permission']) == 1 || isset($staff_permission[15]['edit_permission']) == 1 || isset($staff_permission[15]['delete_permission']) == 1) {

         ?>

            <!-- End -->

            <li class="submenu <?php echo ($menuactive == 'unit_list' ||  $menuactive == 'set_unit') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-film" aria-hidden="true"></i> -->

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <!-- <i class="fa fa-university" aria-hidden="true"></i> -->

                  <i class="fa fa-pie-chart" aria-hidden="true"></i>

                  <span>Distance Units</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <?php /* ?>

                     <li class="<?php echo ($menuactive=='bank_list')?'active':'';?>" >

                        <a href="admin/bank_list"><i class="fa fa-dot-circle-o"></i> <span>Bank List</span></a>

                     </li>

                     <?php */ ?>

                  <?php /* ?>

                     <li class="<?php echo ($menuactive=='favourite_banks' || $menuactive=='view_favourite_bank')?'active':'';?>" >

                        <a href="<?php echo base_url(); ?>admin/favourite_banks"><i class="fa fa-dot-circle-o"></i> <span>Favourite Banks</span></a>

                     </li>

                     <?php */ ?>

                  <li class="<?php echo ($menuactive == 'unit_list' ||  $menuactive == 'set_unit') ? 'active' : ''; ?>">

                     <a href="admin/unit_list"><i class="fa fa-dot-circle-o"></i> <span>Distance Unit List</span></a>

                  </li>

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <!-- End -->

            <li class="submenu <?php echo ($menuactive == 'unit_list' ||  $menuactive == 'set_unit') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-film" aria-hidden="true"></i> -->

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <!-- <i class="fa fa-university" aria-hidden="true"></i> -->

                  <i class="fa fa-pie-chart" aria-hidden="true"></i>

                  <span>Distance Units</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <?php /* ?>

                     <li class="<?php echo ($menuactive=='bank_list')?'active':'';?>" >

                        <a href="admin/bank_list"><i class="fa fa-dot-circle-o"></i> <span>Bank List</span></a>

                     </li>

                     <?php */ ?>

                  <?php /* ?>

                     <li class="<?php echo ($menuactive=='favourite_banks' || $menuactive=='view_favourite_bank')?'active':'';?>" >

                        <a href="<?php echo base_url(); ?>admin/favourite_banks"><i class="fa fa-dot-circle-o"></i> <span>Favourite Banks</span></a>

                     </li>

                     <?php */ ?>

                  <li class="<?php echo ($menuactive == 'unit_list' ||  $menuactive == 'set_unit') ? 'active' : ''; ?>">

                     <a href="admin/unit_list"><i class="fa fa-dot-circle-o"></i> <span>Distance Unit List</span></a>

                  </li>

               </ul>

            </li>

         <?php

         }

         ?>





         <!-- Ticket section -->

         <?php

         if (isset($staff_permission[15]['view_permission']) == 1 || isset($staff_permission[15]['add_permission']) == 1 || isset($staff_permission[15]['edit_permission']) == 1 || isset($staff_permission[15]['delete_permission']) == 1) {

         ?>

            <!-- End -->

            <li class="submenu <?php echo ($menuactive == 'ticket_list' || $menuactive == 'view_detail' || $menuactive == 'reply_to_ticket' || $menuactive == 'view_ticket_conversation') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-film" aria-hidden="true"></i> -->

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <!-- <i class="fa fa-university" aria-hidden="true"></i> -->

                  <i class="fa fa-ticket"></i>

                  <span>Genrated Tickets</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <?php /* ?>

                     <li class="<?php echo ($menuactive=='bank_list')?'active':'';?>" >

                        <a href="admin/bank_list"><i class="fa fa-dot-circle-o"></i> <span>Bank List</span></a>

                     </li>

                     <?php */ ?>

                  <?php /* ?>

                     <li class="<?php echo ($menuactive=='favourite_banks' || $menuactive=='view_favourite_bank')?'active':'';?>" >

                        <a href="<?php echo base_url(); ?>admin/favourite_banks"><i class="fa fa-dot-circle-o"></i> <span>Favourite Banks</span></a>

                     </li>

                     <?php */ ?>

                  <li class="<?php echo ($menuactive == 'ticket_list' || $menuactive == 'view_detail' || $menuactive == 'reply_to_ticket' || $menuactive == 'view_ticket_conversation') ? 'active' : ''; ?>">

                     <a href="admin/ticket_list"><i class="fa fa-dot-circle-o"></i> <span>Tickets List</span></a>

                  </li>

               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <!-- End -->

            <li class="submenu <?php echo ($menuactive == 'ticket_list' || $menuactive == 'view_detail' || $menuactive == 'reply_to_ticket'  || $menuactive == 'view_ticket_conversation') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-film" aria-hidden="true"></i> -->

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <!-- <i class="fa fa-university" aria-hidden="true"></i> -->

                  <i class="fa fa-ticket"></i>

                  <span>Genrated Tickets</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <?php /* ?>

                     <li class="<?php echo ($menuactive=='bank_list')?'active':'';?>" >

                        <a href="admin/bank_list"><i class="fa fa-dot-circle-o"></i> <span>Bank List</span></a>

                     </li>

                     <?php */ ?>

                  <?php /* ?>

                     <li class="<?php echo ($menuactive=='favourite_banks' || $menuactive=='view_favourite_bank')?'active':'';?>" >

                        <a href="<?php echo base_url(); ?>admin/favourite_banks"><i class="fa fa-dot-circle-o"></i> <span>Favourite Banks</span></a>

                     </li>

                     <?php */ ?>

                  <li class="<?php echo ($menuactive == 'ticket_list' || $menuactive == 'view_detail' || $menuactive == 'reply_to_ticket' || $menuactive == 'view_ticket_conversation') ? 'active' : ''; ?>">

                     <a href="admin/ticket_list"><i class="fa fa-dot-circle-o"></i> <span>Tickets List</span></a>

                  </li>

               </ul>

            </li>

         <?php

         }

         ?>

         <!-- End -->



         <?php /*  ?>

            <!-- Ticket section -->

            <?php

            if ($staff_permission[15]['view_permission'] == 1 || $staff_permission[15]['add_permission'] == 1 || $staff_permission[15]['edit_permission'] == 1 || $staff_permission[15]['delete_permission'] == 1) {

               ?>

               <!-- End -->

               <li class="submenu <?php echo ($menuactive=='feedback_list')?'open':'';?>">

                  <a href="javascript:void(0);">

                     <!-- <i class="fa fa-film" aria-hidden="true"></i> -->

                     <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                     <!-- <i class="fa fa-university" aria-hidden="true"></i> -->

                     <i class="fa fa-ticket"></i>

                     <span>Feedback</span> <span class="label label-important"><b class="caret"></b></span>

                  </a>

                  <ul>

                     

                     <li class="<?php echo ($menuactive=='bank_list')?'active':'';?>" >

                        <a href="admin/bank_list"><i class="fa fa-dot-circle-o"></i> <span>Bank List</span></a>

                     </li>

                     

                     <li class="<?php echo ($menuactive=='favourite_banks' || $menuactive=='view_favourite_bank')?'active':'';?>" >

                        <a href="<?php echo base_url(); ?>admin/favourite_banks"><i class="fa fa-dot-circle-o"></i> <span>Favourite Banks</span></a>

                     </li>

                     

                     <li class="<?php echo ($menuactive=='feedback_list')?'active':'';?>" >

                        <a href="admin/feedback_list"><i class="fa fa-dot-circle-o"></i> <span>User Feedback List</span></a>

                     </li>

                  </ul>

               </li>

               <?php          

            } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

               ?>

               <!-- End -->

               <li class="submenu <?php echo ($menuactive=='feedback_list')?'open':'';?>">

                  <a href="javascript:void(0);">

                     <!-- <i class="fa fa-film" aria-hidden="true"></i> -->

                     <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                     <!-- <i class="fa fa-university" aria-hidden="true"></i> -->

                     <i class="fa fa-ticket"></i>

                     <span>Feedback</span> <span class="label label-important"><b class="caret"></b></span>

                  </a>

                  <ul>

                     

                     <li class="<?php echo ($menuactive=='bank_list')?'active':'';?>" >

                        <a href="admin/bank_list"><i class="fa fa-dot-circle-o"></i> <span>Bank List</span></a>

                     </li>

                     

                     <li class="<?php echo ($menuactive=='favourite_banks' || $menuactive=='view_favourite_bank')?'active':'';?>" >

                        <a href="<?php echo base_url(); ?>admin/favourite_banks"><i class="fa fa-dot-circle-o"></i> <span>Favourite Banks</span></a>

                     </li>

                     

                     <li class="<?php echo ($menuactive=='feedback_list')?'active':'';?>" >

                        <a href="admin/feedback_list"><i class="fa fa-dot-circle-o"></i> <span>User Feedback List</span></a>

                     </li>

                  </ul>

               </li>

               <?php

            }

            ?>

            <!-- End -->



            <?php */  ?>





         <!-- Ticket section -->

         <?php

         if (isset($staff_permission[15]['view_permission']) == 1 || isset($staff_permission[15]['add_permission']) == 1 || isset($staff_permission[15]['edit_permission']) == 1 || isset($staff_permission[15]['delete_permission']) == 1) {

         ?>

            <!-- End -->

            <li class="submenu <?php echo ($menuactive == 'set-date' || $menuactive == 'list-date' || $menuactive == 'feedback_list') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-film" aria-hidden="true"></i> -->

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <!-- <i class="fa fa-university" aria-hidden="true"></i> -->

                  <i class="fa fa-edit"></i>

                  <span>Survey Management</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <?php /* ?>

                     <li class="<?php echo ($menuactive=='bank_list')?'active':'';?>" >

                        <a href="admin/bank_list"><i class="fa fa-dot-circle-o"></i> <span>Bank List</span></a>

                     </li>

                     <?php */ ?>

                  <?php /* ?>

                     <li class="<?php echo ($menuactive=='favourite_banks' || $menuactive=='view_favourite_bank')?'active':'';?>" >

                        <a href="<?php echo base_url(); ?>admin/favourite_banks"><i class="fa fa-dot-circle-o"></i> <span>Favourite Banks</span></a>

                     </li>

                     <?php */ ?>

                  <li class="<?php echo ($menuactive == 'list-date') ? 'active' : ''; ?>">

                     <a href="admin/list-date"><i class="fa fa-dot-circle-o"></i> <span>Survey settings</span></a>

                  </li>



                  <li class="<?php echo ($menuactive == 'feedback_list') ? 'active' : ''; ?>">

                     <a href="admin/feedback_list"><i class="fa fa-dot-circle-o"></i> <span>User Feedback List</span></a>

                  </li>



               </ul>

            </li>

         <?php

         } elseif ($this->session->userdata('logged_ins')['user_role'] == 1) {

         ?>

            <!-- End -->

            <li class="submenu <?php echo ($menuactive == 'set-date' || $menuactive == 'list-date' || $menuactive == 'feedback_list') ? 'open' : ''; ?>">

               <a href="javascript:void(0);">

                  <!-- <i class="fa fa-film" aria-hidden="true"></i> -->

                  <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

                  <!-- <i class="fa fa-university" aria-hidden="true"></i> -->

                  <i class="fa fa-edit"></i>

                  <span>Survey Management</span> <span class="label label-important"><b class="caret"></b></span>

               </a>

               <ul>

                  <?php /* ?>

                     <li class="<?php echo ($menuactive=='bank_list')?'active':'';?>" >

                        <a href="admin/bank_list"><i class="fa fa-dot-circle-o"></i> <span>Bank List</span></a>

                     </li>

                     <?php */ ?>

                  <?php /* ?>

                     <li class="<?php echo ($menuactive=='favourite_banks' || $menuactive=='view_favourite_bank')?'active':'';?>" >

                        <a href="<?php echo base_url(); ?>admin/favourite_banks"><i class="fa fa-dot-circle-o"></i> <span>Favourite Banks</span></a>

                     </li>

                     <?php */ ?>

                  <li class="<?php echo ($menuactive == 'set-date' || $menuactive == 'list-date') ? 'active' : ''; ?>">

                     <a href="admin/list-date"><i class="fa fa-dot-circle-o"></i> <span>Survey settings</span></a>

                  </li>



                  <li class="<?php echo ($menuactive == 'feedback_list') ? 'active' : ''; ?>">

                     <a href="admin/feedback_list"><i class="fa fa-dot-circle-o"></i> <span>User Feedback List</span></a>

                  </li>

               </ul>

            </li>

         <?php

         }

         ?>

         <!-- End -->





      </ul>

   </div>

   <!-- Breadcrumb Start -->

   <div id="content">

      <div id="content-header">

         <div id="breadcrumb">

            <a href="<?php echo base_url() . 'admin/dashboard'; ?>" title="Go to Home" class=""><i class="icon-home"></i> Home </a>

            <!-- User Management -->

            <?php
				// if($menuactive=='users' || $menuactive=='add' || $menuactive=='edit' || $menuactive=='view' || $menuactive=='family_member' || $menuactive=='addfamily_member' || $menuactive_add=='editfamily' || $menuactive=='viewfamily_member'  || $menuactive=='caregiver' || $menuactive=='addcaregiver' || $menuactive=='editcare' || $menuactive=='viewcaregiver' ||$menuactive=='contacts_list' ||$menuactive=='note_list' ||$menuactive=='view_note' || $menuactive_add == 'user' || $menuactive_add == 'family' || $menuactive_add == 'caregiver' ||$menuactive=='userphoto' ||$menuactive=='view_photo'||$menuactive=='email_list' || $menuactives=='email' || $menuactives == 'user' || $menuactive == 'view_person' || $menuactive_add=='add'){ 
            ?>

            <?php if ($menuactive == 'users') { ?>

               <?php echo "<span class='brd'>Person Listing</span>"; ?>

            <?php } elseif ($this->uri->segment(2) == 'add') { ?>

               <?php if ($this->uri->segment(3) == 'family') { ?>

                  <a href="admin/family_member/list"><span>Family Member List</span></a>

               <?php } elseif ($this->uri->segment(3) == 'caregiver') { ?>

                  <a href="admin/caregiver/list"><span>Care Giver List</span></a>

               <?php } else { ?>

                  <a href="admin/users/list"><span>Person Listing</span></a>

               <?php } ?>

               <?php echo "<span class='brd'> Add user </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == 'edit') { ?>

               <a href="admin/users/list"><span>Person Listing</span></a>

               <?php echo "<span class='brd'>Edit user</span>"; ?>

            <?php } elseif ($this->uri->segment(2) == 'view_person') { ?>

               <a href="admin/users/list"><span>Person Listing</span></a>

               <?php echo "<span class='brd'> View user</span>"; ?>

            <?php } elseif ($menuactive == 'family_member') { ?>

               <?php echo "<span class='brd'> Family Member List</span>"; ?>

            <?php } elseif ($this->uri->segment(2) == "editfamily") { ?>

               <a href="admin/family_member/list"><span>Family Member List</span></a>

               <?php echo "<span class='brd'> Edit user</span>"; ?>

            <?php } elseif ($this->uri->segment(2) == "viewfamily_member") { ?>

               <a href="admin/family_member/list"><span>Family Member List</span></a>

               <?php echo "<span class='brd'> View user</span>"; ?>

            <?php } elseif ($this->uri->segment(2) == "editcare") { ?>

               <a href="admin/caregiver/list"><span>Care Giver List</span></a>

               <?php echo "<span class='brd'> Edit user</span>"; ?>

            <?php } elseif ($this->uri->segment(2) == "viewcaregiver") { ?>

               <a href="admin/caregiver/list"><span>Care Giver List</span></a>

               <?php echo "<span class='brd'> View user</span>"; ?>

            <?php } elseif ($menuactive == 'caregiver') { ?>

               <?php echo "<span class='brd'> Care Giver List</span>"; ?>

            <?php } elseif ($menuactive == 'contacts_list') { ?>

               <?php echo "<span class='brd'> Contact List</span>"; ?>

            <?php } elseif ($menuactive == 'note_list') { ?>

               <?php echo "<span class='brd'> Note List </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == "view_note") { ?>

               <a href="<?php echo base_url(); ?>admin/note_list"><span>Note List</span></a>

               <?php echo "<span class='brd'> View Note</span>"; ?>

            <?php } elseif ($menuactive == 'userphoto') { ?>

               <?php echo "<span class='brd'> Photo Gallery</span>"; ?>

            <?php } elseif ($this->uri->segment(2) == "view_photo") { ?>

               <a href="<?php echo base_url(); ?>admin/userphoto/list"><span>Photo Gallery </span></a>

               <?php echo "<span class='brd'> View Photo</span>"; ?>

            <?php } elseif ($menuactive == 'email_list') { ?>

               <?php echo "<span class='brd'> Emails </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == "view") { ?>

               <a href="<?php echo base_url(); ?>admin/email_list/list"><span>Emails</span></a>

               <?php echo "<span class='brd'> View email</span>"; ?>



               <!-- Doctor Management -->

               <?php /*}elseif($menuactive=='doctor_list' || $menuactive=='doctor_shedules' ||$menuactive=='medicine_shedules' ||$menuactive=='doctor_appointmensts' ||$menuactive=='view_doctor' ||$menuactive=='edit_doctor' ||$menuactive=='view_shedule_times' || $menuactive=='edit_shedule_time' || $menuactive=='view_appointment' || $menuactive=='view_medicine_shedule' || $menuactive== 'add_shedule' || $menuactive== 'add_doctor' || $menuactive=='list'||$menuactive== 'Add' || $this->uri->segment(3)== 'edit' || $this->uri->segment(3)== 'viewdoctor_category'){*/ ?>

            <?php } elseif ($menuactive == 'doctor_list') {  ?>

               <?php echo "<span class='brd'> Doctor List</span>"; ?>

            <?php } elseif ($this->uri->segment(2) == "add_doctor") { ?>

               <a href="admin/doctor_list"><span>Doctor List</span></a>

               <?php echo "<span class='brd'> Add doctor </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == "edit_doctor") { ?>

               <a href="admin/doctor_list"><span>Doctor List</span></a>

               <?php echo "<span class='brd'> Edit doctor </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == "view_doctor") { ?>

               <a href="admin/doctor_list"><span>Doctor List</span></a>

               <?php echo "<span class='brd'> View doctor </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == "add_shedule") { ?>

               <a href="admin/doctor_list"><span>Doctor List</span></a>

               <?php echo "<span class='brd'> Add doctor schedule </span>"; ?>

            <?php } elseif ($this->uri->segment(3) == "list") { ?>

                  <a href="admin/users/list"><span>Person Listing</span></a>


            <?php } elseif ($this->uri->segment(3) == "Add") { ?>

               <a href="admin/doctor-category/list"><span>Doctor Specialties List</span></a>

               <?php echo "<span class='brd'> Add doctor specialty</span>"; ?>

            <?php } elseif ($this->uri->segment(3) == "edit") { ?>

               <a href="admin/doctor-category/list"><span>Doctor Specialties List</span></a>

               <?php echo "<span class='brd'> Edit doctor specialty</span>"; ?>

            <?php } elseif ($this->uri->segment(3) == "viewdoctor_category") { ?>

               <a href="admin/doctor-category/list"><span>Doctor Specialties List</span></a>

               <?php echo "<span class='brd'> View doctor specialty</span>"; ?>

            <?php } elseif ($menuactive == 'list') { ?>

               <?php echo " <span class='brd'> Doctor Specialties List </span>"; ?>

            <?php } elseif ($menuactive == 'doctor_schedules') { ?>

               <?php echo "<span class='brd'> Doctor schedule list </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == 'view_schedule_times') { ?>

               <a href="admin/doctor_schedules"><span>Doctor Schedule List</span></a>

               <?php echo "<span class='brd'> View doctor schedule </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == 'edit_schedule_time') { ?>

               <a href="admin/doctor_schedules"><span>Doctor Schedule List</span></a>

               <?php echo "<span class='brd'>Edit schedule time</span>"; ?>

            <?php } elseif ($menuactive == 'doctor_appointmensts') { ?>

               <?php echo "<span class='brd'> Doctor Appointments </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == 'view_appointment') { ?>

               <a href="admin/doctor_appointmensts"><span>Doctor Appointments</span></a>

               <?php echo "<span class='brd'> View Appointment </span>"; ?>

            <?php } elseif ($menuactive == 'medicine_schedules') { ?>

               <?php echo "<span class='brd'> Medicine Schedule List </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == 'view_medicine_schedule') { ?>

               <a href="admin/medicine_schedules"><span>Medicine Schedule List</span></a>

               <?php echo "<span class='brd'> View medicine schedule </span>"; ?>

               <?php //} 
               ?>

               <!-- Test Type -->

               <?php /*}elseif($menuactive=='Test_type' || $menuactive=='add_test_type' || $menuactive=='edit' || $menuactive=='view' || $menuactive=='test_type_list' || $menuactive=='edit_test_type'){*/ ?>

            <?php } elseif ($menuactive == 'test_type_list') { ?>

               <?php echo "<span class='brd'>Test Type List </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == 'add_test_type') { ?>

               <a href="<?php echo base_url(); ?>admin/test_type_list"><span>Test Type List</span></a>

               <?php echo "<span class='brd'> Add test type </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == 'edit_test_type') { ?>

               <a href="<?php echo base_url(); ?>admin/test_type_list"><span>Test Type List</span></a>

               <?php echo "<span class='brd'> Edit test type</span>"; ?>

               <?php //} 
               ?>

               <!-- Vital sign test type -->

            <?php } elseif ($menuactive == 'vital_sign_list') { ?>

               <?php echo "<span class='brd'>vital sign test type list </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == 'add_vital_sign') { ?>

               <a href="<?php echo base_url(); ?>admin/vital_sign_list"><span>Vital test type list</span></a>

               <?php echo "<span class='brd'> Add vital test type </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == 'edit_vital_sign') { ?>

               <a href="<?php echo base_url(); ?>admin/vital_sign_list"><span>Vital test type list</span></a>

               <?php echo "<span class='brd'> Edit vital test type</span>"; ?>

               <?php //} 
               ?>

               <!-- Vital sign test type end-->

               <!-- Vital sign detail -->

            <?php } elseif ($menuactive == 'vital_sign') { ?>

               <?php echo "<span class='brd'>vital sign list </span>"; ?>

            <?php } elseif ($menuactive == 'view_vital_sign') { ?>

               <a href="<?php echo base_url(); ?>admin/vital_sign"><span>Vital sign list</span></a>

               <?php echo "<span class='brd'>View vital sign</span>"; ?>

            <?php } elseif ($this->uri->segment(2) == 'view_vital_sign') { ?>

               <a href="<?php echo base_url(); ?>admin/vital_sign"><span>Vital sign list</span></a>

               <?php echo "<span class='brd'> View vital sign</span>"; ?>

               <?php //} 
               ?>

               <!-- <a href="<?php echo base_url(); ?>admin/vital_sign_list"><span>Vital test type list</span></a>

                  <?php //echo "<span class='brd'> Edit vital test type</span>"; 
                  ?> -->

               <?php //} 
               ?>

               <!-- Vital sign detail End-->

               <!-- Acknowledge -->

               <?php /*}elseif($menuactive=='acknowledge_list' || $menuactive=='view_acknowledge'){*/ ?>

            <?php } elseif ($menuactive == 'acknowledge_list') { ?>

               <?php echo "<span class='brd'> Acknowledge List </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == 'view_acknowledge') { ?>

               <a href="admin/acknowledge_list"><span>Acknowledge</span></a>

               <?php echo "<span class='brd'> View acknowledge </span>"; ?>

               <?php //} 
               ?>

               <!-- Pharma Company -->

               <?php /*}elseif($menuactive=='Pharma_company' || $menuactive=='add_pharmacompany' || $menuactive=='edit' || $menuactive=='pharmacompany_list' || $menuactive=='view_pharmacompany' || $menuactive=='edit_pharmacompany'){*/ ?>

            <?php } elseif ($menuactive == 'subscriptions') { ?>

               <?php echo "<span class='brd'> subscriptions List </span>"; ?>

            <?php } elseif ($menuactive == 'plan_list') { ?>

               <a href="<?php echo base_url(); ?>admin/plan_list"><span>subscriptions plan list</span></a>



            <?php } elseif ($menuactive == 'view_ticket_conversation') { ?>

               <a href="<?php echo base_url(); ?>admin/ticket_list"><span>Ticket list</span></a>

               <?php echo "<span class='brd'>view ticket conversation </span>"; ?>

            <?php } elseif ($menuactive == 'reply_to_ticket') { ?>

               <a href="<?php echo base_url(); ?>admin/ticket_list"><span>Ticket list</span></a>

               <?php echo "<span class='brd'>Reply ticket </span>"; ?>

            <?php } elseif ($menuactive == 'add_plan') { ?>

               <a href="<?php echo base_url(); ?>admin/plan_list"><span>subscriptions plan list</span></a>

               <?php echo "<span class='brd'>Add subscription plan </span>"; ?>

            <?php } elseif ($menuactive == 'edit_plan') { ?>

               <a href="<?php echo base_url(); ?>admin/plan_list"><span>subscriptions plan list</span></a>

               <?php echo "<span class='brd'>Edit subscription plan </span>"; ?>

            <?php } elseif ($menuactive == 'pharmacompany_list') { ?>

               <?php echo "<span class='brd'> Pharma Company List</span>"; ?>

            <?php } elseif ($this->uri->segment(2) == 'add_pharmacompany') { ?>

               <a href="<?php echo base_url(); ?>admin/pharmacompany_list"><span>Pharma Company List</span></a>

               <?php echo "<span class='brd'> Add pharma company </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == 'edit_pharmacompany') { ?>

               <a href="<?php echo base_url(); ?>admin/pharmacompany_list"><span>Pharma Company List</span></a>

               <?php echo "<span class='brd'> Edit pharma company </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == 'view_pharmacompany') { ?>

               <a href="<?php echo base_url(); ?>admin/pharmacompany_list"><span>Pharma Company List</span></a>

               <?php echo "<span class='brd'> View pharma company</span>"; ?>

               <?php //} 
               ?>

               <!-- Banks -->

               <?php /*}elseif($menuactive=='banks' || $menuactive=='bank_list' || $menuactive=='favourite_banks' || $menuactive=='view' || $menuactive=='view_favourite_bank'){*/ ?>

            <?php } elseif ($menuactive == 'favourite_banks') { ?>

               <?php echo "<span class='brd'> Favourite Banks </span>"; ?>

            <?php } elseif ($this->uri->segment(2) == 'view_favourite_bank') { ?>

               <a href="<?php echo base_url(); ?>admin/favourite_banks"><span>Favourite Banks</span></a>

               <?php echo "<span class='brd'> View favourite Bank </span>"; ?>

               <?php //} 
               ?>

               <!-- Restaurents -->

               <?php /*}elseif($menuactive=='restaurant_list' || $menuactive=='favorite_restaurant_list' || $menuactive=='view_restaurant'){*/ ?>

            <?php } elseif ($this->uri->segment(2) == 'favorite_restaurant_list') { ?>

               <?php echo " <span class='brd'>Favorite restaurant</span>"; ?>

            <?php } elseif ($menuactive == 'view_restaurant') { ?>

               <a href="<?php echo base_url(); ?>admin/favorite_restaurant_list"><span>Favorite restaurant </span></a>

               <?php echo "<span class='brd'> View favorite restaurant </span>"; ?>

               <?php //} 
               ?>

               <!-- Grocery -->

               <?php /*}elseif($menuactive=='grocery' || $menuactive=='grocery_list' || $menuactive=='favourite_grocery' ||$menuactive=='view_grocery' || $menuactive=='view_favourite_grocery'){*/ ?>

            <?php } elseif ($menuactive == 'favourite_grocery') { ?>

               <?php echo "<span class='brd'> Favourite Grocery </span>"; ?>

            <?php } elseif ($menuactive == 'view_favourite_grocery') { ?>

               <a href="<?php echo base_url(); ?>admin/favourite_grocery"><span>Favourite Grocery</span></a>

               <?php echo "<span class='brd'>View favourite grocery</span>"; ?>

               <?php //} 
               ?>

               <!-- Entertainment -->

               <?php /*}elseif($menuactive=='Articles' || $menuactive=='add_article' || $menuactive=='edit' || $menuactive=='view' || $menuactive=='articles_list' ||$menuactive=='music_list'||$menuactive=='view_music' ||$menuactive=='edit_music' ||$menuactive=='movie_list' ||$menuactive=='view_movie'||$menuactive=='edit_movie' ||$menuactive=='music_banner_list' ||$menuactive=='view_music_banner' ||$menuactive=='movie_category_list' ||$menuactive=='music_category_list'||$menuactive=='edit_music_banner' ||$menuactive=='movie_banner_list' ||$menuactive=='view_movie_banner'||$menuactive=='edit_movie_banner'||$menuactive=='add_movie_banner' ||$menuactive=='articles_list' ||$menuactive=='view_article' ||$menuactive=='edit_article' ||$menuactive=='add_article'||$menuactive=='article_category_list' ||$menuactive=='add_article_category'||$menuactive=='edit_article_category' ||$menuactive=='view_article_category' || $menuactive=='view_music_category' || $menuactive=='edit_music_category' || $menuactive=='add_music_banner' || $menuactive=='add_movie' || $menuactive=='add_movie_category'|| $menuactive=='add_music' ||$menuactive=='add_music_category' ||$menuactive=='edit_movie_category' ||$menuactive=='view_movie_category'){*/ ?>

            <?php } elseif ($menuactive == 'articles_list') { ?>

               <?php echo "<span class='brd'> Articles List</span>"; ?>

            <?php } elseif ($menuactive == 'view_article') { ?>

               <a href="<?php echo base_url(); ?>admin/articles_list"><span>Articles List</span></a>

               <?php echo "<span class='brd'> View article</span>"; ?>

            <?php } elseif ($menuactive == 'edit_article') { ?>

               <a href="<?php echo base_url(); ?>admin/articles_list"><span>Articles List</span></a>

               <?php echo "<span class='brd'> Edit article </span>"; ?>

            <?php } elseif ($menuactive == 'article_category_list') { ?>

               <?php echo "<span class='brd'> Article Category List </span>"; ?>

            <?php } elseif ($menuactive == 'view_article_category') { ?>

               <a href="<?php echo base_url(); ?>admin/article_category_list">Article Category List</a>

               <?php echo "<span class='brd'> View article category</span>"; ?>

            <?php } elseif ($menuactive == 'edit_article_category') { ?>

               <a href="<?php echo base_url(); ?>admin/article_category_list">Article Category List</a>

               <?php echo "<span class='brd'> Edit article category</span>"; ?>

            <?php } elseif ($menuactive == 'music_list') { ?>

               <?php echo "<span class='brd'>Music List</span>"; ?>

            <?php } elseif ($menuactive == 'view_music') { ?>

               <a href="<?php echo base_url(); ?>admin/music_list"><span>Music List</span></a>

               <?php echo "<span class='brd'> View Music </span>"; ?>

            <?php } elseif ($menuactive == 'edit_music') { ?>

               <a href="<?php echo base_url(); ?>admin/music_list"><span>Music List</span></a>

               <?php echo "<span class='brd'> Edit Music </span>"; ?>

            <?php } elseif ($menuactive == 'add_music') { ?>

               <a href="<?php echo base_url(); ?>admin/music_list"><span>Music List</span></a>

               <?php echo "<span class='brd'> Add Music</span>"; ?>

            <?php } elseif ($menuactive == 'music_category_list') { ?>

               <?php echo "<span class='brd'> Music Category List </span>"; ?>

            <?php } elseif ($menuactive == 'add_music_category') { ?>

               <a href="<?php echo base_url(); ?>admin/music_category_list"> Music Category List</a>

               <?php echo "<span class='brd'> Add music category</span>"; ?>

            <?php } elseif ($menuactive == 'edit_music_category') { ?>

               <a href="<?php echo base_url(); ?>admin/music_category_list"> Music Category List</a>

               <?php echo "<span class='brd'> Edit music category</span>"; ?>

            <?php } elseif ($menuactive == 'view_music_category') { ?>

               <a href="<?php echo base_url(); ?>admin/music_category_list"> Music Category List</a>

               <?php echo "<span class='brd'> View music category </span>"; ?>

            <?php } elseif ($menuactive == 'music_banner_list') { ?>

               <?php echo "<span class='brd'> Music Banner List </span>"; ?>

            <?php } elseif ($menuactive == 'view_music_banner') { ?>

               <a href="<?php echo base_url(); ?>admin/music_banner_list">Music Banner List</a>

               <?php echo "<span class='brd'> View music banner </span>"; ?>

            <?php } elseif ($menuactive == 'edit_music_banner') { ?>

               <a href="<?php echo base_url(); ?>admin/music_banner_list">Music Banner List</a>

               <?php echo "<span class='brd'> Edit music banner </span>"; ?>

            <?php } elseif ($menuactive == 'add_music_banner') { ?>

               <a href="<?php echo base_url(); ?>admin/music_banner_list">Music Banner List</a>

               <?php echo "<span class='brd'> Add music banner </span>"; ?>

            <?php } elseif ($menuactive == 'movie_list') { ?>

               <?php echo "<span class='brd'> Movie List </span>"; ?>

            <?php } elseif ($menuactive == 'view_movie') { ?>

               <a href="<?php echo base_url(); ?>admin/movie_list"><span>Movie List</span></a>

               <?php echo "<span class='brd'> View movie </span>"; ?>

            <?php } elseif ($menuactive == 'edit_movie') { ?>

               <a href="<?php echo base_url(); ?>admin/movie_list"><span>Movie List</span></a>

               <?php echo "<span class='brd'> Edit movie </span>"; ?>

            <?php } elseif ($menuactive == 'add_movie') { ?>

               <a href="<?php echo base_url(); ?>admin/movie_list"><span>Movie List</span></a>

               <?php echo "<span class='brd'> Add movie </span>"; ?>

            <?php } elseif ($menuactive == 'movie_category_list') { ?>

               <?php echo "<span class='brd'> Movie Category List </span>"; ?>

            <?php } elseif ($menuactive == 'add_movie_category') { ?>

               <a href="<?php echo base_url(); ?>admin/movie_category_list">Movie Category List</a>

               <?php echo "<span class='brd'> Add movie category </span>"; ?>

            <?php } elseif ($menuactive == 'edit_movie_category') { ?>

               <a href="<?php echo base_url(); ?>admin/movie_category_list">Movie Category List</a>

               <?php echo "<span class='brd'> Edit movie category </span>"; ?>

            <?php } elseif ($menuactive == 'view_movie_category') { ?>

               <a href="<?php echo base_url(); ?>admin/movie_category_list">Movie Category List</a>

               <?php echo "<span class='brd'> View movie category </span>"; ?>

            <?php } elseif ($menuactive == 'movie_banner_list') { ?>

               <?php echo "<span class='brd'> Movie Banner List </span>"; ?>

            <?php } elseif ($menuactive == 'view_movie_banner') { ?>

               <a href="<?php echo base_url(); ?>admin/movie_banner_list">Movie Banner List</a>

               <?php echo "<span class='brd'> View movie banner </span>"; ?>

            <?php } elseif ($menuactive == 'edit_movie_banner') { ?>

               <a href="<?php echo base_url(); ?>admin/movie_banner_list">Movie Banner List</a>

               <?php echo "<span class='brd'> Edit movie banner </span>"; ?>

            <?php } elseif ($menuactive == 'add_movie_banner') { ?>

               <a href="<?php echo base_url(); ?>admin/movie_banner_list">Movie Banner List</a>

               <?php echo "<span class='brd'> Add movie banner </span>"; ?>

               <?php //} 
               ?>

            <?php } elseif ($menuactive == 'meditation_articles_list') { ?>

               <?php echo "<span class='brd'> Meditation Articles List</span>"; ?>

            <?php } elseif ($menuactive == 'edit_meditation_article') { ?>

               <a href="<?php echo base_url(); ?>admin/meditation_articles_list"><span>Meditation Articles List</span></a>

               <?php echo "<span class='brd'> Edit meditation article</span>"; ?>

            <?php } elseif ($menuactive == 'view_meditation_article') { ?>

               <a href="<?php echo base_url(); ?>admin/meditation_articles_list"><span>Meditation Articles List</span></a>

               <?php echo "<span class='brd'> view meditation article</span>"; ?>

            <?php } elseif ($menuactive == 'meditation_video_category_list') { ?>

               <?php echo "<span class='brd'> Meditation Video category List</span>"; ?>

            <?php } elseif ($menuactive == 'meditation_article_category_list') { ?>

               <?php echo "<span class='brd'> Meditation Article category List</span>"; ?>

            <?php } elseif ($menuactive == 'add_meditation_article_category') { ?>

               <a href="<?php echo base_url(); ?>admin/meditation_article_category_list"><span>Meditation Article category List</span></a>

               <?php echo "<span class='brd'>Add Meditation Article Category</span>"; ?>

            <?php } elseif ($menuactive == 'view_meditation_article_category') { ?>

               <a href="<?php echo base_url(); ?>admin/meditation_article_category_list"><span>Meditation Article category List</span></a>

               <?php echo "<span class='brd'>View meditation article category</span>"; ?>

            <?php } elseif ($menuactive == 'edit_meditation_article_category') { ?>

               <a href="<?php echo base_url(); ?>admin/meditation_article_category_list"><span>Meditation Article category List</span></a>

               <?php echo "<span class='brd'>Edit Meditation Article Category</span>"; ?>

            <?php } elseif ($menuactive == 'edit_meditation_vedio_category') { ?>

               <a href="<?php echo base_url(); ?>admin/meditation_video_category_list"><span>meditation vedio category list</span></a>

               <?php echo "<span class='brd'> edit meditation video category</span>"; ?>

            <?php } elseif ($menuactive == 'view_meditation_vedio_category') { ?>

               <a href="<?php echo base_url(); ?>admin/meditation_video_category_list"><span>meditation vedio category list</span></a>

               <?php echo "<span class='brd'> View meditation video category</span>"; ?>

            <?php } elseif ($menuactive == 'add_meditation_video_category') { ?>

               <a href="<?php echo base_url(); ?>admin/meditation_video_category_list"><span>meditation video category list</span></a>

               <?php echo "<span class='brd'> add meditation video category</span>"; ?>

            <?php } elseif ($menuactive == 'edit_meditation_video') { ?>

               <a href="<?php echo base_url(); ?>admin/meditation_video_list"><span>meditation video list</span></a>

               <?php echo "<span class='brd'> Edit meditation video </span>"; ?>

            <?php } elseif ($menuactive == 'view_meditation_video') { ?>

               <a href="<?php echo base_url(); ?>admin/meditation_video_list"><span>meditation video list</span></a>

               <?php echo "<span class='brd'> View meditation video </span>"; ?>

            <?php } elseif ($menuactive == 'meditation_video_list') { ?>

               <a href="<?php echo base_url(); ?>admin/meditation_video_list"><span>meditation video list</span></a>

            <?php } elseif ($menuactive == 'add_meditation_video') { ?>

               <a href="<?php echo base_url(); ?>admin/meditation_video_list"><span>meditation video list</span></a>

               <?php echo "<span class='brd'> Add meditation Video</span>"; ?>

               <!-- meditation_video_banner start -->

            <?php } elseif ($menuactive == 'meditation_video_banner_list') { ?>

               <a href="<?php echo base_url(); ?>admin/meditation_video_banner_list"><span>meditation video banner list</span></a>

               <?php //echo "<span class='brd'> Add meditation Video</span>"; 
               ?>

               <!-- meditation_video_banner end -->

            <?php } elseif ($menuactive == 'add_meditation_video_banner') { ?>

               <a href="<?php echo base_url(); ?>admin/meditation_video_banner_list"><span>meditation video banner list</span></a>

               <?php echo "<span class='brd'> Add meditation video banner</span>"; ?>

               <!-- meditation_video_banner end -->

            <?php } elseif ($menuactive == 'edit_meditation_video_banner') { ?>

               <a href="<?php echo base_url(); ?>admin/meditation_video_banner_list"><span>meditation video banner list</span></a>

               <?php echo "<span class='brd'> Edit meditation video banner</span>"; ?>

               <!-- meditation_video_banner end -->

            <?php } elseif ($menuactive == 'view_meditation_video_banner') { ?>

               <a href="<?php echo base_url(); ?>admin/meditation_video_banner_list"><span>meditation video banner list</span></a>

               <?php echo "<span class='brd'> View meditation video banner</span>"; ?>

               <!-- meditation_video_banner end -->

            <?php } elseif ($menuactive == 'add_meditation_article') { ?>

               <a href="<?php echo base_url(); ?>admin/meditation_articles_list"><span>Meditation Articles List</span></a>

               <?php echo "<span class='brd'> Add meditation article</span>"; ?>

            <?php } elseif ($menuactive == 'ticket_list') { ?>

               <a href="<?php echo base_url(); ?>admin/ticket_list"><span>Tickets List</span></a>

               <?php //echo "<span class='brd'> View ticket detail</span>"; 
               ?>

            <?php } elseif ($menuactive == 'view_detail') { ?>

               <a href="<?php echo base_url(); ?>admin/ticket_list"><span>Tickets List</span></a>

               <?php echo "<span class='brd'> View ticket detail</span>"; ?>

            <?php } elseif ($menuactive == 'edit_article') { ?>

               <a href="<?php echo base_url(); ?>admin/articles_list"><span>Articles List</span></a>

               <?php echo "<span class='brd'> Edit article </span>"; ?>

               <?php //} 
               ?>

               <!-- Units -->

               <?php /*}elseif($menuactive=='unit_list' ||  $menuactive=='set_unit'){*/ ?>

            <?php } elseif ($menuactive == 'unit_list') { ?>

               <?php echo "<span class='brd'> Distance Units </span>"; ?>

            <?php } elseif ($menuactive == 'set_unit') { ?>

               <a href="admin/unit_list"><span>Distance Units</span></a>

               <?php echo "<span class='brd'> Set unit </span>"; ?>

               <?php //} 
               ?>

            <?php } ?>

         </div>

      </div>

      <?php echo $contents; ?>

      <!-- footer  -->

   </div>

   <!-- Breadcrumb End -->

   <!--Footer-part-->

   <div class="row-fluid">

      <div id="footer" class="span12"> © ( CarePro ) 2019. All rights reserved. </div>

   </div>

   <div class="modal fade" id="deleteModal" role="dialog">

      <div class="modal-dialog modal-sm">

         <div class="modal-content">

            <div class="modal-header">

               <button type="button" class="close" data-dismiss="modal">&times;</button>

               <h4 class="modal-title">Delete</h4>

            </div>

            <div class="modal-body">

               <h3>Are you sure ? You want to delete this</h3>

            </div>

            <div class="modal-footer">

               <button type="button" class="btn btn-primary" id="ok" data-dismiss="modal">Ok</button>

               <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>

            </div>

         </div>

      </div>

   </div>

   <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->

   <script src="<?php echo base_url(); ?>assets/user_dashboard/js/jquery.min.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/typeahead.bundle.js"></script>

   <script src="<?php echo base_url(); ?>assets/user_dashboard/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>

   <script src="<?php echo base_url(); ?>assets/user_dashboard/js/bootstrap.min.js" type="text/javascript"></script>

   <script src="<?php echo base_url(); ?>assets/user_dashboard/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>

   <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css">

   <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

   <script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/src/js/bootstrap-datetimepicker.js"></script>

   <!--<script src="assets/admin/js/jquery.min.js"></script>-->

   <script src="<?php echo base_url(); ?>assets/admin/js/jquery.ui.custom.js"></script>

   <!--<script src="assets/admin/js/bootstrap.min.js"></script>-->

   <script src="<?php echo base_url(); ?>assets/css/multiselect/bootstrap-multiselect.js"></script>

   <script src="<?php echo base_url(); ?>assets/admin/js/matrix.js"></script>

   <!-- <script src="assets/admin/js/matrix.dashboard.js"></script> -->

   <!-- <script src="assets/admin/js/main.js"></script> -->

   <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.dataTables.min.js"></script>

   <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/plugins/dataTables.bootstrap.min.js"></script>

   <script src="<?php echo base_url(); ?>assets/admin/js/plugins/bootstrap-datepicker.min.js"></script>

   <script src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery.timepicker.min.js"></script>

   <script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAgKI8FTGJ_QKQV7MAf4V-YirL5PTMdLDw&libraries=places"></script>

   <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js">

      </script> -->

   <!-- <script src="assets/admin/js/admin-custom.epco.js"></script> -->

   <script src="<?php echo base_url(); ?>assets/admin/js/timepicki.js"></script>

   <script type="text/javascript">
      $('#sampleTable').DataTable();
   </script>

   <script type="text/javascript">
      $.validator.addMethod("loginRegex", function(value, element) {

         return this.optional(element) || /^[a-z0-9\\-]+$/i.test(value);

      }, "Username must contain only letters, numbers, or dashes.");



      // jQuery validation for Edit admin

      $('#EditProfile').validate({

         ignore: [],

         rules: {

            firstname: {

               required: true

            },

            lastname: {

               required: true

            },

            email: {

               required: true,

               email: true,

            },

            country_code: {

               required: true

            },

            mobile_no: {

               required: true,

               number: true,

               minlength: 10,

               maxlength: 12,

            },

            username: {

               required: true,

               loginRegex: true,

            },

            dob: {

               required: true

            },

            address: {

               required: true

            },

            gender: {

               required: true

            },

            language_speak: {

               required: true,

            },

            user_profile_image: {

               required: true,

            },

         },

         submitHandler: function(form) {

            form.submit();

         }

      });



      /*--------- Change user password ----------*/

      $("#updatepassword").validate({

         rules: {

            oldpass: "required",

            newpass: {

               required: true,

               minlength: 6

            },

            newcpass: {

               required: true,

               equalTo: "#newpass",

               minlength: 6

            }

         },

         // Specify validation error messages

         messages: {

            oldpass: "Please enter your password",

            newpass: {

               required: "Please provide new password",

               minlength: "Your password must be at least 6 characters long"

            },

            newcpass: {

               required: "Please provide new password again",

               minlength: "Your password must be at least 6 characters long",

               equalTo: "Password must be same"

            },

         },

         submitHandler: function(form) {

            var formData = new FormData($("#updatepassword")[0]);

            $.ajax({

               url: '<?php echo base_url(); ?>' + 'admin/updatepassword',

               type: 'POST',

               dataType: 'json',

               data: formData,

               success: function(result) {

                  $('#results').html(result.msg);

                  if (result.status == 200) {

                     // redirect to google after 5 seconds

                     window.setTimeout(function() {

                        window.location.href = '<?php echo base_url(); ?>' + 'admin/dashboard';

                     }, 2000);

                  }

               },

               cache: false,

               contentType: false,

               processData: false

            });

         }

      });



      // jQuery validation for add user

      $('#AddUsers').validate({

         ignore: [],

         rules: {

            firstname: {

               required: true

            },

            lastname: {

               required: true

            },

            email: {

               required: true,

               email: true,

               remote: {

                  url: '<?php echo base_url(); ?>' + "validate/user/data",

                  type: "post",

               }

            },

            country_code: {

               required: true

            },

            mobile: {

               required: true,

               number: true,

               minlength: 10,

               maxlength: 12,

            },

            username: {

               required: true,

               loginRegex: true,

               remote: {

                  url: '<?php echo base_url(); ?>' + "validate/user/data",

                  type: "post",

               }

            },

            dob: {

               required: true

            },

            address: {

               required: true

            },

            gender: {

               required: true

            },

            password: {

               required: true,

               minlength: 4,

               maxlength: 30,

            },

            cpassword: {

               required: true,

               equalTo: "#password",

            },

            language_speak: {

               required: true,

            },

            user_profile_image: {

               required: true,

            },

         },

         messages: {

            email: {

               remote: "Email already in use!"

            },

            mobile_no: {

               remote: "Mobile already in use!"

            },

            username: {

               remote: "Username already in use!"

            },

            user_profile_image: {

               required: "Please select an image.",

            },

         },

         submitHandler: function(form) {

            form.submit();

         }

      });



      // jQuery validation for add order

      $('#AddOrders').validate({

         ignore: [],

         rules: {

            lang_type: {

               required: true

            },

            o_product_name: {

               required: true

            },

            o_product_desc: {

               required: true

            },

            o_order_qty: {

               required: true,

               number: true

            },

            product_price: {

               required: true,

               number: true

            },

            deliver_from: {

               required: true

            },

            deliver_to: {

               required: true

            },

            user_id: {

               required: true

            },

            traveler_id: {

               required: true

            },

            o_order_total: {

               required: true,

               number: true

            },

            o_product_image: {

               required: true,

            },

         },

         messages: {

            email: {

               remote: "Email already in use!"

            },

            mobile_no: {

               remote: "Mobile already in use!"

            },

            username: {

               remote: "Username already in use!"

            },

         },

         submitHandler: function(form) {

            form.submit();

         }

      });

      // jQuery validation for add testimonial

      $('#AddTestimonial').validate({

         ignore: [],

         rules: {

            lang_type: {

               required: true

            },

            test_title: {

               required: true

            },

            test_content: {

               required: true

            },

            test_upload: {

               required: true,

            },

         },

         submitHandler: function(form) {

            form.submit();

         }

      });



      // jQuery validation for Edit admin

      $('#AddTranslation').validate({

         rules: {

            label_english: {

               required: true

            },

            label_arabic: {

               required: true

            },

            label_hindi: {

               required: true,

               email: true,

            },

            label_portuguese: {

               required: true

            },

         },

         submitHandler: function(form) {

            form.submit();

         }

      });



      // jQuery validation for add testimonial

      $('#AddService').validate({

         ignore: [],

         rules: {

            lang_type: {

               required: true

            },

            service_title: {

               required: true

            },

            service_content: {

               required: true

            },

            service_img: {

               required: true,

            },

         },

         submitHandler: function(form) {

            form.submit();

         }

      });



      $('#AddCategory').validate({

         rules: {

            lang_type: {

               required: true

            },

            category_name: {

               required: true

            },

            "subcategory_ids[]": {

               required: true

            },

         },

         submitHandler: function(form) {

            form.submit();

         }

      });



      $('#AddSubcategory').validate({

         rules: {

            lang_type: {

               required: true

            },

            subcategory_name: {

               required: true

            },

         },

         submitHandler: function(form) {

            form.submit();

         }

      });





      // jQuery validation for add user

      $('#AddTransports').validate({

         rules: {

            transport_name: {

               required: true

            },

            user_profile_image: {

               required: true,

            },

         },

         submitHandler: function(form) {

            form.submit();

         }

      });



      // Date of birth date picker

      $('.DOB').datepicker({

         format: 'yyyy-mm-dd',

         //todayHighlight: true,

         endDate: '+0d',

         autoclose: true

      });

      // Deliver Before date picker

      $('.deliver_before').datepicker({

         format: 'yyyy-mm-dd',

         todayHighlight: true,

         startDate: '+0d',

         autoclose: true

      });



      // Intalling google address and get latitude, longitude

      function initialize() {

         var input = document.getElementById('address');

         autocomplete = new google.maps.places.Autocomplete(input);



         google.maps.event.addListener(autocomplete, 'place_changed', function() {

            var place = autocomplete.getPlace();

            var lat = place.geometry.location.lat();

            var long = place.geometry.location.lng();

            //alert('latitude'+' '+lat+','+ 'longitude'+' '+long);

            jQuery("#lat").val(lat);

            jQuery("#long").val(long);

            IsplaceChange = true;

         });

      }

      google.maps.event.addDomListener(window, 'load', initialize);

      // Intalling google address and get latitude, longitude

      function initialize_form() {

         var input = document.getElementById('deliver_from');

         autocomplete = new google.maps.places.Autocomplete(input);



         google.maps.event.addListener(autocomplete, 'place_changed', function() {

            var place = autocomplete.getPlace();

            var lat = place.geometry.location.lat();

            var long = place.geometry.location.lng();

            //alert('latitude'+' '+lat+','+ 'longitude'+' '+long);

            jQuery("#lat").val(lat);

            jQuery("#long").val(long);

            IsplaceChange = true;

         });

      }

      google.maps.event.addDomListener(window, 'load', initialize_form);



      // Intalling google address and get latitude, longitude

      function initialize_to() {

         var input = document.getElementById('deliver_to');

         autocomplete = new google.maps.places.Autocomplete(input);



         google.maps.event.addListener(autocomplete, 'place_changed', function() {

            var place = autocomplete.getPlace();

            var lat = place.geometry.location.lat();

            var long = place.geometry.location.lng();

            //alert('latitude'+' '+lat+','+ 'longitude'+' '+long);

            jQuery("#lat").val(lat);

            jQuery("#long").val(long);

            IsplaceChange = true;

         });

      }

      google.maps.event.addDomListener(window, 'load', initialize_to);



      // Enable field for multiple select

      $(document).ready(function() {

         $('.multiSelect').multiselect();

      });



      // Upload image and get preview of the image

      $(document).ready(function() {

         $(document).on('change', '.btn-file :file', function() {

            var input = $(this),

               label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

            input.trigger('fileselect', [label]);

         });



         $('.btn-file :file').on('fileselect', function(event, label) {

            var input = $(this).parents('.input-group').find('#user_profile_image'),

               log = label;

            if (input.length) {

               input.val(log);

            } else {

               if (log)

                  alert(log);

            }

         });



         function readURL(input) {

            if (input.files && input.files[0]) {

               var reader = new FileReader();



               reader.onload = function(e) {

                  $('#img-upload').attr('src', e.target.result);

               }



               reader.readAsDataURL(input.files[0]);

            }

         }



         $("#profile_image").change(function() {

            readURL(this);

         });

      });



      //To delete single row of lists

      function deleteStatus(id, attr) {

         $("#deleteModal").modal('show');

         $("#ok").click(function() {

            $.ajax({

               url: '<?php echo base_url(); ?>' + attr,

               type: "POST",

               data: {
                  'id': id
               },

               success: function(data) {

                  var dec = JSON.parse(data);

                  $("div.status-cng").addClass('alert alert-success alert-dismissible');

                  $("div.status-cng").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + dec.msg);

                  setTimeout(function() {

                     location.reload();

                  }, 2000);



               },
               error: function() {

                  $("div.status-cng").addClass('alert alert-danger alert-dismissible');

                  $("div.status-cng").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + dec.msg);

                  setTimeout(function() {

                     location.reload();

                  }, 2000);

               }

            });

         });

      }



      //To update status single row of lists

      function updateStatus(id, attr) {

         $.ajax({

            url: '<?php echo base_url(); ?>' + attr,

            type: "POST",

            data: {
               'id': id
            },

            success: function(data) {

               var dec = JSON.parse(data);

               $("div.status-cng").addClass('alert alert-success alert-dismissible');

               $("div.status-cng").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + dec.msg);

               setTimeout(function() {

                  location.reload();

               }, 2000);

            },
            error: function() {

               $("div.status-cng").addClass('alert alert-danger alert-dismissible');

               $("div.status-cng").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + dec.msg);

               setTimeout(function() {

                  location.reload();

               }, 2000);

            }

         });

      }
   </script>





   <script>
      function genrateToken() {

         // set the length of the string

         var stringLength = 8;



         // list containing characters for the random string

         var stringArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];



         var rndString = "";



         // build a string with random characters

         for (var i = 1; i < stringLength; i++) {

            var rndNum = Math.ceil(Math.random() * stringArray.length) - 1;

            rndString = rndString + stringArray[rndNum];

         };



         $("#token").val(rndString);

      }

      /*$( document ).ready(function() {



         // set the length of the string

         var stringLength = 15;



         // list containing characters for the random string

           var stringArray = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','!','?'];



         $("#generateToken").click(function (){



            var rndString = "";

         

            // build a string with random characters

            for (var i = 1; i < stringLength; i++) { 

               var rndNum = Math.ceil(Math.random() * stringArray.length) - 1;

               rndString = rndString + stringArray[rndNum];

            };

            

            $("#token").val(rndString);



         });



      });*/
   </script>

</body>

</html>
