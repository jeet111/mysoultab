<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_test extends MX_Controller {

  public function __construct() {
      parent:: __construct();
      $this->load->library('session');
      $this->load->library('form_validation');
      $this->form_validation->set_error_delimiters('<div class="error">', '</div>');       
	     $this->load->model('medicine_model');
		 $this->load->model('Common_model_new'); 
      $this->load->helper(array('common_helper'));
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));      
  }
  
  public function medicine_reminder1()
  { 
      $date = date('d/m/Y H:i:s');

	  $user_id = $this->session->userdata('logged_in')['id']; 
	  $user = $this->Common_model_new->getAllwhere("cp_users",array("status" => 1));
	  

  foreach($user as $u){
	  $medicine_data = $this->Common_model_new->getAllwhere("medicine_schedule",array("user_id" => $u->id));
	  foreach($medicine_data as $data){
		if($data->medicine_reminder == 1){ 
	  $medicine_date = date('Y-m-d',strtotime($data->medicine_create_date ."+$data->medicine_take_days days"));
	 $test = $data->medicine_create_date;
	 
	
	  $explode = explode(' ',$test);
	 $dates = $this->getDatesFromRange($explode[0], $medicine_date);
	   
	   foreach($dates as $dd){
		  $today = date('Y-m-d');
		   		  
		   if($today == $dd){echo 'test'.'<br>';
		   $times = date('h:i A', strtotime($date));
		   //echo $data->medicine_time;
		   $explode_time = explode(',',$data->medicine_time);
		   foreach($explode_time as $ti){
		   if($times == $ti){echo 'testtt'.'<br>';
		   $email = $u->email;
            $message =
              "<table>
                 <tr><th>Medicine Name:</th>
                 <td>".$data->medicine_name."</td></tr>
                 <tr><th>Date:</th>
                 <td>".$dd."</td></tr>
                 <tr><th>Time:</th>
                 <td>".$data->medicine_time."</td></tr>
                 
              </table>";
           $subject = "Medicine Reminder";
           sendEmail($email,$subject,$message);
		   $notification_table = $this->Common_model_new->insertData("cp_notification",array("user_id" => $u->id,"medicine_id" => $data->medicine_id,"notification_date" => date("Y-m-d H:i:s")));
		   }}}else{
			  // echo 'error'.'<br>';
		   }
	   }
	   
  }}}die;
	  
  }
  
   public function medicine_reminder()
  { 
      $date = date('d/m/Y h:i:s');
	

	  $user_id = $this->session->userdata('logged_in')['id']; 
	  $user = $this->Common_model_new->getAllwhere("cp_users",array("status" => 1));
	  

  foreach($user as $u){
	  $medicine_data = $this->Common_model_new->getAllwhere("medicine_schedule",array("user_id" => $u->id));
	  foreach($medicine_data as $data){
		if($data->medicine_reminder == 1){ 
	  $medicine_date = date('Y-m-d',strtotime($data->medicine_create_date ."+$data->medicine_take_days days"));
	 $test = $data->medicine_create_date;
	 
	
	  $explode = explode(' ',$test);
	 $dates = $this->getDatesFromRange($explode[0], $medicine_date);
	  //echo '<pre>';print_r($dates);
	   foreach($dates as $dd){
		 $today = date('Y-m-d');
		 
		   	 //$dates= date('Y-m-d h:i:s');	  
		   if($today == $dd){echo 'test'.'<br>';
		    $times = date('h:i A');
		   //echo $data->medicine_time;
		   $explode_time = explode(',',$data->medicine_time);
		   //print_r($explode_time);
		   foreach($explode_time as $ti){
			   
		   if($times == $ti){echo 'testtt'.'<br>';
		   $email = $u->email;
            $message =
              "<table>
                 <tr><th>Medicine Name:</th>
                 <td>".$data->medicine_name."</td></tr>
                 <tr><th>Date:</th>
                 <td>".$dd."</td></tr>
                 <tr><th>Time:</th>
                 <td>".$data->medicine_time."</td></tr>
                 
              </table>";
           $subject = "Medicine Reminder";
		       
          //$send = sendEmail($email,$subject,$message);
		   //print_r($message);
		 $array = array("user_id" => $u->id,"common_id" => $data->medicine_id,'type' => 1,"notification_date" => date("Y-m-d H:i:s"));
		  
		   $notification_table = $this->Common_model_new->insertData("cp_common_notification",array("user_id" => $u->id,"common_id" => $data->medicine_id,'type' => 1,"notification_date" => date("Y-m-d H:i:s")));
		   }}}else{
			  // echo 'error'.'<br>';
		   }
	   }
	   
  }}}die;
	  
  }
  
 public function readnotification() {
	    $medicine = $this->input->post("medicine");
		$segment = $this->input->post("segment");
        $update = $this->Common_model_new->updateData('cp_common_notification', array('is_read' => 1), array('user_id' => $this->session->userdata('logged_in')['id'],'notification_id' => $segment));
        $response = array('success' => true, 'message' => 'Notification read successfully');
        echo json_encode($response);
    }
  
  function getDatesFromRange($start, $end, $format = 'Y-m-d') {
    $array = array();
    $interval = new DateInterval('P1D');

    $realEnd = new DateTime($end);
    $realEnd->add($interval);

    $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

    foreach($period as $date) { 
        $array[] = $date->format($format); 
    }

    return $array;
}

  
}