<!-- header  -->



<!DOCTYPE html>



<html lang="en">



  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <link rel="icon" href="assets/admin/img/favicon.ico" type="image/ico" sizes="16x16">



    <title><?php echo !empty($title) ? $title : "Admin Panel"; ?></title>



    <base href="<?php echo base_url();?>">



    <link rel="stylesheet" href="assets/admin/css/admin_master.css" />

    
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/newinternal.css" />


    <link rel="stylesheet" href="assets/admin/css/bootstrap-responsive.min.css" />



    <link rel="stylesheet" href="assets/admin/css/bootstrap.min.css" />



    <link rel="stylesheet" href="assets/css/multiselect/bootstrap-multiselect.css">



    <link rel="stylesheet" href="assets/admin/css/datepicker.css" />



    <!-- <link rel="stylesheet" href="assets/admin/css/jquery.timepicker.min.css" /> -->



    <link rel="stylesheet" href="assets/admin/css/timepicki.css">



    <link rel="stylesheet" href="assets/admin/css/font-awesome.css" />



    <link rel="stylesheet" href="assets/admin/css/jquery-customselect.css" />



    <link rel="stylesheet" href="assets/admin/css/matrix-style.css" />



    <link rel="stylesheet" href="assets/admin/css/matrix-media.css" />



    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>



    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">



    <!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/jquery.dataTables.css" />





    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/jquery.dataTables.min.css" />





    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/jquery.dataTables_themeroller.css" /> -->



  </head>



  <body>



    <div id="header">



      <h1><a href="<?php echo base_url();?>admin/dashboard">Care Pro</a></h1>  



    </div>           



    <div id="user-nav" class="navbar navbar-inverse">



      <ul class="nav">



        <li  class="dropdown" id="profile-messages" >



          <a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle">



            <i class="icon icon-user"></i>



            <?php



            // $user = $this->session->userdata("user_id"); ?>



            <span class="text">Welcome <?php //echo customerdata($user); ?></span><b class="caret"></b>



          </a>



          <ul class="dropdown-menu">



            <li>



              <a href="admin/adminprofile"><i class="icon-user"></i> My Profile</a>



            </li>

            <li>



              <a href="admin/adminedit"><i class="icon-user"></i> Edit Profile</a>



            </li>

            <li>



              <a href="admin/changepassword"><i class="icon-user"></i> Change Password</a>



            </li>



          </ul>



        </li>





        <li class="">



          <a title="" href="admin/logout">



            <i class="icon icon-share-alt"></i> <span class="text">Logout</span>



          </a>



        </li>



      </ul>



    </div>



    <div id="sidebar">

      <a href="admin/dashboard" class="visible-phone"><i class="fa fa-dot-circle-o"></i> Dashboard</a>

      <ul>

        <li class="<?php echo ($menuactive=='dashboard')?'active':'';?>" >

          <a href="admin/dashboard"> <i class="fa fa-fw fa-dashboard"></i> <span>Dashboard</span></a>

        </li>

        <li class="submenu <?php echo ($menuactive=='users' || $menuactive=='add' || $menuactive=='edit' || $menuactive=='view' || $menuactive=='family_member' || $menuactive=='addfamily_member' || $menuactive=='editfamily' || $menuactive=='viewfamily_member'  || $menuactive=='caregiver' || $menuactive=='addcaregiver' || $menuactive=='editcare' || $menuactive=='viewcaregiver' ||$menuactive=='contacts_list' ||$menuactive=='note_list' ||$menuactive=='view_note' || $menuactive_add == 'user' || $menuactive_add == 'family' || $menuactive_add == 'caregiver')?'open':'';?>">

          <a href="#">

            <i class="fa fa-fw fa-user-circle-o"></i>

            <span>Users Management</span> <span class="label label-important"><b class="caret"></b></span>

          </a>

          <ul>

            <li class="<?php echo ($menuactive=='users' || $menuactive=='add' || $menuactive=='edit' || $menuactive=='view' || $menuactive_add == 'user')?'active':'';?>" >

              <a href="admin/users/list"><i class="fa fa-dot-circle-o"></i> <span>Person Listing</span></a>

            </li>

            <li class="<?php echo ($menuactive=='family_member' || $menuactive=='addfamily_member' || $menuactive=='editfamily' || $menuactive=='viewfamily_member' || $menuactive_add == 'family')?'active':'';?>" >

              <a href="admin/family_member/list"><i class="fa fa-dot-circle-o"></i> <span>Family Member List</span></a>

            </li>

            <li class="<?php echo ($menuactive=='caregiver' || $menuactive=='addcaregiver' || $menuactive=='editcare' || $menuactive=='viewcaregiver' || $menuactive_add == 'caregiver')?'active':'';?>" >

              <a href="admin/caregiver/list"><i class="fa fa-dot-circle-o"></i> <span>Care Giver List</span></a>

            </li>



            <li class="<?php echo ($menuactive=='contacts_list')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/contacts_list"><i class="fa fa-dot-circle-o"></i> <span>Contact List</span></a>

              </li>





            <li class="<?php echo ($menuactive=='note_list' ||$menuactive=='view_note')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/note_list"><i class="fa fa-dot-circle-o"></i> <span>Note List</span></a>

              </li>

          </ul>

        </li>



        

        

        <!-- Doctor Management Created by 95 on 4-2-2019 Start -->

        <li class="submenu <?php echo ($menuactive=='doctor-category' || $menuactive=='Add' || $menuactive=='edit' || $menuactive=='view' || $menuactive=='list' || $menuactive=='add_doctor' || $menuactive=='doctor_list' || $menuactive=='doctor_shedules' ||$menuactive=='medicine_shedules' ||$menuactive=='doctor_appointmensts' )?'open':'';?>">

          <a href="#">

            <i class="fa fa-user-md" aria-hidden="true"></i>

            <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

            <span>Doctor Management</span> <span class="label label-important"><b class="caret"></b></span>

          </a>

          <ul>





        <?php 

        //echo $this->uri->segment(3);

         ?>





          <li class="<?php echo ($menuactive=='doctor-category' || $menuactive=='edit' || $menuactive=='view' || $menuactive=='doctor_list' )?'active':'';?>" >

                <a href="admin/doctor_list"><i class="fa fa-dot-circle-o"></i> <span>Doctor List</span></a>

              </li>







          <li class="<?php echo ($menuactive=='doctor-category' || $menuactive=='edit' || $menuactive=='view' || $menuactive=='list')?'active':'';?>" >

                <a href="admin/doctor-category/list"><i class="fa fa-dot-circle-o"></i> <span>Doctor Specialties List</span></a>

              </li>



          <li class="<?php echo ($menuactive=='doctor_shedules')?'active':'';?>" >

                <a href="admin/doctor_shedules"><i class="fa fa-dot-circle-o"></i> <span>Doctor Schedule List</span></a>

              </li>





          <li class="<?php echo ($menuactive=='doctor_appointmensts')?'active':'';?>" >

                <a href="admin/doctor_appointmensts"><i class="fa fa-dot-circle-o"></i> <span>Doctor Appointments</span></a>

              </li>



            <li class="<?php echo ($menuactive=='medicine_shedules')?'active':'';?>" >

                <a href="admin/medicine_shedules"><i class="fa fa-dot-circle-o"></i> <span>Medicine Schedule List</span></a>

              </li>



            <!-- <li class="<?php echo ($menuactive=='Add')?'active':'';?>" >

              <a href="admin/doctor-category/Add">

                

                <i class="fa fa-dot-circle-o"></i>

                <span>Add Specialties</span></a>

              </li> -->

              

			  <!-- <li class="<?php echo ($menuactive=='doctor-category' || $menuactive=='edit' || $menuactive=='view' || $menuactive=='add_doctor')?'active':'';?>" >

                <a href="admin/add_doctor"><i class="fa fa-dot-circle-o"></i> <span>Add Doctor</span></a>

              </li> -->

			  



              





              



              

              

            </ul>

          </li>

          <!-- Doctor Management Created by 95 on 4-2-2019 End -->









        <li class="<?php echo ($menuactive=='userphoto')?'active':'';?>" >

          <a href="admin/userphoto/list"> <i class="fa fa-fw fa-photo"></i> <span>Photo Gallery </span></a>

        </li>

        <li class="<?php echo ($menuactive=='email_list')?'active':'';?>" >

          <a href="admin/email_list/list"> <i class="fa fa-envelope"></i> <span>Emails</span></a>

        </li>

		<li class="submenu <?php echo ($menuactive=='Test_type' || $menuactive=='add_test_type' || $menuactive=='edit' || $menuactive=='view' || $menuactive=='test_type_list')?'open':'';?>">

          <a href="javascript:void(0);">

            <i class="fa fa-file-text-o" aria-hidden="true"></i>

            <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

            <span>Test Type</span> <span class="label label-important"><b class="caret"></b></span>

          </a>

          <ul>

		     <li class="<?php echo ($menuactive=='test_type_list')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/test_type_list"><i class="fa fa-dot-circle-o"></i> <span>Test Type List</span></a>

              </li>

			  <!-- <li class="<?php echo ($menuactive=='add_test_type')?'active':'';?>" >

                <a href="admin/add_test_type"><i class="fa fa-dot-circle-o"></i> <span>Add test type</span></a>

              </li> -->

		  </ul>



</li>





<li class="<?php echo ($menuactive=='acknowledge_list')?'active':'';?>" >

          <a href="admin/acknowledge_list"> <i class="fa fa-file" aria-hidden="true"></i> <span>Acknowledge</span></a>

        </li>







<li class="submenu <?php echo ($menuactive=='Articles' || $menuactive=='add_article' || $menuactive=='edit' || $menuactive=='view' || $menuactive=='articles_list' ||$menuactive=='music_list'||$menuactive=='view_music' ||$menuactive=='edit_music' ||$menuactive=='movie_list' ||$menuactive=='view_movie'||$menuactive=='edit_movie' ||$menuactive=='music_banner_list' ||$menuactive=='view_music_banner' ||$menuactive=='movie_category_list' ||$menuactive=='music_category_list'||$menuactive=='edit_music_banner' ||$menuactive=='movie_banner_list' ||$menuactive=='view_movie_banner'||$menuactive=='edit_movie_banner'||$menuactive=='add_movie_banner')?'open':'';?>">

          <a href="javascript:void(0);">

            <i class="fa fa-book" aria-hidden="true"></i>

            

            <span>Entertainment</span> <span class="label label-important"><b class="caret"></b></span>

          </a>

          <ul>



         <li class="<?php echo ($menuactive=='articles_list')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/articles_list"><i class="fa fa-dot-circle-o"></i> <span>Articles List</span></a>

              </li>



        <li class="<?php echo ($menuactive=='music_list' ||$menuactive=='view_music' ||$menuactive=='edit_music')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/music_list"><i class="fa fa-dot-circle-o"></i> <span>Music List</span></a>

              </li>



        <li class="<?php echo ($menuactive=='movie_list' ||$menuactive=='view_movie'||$menuactive=='edit_movie')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/movie_list"><i class="fa fa-dot-circle-o"></i> <span>Movie List</span></a>

              </li>

			  

	 <li class="<?php echo ($menuactive=='music_category_list' ||$menuactive=='add_music_category'||$menuactive=='edit_music_category')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/music_category_list"><i class="fa fa-dot-circle-o"></i> <span>Music Category List</span></a>

              </li>

			  

	<li class="<?php echo ($menuactive=='movie_category_list' ||$menuactive=='add_movie_category'||$menuactive=='edit_movie_category')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/movie_category_list"><i class="fa fa-dot-circle-o"></i> <span>Movie Category List</span></a>

              </li>		  





        <li class="<?php echo ($menuactive=='music_banner_list' ||$menuactive=='view_music_banner'||$menuactive=='edit_music_banner')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/music_banner_list"><i class="fa fa-dot-circle-o"></i> <span>Music Banner List</span></a>

              </li>





        <li class="<?php echo ($menuactive=='movie_banner_list' ||$menuactive=='view_movie_banner'||$menuactive=='edit_movie_banner'||$menuactive=='add_movie_banner')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/movie_banner_list"><i class="fa fa-dot-circle-o"></i> <span>Movie Banner List</span></a>

        </li>



        

      </ul>



</li>





<!-- <li class="submenu <?php echo ($menuactive=='Music' || $menuactive=='add_music' || $menuactive=='edit' || $menuactive=='view' || $menuactive=='music_list')?'open':'';?>">

          <a href="javascript:void(0);">

            <i class="fa fa-music" aria-hidden="true"></i>

            

            <span>Music</span> <span class="label label-important"><b class="caret"></b></span>

          </a>

          <ul>





            

         <li class="<?php echo ($menuactive=='music_list')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/music_list"><i class="fa fa-dot-circle-o"></i> <span>Music List</span></a>

              </li>

        

      </ul>



</li> -->







<!-- <li class="submenu <?php echo ($menuactive=='Movie' || $menuactive=='add_movie' || $menuactive=='edit' || $menuactive=='view' || $menuactive=='movie_list')?'open':'';?>">

          <a href="javascript:void(0);">

            <i class="fa fa-film" aria-hidden="true"></i>

            

            <span>Movie</span> <span class="label label-important"><b class="caret"></b></span>

          </a>

          <ul>





         <li class="<?php echo ($menuactive=='movie_list')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/movie_list"><i class="fa fa-dot-circle-o"></i> <span>Movie List</span></a>

              </li>

        

      </ul>



</li> -->







<li class="submenu <?php echo ($menuactive=='Pharma_company' || $menuactive=='add_pharmacompany' || $menuactive=='edit' || $menuactive=='pharmacompany_list')?'open':'';?>">

          <a href="javascript:void(0);">

            <i class="fa fa-film" aria-hidden="true"></i>

            <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

            <span>Pharma Company</span> <span class="label label-important"><b class="caret"></b></span>

          </a>

          <ul>





            <!-- <li class="<?php echo ($menuactive=='add_pharmacompany')?'active':'';?>" >

                <a href="admin/add_pharmacompany"><i class="fa fa-dot-circle-o"></i> <span>Add Pharma Company</span></a>

              </li> -->

         <li class="<?php echo ($menuactive=='pharmacompany_list')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/pharmacompany_list"><i class="fa fa-dot-circle-o"></i> <span>Pharma Company List</span></a>

              </li>

        

      </ul>



</li>









<li class="submenu <?php echo ($menuactive=='banks' || $menuactive=='bank_list' || $menuactive=='favourite_banks' || $menuactive=='view')?'open':'';?>">

          <a href="javascript:void(0);">

            <i class="fa fa-film" aria-hidden="true"></i>

            <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

            <span>Banks</span> <span class="label label-important"><b class="caret"></b></span>

          </a>

          <ul>





            <li class="<?php echo ($menuactive=='bank_list')?'active':'';?>" >

                <a href="admin/bank_list"><i class="fa fa-dot-circle-o"></i> <span>Bank List</span></a>

              </li>

         <li class="<?php echo ($menuactive=='favourite_banks')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/favourite_banks"><i class="fa fa-dot-circle-o"></i> <span>Favourite Banks</span></a>

              </li>

        

      </ul>



</li>



<!-- <li class="submenu <?php echo ($menuactive=='Contact' || $menuactive=='contacts_list')?'open':'';?>">

          <a href="javascript:void(0);">

            <i class="fa fa-address-book" aria-hidden="true"></i>

            

            <span>User's contact</span> <span class="label label-important"><b class="caret"></b></span>

          </a>

          <ul>



         <li class="<?php echo ($menuactive=='contacts_list')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/contacts_list"><i class="fa fa-dot-circle-o"></i> <span>Contact List</span></a>

              </li>

        

      </ul>



</li> -->



<li class="submenu <?php echo ($menuactive=='restaurant_list' || $menuactive=='favorite_restaurant_list' || $menuactive=='view_restaurant')?'open':'';?>">

          <a href="javascript:void(0);">

            <i class="fa fa-address-book" aria-hidden="true"></i>

            <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

            <span>Restaurants</span> <span class="label label-important"><b class="caret"></b></span>

          </a>

          <ul>



         <li class="<?php echo ($menuactive=='restaurant_list')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/restaurant_list"><i class="fa fa-dot-circle-o"></i> <span>Restaurant List</span></a>

         </li>

		<li class="<?php echo ($menuactive=='favorite_restaurant_list')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/favorite_restaurant_list"><i class="fa fa-dot-circle-o"></i> <span>Favorite restaurant </span></a>

         </li> 

        

      </ul>



</li>



<!-- <li class="submenu <?php echo ($menuactive=='grocery_list' || $menuactive=='favorite_grocery_list' || $menuactive=='view_grocery')?'open':'';?>">

          <a href="javascript:void(0);">

            <i class="fa fa-address-book" aria-hidden="true"></i>

           

            <span>Grocery</span> <span class="label label-important"><b class="caret"></b></span>

          </a>

          <ul>



         <?php  ?><li class="<?php echo ($menuactive=='grocery_list')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/restaurant_list"><i class="fa fa-dot-circle-o"></i> <span>Restaurant List</span></a>

         </li><?php  ?>

		<li class="<?php echo ($menuactive=='favorite_grocery_list')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/favorite_grocery_list"><i class="fa fa-dot-circle-o"></i> <span>Favorite grocery </span></a>

         </li> 

        

      </ul>



</li> -->









<li class="submenu <?php echo ($menuactive=='grocery' || $menuactive=='grocery_list' || $menuactive=='favourite_grocery' ||$menuactive=='view_grocery')?'open':'';?>">

          <a href="javascript:void(0);">

            <i class="fa fa-film" aria-hidden="true"></i>

            <!-- <i class="fa fa-fw fa-user-circle-o"></i> -->

            <span>Grocerys</span> <span class="label label-important"><b class="caret"></b></span>

          </a>

          <ul>





            <li class="<?php echo ($menuactive=='grocery_list' ||$menuactive=='view_grocery')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/grocery_list"><i class="fa fa-dot-circle-o"></i> <span>Grocery List</span></a>

              </li>

         <li class="<?php echo ($menuactive=='favourite_grocery')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/favourite_grocery"><i class="fa fa-dot-circle-o"></i> <span>Favourite Grocery</span></a>

              </li>

        

      </ul>



</li>







<!-- <li class="submenu <?php echo ($menuactive=='note' || $menuactive=='note_list'  ||$menuactive=='view_note')?'open':'';?>">

          <a href="javascript:void(0);">

            <i class="fa fa-film" aria-hidden="true"></i>

            

            <span>Note</span> <span class="label label-important"><b class="caret"></b></span>

          </a>

          <ul>





            <li class="<?php echo ($menuactive=='note_list' ||$menuactive=='view_note')?'active':'';?>" >

                <a href="<?php echo base_url(); ?>admin/note_list"><i class="fa fa-dot-circle-o"></i> <span>Note List</span></a>

              </li>

         

        

      </ul>



</li> -->

        

      </ul>



    </div>

    <div id="content">

      <div id="content-header">

        <div id="breadcrumb"> <a href="<?php echo base_url().'admin/dashboard'; ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>

      </div>



    <?php echo $contents;?>



<!-- footer  -->



</div>



<!--Footer-part-->







<div class="row-fluid">



  <div id="footer" class="span12"> © ( CarePro ) 2019. All rights reserved. </div>



</div>



<div class="modal fade" id="deleteModal" role="dialog">

      <div class="modal-dialog modal-sm">

        <div class="modal-content">

          <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal">&times;</button>

            <h4 class="modal-title">Delete</h4>

          </div>

          <div class="modal-body">

            <h3>Are you sure ? You want to delete this</h3>

          </div>

          <div class="modal-footer">

            <button type="button" class="btn btn-primary" id="ok" data-dismiss="modal">Ok</button>

            <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>

          </div>

        </div>

      </div>

</div>



<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

        

        

  <script src="<?php echo base_url(); ?>assets/user_dashboard/js/jquery.min.js"></script>

        

  <script src="<?php echo base_url(); ?>assets/user_dashboard/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>

		

  <script src="<?php echo base_url(); ?>assets/user_dashboard/js/bootstrap.min.js" type="text/javascript"></script>

  <script src="<?php echo base_url(); ?>assets/user_dashboard/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>

        

<link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/src/js/bootstrap-datetimepicker.js"></script>



		

<!--<script src="assets/admin/js/jquery.min.js"></script>-->



<script src="assets/admin/js/jquery.ui.custom.js"></script>



<!--<script src="assets/admin/js/bootstrap.min.js"></script>-->



<script src="assets/css/multiselect/bootstrap-multiselect.js"></script>



<script src="assets/admin/js/matrix.js"></script>



<!-- <script src="assets/admin/js/matrix.dashboard.js"></script> -->



<!-- <script src="assets/admin/js/main.js"></script> -->



<script type="text/javascript" src="assets/admin/js/plugins/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="assets/admin/js/plugins/dataTables.bootstrap.min.js"></script>



<script src="assets/admin/js/plugins/bootstrap-datepicker.min.js"></script>



<script src="assets/admin/js/plugins/jquery.timepicker.min.js"></script>  



<script src="assets/admin/js/jquery.validate.js"></script>



<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAgKI8FTGJ_QKQV7MAf4V-YirL5PTMdLDw&libraries=places"></script>



<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js">



</script> -->



<!-- <script src="assets/admin/js/admin-custom.epco.js"></script> -->



<script src="assets/admin/js/timepicki.js"></script>



<script type="text/javascript">$('#sampleTable').DataTable();</script>



<script type="text/javascript">



  $.validator.addMethod("loginRegex", function(value, element) {

        return this.optional(element) || /^[a-z0-9\\-]+$/i.test(value);

    }, "Username must contain only letters, numbers, or dashes.");



  // jQuery validation for Edit admin

  $('#EditProfile').validate({

    ignore: [],

    rules: {

      firstname: {

        required: true

      },

      lastname: {

        required: true

      },

      email: {

        required: true,

        email: true,

      },

      country_code: {

        required: true

      },

      mobile_no: {

        required: true,

        number: true,

        minlength: 10,

        maxlength: 12,

      },

      username: {

        required: true,

        loginRegex:true,

      },

      dob: {

        required: true

      },

      address: {

        required: true

      },

      gender: {

        required: true

      },

      language_speak: {

        required: true,

      },

      user_profile_image: {

        required: true,

      },

    },

    submitHandler: function(form) {

      form.submit();

    }

  });



  /*--------- Change user password ----------*/

  $("#updatepassword").validate({

    rules: {

      oldpass: "required",

      newpass:  {

        required: true,

        minlength: 6

      },

      newcpass: {

        required: true,

        equalTo: "#newpass",

        minlength: 6

      }

    },

    // Specify validation error messages

    messages: {

      oldpass: "Please enter your password",

      newpass: {

        required: "Please provide new password",

        minlength: "Your password must be at least 6 characters long"

      },

      newcpass: {

        required: "Please provide new password again",

        minlength: "Your password must be at least 6 characters long",

        equalTo: "Password must be same"

      },

    },

    submitHandler: function(form) {

      var formData = new FormData($("#updatepassword")[0]);

      $.ajax({

        url: '<?php echo base_url(); ?>'+'admin/updatepassword',

        type: 'POST',

        dataType: 'json',

        data: formData,

        success: function(result){

          $('#results').html(result.msg);

          if (result.status==200) {

          // redirect to google after 5 seconds

          window.setTimeout(function() {

            window.location.href = '<?php echo base_url(); ?>'+'admin/dashboard';

            }, 2000);

          }

        },

        cache: false,

        contentType: false,

        processData: false

      });

    }

  });



  // jQuery validation for add user

  $('#AddUsers').validate({

    ignore: [],

    rules: {

      firstname: {

        required: true

      },

      lastname: {

        required: true

      },

      email: {

        required: true,

        email: true,

        remote: {

          url: '<?php echo base_url(); ?>'+"validate/user/data",

          type: "post",

        }

      },

      country_code: {

        required: true

      },

      mobile: {

        required: true,

        number: true,

        minlength: 10,

        maxlength: 12,

      },

      username: {

        required: true,

        loginRegex:true,

        remote: {

          url: '<?php echo base_url(); ?>'+"validate/user/data",

          type: "post",

        }

      },

      dob: {

        required: true

      },

      address: {

        required: true

      },

      gender: {

        required: true

      },

      password: {

        required: true,

        minlength: 4,

        maxlength: 30,

      },

      cpassword: {

        required: true,

        equalTo: "#password",

      },

      language_speak: {

        required: true,

      },

      user_profile_image: {

        required: true,

      },

    },

    messages: {

      email: {

        remote: "Email already in use!"

      },

      mobile_no: {

        remote: "Mobile already in use!"

      },

      username: {

        remote: "Username already in use!"

      },

      user_profile_image: {

        required: "Please select an image.",

      },

    },

    submitHandler: function(form) {

      form.submit();

    }

  });



  // jQuery validation for add order

  $('#AddOrders').validate({

    ignore: [],

    rules: {

      lang_type: {

        required: true

      },

      o_product_name: {

        required: true

      },

      o_product_desc: {

        required: true

      },

      o_order_qty: {

        required: true,

        number: true

      },

      product_price: {

        required: true,

        number: true

      },

      deliver_from: {

        required: true

      },

      deliver_to: {

        required: true

      },

      user_id: {

        required: true

      },

      traveler_id: {

        required: true

      },

      o_order_total: {

        required: true,

        number:true

      },

      o_product_image: {

        required: true,

      },

    },

    messages: {

      email: {

        remote: "Email already in use!"

      },

      mobile_no: {

        remote: "Mobile already in use!"

      },

      username: {

        remote: "Username already in use!"

      },

    },

    submitHandler: function(form) {

      form.submit();

    }

  });

  // jQuery validation for add testimonial

  $('#AddTestimonial').validate({

    ignore: [],

    rules: {

      lang_type: {

        required: true

      },

      test_title: {

        required: true

      },

      test_content: {

        required: true

      },

      test_upload: {

        required: true,

      },

    },

    submitHandler: function(form) {

      form.submit();

    }

  });



  // jQuery validation for Edit admin

  $('#AddTranslation').validate({

    rules: {

      label_english: {

        required: true

      },

      label_arabic: {

        required: true

      },

      label_hindi: {

        required: true,

        email: true,

      },

      label_portuguese: {

        required: true

      },

    },

    submitHandler: function(form) {

      form.submit();

    }

  });



   // jQuery validation for add testimonial

  $('#AddService').validate({

    ignore: [],

    rules: {

      lang_type: {

        required: true

      },

      service_title: {

        required: true

      },

      service_content: {

        required: true

      },

      service_img: {

        required: true,

      },

    },

    submitHandler: function(form) {

      form.submit();

    }

  });



  $('#AddCategory').validate({

    rules: {

      lang_type: {

        required: true

      },

      category_name: {

        required: true

      },

      "subcategory_ids[]": {

        required: true

      },

    },

    submitHandler: function(form) {

      form.submit();

    }

  });



  $('#AddSubcategory').validate({

    rules: {

      lang_type: {

        required: true

      },

      subcategory_name: {

        required: true

      },

    },

    submitHandler: function(form) {

      form.submit();

    }

  });





  // jQuery validation for add user

  $('#AddTransports').validate({

    rules: {

      transport_name: {

        required: true

      },

      user_profile_image: {

        required: true,

      },

    },

    submitHandler: function(form) {

      form.submit();

    }

  });



  // Date of birth date picker

  $('.DOB').datepicker({

    format: 'yyyy-mm-dd',

    //todayHighlight: true,

    endDate: '+0d',

    autoclose: true

  });

  // Deliver Before date picker

  $('.deliver_before').datepicker({

    format: 'yyyy-mm-dd',

    todayHighlight: true,

    startDate: '+0d',

    autoclose: true

  });



  // Intalling google address and get latitude, longitude

  function initialize() {

    var input = document.getElementById('address');

    autocomplete = new google.maps.places.Autocomplete(input);



    google.maps.event.addListener(autocomplete, 'place_changed', function (){

        var place = autocomplete.getPlace();

        var lat = place.geometry.location.lat();

        var long = place.geometry.location.lng();

        //alert('latitude'+' '+lat+','+ 'longitude'+' '+long);

        jQuery("#lat").val(lat);

        jQuery("#long").val(long);

        IsplaceChange = true;

    });

  }

  google.maps.event.addDomListener(window, 'load', initialize);

    // Intalling google address and get latitude, longitude

  function initialize_form() {

    var input = document.getElementById('deliver_from');

    autocomplete = new google.maps.places.Autocomplete(input);



    google.maps.event.addListener(autocomplete, 'place_changed', function (){

        var place = autocomplete.getPlace();

        var lat = place.geometry.location.lat();

        var long = place.geometry.location.lng();

        //alert('latitude'+' '+lat+','+ 'longitude'+' '+long);

        jQuery("#lat").val(lat);

        jQuery("#long").val(long);

        IsplaceChange = true;

    });

  }

  google.maps.event.addDomListener(window, 'load', initialize_form);



  // Intalling google address and get latitude, longitude

  function initialize_to() {

    var input = document.getElementById('deliver_to');

    autocomplete = new google.maps.places.Autocomplete(input);



    google.maps.event.addListener(autocomplete, 'place_changed', function (){

        var place = autocomplete.getPlace();

        var lat = place.geometry.location.lat();

        var long = place.geometry.location.lng();

        //alert('latitude'+' '+lat+','+ 'longitude'+' '+long);

        jQuery("#lat").val(lat);

        jQuery("#long").val(long);

        IsplaceChange = true;

    });

  }

  google.maps.event.addDomListener(window, 'load', initialize_to);



  // Enable field for multiple select

  $(document).ready(function() {

    $('.multiSelect').multiselect();

  });



  // Upload image and get preview of the image

  $(document).ready( function() {

    $(document).on('change', '.btn-file :file', function() {

      var input = $(this),

      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

      input.trigger('fileselect', [label]);

    });



    $('.btn-file :file').on('fileselect', function(event, label) {        

      var input = $(this).parents('.input-group').find('#user_profile_image'),

      log = label;        

      if( input.length ) {

        input.val(log);

      } else {

        if( log )

          alert(log);

      }      

    });



    function readURL(input) {

      if (input.files && input.files[0]) {

        var reader = new FileReader();

          

        reader.onload = function (e) {

          $('#img-upload').attr('src', e.target.result);

        }

          

        reader.readAsDataURL(input.files[0]);

      }

    }



    $("#profile_image").change(function(){

        readURL(this);

    });   

  });



  //To delete single row of lists

  function deleteStatus(id, attr){

    $("#deleteModal").modal('show');

    $("#ok").click(function(){

      $.ajax({

        url: '<?php echo base_url(); ?>'+attr,

        type: "POST",

        data:{'id':id},

        success: function(data){       

          var dec = JSON.parse(data);

          $("div.status-cng").addClass('alert alert-success alert-dismissible');

          $("div.status-cng").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+dec.msg);

          setTimeout(function(){

            location.reload(); 

          }, 2000);

          

        },error: function(){

          $("div.status-cng").addClass('alert alert-danger alert-dismissible');

          $("div.status-cng").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+dec.msg);

          setTimeout(function(){

            location.reload(); 

          }, 2000);

        }

      });

    });    

  }



  //To update status single row of lists

  function updateStatus(id, attr){

    $.ajax({

      url: '<?php echo base_url(); ?>'+attr,

      type: "POST",

      data:{'id':id},

      success: function(data){        

        var dec = JSON.parse(data);

        $("div.status-cng").addClass('alert alert-success alert-dismissible');

        $("div.status-cng").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+dec.msg);

        setTimeout(function(){

          location.reload(); 

        }, 2000);

      },error: function(){

        $("div.status-cng").addClass('alert alert-danger alert-dismissible');

        $("div.status-cng").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+dec.msg);

        setTimeout(function(){

          location.reload(); 

        }, 2000);

      }

    });

  }  

</script>



</body>



</html>

    