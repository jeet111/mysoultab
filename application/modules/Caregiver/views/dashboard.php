				<div class="row">
					<div class="col-lg-11">
						<div class="Games">
							<h2 align="center"> Caregiver Dashboard </h2>
						</div>
                    </div>
					
					<div class="row"> 
                        <div class="col-lg-11">
                            <h4 align="center">Display Caregiver Profile Button On Mobile App</h4>
                        </div>

						<div class="col-lg-11">
							<div id="msg_show" align="center"></div>
						</div>

						<div class="col-md-10">
							<?php 
							if(!empty($caregiver_array) && $caregiver_array !=''){ 
							foreach($caregiver_array as $res){ 
							if($res->settings == 1) { ?>
								<div class="tooltip-2"><a data-btn="<?php echo 'Caregiver_profile'?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-on" aria-hidden="true"> </i> </a> Current Status Active</div>
							<?php }else { ?>
								<div class="tooltip-2 "><a data-btn="<?php echo 'Caregiver_profile'?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-off" aria-hidden="true"> </i></a> Current Status Inactive</div>
							<?php } ?>
							<?php } } ?>  
						</div>
                    </div>
				</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>

<script type="text/javascript">

	$(document).on('click', '.updateStatus', function() {
        var update_status = $(this).attr('data-btn');
        var update_st = $(this).attr('data-st');
        //alert(update_status+'-'+update_st);
        $.ajax({
            url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
            type: "POST",
            data: {
                'btn': update_status
            },
            dataType: "json",
            success: function(data) {
                setTimeout(function() {
                    $("#msg_show").html("<div class='alert alert-success'>" + data.status + "</div>");
                    location.reload();

                }, 1500);
            }
        });
    });
</script>


