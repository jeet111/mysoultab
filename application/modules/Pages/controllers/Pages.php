<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pages extends MX_Controller {
  public function __construct() {
    parent:: __construct();
    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->library('table');
    $this->load->library('pagination');
    $this->load->model('common_model');
    $this->load->model('common_model_new');
    $this->load->model('Common_model_new');
    $this->load->helper(array('common_helper'));
    $this->load->helper(array('url'));
    $this->load->helper(array('form'));
    not_login();
  }
  public function slider()
  {
    $this->load->view('slider2');
  }
  public function ajax_photo_add()
  {
    $checkLogin = $this->session->userdata('logged_in');
    $data = array(
      'user_id' => $checkLogin['id'],
      'u_photo_status'=>'1',
      'u_photo_created' => date('Y-m-d H:i:s')
    );
    $config1['upload_path'] = 'uploads/photos/';
    $config1['allowed_types'] = 'gif|jpg|jpeg|png';
           //$data['max_size'] = '5000';
    $config1['encrypt_name'] = true;
    $this->load->library('upload', $config1);
    $this->upload->initialize($config1);


    if ($this->upload->do_upload('imgInp')) {
      $attachment_data_image = array('upload_data' => $this->upload->data());
      $picturefile = $attachment_data_image['upload_data']['file_name'];
      $return = array('code' => 1, 'message' => 'Successfully added');


      /*For image resize*/
      $this->load->library('image_lib');
      $configer =  array(
        'image_library'   => 'gd2',
        'source_image'    =>  $attachment_data_image['upload_data']['full_path'],
        'maintain_ratio'  =>  TRUE,
        'width'           =>  370,
        'height'          =>  410,
      );
      $this->image_lib->clear();
      $this->image_lib->initialize($configer);
      $this->image_lib->resize();
      $data['u_photo'] = $picturefile;
      $insertId = $this->common_model->addRecords('cp_user_photo', $data);
      $return = array("code" => 1,'message' => 'Photo have been successfully added');
    } else {
      $return = array('code' => 2, 'message' => strip_tags($this->upload->display_errors()));
    }

  //   if ($this->upload->do_upload('imgInp')) {
  //     $attachment_data = array('upload_data' => $this->upload->data());
  //     $uploadfile = $attachment_data['upload_data']['file_name'];
  //     /*For image resize*/
  //     $this->load->library('image_lib');
  //     $configer =  array(
  //       'image_library'   => 'gd2',
  //       'source_image'    =>  $attachment_data['upload_data']['full_path'],
  //       'maintain_ratio'  =>  TRUE,
  //       'width'           =>  370,
  //       'height'          =>  410,
  //     );
  //     $this->image_lib->clear();
  //     $this->image_lib->initialize($configer);
  //     $this->image_lib->resize();
  //     $data['u_photo'] = $uploadfile;
  //     $insertId = $this->common_model->addRecords('cp_user_photo', $data);
  //     $return = array("code" => 1,'message' => 'Photo have been successfully added');
  //   }else{
  //    $return = array('code' => 2,'message' => strip_tags($this->upload->display_errors()));
  //  }
   echo json_encode($return);
 }
 public function dashboard(){ 
  // echo "sdfdsfsdf";
  // die('rer');    
  $data['xyz']='this is test';
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    //echo $this->session->userdata('long');die;
      //$data['total_email'] = $this->common_model->jointwotablenm('cp_emails', 'user_id', 'cp_users', 'id',array('to_email ' => $checkLogin['email'],'email_status' => 1,'cp_emails.inbox_status' => 0),'','cp_emails.email_id','desc');
   $data['total_email'] = $this->Common_model_new->getEmails1($checkLogin['id']);
      //echo '<pre>';print_r($data['total_email']);die;
   $data['doctor_appointments'] = $this->common_model_new->getAllappointments($checkLogin['id']);
   $data['count_photo'] = $this->common_model_new->getAllwhere("cp_user_photo",array("user_id" => $checkLogin['id']));
   $data['paymentDetails'] = $this->common_model_new->getAllwhere("subscription_payment_transactions",array("user_id" => $checkLogin['id']));
          //$url = "http://api.openweathermap.org/data/2.5/weather?q=indore,IN&APPID=62f6de3f7c0803216a3a13bbe4ea9914&units=metric";
   $url = "https://api.openweathermap.org/data/2.5/forecast?q=indore,IN&APPID=62f6de3f7c0803216a3a13bbe4ea9914&units=metric";
   $json=file_get_contents($url);
   $data['weather']=json_decode($json,true);
   //$data['weather']='';
   
   $data['user_data'] = $this->common_model->getSingleRecordById('cp_users',array('id' => $user_id));
   $this->template->set('title', 'User Dashboard');
   $this->template->load('user_dashboard_layout', 'contents', 'user_dashboard', $data);
 }else{
  redirect('login');
}
}
public function user_transacctions()
{
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
  //$checkLogin['id']
    $data['paymentDetails'] = $this->common_model_new->getAllwhere("subscription_payment_transactions",array("user_id" => 737));
    $data['plans'] = $this->common_model_new->getAllwhere("subscription_plan",array("status" =>1));
    $this->template->set('title', 'User Dashboard');
    $this->template->load('user_dashboard_layout', 'contents', 'transaction_list', $data);
  }else{
    redirect('login');
  }
}
public function address_book()
{
  $data['menuactive'] = $this->uri->segment(1);    
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
  //$checkLogin['id']
    $data['addresses'] = $this->common_model_new->getAllwhere("address_book",array("user_id" =>$checkLogin['id']));
    //$data['plans'] = $this->common_model_new->getAllwhere("subscription_plan",array("status" =>1));
    $this->template->set('title', 'User Dashboard');
    $this->template->load('new_user_dashboard_layout', 'contents', 'address_list', $data);
  }else{
    redirect('login');
  }
}
public function add_address()
{
  if($this->session->userdata('logged_in')){
    $data = array();
    $data['menuactive'] = $this->uri->segment(1);
    $this->template->set('title', 'Address');
    $this->template->load('new_user_dashboard_layout', 'contents', 'add_address', $data);
  }else{
    redirect('login');
  }
}
public function delete_address()
{
  $delete_id = $this->uri->segment('2');
  $this->Common_model_new->delete("address_book",array("address_id" => $delete_id));
  $this->session->set_flashdata('success', 'Successfully deleted');
  redirect('address_book');    
}
public function add_address1(){ 
  $data = array();
  $data['menuactive'] = $this->uri->segment(1);
  $address = $this->input->post('address');
  // $reminder_description = $this->input->post('reminder_description');
  // $reminder_date = $this->input->post('reminder_date'); 
  // $before_time = $this->input->post('before_time'); 
  // $repeat_time = $this->input->post('repeat_time'); 
  $data['singleData'] =  array();
  //$reminder_time = $this->input->post('reminder_time');
  $array =array(
   'user_id' => $this->session->userdata('logged_in')['id'],
   'address' => $address,
   //'reminder_description'=> $reminder_description,
   //'reminder_date' => $reminder_date,
   //'reminder_time' => $reminder_time,
   //'reminder_before'=>$before_time,
   //'repeat'=>$repeat_time,
   'created' => date('Y-m-d H:i:s')
 );
  $result = $this->Common_model_new->insertData('address_book',$array);
  if($result){
    echo 1;
  }else{
    echo 0;
  }
}
public function edit_address()
{
  if($this->session->userdata('logged_in')){  
    $data = array();
    $data['menuactive'] = $this->uri->segment(1);
    $data['singleData'] = $this->Common_model_new->getsingle("address_book",array('address_id' => $this->uri->segment('2')));
    $this->template->set('title', 'Address');
    $this->template->load('new_user_dashboard_layout', 'contents', 'edit_address', $data);
  }else{
    redirect('login');
  }
}
public function edit_address1(){ 
  $data = array();
  $data['menuactive'] = $this->uri->segment(1);
  $address_id= $this->input->post('address_id');
  $address = $this->input->post('address');
  // $reminder_description = $this->input->post('reminder_description');
  // $reminder_date = $this->input->post('reminder_date');
  // $before_time = $this->input->post('before_time'); 
  // $repeat_time = $this->input->post('repeat_time');  
  // $reminder_time = $this->input->post('reminder_time');
  //$explode = explode('/',$reminder_date);
  //$new_explode = $explode['2'].'-'.$explode['0'].'-'.$explode['1'];  
  $array =array(
   //'user_id' => $this->session->userdata('logged_in')['id'],
   'address' => $address,
   // 'reminder_description'=> $reminder_description,
   // 'reminder_date' => $reminder_date,
   // 'reminder_time' => $reminder_time,
   // 'reminder_before'=>$before_time,
   // 'repeat'=>$repeat_time,
   'created' => date('Y-m-d H:i:s')
 ); 
  $result = $this->Common_model_new->updateData('address_book',$array,array('address_id' => $address_id));
  if($result){
    echo 1;
  }else{
    echo 0;
  }
}
public function view_address()
{
  $address_id = $this->uri->segment(2);
  $data['menuactive'] = $this->uri->segment(1);
  $data['addressDetail']  = $this->Common_model_new->getsingle('address_book',array('address_id'=>$address_id));
  $this->template->set('title', 'Address');
  $this->template->load('new_user_dashboard_layout', 'contents', 'view_address', $data);
}

public function call()
{
  //die($this->uri->segment(1));
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  $data['socialList'] = $this->common_model_new->getAllwhere("call",array("user_id" =>$checkLogin['id']));
  $data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'call'));
  $this->template->set('title', 'call');   
  $this->template->load('new_user_dashboard_layout', 'contents', 'call',$data);       
}
public function transpotation()
{
  //die($this->uri->segment(1));
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  $data['transportation_array'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'Transportation'));
  $this->template->set('title', 'Transpotation');   
  $this->template->load('new_user_dashboard_layout', 'contents', 'transportation_dashboard',$data);       
}
public function vital_signs()
{
  //die($this->uri->segment(1));
  $data['menuactive'] = $this->uri->segment(1);
  $user_id = $this->session->userdata('logged_in')['id'];

  $checkLogin = $this->session->userdata('logged_in');
  $data['socialList'] = $this->common_model_new->getAllwhere("socials",array("user_id" =>$checkLogin['id']));
  $data['vitalArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'Vtal Signs',"user_id"=>$user_id));
  $this->template->set('title', 'Vital Signs');   
  $this->template->load('user_dashboard_layout', 'contents', 'vital_signs',$data);       
}
public function movie()
{
  //die($this->uri->segment(1));
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  $data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'Movies',"user_id"=> $checkLogin['id']));
  $this->template->set('title', 'Movie');   
  $this->template->load('new_user_dashboard_layout', 'contents', 'movie',$data);       
}
public function medicine_list()
{
  //die($this->uri->segment(1));
  $data['menuactive'] = $this->uri->segment(1);
  $user_id = $this->session->userdata('logged_in')['id'];

  $checkLogin = $this->session->userdata('logged_in');
  $data['medicine_list'] = $this->common_model_new->getAllwhere("medicine_list",array("user_id" =>$checkLogin['id']));
  $data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'Medicine Schedule',"user_id" =>$user_id));

  $this->template->set('title', 'Medicine Schedule');   
  $this->template->load('new_user_dashboard_layout', 'contents', 'medicine_list',$data);       
}
public function social()
{
  //die($this->uri->segment(1));
  $data['menuactive'] = $this->uri->segment(1);
  $user_id = $this->session->userdata('logged_in')['id'];

  $checkLogin = $this->session->userdata('logged_in');
  $data['socialList'] = $this->common_model_new->getAllwhere("socials",array("user_id" =>$checkLogin['id']));
  $data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'Social',"user_id"=>$user_id));
  $this->template->set('title', 'Social');   
  $this->template->load('user_dashboard_layout', 'contents', 'socials',$data);       
}
public function add_social()
{
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    //$this->form_validation->set_rules('social_url', 'URL', 'required');
    $this->form_validation->set_rules('socialname', 'Name', 'required');
    $this->form_validation->set_rules('type', 'Type', 'required');
    //$this->form_validation->set_rules('app', 'App', 'required');
    if ($this->form_validation->run() == TRUE)
    {
      if($_POST['submit']){
        //Check whether user upload picture
        // if(!empty($_FILES['icon']['name'])){
        //   $config['upload_path'] = 'uploads/social/';
        //   $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|txt|doc|docx';
        //   $config['file_name'] = $_FILES['icon']['name'];
        //         //Load upload library and initialize configuration
        //   $this->load->library('upload',$config);
        //   $this->upload->initialize($config);
        //   if($this->upload->do_upload('icon')){
        //     $uploadData = $this->upload->data();
        //     $picture = $uploadData['file_name'];   
        //   }else{
        //     $picture = '';
        //   }
        // }else{
        //   $picture = '';
        // }
        if($this->input->post('type')==2){
          $app_id  = $this->input->post('app');
          $SingleappData = $this->common_model->getSingleRecordById('app_list',array('id' => $app_id));
          $app_url = $SingleappData['url'];
          $picture = $SingleappData['icon'];
          $url=''; 
          $array =array(
           'user_id' => $this->session->userdata('logged_in')['id'],
           'social_name' => $this->input->post('socialname'),
           'type' => $this->input->post('type'),
           'app_id'=>$app_id,
           'app_url' => $app_url,
           'social_url'=> $url,
           'social_icon' => $picture,
           'created' => date('Y-m-d H:i:s')
         );
        }else{
         $url = $this->input->post('social_url');
         $app_url ='';
         $array =array(
           'user_id' => $this->session->userdata('logged_in')['id'],
           'social_name' => $this->input->post('socialname'),
           'type' => $this->input->post('type'),
           'app_url' => $app_url,
           'social_url'=> $url,
         //'social_icon' => $picture,
           'created' => date('Y-m-d H:i:s')
         );
       }
       $insert = $this->Common_model_new->insertData('socials',$array);
       if($insert){
        $this->session->set_flashdata('success','Social site added successfully');
        redirect('social');
      }
    }      
  }
  $data['appData'] = $this->common_model_new->getAllwhere("app_list",array('status' => 1));
  //$data['appData'] = $this->common_model->getSingleRecordById('app_list',array('status' => 1)); 
  $this->template->set('title', 'Social');  
  $this->template->load('user_dashboard_layout', 'contents', 'add_social',$data);   
}else{
 redirect('login'); 
}
}
/*Created by 95 for edit test report by user*/
public function edit_social($social_id)
{
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    if(isset($_POST['submit'])){
      //$this->form_validation->set_rules('social_url', 'URL', 'required');
      $this->form_validation->set_rules('socialname', 'Name', 'required');
      $this->form_validation->set_rules('type', 'Type', 'required');
      if ($this->form_validation->run() == TRUE) 
      {
        if($this->input->post('type')==2){
          $app_id = $this->input->post('app');
          $SingleappData = $this->common_model->getSingleRecordById('app_list',array('id' => $app_id));
          $app_url = $SingleappData['url'];
          $picture = $SingleappData['icon'];
          $url='';
          $data=array(
            'user_id' => $this->session->userdata('logged_in')['id'],
            'social_name' => $this->input->post('socialname'),
            'type' => $this->input->post('type'),
            'app_id'=>$app_id,
            'app_url' => $app_url,
            'social_url'=> $url,            
            'social_icon' => $picture,
            'updated' => date('Y-m-d H:i:s')
          );
        }else{
         $url = $this->input->post('social_url');
         $app_url ='';
         $data=array(
          'user_id' => $this->session->userdata('logged_in')['id'],
          'social_name' => $this->input->post('socialname'),
          'type' => $this->input->post('type'),
          'app_id'=>'',
          'app_url' => $app_url,
          'social_url'=> $url,            
              //'social_icon' => $picture,
          'updated' => date('Y-m-d H:i:s')
        );
       }
       $this->Common_model_new->updateData('socials',$data,array('id' => $social_id));
       $this->session->set_flashdata('success','Social site updated successfully');
       redirect('social');
     }
   }
   $data['socialData'] = $this->common_model->getSingleRecordById('socials',array('id' => $social_id,'user_id'=>$this->session->userdata('logged_in')['id']));
   $data['appData'] = $this->common_model_new->getAllwhere("app_list",array('status' => 1)); 
   $this->template->set('title', 'Social');
   $this->template->load('user_dashboard_layout', 'contents', 'edit_social',$data);
 }else{
   redirect('login'); 
 }
}
/*Created by 95 for single testreport delete*/
public function delete_social()
{
  $delete_id = $this->uri->segment('2');
  $where_condition=array("id" => $delete_id);
  $this->common_model->deleteRecords('socials',$where_condition);
  //$this->testreport_model->delete("socials",array("id" => $delete_id));
  $this->session->set_flashdata('success', 'Successfully deleted');
  redirect('social'); 
}


public function add_transpotation()   
{  
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    //$this->form_validation->set_rules('tr_url', 'URL', 'required');
    $this->form_validation->set_rules('trname', 'Name', 'required');
    $this->form_validation->set_rules('type', 'Type', 'required');
    if ($this->form_validation->run() == TRUE) 
    {
      if($_POST['submit']){
        //Check whether user upload picture
        if(!empty($_FILES['icon']['name'])){
          $config['upload_path'] = 'uploads/transpotation/';
          $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|txt|doc|docx';
          $config['file_name'] = $_FILES['icon']['name'];
                //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('icon')){
            $uploadData = $this->upload->data();
            $picture = $uploadData['file_name'];   
          }else{
            $picture = '';
          }
        }else{
          $picture = '';
        }
        if($this->input->post('type')==2){
          //$url='';
          $app_url = $this->input->post('app');
          $url='';
        }else{
         $url = $this->input->post('tr_url');
         $app_url ='';
       }
       $array =array(
         'user_id' => $this->session->userdata('logged_in')['id'],
         'tr_name' => $this->input->post('trname'),
         'type' => $this->input->post('type'),
         'app_url'=> $app_url,
         'tr_url'=> $url,
         'tr_icon' => $picture,
         'created' => date('Y-m-d H:i:s')
       );
       $insert = $this->Common_model_new->insertData('transpotation',$array);
       if($insert){
        $this->session->set_flashdata('success','transport added successfully');
        redirect('transpotation');
      }
    }
  }
  $this->template->set('title', 'Transpotation');  
  $this->template->load('user_dashboard_layout', 'contents', 'add_transpotation',$data);   
}else{
 redirect('login'); 
}
}
/*Created by 95 for edit test report by user*/
public function edit_transpotation($transport_id)
{
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    if(isset($_POST['submit'])){
     //$this->form_validation->set_rules('tr_url', 'URL', 'required');
     $this->form_validation->set_rules('trname', 'Name', 'required');
     $this->form_validation->set_rules('type', 'Type', 'required');
     if ($this->form_validation->run() == TRUE) 
     {
  //Check whether user upload picture
      if(!empty($_FILES['icon']['name'])){
        $config['upload_path'] = 'uploads/transpotation/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|txt|doc|docx';
        $config['file_name'] = $_FILES['icon']['name'];
                //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('icon')){
          $uploadData = $this->upload->data();
          $picture = $uploadData['file_name'];
        }else{
          $picture = '';
        }
        if($this->input->post('type')==2){
          //$url='';
          $app_url = $this->input->post('app');
          $url='';
        }else{
         $url = $this->input->post('tr_url');
         $app_url ='';
       }
       $data=array(
        'user_id' => $this->session->userdata('logged_in')['id'],
        'tr_name' => $this->input->post('trname'),
        'type' => $this->input->post('type'),
        'app_url'=> $app_url,
        'tr_url'=> $url,
        'tr_icon' => $picture,
        'updated' => date('Y-m-d H:i:s')
      );
       $this->Common_model_new->updateData('transpotation',$data,array('id' => $transport_id));
       $this->session->set_flashdata('success','Transpotation updated successfully');
       redirect('transpotation');
     }else{
      $picture = '';
    }
    $data=array(
      'user_id' => $this->session->userdata('logged_in')['id'],
      'tr_name' => $this->input->post('trname'),
      'type' => $this->input->post('type'),
      'tr_url'=> $this->input->post('tr_url'),
      'updated' => date('Y-m-d H:i:s')
    );
    $insert= $this->Common_model_new->updateData('transpotation',$data,array('id' => $transport_id));
    if($insert){
      $this->session->set_flashdata('success','Transpotation updated successfully');
      redirect('transpotation');
    }
  }
}
$data['transportData'] = $this->common_model->getSingleRecordById('transpotation',array('id' => $transport_id,'user_id'=>$this->session->userdata('logged_in')['id']));
$this->template->set('title', 'Transpotation');
$this->template->load('user_dashboard_layout', 'contents', 'edit_transpotation',$data);
}else{
 redirect('login'); 
}
}
/*Created by 95 for single testreport delete*/
public function delete_transpotation()
{
  $delete_id = $this->uri->segment('2');
  $where_condition=array("id" => $delete_id);
  $this->common_model->deleteRecords('transpotation',$where_condition);
  //$this->testreport_model->delete("socials",array("id" => $delete_id));
  $this->session->set_flashdata('success', 'Successfully deleted');
  redirect('transpotation'); 
}
public function weekly_weather()
{
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  $user_id= $checkLogin['id'];
  // echo "<pre>";
  // print_r($checkLogin);
  // die;
  if(!empty($checkLogin))
  {
    $url = "https://api.openweathermap.org/data/2.5/forecast?q=indore,IN&APPID=62f6de3f7c0803216a3a13bbe4ea9914&units=metric";
    $json=file_get_contents($url);
    $data['weather']=json_decode($json,true);
    $data['user_data'] = $this->common_model->getSingleRecordById('cp_users',array('id' => $user_id));
    $data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'Weather',"user_id"=>$user_id));
    $this->template->set('title', 'User Dashboard');
    $this->template->load('user_dashboard_layout', 'contents', 'weekly_weather', $data);
  }else{
    redirect('login'); 
  }
}

public function change_button_status()
{
  $btn = $this->input->post('btn');
  $user_id = $this->session->userdata('logged_in')['id'];
	//$user_id = $this->session->userdata('logged_in')['cp_users_id'];
	
	if(!empty($user_id) && $user_id !=''){
		//$update_status = $this->input->post('update_status');
		$checkLogin = $this->session->userdata('logged_in');
		$getdata = $this->common_model->getsingle('setting_butns',array('btnname' =>  $btn,'user_id'=>$user_id));
	
		if(empty($getdata)){
			if($btn == 'dailyroutine'){
					$data = array(
						'btnname' => $btn,
						'btnicon' =>  "https://www.mysoultab.com/assets/images/btn/daily_routine_icon.png",
						'user_id'=>$user_id,
						'type'=>'28',
						'tab_name'=>'personal',
						'isSocialList'=>'0',
						'created' => date('Y-m-d H:i:s'),
						'updated' => date('Y-m-d H:i:s'),
						'settings' =>1
					);
					$insertId = $this->common_model->addRecords('setting_butns', $data);
					$message="Button is Active Successfully on the App";
					$this->session->set_flashdata('success', $message);

					$error['status'] = $message;
					echo json_encode( $error);
			}
		}
	
	
		if(!empty($getdata->settings) && $getdata->settings ==1){
			$button_status = $this->common_model->updateData("setting_butns",$data = array(
				'settings'=>0,
				'updated'=>date('Y-m-d h:m:s'),
			),array("user_id" => $user_id,'btnname'=> $btn));
		
			$message="Button is Deactive Successfully on the App";
			$this->session->set_flashdata('success', $message);
			$error['status'] = $message;
			echo json_encode( $error);
		}else{
			$button_status = $this->common_model->updateData("setting_butns",$data = array(
				'settings'=>1,
				'updated'=>date('Y-m-d h:m:s'),
			),array("user_id" => 	$user_id,'btnname'=> $btn));
			$message="Button is Active Successfully on the App";
			$this->session->set_flashdata('success', $message);
			$error['status'] = $message;
			echo json_encode( $error);
		}
		
		
	}else{
		$error['status'] = 'empty data sent for update record';
		echo json_encode( $error);
	}
}
public function user_email_list(){
  $user_id = $this->session->userdata('logged_in')['id'];

  $data['menuactive'] = $this->uri->segment(1);
  // print_r($data['menuactive']); 
  // die;
  $checkLogin = $this->session->userdata('logged_in');
  $unique_id = substr(number_format(time() * rand(),0,'',''),0,4);
  $this->session->set_userdata('unique_id',$unique_id);
  if(!empty($checkLogin))
  {
         // $data['user_data'] = $this->common_model->jointwotablenm('cp_emails', 'user_id', 'cp_users', 'id',array('to_email ' => $checkLogin['email'],'email_status' => 1,'cp_emails.inbox_status' => '0'),'','cp_emails.email_id','desc');
    /*-----------------------------new------------------*/
    $array = "id !=".$checkLogin['id'];
    $data["dfdsf"] = $this->common_model->getAllwhere("cp_users",$array);
    // echo "<pre>";
    // print_r($data["dfdsf"]);
    // die;
    //$test=array
    $test='';
    foreach($data["dfdsf"] as $user){
      $test .=  '"'.$user->email.'"'.',';
      // echo $test;
      // echo "<br>";
    }
    // die('sdf');
    // echo "<pre>";
    // print_r($test);
    // die;
    //  $var_search = rtrim($test, ',');
    $data['test'] = $test;
    $emailId = $this->Common_model_new->getsingle('cp_emails', array('user_id' => $checkLogin['id']));
		if(!empty($emailId) && $emailId!=''){
			$totable = $this->Common_model_new->getAllwhere('cp_to_email', array('email_id' => $emailId->email_id));
		}
		// foreach($totable as $to){
    $emailData =$this->Common_model_new->getEmails1($checkLogin['id']);
    $data['user_data'] = $emailData;
    //echo '<pre>';print_r($data['user_data']);die;
    if(!empty($emailData)){
     foreach ( $emailData as $row ){
      if($row['create_email'] == "0000-00-00"){
       $emails_create = $row['create_email'];
     }else{
       $emails_create = date("M d, Y H:i:s", strtotime($row['create_email']));
     }
   }
    //}
 }
 /*-------------------------------------------------------------------*/
   // echo $this->db->last_query();die;
      //$data['user_data'] = $this->common_model->getAllRecordsById('cp_emails',array('to_email ' => $checkLogin['email']));
 $data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'Email',"user_id"=>$user_id));
 $this->template->set('title', 'Mailbox');
 $this->template->load('new_user_dashboard_layout', 'contents', 'user_emails', $data);
}else{
  redirect('login');
}
}
public function user_sendemail_list(){
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  $unique_id = substr(number_format(time() * rand(),0,'',''),0,4);
  $this->session->set_userdata('unique_id',$unique_id);
  if(!empty($checkLogin))
  {
         // $data['user_data'] = $this->common_model->getAllRecordsByIdnew('cp_emails',array('user_id' => $checkLogin['id'],'email_status' => 1,'send_box_status' => '0'),'email_id','desc');
      //print_r($data); die;
    $array = "id !=".$checkLogin['id'];
    $data["dfdsf"] = $this->common_model->getAllwhere("cp_users",$array);
    $test='';
    foreach($data["dfdsf"] as $user){
      $test .=  '"'.$user->email.'"'.',';
    }
    //  $var_search = rtrim($test, ',');
    $data['test'] = $test;
    // $data['user_data'] = $this->db->select('t1.email_id,t1.from_email,t1.to_email,t1.subject,t1.message,t1.create_email,t1.email_status,t1.user_id as euserid,t1.unique_flag,t1.send_box_status,t1.inbox_status,t1.to_email_list,t2.id,t2.name,t2.lastname,t2.dob,t2.username,t2.city,t2.state,t2.zipcode,t2.company_school,t2.email,t2.mobile,t2.password,t2.profile_image,t2.device_id,t2.device_type,t2.device_token,t2.user_role,t2.apartment,t2.street_name,t2.act_link,t2.status,t2.conversation_status,t2.user_verify_by_app,t2.device_verify,t2.device_verify_by_token,t2.create_user,t2.update_user,t2.otp,t2.otp_expire,t2.country_code,t2.mobile_with_code,t2.gender,t2.address,t2.user_id')
    // ->from('cp_emails as t1')
    // ->where('t1.id', $id)
    // ->join('cp_users as t2', 't1.user_id = t2.id', 'LEFT')
    // ->order_by("t1.email_id", "desc")
    // ->get();
    $data['user_data'] = $this->Common_model_new->jointwotablennquery("cp_emails", "user_id", "cp_users","id",array("cp_emails.user_id" => $checkLogin['id']),"*","cp_emails.email_id","desc");
    // echo "<pre>";
    // print_r($data['user_data']);
    // die;
    $this->template->set('title', 'Mailbox');
    $this->template->load('user_dashboard_layout', 'contents', 'senduser_emails', $data);
  }else{
    redirect('login');
  }
}
public function add_remainder(){
  $data = array();
  $data['menuactive'] = $this->uri->segment(1);
  if(isset($_POST['submit'])){
    $this->form_validation->set_rules('fullname', 'fullname', 'trim|required');
    if($this->form_validation->run() == false)
    {
      $this->template->set('title', 'Contact');
      $this->template->load('page_layout', 'contents', 'contact', $data);
    }else{
      $data = array(
        'full_name' => $this->input->post('fullname'),
        'email' =>  $this->input->post('email'),
        'comment' => $this->input->post('comment'),
        'create_date' => date('Y-m-d H:i:s'),
        'status' =>0
      );
      $insertId = $this->common_model->addRecords('cp_reminder', $data);
      if ($insertId) {
        $message = "Your message has been sumitted.";
        $this->session->set_flashdata('success', $message);
        redirect('contact');
      }
      $message = array('message' => 'Your message has not been sumitted.');
      $this->session->set_flashdata('success', $message);
      redirect('contact');
    }
  }else{
    $this->template->set('title', 'Remainder');
    $this->template->load('user_dashboard_layout', 'contents', 'add_remainder', $data);
  }
}
public function edit_remainder(){
  $data = array();
  $data['menuactive'] = $this->uri->segment(1);
  if(isset($_POST['submit'])){
    $this->form_validation->set_rules('fullname', 'fullname', 'trim|required');
    if($this->form_validation->run() == false)
    {
      $this->template->set('title', 'Contact');
      $this->template->load('page_layout', 'contents', 'contact', $data);
    }else{
      $data = array(
        'full_name' => $this->input->post('fullname'),
        'email' =>  $this->input->post('email'),
        'comment' => $this->input->post('comment'),
        'create_date' => date('Y-m-d H:i:s'),
        'status' =>0
      );
      $insertId = $this->common_model->addRecords('cp_reminder', $data);
      if ($insertId) {
        $message = "Your message has been sumitted.";
        $this->session->set_flashdata('success', $message);
        redirect('contact');
      }
      $message = array('message' => 'Your message has not been sumitted.');
      $this->session->set_flashdata('success', $message);
      redirect('contact');
    }
  }else{
    $this->template->set('title', 'Remainder');
    $this->template->load('user_dashboard_layout', 'contents', 'edit_remainder', $data);
  }
}
  // public function sendemail(){
  //   print_r($_FILES); die;
  //   $checkLogin = $this->session->userdata('logged_in');
  //   if(!empty($checkLogin))
  //   {
  //       $data = array(
  //         'to_email' => $this->input->post('email_to'),
  //         'message' => $this->input->post('email_message'),
  //         'subject' => $this->input->post('subject'),
  //         'user_id' => $checkLogin['id'],
  //         'create_email' => date('Y-m-d'),
  //         'email_status' => 1
  //       );
  //       print_r($_FILES); die;
  //       $insertId = $this->common_model->addRecords('cp_emails', $data);
  //       if(!empty($_FILES['attachment_file']['name'])) {
  //         $count = count($_FILES['attachment_file']['name']);
  //             foreach($_FILES['attachment_file']['name'] as $key=>$val){
  //                  $random = $this->generateRandomString(10);
  //                  $ext = pathinfo($_FILES['attachment_file']['name'][$key], PATHINFO_EXTENSION);
  //                  $file_name = $random.".".$ext;
  //                  $target_dir = "uploads/email/";
  //                  $target_file = $target_dir .$file_name;
  //                  if (move_uploaded_file($_FILES["attachment_file"]["tmp_name"][$key], $target_file)){
  //                    $productFile = $file_name;
  //                  }
  //                 $orderGalleryData = array('attachment_file'=>$productFile,
  //                                        'email_id'=>$insertId,
  //                             );
  //                 $this->common_model->addRecords('cp_emails_attachment', $orderGalleryData);
  //             }
  //       }
  //       if ($insertId) {
  //             $email = $data['to_email'];
  //             $message =$data['message'];
  //             $subject = $data['subject'];
  //             sendEmail($email,$subject,$message);
  //         }
  //   }else{
  //       redirect('email_list');
  //   }
  // }
/*Modified by 95 for email attachment with email on 2-2-2019*/
public function sendemail(){
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    $data = array(
      'to_email' => $this->input->post('email_to'),
      'message' => $this->input->post('message'),
      'subject' => $this->input->post('subject'),
      'user_id' => $checkLogin['id'],
      'create_email' => date('Y-m-d'),
      'email_status' => 1
    );
    $insertId = $this->common_model->addRecords('cp_emails', $data);
    if(!empty($_FILES['file_0']['name'])) {
      $config['file_name']     = time().$_FILES['file_0']['name'];
      $config['upload_path']   = 'uploads/email_attachment/';
      $config['allowed_types'] = 'gif|jpg|jpeg|png';
      $config['max_size']      = '10000';
      $config['max_width']     = '0';
      $config['max_height']    = '0';
      $config['remove_spaces'] = true;
      $this->load->library('upload', $config);
      $this->upload->initialize($config);
      if (!$this->upload->do_upload('file_0')) {
        $error = array('error' => $this->upload->display_errors());
      }
      else
      {
        $file_name  =  $this->upload->data('file_name');
        $dataarray = array(
          'attachment_file'=>$file_name,
          'email_id'=>$insertId,
          'attachment_created'=>date('Y-m-d'),
        );
        $this->common_model->addRecords('cp_emails_attachment', $dataarray);
      }
    }
    if ($insertId) {
      $email = $data['to_email'];
      $message =$data['message'];
      $subject = $data['subject'];
      sendEmail($email,$subject,$message);
              //redirect('email_list');
    }
  }
  else{
    redirect('email_list');
  }
}
public function delete_emails(){
    //print_r($_POST["ids"]);die;
  $lengths = array($_POST["ids"]);
  if(isset($lengths))
  {
   foreach($lengths as $id)
   {
    $where_condition = array('email_id' => $id);
    $upd = $this->common_model->updateRecords('cp_emails',array('email_status' => 3),$where_condition);
      //print_r($upd); die;
    if($upd){
      echo 'success';
    }
  }
}
}
public function upload_user_images(){
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    $data = array(
      'user_id' => $checkLogin['id'],
      'u_photo_status'=>'1',
      'u_photo_created' => date('Y-m-d H:i:s')
    );
    if(!empty($_FILES['imgInp']['name'])) {
     $config['upload_path'] = 'uploads/photos/';
     $config['allowed_types'] = 'jpg|jpeg|png|gif';
                  //Load upload library and initialize configuration
     $this->load->library('upload',$config);
     $this->upload->initialize($config);
     if($this->upload->do_upload('imgInp')){
       $attachment_data = array('upload_data' => $this->upload->data());
       $picture2 = $attachment_data['upload_data']['file_name'];
       /*For image resize*/
       $dirpath = $attachment_data['upload_data']['full_path'];
       $this->load->library('image_lib');
       $configer =  array(
        'image_library'   => 'gd2',
        'source_image'    =>  $dirpath,
        'maintain_ratio'  =>  TRUE,
        'width'           =>  370,
        'height'          =>  410,
      );
       $this->image_lib->clear();
       $this->image_lib->initialize($configer);
       $this->image_lib->resize();
       $data['u_photo'] = $picture2;
     }}
     $insertId = $this->common_model->addRecords('cp_user_photo', $data);
     if ($insertId) {
      $this->session->set_flashdata('susccess','Photo added successfully.');
      redirect('user_photos');
    }
  }else{
    redirect('user_photos');
  }
}
public function delete_photos(){
  if(isset($_POST["id"]))
  {
   foreach($_POST["id"] as $id)
   {
    $fieldname = $this->common_model->getsingle("cp_user_photo",array("u_photo_id" => $id));
    unlink("uploads/photos/".$fieldname->u_photo);
    $where_condition = array('u_photo_id' => $id);
    $upd = $this->common_model->deleteRecords('cp_user_photo',$where_condition);
    if($upd){
      echo 'success';
    }
  }
}
}
  // public function delete_photo(){
  //   if(isset($_POST["id"]))
  //   {
  //     $id = $_POST["id"];
  //     $where_condition = array('u_photo_id' => $id);
  //     $upd = $this->common_model->deleteRecords('cp_user_photo',$where_condition);
  //     if($upd){
  //       echo 'success';
  //     }
  //   }
  // }
public function delete_photo(){
  if(isset($_POST["id"]))
  {
   foreach($_POST["id"] as $id)
   {
    $where_condition = array('photo_id' => $id);
    $upd = $this->common_model->deleteRecords('cp_photo_favourite',$where_condition);
    if($upd){
      echo 'success';
    }
  }
}
}
public function user_photos(){
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  $user_id = $this->session->userdata('logged_in')['id'];

  if(!empty($checkLogin))
  {
    $data['user_photo_data'] = $this->common_model->getAllRecordsByIdorder('cp_user_photo',array('user_id' => $checkLogin['id']));
      //print_r($data); die;
    $data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'Photo',"user_id"=>$user_id));
    $this->template->set('title', 'Photos');
      //$this->load->view('user_photos',$data);
    $this->template->load('new_user_dashboard_layout', 'contents', 'user_photos', $data);
  }else{
    redirect('login');
  }
}
public function addphotos_favourite(){
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    if(isset($_POST["id"]))
    {
      foreach($_POST["id"] as $id)
      {
        $fav =$this->common_model->getSingleRecordById('cp_photo_favourite',array('photo_id'=> $id));
        if(empty($fav['photo_id'])){
          $data = array(
            'photo_id' => $id,
            'user_id'  => $checkLogin['id'],
            'favourite'=>'1',
            'photo_fav_created' => date('Y-m-d H:i:s')
          );
          $upd = $this->common_model->addRecords('cp_photo_favourite', $data);
          if($upd){
            echo 'success';
          }
        }else{
          $where_condition = array('photo_id' => $id);
          $upd = $this->common_model->deleteRecords('cp_photo_favourite',$where_condition);
          if($upd){
            echo 'success delete';
          }
        }
      }
    }
  }
}
public function add_Favourite_Photo(){
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    if(isset($_POST["id"]))
    {
      $id = $_POST["id"];
      $fav =$this->common_model->getSingleRecordById('cp_photo_favourite',array('photo_id'=> $id));
      if(empty($fav['photo_id'])){
        $data = array(
          'photo_id' => $id,
          'user_id'  => $checkLogin['id'],
          'favourite'=>'1',
          'photo_fav_created' => date('Y-m-d H:i:s')
        );
        $upd = $this->common_model->addRecords('cp_photo_favourite', $data);
        if($upd){
          echo 1;
        }
      }else{
        $where_condition = array('photo_id' => $id);
        $upd = $this->common_model->deleteRecords('cp_photo_favourite',$where_condition);
        if($upd){
          $total_favourite = $this->common_model->jointwotablenn('cp_user_photo', 'u_photo_id', 'cp_photo_favourite', 'photo_id',array('cp_photo_favourite.user_id' => $this->session->userdata('logged_in')['id']),'*','cp_photo_favourite.photo_id','desc');
          $newdata = '';
          if(!empty($total_favourite)){
            foreach($total_favourite as $tot_fav){
 //$fav = $this->common_model->getSingleRecordById('cp_photo_favourite',array('photo_id'=> $value['u_photo_id']));
              $newdata .= '<div class="col-md-3">
              <div class="sc_photo_list_item ">
              <div class="sc_photo_list_item_photo">
              <a href="'.base_url().'uploads/photos/'.$tot_fav->u_photo.'" class="lightbox">
              <img class="wp-post-image" alt="" src="'.base_url().'uploads/photos/'.$tot_fav->u_photo.'">
              </a>
              <label class="checkbox_con">
              <input type="checkbox" class="photo_check_box" value="'.$tot_fav->u_photo_id.'">
              <span class="checkmark"></span>
              </label>
              <div class="red box '.$tot_fav->u_photo_id.'">
              <div class="sc_team_item_hover">
              <div class="sc_team_item_socials" style="display: inline-block;">
              <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
              <div class="sc_socials_item">
              <a href="#" class="social_icons social_facebook btn_favourite" id="btn_favourite" onclick="unfavrt('.$tot_fav->photo_id.')" title="Unfavourite" >';
              if(empty($tot_fav->photo_id)){
               $newdata .= '<i class="fa fa-heart-o"></i>';
             }else{
               $newdata .= '<i id="need_unfvt'.$tot_fav->photo_id.'" class="fa fa-heart dipH" style="display: block;"></i>';
             }
             $newdata .= '</a>
             </div>
             <div class="sc_socials_item">
             <!-- <a href="#" class="social_icons social_twitter btnnew_delete" id="btnnew_delete"><i class="fa fa-trash-o"></i></a> -->
             </div>
             </div>
             </div>
             </div>
             </div>
             </div>
             </div>
             </div>';
           }
         }else{
          $newdata .= '<div class="photo-list-empty">
          <span class="blnk_photo"><i class="fa fa-picture-o" aria-hidden="true"></i></span>
          <h4>Favorite photos not found</h4>
          </div>';
        }
        $array = array("result" => '100',"newdata" => $newdata);
        echo json_encode($array);
      }
    }
  }
}
}
public function remove_Favourite_Photo($value='')
{
  # code...
}
public function photos_favourite(){
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  $user_id = $this->session->userdata('logged_in')['id'];

  if(!empty($checkLogin))
  {
    $data['user_photo_data'] = $this->common_model->getFavPhotoListorder($checkLogin['id']);
    $data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'favorite_photos',"user_id"=>$user_id));
    $this->template->set('title', 'Favourite Photos');
    $this->template->load('new_user_dashboard_layout', 'contents', 'favourite_photos', $data);
  }else{
    redirect('login');
  }
}
public function generateRandomString($length = 12) {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString;
}
public function logout(){
 $sess_array = array(
  'id' => '',
  'email' => '',
  'user_role' => '',
  'lat' => '',
  'long' => '',
);
 $this->session->unset_userdata('logged_in',$sess_array);
        //$this->session->sess_destroy();
 redirect('login');
}
/*Created by 95 for show all doctor category list start*/
public function doctor_category(){
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    $config = array();
    $config["base_url"] = base_url().'doctor_speciality/';
    $config["total_rows"] = $this->db->count_all('cp_doctor_categories');
    $config["per_page"] = 8;
    $config["uri_segment"] = 2;
    $this->pagination->initialize($config);
    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
    $data["dcategory_data"] = $this->common_model->
    getDoctorCategoryList($config["per_page"],  $page);
    $data["links"] = $this->pagination->create_links(); 
    $data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'doctors_specialties'));
    $this->template->set('title', 'Doctor Specialties');
    $this->template->load('user_dashboard_layout', 'contents', 'doctor_category_list', $data);
  }else{
    redirect('login');
  }
}
/*Created by 95 for show all doctor category list end*/
/*Created by 95 on 18-2-2019 for show doctor list by category start*/
public function view_speciality($id){
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    $category_details = $this->common_model->getSingleRecordById('cp_doctor_categories',array('dc_id'=>$id));
    $doctor_list = $this->common_model->getDoctorList1($id);
    $doctors=array();
    foreach($doctor_list as $doctor_list){
      $doctors[] = array('id'=>$doctor_list['doctor_id'],'image'=>$category_details['dc_icon'],'doctor_name'=>$doctor_list['doctor_name'],'Category_name'=>$this->getCategory($doctor_list['doctor_category_id']));
    }
    $data['alldoctors']=$doctors;
    $this->template->set('title', 'All Doctors ');
    $this->template->load('user_dashboard_layout', 'contents', 'viewdoctor_list', $data);
  }else{ 
    redirect('login');
  }
    // $data['menuactive'] = $this->uri->segment(1);
    // $checkLogin = $this->session->userdata('logged_in');
    // if(!empty($checkLogin))
    // {
    //   $data['dc_detail'] = $this->common_model->getDoctorList($id);
    //   $this->template->set('title', 'Favourite Photos');
    //   $this->template->load('user_dashboard_layout', 'contents', 'viewdoctor_list', $data);
    // }else{
    //     redirect('login');
    // }
}
/*Created by 95 on 18-2-2019 for show doctor list by category end*/
public function getCategory($id){
 $id = explode(',',$id);
 $this->db->select('dc_name');
 $this->db->from('cp_doctor_categories');
 $this->db->where_in('dc_id',$id);
 $query = $this->db->get();
 $result = $query->result_array();
 $last_names1 = array_column($result, 'dc_name');
 $result_data = implode(',',$last_names1);
 return $result_data;
}
/*Created by 95 on 28-2-2019 for show the doctor detail and make an appointment functionality*/
public function view_doctor($doctor_id){
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
   $data['time_slot'] = $this->common_model_new->jointwotablegroupby_order('dr_available_date', 'avdate_id', 'dr_available_time', 'avtime_date_id','dr_available_time.avtime_day_slot',array('dr_available_date.avdate_dr_id' => $doctor_id,'dr_available_date.avdate_date' => date('Y-m-d')),'*','avtime_day_slot_id','asc');
   if(isset($_GET['submit'])){
    $avail_time = $this->input->get('avail_time');
    $available_date = $this->common_model_new->getsingle('dr_available_date',array('avdate_dr_id'=>$doctor_id,'avdate_date'=>$this->input->get('reminder_date')));
    if(!empty($available_date)){
      $avdate_id = $available_date->avdate_id;
      $avtime_slot = $this->common_model_new->getsingle('dr_available_time',array('avtime_date_id'=>$avdate_id,'avtime_id'=>$avail_time));
      if(!empty($checkLogin)){
        $user_id = $checkLogin['id'];
      }
      if(!empty($avtime_slot->avtime_id)){
        $avtime_id =  $avtime_slot->avtime_id;
        if($this->input->get('remind')){
          $remind  = $this->input->get('remind');
        }else{
         $remind=0;
       }
       $check_appointment = $this->common_model_new->jointwotable("doctor_appointments", "doctor_id", "dr_available_date", "avdate_dr_id",array("doctor_appointments.doctor_id" => $doctor_id,"dr_available_date.avdate_date" => $this->input->get('reminder_date'),"doctor_appointments.dr_appointment_time_id" => $avail_time),"*");     
       //echo $this->db->last_query();die;
       $time_check =  $this->common_model_new->getsingle("dr_available_time",array("avtime_id" => $this->input->get('avail_time')));
       $new_appint = $this->common_model_new->getsingle("doctor_appointments",array("doctor_id" => $doctor_id,"dr_appointment_date_id" => $check_appointment->dr_appointment_date_id,"dr_appointment_time_id" => $time_check->avtime_id));
      //echo $this->db->last_query();die;
       if(empty($new_appint)){
        $data=array('doctor_id'=>$doctor_id,'user_id'=>$user_id,'dr_appointment_date_id'=>$avdate_id,'dr_appointment_time_id'=>$avail_time,'appointments_reminder'=>$remind);
        $appointment_id = $this->common_model_new->insertData('doctor_appointments',$data);
        //doctor detail
        $docDetail = $this->common_model_new->getsingle('cp_doctor',array('doctor_id'=>$doctor_id));
        // user deetail
        $userDetail = $this->common_model_new->getsingle('cp_users',array('id'=>$user_id));
        $name= $userDetail->name;
        $lastname =$userDetail->lastname;
        $usrfullname= $name.' '.$lastname;
        $usremail= $userDetail->email;
        $usrmobile=$userDetail->mobile;
        // echo $usrfullname;
        // echo "<br>";
        // echo $usremail;
        // die;
        //Appointment detail
        $appointment_data = $this->common_model_new->getAppointmentDetail($appointment_id);
        //$appointment_data[0]->doctor_id 
        $apointDate= date('d-m-Y',strtotime($appointment_data[0]->avdate_date));
        $apointTime = $appointment_data[0]->avtime_text;
        $doctorEmail = $docDetail->doctor_email;
        $doctorName = $docDetail->doctor_name;
        $TemplateData = $this->common_model_new->getsingle('mail_template',array('template_id'=>6));
        $findArray=array("%Drname%","%Date%","%Time%","%name%","%email%","%Mobile%");
        $replaaceArray=array($doctorName,$apointDate,$apointTime,$usrfullname,$usremail,$usrmobile);
        $description= str_replace($findArray,$replaaceArray,$TemplateData->description);
        $message = $description;
          //$message = 'Dear '.$firstname.' '.$lastname.',<br><p>Welcome to our carepro app.</p><br><p>Please open tablet and please do further process and enjoy our services.</p><br><p>Best Wishes,</p><br><p>CarePro Team</p>';
        $subject = $TemplateData->subject;
        $check = new_send_mail($message, $subject, $doctorEmail, '');
        // echo $check; 
        // die;
        if($remind==1){
         $NotiData=array(
          'common_id'=>$appointment_id,
          'user_id'=>$user_id,
          'is_read'=>0,
          'type'=>3,
          'notification_date'=>date('Y-m-d H:i:s'),
        );
         $this->common_model_new->insertData('cp_common_notification',$NotiData);
       }
     }else{
      $this->session->set_flashdata('failed','This appointment time is already scheduled by a user, please select any other.');
    }
    if($appointment_id){
     redirect('thank_you?appointment='.base64_encode($appointment_id));
   }
 }else{
  $this->session->set_flashdata('failed','Appointment time not available.');
}
}else{
  $this->session->set_flashdata('failed','Appointment date not available.');
}
}
$Doctor_dates = $this->common_model_new->getAllDoctorappointmets("dr_available_date",array("avdate_dr_id" => $doctor_id),"avdate_date","avdate_date");
foreach ($Doctor_dates as $value) {
 $dates[]= array('date'=>$value['avdate_date']);
}
$data['doctorDates']= $dates;
$data['dc_detail'] = $this->common_model_new->getsingle('cp_doctor',array('doctor_id'=>$doctor_id));
$this->template->set('title', 'Doctor Detail');
$this->template->load('user_dashboard_layout_activities', 'contents', 'view_doctor',$data);
}else{
  redirect('login');
}
}
// public function view_doctor($doctor_id){
//       $data['menuactive'] = $this->uri->segment(1);
//       $checkLogin = $this->session->userdata('logged_in');
//       if(!empty($checkLogin))
//       {
//        $data['time_slot'] = $this->common_model_new->jointwotablegroupby_order('dr_available_date', 'avdate_id', 'dr_available_time', 'avtime_date_id','dr_available_time.avtime_day_slot',array('dr_available_date.avdate_dr_id' => $doctor_id,'dr_available_date.avdate_date' => date('Y-m-d')),'*','avtime_day_slot_id','asc');
//        if(isset($_GET['submit'])){
//         $avail_time = $this->input->get('avail_time');
//         $available_date = $this->common_model_new->getsingle('dr_available_date',array('avdate_dr_id'=>$doctor_id,'avdate_date'=>$this->input->get('reminder_date')));
//         if(!empty($available_date)){
//           $avdate_id = $available_date->avdate_id;
//           $avtime_slot = $this->common_model_new->getsingle('dr_available_time',array('avtime_date_id'=>$avdate_id,'avtime_id'=>$avail_time));
//           if(!empty($checkLogin)){
//             $user_id = $checkLogin['id'];
//           }
//           if(!empty($avtime_slot->avtime_id)){
//             $avtime_id =  $avtime_slot->avtime_id;
//             if($this->input->get('remind')){
//               $remind  = $this->input->get('remind');
//             }else{
//              $remind=0;
//            }
//            $check_appointment = $this->common_model_new->jointwotable("doctor_appointments", "doctor_id", "dr_available_date", "avdate_dr_id",array("doctor_appointments.doctor_id" => $doctor_id,"dr_available_date.avdate_date" => $this->input->get('reminder_date')),"*");
//        //echo $this->db->last_query();die;
//            $time_check =  $this->common_model_new->getsingle("dr_available_time",array("avtime_id" => $this->input->get('avail_time')));
//            $new_appint = $this->common_model_new->getsingle("doctor_appointments",array("doctor_id" => $doctor_id,"dr_appointment_date_id" => $check_appointment->dr_appointment_date_id,"dr_appointment_time_id" => $time_check->avtime_id));
//       //echo $this->db->last_query();die;
//            if(empty($new_appint)){
//             $data=array('doctor_id'=>$doctor_id,'user_id'=>$user_id,'dr_appointment_date_id'=>$avdate_id,'dr_appointment_time_id'=>$avail_time,'appointments_reminder'=>$remind);
//             $appointment_id = $this->common_model_new->insertData('doctor_appointments',$data);
//           }else{
//             $this->session->set_flashdata('failed','Sorry, this appointment time is taken by another user. Please select
//               other time.');
//           }
//           if($appointment_id){
//            redirect('thank_you?appointment='.base64_encode($appointment_id));
//          }
//        }else{
//         $this->session->set_flashdata('failed','Appointment time not available.');
//       }
//     }else{
//       $this->session->set_flashdata('failed','Appointment date not available.');
//     }
//   }
//   $Doctor_dates = $this->common_model_new->getAllDoctorappointmets("dr_available_date",array("avdate_dr_id" => $doctor_id),"avdate_date","avdate_date");
//   foreach ($Doctor_dates as $value) {
//    $dates[]= array('date'=>$value['avdate_date']);
//  }
//  $data['doctorDates']= $dates;
//  $data['dc_detail'] = $this->common_model_new->getsingle('cp_doctor',array('doctor_id'=>$doctor_id));
//  $this->template->set('title', 'Doctor Detail');
//  $this->template->load('user_dashboard_layout_activities', 'contents', 'view_doctor',$data);
// }else{
//   redirect('login');
// }
// }
  //    public function view_doctor($doctor_id){
  //     $data['menuactive'] = $this->uri->segment(1);
  //     $checkLogin = $this->session->userdata('logged_in');
  //     if(!empty($checkLogin))
  //     {
  //       if(isset($_POST['submit'])){
  //         $available_date = $this->common_model_new->getsingle('dr_available_date',array('avdate_dr_id'=>$doctor_id,'avdate_date'=>$this->input->post('reminder_date')));
  //         if(!empty($available_date)){
  //           $avdate_id = $available_date->avdate_id;
  //           $avtime_slot = $this->common_model_new->getsingle('dr_available_time',array('avtime_date_id'=>$avdate_id,'avtime_day_slot'=>$this->input->post('selector')));
  //           if(!empty($checkLogin)){
  //             $user_id = $checkLogin['id'];
  //           }
  //           $avtime_id =  $avtime_slot->avtime_id;
  //           $data=array('doctor_id'=>$doctor_id,'user_id'=>$user_id,'dr_appointment_date_id'=>$avdate_id,'dr_appointment_time_id'=>$avtime_id);
  //           $appointment_id = $this->common_model_new->insertData('doctor_appointments',$data);
  //           if($appointment_id){
  //            redirect('thank_you?appointment='.base64_encode($appointment_id));
  //          }
  //        }else{
  //         $this->session->set_flashdata('failed','Appointment Not available.');
  //       }
  //     }
  //     $data['dc_detail'] = $this->common_model_new->getsingle('cp_doctor',array('doctor_id'=>$doctor_id));
  //     $this->template->set('title', 'Doctor Detail');
  //     $this->template->load('user_dashboard_layout', 'contents', 'view_doctor',$data);
  //   }else{
  //     redirect('login');
  //   }
  // }
/*Created by 95 on 28-2-2019 for show the appointment detail oon thank you page*/
public function thank_you(){
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    $detail = $this->input->get('appointment');
    $appointment_id = base64_decode($detail);
    $appointment_data = $this->common_model_new->getAppointmentDetail($appointment_id);
   // echo '<pre>';print_r($appointment_data[0]->doctor_id);die;
    $data['AppointmentData']  = $appointment_data;
    $this->template->set('title', 'Thank You');
    $this->template->load('user_dashboard_layout', 'contents', 'thank_you',$data);
  }else{
    redirect('login');
  }
}
public function search_doctor_category()
{
 $cat_id = $this->input->post("category_id");
 $doctor_category = $this->input->post("doctor_name");
 $alldoctors = $this->common_model_new->getsearchdata($doctor_category,$cat_id);
 $category_image = $this->common_model_new->getsingle('cp_doctor_categories',array('dc_id' => $cat_id));
    //echo $this->db->last_query();die;
 foreach($alldoctors as $all_doc){
   $category_data = $this->common_model_new->getCategory1($all_doc['doctor_category_id']);
   $array[] = array(
    'doctor_id' => $all_doc['doctor_id'],
    'doctor_name' => $all_doc['doctor_name'],
    'doctor_category' => $category_data,
    'category_image' => $category_image->dc_icon,
  );
 }
 $cat_all = $array;
 $list = $this->load->view('ajax_category_search_data', array('category_search' => $cat_all), true);
 $this->output->set_content_type('application/json');
 $return = array('success' => true, 'list' => $list);
 echo json_encode($return);
}
public function detail_article()
{
 $data['menuactive'] = $this->uri->segment(1);
 $checkLogin = $this->session->userdata('logged_in');
 if(!empty($checkLogin))
 {
  $segment = base64_decode($this->uri->segment("2"));
  $data['detail_article'] = $this->common_model_new->getsingle("cp_articles",array("article_id" => $segment));
  $data['check_article'] = $this->common_model_new->getsingle("cp_article_favourite",array("article_id" => $segment,"user_id" => $checkLogin['id']));
  $data['popular_article'] = $this->common_model_new->getAllorlimittrash('cp_articles','article_id','desc','5');
  $this->template->set('title', 'Detail Article');
  $this->template->load('new_user_dashboard_layout', 'contents', 'detail_article',$data);
}else{
  redirect('login');
}
}
public function favourite_article()
{
 $article_id = base64_decode($this->input->post('article_id'));
 $checkLogin = $this->session->userdata('logged_in');
 $check_article = $this->common_model_new->getsingle("cp_article_favourite",array("article_id" => $article_id,"user_id" => $checkLogin['id']));
 if(!empty($check_article)){
  $this->common_model_new->delete("cp_article_favourite",array("article_id" => $article_id,"user_id" => $checkLogin['id']));
  echo '0';
}else{
  $array = array("user_id" => $checkLogin["id"],"article_id" => $article_id,"fav" => 1,"article_fav_created" => date("Y-m-d H:i:s"));
  $this->common_model_new->insertData("cp_article_favourite",$array);
  echo '1';
}
}
public function article_list()
{
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    //$where = "deleted_user_id !=".$checkLogin['id'];
    $data['article_list'] = $this->common_model_new->getAllor("cp_articles","article_id","desc");
    $this->template->set('title', 'Article');
    $this->template->load('user_dashboard_layout', 'contents', 'article_list',$data);
  }else{
    redirect('login');
  }
}
public function delete_article()
{
  $segment = $this->uri->segment("2");
  $checkLogin = $this->session->userdata('logged_in');
     //$this->common_model_new->updateData("cp_articles",array("deleted_user_id" => $checkLogin['id']),array("article_id" => $segment));
  $this->common_model_new->insertData('cp_article_trash',array('user_id' => $checkLogin['id'],'article_id' => $segment,'article_trash_date' => date('Y-m-d H:i:s')));
  $this->session->set_flashdata('success', 'Successfuly deleted');
  redirect("articles");
}
public function delete_checkboxes_article()
{
  $bodytype = $this->input->post("bodytype");
  $session_id = $this->input->post("session_id");
  $explode = explode(",",$bodytype);
  foreach($explode as $exp){
    $this->common_model_new->insertData('cp_article_trash',array('user_id' => $session_id,'article_id' => $exp,'article_trash_date' => date('Y-m-d H:i:s')));
    // echo $this->db->last_query();
  }
  echo '1';
}
public function view_slots()
{
 $date = $this->input->post('date');
 $doctor_id = $this->input->post('doctor_id');
 $checkLogin = $this->session->userdata('logged_in');
 $time_slot = $this->common_model_new->jointwotablegroupby_order('dr_available_date', 'avdate_id', 'dr_available_time', 'avtime_date_id','dr_available_time.avtime_day_slot',array('dr_available_date.avdate_dr_id' => $doctor_id,'dr_available_date.avdate_date' => $date),'*','avtime_day_slot_id','asc');
 if(!empty($time_slot)){
  $data = '<div class="tm-team-list-title"><i class="fa fa-clock-o"></i>Available Time <span style="color: red;">*</span>:</div>
  <div class="tm-team-list-value">
  <ul class="list_time">';
    // if(empty($time_check) && empty($check_appointment)){
  if(!empty($class="new_selector")){
    foreach($time_slot as $slot){
     $data .= '<li>
     <input type="radio" class="timeslots" id="f-option_'.$slot->avtime_id.'" data-slot="'.$slot->avtime_day_slot.'" name="selector" value="'.$slot->avtime_day_slot.'">
     <label for="f-option_'.$slot->avtime_id.'">'.$slot->avtime_day_slot.'</label>
     <div class="check"></div>
     </li>';
   }}
   $data .= '</ul>
   </div>';
 }else{
   $data .= '<div class="no-record">No date available on this time</div>';
 }
 echo $data;
}
public function view_timeslot()
{
 $slot_id = $this->input->post("slot_id");
 $doctor_id = $this->input->post("doctor_id");
 $date_id = $this->input->post("date_id");
 $dropdown = $this->common_model_new->getsingle("dr_available_date",array("avdate_dr_id" => $doctor_id,"avdate_date" => $date_id));
 $newtime = $this->common_model_new->getAllwhere("dr_available_time",array("avtime_date_id" => $dropdown->avdate_id,"avtime_day_slot" => $slot_id));
 $data = "<div class='tm-team-list-title'> </div>
 <div class='tm-team-list-value dat'>
 <div class='bootstrap-timepicker'>
 <div class='input-group date' id='datetimepickerhs'>";
 if(!empty($newtime)){
  $data .= "<select name='avail_time' class='form-control newAv'>";
  foreach($newtime as $new_avail_time){
    $data .= "<option value=".$new_avail_time->avtime_id.">".$new_avail_time->avtime_text."</option>";
  }
  $data .= "</select>";
}else{
  $data .= "Not available time for this date";
}
$data .= '</div></div></div>';
echo $data;
}
public function search_music_list()
{
 $music_title = $this->input->post('music_title');
 $data['page'] = 0;
 $pagecon['per_page'] = 20;
 $allmusic = $this->common_model_new->getsearchdatamusic($music_title,$pagecon['per_page'],$data['page']);
 $data['music_list'] = $allmusic['rows'];
 $data['count_total'] = $allmusic['num_rows'];
 $this->load->view("ajax_search_music",$data);
}
public function search_movie_list()
{
  $movie_title = $this->input->post('movie_title');
  $data['page'] = 0;
  $pagecon['per_page'] = 20;
  $allmovie = $this->common_model_new->getsearchdatamovie($movie_title,$pagecon['per_page'],$data['page']);
  $data['movie_list'] = $allmovie['rows'];
  $data['count_total'] = $allmovie['num_rows'];
  $this->load->view("ajax_search_movie",$data);
}
public function contact_detail()
{
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    $this->template->set('title', 'Contact Detail');
    $this->template->load('user_dashboard_layout', 'contents', 'contact_detail', $data);
  }else{
    redirect('login');
  }
}
public function update_profile()
{
 $data = array();
 $data['menuactive'] = $this->uri->segment(1);
 $checkLogin = $this->session->userdata('logged_in');
 $data['singleData'] = $this->common_model_new->getsingle('cp_users',array('id' => $checkLogin['id']));
 if(isset($_POST['submit'])){
   if ($_FILES['profile_image']['error'] == 0) {
    $config2['upload_path'] = './././uploads/profile_images/';
    $config2['allowed_types'] = 'jpg|png|gif|jpeg';
    $config2['max_size'] = '5000';
    $config2['encrypt_name'] = true;
    $this->upload->initialize($config2);

    if (!empty($_FILES["profile_image"]["name"])) {
      if ($this->upload->do_upload('profile_image')) {
        $attachment_data_image = array('upload_data' => $this->upload->data());
        $picturefile = $attachment_data_image['upload_data']['file_name'];
        /*For image resize*/
        $this->load->library('image_lib');
        $configer =  array(
          'image_library'   => 'gd2',
          'source_image'    =>  $attachment_data_image['upload_data']['full_path'],
          'maintain_ratio'  =>  TRUE,
          'width'           =>  150,
          'height'          =>  150,
        );
        $this->image_lib->clear();
        $this->image_lib->initialize($configer);
        $this->image_lib->resize();
      } 
    }
  if($_FILES['profile_image']['name']){
    $image = $picturefile;
  }else{
    $image = $data['singleData']->profile_image;
  }
  $array = array(
    'mobile' => $_POST['mobile'],
    'name' => $_POST['name_user'],
    'profile_image' => $image,
  );
  // _dx($array);
  $this->common_model_new->updateData('cp_users',$array,array("id" => $checkLogin['id']));
  $this->session->set_flashdata('success', 'Successfully Updated');
  redirect('user/update_profile');
}
 }
$data['paymentDetails'] = $this->common_model_new->getAllwhere("subscription_payment_transactions",array("user_id" => 737));
$data['plans'] = $this->common_model_new->getAllwhere("subscription_plan",array("status" =>1));
$this->template->set('title', 'Home');
$this->template->load('new_user_dashboard_layout', 'contents', 'update_profile', $data);

}
public function change_password()
{
 $data['menuactive'] = $this->uri->segment(1);
 if($_POST['change_password']){
  $checkLogin = $this->session->userdata('logged_in');
  $old_password = $_POST['old_password'];
  $password = $_POST['new_password'];
  $check_password = $this->common_model_new->getsingle("cp_users",array("id" => $checkLogin['id']));
  //echo $this->db->last_query();die;
    //echo $check_password->password;die;
  if(md5(@$old_password) == $check_password->password){
    $where = array('id' => $checkLogin['id'],'password'=>md5(@$old_password));
    $update = $this->common_model_new->updateData('cp_users', array('password' => md5(@$password)), $where);
    $this->session->set_flashdata('success','Password updated successfully');
    redirect("user/update_profile");
  }else{
    $this->session->set_flashdata('danger','Please old password must be correct');
    redirect("user/update_profile");
  }
}
}
public function delete_favorite_article()
{
 $bodytype = $this->input->post("bodytype");
 $session_id = $this->input->post("session_id");
 $explode = explode(",",$bodytype);
 foreach($explode as $exp){
  $this->common_model_new->insertData('cp_article_trash',array('user_id' => $session_id,'article_id' => $exp,'article_trash_date' => date('Y-m-d H:i:s')));
    // echo $this->db->last_query();
}
echo '1';
}
public function favarticle_list()
{
 $data['menuactive'] = $this->uri->segment(1);
 $checkLogin = $this->session->userdata('logged_in');
 if(!empty($checkLogin))
 {
    //$where = "deleted_user_id !=".$checkLogin['id'];
  $data['article_list'] = $this->common_model_new->getAllFavoriteArticls($this->session->userdata('logged_in')['id']);
  $this->template->set('title', 'Favorite Article');
  $this->template->load('user_dashboard_layout', 'contents', 'favarticle_list',$data);
}
}
public function articles()
{
 $data['menuactive'] = $this->uri->segment(1);
 $checkLogin = $this->session->userdata('logged_in');
 if(!empty($checkLogin))
 {
   $data['page'] = 0;
   $pagecon['per_page'] = 20;
   $article_list1 = $this->Common_model_new->article_listing($pagecon['per_page'],$data['page']);
    //echo '<pre>';print_r($article_list1);die;
   if (!empty($article_list1['rows'])) {
    $data['article_list'] = $article_list1['rows'];
    $data['count_total'] = $article_list1['num_rows'];
  } else {
    $data['article_list'] = '';
    $data['count_total'] = '';
  }
  $this->template->set('title', 'Articles');
  $this->template->load('user_dashboard_layout', 'contents', 'article',$data);
}else{
  redirect('login');
}
}
public function get_article_offset()
{
 $data['offset'] = $this->input->post('offset');
 $like_val = $this->input->post('like_val');
 if(!empty($like_val)){
  $pagecon['per_page'] = 20;
  $allarticle = $this->Common_model_new->getsearchdataarticle($like_val,$pagecon['per_page'],$data['offset']);
  $data['article_list'] = $allarticle['rows'];
  $data['count_total'] = $allarticle['num_rows'];
}else{
 $pagecon['per_page'] = 20;
 $allarticle = $this->Common_model_new->article_listing($pagecon['per_page'],$data['offset']);
    //echo '<pre>';print_r($music_list1);die;
 if (!empty($allarticle['rows'])) {
  $data['article_list'] = $allarticle['rows'];
  $data['count_total'] = $allarticle['num_rows'];
} else {
  $data['article_list'] = '';
  $data['count_total'] = '';
}
}
$this->load->view('ajax_value_article', $data);
}
public function search_article_list()
{
 $article_title = $this->input->post('article_title');
 $data['page'] = 0;
 $pagecon['per_page'] = 20;
 $allarticle = $this->Common_model_new->getsearchdataarticle($article_title,$pagecon['per_page'],$data['page']);
  //echo '<pre>';print_r($allarticle);die;
 $data['article_list'] = $allarticle['rows'];
 $data['count_total'] = $allarticle['num_rows'];
 $this->load->view("ajax_search_article",$data);
}
public function favorite_articles()
{
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
   $data['page'] = 0;
   $pagecon['per_page'] = 20;
   $article_list1 = $this->Common_model_new->getfavoritearticlelist($pagecon['per_page'],$data['page']);
    //echo '<pre>';print_r($article_list1);die;
   if (!empty($article_list1['rows'])) {
    $data['article_list'] = $article_list1['rows'];
    $data['count_total'] = $article_list1['num_rows'];
  } else {
    $data['article_list'] = '';
    $data['count_total'] = '';
  }
  $this->template->set('title', 'Articles');
  $this->template->load('user_dashboard_layout', 'contents', 'favarticle',$data);
}else{
  redirect('login');
}
}
public function get_favarticle_offset()
{
  $data['offset'] = $this->input->post('offset');
  $like_val = $this->input->post('like_val');
  if(!empty($like_val)){
    $pagecon['per_page'] = 20;
    $allarticle = $this->Common_model_new->getsearchfavdataarticle($like_val,$pagecon['per_page'],$data['offset']);
    $data['article_list'] = $allarticle['rows'];
    $data['count_total'] = $allarticle['num_rows'];
  }else{
   $pagecon['per_page'] = 20;
   $allarticle = $this->Common_model_new->getfavoritearticlelist($pagecon['per_page'],$data['offset']);
    //echo '<pre>';print_r($music_list1);die;
   if (!empty($allarticle['rows'])) {
    $data['article_list'] = $allarticle['rows'];
    $data['count_total'] = $allarticle['num_rows'];
  } else {
    $data['article_list'] = '';
    $data['count_total'] = '';
  }
}
$this->load->view('ajax_favorite_article_value', $data);
}
public function search_favarticle_list()
{
  $like_val = $this->input->post('article_title');
  $checkLogin = $this->session->userdata('logged_in');
  $data['page'] = 0;
  $pagecon['per_page'] = 20;
  $article_list1 = $this->Common_model_new->getsearchfavdataarticle($like_val,$pagecon['per_page'],$data['page']);
    //echo '<pre>';print_r($article_list1);die;
  if (!empty($article_list1['rows'])) {
    $data['article_list'] = $article_list1['rows'];
    $data['count_total'] = $article_list1['num_rows'];
  } else {
    $data['article_list'] = '';
    $data['count_total'] = '';
  }
  $this->load->view("ajax_favorite_search_article_value",$data);
}
public function detail_article_fav()
{
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    $segment = $this->uri->segment("2");
    $data['detail_article'] = $this->common_model_new->getsingle("cp_articles",array("article_id" => $segment));
    $data['check_article'] = $this->common_model_new->getsingle("cp_article_favourite",array("article_id" => $segment,"user_id" => $checkLogin['id']));
    $data['popular_article'] = $this->common_model_new->getAllorlimittrash('cp_articles','article_id','desc','5');
    $this->template->set('title', 'Detail Article');
    $this->template->load('user_dashboard_layout', 'contents', 'detail_article_fav',$data);
  }else{
    redirect('login');
  }
}
public function nearby_restaurant()
{
 $data['menuactivew'] = $this->uri->segment(1);
 $lat = $this->session->userdata('lat');
 $long = $this->session->userdata('long');
  // print_r($getlocation);die;
 $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=5000&type=restaurant&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
 $restaurant = "restaurant";
 $ch = curl_init($url);
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$restaurant");
 curl_setopt($ch, CURLOPT_POST, 1);
 $headers = array();
 $headers[] = "CAREPRO-AUTH-KEY: 12345";
 $headers[] = "Content-Type: application/x-www-form-urlencoded";
 curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
 $result = curl_exec($ch);
 if (curl_errno($ch)) {
 }
 curl_close ($ch);
    //echo "<pre>";print_r($result);  die;
 $j_decoded = json_decode($result);
 foreach($j_decoded->results as $rest){
  $check_restaurant = $this->Common_model_new->getsingle('cp_restaurant',array('user_id' => $this->session->userdata('logged_in')['id'],'place_id' => $rest->place_id));
  if(empty($check_restaurant)){
    foreach($rest->photos as $photo){
      $image = $photo->photo_reference;
      $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyCrVGd7PYWdG6NMU-zXRvowyPY1qXr2mjQ";
      if($rest->icon){
        $icon = $rest->icon;
        $icon_path = "https://maps.gstatic.com/mapfiles/place_api/icons/$icon";
      }else{
        $icon_path = '';
      }
    }
    if(!empty($rest->rating)){
      $rat = $rest->rating;
    }else{
      $rat = '';
    }
    $array = array(
     'rest_name' => $rest->name,
     'rest_icon' =>  $icon_path,
     'rating' => $rat,
     'place_id' => $rest->place_id,
     'user_id' => $this->session->userdata('logged_in')['id'],
     'rest_address' => $rest->vicinity,
     'rest_lat' => $rest->geometry->location->lat,
     'rest_long' => $rest->geometry->location->lng,
     'rest_image' => $image_path,
     'rest_created' => date('Y-m-d H:i:s')
   );
    $this->Common_model_new->insertData('cp_restaurant',$array);
  }
}
$per_page = '20';
$offset = 0;
$nearbyrestaurants = $this->Common_model_new->nearbyrest_listing($per_page,$offset);
$data['nearbyrestaurants'] = $nearbyrestaurants['rows'];
$data['count_total'] = $nearbyrestaurants['num_rows'];
$this->template->set('title', 'Near by Restaurants');
$this->template->load('user_dashboard_layout', 'contents', 'nearby_restaurant',$data);
}
/*view single bank*/
public function view_restaurant()
{
  $data['menuactivew'] = $this->uri->segment(1);
  $radius = $this->uri->segment('3');
  $segment = base64_decode($this->uri->segment('2'));
  $la = $_GET['lat'];
  $lo = $_GET['long'];
  $lat = str_replace("%","",$la);
  $long = str_replace("%","",$lo);
  $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=restaurant&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
  $value = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$segment&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
  $restaurant = "restaurant";
  $ch = curl_init($value);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$restaurant");
  curl_setopt($ch, CURLOPT_POST, 1);
  $headers = array();
  $headers[] = "CAREPRO-AUTH-KEY: 12345";
  $headers[] = "Content-Type: application/x-www-form-urlencoded";
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  $result = curl_exec($ch);
  if (curl_errno($ch)) {
  }
  curl_close ($ch);
    //echo "<pre>";print_r($result);  die;
  $j_decoded = json_decode($result);
    //echo "<pre>";print_r($j_decoded->result); die;
  if(isset($j_decoded->result->photos[0]->photo_reference)){
   $image =  $j_decoded->result->photos[0]->photo_reference;
 }else{
   $image="";
 }
 if(!empty($image)){
   $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
 }else{
   $image_path='';
 }
 if(!empty($j_decoded->result->rating)){
   $rat = $j_decoded->result->rating;
 }else{
   $rat = '';
 }
 $array = array(
   'rest_name' => $j_decoded->result->name,
   'rating' => $rat,
   'place_id' => $j_decoded->result->place_id,
   'user_id' => $this->session->userdata('logged_in')['id'],
   'rest_address' => $j_decoded->result->formatted_address,
   'rest_lat' => $j_decoded->result->geometry->location->lat,
   'rest_long' => $j_decoded->result->geometry->location->lng,
   'rest_image' => $image_path,
   'rest_created' => date('Y-m-d H:i:s')
 );
 $data['view_restaurant'] = $array;
  //echo '<pre>';print_r($array);die;
 $this->template->set('title', 'Restaurants');
 $this->template->load('user_dashboard_layout', 'contents', 'view_restaurant',$data);
}
/*view single favorite restaurant */
public function view_favorite_restaurant()
{
  $data['menuactivew'] = $this->uri->segment(1);
  $segment = $this->uri->segment('2');
          //$data['view_restaurant'] = $this->common_model_new->getsingle("cp_restaurant",array("rest_id" => $segment));
  $data['view_restaurant'] = $this->common_model_new->getsingle("cp_fav_restaurant",array("fav_rest_id" => $segment));
  $this->template->set('title', 'Restaurants');
  $this->template->load('user_dashboard_layout', 'contents', 'view_favorite_restaurant',$data);
}
function get_ajax_offset()
{
 $data['offset'] = $this->input->post('offset');
 $like_val = $this->input->post('like_val');
 if(!empty($like_val)){
  $pagecon['per_page'] = 20;
  $allarticle = $this->Common_model_new->getsearchdatarestaurant($like_val,$pagecon['per_page'],$data['offset']);
  if (!empty($allarticle['rows'])) {
    $data['nearbyrestaurants'] = $allarticle['rows'];
    $data['count_total'] = $allarticle['num_rows'];
  } else {
    $data['nearbyrestaurants'] = '';
    $data['count_total'] = '';
  }
}else{
 $pagecon['per_page'] = 20;
 $allarticle = $this->Common_model_new->nearbyrest_listing($pagecon['per_page'],$data['offset']);
    //echo '<pre>';print_r($music_list1);die;
 if (!empty($allarticle['rows'])) {
  $data['nearbyrestaurants'] = $allarticle['rows'];
  $data['count_total'] = $allarticle['num_rows'];
} else {
  $data['nearbyrestaurants'] = '';
  $data['count_total'] = '';
}
}
$this->load->view('ajax_value_restaurant', $data);
}
public function search_restaurant_list()
{
 $restaurant_title = $this->input->post('restaurant_title');
 $data['page'] = 0;
 $pagecon['per_page'] = 20;
 $allmusic = $this->Common_model_new->getsearchdatarestaurant($restaurant_title,$pagecon['per_page'],$data['page']);
 $data['nearbyrestaurants'] = $allmusic['rows'];
 $data['count_total'] = $allmusic['num_rows'];
 $this->load->view("ajax_new_search_value",$data);
}
function session_value()
{
  $lat = $_POST['lat'];
  $long = $_POST['longt'];
  $this->session->set_userdata("lat",$lat);
  $this->session->set_userdata("long",$long);
}
public function favorite_restaurant()
{
  $data['menuactivew'] = $this->uri->segment(1);
  $restaurant_id = $this->input->post('restaurant_id');
  //$restaurant_data = $this->common_model_new->getsingle("cp_restaurant",array("user_id" => $this->session->userdata('logged_in')['id'],"rest_id" => $restaurant_id));
  $value = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$restaurant_id&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
  $restaurant = "restaurant";
  $ch = curl_init($value);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$restaurant");
  curl_setopt($ch, CURLOPT_POST, 1);
  $headers = array();
  $headers[] = "CAREPRO-AUTH-KEY: 12345";
  $headers[] = "Content-Type: application/x-www-form-urlencoded";
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  $result = curl_exec($ch);
  if (curl_errno($ch)) {
  }
  curl_close ($ch);
    //echo "<pre>";print_r($result);  die;
  $j_decoded = json_decode($result);
  $check_restaurant = $this->common_model_new->getsingle("cp_fav_restaurant",array("fav_rest_place_id" => $restaurant_id,"user_id" => $this->session->userdata('logged_in')['id']));
  if(!empty($check_restaurant)){
   $this->common_model_new->delete("cp_fav_restaurant",array("fav_rest_place_id" => $restaurant_id,"user_id" => $this->session->userdata('logged_in')['id']));
   echo '0';
 }else{
   if(isset($j_decoded->result->photos[0]->photo_reference)){
     $image =  $j_decoded->result->photos[0]->photo_reference;
   }else{
     $image="";
   }
   if(!empty($image)){
     $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
   }else{
     $image_path='';
   }
   if(!empty($j_decoded->result->rating)){
     $rat = $j_decoded->result->rating;
   }else{
     $rat = '';
   }
   $array = array(
    'fav_rest_name' => $j_decoded->result->name,
    'fav_rest_image' => $image_path,
    'fav_rest_rating' => $rat,
      //'fav_rest_lat' => $restaurant_data->rest_lat,
      //'fav_rest_long' => $restaurant_data->rest_long,
    'fav_rest_address' => $j_decoded->result->formatted_address,
    'fav_rest_place_id' => $restaurant_id,
    'user_id' => $this->session->userdata('logged_in')['id'],
    'fav_rest_created' => date('Y-m-d H:i:s')
  );
   $this->common_model_new->insertData("cp_fav_restaurant",$array);
   echo '1';
 }
}
public function favorite_restaurant_list()
{
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
   $data['page'] = 0;
   $pagecon['per_page'] = 20;
   $favorite_restaurant1 = $this->Common_model_new->getfavoriterestaurantlist($pagecon['per_page'],$data['page']);
    //echo '<pre>';print_r($favorite_restaurant1);die;
   if (!empty($favorite_restaurant1['rows'])) {
    $data['favorite_restaurant'] = $favorite_restaurant1['rows'];
    $data['count_total'] = $favorite_restaurant1['num_rows'];
  } else {
    $data['favorite_restaurant'] = '';
    $data['count_total'] = '';
  }
  $data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'favorite_restaurant'));
  $this->template->set('title', 'Favorite Restaurant'); 
  $this->template->load('user_dashboard_layout', 'contents', 'favorite_restaurant',$data);
}else{
  redirect('login');
}
}
public function get_fav_ajax_offset()
{
  $data['offset'] = $this->input->post('offset');
  $like_val = $this->input->post('like_val');
  if(!empty($like_val)){
    $pagecon['per_page'] = 20;
    $allarticle = $this->Common_model_new->getsearchdatafavrestaurant($like_val,$pagecon['per_page'],$data['offset']);
    if (!empty($allarticle['rows'])) {
      $data['favorite_restaurant'] = $allarticle['rows'];
      $data['count_total'] = $allarticle['num_rows'];
    } else {
      $data['favorite_restaurant'] = '';
      $data['count_total'] = '';
    }
  }else{
   $pagecon['per_page'] = 20;
   $nearbyrestaurants = $this->Common_model_new->getfavoriterestaurantlist($pagecon['per_page'],$data['offset']);
    //echo '<pre>';print_r($music_list1);die;
   if (!empty($nearbyrestaurants['rows'])) {
    $data['favorite_restaurant'] = $nearbyrestaurants['rows'];
    $data['count_total'] = $nearbyrestaurants['num_rows'];
  } else {
    $data['favorite_restaurant'] = '';
    $data['count_total'] = '';
  }
}
$this->load->view('ajax_value_fav_restaurant', $data);
}
public function search_fav_restaurant()
{
  $restaurant_title = $this->input->post('restaurant_title');
  $data['page'] = 0;
  $pagecon['per_page'] = 20;
  $allmusic = $this->Common_model_new->getsearchdatafavrestaurant($restaurant_title,$pagecon['per_page'],$data['page']);
  $data['favorite_restaurant'] = $allmusic['rows'];
  $data['count_total'] = $allmusic['num_rows'];
  $this->load->view("ajax_new_fav_search_value",$data);
}
public function GetResult($lat,$long)
{
  //$lat = $this->session->userdata('lat');
 // $long = $this->session->userdata('long');
       //$url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=10000&type=bank&sensor=true&key=AIzaSyBLEPBRfw7sMb73Mr88L91Jqh3tuE4mKsE";
  $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=10000&type=bank&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
  $json=file_get_contents($url);
  $data['banks']=json_decode($json,true);
           //echo "<pre>"; print_r($data['banks']); echo "</pre>"; die;
           // if(!empty($data['banks']['results'])){
           //   $datas=array();
           //    foreach($data['banks']['results'] as $key => $bank_details){
           //       if(isset($bank_details['photos'][0]['photo_reference'])){
           //         $image =  $bank_details['photos'][0]['photo_reference'];
           //       }else{
           //         $image="";
           //       }
           //         if(!empty($image)){
           //         $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
           //         }else{
           //           $image_path='';
           //         }
           //        $datas[] = array(
           //                       'bank_id'=>$key,
           //                       'bank_name'=>$bank_details['name'],
           //                       'bank_image'=>$image_path,
           //                       'bank_lat'=>$bank_details['geometry']['location']['lat'],
           //                       'bank_long'=>$bank_details['geometry']['location']['lng'],
           //                       'bank_address'=>$bank_details['vicinity'],
           //                       'place_id'=>$bank_details['place_id'],
           //                       'bank_rating'=>$bank_details['rating'],
           //                       'user_id'=>$this->session->userdata('logged_in')['id'],
           //                       'bank_created'=>date('Y-m-d H:i:s')
           //                       );
           //     }
           // }
          //$data['banks']=$datas;
  $result = $data['banks'];
        //$result = array_slice($data['banks'], $offset, $per_page);
  return $result;
}
public function all_banks()
{
 $data['menuactive'] = $this->uri->segment(1);
 $checkLogin = $this->session->userdata('logged_in');
 //$lat = $this->session->userdata('lat');
 //$long = $this->session->userdata('long');
 $lat = $this->input->get("lat");
 $long = $this->input->get("long");
 if(!empty($checkLogin))
 {
  $data['unit']=$this->common_model_new->getAll('place_unit');
        //$data['unit']='Miles';
  if($data['unit'][0]->place_unit=='Miles'){
    $radius = "8047";
    $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=bank&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
  }else if($data['unit'][0]->place_unit=='KM'){
    $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=5000&type=bank&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
  }
     //$url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=22.7167,75.8333&radius=5000&type=bank&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
  $bank = "bank";
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$bank");
  curl_setopt($ch, CURLOPT_POST, 1);
  $headers = array();
  $headers[] = "CAREPRO-AUTH-KEY: 12345";
  $headers[] = "Content-Type: application/x-www-form-urlencoded";
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  $result = curl_exec($ch);
  if (curl_errno($ch)) {
  }
  curl_close ($ch);
  $j_decoded = json_decode($result);
  $data['count_total'] = count($j_decoded->results);
        // $per_page = '5';
        // $offset = 0;
  $data['banks'] = $this->GetResult($lat,$long);
        // echo "<pre>";
        // print_r($restData);
        // die;
        //$restData['next_page_token'];
        // echo "<pre>";
        // print_r($data['banks']);
        // die;
  if(!empty($data['banks']['results'])){
   $datas=array();
   foreach($data['banks']['results'] as $key => $bank_details){
     if(isset($bank_details['photos'][0]['photo_reference'])){
       $image =  $bank_details['photos'][0]['photo_reference'];
     }else{
       $image="";
     }
     if(!empty($image)){
       $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
     }else{
       $image_path='';
     }
     $datas[] = array(
       'bank_id'=>$key,
       'bank_name'=>$bank_details['name'],
       'bank_image'=>$image_path,
       'bank_lat'=>$bank_details['geometry']['location']['lat'],
       'bank_long'=>$bank_details['geometry']['location']['lng'],
       'bank_address'=>$bank_details['vicinity'],
       'place_id'=>$bank_details['place_id'],
       'bank_rating'=>(!empty($bank_details['rating'])) ? $bank_details['rating'] : ''   ,
       'user_id'=>$this->session->userdata('logged_in')['id'],
       'bank_created'=>date('Y-m-d H:i:s')
     );
   }
 }
 $data['Banks']=$datas;
 $data['bankData'] = $data['Banks'];
 $data['next_page']  = $data['banks']['next_page_token'];
 $data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'all_banks'));
 $this->template->set('title', 'Bank');
 $this->template->load('user_dashboard_layout', 'contents', 'all_banks',$data);
}else{
 redirect('login');
}
}
public function radius_bank()
{
  $data['menuactive'] = $this->uri->segment(1);
  $radius = $this->uri->segment("3");
 // $lat = $this->session->userdata('lat');
 // $long = $this->session->userdata('long');
  $lat = $_GET["lat"];
  $long = $_GET["long"];
  $next_page = $this->input->post("next_page");
      // $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=bank&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo&pagetoken=$next_page";
  $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=bank&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo&pagetoken=$next_page";
    //print_r($url);die;
  $json=file_get_contents($url);
  $data['banks']=json_decode($json,true);
  $data['count_total'] = count($data['banks']);
  $result = $data['banks'];
  if(!empty($data['banks']['results'])){
   $datas=array();
   foreach($data['banks']['results'] as $key => $bank_details){
     if(isset($bank_details['photos'][0]['photo_reference'])){
       $image =  $bank_details['photos'][0]['photo_reference'];
     }else{
       $image="";
     }
     if(!empty($image)){
       $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
     }else{
       $image_path='';
     }
     $datas[] = array(
       'bank_id'=>$key,
       'bank_name'=>$bank_details['name'],
       'bank_image'=>$image_path,
       'bank_lat'=>$bank_details['geometry']['location']['lat'],
       'bank_long'=>$bank_details['geometry']['location']['lng'],
       'bank_address'=>$bank_details['vicinity'],
       'place_id'=>$bank_details['place_id'],
       'bank_rating'=>$bank_details['rating'],
       'user_id'=>$this->session->userdata('logged_in')['id'],
       'bank_created'=>date('Y-m-d H:i:s')
     );
   }
 }
 $data['Banks']=$datas;
 $data['bankData'] = $data['Banks'];
 $data['next_page']  = $data['banks']['next_page_token'];
 $data['unit']=$this->common_model_new->getAll('place_unit');
//$data['unit']='Miles';
 $this->template->set('title', 'Bank');
 $this->template->load('user_dashboard_layout', 'contents', 'all_banks',$data);
}
public function get_ajax_offset_new_bank1()
{
 $data['offset'] = $this->input->post('offset');
 $like_val = $this->input->post('like_val');
 $lat = $this->session->userdata('lat');
 $long = $this->session->userdata('long');
 $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=10000&type=bank&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
      //$url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=22.7167,75.8333&radius=10000&type=restaurant&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
 $restaurant = "restaurant";
 $ch = curl_init($url);
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$restaurant");
 curl_setopt($ch, CURLOPT_POST, 1);
 $headers = array();
 $headers[] = "CAREPRO-AUTH-KEY: 12345";
 $headers[] = "Content-Type: application/x-www-form-urlencoded";
 curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
 $result = curl_exec($ch);
 if (curl_errno($ch)) {
 }
 curl_close ($ch);
 $j_decoded = json_decode($result);
 $data['count_total'] = count($j_decoded->results);
 $per_page = '5';
 $offset = $data['offset'];
 $restData = $this->GetResult($per_page,$offset);
 $data['bankData'] = $restData;
 $this->load->view('ajax_value_bank', $data);
}
public function get_ajax_offset_new_bank_backup()
{
  $next_page_token = $this->input->post('next_page_token');
  $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=22.7167,75.8333&radius=10000&type=bank&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo&pagetoken=$next_page_token";
  $bank = "bank";
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$bank");
  curl_setopt($ch, CURLOPT_POST, 1);
  $headers = array();
  $headers[] = "CAREPRO-AUTH-KEY: 12345";
  $headers[] = "Content-Type: application/x-www-form-urlencoded";
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  $result = curl_exec($ch);
  if (curl_errno($ch)) {
  }
  curl_close ($ch);
  $j_decoded = json_decode($result);
  $data['count_total'] = count($j_decoded->results);
  $data['next_page'] = $j_decoded->next_page_token;
  $datas=array();
  foreach($j_decoded->results as $key => $bank_details){
    if(isset($bank_details->photos[0]->photo_reference)){
      $image =  $bank_details->photos[0]->photo_reference;
    }else{
      $image="";
    }
    if(!empty($image)){
      $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
    }else{
      $image_path='';
    }
    $datas[] = array(
     'bank_id'=>$key,
     'bank_name'=>$bank_details->name,
     'bank_image'=>$image_path,
     'bank_lat'=>$bank_details->geometry->location->lat,
     'bank_long'=>$bank_detailsgeometry->location->lng,
     'bank_address'=>$bank_details->vicinity,
     'place_id'=>$bank_details->place_id,
     'bank_rating'=>$bank_details->rating,
     'user_id'=>$this->session->userdata('logged_in')['id'],
     'bank_created'=>date('Y-m-d H:i:s')
   );
  }
  $data['bankData'] = $datas;
       //$this->load->view('ajax_value_bank', $data);
  $list = $this->load->view('ajax_value_bank', array('bankData' => $datas,'count_total' => $data['count_total'], 'next_page' => $data['next_page']), true);
  $this->output->set_content_type('application/json');
        //$this->output->set_output(json_encode(array("code" => 100, "success" => true,'tab'=> $tab,'tabbing'=>$tabbing)));
  $return = array('success' => true, 'list' => $list,'next_page' => $data['next_page']);
  echo json_encode($return);
}
public function get_ajax_offset_new_bank()
{
  $token = $this->input->post('next_page_token');
  //$lat = $this->session->userdata('lat');
 // $long = $this->session->userdata('long');
  $lat = $_POST["lat"];
  $long = $_POST["long"];
  $radius = $this->input->post("radius");
   //     $data['unit']='KM';
   //   if($data['unit']=='Miles'){
   //    $radius = round(8046.72);
   //   $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=bank&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
   // }else if($data['unit']=='KM'){
   //  $radius=5000;
   //  $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=bank&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo&pagetoken=$token";
   // }
  $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=bank&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo&pagetoken=$token";
  $bank = "bank";
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$bank");
  curl_setopt($ch, CURLOPT_POST, 1);
  $headers = array();
  $headers[] = "CAREPRO-AUTH-KEY: 12345";
  $headers[] = "Content-Type: application/x-www-form-urlencoded";
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  $result = curl_exec($ch);
  if (curl_errno($ch)) {
  }
  curl_close ($ch);
  $j_decoded = json_decode($result);
    //echo '<pre>';print_r($j_decoded->results);die;
  $data['count_total'] = count($j_decoded->results);
  $data['pagetoken'] = $j_decoded->next_page_token;
    //echo $data['pagetoken'];
  $datas=array();
  foreach($j_decoded->results as $key => $rest_details){
   if(isset($rest_details->photos[0]->photo_reference)){
     $image =  $rest_details->photos[0]->photo_reference;
   }else{
     $image="";
   }
   if(!empty($image)){
     $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
   }else{
     $image_path='';
   }
   if(!empty($rest_details->rating)){
    $rat = $rest_details->rating;
  }else{
    $rat = '';
  }
  $datas[] = array(
   'bank_id'=>$key,
   'bank_name'=>$rest_details->name,
   'bank_image'=>$image_path,
   'bank_lat'=>$rest_details->geometry->location->lat,
   'bank_long'=>$rest_details->location->lng,
   'bank_address'=>$rest_details->vicinity,
   'place_id'=>$rest_details->place_id,
   'bank_rating'=>$rest_details->rating,
   'user_id'=>$this->session->userdata('logged_in')['id'],
   'bank_created'=>date('Y-m-d H:i:s')
 );
}
$data['nearbyrestaurants'] = $datas;
 //$this->load->view('ajax_value_restaurant', $data);
$unit='Miles';
$list = $this->load->view('ajax_value_bank', array('bankData' => $datas,'count_total' => $data['count_total'], 'next_page' => $data['pagetoken'],'radius' => $radius,'lat' => $lat,'long' => $long,'unit'=>$unit), true);
$this->output->set_content_type('application/json');
        //$this->output->set_output(json_encode(array("code" => 100, "success" => true,'tab'=> $tab,'tabbing'=>$tabbing)));
$return = array('success' => true, 'list' => $list,'next_page' => $data['pagetoken']);
echo json_encode($return);
}
/*view single bank*/
      //   public function view_bank()
      //     {
      //     $data['menuactive'] = $this->uri->segment(1);
      //     $segment = $this->uri->segment('2');
      //     $data['view_bank'] = $this->common_model_new->getsingle("cp_bank",array("bank_id" => $segment));
      //     $this->template->set('title', 'Banks');
      //     $this->template->load('user_dashboard_layout', 'contents', 'view_bank',$data);
      // }
public function view_bank()
{
 $data['menuactive'] = $this->uri->segment(1);
 $radius = $this->uri->segment('3');
 $segment = $this->uri->segment('2');
 //$lat = $this->session->userdata('lat');
 //$long = $this->session->userdata('long');
 $la = $_GET['lat'];
 $lo = $_GET['long'];
 $lat = str_replace("%","",$la);
 $long = str_replace("%","",$lo);
 $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=bank&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
 $value = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$segment&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
 $bank = "bank";
 $ch = curl_init($value);
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 curl_setopt($ch, CURLOPT_POSTFIELDS, "type=bank");
 curl_setopt($ch, CURLOPT_POST, 1);
 $headers = array();
 $headers[] = "CAREPRO-AUTH-KEY: 12345";
 $headers[] = "Content-Type: application/x-www-form-urlencoded";
 curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
 $result = curl_exec($ch);
 if (curl_errno($ch)) {
 }
 curl_close ($ch);
    //echo "<pre>";print_r($result);  die;
 $j_decoded = json_decode($result);
    //echo "<pre>";print_r($j_decoded->result); die;
 if(isset($j_decoded->result->photos[0]->photo_reference)){
   $image =  $j_decoded->result->photos[0]->photo_reference;
 }else{
   $image="";
 }
 if(!empty($image)){
   $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
 }else{
   $image_path='';
 }
 if(!empty($j_decoded->result->rating)){
   $rat = $j_decoded->result->rating;
 }else{
   $rat = '';
 }
 $array = array(
  'bank_name'=> $j_decoded->result->name,
  'bank_image'=>$image_path,
  'bank_lat'=>$j_decoded->result->geometry->location->lat,
  'bank_long'=>$j_decoded->result->geometry->location->lng,
  'bank_address'=>$j_decoded->result->formatted_address,
  'place_id'=>$j_decoded->result->place_id,
  'bank_rating'=>$rat,
  'user_id'=>$this->session->userdata('logged_in')['id'],
  'bank_created'=>date('Y-m-d H:i:s')
);
 $data['view_bank'] = $array;
  // echo "<pre>"; print_r($data['banks']); echo "</pre>";
  // die;
  //$data['unit']='Miles';
 $data['unit']=$this->common_model_new->getAll('place_unit');
  //echo '<pre>';print_r($array);die;
 $this->template->set('title', 'Banks');
 $this->template->load('user_dashboard_layout', 'contents', 'view_bank',$data);
}
/*view single bank*/
public function view_bank_bckup()
{
  $data['menuactive'] = $this->uri->segment(1);
  $segment = $this->uri->segment('2');
  $lat = $this->session->userdata('lat');
  $long = $this->session->userdata('long');
        //$url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=10000&type=bank&sensor=true&key=AIzaSyBLEPBRfw7sMb73Mr88L91Jqh3tuE4mKsE";
  $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=10000&type=bank&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo&name=$bank_title";
  $json=file_get_contents($url);
  $data['banks']=json_decode($json,true);
  if(!empty($data['banks']['results'])){
    $datass=array();
    foreach($data['banks']['results'] as $key => $bank_details){
      if(isset($bank_details['photos'][0]['photo_reference'])){
        $image =  $bank_details['photos'][0]['photo_reference'];
      }else{
        $image="";
      }
      if(!empty($image)){
        $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
      }else{
        $image_path='';
      }
      $datass[] = array(
        'bank_id'=>$key,
        'bank_name'=>$bank_details['name'],
        'bank_image'=>$image_path,
        'bank_lat'=>$bank_details['geometry']['location']['lat'],
        'bank_long'=>$bank_details['geometry']['location']['lng'],
        'bank_address'=>$bank_details['vicinity'],
        'place_id'=>$bank_details['place_id'],
        'bank_rating'=>$bank_details['rating'],
        'user_id'=>$this->session->userdata('logged_in')['id'],
        'bank_created'=>date('Y-m-d H:i:s')
      );
    }
  }
  $data['view_bank']= $datass[$segment];
  $this->template->set('title', 'Banks');
  $this->template->load('user_dashboard_layout', 'contents', 'view_bank',$data);
}
/*Banks listing scroling pagination*/
public  function get_ajax_offset_value()
{
  $data['offset'] = $this->input->post('offset');
  $like_val = $this->input->post('like_val');
  if(!empty($like_val)){
   $pagecon['per_page'] = 20;
   $allbanks = $this->common_model_new->get_searched_bank($like_val,$pagecon['per_page'],$data['offset']);
   if (!empty($allbanks['rows'])) {
    $data['bankData'] = $allbanks['rows'];
    $data['count_total'] = $allbanks['num_rows'];
  } else {
    $data['bankData'] = '';
    $data['count_total'] = '';
  }
}else{
  $pagecon['per_page'] = 20;
  $allbanks = $this->common_model_new->bank_listing($pagecon['per_page'],$data['offset']);
      //echo '<pre>';print_r($music_list1);die;
  if (!empty($allbanks['rows'])) {
    $data['bankData'] = $allbanks['rows'];
    $data['count_total'] = $allbanks['num_rows'];
  } else {
    $data['bankData'] = '';
    $data['count_total'] = '';
  }
}
$this->load->view('ajax_value_bank', $data);
}
/*Bank searching*/
  // public function search_bank_list()
  //   {
  //     $bank_title = $this->input->post('bank_title');
  //   $data['page'] = 0;
  //   $pagecon['per_page'] = 20;
  //   $allbanks = $this->common_model_new->get_searched_bank($bank_title,$pagecon['per_page'],$data['page']);
  //   $data['bankData'] = $allbanks['rows'];
  //   $data['count_total'] = $allbanks['num_rows'];
  //   $this->load->view("ajax_bank_search_value",$data);
  // }
/*Bank searching*/
public function search_bank_list()
{
 $data['menuactive'] = $this->uri->segment(1);
    //$bank_title = $this->input->post('bank_title');
 $segment = $this->uri->segment("2");
 $radius = $this->uri->segment("3");
 $data['unit']=$this->common_model_new->getAll('place_unit');
 //$lat = $this->session->userdata('lat');
// $long = $this->session->userdata('long');
 $lat = $_GET["lat"];
 $long = $_GET["long"];
 $data['lat'] = $_GET["lat"];
 $data['long'] = $_GET["long"];
    // echo $segment;
    // die;
 if($radius == ''){
  $data['bank_search'] = '';
  $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$segment&type=bank&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
}else{
 $bank_search = str_replace('%20', ' ', $this->uri->segment("2"));
 $data['bank_search'] = $bank_search;
 $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=bank&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo&name=$segment";
}
        //$url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=10000&type=bank&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo&name=$bank_title";
$json=file_get_contents($url);
      //echo '<pre>';print_r($url);die;
$data['banks']=json_decode($json,true);
if(!empty($data['banks']['results'])){
  $datass=array();
  foreach($data['banks']['results'] as $key => $bank_details){
    if(isset($bank_details['photos'][0]['photo_reference'])){
      $image =  $bank_details['photos'][0]['photo_reference'];
    }else{
      $image="";
    }
    if(!empty($image)){
      $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
    }else{
      $image_path='';
    }
    $datass[] = array(
      'bank_id'=>$key,
      'bank_name'=>$bank_details['name'],
      'bank_image'=>$image_path,
      'bank_lat'=>$bank_details['geometry']['location']['lat'],
      'bank_long'=>$bank_details['geometry']['location']['lng'],
      'bank_address'=>$bank_details['vicinity'],
      'place_id'=>$bank_details['place_id'],
      'bank_rating'=>$bank_details['rating'],
      'user_id'=>$this->session->userdata('logged_in')['id'],
      'bank_created'=>date('Y-m-d H:i:s')
    );
  }
}
$data['bankData'] = $datass;
$data['count_total'] = count($datass);
$data['offset'] = '';
$data['next_page']  = $data['banks']['next_page_token'];
$this->template->set('title', 'All Banks');
$this->template->load('user_dashboard_layout', 'contents', 'all_banks',$data);
    //$this->load->view("ajax_bank_search_value",$data);
}
/*Get Banks by radius*/
public function Get_RadiusBanks()
{
  $radius = $this->input->post('radius');
  $data['radius']=$radius;
  $lat = $this->session->userdata('lat');
  $long = $this->session->userdata('long');
        //$url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=10000&type=bank&sensor=true&key=AIzaSyBLEPBRfw7sMb73Mr88L91Jqh3tuE4mKsE";
  $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=22.7167,75.8333&radius=$radius&type=bank&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo&name=$bank_title";
  $json=file_get_contents($url);
  $data['banks']=json_decode($json,true);
  if(!empty($data['banks']['results'])){
    $datass=array();
    foreach($data['banks']['results'] as $key => $bank_details){
      if(isset($bank_details['photos'][0]['photo_reference'])){
        $image =  $bank_details['photos'][0]['photo_reference'];
      }else{
        $image="";
      }
      if(!empty($image)){
        $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
      }else{
        $image_path='';
      }
      $datass[] = array(
        'bank_id'=>$key,
        'bank_name'=>$bank_details['name'],
        'bank_image'=>$image_path,
        'bank_lat'=>$bank_details['geometry']['location']['lat'],
        'bank_long'=>$bank_details['geometry']['location']['lng'],
        'bank_address'=>$bank_details['vicinity'],
        'place_id'=>$bank_details['place_id'],
        'bank_rating'=>$bank_details['rating'],
        'user_id'=>$this->session->userdata('logged_in')['id'],
        'bank_created'=>date('Y-m-d H:i:s')
      );
    }
  }
  $data['bankData'] = $datass;
  $data['count_total'] = count($datass);
  $data['offset'] = '';
    // $this->template->set('title', 'User Dashboard');
    // $this->template->load('user_dashboard_layout', 'contents', 'all_banks',$data);
  $this->load->view("ajax_bank_radius_value",$data);
}
/*Favorite and Unfavorite banks*/
public function FavouriteUnfavorite_bank()
{
 $place_id = $this->input->post('place_id');
 $checkLogin = $this->session->userdata('logged_in');
 $check_bank = $this->common_model_new->getsingle("cp_fav_banks",array("fav_place_id" => $place_id,"user_id" => $checkLogin['id']));
 if(!empty($check_bank)){
  $this->common_model_new->delete("cp_fav_banks",array("fav_place_id" => $place_id,"user_id" => $checkLogin['id']));
  echo '0';
}else{
 $url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$place_id&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
 $json=file_get_contents($url);
 $bankDetail=json_decode($json,true);
 if($bankDetail!=0){
   if(isset($bankDetail['result']['photos'][0]['photo_reference'])){
    $image =  $bankDetail['result']['photos'][0]['photo_reference'];
  }else{
    $image="";
  }
  if(!empty($image)){
   $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
 }else{
   $image_path='';
 }
 if(!empty($bankDetail['result']['rating'])){
  $rating = $bankDetail['result']['rating'];
}else{
  $rating ='';
}
$data = array(
  'fav_bank_name'=>$bankDetail['result']['name'],
  'fav_bank_image'=>$image_path,
  'fav_bank_lat'=>$bankDetail['result']['geometry']['location']['lat'],
  'fav_bank_long'=>$bankDetail['result']['geometry']['location']['lng'],
  'fav_bank_address'=>$bankDetail['result']['vicinity'],
  'fav_place_id'=>$bankDetail['result']['place_id'],
  'fav_bank_rating'=>$rating,
  'user_id' => $checkLogin["id"],
                    //'bank_id' => $bank_id,
  'fav_bank_created' => date("Y-m-d H:i:s")
);
$this->common_model_new->insertData("cp_fav_banks",$data);
echo '1';
}
}
}
/*Favorite and Unfavorite banks*/
  // public function FavouriteUnfavorite_bank()
  // {
  //    $bank_id = $this->input->post('bank_id');
  //    $checkLogin = $this->session->userdata('logged_in');
  //    $check_bank = $this->common_model_new->getsingle("cp_fav_banks",array("bank_id" => $bank_id,"user_id" => $checkLogin['id']));
  //    if(!empty($check_bank)){
  //       $this->common_model_new->delete("cp_fav_banks",array("bank_id" => $bank_id,"user_id" => $checkLogin['id']));
  //       echo '0';
  //    }else{
  //      $bankDetail = $this->common_model_new->getsingle('cp_bank',array('bank_id'=>$bank_id,'user_id'=>$checkLogin['id']));
  //       $data = array(
  //                   'fav_bank_name'=>$bankDetail->bank_name,
  //                   'fav_bank_image'=>$bankDetail->bank_image,
  //                   'fav_bank_lat'=>$bankDetail->bank_lat,
  //                   'fav_bank_long'=>$bankDetail->bank_long,
  //                   'fav_bank_address'=>$bankDetail->bank_address,
  //                   'fav_place_id'=>$bankDetail->place_id,
  //                   'fav_bank_rating'=>$bankDetail->bank_rating,
  //                   'user_id' => $checkLogin["id"],
  //                   'bank_id' => $bank_id,
  //                   'fav_bank_created' => date("Y-m-d H:i:s")
  //                 );
  //       $this->common_model_new->insertData("cp_fav_banks",$data);
  //       echo '1';
  //    }
  // }
// public function FavouriteUnfavorite_bank()
// {
//   $restaurant_id = $this->input->post('restaurant_id');
//   $restaurant_data = $this->common_model_new->getsingle("cp_banks",array("user_id" => $this->session->userdata('logged_in')['id'],"bank_id" => $restaurant_id));
//    $check_restaurant = $this->common_model_new->getsingle("cp_fav_banks",array("bank_id" => $restaurant_id,"user_id" => $this->session->userdata('logged_in')['id']));
//   if(!empty($check_restaurant)){
//   $this->common_model_new->delete("cp_fav_banks",array("bank_id" => $restaurant_id,"user_id" => $this->session->userdata('logged_in')['id']));
//       echo '0';
//   }else{
//   if(!empty($restaurant_data)){
//     $data = array(
//                     'fav_bank_name'=>$restaurant_data->bank_name,
//                     'fav_bank_image'=>$restaurant_data->bank_image,
//                     'fav_bank_lat'=>$restaurant_data->bank_lat,
//                     'fav_bank_long'=>$restaurant_data->bank_long,
//                     'fav_bank_address'=>$restaurant_data->bank_address,
//                     'fav_place_id'=>$restaurant_data->place_id,
//                     'fav_bank_rating'=>$restaurant_data->bank_rating,
//                     'user_id' => $checkLogin["id"],
//                     'bank_id' => $bank_id,
//                     'fav_bank_created' => date("Y-m-d H:i:s")
//                   );
//         $this->common_model_new->insertData("cp_fav_banks",$data);
// }
//   echo '1';
//   }
// }
/*All favorite banks listing with scroling pagination.*/
public function favorite_banks()
{
  $data['menuactive'] = $this->uri->segment(1);
  $checkLogin = $this->session->userdata('logged_in');
  if(!empty($checkLogin))
  {
    $per_page = '20';
    $offset = 0;
    $FavoriteBanks = $this->common_model_new->favorite_bank_listing($per_page,$offset);
    $data['FavoriteBanks'] = $FavoriteBanks['rows'];
    $data['count_total'] = $FavoriteBanks['num_rows'];
    $data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'favourite_bank')); 
    $this->template->set('title', 'Favorite Bank');
    $this->template->load('user_dashboard_layout', 'contents', 'favorite_banks',$data);
  }else{
    redirect('login');
  }
}
/*view single favorite bank*/
public function view_favorite_bank()
{
  $data['menuactive'] = $this->uri->segment(1);
  $segment = $this->uri->segment('2');
  $data['view_favorite_bank'] = $this->common_model_new->getsingle("cp_fav_banks",array("fav_bank_id" => $segment));
  $this->template->set('title', 'Banks');
  $this->template->load('user_dashboard_layout', 'contents', 'view_favorite_bank',$data);
}
/*Favorite Banks listing scroling pagination*/
public  function get_ajax_favoriteBanks_offset_value()
{
  $data['offset'] = $this->input->post('offset');
  $like_val = $this->input->post('like_val');
  if(!empty($like_val)){
   $pagecon['per_page'] = 20;
   $Favoritebanks = $this->common_model_new->get_searched_favorite_banks($like_val,$pagecon['per_page'],$data['offset']);
   if (!empty($Favoritebanks['rows'])) {
    $data['FavoritebankData'] = $Favoritebanks['rows'];
    $data['count_total'] = $Favoritebanks['num_rows'];
  } else {
    $data['FavoritebankData'] = '';
    $data['count_total'] = '';
  }
}else{
  $pagecon['per_page'] = 20;
  $Favoritebanks = $this->common_model_new->favorite_bank_listing($pagecon['per_page'],$data['offset']);
      //echo '<pre>';print_r($music_list1);die;
  if (!empty($Favoritebanks['rows'])) {
    $data['FavoritebankData'] = $Favoritebanks['rows'];
    $data['count_total'] = $Favoritebanks['num_rows'];
  } else {
    $data['FavoritebankData'] = '';
    $data['count_total'] = '';
  }
}
$this->load->view('ajax_value_favorite_bank', $data);
}
/*Bank searching*/
public function search_favorite_bank_list()
{
  $bank_title = $this->input->post('bank_title');
  $data['page'] = 0;
  $pagecon['per_page'] = 20;
  $Favoritebanks = $this->common_model_new->get_searched_favorite_banks($bank_title,$pagecon['per_page'],$data['page']);
  $data['FavoritebankData'] = $Favoritebanks['rows'];
  $data['count_total'] = $Favoritebanks['num_rows'];
  $this->load->view("ajax_favorite_bank_search_value",$data);
}
/*Unfavorite banks*/
public function Unfavorite_bank()
{
 $fav_bank_id = $this->input->post('fav_bank_id');
 $checkLogin = $this->session->userdata('logged_in');
 $check_bank = $this->common_model_new->getsingle("cp_fav_banks",array("fav_bank_id" => $fav_bank_id,"user_id" => $checkLogin['id']));
 if(!empty($check_bank)){
  $this->common_model_new->delete("cp_fav_banks",array("fav_bank_id" => $fav_bank_id,"user_id" => $checkLogin['id']));
  echo 1;
}
}
public function GetResult_rest($per_page,$offset)
{
  $lat = $this->session->userdata('lat');
  $long = $this->session->userdata('long');
  $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=10000&type=restaurant&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
  $json=file_get_contents($url);
  $data['restaurant']=json_decode($json,true);
  $data['pagetoken'] = $data['restaurant']['next_page_token'];
  if(!empty($data['restaurant']['results'])){
   $datas=array();
   foreach($data['restaurant']['results'] as $key => $rest_details){
     if(isset($rest_details['photos'][0]['photo_reference'])){
       $image =  $rest_details['photos'][0]['photo_reference'];
     }else{
       $image="";
     }
     if(!empty($image)){
       $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
     }else{
       $image_path='';
     }
     if(!empty($rest_details['rating'])){
      $rat = $rest_details['rating'];
    }else{
      $rat = '';
    }
    $datas[] = array(
     'rest_id'=>$key,
     'rest_name'=>$rest_details['name'],
     'rest_image'=>$image_path,
     'rest_lat'=>$rest_details['geometry']['location']['lat'],
     'rest_long'=>$rest_details['geometry']['location']['lng'],
     'rest_address'=>$rest_details['vicinity'],
     'place_id'=>$rest_details['place_id'],
     'rating'=>$rat,
     'user_id'=>$this->session->userdata('logged_in')['id'],
     'rest_created'=>date('Y-m-d H:i:s')
   );
  }
}
$data['restaurants']=$datas;
$data['total_count'] = count($data['restaurants']);
$result = array_slice($data['restaurants'], $offset, $per_page);
return $result;
}
public function get_all_restaurants()
{
 $data['menuactive'] = $this->uri->segment(1);
 $checkLogin = $this->session->userdata('logged_in');
 if(!empty($checkLogin))
 {
  $lat = $this->input->get("lat");
  $long = $this->input->get("long");
 //$lat = $this->session->userdata('lat');
  //$long = $this->session->userdata('long');
  $data['unit']=$this->common_model_new->getAll('place_unit');
  if($data['unit'][0]->place_unit=='Miles'){
    $radius = "8047";
    $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=restaurant&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
  }elseif ($data['unit'][0]->place_unit=='KM') {
    $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=5000&type=restaurant&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
  }
  $restaurant = "restaurant";
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$restaurant");
  curl_setopt($ch, CURLOPT_POST, 1);
  $headers = array();
  $headers[] = "CAREPRO-AUTH-KEY: 12345";
  $headers[] = "Content-Type: application/x-www-form-urlencoded";
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  $result = curl_exec($ch);
  if (curl_errno($ch)) {
  }
  curl_close ($ch);
  $j_decoded = json_decode($result);
  $data['pagetoken'] = $j_decoded->next_page_token;
  $data['count_total'] = count($j_decoded->results);
  $per_page = count($j_decoded->results);
  $offset = 0;
   // $restData = $this->GetResult_rest($per_page,$offset);
  $json=file_get_contents($url);
  $data['restaurant']=json_decode($json,true);
  if(!empty($data['restaurant']['results'])){
   $datas=array();
   foreach($data['restaurant']['results'] as $key => $rest_details){
     if(isset($rest_details['photos'][0]['photo_reference'])){
       $image =  $rest_details['photos'][0]['photo_reference'];
     }else{
       $image="";
     }
     if(!empty($image)){
       $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
     }else{
       $image_path='';
     }
     if(!empty($rest_details['rating'])){
      $rat = $rest_details['rating'];
    }else{
      $rat = '';
    }
    $datas[] = array(
     'rest_id'=>$key,
     'rest_name'=>$rest_details['name'],
     'rest_image'=>$image_path,
     'rest_lat'=>$rest_details['geometry']['location']['lat'],
     'rest_long'=>$rest_details['geometry']['location']['lng'],
     'rest_address'=>$rest_details['vicinity'],
     'place_id'=>$rest_details['place_id'],
     'rating'=>$rat,
     'user_id'=>$this->session->userdata('logged_in')['id'],
     'rest_created'=>date('Y-m-d H:i:s')
   );
  }
}
$data['nearbyrestaurants']=$datas;
$data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'all_restaurant'));
$this->template->set('title', 'Restaurant');
$this->template->load('user_dashboard_layout', 'contents', 'nearby_restaurant',$data);
}else{
 redirect('login');
}
}
public function get_ajax_offset_token()
{
  $token = $this->input->post("offset");
 //$lat = $this->session->userdata('lat');
  //$long = $this->session->userdata('long');
  $lat = $_POST["lat"];
  $long = $_POST["long"];
  $radius = $this->input->post("radius");
  $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=restaurant&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo&pagetoken=$token";
  $restaurant = "restaurant";
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$restaurant");
  curl_setopt($ch, CURLOPT_POST, 1);
  $headers = array();
  $headers[] = "CAREPRO-AUTH-KEY: 12345";
  $headers[] = "Content-Type: application/x-www-form-urlencoded";
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  $result = curl_exec($ch);
  if (curl_errno($ch)) {
  }
  curl_close ($ch);
  $j_decoded = json_decode($result);
    //echo '<pre>';print_r($j_decoded->results);die;
  $data['count_total'] = count($j_decoded->results);
  $data['pagetoken'] = $j_decoded->next_page_token;
    //echo $data['pagetoken'];
  $datas=array();
  foreach($j_decoded->results as $key => $rest_details){
   if(isset($rest_details->photos[0]->photo_reference)){
     $image =  $rest_details->photos[0]->photo_reference;
   }else{
     $image="";
   }
   if(!empty($image)){
     $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
   }else{
     $image_path='';
   }
   if(!empty($rest_details->rating)){
    $rat = $rest_details->rating;
  }else{
    $rat = '';
  }
  $datas[] = array(
   'rest_id'=>$key,
   'rest_name'=>$rest_details->name,
   'rest_image'=>$image_path,
   'rest_lat'=>$rest_details->geometry->location->lat,
   'rest_long'=>$rest_details->geometry->location->lng,
   'rest_address'=>$rest_details->vicinity,
   'place_id'=>$rest_details->place_id,
   'rating'=>$rat,
   'user_id'=>$this->session->userdata('logged_in')['id'],
   'rest_created'=>date('Y-m-d H:i:s')
 );
}
$data['nearbyrestaurants'] = $datas;
 //$this->load->view('ajax_value_restaurant', $data);
$list = $this->load->view('ajax_value_restaurant', array('nearbyrestaurants' => $datas,'count_total' => $data['count_total'], 'pagetoken' => $data['pagetoken'],'radius' => $radius), true);
$this->output->set_content_type('application/json');
        //$this->output->set_output(json_encode(array("code" => 100, "success" => true,'tab'=> $tab,'tabbing'=>$tabbing)));
$return = array('success' => true, 'list' => $list,'pagetoken' => $data['pagetoken']);
echo json_encode($return);
}
public function radius_restaurant()
{
 $data['menuactive'] = $this->uri->segment(1);
 $radius = $this->uri->segment("3");
 //$lat = $this->session->userdata('lat');
 //$long = $this->session->userdata('long');
 $lat = $_GET["lat"];
 $long = $_GET["long"];
 $data['unit']=$this->common_model_new->getAll('place_unit');
 $pagetoken = $this->input->post("pagetoken");
 $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=restaurant&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo&pagetoken=$pagetoken";
      //print_r($url);die;
 $restaurant = "restaurant";
 $ch = curl_init($url);
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$restaurant");
 curl_setopt($ch, CURLOPT_POST, 1);
 $headers = array();
 $headers[] = "CAREPRO-AUTH-KEY: 12345";
 $headers[] = "Content-Type: application/x-www-form-urlencoded";
 curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
 $result = curl_exec($ch);
 if (curl_errno($ch)) {
 }
 curl_close ($ch);
 $j_decoded = json_decode($result);
 $data['count_total'] = count($j_decoded->results);
 $data['pagetoken'] = $j_decoded->next_page_token;
 foreach($j_decoded->results as $key => $rest_details){
   if(isset($rest_details->photos[0]->photo_reference)){
     $image =  $rest_details->photos[0]->photo_reference;
   }else{
     $image="";
   }
   if(!empty($image)){
     $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
   }else{
     $image_path='';
   }
   if(!empty($rest_details->rating)){
    $rat = $rest_details->rating;
  }else{
    $rat = '';
  }
  $datas[] = array(
   'rest_id'=>$key,
   'rest_name'=>$rest_details->name,
   'rest_image'=>$image_path,
   'rest_lat'=>$rest_details->geometry->location->lat,
   'rest_long'=>$rest_details->geometry->location->lng,
   'rest_address'=>$rest_details->vicinity,
   'place_id'=>$rest_details->place_id,
   'rating'=>$rat,
   'user_id'=>$this->session->userdata('logged_in')['id'],
   'rest_created'=>date('Y-m-d H:i:s')
 );
}
$data['nearbyrestaurants'] = $datas;
//echo '<pre>';print_r($j_decoded->results);die;
$this->template->set('title', 'Restaurant');
$this->template->load('user_dashboard_layout', 'contents', 'nearby_restaurant',$data);
}
public function get_ajax_offset_new()
{
  $data['offset'] = $this->input->post('offset');
  $like_val = $this->input->post('like_val');
  $lat = $this->session->userdata('lat');
  $long = $this->session->userdata('long');
  // print_r($getlocation);die;
  $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=22.7167,75.8333&radius=10000&type=restaurant&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
  $restaurant = "restaurant";
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$restaurant");
  curl_setopt($ch, CURLOPT_POST, 1);
  $headers = array();
  $headers[] = "CAREPRO-AUTH-KEY: 12345";
  $headers[] = "Content-Type: application/x-www-form-urlencoded";
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  $result = curl_exec($ch);
  if (curl_errno($ch)) {
  }
  curl_close ($ch);
  $j_decoded = json_decode($result);
  $data['count_total'] = count($j_decoded->results);
  $per_page = '5';
  $offset = $data['offset'];
  $restData = $this->GetResult_rest($per_page,$offset);
  $data['nearbyrestaurants'] = $restData;
  $this->load->view('ajax_value_restaurant', $data);
}
public function getsearchdata()
{
  $data['menuactive'] = $this->uri->segment(1);
  $segment = $this->uri->segment("2");
  $radius = $this->uri->segment("3");
  $data['unit']=$this->common_model_new->getAll('place_unit');
  //$lat = $this->session->userdata('lat');
 // $long = $this->session->userdata('long');
  $lat = $_GET["lat"];
  $long = $_GET["long"];
  if($radius == ''){
    $data['rest_search'] = '';
    $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$segment&type=restaurant&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo";
  }else{
   $rest_search = str_replace('%20', ' ', $this->uri->segment("2"));
   $data['rest_search'] = $rest_search;
   $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=restaurant&sensor=true&key=AIzaSyAaxWTGgTrUttE-sE_poGr_heL_aoo9Hoo&name=$segment";
 }
 $restaurant = "restaurant";
 $ch = curl_init($url);
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 curl_setopt($ch, CURLOPT_POSTFIELDS, "type=$restaurant");
 curl_setopt($ch, CURLOPT_POST, 1);
 $headers = array();
 $headers[] = "CAREPRO-AUTH-KEY: 12345";
 $headers[] = "Content-Type: application/x-www-form-urlencoded";
 curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
 $result = curl_exec($ch);
 if (curl_errno($ch)) {
 }
 curl_close ($ch);
 $j_decoded = json_decode($result);
 $data['pagetoken'] = $j_decoded->next_page_token;
 foreach($j_decoded->results as $key => $rest){
  foreach($rest->photos as $photo){
    $image = $photo->photo_reference;
    $image_path = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$image&key=AIzaSyCrVGd7PYWdG6NMU-zXRvowyPY1qXr2mjQ";
    if($rest->icon){
      $icon = $rest->icon;
      $icon_path = "https://maps.gstatic.com/mapfiles/place_api/icons/$icon";
    }else{
      $icon_path = '';
    }
  }
  if(!empty($rest->rating)){
    $rat = $rest->rating;
  }else{
    $rat = '';
  }
  $datas[] = array(
   'rest_id'=>$key,
   'rest_name' => $rest->name,
   'rest_icon' =>  $icon_path,
   'rating' => $rat,
   'place_id' => $rest->place_id,
   'user_id' => $this->session->userdata('logged_in')['id'],
   'rest_address' => $rest->vicinity,
   'rest_lat' => $rest->geometry->location->lat,
   'rest_long' => $rest->geometry->location->lng,
   'rest_image' => $image_path,
   'rest_created' => date('Y-m-d H:i:s')
 );
}
$data["count_total"] = count($j_decoded->results);
$data['nearbyrestaurants'] = $datas;
$data['offset'] = '';
$this->template->set('title', 'Restaurant');
$this->template->load('user_dashboard_layout', 'contents', 'nearby_restaurant',$data);
}
public function chat(){
  $this->load->view('chat.php');
}
/**Search Result Navjyoti**/
public function search_result()
  {

    $helpwithRes = $this->input->post('help_with_array');
    $bodypartRes = $this->input->post('body_parts_array');
    $yogastyleRes = $this->input->post('yoga_style_array');
    $yogaDurRes = $this->input->post('yoga_duration_array');
    $yogaTypeRes = $this->input->post('yoga_type_array');
    $yogaPoseRes = $this->input->post('yoga_pose_array');
    $yogaLevelRes = $this->input->post('yoga_level_array');
    $data = $this->common_model_new->get_searched_yoga_video($helpwithRes, $bodypartRes, $yogastyleRes, $yogaDurRes, $yogaTypeRes, $yogaPoseRes, $yogaLevelRes);
    foreach ($data as $all_doc) {

      $array[] = array(
        'yoga_url' => $all_doc['yoga_url']
      );   
  

  }
  $cat_all = $array;
  $this->output->set_content_type('application/json');
  echo json_encode($cat_all); 
}

/*************Favorite yoga video */

public function add_fav(){
  $user_id = $this->session->userdata('logged_in')['id'];
  $favorite = $this->input->post('favorite');
  // _d( $favorite);
  $yogaid = $this->input->post('yoga_id');
  $data = $this->common_model_new->getsingle("yoga_fav_video",array("yoga_id" => $yogaid)); 

if(!empty($data)){
//  _dx($data);
    $update_array = array(
      "favorite"	=> $favorite,
      "yoga_id"		=> $yogaid,
      "user_id"			=> $user_id
  
    );    
    $query = $this->common_model_new->updateRecords('yoga_fav_video',$update_array,array('yoga_id' => $data->yoga_id));  
    $this->output->set_content_type('application/json');
    $return = array('success' => true, 'list' =>  $update_array);
    echo json_encode($return);

    // redirect('yoga');
  }else{
    // _d("insert");
      $insert_array = array(
        "favorite"	=> '1',
        "yoga_id"		=> $yogaid,
        "user_id"			=> $user_id,
        'create_record' => date('Y-m-d H:i:s')

      );
      
      $query = $this->common_model_new->addRecords('yoga_fav_video', $insert_array);
      $this->output->set_content_type('application/json');
      $return = array('success' => true, 'list' => 'insert');
      echo json_encode($return);
     
      // redirect('yoga');

    }

}
}
?>
