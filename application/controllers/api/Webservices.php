<?php



defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/twilio-php-master/Twilio/autoload.php';

use Twilio\Rest\Client;

use Twilio\Jwt\AccessToken;

use Twilio\Jwt\Grants\VoiceGrant;

use Twilio\Jwt\Grants\VideoGrant;

class Webservices extends MX_Controller {



  public function __construct(){   



    parent:: __construct();



    if ($_POST) 
      { $this->param = $_POST;
      }else{ 
        $this->param = json_decode(file_get_contents("php://input"),true); 
      }

      $userData = isset($this->param['user_language']);

      $GLOBALS['user_lang'] = $userData;



    }

    public function addPhoto(){



     $data = $_POST;

     $object_info = $data;

     $required_parameter = array('user_id');

     $chk_error = check_required_value($required_parameter, $object_info);

     if ($chk_error) {

      $resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

            //$this->response($resp);

      echo json_encode($resp);die;

    }

    if(!empty($_FILES['photo_image']['name'])) { 

     $random = $this->generateRandomString(10);

     $ext = pathinfo($_FILES['photo_image']['name'], PATHINFO_EXTENSION);

     $file_name = $random.".".$ext;

     $target_dir = "uploads/photos/";

     $target_file = $target_dir .$file_name;

     if (move_uploaded_file($_FILES["photo_image"]["tmp_name"], $target_file)){

      $photoFile = $file_name;

    }

  }

  $post_data = array

  (

    'user_id'=> $data['user_id'],

    'u_photo_created' => date('Y-m-d h:i:s'),

    'u_photo_status' => 1,  

    'u_photo'        =>$photoFile,   

  );

  $photoId = $this->common_model->addRecords('cp_user_photo', $post_data);

  if($photoId) 

  {

    $photoData = $this->common_model->getSingleRecordById('cp_user_photo', array('u_photo_id' => $photoId));

    $photoData1 = array(

      'id' => $photoData['u_photo_id'],

      'user_id' => $photoData['user_id'],

      'image_url'=>base_url().'uploads/photos/'.$photoData['u_photo']

    );

    $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'photo added successfully', 'response' => $photoData1);

  }else{

   $resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => message(18)));

 }

      //$this->response($resp);

 echo json_encode($resp);



}



/*-------Add test report--------*/

public function add_test_report()

{ 

  // echo "<pre>";
  // print_r($_POST);
  // die;

  $data = $_POST;

  $object_info = $data;		

  $required_parameter = array('doctor_id',"test_type_id","user_id");

  /*$required_parameter = array('doctor_id',"test_type_id","description","user_id");*/

  $chk_error = check_required_value($required_parameter, $object_info);

		//print_r($data['description']);die;

  if ($chk_error) { 



   $resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper(isset($chk_error['param'])));

         //$this->response($resp);

   echo json_encode($resp);die;     



 }



 if(!empty($_FILES['image']['name'])) { 



   $random = $this->generateRandomString(10);



   $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);



   $file_name = $random.".".$ext;

   //$target_dir = getcwd().'/uploads/test_report/';

   //die;
   $target_dir = "uploads/test_report/";



   $target_file = $target_dir .$file_name;

   // echo $target_file;
   // die; 


   if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)){

    $photoFile = $file_name;

  }

}



if($photoFile){

 $image = $photoFile;

}else{

 $image = '';

}



if(!empty($data['other_test_type'])){



 $array_other = array('other_test_type' => $data['other_test_type'],'test_type' => $data['other_test_type'],'create_date' => date("Y-m-d H:i:s"));

 $new_test_ty = $this->common_model->addRecords('cp_test_type', $array_other);



 $array_virtualtype = array('test_type' => $data['other_test_type'],'other_test_type'=>$data['other_test_type'],'create_date' => date("Y-m-d H:i:s"),'update_date' => date("Y-m-d H:i:s"));

 $new_virtualtype = $this->common_model->addRecords('cp_vital_test_type', $array_virtualtype);



 $unit = (!empty($data['unit']) ? $data['unit'] : '').' '.(!empty($data['degree_salcius']) ? $data['degree_salcius'] : '');

 $array_unittype = array('unit' => $unit,'test_type_id'=>$new_test_ty,'degree'=>(!empty($data['degree_salcius']) ? $data['degree_salcius'] : ''),'create_date' => date("Y-m-d H:i:s"),'update_date' => date("Y-m-d H:i:s"));

 $new_unittype = $this->common_model->addRecords('cp_vital_test_type_unit', $array_unittype);

}



if(!empty($data['other_doctor_name'])){



 $array_otherdoc = array('other_doctor_name' => $data['other_doctor_name'],'doctor_name'=>$data['other_doctor_name'],'doctor_created' => date("Y-m-d H:i:s"));

 $new_doc = $this->common_model->addRecords('cp_doctor', $array_otherdoc);



}



if(!empty($data['other_test_type'])){

 $testty = $new_test_ty;

}else{

 $testty = $data['test_type_id'];

}



if(!empty($data['other_doctor_name'])){

 $doct = $new_doc;

}else{

 $doct = $data['doctor_id'];

}



$test_report_data = array( 

 'user_id'=> $data['user_id'],

 'test_report_title' => $data['test_report_title'],

 'doctor_id'=> $doct,

 'test_type_id'=> $testty,

 'description'=> $data['description'],

 'image' => $image,

 'create_date'=> date('Y-m-d H:i:s'),         

);



$reportId = $this->common_model->addRecords('cp_test_report', $test_report_data);



if($reportId){



 $reportData = $this->common_model->getSingleRecordById('cp_test_report', array('id' => $reportId));



 $testunit = $this->common_model->getsingle('cp_vital_test_type_unit', array('test_type_id' => $data['test_type_id']));



 $testtype = $this->common_model->getsingle('cp_test_type', array('id' => $data['test_type_id']));

 $doctorname = $this->common_model->getsingle('cp_doctor', array('doctor_id' => $data['doctor_id']));



 $reminderData1 = array(

  'id' => $reportData['id'],

  'doctor_id' => $doctorname->doctor_name,

  'test_type_id' => $testtype->test_type,

  'unit' => (!empty($testunit->unit) ? $testunit->unit : ''),

  'test_report_title' => $reportData['test_report_title'],

  'description' => $reportData['description'],

  'user_id' => $reportData['user_id'],

  'image_url'=>base_url().'uploads/test_report/'.$reportData['image']                  

);



 $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Test report added successfully', 'response' => $reminderData1);



} else {

 $resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

}

echo json_encode($resp);

}







public function generateRandomString($length = 5) {

  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

  $charactersLength = strlen($characters);

  $randomString = '';

  for ($i = 0; $i < $length; $i++) {

    $randomString .= $characters[rand(0, $charactersLength - 1)];

  }

  return $randomString;

}

public function  orderImage($email_id){

 $this->db->select('attachment_file');

 $this->db->from('cp_emails_attachment');

 $this->db->where('email_id',$email_id);

 $query = $this->db->get();

 $result = $query->result_array();

 foreach($result as $result_details){

  $imageArray[]  = base_url().'uploads/email/'.$result_details['attachment_file'];

}

		// print_r($email_id);die;

return $imageArray;

}

public function send_mail_backup() {



        //echo "test";die;

  $data = $_POST;

  $object_info = $data;

  $required_parameter = array('to_email','subject','message','user_id');

  $chk_error = check_required_value($required_parameter, $object_info);

  if ($chk_error) {

    $resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

    echo json_encode($resp);die;

  }





  $post_data = array

  (

    'to_email'=> $data['to_email'],

    'subject'=> $data['subject'],

    'message'=> $data['message'],

    'user_id'=> $data['user_id'],

    'create_email' => date('Y-m-d'),

    'email_status' => 1,     

  );

  $emailId = $this->common_model->addRecords('cp_emails', $post_data);

  if(!empty($_FILES['attachment_file']['name'])) {  

    $count = count($_FILES['attachment_file']['name']);

    foreach($_FILES['attachment_file']['name'] as $key=>$val){



     $random = $this->generateRandomString(10);

     $ext = pathinfo($_FILES['attachment_file']['name'][$key], PATHINFO_EXTENSION);

     $file_name = $random.".".$ext;

     $target_dir = "uploads/email/";

     $target_file = $target_dir .$file_name;

                   //echo $file_name;die;

     if (move_uploaded_file($_FILES["attachment_file"]["tmp_name"][$key], $target_file)){

       $productFile = $file_name;

     }

     $orderGalleryData = array('attachment_file'=>$productFile,'email_id'=>$emailId,);



                  //print_r("<pre/>");

                  //print_r($orderGalleryData);

                 // die;

     $this->db->insert('cp_emails_attachment',$orderGalleryData);

   }

 } 



        //echo "test";die;

 if($emailId) 

 {



  $attachment = $this->orderImage($emailId);

           // print_r("<pre/>");

            //print_r($attachment);

           // die;

  $user_name = $data['to_email'];

  $subject = $data['subject'];

  $message = $data['message'];

  $chk_mail= sendEmail1($user_name,$subject,$message,$attachment);

  $emailData = $this->common_model->getSingleRecordById('cp_emails', array('email_id' => $emailId));

  if($emailData['create_email'] == "0000-00-00"){

   $emails_create = $emailData['create_email'];

 }else{

   $emails_create = date("M d, Y", strtotime($emailData['create_email']));

 }



            //print_r("<pre/>");

            //print_r($emailData);

            //die;

 $emailData1 = array(

  'to_email' => $emailData['to_email'],

  'subject' => $emailData['subject'],

  'message' => $emailData['message'],

  'user_id' => $emailData['user_id'],

  'create_date' => $emails_create,

  'images'=>$this->orderImage($emailData['email_id'])

);





 $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Message sent.', 'response' => $emailData1);

} else 

{

  $resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

}

echo json_encode($resp);

}



/*-------Add test report--------*/

public function update_test_report()

{







  $data = $_POST;



  $object_info = $data;   

  /*$required_parameter = array('doctor_id',"test_type_id","description","user_id","test_report_id","title");*/

  $required_parameter = array('doctor_id',"test_type_id","user_id","test_report_id","title");



  $chk_error = check_required_value($required_parameter, $object_info);

    //print_r($data['description']);die;

  if ($chk_error) { 

    $resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

            //$this->response($resp);

    echo json_encode($resp);die;

  }



  if(!empty($_FILES['image']['name'])) { 

   $random = $this->generateRandomString(10);

   $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);

   $file_name = $random.".".$ext;

   $target_dir = "uploads/test_report/";

   $target_file = $target_dir .$file_name;

   if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)){

    $photoFile = $file_name;

  }



  if($photoFile){

    $image = $photoFile;

  }else{

    $image = '';

  }



  $test_report_data['image'] = $image;

}



$reportData = $this->common_model->getSingleRecordById('cp_test_report', array('id' => $data['test_report_id']));



$testtype = $this->common_model->getsingle('cp_test_type', array('id' => $reportData['test_type_id']));



$doctorname = $this->common_model->getsingle('cp_doctor', array('doctor_id' => $reportData['doctor_id']));



if(!empty($data['other_test_type'])){

 $this->common_model->updateRecords('cp_test_type', array("test_type" => $data['other_test_type'],'other_test_type'=>$data['other_test_type']), array("id"=>$reportData['test_type_id']));



 if (!empty($data['degree_salcius'])) {

  $this->common_model->updateRecords('cp_vital_test_type_unit', array("unit" => $data['degree_salcius'],'degree'=>$data['degree_salcius']), array("test_type_id"=>$reportData['test_type_id']));

}





$newt = $this->common_model->getsingle('cp_test_type', array('id' => $reportData['test_type_id']));

$newvaltest = $reportData['test_type_id'];

}else{

 $newvaltest = $data['test_type_id'];



 if (!empty($data['degree_salcius'])) {

  $this->common_model->updateRecords('cp_vital_test_type_unit', array("unit" => $data['degree_salcius'],'degree'=>$data['degree_salcius']), array("test_type_id"=>$data['test_type_id']));

}

}



if(!empty($data['other_doctor_name'])){ 

 $this->common_model->updateRecords('cp_doctor', array("doctor_name" => $data['other_doctor_name'],'other_doctor_name'=>$data['other_doctor_name']), array("doctor_id"=>$reportData['doctor_id']));

 $newd = $this->common_model->getsingle('cp_doctor', array('doctor_id' => $reportData['doctor_id']));

 $newvaldoc = $reportData['doctor_id'];

}else{

 $newvaldoc = $data['doctor_id'];

}





$test_report_data['user_id'] = $data['user_id'];

$test_report_data['doctor_id'] = $newvaldoc;

$test_report_data['test_type_id'] = $newvaltest;

$test_report_data['description'] = $data['description'];

$test_report_data['test_report_title'] = $data['title'];



       /* $test_report_data = array

        ( 

          'user_id'=> $data['user_id'],

      'test_report_title' => $data['test_report_title'],

          'doctor_id'=> $data['doctor_id'],

          'test_type_id'=> $data['test_type_id'],

          'description'=> $data['description'],

          //'image' => $image,

          'create_date'=> date('Y-m-d H:i:s'),         

        );*/

        //$reportId = $this->common_model->addRecords('cp_test_report', $test_report_data);



       // print_r("<pre/>");

       // print_r($test_report_data);

        //die;



        $condition = array('id' => $data['test_report_id']);



        $reportId = $this->common_model->updateRecords('cp_test_report', $test_report_data, $condition);



        $testunit = $this->common_model->getsingle('cp_vital_test_type_unit', array('test_type_id' => $reportData['test_type_id']));

        if($reportId){









         $reminderData1 = array(

          'id' => $reportData['id'],

          'doctor_id' => $newvaldoc,

          'test_type_id' => $newvaltest,

          'unit' => (!empty($testunit->unit) ? $testunit->unit : ''),

          'test_report_title' => $reportData['test_report_title'],

          'description' => $reportData['description'],

          'user_id' => $reportData['user_id'],

          'image_url'=>base_url().'uploads/test_report/'.$reportData['image']                  

        );

         $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Test report added successfully', 'response' => $reminderData1);

       }

       else 

       {

        $resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

      }

      echo json_encode($resp);

    }



    public function addUserContact(){



      $data = $_POST;

      $object_info = $data;

      $required_parameter = array('contact_name','mobile_no','user_id');

      $chk_error = check_required_value($required_parameter, $object_info);

      if ($chk_error) {

        $resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

        echo json_encode($resp);die;

      }

      if(!empty($_FILES['contact_image']['name'])) { 

       $random = $this->generateRandomString(10);

       $ext = pathinfo($_FILES['contact_image']['name'], PATHINFO_EXTENSION);

       $file_name = $random.".".$ext;

       $target_dir = "uploads/contact/";

       $target_file = $target_dir .$file_name;

       if (move_uploaded_file($_FILES["contact_image"]["tmp_name"], $target_file)){

        $contactFile = $file_name;

        $post_data['user_contact_image']  = $contactFile;

      }

    }

      /*$post_data = array

        (

          'contact_name'=> $data['contact_name'],

          'contact_mobile_number'=> $data['mobile_no'],

          'contact_user_id'=> $data['user_id'],

          'user_contact_image'        =>$contactFile,   

          'contact_create_date' => date('Y-m-d h:i:s'),

        );*/



        $post_data['contact_name']  =  $data['contact_name'];

        $post_data['contact_mobile_number']  = $data['mobile_no'];

        $post_data['contact_user_id']  = $data['user_id'];

        $post_data['contact_create_date']  = date('Y-m-d h:i:s');

        

        $contactUserId = $this->common_model->addRecords('cp_user_contact', $post_data);



        if($contactUserId) 

        {

          $contactData = $this->common_model->getSingleRecordById('cp_user_contact', array('contact_id' => $contactUserId));

          $contactData1 = array(

            'id' => $contactData['contact_id'],

            'user_id' => $contactData['contact_user_id'],

            'image_url'=>base_url().'uploads/contact/'.$contactData['user_contact_image']

          );

          $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'contact added successfully', 'response' => $contactData1);

        }else{

         $resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => message(18)));

       }

      //$this->response($resp);

       echo json_encode($resp);



     }



     public function updateUserContact(){

      $data = $_POST;

      $object_info = $data;

      $required_parameter = array('contact_name','mobile_no','contact_id');

      $chk_error = check_required_value($required_parameter, $object_info);

      if ($chk_error) {

        $resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

        echo json_encode($resp);die;

      }

      if(!empty($_FILES['contact_image']['name'])) { 

       $random = $this->generateRandomString(10);

       $ext = pathinfo($_FILES['contact_image']['name'], PATHINFO_EXTENSION);

       $file_name = $random.".".$ext;

       $target_dir = "uploads/contact/";

       $target_file = $target_dir .$file_name;

       if (move_uploaded_file($_FILES["contact_image"]["tmp_name"], $target_file)){

        $contactFile = $file_name;



        $post_data['user_contact_image'] = $contactFile;

      }

    }



    $post_data['contact_name'] = $data['contact_name'];

    $post_data['contact_mobile_number'] = $data['mobile_no'];



     /* $post_data = array

        (

          'contact_name'=> $data['contact_name'],

          'contact_mobile_number'=> $data['mobile_no'],

          'user_contact_image'        =>$contactFile,   

        );*/





        $contactUserId = $this->common_model->updateRecords('cp_user_contact', $post_data,array('contact_id' => $data['contact_id'])); 



        if($contactUserId) 

        {

          $contactData = $this->common_model->getSingleRecordById('cp_user_contact', array('contact_id' =>  $data['contact_id']));

          $contactData1 = array(

            'id' => $contactData['contact_id'],

            'user_id' => $contactData['contact_user_id'],

            'image_url'=>base_url().'uploads/contact/'.$contactData['user_contact_image']

          );

          $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Contact update successfully', 'response' => $contactData1);

        }else{

         $resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => message(18)));

       }

       echo json_encode($resp);

     }



     public function addFamilyContact(){



      $data = $_POST;

      $object_info = $data;

      $required_parameter = array('contact_name','mobile_no','user_id');

      $chk_error = check_required_value($required_parameter, $object_info);

      if ($chk_error) {

        $resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

        echo json_encode($resp);die;

      }

      if(!empty($_FILES['contact_image']['name'])) { 

       $random = $this->generateRandomString(10);

       $ext = pathinfo($_FILES['contact_image']['name'], PATHINFO_EXTENSION);

       $file_name = $random.".".$ext;

       $target_dir = "uploads/contact/";

       $target_file = $target_dir .$file_name;

       if (move_uploaded_file($_FILES["contact_image"]["tmp_name"], $target_file)){

        $contactFile = $file_name;

      }

    }

    $post_data = array

    (

      'contact_name'=> $data['contact_name'],

      'contact_mobile_number'=> $data['mobile_no'],

      'contact_user_id'=> $data['user_id'],

      'user_contact_image'        =>$contactFile,   

      'contact_create_date' => date('Y-m-d h:i:s'),

    );

    $contactUserId = $this->common_model->addRecords('cp_user_contact', $post_data);



    if($contactUserId) 

    {

      $contactData = $this->common_model->getSingleRecordById('cp_user_contact', array('contact_id' => $contactUserId));

      $contactData1 = array(

        'id' => $contactData['contact_id'],

        'user_id' => $contactData['contact_user_id'],

        'image_url'=>base_url().'uploads/contact/'.$contactData['user_contact_image']

      );

      $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'contact added successfully', 'response' => $contactData1);

    }else{

     $resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => message(18)));

   }

      //$this->response($resp);

   echo json_encode($resp);



 }



 public function updateFamilyContact(){

  $data = $_POST;

  $object_info = $data;

  $required_parameter = array('contact_name','mobile_no','contact_id');

  $chk_error = check_required_value($required_parameter, $object_info);

  if ($chk_error) {

    $resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

    echo json_encode($resp);die;

  }

  if(!empty($_FILES['contact_image']['name'])) { 

   $random = $this->generateRandomString(10);

   $ext = pathinfo($_FILES['contact_image']['name'], PATHINFO_EXTENSION);

   $file_name = $random.".".$ext;

   $target_dir = "uploads/contact/";

   $target_file = $target_dir .$file_name;

   if (move_uploaded_file($_FILES["contact_image"]["tmp_name"], $target_file)){

    $contactFile = $file_name;

  }

}

$post_data = array

(

  'contact_name'=> $data['contact_name'],

  'contact_mobile_number'=> $data['mobile_no'],

  'user_contact_image'        =>$contactFile,   

);

$contactUserId = $this->common_model->updateRecords('cp_user_contact', $post_data,array('contact_id' => $data['contact_id'])); 



if($contactUserId) 

{

  $contactData = $this->common_model->getSingleRecordById('cp_user_contact', array('contact_id' =>  $data['contact_id']));

  $contactData1 = array(

    'id' => $contactData['contact_id'],

    'user_id' => $contactData['contact_user_id'],

    'image_url'=>base_url().'uploads/contact/'.$contactData['user_contact_image']

  );

  $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Contact update successfully', 'response' => $contactData1);

}else{

 $resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => message(18)));

}

echo json_encode($resp);

}



  // add user music

public function addUserMusic(){



  $data = $_POST;

  $object_info = $data;

  $required_parameter = array('category_id','user_id','music_title','music_desc','music_artist');

  $chk_error = check_required_value($required_parameter, $object_info);

  if ($chk_error) {

    $resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

    echo json_encode($resp);die;

  }

  if(!empty($_FILES['music_image']['name'])) { 

   $random = $this->generateRandomString(10);

   $ext = pathinfo($_FILES['music_image']['name'], PATHINFO_EXTENSION);

   $music_file_name = $random.".".$ext;

   $target_dir = "uploads/music/image/";

   $target_file = $target_dir .$music_file_name;

   if (move_uploaded_file($_FILES["music_image"]["tmp_name"], $target_file)){

    $musicImage = $music_file_name;

  }

}

if(!empty($_FILES['music_file']['name'])) { 

 $random = $this->generateRandomString(10);

 $ext = pathinfo($_FILES['music_file']['name'], PATHINFO_EXTENSION);

 $file_name = $random.".".$ext;

 $target_dir = "uploads/music/";

 $target_file = $target_dir .$file_name;

 if (move_uploaded_file($_FILES["music_file"]["tmp_name"], $target_file)){

  $musicFile = $file_name;

}

}

$post_data = array

(

  'category_id'=> $data['category_id'],

  'user_id'=> $data['user_id'],

  'music_title'=> $data['music_title'],

  'music_desc'=> $data['music_desc'],

  'music_artist'=> $data['music_artist'],

  'music_image'        =>$musicImage,

  'music_file'        =>$musicFile,   

  'music_created' => date('Y-m-d h:i:s'),

);

$musicUserId = $this->common_model->addRecords('music', $post_data);



if($musicUserId) 

{

  $musicData = $this->common_model->getSingleRecordById('music', array('music_id' => $musicUserId));





  $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'music added successfully', 'response' => array('music_data' => "music added successfully"));



}else{

  $resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

}

echo json_encode($resp);

}



  // update user music

public function updateUserMusic(){



  $data = $_POST;

  $object_info = $data;

  $required_parameter = array('category_id','music_title','music_desc','music_artist','music_id');

  $chk_error = check_required_value($required_parameter, $object_info);

  if ($chk_error) {

    $resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

    echo json_encode($resp);die;

  }

  if(!empty($_FILES['music_image']['name'])) { 

   $random = $this->generateRandomString(10);

   $ext = pathinfo($_FILES['music_image']['name'], PATHINFO_EXTENSION);

   $music_file_name = $random.".".$ext;

   $target_dir = "uploads/music/image/";

   $target_file = $target_dir .$music_file_name;

   if (move_uploaded_file($_FILES["music_image"]["tmp_name"], $target_file)){

    $musicImage = $music_file_name;

    $post_data['music_image'] = $musicImage;

  }

}

if(!empty($_FILES['music_file']['name'])) { 

 $random = $this->generateRandomString(10);

 $ext = pathinfo($_FILES['music_file']['name'], PATHINFO_EXTENSION);

 $file_name = $random.".".$ext;

 $target_dir = "uploads/music/";

 $target_file = $target_dir .$file_name;

 if (move_uploaded_file($_FILES["music_file"]["tmp_name"], $target_file)){

  $musicFile = $file_name;

  $post_data['music_file'] = $musicFile;

}

}

$post_data['category_id'] = $data['category_id'];

$post_data['user_id'] = $data['user_id'];

$post_data['music_title'] = $data['music_title'];

$post_data['music_desc'] = $data['music_desc'];

$post_data['music_artist'] = $data['music_artist'];

$musicUserId = $this->common_model->updateRecords('music', $post_data,array('music_id' => $data['music_id'])); 

if($musicUserId) 

{

  $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'music update successfully', 'response' => array('music_data' => "music update successfully"));



}else{

 $resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

}

echo json_encode($resp);

}



   // add user movie

public function addUserMovie(){



  $data = $_POST;

  $object_info = $data;

  $required_parameter = array('category_id','user_id','movie_title','movie_desc','movie_artist');

  $chk_error = check_required_value($required_parameter, $object_info);

  if ($chk_error) {

    $resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

    echo json_encode($resp);die;

  }

  if(!empty($_FILES['movie_image']['name'])) { 

   $random = $this->generateRandomString(10);

   $ext = pathinfo($_FILES['movie_image']['name'], PATHINFO_EXTENSION);

   $movie_file_name = $random.".".$ext;

   $target_dir = "uploads/movie/image/";

   $target_file = $target_dir .$movie_file_name;

   if (move_uploaded_file($_FILES["movie_image"]["tmp_name"], $target_file)){

    $movieImage = $movie_file_name;

  }

}

if(!empty($_FILES['movie_file']['name'])) { 

 $random = $this->generateRandomString(10);

 $ext = pathinfo($_FILES['movie_file']['name'], PATHINFO_EXTENSION);

 $file_name = $random.".".$ext;

 $target_dir = "uploads/movie/";

 $target_file = $target_dir .$file_name;

 if (move_uploaded_file($_FILES["movie_file"]["tmp_name"], $target_file)){

  $movieFile = $file_name;

}

}

$post_data = array

(

  'category_id'=> $data['category_id'],

  'user_id'=> $data['user_id'],

  'movie_title'=> $data['movie_title'],

  'movie_desc'=> $data['movie_desc'],

  'movie_artist'=> $data['movie_artist'],

  'movie_image'        =>$movieImage,

  'movie_file'        =>$movieFile,   

  'movie_created' => date('Y-m-d h:i:s'),

);

$movieUserId = $this->common_model->addRecords('movie', $post_data);

if($movieUserId) 

{

  $movieData = $this->common_model->getSingleRecordById('movie', array('movie_id' => $movieUserId));

  $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'movie added successfully', 'response' => array('music_data' => "movie added successfully"));



}else{

  $resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

}

echo json_encode($resp);

}



   // update user movie

public function updateUserMovie(){

  $data = $_POST;

  $object_info = $data;

  $required_parameter = array('category_id','movie_title','movie_desc','movie_artist','movie_id');

  $chk_error = check_required_value($required_parameter, $object_info);

  if ($chk_error) {

    $resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

    echo json_encode($resp);die;

  }

  if(!empty($_FILES['movie_image']['name'])) { 

   $random = $this->generateRandomString(10);

   $ext = pathinfo($_FILES['movie_image']['name'], PATHINFO_EXTENSION);

   $movie_file_name = $random.".".$ext;

   $target_dir = "uploads/movie/image/";

   $target_file = $target_dir .$music_file_name;

   if (move_uploaded_file($_FILES["movie_image"]["tmp_name"], $target_file)){

    $movieImage = $music_file_name;

    $post_data['movie_image'] = $movieImage;

  }

}

if(!empty($_FILES['movie_file']['name'])) { 

 $random = $this->generateRandomString(10);

 $ext = pathinfo($_FILES['movie_file']['name'], PATHINFO_EXTENSION);

 $file_name = $random.".".$ext;

 $target_dir = "uploads/movie/";

 $target_file = $target_dir .$file_name;

 if (move_uploaded_file($_FILES["movie_file"]["tmp_name"], $target_file)){

  $movieFile = $file_name;

  $post_data['movie_file'] = $movieFile;

}

}

$post_data['category_id'] = $data['category_id'];

$post_data['user_id'] = $data['user_id'];

$post_data['movie_title'] = $data['movie_title'];

$post_data['movie_desc'] = $data['movie_desc'];

$post_data['movie_artist'] = $data['movie_artist'];

$movieUserId = $this->common_model->updateRecords('movie', $post_data,array('movie_id' => $data['movie_id'])); 



if($movieUserId) 

{

  $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'movie update successfully', 'response' => array('music_data' => "movie update successfully"));



}else{

 $resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

}

echo json_encode($resp);

}

public function updateProfile(){



  $data = $_POST;

  $object_info = $data;

  $required_parameter = array('user_id','mobile_no','name');

  $chk_error = check_required_value($required_parameter, $object_info);

  if ($chk_error) {

    $resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

    echo json_encode($resp);die;

  }

  if(!empty($_FILES['profile_image']['name'])) { 

   $random = $this->generateRandomString(10);

   $ext = pathinfo($_FILES['profile_image']['name'], PATHINFO_EXTENSION);

   $file_name = $random.".".$ext;

   $target_dir = "uploads/profile_images/";

   $target_file = $target_dir .$file_name;

   if (move_uploaded_file($_FILES["profile_image"]["tmp_name"], $target_file)){

    $profileFile = $file_name;

    $post_data['profile_image'] = $profileFile;

  }

}

$post_data['mobile'] = $data['mobile_no'];

$post_data['name'] = $data['name'];

$ProfileUserId = $this->common_model->updateRecords('cp_users', $post_data,array('id' => $data['user_id'])); 

if($ProfileUserId) 

{

  $userData = $this->common_model->getSingleRecordById('cp_users', array('id' =>  $data['user_id']));

  $userData1 = array('id'=>$userData['id'],

   'name'=>$userData['name'],

   'username'=>$userData['username'],

   'email'=>$userData['email'],

   'phone'=>$userData['mobile'],

   'profile_image'=>base_url().'uploads/profile_images/'.$userData['profile_image']

 );

  $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'SUCCESS','response' => array('user_data' => $userData1));                             





        //$resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'user update successfully', 'response' => array('user_data' => "user update successfully"));

}else{

 $resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => message(18)));

}

echo json_encode($resp);



}

public function send_mail() { 



  $data = $_POST;



  $object_info = $data;

  $required_parameter = array('to_email','user_id');

  $chk_error = check_required_value($required_parameter, $object_info);



  if ($chk_error) {

    $resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' =>'YOU_HAVE_MISSED_A_PARAMETER_'. strtoupper($chk_error['param']));

    echo json_encode($resp);die;

  }

  $post_data = array

  (

    'subject'=> (!empty($data['subject']) ? $data['subject'] : ''),

    'message'=> (!empty($data['message']) ? $data['message'] : ''),

    'user_id'=> $data['user_id'],

    'create_email' => date('Y-m-d H:i:s'),

    'email_status' => 1,     

  );

        //echo '<pre>';print_r($_FILES['attachment_file']['name']);die;

  $emailId = $this->common_model->addRecords('cp_emails', $post_data);   

  if(!empty($_FILES['attachment_file']['name'])) {  

    $count = count($_FILES['attachment_file']['name']);

		  //print_r($_FILES['attachment_file']['name']);

    foreach($_FILES['attachment_file']['name'] as $key=>$val){ 



     $random = $this->generateRandomString(10);

     $ext = pathinfo($_FILES['attachment_file']['name'][$key], PATHINFO_EXTENSION);

     $file_name = $random.".".$ext;

     $target_dir = "uploads/email/";

     $target_file = $target_dir .$file_name;

                   //echo $file_name;die;

     if (move_uploaded_file($_FILES["attachment_file"]["tmp_name"][$key], $target_file)){

       $productFile = $file_name;

                     //echo $productFile;die;

     }

     $orderGalleryData = array('attachment_file'=>$productFile,'email_id'=>$emailId,);

     $dfdf = $this->db->insert('cp_emails_attachment',$orderGalleryData);

				  //print_r($orderGalleryData);				 

   }

 } 

 $to_email = explode(',',$data['to_email']);



 foreach($to_email as $to_email_details){



  $to_user_id = $this->common_model->getSingleRecordById('cp_users', array('email' => $to_email_details));

  $other_data = $this->common_model->getsingle("cp_users",array("id" => $to_user_id['id']));







  $session_data = $this->common_model->getsingle("cp_users",array("id" => $data['user_id']));



  $sendnoti = sendNotificationsec($other_data->device_token, $data['user_id'], $to_user_id['id'], $session_data->username,$other_data->username,'5',$other_data->device_id);

		//echo '<pre>';print_r($sendnoti);die;





  $post_data = array("user_id" => $data['user_id'],"user_name" => $session_data->username,"type" =>5,"other_user_id" =>$other_data->id,"other_user_name" => $other_data->username,"create_date" => date('Y-m-d H:i:s'));

  $this->common_model->addRecords("cp_app_notification",$post_data);



  if(!empty($to_user_id)){

    $to_user_id = $to_user_id['id'];

    $toEmailData['user_id'] = $to_user_id;

  }



            //$toEmailData = array('to_email'=>$to_email_details,'email_id'=>$emailId);

  $toEmailData['to_email'] = $to_email_details;

  $toEmailData['email_id'] = $emailId;



  $this->db->insert('cp_to_email',$toEmailData);





}     

if($emailId) 

{

  $attachment = $this->orderImage($emailId);

            //echo $emailId;die;

  $subject = $data['subject'];

  $message = $data['message'];

  foreach($to_email as $to_email_details1){

   $user_name = $to_email_details1;

            //$chk_mail= sendEmail1($user_name,$subject,$message,$attachment);

   $chk_mail= new_send_mail($message, $subject, $user_name,$attachment);





 }

 $emailData = $this->common_model->getSingleRecordById('cp_emails', array('email_id' => $emailId));

 if($emailData['create_email'] == "0000-00-00"){

   $emails_create = $emailData['create_email'];

 }else{

   $emails_create = date("M d, Y", strtotime($emailData['create_email']));

 }

			//echo '<pre>';print_r($emailData);

 $emailData1 = array(



  'subject' => $emailData['subject'],

  'message' => $emailData['message'],

  'user_id' => $emailData['user_id'],

  'create_date' => $emails_create,

  'images'=>$this->orderImage($emailData['email_id'])

);

 $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'Message sent.', 'response' => $emailData1);

} else 

{

  $resp = array('status_code' => ERROR,'status' => 'false', 'message' => 'Some error occured, please try again', 'response' => array('message' => 'Some error occured, please try again'));

}

echo json_encode($resp);

}



public function access_token(){



       //$account_sid = 'SKaeb6d7b26adb79e1d74b7f1889afaad4';

       //$auth_token = 'AIzaSyBZmM30jCgAaA4B1X-ND7L0fB8drAZaZZ0';



      //$client = new Client($account_sid, $auth_token);



      //$twilioAccountSid = 'AC5ac3a5fd26d52942e039e626af48a4b4';

     // $twilioApiKey = 'SKaeb6d7b26adb79e1d74b7f1889afaad4';

     // $twilioApiSecret = 'iR2Qaw0cg7PCwMLvtV8Oqxf6G03X1552';





       //$sid    = "AC50352170c8ed9744b2c4ccb3cf42f4c4";

 $sid    = "AC5ac3a5fd26d52942e039e626af48a4b4";



       //$sid    = "ACa8289b4f00cb3a9d893ab4e94793829e";



       //$token  = "c8ead357ad788b270266ee1d4a82e7bc";

 $token  = "598a5da83008bb2c918fb84aa2195add";



       //$token  = "a652edac6d78a33cac958769631fc591";



 $twilio = new Client($sid, $token);

 $new_key = $twilio->newKeys

 ->create();



       //print_r($new_key);die;           





      //$twilioAccountSid = 'AC50352170c8ed9744b2c4ccb3cf42f4c4';

     // $twilioApiKey = 'c8ead357ad788b270266ee1d4a82e7bc';



      //$twilioAccountSid = 'AC50352170c8ed9744b2c4ccb3cf42f4c4';

      //$twilioAccountSid = 'AC5ac3a5fd26d52942e039e626af48a4b4';  

 $twilioAccountSid = 'AC5ac3a5fd26d52942e039e626af48a4b4'; 

 $twilioApiKey = $new_key->sid;



      //$twilioApiSecret = 'B8ZKYbl5XlbWS43ZRMGFFQ0JXQii2Pqp';

 $twilioApiSecret = $new_key->secret;

      //$outgoingApplicationSid = 'APdbdbe2a3099a474f9b09d0ecaf82b5d7';

 $outgoingApplicationSid = 'AP13639ec86f321bfdf716757d7d45c056';

      //$outgoingApplicationSid = 'AP349673f96c164b9c88a06ee06e56a168';

 $identity = "+12098782148";



 $token = new AccessToken(

  $twilioAccountSid,

  $twilioApiKey,

  $twilioApiSecret,

  3600,

  $identity

);



 $voiceGrant = new VoiceGrant();

 $voiceGrant->setOutgoingApplicationSid($outgoingApplicationSid);



      //$voiceGrant->setOutgoingApplicationSid($outgoingApplicationSid);

     // $voiceGrant->setIncomingAllow(true);





      //$voiceGrant->setIncomingAllow(true);



// Add grant to token

 $token->addGrant($voiceGrant);



// render token to string

 echo $token->toJWT();die;





 /*echo (string)str_replace( '<?xml version="1.0"?>', '', $token->toJWT());die;*/





//header('Content-type: application/json');

//echo json_encode($token->toJWT());



       //$client1 = new AccessToken("ACe2c924ce8254191edcea440ac8802b87","SKaeb6d7b26adb79e1d74b7f1889afaad4","iR2Qaw0cg7PCwMLvtV8Oqxf6G03X1552");







}



public function access_token_new()

{



/**

 * This section ensures that Twilio gets a response.

 */

header('Content-type: text/xml');

echo '<?xml version="1.0" encoding="UTF-8"?>

<Response>

<Say>

This message must be nested in a Response element

in order for Twilio to say it to your caller.

</Say>

</Response>';



}



public function video_room(){



 $sid    = "AC5ac3a5fd26d52942e039e626af48a4b4";

 $token  = "598a5da83008bb2c918fb84aa2195add";

 $twilio = new Client($sid, $token);



 $room = $twilio->video->v1->rooms->create(array("uniqueName" => "DailyStandup"));

 die;



     //print_r($room);die;



     //print_r($room);

     //die;



     //echo $room->sid;die;                     





     //$room = $twilio->video->v1->rooms('DailyStandup')

                         // ->fetch();

     //print($room->uniqueName);

     //die;                     





}

public function video_access_token(){





  $pdata = file_get_contents("php://input");

  $data = json_decode($pdata, true);

  $object_info = $data;

  $required_parameter = array('identity');

  $chk_error = check_required_value($required_parameter, $object_info);

  if ($chk_error) {

    $resp = array('status_code' => MISSING_PARAM,'status' => 'false', 'message' => 'FAILURE', 'response' => array('error' => 'FAILURE', 'error_label' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param'])));

              //$this->response($resp);

    echo json_encode($resp);die;

  }



  $sid    = "AC5ac3a5fd26d52942e039e626af48a4b4";

  $token  = "598a5da83008bb2c918fb84aa2195add";

  $twilio = new Client($sid, $token);

  $new_key = $twilio->newKeys

  ->create();

  $twilioAccountSid = 'AC5ac3a5fd26d52942e039e626af48a4b4'; 

  $twilioApiKey = $new_key->sid;  

  $twilioApiSecret = $new_key->secret;

  $identity = $data['identity'];



  $token = new AccessToken(

    $twilioAccountSid,

    $twilioApiKey,

    $twilioApiSecret,

    3600,

    $identity

  ); 

  $roomName = 'cool room';

  $videoGrant = new VideoGrant();

  $videoGrant->setRoom($roomName);



  $token->addGrant($videoGrant);

        // render token to string

  echo $token->toJWT();        



}

public function video_call_history(){





  $sid    = "AC5ac3a5fd26d52942e039e626af48a4b4";

  $token  = "598a5da83008bb2c918fb84aa2195add";

  $twilio = new Client($sid, $token);



  $calls = $twilio->calls->read();

  foreach ($calls as $record) {



            //print_r("<pre/>");

            //print_r($record);

            //die;



   $callsData[] = array(



    'duration' => $record->duration,

    'from'=>$record->from



  );

 }    



       /* print_r("<pre/>");

        print_r($callsData);

        die;*/

        $resp = array('status_code' => SUCCESS,'status' => 'true', 'message' => 'success', 'response' => $callsData);

        echo json_encode($resp);

        

        





      }



    }

