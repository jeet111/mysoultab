<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Contacts extends MX_Controller {
  public function __construct() {
    parent:: __construct();
    $this->load->library('session');
    $this->load->library('form_validation');
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');       
    $this->load->model('contacts_model');
    $this->load->helper(array('common_helper'));
    $this->load->helper(array('url'));
    $this->load->helper(array('form'));
    $user_id = $this->session->userdata('logged_ins')['id'];
    if(empty($user_id)) {
      redirect('admin/login');
    }      
  }
  /*Created by 95 for show all user's contact*/
  public function contacts_list()
  {
    //die('poewirpoewi');
   $data['menuactive'] = $this->uri->segment(2);
   $data['ContactData']  = $this->contacts_model->getAllContacts();

   // echo "<pre>";  
   // print_r($data['ContactData']);
   // die;
   $this->template->set('title', 'Contacts');
   $this->template->load('admin_dashboard_layout', 'contents', 'usercontact_list',$data);
   
 }

 /*Created by 95 for single contact delete*/
 public function delete_contacts($contact_id)
 {
  
  $this->contacts_model->delete("cp_user_contact",array("contact_id" => $contact_id));
  $this->session->set_flashdata('success', 'Successfully deleted');
  redirect('admin/contacts_list'); 
}




/*Delete email and there attachments from table and uploads also*/
public function deletecontactRecords(){   
  $id = $_POST['id'];
  

  $return = $this->contacts_model->delete("cp_user_contact",array("contact_id" => $id));

  if($return > 0){            
    echo json_encode(array('status'=>1, 'msg'=>'Email Deleted Successfully'));
  }else{
    echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
  }
}



}