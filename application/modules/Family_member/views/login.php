<div class="signup-page">
		<section class="signup">
            <!-- <img src="images/signup-bg.jpg" alt=""> -->
            <div class="container">
                <div class="signup-content">
                    <form method="POST" id="login" class="signup-form" action="<?php echo base_url(); ?>family_member_login">
                        <h2 class="form-title">Account Login</h2>
                        <?php if($this->session->flashdata('message')){
                                ?>
                                <div class="text-center" style="color: green; font-size: 18px;">
                                    <?php echo $this->session->flashdata('message'); ?>
                                </div>
                                <?php
                            }elseif($this->session->flashdata('error')){
                                ?>
                                <div class="text-center" style="color: red; font-size: 18px;">
                                    <?php echo $this->session->flashdata('error'); ?>
                                </div>
                                <?php
                            }  
                        ?>

                        <?php 
                        if($this->session->flashdata('flashSuccess')){
                            ?>
                <div class="text-center" style="color: green; font-size: 18px;">
                    <?php echo $this->session->flashdata('flashSuccess'); ?> 
                </div>
                    <?php      
                        }
                         ?>
						 <?php 
                       if($this->session->flashdata('success')){
                        ?>
                        
                        <div class="text-center" style="color: green; font-size: 18px;">
                                   <?php echo $this->session->flashdata('success'); ?>
                               </div>

                        <?php 
                         }
                         ?>
                        
                        <div class="form-group">
                            <input type="text" class="form-input" name="email" id="email" placeholder="Enter your user id or email" value="<?php if(isset($_COOKIE["email"])) { echo $_COOKIE["email"]; } ?>" />
                            <td class="error"><?php echo form_error('email'); ?></td>
                        </div>                        
                        <div class="form-group">
                            <input type="password" class="form-input" name="password" id="password" placeholder="Enter your password" value="<?php if(isset($_COOKIE["password"])) { echo $_COOKIE["password"]; } ?>" />
                            <span toggle="#password" class="toggle-password"></span>
                            <td class="error"><?php echo form_error('password'); ?></td>
                        </div>
                        <!-- <p class="text-center">
                            <?php //echo validation_errors();

                            if(isset($_GET['error'])) {
                              $error = base64_decode($_GET['error']);

                              echo '<div class="has-error" align="center" style="color:red; font-size:18px;">'.$error.'</div>';  
                            } ?> 
                          </p>-->

                          <div class="form-group">                           
                            <label class="label-agree-term"><input type="checkbox" name="remember" id="remember_me" <?php if(isset($_COOKIE["remember"])) { echo 'checked'; } ?> /> Remember me</label>
                        </div>

                        <div class="form-group">                           
                            <label class="label-agree-term"><a href="forgotpassword" class="term-service">Forgot User Id or Password?</a></label>
                        </div>
						
                        <div class="form-group">
                            <input type="submit" name="submit" id="submit" class="form-submit" value="Login"/>
                        </div>
                    </form>
                    <p class="loginhere">
                        Don't have an account? <a href="signup" class="loginhere-link"> Sign up now</a>
                    </p>
                </div>
            </div>
        </section>
	</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
    <script type="text/javascript">
        $(function() {
          $("#login").validate({
            // Specify validation rules
            rules: {
              email: {
                required: true,
                //email: true
              },
              password: {
                required: true,
                minlength: 5
              }
            },
            // Specify validation error messages
            messages: {
              //email: "Enter your email"
              email: {
                required: "Please enter your user id or email",
               // email: "Please enter valid email"
              },
              password: {
                required: "Please enter your password",
                minlength: "Password must be at least 5 characters"
              },
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
              form.submit();
            }
          });
        });
		
		
		
		
    </script>
	