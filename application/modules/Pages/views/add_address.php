<style>
    .text_heading {
        border-bottom: 0;
    }

    .rem_add .btn.btn-primary {
        margin: 0px 0 15px !important;
    }
</style>
<div class=" text_heading">
    <h3 class="text-center"><i class="fa fa-pencil-square-o"></i> Add Address    </h3>
    <div class="rem_add"><a href="<?php echo base_url(); ?>address_book" class="btn btn-primary">Back</a></div>
</div>

<section class="content photo-list">
    <div class="photo-listMain">
        <form id="addressform">
            <div id="success_message"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Address:<span class="red_star">*</span></label>
                        <textarea class="form-control" id="address" name="address" placeholder="Enter Address"></textarea>
                        <td class="error"><?php echo form_error('address'); ?></td>
                    </div>
                </div>
            </div>

            <div class="box-footerOne">

                <input type="submit" name="submit" class="btn btn-primary" value="Submit">
                 <input type="reset" name="cancel" class="btn btn-primary" value="Cancel" onclick="window.location='<?php echo base_url(); ?>address_book';">

            </div>

        </form>

    </div>

</section>

</aside>
<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDNUQSuu50z139ahVbm7vq9nnz5WwoL3OM&libraries=places"></script>

<script>
    var autocomplete = new google.maps.places.Autocomplete($("#address")[0], {
        componentRestrictions: {
            country: "USA"
        }
    });
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();
        console.log(place.address_components);
    });
    $('#addressform').validate({
        rules: {
            address: "required",

        },

        messages: {
            address: "Please enter address.",
        },

        submitHandler: function(form) {
            $.ajax({
                url: '<?php echo base_url(); ?>Pages/add_address1',
                type: 'post',
                data: $(form).serialize(),
                success: function(response) {
                    if (response == 1) {
                        $('#success_message').fadeIn().html('<div class="alert alert-success message"><button type="button" class="close" data-dismiss="alert">x</button>Address added successfully.</div>');

                        setTimeout(function() {
                            $('#success_message').fadeOut("milliseconds");

                        }, 500);
                    }
                    location.href = '<?php echo base_url(); ?>address_book';
                }

            });

        }

    });
</script>
<script type="text/javascript">
    $(function() {

        var today = new Date();

        $('#datetimepicker').datetimepicker({

            ignoreReadonly: true,

            format: 'YYYY-MM-DD',

            minDate: today

        });

    });
</script>

<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>
