<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Common_model_new extends CI_Model
{
	function __construct()
	{
	}
	public function insertData($table, $datainsert)
	{
		$this->db->insert($table, $datainsert);
		return $this->db->insert_id();
	}
	public function updateData($table, $data, $where)
	{
		
		$this->db->update($table, $data, $where);
		return $this->db->affected_rows();
	}
	public function delete($table, $where)
	{
		$this->db->delete($table, $where);
		return;
	}
	public function deleteFaqs($table, $where)
	{
		extract($where);
		$this->db->where('faq_id', $faq_id);
		$this->db->update($table, array('status' => 3));
		return true;
	}
	public function addRecords($table, $post_data)



	{



		$this->db->insert($table, $post_data);

// echo $this->db->last_query();

		return $this->db->insert_id();
	}
	public function updateRecords($table, $post_data, $where_condition)
	{
		$this->db->where($where_condition);
		$query = $this->db->update($table, $post_data);
		// echo $this->db->last_query();
		// die;
		return $query;
	}

	//  function by ram

	public function allexcludeone($table,$where,$exclude)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('id!=', $exclude);
		$this->db->where($where);
		$query = $this->db->get();
	
		return $query->result_array();
	}

	public function getsingle($table, $where)
	{
		$q = $this->db->get_where($table, $where);
			
		return $q->row();
	}
	public function getsingleorderlimit1($table, $field_first, $table1, $field_second, $where = '', $field, $order_id, $order_by)
	{
		$this->db->select($field);
		$this->db->order_by($order_id, $order_by);
		$this->db->limit('1');
		$this->db->from("$table");
		$this->db->join("$table1", "$table1.$field_second = $table.$field_first", "left");
		if ($where != '') {
			$this->db->where($where);
		}
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			return $q->row();
		} else {
			return false;
		}
	}
	public function getsinglelimit1($table, $where, $order_id, $order_by)
	{
		$this->db->order_by($order_id, $order_by);
		$this->db->limit('1');
		$q = $this->db->get_where($table, $where);
		//echo $this->db->last_query();
		return $q->row();
	}
	public function getsinglelimit1wh($table, $order_id, $order_by)
	{
		$this->db->order_by($order_id, $order_by);
		$this->db->limit('1');
		$this->db->from($table);
		$q = $this->db->get();
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function check_login_credentials($postdata)
	{
		extract($postdata);
		$this->db->select('*');
		//$this->db->from('admin');
		$this->db->from('admin');
		$this->db->where('username', $uname);
		$this->db->where('password', md5($password));
		//$this->db->where('role', 1); 
		$query = $this->db->get();
		return $query->row();
	}
	public function get_row_with_con($table, $where)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		//echo $this->db->last_query();
		//die;
		return $query->row();
	}
	public function getAllwhereorderby($table, $where, $order_id, $order_by)
	{
		$this->db->select('*');
		$this->db->order_by($order_id, $order_by);
		$q = $this->db->get_where($table, $where);
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}

	public function getallwhere_group_by_orderby($table, $where, $order_id, $group_by,$order_by)
	{
		$this->db->select('*');
		$this->db->group_by($group_by); 
		$this->db->order_by($order_id, $order_by);
		$q = $this->db->get_where($table, $where);
		//_dx($this->db->last_query($q));
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();

			$data = obj_to_array($data);
			return $data;
		}
	}


	public function getAll($table)
	{
		$this->db->select('*');
		$q = $this->db->get($table);
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAllFaq()
	{
		$this->db->select('*');
		$this->db->from("faqs");
		$this->db->where("status !=", 3);
		$query = $this->db->get();
		$result = ($query->num_rows() > 0) ? $query->result_array() : FALSE;
		return $result;
	}
	public function getAllor($table, $order_id, $order_by)
	{
		$this->db->select('*');
		$this->db->order_by($order_id, $order_by);
		$q = $this->db->get($table);
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAllorwhere($table, $where, $order_id, $order_by)
	{
		$this->db->select('*');
		$this->db->order_by($order_id, $order_by);
		$q = $this->db->get_where($table, $where);
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAllwhere($table, $where)
	{
		$this->db->select('*');
		$q = $this->db->get_where($table, $where);
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			// echo $this->db->last_query();
			return $data;
		}
	}
	function jointwotablenn($table, $field_first, $table1, $field_second, $where = '', $field, $order_id, $order_by)
	{
		$this->db->select($field);
		$this->db->order_by($order_id, $order_by);
		$this->db->from("$table");
		$this->db->join("$table1", "$table1.$field_second = $table.$field_first");
		if ($where != '') {
			$this->db->where($where);
		}
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $rows) {
				$data[] = $rows;
			}
			$q->free_result();
			return $data;
		}
	}
	public function lang_data()
	{
		if ($this->session->userdata("filterLanguage1")) {
			$lang = $this->session->userdata("filterLanguage1");
		} else {
			$lang = 1;
		}
		$this->db->select('*');
		$this->db->from('language_contents');
		$this->db->where('content_language_id', $lang);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	function getfootercategories($lang)
	{
		$this->db->select('*');
		$this->db->from("gc_footer_category_detail");
		$this->db->join("gc_footer_data_detail", "gc_footer_data_detail.cat_id = gc_footer_category_detail.cat_id", "RIGHT");
		$this->db->where("gc_footer_data_detail.status", 1);
		$this->db->where("gc_footer_category_detail.status", 1);
		$this->db->where("gc_footer_category_detail.lang_id", $lang);
		$this->db->group_by("gc_footer_category_detail.cat_name");
		$this->db->order_by("gc_footer_category_detail.cat_id", "ASC");
		$query = $this->db->get();
		//echo $this->db->last_query();die;    
		foreach ($query->result() as $category) {
			$return[$category->cat_id] = $category;
			$return[$category->cat_id]->subs = $this->get_sub_categories($category->cat_id, $lang); // Get the categories sub categories
		}
		return $return;
	}
	public function get_sub_categories($category_id, $lang)
	{
		$this->db->where('cat_id', $category_id);
		$this->db->where('status', 1);
		$this->db->where('lang_id', $lang);
		$query = $this->db->get('gc_footer_data_detail');
		//echo $this->db->last_query();
		return $query->result();
	}
	function jointwotable($table, $field_first, $table1, $field_second, $where = '', $field)
	{
		$this->db->select($field);
		$this->db->from("$table");
		$this->db->join("$table1", "$table1.$field_second = $table.$field_first", "left");
		if ($where != '') {
			$this->db->where($where);
		}
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			return $q->row();
		} else {
			return false;
		}
	}
	/*Created by 95 for get main category*/
	public function getMainCategory1($value)
	{
		//echo "test";die;
		$this->db->select("*");
		$this->db->from('cp_doctor_categories');
		$this->db->where('dc_id', $value);
		$query = $this->db->get();
		return $query->result_array();
	}
	/*Created by 95 for get all the doctors by category ID*/
	function getDoctorList($id)
	{
		$this->db->select('*');
		$this->db->from('cp_doctor');
		$this->db->where("FIND_IN_SET($id, doctor_category_id)");
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	/*Created by 95 on 28-2-2019 for get the multiple categories of a doctor*/
	public function getCategory1($id)
	{
		$id = explode(',', $id);
		$this->db->select('dc_name');
		$this->db->from('cp_doctor_categories');
		$this->db->where_in('dc_id', $id);
		$query = $this->db->get();
		$result = $query->result_array();
		$last_names1 = array_column($result, 'dc_name');
		$result_data = implode(',', $last_names1);
		return $result_data;
	}
	/*Created by 95 on 28-2-2019 for get the single appointment detail*/
	public function getAppointmentDetail($id = '')
	{
		$this->db->select('*');
		$this->db->from('doctor_appointments a');
		$this->db->join('cp_doctor b', 'b.doctor_id=a.doctor_id', 'left');
		$this->db->join('dr_available_date c', 'c.avdate_id=a.dr_appointment_date_id', 'left');
		$this->db->join('dr_available_time d', 'd.avtime_id=a.dr_appointment_time_id', 'left');
		$this->db->where('a.dr_appointment_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() != 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	public function getAppointmentActivities($appointment_id)
	{
		$sql = "SELECT * FROM `doctor_appointments` JOIN `cp_doctor` ON `cp_doctor`.`doctor_id` = `doctor_appointments`.`doctor_id` LEFT JOIN `dr_available_date` ON `dr_available_date`.`avdate_id` = `doctor_appointments`.`dr_appointment_date_id` LEFT JOIN `dr_available_time` ON `dr_available_time`.`avtime_id` = `doctor_appointments`.`dr_appointment_time_id` WHERE `doctor_appointments`.`dr_appointment_id` = $appointment_id";
		$q = $this->db->query($sql);
		if ($q->num_rows() > 0) {
			return $q->row();
		} else {
			return false;
		}
	}
	public function getAllappointments($user_id)
	{
		$this->db->select('*');
		$this->db->from('doctor_appointments as t1');
		$this->db->join('cp_doctor as t2', 't1.doctor_id = t2.doctor_id', 'LEFT');
		$this->db->join('cp_users as t3', 't3.id = t1.user_id', 'LEFT');
		$this->db->join('dr_available_date as t4', 't4.avdate_id = t1.dr_appointment_date_id', 'LEFT');
		$this->db->join('dr_available_time as t5', 't5.avtime_id = t1.dr_appointment_time_id', 'LEFT');
		$this->db->where('t3.user_role!=', '1');
		$this->db->where('t3.id', $user_id);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function getsearchdata($like, $cat_id)
	{
		$sql = "SELECT * FROM `cp_doctor` WHERE FIND_IN_SET($cat_id,`doctor_category_id`) AND `doctor_name` LIKE '$like%'
	ORDER BY `doctor_id` DESC";
		$q = $this->db->query($sql);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	/*Created by 95 for Get all the activities for show dates hilighted in calender*/
	function getAllappointmets($table, $field_first, $table1, $field_second, $where = '', $field, $group_by, $order_id, $order_by)
	{
		$this->db->select($field);
		$this->db->order_by($order_id, $order_by);
		$this->db->group_by($group_by);
		$this->db->from("$table");
		$this->db->join("$table1", "$table1.$field_second = $table.$field_first");
		if ($where != '') {
			$this->db->where($where);
		}
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			foreach ($q->result_array() as $rows) {
				$data[] = $rows;
			}
			$q->result_array();
			return $data;
		}
	}
	function jointhreetable($table, $field_first, $table1, $field_second, $table2, $field_third, $value, $where = '', $field, $order_id, $order_by)
	{
		$this->db->select($field);
		$this->db->order_by($order_id, $order_by);
		$this->db->from("$table");
		$this->db->join("$table1", "$table1.$field_second = $table.$field_first");
		$this->db->join("$table2", "$table2.$field_third = $table.$value");
		if ($where != '') {
			$this->db->where($where);
		}
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $rows) {
				$data[] = $rows;
			}
			$q->free_result();
			return $data;
		}
	}
	function jointhreetablenn($table, $field_first, $table1, $field_second, $table2, $field_third, $value, $where = '', $field)
	{
		$this->db->select($field);
		$this->db->from("$table");
		$this->db->join("$table1", "$table1.$field_second = $table.$field_first");
		$this->db->join("$table2", "$table2.$field_third = $table.$value");
		if ($where != '') {
			$this->db->where($where);
		}
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			return $q->row();
		} else {
			return false;
		}
	}
	/*Created by 95 for Get all the activities for show dates hilighted in calender*/
	function getAllDoctorappointmets($table, $where = '', $field, $group_by)
	{
		$this->db->select($field);
		$this->db->group_by($group_by);
		$this->db->from("$table");
		if ($where != '') {
			$this->db->where($where);
		}
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			foreach ($q->result_array() as $rows) {
				$data[] = $rows;
			}
			$q->result_array();
			return $data;
		}
	}
	public function getAllorlimit($table, $order_id, $order_by, $limit)
	{
		$this->db->select('*');
		$this->db->order_by($order_id, $order_by);
		$this->db->limit($limit);
		$q = $this->db->get($table);
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAllorlimittrash($table, $order_id, $order_by, $limit)
	{
		$checkLogin = $this->session->userdata('logged_in');
		$this->db->select('*');
		$this->db->order_by($order_id, $order_by);
		$this->db->limit($limit);
		$q = $this->db->get($table);
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$articleDeleteId = $this->Common_model_new->getAllwhere('cp_article_trash', array('article_id' => $row->article_id, 'user_id' => $checkLogin['id']));
				if ($articleDeleteId) {
					continue;
				}
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAllorlimittrashnew($table, $order_id, $order_by, $limit)
	{
		$checkLogin = $this->session->userdata('logged_in');
		$this->db->select('*');
		$this->db->order_by($order_id, $order_by);
		$this->db->limit($limit);
		$q = $this->db->get($table);
		// echo $this->db->last_query();
		// die;
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function music_listing($per_page, $page)
	{
		$sql = 'SELECT * FROM `music` ORDER BY `music_id` DESC LIMIT ' . $page . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		foreach ($q->result_array() as $row) {
			//print_r($row);
			//$data[]  = $row;
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_music_trash', array('music_id' => $row['music_id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = 'SELECT * FROM `music` ORDER BY `music_id` DESC';
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_music_trash', array('music_id' => $row['music_id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	public function movie_listing($per_page, $page)
	{
		$sql = 'SELECT * FROM `movie` ORDER BY `movie_id` DESC LIMIT ' . $page . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		foreach ($q->result_array() as $row) {
			//print_r($row);
			//$data[]  = $row;
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_movie_trash', array('movie_id' => $row['movie_id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = 'SELECT * FROM `movie` ORDER BY `movie_id` DESC';
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_movie_trash', array('movie_id' => $row['movie_id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	function jointwotablegroupby($table, $field_first, $table1, $field_second, $groupby, $where = '', $field)
	{
		$this->db->select($field);
		$this->db->from("$table");
		$this->db->group_by($groupby);
		$this->db->join("$table1", "$table1.$field_second = $table.$field_first");
		if ($where != '') {
			$this->db->where($where);
		}
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $rows) {
				$data[] = $rows;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getsearchdatamusic($like, $per_page, $offset)
	{
		$sql = "SELECT * FROM `music` WHERE `music_title` LIKE '%$like%' OR `music_artist` LIKE '%$like%' ORDER BY `music_id` DESC LIMIT " . $offset . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;	 
		foreach ($q->result_array() as $row) {
			//$data[]  = $row;
			$musicDeleteId = $this->common_model_new->getAllwhere('cp_music_trash', array('music_id' => $row['music_id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = "SELECT * FROM `music` WHERE `music_title` LIKE '%$like%' OR `music_artist` LIKE '%$like%' ORDER BY `music_id` DESC";
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$musicDeleteId = $this->common_model_new->getAllwhere('cp_music_trash', array('music_id' => $row['music_id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	public function getfavoritesearchdatamusic($like, $per_page, $offset)
	{
		//$sql = "SELECT * FROM `cp_music_favorite` WHERE `music_title` LIKE '%$like%' OR `music_artist` LIKE '%$like%' ORDER BY `music_id` DESC LIMIT ".$offset.','.$per_page;
		$sql = "SELECT * FROM `cp_music_favorite` JOIN `music` ON `music`.`music_id` = `cp_music_favorite`.`music_id` WHERE `music`.`music_title` LIKE '%$like%' OR `music`.`music_artist` LIKE '%$like%' ORDER BY `cp_music_favorite`.`music_fav_id` DESC LIMIT " . $offset . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;	 
		foreach ($q->result_array() as $row) {
			//$data[]  = $row;
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_music_trash', array('music_id' => $row['music_id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[] = $row;
		}
		$ret['rows'] = $data;
		//$sql2 = "SELECT * FROM `music` WHERE `music_title` LIKE '%$like%' OR `music_artist` LIKE '%$like%' ORDER BY `music_id` DESC";
		$sql2 = "SELECT * FROM `cp_music_favorite` JOIN `music` ON `music`.`music_id` = `cp_music_favorite`.`music_id` WHERE `music`.`music_title` LIKE '%$like%' OR `music`.`music_artist` LIKE '%$like%' ORDER BY `cp_music_favorite`.`music_fav_id` DESC";
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_music_trash', array('music_id' => $row['music_id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	public function getsearchdatamovie($like, $per_page, $offset)
	{
		$sql = "SELECT * FROM `movie` WHERE `movie_title` LIKE '%$like%' OR `movie_artist` LIKE '$like%' ORDER BY `movie_id` DESC LIMIT " . $offset . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;	 
		foreach ($q->result_array() as $row) {
			//$data[]  = $row;
			$movieDeleteId = $this->common_model_new->getAllwhere('cp_movie_trash', array('movie_id' => $row['movie_id']));
			if ($movieDeleteId) {
				continue;
			}
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = "SELECT * FROM `movie` WHERE `movie_title` LIKE '%$like%' OR `movie_artist` LIKE '%$like%' ORDER BY `movie_id` DESC";
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$movieDeleteId = $this->common_model_new->getAllwhere('cp_movie_trash', array('movie_id' => $row['movie_id']));
			if ($movieDeleteId) {
				continue;
			}
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	public function music_listing_category($where, $per_page, $offset)
	{
		$sql = 'SELECT * FROM `music` WHERE `music_type` = ' . $where . ' ORDER BY `music_id` DESC LIMIT ' . $offset . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		foreach ($q->result_array() as $row) {
			//print_r($row);
			//$data[]  = $row;
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_music_trash', array('music_id' => $row['music_id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = 'SELECT * FROM `music` WHERE `music_type` = ' . $where . ' ORDER BY `music_id` DESC';
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_music_trash', array('music_id' => $row['music_id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	public function movie_listing_category($where, $per_page, $offset)
	{
		$sql = 'SELECT * FROM `movie` WHERE `movie_type` = ' . $where . ' ORDER BY `movie_id` DESC LIMIT ' . $offset . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		// echo $this->db->last_query();
		// die();
		foreach ($q->result_array() as $row) {
			//print_r($row);
			//$data[]  = $row;
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_movie_trash', array('movie_id' => $row['movie_id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = 'SELECT * FROM `movie` WHERE `movie_type` = ' . $where . ' ORDER BY `movie_id` DESC';
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_movie_trash', array('movie_id' => $row['movie_id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	/*Created by 95 on for get the all favorite articls*/
	public function getAllFavoriteArticls($id = '')
	{
		$this->db->select('*');
		$this->db->from('cp_article_favourite a');
		$this->db->join('cp_articles b', 'b.article_id=a.article_id');
		$this->db->where('a.user_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() != 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	public function getfavoritesearchdatamovie($like, $per_page, $offset)
	{
		//$sql = "SELECT * FROM `cp_music_favorite` WHERE `music_title` LIKE '%$like%' OR `music_artist` LIKE '%$like%' ORDER BY `music_id` DESC LIMIT ".$offset.','.$per_page;
		$sql = "SELECT * FROM `cp_movie_favorite` JOIN `movie` ON `movie`.`movie_id` = `cp_movie_favorite`.`movie_id` WHERE `movie`.`movie_title` LIKE '%$like%' OR `movie`.`movie_artist` LIKE '%$like%' ORDER BY `cp_movie_favorite`.`movie_fav_id` DESC LIMIT " . $offset . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;	 
		foreach ($q->result_array() as $row) {
			//$data[]  = $row;
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_movie_trash', array('movie_id' => $row['movie_id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[] = $row;
		}
		$ret['rows'] = $data;
		//$sql2 = "SELECT * FROM `music` WHERE `music_title` LIKE '%$like%' OR `music_artist` LIKE '%$like%' ORDER BY `music_id` DESC";
		$sql2 = "SELECT * FROM `cp_movie_favorite` JOIN `movie` ON `movie`.`movie_id` = `cp_movie_favorite`.`movie_id` WHERE `movie`.`movie_title` LIKE '%$like%' OR `movie`.`movie_artist` LIKE '%$like%' ORDER BY `cp_movie_favorite`.`movie_fav_id` DESC";
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_movie_trash', array('movie_id' => $row['movie_id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	public function article_listing($per_page, $page)
	{
		$checkLogin = $this->session->userdata('logged_in');
		$sql = 'SELECT * FROM `cp_articles` ORDER BY `article_id` DESC LIMIT ' . $page . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;
		foreach ($q->result_array() as $row) {
			//print_r($row);
			//$data[]  = $row;
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_article_trash', array('article_id' => $row['article_id'], 'user_id' => $checkLogin['id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = 'SELECT * FROM `cp_articles` ORDER BY `article_id` DESC';
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_article_trash', array('article_id' => $row['article_id'], 'user_id' => $checkLogin['id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		//print_r($ret);die;
		return $ret;
	}
	public function getsearchdataarticle($like, $per_page, $offset)
	{
		$sql = "SELECT * FROM `cp_articles` WHERE `article_title` LIKE '%$like%' OR `artist_name` LIKE '%$like%' ORDER BY `article_id` DESC LIMIT " . $offset . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;	 
		foreach ($q->result_array() as $row) {
			//$data[]  = $row;
			$movieDeleteId = $this->common_model_new->getAllwhere('cp_article_trash', array('article_id' => $row['article_id']));
			if ($movieDeleteId) {
				continue;
			}
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = "SELECT * FROM `cp_articles` WHERE `article_title` LIKE '%$like%' OR `artist_name` LIKE '%$like%' ORDER BY `article_id` DESC";
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$movieDeleteId = $this->common_model_new->getAllwhere('cp_article_trash', array('article_id' => $row['article_id']));
			if ($movieDeleteId) {
				continue;
			}
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	public function getfavoritearticlelist($per_page, $page)
	{
		$checkLogin = $this->session->userdata('logged_in');
		$sql = "SELECT * FROM `cp_article_favourite` JOIN `cp_articles` ON `cp_articles`.`article_id` = `cp_article_favourite`.`article_id` WHERE `cp_article_favourite`.`user_id` = " . $checkLogin['id'] . " ORDER BY `cp_article_favourite`.`article_fav_id` DESC LIMIT " . $page . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;	 
		foreach ($q->result_array() as $row) {
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_article_trash', array('article_id' => $row['article_id'], 'user_id' => $checkLogin['id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = "SELECT * FROM `cp_article_favourite` JOIN `cp_articles` ON `cp_articles`.`article_id` = `cp_article_favourite`.`article_id` WHERE `cp_article_favourite`.`user_id` = " . $checkLogin['id'] . " ORDER BY `cp_article_favourite`.`article_fav_id` DESC";
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_article_trash', array('article_id' => $row['article_id'], 'user_id' => $checkLogin['id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	public function getsearchfavdataarticle($like, $per_page, $page)
	{
		$checkLogin = $this->session->userdata('logged_in');
		$sql = "SELECT * FROM `cp_article_favourite` JOIN `cp_articles` ON `cp_articles`.`article_id` = `cp_article_favourite`.`article_id` WHERE `cp_articles`.`article_title` LIKE '%$like%' OR `cp_articles`.`artist_name` LIKE '%$like%' AND `cp_article_favourite`.`user_id` = " . $checkLogin['id'] . " ORDER BY `cp_article_favourite`.`article_fav_id` DESC LIMIT " . $page . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;	 
		foreach ($q->result_array() as $row) {
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_article_trash', array('article_id' => $row['article_id'], 'user_id' => $checkLogin['id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = "SELECT * FROM `cp_article_favourite` JOIN `cp_articles` ON `cp_articles`.`article_id` = `cp_article_favourite`.`article_id` WHERE `cp_articles`.`article_title` LIKE '%$like%' OR `cp_articles`.`artist_name` LIKE '%$like%' AND `cp_article_favourite`.`user_id` = " . $checkLogin['id'] . " ORDER BY `cp_article_favourite`.`article_fav_id` DESC";
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$musicDeleteId = $this->Common_model_new->getAllwhere('cp_article_trash', array('article_id' => $row['article_id'], 'user_id' => $checkLogin['id']));
			if ($musicDeleteId) {
				continue;
			}
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	function jointwotablegroupby_order($table, $field_first, $table1, $field_second, $groupby, $where = '', $field, $order_id, $order_by)
	{
		$this->db->select($field);
		$this->db->from("$table");
		$this->db->group_by($groupby);
		$this->db->order_by($order_id, $order_by);
		$this->db->join("$table1", "$table1.$field_second = $table.$field_first");
		if ($where != '') {
			$this->db->where($where);
		}
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $rows) {
				$data[] = $rows;
			}
			$q->free_result();
			return $data;
		}
	}
	public function nearbyrest_listing($per_page, $page)
	{
		$checkLogin = $this->session->userdata('logged_in');
		$user_id = $checkLogin['id'];
		$sql = 'SELECT * FROM `cp_restaurant` WHERE `user_id` = ' . $user_id . ' ORDER BY `rest_id` ASC LIMIT ' . $page . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;
		foreach ($q->result_array() as $row) {
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = 'SELECT * FROM `cp_restaurant` WHERE `user_id` = ' . $user_id . ' ORDER BY `rest_id` ASC';
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		//print_r($ret);die;
		return $ret;
	}
	public function getsearchdatarestaurant($like, $per_page, $offset)
	{
		$checkLogin = $this->session->userdata('logged_in');
		$user_id = $checkLogin['id'];
		$sql = "SELECT * FROM `cp_restaurant` WHERE `rest_name` LIKE '%$like%' AND `user_id` = " . $user_id . " ORDER BY `rest_id` ASC LIMIT " . $offset . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;	 
		foreach ($q->result_array() as $row) {
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = "SELECT * FROM `cp_restaurant` WHERE `rest_name` LIKE '%$like%' AND `user_id` = " . $user_id . " ORDER BY `rest_id` ASC";
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	/*All banks listing*/
	public function bank_listing($per_page, $page)
	{
		$checkLogin = $this->session->userdata('logged_in');
		$user_id = $checkLogin['id'];
		$sql = 'SELECT * FROM `cp_bank` WHERE `user_id` = ' . $user_id . ' ORDER BY `bank_id` ASC LIMIT ' . $page . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = 'SELECT * FROM `cp_bank` WHERE `user_id` = ' . $user_id . ' ORDER BY `bank_id` ASC';
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	/*Bank searching*/
	public function get_searched_bank($like, $per_page, $offset)
	{
		$checkLogin = $this->session->userdata('logged_in');
		$user_id = $checkLogin['id'];
		$sql = "SELECT * FROM `cp_bank` WHERE `bank_name` LIKE '%$like%' AND `user_id` = " . $user_id . " ORDER BY `bank_id` ASC LIMIT " . $offset . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = "SELECT * FROM `cp_bank` WHERE `bank_name` LIKE '%$like%' AND `user_id` = " . $user_id . " ORDER BY `bank_id` ASC";
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	public function getfavoriterestaurantlist($per_page, $page)
	{
		$checkLogin = $this->session->userdata('logged_in');
		//$sql = 'SELECT * FROM `cp_fav_restaurant` JOIN `cp_restaurant` ON `cp_restaurant`.`rest_id` = `cp_fav_restaurant`.`restaurant_id` WHERE `cp_fav_restaurant`.`user_id` =  '.$checkLogin['id'].' ORDER BY `fav_rest_id` DESC LIMIT '.$page.','.$per_page;
		$sql = 'SELECT * FROM `cp_fav_restaurant` WHERE `cp_fav_restaurant`.`user_id` =  ' . $checkLogin['id'] . ' ORDER BY `fav_rest_id` DESC LIMIT ' . $page . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;
		foreach ($q->result_array() as $row) {
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = 'SELECT * FROM `cp_fav_restaurant` WHERE `cp_fav_restaurant`.`user_id` =  ' . $checkLogin['id'] . ' ORDER BY `fav_rest_id` DESC';
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		//print_r($ret);die;
		return $ret;
	}
	public function getsearchdatafavrestaurant($like, $per_page, $page)
	{
		$checkLogin = $this->session->userdata('logged_in');
		$user_id = $checkLogin['id'];
		//$sql = "SELECT * FROM `cp_fav_restaurant` JOIN `cp_restaurant` ON `cp_restaurant`.`rest_id` = `cp_fav_restaurant`.`restaurant_id` WHERE `fav_rest_name` LIKE '%$like%' AND `cp_fav_restaurant`.`user_id` = ".$user_id." ORDER BY `fav_rest_id` DESC LIMIT ".$page.','.$per_page;
		$sql = "SELECT * FROM `cp_fav_restaurant` WHERE  `fav_rest_name` LIKE '%$like%' AND `cp_fav_restaurant`.`user_id` =  " . $checkLogin['id'] . " ORDER BY `fav_rest_id` DESC LIMIT " . $page . "," . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;	 
		foreach ($q->result_array() as $row) {
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = "SELECT * FROM `cp_fav_restaurant` WHERE  `fav_rest_name` LIKE '%$like%' AND `cp_fav_restaurant`.`user_id` =  " . $checkLogin['id'] . " ORDER BY `fav_rest_id` DESC";
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	public function favorite_bank_listing($per_page, $page)
	{
		$checkLogin = $this->session->userdata('logged_in');
		$user_id = $checkLogin['id'];
		$sql = 'SELECT * FROM `cp_fav_banks` WHERE `user_id` = ' . $user_id . ' ORDER BY `fav_bank_id` ASC LIMIT ' . $page . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;
		foreach ($q->result_array() as $row) {
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = 'SELECT * FROM `cp_fav_banks` WHERE `user_id` = ' . $user_id . ' ORDER BY `fav_bank_id` ASC';
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		//print_r($ret);die;
		return $ret;
	}
	/*==========================*/
	public function get_searched_favorite_banks($like, $per_page, $offset)
	{
		$checkLogin = $this->session->userdata('logged_in');
		$user_id = $checkLogin['id'];
		$sql = "SELECT * FROM `cp_fav_banks` WHERE `fav_bank_name` LIKE '%$like%' AND `user_id` = " . $user_id . " ORDER BY `fav_bank_id` ASC LIMIT " . $offset . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;	 
		foreach ($q->result_array() as $row) {
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = "SELECT * FROM `cp_fav_banks` WHERE `fav_bank_name` LIKE '%$like%' AND `user_id` = " . $user_id . " ORDER BY `fav_bank_id` ASC";
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	public function grocery_listing($per_page, $page)
	{
		$checkLogin = $this->session->userdata('logged_in');
		$sql = 'SELECT * FROM `cp_grocery` WHERE `user_id` =  ' . $checkLogin['id'] . ' ORDER BY `grocery_id` ASC LIMIT ' . $page . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;
		foreach ($q->result_array() as $row) {
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = 'SELECT * FROM `cp_grocery` WHERE `user_id` =  ' . $checkLogin['id'] . ' ORDER BY `grocery_id` ASC';
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		//print_r($ret);die;
		return $ret;
	}
	public function getsearchdatagrocery($like, $per_page, $offset)
	{
		$checkLogin = $this->session->userdata('logged_in');
		$user_id = $checkLogin['id'];
		$sql = "SELECT * FROM `cp_grocery` WHERE `grocery_name` LIKE '%$like%' AND `user_id` = " . $user_id . " ORDER BY `grocery_id` ASC LIMIT " . $offset . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;	 
		foreach ($q->result_array() as $row) {
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = "SELECT * FROM `cp_grocery` WHERE `grocery_name` LIKE '%$like%' AND `user_id` = " . $user_id . " ORDER BY `grocery_id` ASC";
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	public function getfavoritegrocerylist($per_page, $page)
	{
		$checkLogin = $this->session->userdata('logged_in');
		$sql = 'SELECT * FROM `cp_fav_grocery` WHERE `cp_fav_grocery`.`user_id` =  ' . $checkLogin['id'] . ' ORDER BY `fav_grocery_id` DESC LIMIT ' . $page . ',' . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;
		foreach ($q->result_array() as $row) {
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = 'SELECT * FROM `cp_fav_grocery` WHERE `cp_fav_grocery`.`user_id` =  ' . $checkLogin['id'] . ' ORDER BY `fav_grocery_id` DESC';
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		//print_r($ret);die;
		return $ret;
	}
	public function getsearchdatafavgrocery($like, $per_page, $page)
	{
		$checkLogin = $this->session->userdata('logged_in');
		$user_id = $checkLogin['id'];
		$sql = "SELECT * FROM `cp_fav_grocery` WHERE  `fav_grocery_name` LIKE '%$like%' AND `cp_fav_grocery`.`user_id` =  " . $checkLogin['id'] . " ORDER BY `fav_grocery_id` DESC LIMIT " . $page . "," . $per_page;
		$q = $this->db->query($sql);
		$data =  array();
		//echo $this->db->last_query();die;	 
		foreach ($q->result_array() as $row) {
			$data[] = $row;
		}
		$ret['rows'] = $data;
		$sql2 = "SELECT * FROM `cp_fav_grocery` WHERE  `fav_grocery_name` LIKE '%$like%' AND `cp_fav_grocery`.`user_id` =  " . $checkLogin['id'] . " ORDER BY `fav_grocery_id` DESC";
		$q = $this->db->query($sql2);
		$data = array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		$ret['num_rows'] = count($data);
		return $ret;
	}
	public function getAllwhereorderbylimit($table, $where, $order_id, $order_by, $limit)
	{
		$this->db->select('*');
		$this->db->order_by($order_id, $order_by);
		$this->db->limit($limit);
		$q = $this->db->get_where($table, $where);
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAllPopular()
	{
		$this->db->select('*');
		$this->db->order_by('music_view', 'DESC');
		$this->db->limit(10);
		$q = $this->db->get('music');
		// echo $this->db->last_query();
		// die();
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAllRecent()
	{
		$this->db->select('*');
		$this->db->order_by('music_id', 'DESC');
		$this->db->limit(10);
		$q = $this->db->get('music');
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAllCategory()
	{
		$this->db->select('*');
		$this->db->from('cp_music_category');
		$this->db->where('music_category_status', 1);
		$this->db->order_by('music_category_id', 'DESC');
		$this->db->limit(10);
		$q = $this->db->get();
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAllMyMusic()
	{
		$this->db->select('*');
		$this->db->order_by('music_id', 'DESC');
		$this->db->limit(10);
		$q = $this->db->get_where('music', array('user_id' => $this->session->userdata("logged_in")['id']));
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAllBanner($value = '')
	{
		$this->db->select('*');
		$this->db->from('cp_music_banner');
		$this->db->where('music_banner_status', 1);
		$this->db->order_by('music_banner_id', 'DESC');
		//$this->db->limit(3);
		$q = $this->db->get();
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function totalgetinfo($table, $order_id, $order_by, $limit)
	{
		$this->db->order_by($order_id, $order_by);
		$this->db->limit($limit);
		$this->db->from($table);
		$q = $this->db->get();
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getlikepopular($like)
	{
		$sql2 = "SELECT * FROM `music` WHERE `music_title` LIKE '%$like%' ORDER BY `music_view` DESC LIMIT 10";
		$q = $this->db->query($sql2);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function getlikerecent($like)
	{
		$sql2 = "SELECT * FROM `music` WHERE `music_title` LIKE '%$like%' ORDER BY `music_id` DESC LIMIT 10";
		$q = $this->db->query($sql2);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function getlikecategory($like)
	{
		$sql2 = "SELECT * FROM `cp_music_category` WHERE `music_category_name` Like '%$like%' AND `music_category_status` = 1 ORDER BY `music_category_id` DESC LIMIT 10";
		$q = $this->db->query($sql2);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function getlikemymusic($like, $user_id)
	{
		$sql2 = "SELECT * FROM `music` WHERE `music_title` Like '%$like%' AND `user_id` = '$user_id' ORDER BY `music_id` DESC LIMIT 10";
		$q = $this->db->query($sql2);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function getlikepopmusic($like)
	{
		$sql2 = "SELECT * FROM `music` WHERE `music_title` Like '%$like%' ORDER BY `music_view` DESC";
		$q = $this->db->query($sql2);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function getlikerecmusic($like)
	{
		$sql2 = "SELECT * FROM `music` WHERE `music_title` Like '%$like%' ORDER BY `music_id` DESC";
		$q = $this->db->query($sql2);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function getlikemymmusic($like, $user_id)
	{
		$sql2 = "SELECT * FROM `music` WHERE `music_title` Like '%$like%' AND `user_id` = '$user_id' ORDER BY `music_id` DESC";
		$q = $this->db->query($sql2);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function getlikecat($like, $category_id)
	{
		$sql2 = "SELECT * FROM `music` WHERE `music_title` Like '%$like%' AND `category_id` = '$category_id' ORDER BY `music_id` DESC";
		$q = $this->db->query($sql2);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function getcatsearch($like)
	{
		$sql2 = "SELECT * FROM `cp_music_category` WHERE `music_category_name` Like '%$like%' AND `music_category_status` = 1 ORDER BY `music_category_id` DESC";
		$q = $this->db->query($sql2);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	function jointwotableor($table, $field_first, $table1, $field_second, $where = '', $order_id, $order_by, $field)
	{
		$this->db->select($field);
		$this->db->order_by($order_id, $order_by);
		$this->db->from("$table");
		$this->db->join("$table1", "$table1.$field_second = $table.$field_first", "left");
		if ($where != '') {
			$this->db->where($where);
		}
		$q = $this->db->get();
		//echo $this->db->last_query();die;
		if ($q->num_rows() > 0) {
			return $q->row();
		} else {
			return false;
		}
	}
	function getlikefavmusic($like, $user_id)
	{
		$sql = "SELECT * FROM `cp_music_favorite` LEFT JOIN `music` ON `music`.`music_id` = `cp_music_favorite`.`music_id` WHERE `music`.`music_title` Like '%$like%' AND `cp_music_favorite`.`user_id` = '$user_id' ORDER BY `cp_music_favorite`.`music_fav_id` DESC";
		$q = $this->db->query($sql);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function getfavlimitsearch($like, $user_id)
	{
		$sql = "SELECT * FROM `cp_music_favorite` LEFT JOIN `music` ON `music`.`music_id` = `cp_music_favorite`.`music_id` WHERE `music`.`music_title` Like '%$like%' AND `cp_music_favorite`.`user_id` = '$user_id' ORDER BY `cp_music_favorite`.`music_fav_id` DESC LIMIT 10";
		$q = $this->db->query($sql);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function getfavlimitsearchli($like, $user_id)
	{
		$sql = "SELECT * FROM `cp_music_favorite` LEFT JOIN `music` ON `music`.`music_id` = `cp_music_favorite`.`music_id` WHERE `music`.`music_title` Like '%$like%' AND `cp_music_favorite`.`user_id` = '$user_id' ORDER BY `cp_music_favorite`.`music_fav_id` DESC";
		$q = $this->db->query($sql);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function get_all_results_withFields_Like_con($table, $fields, $where, $likeField, $likeVal)
	{
		$this->db->select($fields);
		$this->db->like($likeField, $likeVal);
		if (!empty($where)) {
			$this->db->where($where);
		}
		$this->db->from($table);
		$query = $this->db->get();
		return $query->result();
	}
	/*=======Article Start========*/
	public function getAllPopularArticle($category_id = '')
	{
		$this->db->select('*');
		$this->db->order_by('article_view', 'DESC');
		$this->db->limit(10);
		$q = $this->db->get_where('cp_articles', array('category_id' => $category_id));
		// echo $this->db->last_query();
		// die();
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAllRecentArticle($category_id = '')
	{
		$this->db->select('*');
		$this->db->order_by('article_id', 'DESC');
		$this->db->limit(10);
		$q = $this->db->get_where('cp_articles', array('category_id' => $category_id));
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAllCategoryArticle()
	{
		$this->db->select('*');
		$this->db->from('cp_article_category');
		$this->db->where('article_category_status', 1);
		$this->db->order_by('article_category_id', 'DESC');
		$this->db->limit(10);
		$q = $this->db->get();
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAllMyArticle($category_id = '')
	{
		$this->db->select('*');
		$this->db->order_by('article_id', 'DESC');
		$this->db->limit(10);
		$q = $this->db->get_where('cp_articles', array('user_id' => $this->session->userdata("logged_in")['id'], 'category_id' => $category_id));
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getAllMyFavArticle($category_id = '')
	{
		$this->db->select('*');
		$this->db->from('cp_article_favourite');
		$this->db->join('cp_articles', 'cp_articles.article_id=cp_article_favourite.article_id', 'left');
		$this->db->where('cp_article_favourite.user_id', $this->session->userdata("logged_in")['id']);
		$this->db->where('cp_articles.category_id', $category_id);
		$this->db->order_by('cp_article_favourite.article_fav_id', 'DESC');
		$this->db->limit(10);
		$q = $this->db->get();
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}
	function getEmails1($user_id)
	{
		$new_array = '';
		$this->db->select('*');
		$this->db->from('cp_to_email');
		$this->db->join('cp_emails', 'cp_to_email.email_id = cp_emails.email_id');
		$this->db->join('cp_users', 'cp_to_email.user_id = cp_users.id');
		$this->db->where('cp_to_email.user_id', $user_id);
		$this->db->order_by('cp_emails.email_id', 'DESC');
		$query = $this->db->get();
		$result = $query->result_array();
		//echo $this->db->last_query();die;
		// print_r("<pre/>");
		// print_r($result);
		// die;
		foreach ($result as $result_details) {
			$emailId = $this->Common_model_new->getsingle('cp_email_trash', array('email_id' => $result_details['email_id'], 'user_id' => $user_id));
			//print_r("<pre/>");
			//print_r($emailId);
			//die;
			if ($emailId) {
				continue;
			}
			$new_array = $result_details;
		}
		return $new_array;
	}
	function jointwotablennquery($table, $field_first, $table1, $field_second, $where = '', $field, $order_id, $order_by)
	{
		$checkLogin = $this->session->userdata('logged_in');
		$this->db->select($field);
		$this->db->order_by($order_id, $order_by);
		$this->db->from("$table");
		$this->db->join("$table1", "$table1.$field_second = $table.$field_first");
		if ($where != '') {
			$this->db->where($where);
		}
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $rows) {
				$emailId = $this->Common_model_new->getsingle('cp_email_trash', array('email_id' => $rows->email_id, 'user_id' => $checkLogin['id']));
				//print_r("<pre/>");
				//print_r($emailId);
				//die;
				if ($emailId) {
					continue;
				}
				$data[] = $rows;
			}
			$q->free_result();
			return $data;
		}
	}
	public function getlikepopulararticle($like, $segment)
	{
		$sql2 = "SELECT * FROM `cp_articles` WHERE `article_title` LIKE '%$like%' AND `category_id` = '$segment' ORDER BY `article_view` DESC LIMIT 10";
		$q = $this->db->query($sql2);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function getlikerecentarticle($like, $segment)
	{
		$sql2 = "SELECT * FROM `cp_articles` WHERE `article_title` LIKE '%$like%' AND `category_id` = '$segment' ORDER BY `article_id` DESC LIMIT 10";
		$q = $this->db->query($sql2);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function getcatsearcharticle($like)
	{
		$sql2 = "SELECT * FROM `cp_article_category` WHERE `article_category_name` Like '%$like%' AND `article_category_status` = 1 ORDER BY `article_category_id` DESC";
		$q = $this->db->query($sql2);
		$data =  array();
		//echo $this->db->last_query();die;  
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function getlikemyarticle($like, $user_id, $segment)
	{
		$sql2 = "SELECT * FROM `cp_articles` WHERE `article_title` Like '%$like%' AND `user_id` = '$user_id' AND `category_id` = '$segment' ORDER BY `article_id` DESC";
		$q = $this->db->query($sql2);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	function getlikefavarticle($like, $user_id)
	{
		$sql = "SELECT * FROM `cp_article_favourite` LEFT JOIN `cp_articles` ON `cp_articles`.`article_id` = `cp_article_favourite`.`article_id` WHERE `cp_articles`.`article_title` Like '%$like%' AND `cp_article_favourite`.`user_id` = '$user_id' ORDER BY `cp_article_favourite`.`article_fav_id` DESC";
		$q = $this->db->query($sql);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function getlikepopulararticleview($like, $category_id)
	{
		$sql2 = "SELECT * FROM `cp_articles` WHERE `article_title` LIKE '%$like%' AND `category_id` = '$category_id' ORDER BY `article_view` DESC";
		$q = $this->db->query($sql2);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function getlikerecentarticleview($like, $category_id)
	{
		$sql2 = "SELECT * FROM `cp_articles` WHERE `article_title` LIKE '%$like%' AND `category_id` = '$category_id' ORDER BY `article_id` DESC";
		$q = $this->db->query($sql2);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function getlikemyarticleview($like, $user_id, $category_id)
	{
		$sql2 = "SELECT * FROM `cp_articles` WHERE `article_title` Like '%$like%' AND `user_id` = '$user_id' AND `category_id` = '$category_id' ORDER BY `article_id` DESC";
		$q = $this->db->query($sql2);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	function getlikefavarticleview($like, $user_id, $category_id)
	{
		$sql = "SELECT * FROM `cp_article_favourite` LEFT JOIN `cp_articles` ON `cp_articles`.`article_id` = `cp_article_favourite`.`article_id` WHERE `cp_articles`.`article_title` Like '%$like%' AND `cp_article_favourite`.`user_id` = '$user_id' AND `category_id` = '$category_id' ORDER BY `cp_article_favourite`.`article_fav_id` DESC";
		$q = $this->db->query($sql);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	public function getsearchdatamoviecat($like)
	{
		$sql = "SELECT * FROM `cp_movie_category` WHERE `movie_category_name` Like '%$like%' AND `movie_category_status` = 1";
		$q = $this->db->query($sql);
		$data =  array();
		foreach ($q->result_array() as $row) {
			$data[]  = $row;
		}
		return $data;
	}
	/*=======Article End=======*/
	/*Recent Articles according to type and category */
	public function AllRecentArticle($category_id = '')
	{
		$this->db->select('*');
		$this->db->order_by('article_id', 'DESC');
		$this->db->limit(5);
		$q = $this->db->get_where('cp_articles', array('category_id' => $category_id));
		// echo $this->db->last_query();
		// die();
		$num_rows = $q->num_rows();
		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}

	/*******Yoga video  */

	public function getAllhelpwith($table)
	{
		$this->db->select('help_with');
		$this->db->distinct();
		$query = $this->db->get('yoga_videos');

		$num_rows = $query->num_rows();
		$data = $query->result();
		if ($num_rows > 0) {
			foreach ($query->result_array() as $row) {
				$data[]  = $row;
			}
			return $data;
		}
	}
	public function getAllbodypart($table)
	{

		$this->db->select('body_part');
		$this->db->distinct();
		$query = $this->db->get('yoga_videos');
		$num_rows = $query->num_rows();
		$data = $query->result();
		if ($num_rows > 0) {
			foreach ($query->result_array() as $row) {
				$data[]  = $row;
			}
			return $data;
		}
	}
	public function getAllyogastyle($table)
	{
		$this->db->distinct();
		$this->db->select('yoga_style');
		$query = $this->db->get('yoga_videos');
		$num_rows = $query->num_rows();
		$data = $query->result();
		if ($num_rows > 0) {
			foreach ($query->result_array() as $row) {
				$data[]  = $row;
			}
			return $data;
		}
	}
	public function getAllyogaduration($table)
	{
		$this->db->distinct();
		$this->db->select('yoga_duration');
		$query = $this->db->get('yoga_videos');

		$num_rows = $query->num_rows();
		$data = $query->result();
		if ($num_rows > 0) {
			foreach ($query->result_array() as $row) {
				$data[]  = $row;
			}
			return $data;
		}
	}
	public function getAllyogatype($table)
	{
		$this->db->select('yoga_type');
		$this->db->distinct();
		$query = $this->db->get('yoga_videos');
		$num_rows = $query->num_rows();
		$data = $query->result();
		if ($num_rows > 0) {
			foreach ($query->result_array() as $row) {
				$data[]  = $row;
			}
			return $data;
		}
	}
	public function getAllyogapose($table)
	{


		$this->db->distinct();
		$this->db->select('yoga_pose');
		$query = $this->db->get('yoga_videos');

		$num_rows = $query->num_rows();
		$data = $query->result();
		if ($num_rows > 0) {
			return $data;
		}
	}
	public function getAllyogalevel($table)
	{
		$this->db->distinct();
		$this->db->select('yoga_level');
		$query = $this->db->get('yoga_videos');

		$num_rows = $query->num_rows();
		$data = $query->result();
		if ($num_rows > 0) {
			return $data;
		}
	}
	public function getAllYogavideowhere($table, $where)
	{

		$this->db->select('*');
		$q = $this->db->get_where($table, $where);

		// print_r($this->db->last_query());    
		$num_rows = $q->num_rows();

		if ($num_rows > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			$q->free_result();
			return $data;
			// print_r($data);
		}
	}

	/**********************Search Video result */


	public function get_searched_yoga_video($helpwithRes, $bodypartRes, $yogastyleRes, $yogaDurRes, $yogaTypeRes, $yogaPoseRes, $yogaLevelRes)
	{
		// _d($helpwithRes);
		// _d($bodypartRes);
		//  _d($yogastyleRes);
		// _d($yogaDurRes);
		// _d($yogaTypeRes);
		//  _d($yogaPoseRes);
		//  _d($yogaLevelRes);
		$where_condition = "";
		if (!empty($helpwithRes) && $helpwithRes != '') {
			$help_with_result = "'" . implode("', '", $helpwithRes) . "'";
			if ($help_with_result != '') {
				if ($where_condition != "") {
					$where_condition .= $where_condition . " and `help_with` IN (" . $help_with_result . ")";
				} else {
					$where_condition .= "`help_with` IN (" . $help_with_result . ")";
				}
			}
		}
		if (!empty($bodypartRes) && $bodypartRes != '') {
			$body_part_result = "'" . implode("', '", $bodypartRes) . "'";
			if ($body_part_result != '') {
				if ($where_condition != "") {
					$where_condition .= $where_condition . " and `body_part` IN (" . $body_part_result . ")";
				} else {
					$where_condition .= "`body_part` IN (" . $body_part_result . ")";
				}
			}
		}
		if (!empty($yogastyleRes) && $yogastyleRes != '') {
			$yoga_style_result = "'" . implode("', '", $yogastyleRes) . "'";
			if ($yoga_style_result != '') {
				if ($where_condition != "") {
					$where_condition .= $where_condition . " and `yoga_style` IN (" . $yoga_style_result . ")";
				} else {
					$where_condition .= "`yoga_style` IN (" . $yoga_style_result . ")";
				}
			}
		}
		if (!empty($yogaDurRes) && $yogaDurRes != '') {
			$yoga_duration_result = "'" . implode("', '", $yogaDurRes) . "'";
			if ($yoga_duration_result != '') {
				if ($where_condition != "") {
					$where_condition .= $where_condition . " and `yoga_duration` IN (" . $yoga_duration_result . ")";
				} else {
					$where_condition .= "`yoga_duration` IN (" . $yoga_duration_result . ")";
				}
			}
		}
		if (!empty($yogaTypeRes) && $yogaTypeRes != '') {
			$yoga_type_result = "'" . implode("', '", $yogaTypeRes) . "'";
			if ($yoga_type_result != '') {
				if ($where_condition != "") {
					$where_condition .= $where_condition . " and `yoga_type` IN (" . $yoga_type_result . ")";
				} else {
					$where_condition .= "`yoga_type` IN (" . $yoga_type_result . ")";
				}
			}
		}
		if (!empty($yogaPoseRes) && $yogaPoseRes != '') {
			$yoga_pose_result = "'" . implode("', '", $yogaPoseRes) . "'";
			if ($yoga_pose_result != '') {
				if ($where_condition != "") {
					$where_condition .= $where_condition . " and `yoga_pose` IN (" . $yoga_pose_result . ")";
				} else {
					$where_condition .= "`yoga_pose` IN (" . $yoga_pose_result . ")";
				}
			}
		}
		if (!empty($yogaLevelRes) && $yogaLevelRes != '') {
			$yoga_level_result = "'" . implode("', '", $yogaLevelRes) . "'";
			if ($yoga_level_result != '') {
				if ($where_condition != "") {
					$where_condition .= $where_condition . " and `yoga_level` IN (" . $yoga_level_result . ")";
				} else {
					$where_condition .= "`yoga_level` IN (" . $yoga_level_result . ")";
				}
			}
		}
		// $sql = "SELECT `yoga_url` FROM `yoga_videos` WHERE `help_with` IN ($help_with_result) AND `body_part` IN ($body_part_result) AND `yoga_style` IN ($yoga_style_result) AND `yoga_duration` IN ($yoga_duration_result) AND `yoga_type` IN ($yoga_type_result) AND `yoga_pose` IN ($yoga_pose_result) AND `yoga_level` IN ($yoga_level_result)";

		$sql = "SELECT `yoga_url` FROM `yoga_videos` WHERE " . $where_condition;

		$q = $this->db->query($sql);
		$data =  array();
		// echo $this->db->last_query();die;	 
		foreach ($q->result_array() as $row) {
			$data[] = $row;
		}
		$ret = $data;
		return $ret;
	}

	/********************* */
}
