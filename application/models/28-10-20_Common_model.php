<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_model extends CI_Model

{
	function checkRow($params = array()){
		$this->db->select('*');
		$this->db->from('cp_users');

		if(array_key_exists("email",$params)) {
			$this->db->where("(email = '".$params['email']."')");
		}
		if(array_key_exists("username",$params)) {
			$this->db->where("(username = '".$params['username']."')");
		}
		if(array_key_exists("password",$params)) {
			$this->db->where('password',$params['password']);
		}
		$query = $this->db->get();
		$result = $query->row_array();
		return $result;
	}
	public function activate($data, $id){
		$this->db->where('cp_users.id', $id);
		return $this->db->update('cp_users', $data);
	}

	/*Created by 95 on 28-2-2019 for get the single appointment detail*/
	public function getAppointmentDetail($id='')
	{
		$this->db->select('*');
		$this->db->from('doctor_appointments a'); 
		$this->db->join('cp_doctor b', 'b.doctor_id=a.doctor_id', 'left');
		$this->db->join('dr_available_date c', 'c.avdate_id=a.dr_appointment_date_id', 'left');

		$this->db->join('dr_available_time d', 'd.avtime_id=a.dr_appointment_time_id', 'left');
		$this->db->where('a.dr_appointment_id',$id);
		$query = $this->db->get(); 
		if($query->num_rows() != 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}


	function getAllRecords($table)
	{

		$query = $this->db->get($table);
		return $query->result_array();
	}







	function getSingleRecordById($table,$conditions)



	{



		$query = $this->db->get_where($table,$conditions);



		return $query->row_array();



		//echo $this->db->last_query();



	   //die();



	}




	function getWhereIn($table,$column,$in_arr) {	
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where_in($column,$in_arr);
		/*$this->db->from('cp_user_photo');
		$this->db->where_in('u_photo_id',array('177','178'));*/
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}


	function getAllRecordsById($table,$conditions)



	{



		$query = $this->db->get_where($table,$conditions);



		return $query->result_array();



	}


	// function getRecordsByName($table,$conditions)
	// {



	// 	$query = $this->db->get_where($table,$conditions);

	// 	return $query->result_array();
	// }


	function getRecordsByName($table,$conditions){

		$category_id=$conditions['category_id'];
		$search=$conditions['movie_title'];

		
		$where = array('category_id'=>$category_id);
		//$where = "FIND_IN_SET('".$category_id."', doctor_category_id)";

		$this->db->select('*');

		$this->db->from($table);

		$this->db->where($where);

   //$this->db->where("doctor_name LIKE '%$searchKeyword'");

		$this->db->like('movie_title',"$search"); 

		$query = $this->db->get();

		$result = $query->result_array();

		return $result;

	}

	function getAllRecordsByIdorder($table,$conditions)



	{


		$this->db->order_by('u_photo_id','desc');
		$query = $this->db->get_where($table,$conditions);



		return $query->result_array();


	}



	function addRecords($table,$post_data)



	{



		$this->db->insert($table,$post_data);



		return $this->db->insert_id();



	}







	function addRecordsReturnId($table,$post_data)



	{



		$this->db->insert($table,$post_data);



		return $this->db->insert_id();



	}







	function updateRecords($table, $post_data, $where_condition)
	{
		$this->db->where($where_condition);
		$query = $this->db->update($table, $post_data);
		// echo $this->db->last_query();
		// die;
		return $query;
	}







	function deleteRecords($table,$where_condition)
	{

		$this->db->where($where_condition);
		$this->db->delete($table);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}

	}







	function getPaginateRecords($table, $result, $offset = 0)



	{



		$query = $this->db->get($table,$result,$offset);



		return $query->result_array();



	}







	function getPaginateRecordsByConditions($table, $result, $offset=0, $condition)



	{



		$query = $this->db->get_where($table, $condition, $result, $offset);



		return $query->result_array();



	}







	function getPaginateRecordsByLikeConditions($table, $result, $offset=0, $condition, $like_field, $like_value)



	{



		$this->db->like($like_field, $like_value);



		$query = $this->db->get_where($table, $condition, $result, $offset);



		return $query->result_array();



	}







	function getTotalRecords($table)



	{



		$query = $this->db->get($table);



		return $query->num_rows();



	}







	function getTotalRecordsByIdLike($table, $condition, $like_field, $like_value)



	{



		$this->db->like($like_field, $like_value);



		$query = $this->db->get_where($table, $condition);



		return $query->num_rows();



	}







	function getPaginateRecordsByCondition($table,$result,$offset=0,$where_condition,$condition)



	{



		$this->db->where($where_condition,$condition);



		$query = $this->db->get($table,$result,$offset);



		return $query->result_array();



	}







	function getPaginateRecordsByOrderByCondition($table, $field, $short, $result, $offset=0, $condition)



	{



		$this->db->where($condition);



		$this->db->order_by($field, $short);



		$query = $this->db->get($table,$result,$offset);



		return $query->result_array();



	}







	function getTotalRecordsByCondition($table, $condition)



	{



		$this->db->where($condition);



		$query = $this->db->get($table);



		return $query->num_rows();



	}







	function fetchMaxRecord($table,$field)



	{



		$this->db->select_max($field,'max');



		$query = $this->db->get($table);



		return $query->row_array();



	}







	function fetchRecordsByOrder($table,$field,$sort)



	{



		$this->db->order_by($field,$sort);



		$query = $this->db->get($table);



		return $query->result_array();



	}







	function getAllRecordsByLimitId($table,$conditions,$limit)



	{



		$this->db->limit($limit);



		$query = $this->db->get_where($table,$conditions);



		return $query->result_array();



	}







	function getLatestRecords($table,$date,$limit)



	{



		$this->db->order_by($date,'desc');



		$this->db->limit($limit);



		$query = $this->db->get($table);



		return $query->result_array();



	}







	function getRelatedRecords($table,$date,$conditions)



	{



		$this->db->order_by($date,'desc');



		$this->db->limit(4);



		$query = $this->db->get_where($table,$conditions);



		return $query->result_array();



	}







	function getAscLatestRecords($table,$date,$limit)



	{



		$this->db->order_by($date,'asc');



		$this->db->limit($limit);



		$query = $this->db->get($table);



		return $query->result_array();



	}







	function getLimitedRecords($table,$limit)



	{



		$this->db->limit($limit);



		$query = $this->db->get($table);



		return $query->result_array();



	}







	function getRecordCount($table, $where_condition)



	{



		$this->db->where($where_condition);



		$query = $this->db->get($table);



		return $query->num_rows();



		// echo $this->db->last_query();



		// die();



	}







	function getcatuseridbybusinesslist($table,$where_condition)

	{



		$this->db->select('*');



		$this->db->from($table);



		$this->db->where($where_condition);



		$this->db->order_by('features_ads','desc');



		$query = $this->db->get();



		return $query->result_array();

	    //echo $this->db->last_query();

		//die();



	}









	function getallservicesdetail()



	{



		$this->db->select('*');



		$this->db->from('services');



		$query = $this->db->get();



		return $query->result_array();



	}







	function getallusersdetail()



	{



		$this->db->select('*');



		$this->db->from('users');



		$query = $this->db->get();



		return $query->result_array();



	}







	function getdriverworkload($data)



	{







		$this->db->select('distinct(company_name),company_number');



		$this->db->from('orders as o');



		$this->db->Where($data);



		$query = $this->db->get();



		return $query->result_array();



	}







	function getDriverOrders($data)



	{







		$this->db->select('o.*,u.user_lat,u.user_long');



		$this->db->from('orders as o');



		$this->db->JOIN('users as  u', 'u.user_id = o.customer_id', 'left');



		$this->db->Where($data);



		$query = $this->db->get();



		return $query->result_array();



	}



	function getRecenttrip(){

		$this->db->select('gt.*,gu.firstname,gu.lastname,gu.email,gu.profile_image,gu.mobile_no');

		$this->db->from('gb_traveler_trips as gt');

		$this->db->order_by('gt.trip_id','desc');

		$this->db->JOIN('gb_users as  gu', 'gu.id = gt.traveler_id', 'left');

		$this->db->Where('trip_status',1);

		$this->db->limit(6);

		$query = $this->db->get();

		return $query->result_array();

	}





	function getRouterWorkload($data,$date)



	{



		$this->db->select('o.order_id, o.customer_id, o.driver_id, o.service_id, o.order_status, o.status, o.load_created, u.user_id, u.user_phone, u.user_address, u.user_lat, u.user_long, u.user_com_name, u.user_com_no, u.user_com_type, u.user_com_cno');







		$this->db->from('orders as o');



		$this->db->JOIN('users as  u', 'u.user_id = o.customer_id', 'left');



		$this->db->Where(array('o.driver_id' => $data ,'o.order_status' => 1,'o.load_created' => $date));







		$query = $this->db->get();



		return $query->result();



	}











	function getdriverworkloid($date)



	{



		$this->db->select('*');



		$this->db->from('orderdetail');



		$this->db->Where($date);







		$query = $this->db->get();



		return $query->result_array();



	}



















	function getcountrydetail()



	{



		$this->db->select('*');



		$this->db->from('countries');



		$this->db->order_by('name','asc');



		$query = $this->db->get();



		return $query->result_array();



	}







	function getstate($country_id)



	{



		$this->db->select('states.id,states.name as statename');



		$this->db->from('states');



        //$this->db->join('countries', 'states.country_id=countries.id','left');



		$this->db->where('country_id',$country_id);



		$query = $this->db->get();



		return $query->result_array();



	}







	function getcity($state_id)



	{



		$this->db->select('*');



		$this->db->from('cities');



        //$this->db->join('states', 'cities.state_id=states.id','left');



		$this->db->where('state_id',$state_id);



		$query = $this->db->get();



		//echo  $this->db->last_query();



		//die();



		return $query->result_array();



	}



	public function generateRandomString($length = 12) {



    // $length = 12;



		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';



		$charactersLength = strlen($characters);



		$randomString = '';



		for ($i = 0; $i < $length; $i++) {



			$randomString .= $characters[rand(0, $charactersLength - 1)];



		}



		return $randomString;



	}















	function cheackDevice($user_id,$token)



	{



		$this->db->select('*');



		$this->db->from('users');



		$this->db->where(array('user_id' => $user_id,'token' => $token));



		$query = $this->db->get();



		return $query->row_array();



	}

	function getDataInPagination($table, $limit, $offset, $condition) {



		$this->db->order_by('noti_id','desc');

		$this->db->where($condition);

		$query = $this->db->get($table,$limit,$offset,$condition);

		return $query->result_array();

	}

	function getOrdersCount(){

		$this->db->select('*');

		$this->db->from('gb_orders');

		$this->db->order_by('o_id','desc');

		$this->db->where('order_status',1);

		$query = $this->db->get();

		$result = $query->result_array();

		return $result;

	}

	function getOrders($start,$limit,$params = array()){



		$this->db->select('gb_orders.o_id,gb_orders.o_product_name,gb_orders.deliver_from,gb_orders.deliver_to,gb_orders.deliver_before,gb_orders.user_id,u.username,gb_orders.product_url,gb_orders.o_product_image,gb_orders.delivery_reward,gb_orders.product_price,gb_orders.o_order_qty');

		$this->db->from('gb_orders');

		$this->db->JOIN('gb_users as  u', 'u.id = gb_orders.user_id', 'left');

		if(array_key_exists("like",$params)){

			foreach ($params['like'] as $key => $value) {

				$this->db->like($key,$value);

			}

		}

		$this->db->order_by('o_id','desc');

		$this->db->where('order_status',1);

		$this->db->limit($limit, $start);

		$query = $this->db->get();

		$result = $query->result_array();

		return $result;

	}

	function getUserDetails($user_id){

		$this->db->select('*');

		$this->db->from('gb_users');

		$this->db->where('id',$user_id);

		$query = $this->db->get();

		$result = $query->result_array();

		return $result;

	}
	function getTravelerTrips($start,$limit,$params = array()){

		$this->db->select('gb_traveler_trips.trip_id,u.username,u.profile_image,gb_traveler_trips.travel_from,gb_traveler_trips.travel_to,gb_traveler_trips.traveler_id');

		$this->db->from('gb_traveler_trips');

		$this->db->where('trip_status',1);

		$this->db->JOIN('gb_users as  u', 'u.id = gb_traveler_trips.traveler_id', 'left');

		if(array_key_exists("like",$params)){

			foreach ($params['like'] as $key => $value) {

				$this->db->like($key,$value);

			}

		}

		$this->db->order_by('trip_id','desc');

		$this->db->limit($limit, $start);

		$query = $this->db->get();

		$result = $query->result_array();

		return $result;

	}

	function jointwotable($table, $field_first, $table1, $field_second,$where='',$field) {

		$this->db->select($field);

		$this->db->from("$table");

		$this->db->join("$table1", "$table1.$field_second = $table.$field_first");

		if($where !=''){

			$this->db->where($where);

		}

		$q = $this->db->get();

		if($q->num_rows() > 0) {

			foreach($q->result() as $rows) {

				$data[] = $rows;

			}

			$q->free_result();

			return $data;

		}

	}
	function getEmailCount($user_id){
		$this->db->select('*');
		$this->db->from('cp_emails');
      // $this->db->where('email_status',1);
      // $this->db->where('send_box_status',0);
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		$result = $query->result_array();
		foreach($result as $result_details){

			$emailId = $this->common_model->getSingleRecordById('cp_email_trash', array('email_id' => $result_details['email_id'],'user_id'=>$user_id));

        //print_r($emailId);
        //die;
			if($emailId){
				continue;
			}

			$new_array[] = $result_details;
		}
		return $new_array;
	}
	function getEmails($start,$limit,$params = array()){
		$this->db->select('*');
		$this->db->from('cp_emails');
		$this->db->join('cp_users', 'cp_emails.user_id = cp_users.id');

       //$this->db->where('email_status',1);
      // $this->db->where('send_box_status',0);
		$this->db->order_by('cp_emails.email_id','desc');
		$this->db->where('user_id',$params);
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		$result = $query->result_array();
		foreach($result as $result_details){

			$emailId = $this->common_model->getSingleRecordById('cp_email_trash', array('email_id' => $result_details['email_id'],'user_id'=>$params));

        //print_r($emailId);
        //die;
			if($emailId){
				continue;
			}

			$new_array[] = $result_details;
		}
		return $new_array;
	}
	function getEmailCount1($user_id,$user_id1){


		$this->db->select('*');
       //$this->db->from('cp_emails');
		$this->db->from('cp_to_email');
       //$this->db->where('email_status',1);
		$this->db->where('user_id',$user_id1);
       //$this->db->where('to_email',$email);
       //$this->db->where('inbox_status',0);
		$query = $this->db->get();
		$result = $query->result_array();


		foreach($result as $result_details){

			$emailId = $this->common_model->getSingleRecordById('cp_email_trash', array('email_id' => $result_details['email_id'],'user_id'=>$user_id1));

        //print_r($emailId);
        //die;
			if($emailId){
				continue;
			}

			$new_array[] = $result_details;
		}


		return $new_array;
	}
	function gettestreportCount1($user_id){
		$this->db->select('*');
		$this->db->from('cp_test_report');
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	function getEmails1($start,$limit,$params = array(),$user_id){


		$this->db->select('*');
       //$this->db->from('cp_emails');
		$this->db->from('cp_to_email');
		$this->db->join('cp_emails', 'cp_to_email.email_id = cp_emails.email_id');
		$this->db->join('cp_users', 'cp_to_email.user_id = cp_users.id');

		$this->db->where('cp_to_email.user_id',$user_id);
       //$this->db->where('email_status',1);
		$this->db->order_by('cp_emails.email_id','desc');
       //$this->db->where('user_id',$params);
       //$this->db->where('to_email',$params);
      // $this->db->where('inbox_status',0);
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		$result = $query->result_array();

       //print_r("<pre/>");
       //print_r($result);
       //die;



		foreach($result as $result_details){

            //$emailId = $this->common_model->getSingleRecordById('cp_to_email', array('email_id' => $result_details['email_id'],'to_email'=>$params));

			$emailId = $this->common_model->getSingleRecordById('cp_email_trash', array('email_id' => $result_details['email_id'],'user_id'=>$user_id));

             //print_r("<pre/>");
             //print_r($emailId);
             //die;

			if($emailId){
				continue;
			}

			$new_array[] = $result_details;
		}
		return $new_array;
	}
	function getTrash($start,$limit,$user_id){


		$this->db->select('*');
       //$this->db->from('cp_emails');
		$this->db->from('cp_email_trash');
		$this->db->join('cp_emails', 'cp_email_trash.email_id = cp_emails.email_id');
		$this->db->join('cp_users', 'cp_email_trash.user_id = cp_users.id');

		$this->db->where('cp_email_trash.user_id',$user_id);
       //$this->db->where('email_status',1);
		$this->db->order_by('cp_email_trash.email_id','desc');
       //$this->db->where('user_id',$params);
       //$this->db->where('to_email',$params);
      // $this->db->where('inbox_status',0);
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	function getCountTrash($user_id){

		$this->db->select('*');
		$this->db->from('cp_email_trash');
		$this->db->join('cp_emails', 'cp_email_trash.email_id = cp_emails.email_id');
		$this->db->join('cp_users', 'cp_email_trash.user_id = cp_users.id');
		$this->db->where('cp_email_trash.user_id',$user_id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	function getalltestreport($start,$limit,$params = array()){
		$this->db->select('*');
		$this->db->from('cp_test_report');
		$this->db->where('user_id',$params);
		$this->db->order_by('id','desc');
       //$this->db->where('user_id',$params);

		$this->db->limit($limit, $start);
		$query = $this->db->get();
		$result = $query->result_array();
	   //echo $this->db->last_query();die;
		return $result;
	}

	function getPhotoCount($user_id){
		$this->db->select('*');
		$this->db->from('cp_user_photo');
		$this->db->where('u_photo_status',1);
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	function getPhoto($start,$limit,$params = array()){
		$this->db->select('*');
		$this->db->from('cp_user_photo');
		$this->db->where('u_photo_status',1);
		$this->db->order_by('u_photo_id','desc');
		$this->db->where('user_id',$params);
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}


	function deleteEmails($params){
		foreach($params as $pr){
            //$this->db->delete('cp_emails', array("email_id"=>$pr));

			$data = array('send_box_status'=>1);

			$this->db->where('email_id', $pr);
			$this->db->update('cp_emails', $data);
		}
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	function deleteEmails1($params){
		foreach($params as $pr){
            //$this->db->delete('cp_emails', array("email_id"=>$pr));

			$data = array('inbox_status'=>1);

			$this->db->where('email_id', $pr);
			$this->db->update('cp_emails', $data);
		}
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function deletemedicineschedule($params)
	{
		foreach($params as $pr){
			$this->db->delete('medicine_schedule', array("medicine_id"=>$pr));


		}
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function deletetestreport($params)
	{
		foreach($params as $pr){
			$this->db->delete('cp_test_report', array("id"=>$pr));


		}
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function deleteacknowledge($params)
	{
		foreach($params as $pr){
			$this->db->delete('acknowledge', array("acknowledge_id"=>$pr));


		}
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	public function deletepickupmedicine($params)
	{
		foreach($params as $pr){
			$this->db->delete('picup_medicine', array("id"=>$pr));
		}
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function deleteReminders($params){
		foreach($params as $pr){
			$this->db->delete('cp_reminder', array("reminder_id"=>$pr));
		}
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	function deletePhotos($params){
		foreach($params as $pr){
			$this->db->delete('cp_user_photo', array("u_photo_id"=>$pr));
		}
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	function deletePhotosfav($params){
		foreach($params as $pr){
			$this->db->delete('cp_photo_favourite', array("photo_fav_id"=>$pr));
		}
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	function deleteRest($params){
		foreach($params as $pr){
			$this->db->delete('cp_fav_restaurant', array("fav_rest_id"=>$pr));
		}
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	function deleteArticle($params){
		foreach($params as $pr){
			$this->db->delete('cp_article_favourite', array("article_fav_id"=>$pr));
		}
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	function deleteContact($params){
		foreach($params as $pr){
			$this->db->delete('cp_user_contact', array("contact_id"=>$pr));
		}
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function getRows($table,$params = array()){
		$this->db->select('*');
		$this->db->from($table);
    //fetch data by conditions
    //$this->db->where("status !=", 3);

		if(array_key_exists("conditions",$params)){
			foreach ($params['conditions'] as $key => $value) {
				$this->db->where($key,$value);
			}
		}

		if(array_key_exists("filter",$params)){
			foreach ($params['filter'] as $key => $value) {
				$this->db->like($key,$value);
			}
		}

		$order_mode = "DESC";
		if(array_key_exists("sorting",$params)){
			foreach ($params['sorting'] as $key => $value) {
				$this->db->order_by($value, $order_mode);
				$this->session->set_userdata('order_by',$value);
			}
		} else {
      //$this->db->order_by('create_user', 'DESC');
		}

		if(array_key_exists("id",$params)){
			$this->db->where('id',$params['id']);
			$query = $this->db->get();
			$result = $query->row_array();
		}else{
        //set start and limit
			if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
				$this->db->limit($params['limit'],$params['start']);
			} elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
				$this->db->limit($params['limit']);
			}
			$query = $this->db->get();
			if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
				$result = $query->num_rows();
			}elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
				$result = ($query->num_rows() > 0)?$query->row_array():FALSE;
			}else{
				$result = ($query->num_rows() > 0)?$query->result_array():FALSE;
			}
		}
    //echo $this->db->last_query(); die();
		return $result;
	}

	function getFavPhotoList($user_id){

  	   //echo "test";die;
		$this->db->select('*');
		$this->db->from('cp_photo_favourite');
		$this->db->where('cp_photo_favourite.user_id',$user_id);
		$this->db->JOIN('cp_user_photo as  u', 'u.u_photo_id = cp_photo_favourite.photo_id');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	function getFavPhotoListorder($user_id){

  	   //echo "test";die;
		$this->db->select('*');
		$this->db->from('cp_photo_favourite');
		$this->db->order_by('photo_fav_id', 'desc');
		$this->db->where('cp_photo_favourite.user_id',$user_id);
		$this->db->JOIN('cp_user_photo as  u', 'u.u_photo_id = cp_photo_favourite.photo_id');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	public function getAllwhere($table,$where)
	{
		$this->db->select('*');
		$q = $this->db->get_where($table,$where);
		$num_rows = $q->num_rows();
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}

	public function getuserwhere($mobile)
	{
		//$sql = "SELECT * FROM `cp_users` WHERE `mobile`='$mobile' or `email`='$email' or `username`='$username'";
		$sql = "SELECT * FROM `cp_users` WHERE `mobile`='$mobile'";
		//print_r($sql);die;

		$q = $this->db->query($sql);
		$data = array();
		foreach($q->result_array() as $row)
		{

			$data[]  = $row;
		}



		return $data;
	}

	public function countwhereuser($table,$where)
	{
		$this->db->select('*');
		$q = $this->db->get_where($table,$where);
		return $q->num_rows();

	}

	function getAllRecordsByIdnew($table,$conditions,$order_id,$order_by)



	{

		$this->db->order_by( $order_id, $order_by);

		$query = $this->db->get_where($table,$conditions);



		return $query->result_array();



	}

	function jointwotablenm($table, $field_first, $table1, $field_second,$where='',$field,$order_id,$order_by) {

		$this->db->select($field);
		$this->db->order_by( $order_id, $order_by);

		$this->db->from("$table");

		$this->db->join("$table1", "$table1.$field_second = $table.$field_first");

		if($where !=''){

			$this->db->where($where);

		}

		$q = $this->db->get();

		if($q->num_rows() > 0) {

			foreach($q->result() as $rows) {

				$data[] = $rows;

			}

			$q->free_result();

			return $data;

		}

	}


// /*Created by 95 for get all the doctor category start*/
// function getDoctorCategoryList(){
//        $this->db->select('*');
//        $this->db->from('cp_doctor_categories');
//        $query = $this->db->get();
//        $result = $query->result_array();
//        return $result;
//     }
// /*Created by 95 for get all the doctor category end*/


	/*Added by 95 for get all the doctor category start*/
	public function getDoctorCategoryList($limit, $start){
		$startt = max(0, ( $start -1 ) * $limit);
		$this->db->select('*');
		$this->db->from('cp_doctor_categories');
		$this->db->where('dc_status',1);
		$this->db->limit($limit, $startt);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
       //  $this->db->limit($limit, $start);
       //  $query = $this->db->get("cp_doctor_categories");
       // if ($query->num_rows() > 0) {
       //     foreach ($query->result_array() as $row) {
       //         $data[] = $row;
       //      }
       //      return $data;
       //  }
       //  return false;
	}

	/*Added by 95 for get all the doctor category end*/

	/*Created by 95 for get all the  start*/
	function getDoctorList($id){
		$this->db->select('*');
		$this->db->from('cp_doctor');
		$this->db->where_in('doctor_category_id',implode("','",$id));
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	/*Created by 95 for get all the doctor category end*/


	function getDoctorList1($id){
		$this->db->select('*');
		$this->db->from('cp_doctor');
		$this->db->where("FIND_IN_SET($id, doctor_category_id)");
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	public function getAllwhereorderby($table,$where,$order_id,$order_by)
	{
		$this->db->select('*');
		$this->db->order_by($order_id,$order_by);
		$q = $this->db->get_where($table,$where);
		$num_rows = $q->num_rows();
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}



	public function getAllshareby($where)
	{
		$this->db->select('*');
		$this->db->order_by('id','DESC');
		$this->db->from('share_gallery');
		$this->db->where('status =', 1);
		$this->db->where_in('receiver_ids', $where,FALSE); 
		$q= $this->db->get();
		// $this->db->select('*');
		// $this->db->order_by($order_id,$order_by);
		// $q = $this->db->get_where($table,$where);
		$num_rows = $q->num_rows();
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}


	public function getAllsharewith($where)
	{
		$this->db->select('*');
		$this->db->order_by('id','DESC');
		$this->db->from('share_gallery');
		$this->db->where('status =', 1);
		$this->db->where_in('sender_id', $where,FALSE); 
		$q= $this->db->get();
		// $this->db->select('*');
		// $this->db->order_by($order_id,$order_by);
		// $q = $this->db->get_where($table,$where);
		$num_rows = $q->num_rows();
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}

	function jointwotablenn($table, $field_first, $table1, $field_second,$where='',$field,$order_id,$order_by) {

		$this->db->select($field);
		$this->db->order_by( $order_id, $order_by);
		$this->db->from("$table");
		$this->db->join("$table1", "$table1.$field_second = $table.$field_first");
		if($where !=''){
			$this->db->where($where);
		}
		$q = $this->db->get();
		if($q->num_rows() > 0) {
			foreach($q->result() as $rows) {
				$data[] = $rows;
			}
			$q->free_result();
			return $data;
		}
	}
	function deleteAppointments($params){
		foreach($params as $pr){
			$this->db->delete('doctor_appointments', array("dr_appointment_id"=>$pr));
		}
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function getsingle($table,$where)
	{
		$q = $this->db->get_where($table,$where);
		return $q->row();
	}

	public function getAll($table,$order_id,$order_by)
	{
		$this->db->select('*');
		$this->db->order_by( $order_id, $order_by);
		$q = $this->db->get($table);

		$num_rows = $q->num_rows();
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}

	function getAllDoctorappointmets($table, $where='',$field,$group_by) {

		$this->db->select($field);

		$this->db->group_by($group_by);
		$this->db->from("$table");

		if($where !=''){
			$this->db->where($where);
		}
		$q = $this->db->get();

		if($q->num_rows() > 0) {
			foreach($q->result_array() as $rows) {
				$data[] = $rows;
			}
			$q->result_array();
			return $data;
		}
	}

	public function getAllMyFavArticle($category_id='',$user_id)
	{
		$this->db->select('*');
		$this->db->from('cp_article_favourite');
		$this->db->join('cp_articles','cp_articles.article_id=cp_article_favourite.article_id' ,'left');
		$this->db->where('cp_article_favourite.user_id',$user_id);

		$this->db->where('cp_articles.category_id',$category_id);
		$this->db->order_by('cp_article_favourite.article_fav_id','DESC');

		$q=$this->db->get();
		$num_rows = $q->num_rows();
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}

	public function getsearchemail($like,$user_id)
	{
		$sql = "SELECT * FROM `cp_emails` LEFT JOIN `cp_to_email` ON `cp_to_email`.`email_id` = `cp_emails`.`email_id` WHERE `cp_emails`.`subject` Like '%$like%' OR `cp_emails`.`message` Like '%$like%' OR `cp_emails`.`to_email_list` Like '%$like%' OR `cp_to_email`.`to_email` Like '%$like%' AND `cp_emails`.`user_id`= $user_id ORDER BY `cp_emails`.`email_id` DESC";
		$q = $this->db->query($sql);
		$data =  array() ;

		foreach($q->result_array() as $row)
		{
			$data[]  = $row;
		}

		return $data;
	}

	function newjointwotable($table, $field_first, $table1, $field_second,$where='',$field) {

		$this->db->select($field);
		$this->db->from("$table");
		$this->db->join("$table1", "$table1.$field_second = $table.$field_first","left");
		if($where !=''){
			$this->db->where($where);
		}
		$q = $this->db->get();
		if($q->num_rows() > 0) {
			return $q->row();

		}else{
			return false;
		}
	}


	public function countAppointmentNot()
	{
		$checkLogin = $this->session->userdata('logged_in');
		$this->db->select('*');
		$this->db->from('cp_common_notification a');
		$this->db->join('doctor_appointments b', 'b.dr_appointment_id=a.common_id', 'left');
		$this->db->join('dr_available_date c', 'c.avdate_id=b.dr_appointment_date_id', 'left');
		$checkLogin = $this->session->userdata('logged_in');
		$where=array("a.type" => 3,"is_read"=>0,"a.user_id" =>$checkLogin['id'],"c.avdate_date"=>date('Y-m-d'));
		$this->db->where($where);
		$this->db->order_by('a.notification_id','desc');
		$q = $this->db->get();
		return $q->num_rows();
	}

	function getAppointmentNot() {
		$checkLogin = $this->session->userdata('logged_in');
		$this->db->select('*');
		$this->db->from('cp_common_notification a');
		$this->db->join('doctor_appointments b', 'b.dr_appointment_id=a.common_id', 'left');
		$this->db->join('dr_available_date c', 'c.avdate_id=b.dr_appointment_date_id', 'left');
		$checkLogin = $this->session->userdata('logged_in');
		$where=array("a.type" => 3,"is_read"=>0,"a.user_id" =>$checkLogin['id'],"c.avdate_date"=>date('Y-m-d'));
		$this->db->where($where);
		$this->db->order_by('a.notification_id','desc');
		$q = $this->db->get();
		if($q->num_rows() > 0) {
			foreach($q->result() as $rows) {
				$data[] = $rows;
			}
			$q->free_result();
			return $data;
		}
	}

	function getCallHistory($start,$limit,$user_id){


		$this->db->select('*');
		$this->db->from('cp_call_history');
		$this->db->where('user_id',$user_id);
		$this->db->order_by('call_id','desc');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}


	function getTicketList($start,$limit,$user_id){ 


		$this->db->select('*');
		$this->db->from('user_tickets');
		$this->db->where('user_id',$user_id);
		$this->db->order_by('ticket_id','desc');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}


	function getTicketConversationList($start,$limit,$user_id,$ticket_id){ 


		$this->db->select('*');
		$this->db->from('tickets_reply');
		$this->db->where('user_id',$user_id);
		$this->db->where('ticket_id',$ticket_id);
		$this->db->order_by('reply_id','asc');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}


	public function getDetailField($where,$user_id){
		$whereCondititon = $where['search'];

		//print_r($whereCondititon);die;

		$userid = $user_id;

		$sql = 'SELECT * FROM `medicine_schedule` WHERE `user_id` ='.$userid;

		if(!empty($whereCondititon["start_date"]) && !empty($whereCondititon["end_date"])){
			$sql .= ' AND `medicine_create_date` BETWEEN "'.$whereCondititon["start_date"].'" AND "'.$whereCondititon["end_date"].'" AND end_date BETWEEN "'.$whereCondititon["start_date"].'" AND "'.$whereCondititon["end_date"].'"';

		}

		if(!empty($whereCondititon["start_date"]) && empty($whereCondititon["end_date"])){
			$sql .= ' AND `medicine_create_date` ="'.$whereCondititon["start_date"].'"';
		}

		if(empty($whereCondititon["start_date"]) && !empty($whereCondititon["end_date"])){
			$sql .= ' AND `end_date`="'.$whereCondititon["end_date"].'"';
		}


		$q=$this->db->query($sql);

//print_r($this->db->last_query());die;
		$num_rows = $q->num_rows();

		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}

	}

	public function getAllordynamic($table,$user_id)
	{
		$sql = "SELECT * FROM `cp_note` WHERE user_id = $user_id ORDER BY read_status, CASE WHEN read_status = 0 THEN read_status ELSE NULL END , read_status DESC";


		$q = $this->db->query($sql);

		// echo $this->last_query();
		// die;

		$data =  array() ;
		foreach($q->result_array() as $row)
		{
			$data[]  = $row;
		}

		return $data;
	}

	public function getAllornew($table,$user_id)
	{
		$sql = "SELECT * FROM `cp_note` WHERE user_id = $user_id ORDER BY note_id DESC";


		$q = $this->db->query($sql);

		// echo $this->last_query();
		// die;
		$data =  array() ;
		foreach($q->result_array() as $row)
		{
			$data[]  = $row;
		}

		return $data;
	}




	public function DeleteMultipleViatalById($postDAta)
	{

		foreach($postDAta as $pr_id){
			$this->db->delete('cp_vital_sign', array("vital_sign_id"=>$pr_id));
		}
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function getAllorderby($table,$order_id,$order_by)
	{
		$this->db->select('*');	
		$this->db->order_by($order_id,$order_by);		
		$q = $this->db->get($table);		
		$num_rows = $q->num_rows();		
		if($num_rows >0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			$q->free_result();
			return $data;
		}
	}

	/*Created by 95 for show all the shedule dates*/
	public function getAllSheduleDates()
	{
		$this->db->select('*');
		$this->db->from('dr_available_date as t1');
		$this->db->join('cp_doctor as t2', 't1.avdate_dr_id = t2.doctor_id', 'LEFT');
		$this->db->order_by('avdate_id','desc');
		$query = $this->db->get();
		return $query->result_array();
	}

	/*Created by 95 for show appointmets*/
	public function getAllappointments()
	{
		$this->db->select('*');
		$this->db->from('doctor_appointments as t1');
		$this->db->join('cp_doctor as t2', 't1.doctor_id = t2.doctor_id', 'LEFT');
		$this->db->join('cp_users as t3', 't3.id = t1.user_id', 'LEFT');
		$this->db->join('dr_available_date as t4', 't4.avdate_id = t1.dr_appointment_date_id', 'LEFT');
		$this->db->join('dr_available_time as t5', 't5.avtime_id = t1.dr_appointment_time_id', 'LEFT');
		$this->db->where('t3.user_role!=','1');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getAllappointmentsnew($per_page=10,$page=1)
	{
		$sql = 'SELECT * FROM `doctor_appointments` as `t1` LEFT JOIN `cp_doctor` as `t2` ON `t1`.`doctor_id` = `t2`.`doctor_id` LEFT JOIN `cp_users` as `t3` ON `t3`.`id` = `t1`.`user_id` LEFT JOIN `dr_available_date` as `t4` ON `t4`.`avdate_id` = `t1`.`dr_appointment_date_id` LEFT JOIN `dr_available_time` as `t5` ON `t5`.`avtime_id` = `t1`.`dr_appointment_time_id` WHERE `t3`.`user_role` != "1" ORDER BY `t1`.`doctor_id` DESC LIMIT '.$page.','.$per_page;

		$q = $this->db->query($sql);
		$data =  array() ;
		 // echo $this->db->last_query();die;

		foreach($q->result_array() as $row)
		{
			$data[]  = $row;
		}

		$ret['rows'] = $data;

		$sql2 = 'SELECT * FROM `doctor_appointments` as `t1` LEFT JOIN `cp_doctor` as `t2` ON `t1`.`doctor_id` = `t2`.`doctor_id` LEFT JOIN `cp_users` as `t3` ON `t3`.`id` = `t1`.`user_id` LEFT JOIN `dr_available_date` as `t4` ON `t4`.`avdate_id` = `t1`.`dr_appointment_date_id` LEFT JOIN `dr_available_time` as `t5` ON `t5`.`avtime_id` = `t1`.`dr_appointment_time_id` WHERE `t3`.`user_role` != "1" ORDER BY `t1`.`doctor_id` DESC';
		$q = $this->db->query($sql2);
		$data = array();
		foreach($q->result_array() as $row)
		{
			$data[]  = $row;
		}

		$ret['num_rows'] = count($data);

		return $ret;
	}

	/*Created by 95 for show single appointment*/
	public function getAppointment($id='')
	{
		$this->db->select('*');
		$this->db->from('doctor_appointments as t1');
		$this->db->where('t1.dr_appointment_id',$id);
		$this->db->join('cp_doctor as t2', 't1.doctor_id = t2.doctor_id', 'LEFT');
		$this->db->join('cp_users as t3', 't3.id = t1.user_id', 'LEFT');
		$this->db->join('dr_available_date as t4', 't4.avdate_id = t1.dr_appointment_date_id', 'LEFT');
		$this->db->join('dr_available_time as t5', 't5.avtime_id = t1.dr_appointment_time_id', 'LEFT');
		$this->db->where('t3.user_role!=','1');
		$query = $this->db->get();
		return $query->result();
	}

	/*Created by 95 for show all the medicine shedule*/
	public function getAllMedicineShedule()
	{
		$this->db->select('*');
		$this->db->from('medicine_schedule as t1');
		$this->db->join('cp_doctor as t2', 't1.doctor_id = t2.doctor_id', 'LEFT');

		$this->db->join('cp_users as t3', 't3.id = t1.user_id', 'LEFT');
		$this->db->order_by('medicine_id','desc');
		$this->db->where('t3.user_role!=','1');
		$query = $this->db->get();
		return $query->result_array();
	}

	/*Created by 95 for add doctor shedule date*/
	public function insertDoctorShedule($data)
	{
		$this->db->insert('dr_available_date', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	/*Created by 95 for add doctor shedule time*/
	public function insertDoctorTime($data)
	{
		$this->db->insert('dr_available_time', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}	

	public function getnewscheduledata($per_page=10,$page=1)
	{
		$SQL = "select cp_doctor.doctor_id, cp_doctor.other_doctor_name, cp_doctor.doctor_name, dr_available_date.avdate_id, dr_available_date.avdate_dr_id, dr_available_date.avdate_date from cp_doctor inner join dr_available_date on cp_doctor.doctor_id=dr_available_date.avdate_id LIMIT ".$page.",".$per_page;
		$query = $this->db->query($SQL);
		return $query->result_array();
	}	

	/*Created by 95 for show all the shedule time slots with single date*/
	public function getAllSheduleTimes($id='')
	{
		$this->db->select('*');
		$this->db->from('dr_available_time as t1');
		$this->db->where('t1.avtime_date_id',$id);
		$this->db->join('dr_available_date as t2', 't2.avdate_id = t1.avtime_date_id', 'LEFT');
		$this->db->order_by('t1.avtime_id','desc');
		$query = $this->db->get();
		return $query->result_array();
	}

	function getAllUsers()
	{
		$this->db->select('*');
		$this->db->from('cp_users');
		$this->db->where('user_role !=',1);
		$this->db->where('user_role !=',5);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getsingledoctorappoint($table,$where)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result_array();
	}

	/*Created by 95 for updated or edit shedule time slots*/
	public function getSheduleTimes($id='')
	{
		$this->db->select('*');
		$this->db->from('dr_available_time');
		$this->db->where('avtime_id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}



	public function getuserVerified($table,$device_id,$token)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('device_id',$device_id);
		$this->db->where('token',$token);
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result_array();  
	}

	public function getuser_device_Verified($table,$device_id)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('device_id',$device_id);
		//$this->db->where('token',$token);
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result_array();  
	}

	public function updateData($table,$data,$where)
	{
		$this->db->update($table,$data,$where);
		return $this->db->affected_rows();
	}
}
