<!-- jQuery UI 1.10.3 -->

<aside class="right-side">    
    <section class="content-header no-margin">
        <h1 class="text-center"> Edit Social
            <div class="back_reminder"><a href="<?php echo base_url(); ?>social" class="btn btn-danger">Back</a></div>
        </h1>
    </section>    
    <section class="content photo-list">
        <div class="photo-listMain">            
           <form id="addressform" enctype='multipart/form-data' method="post">

            <div id="success_message"></div>
            <?php //echo "<pre>"; print_r($appData); ?>
            <div class="row enterbengaldistricts">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Url:<span class="red_star">*</span></label>
                        <input class="form-control" type="text" name="social_url" value="<?php echo $socialData['social_url']; ?>">


                        <td class="error"><?php echo form_error('social_url'); ?></td>                            
                    </div>
                </div>					
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Name:<span class="red_star">*</span></label>
                        <input class="form-control" type="text" name="socialname" value="<?php echo $socialData['social_name']; ?>">
                        <!-- <textarea class="form-control" id="address" name="address"  placeholder="Enter Address"></textarea> -->


                        <td class="error"><?php echo form_error('socialname'); ?></td>                            
                    </div>
                </div>                  
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Type:<span class="red_star">*</span></label>
                        <select name="type" id="type"  class="form-control">
                            <option value="1" <?php if($socialData['type']==1){echo "selected";} ?>>Website</option>
                            <option value="2" <?php if($socialData['type']==2){echo "selected";} ?>>App</option>
                        </select>
                        <!-- <textarea class="form-control" id="address" name="address"  placeholder="Enter Address"></textarea> -->


                        <td class="error"><?php echo form_error('type'); ?></td>                            
                    </div>
                </div>                  
            </div>

            <div class="row enterotherstates" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">App:<span class="red_star">*</span></label>
                        <select  class="form-control" name="app">
                            <option value="">select</option>
                            <?php foreach ($appData as $key => $app) { ?>  


                                <option value="<?php echo $app->id; ?>" <?php if($socialData['app_id']==$app->id){echo "selected";} ?>><?php echo $app->name; ?></option>
                            <?php } ?>
                        </select>
                        <!-- <textarea class="form-control" id="address" name="address"  placeholder="Enter Address"></textarea> -->


                        <td class="error"><?php echo form_error('app'); ?></td>                            
                    </div>
                </div>                  
            </div>
            <!-- <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Icon:<span class="red_star">*</span></label>
                        <input class="form-control" type="file" name="icon">
                        <td class="error"><?php echo form_error('icon'); ?></td>                            
                    </div>
                </div>                  
            </div> -->   
            <div class="box-footerOne">
                <input type="submit" name="submit" class="btn btn-primary" value="Submit">
            </div>                  
        </form>              
    </div>
</section>
</aside>

<!--Added by 95 on 5-2-2019 For Doctor category for validation-->
<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

<script>

   $(document).ready(function(){

    var tg='<?php echo $socialData['type']?>';
    if(tg==2){
        $(".enterotherstates").show();
        $(".enterbengaldistricts").hide();
        
    }else{
        $(".enterotherstates").hide();
        $(".enterbengaldistricts").show(); 
    }
    
    $("#type").change(function(){

        $( "select option:selected").each(function(){
                        //enter bengal districts
                        if($(this).attr("value")=="1"){
                            $(".enterotherstates").hide();  
                            $(".enterbengaldistricts").show();
                        }
                        //enter other states
                        if($(this).attr("value")=="2"){
                            $(".enterbengaldistricts").hide();
                            $(".enterotherstates").show(); 
                        }
                    });
    });  
}); 

</script>

<script type="text/javascript">
    $('#addressform').validate({
        rules: {
            social_url:"required",
            socialname:"required",
            // reminder_time:"required",
            // before_time:"required",
            // repeat_time:"required",
        },
        messages: {
            //reminder_title: "Please enter reminder title.",
            social_url:"Please enter url.",
            socialname:"Please enter social site name."
            // reminder_date:"Please select reminder date.",
            // reminder_time:"Please select reminder time.",
            // before_time:"Please select remind before time.",
            // repeat_time:"Please select repeat reminder time.",
        }
        // submitHandler: function(form){
        //     $.ajax({
        //         url: '<?php echo base_url();?>Pages/add_social1',
        //         type: 'post',
        //         data: $(form).serialize(),
        //         contentType: false,
        //         processData: true,
        //         success: function(response) {

        //             console.log(response); 

        //             // if(response==1){
        //             //     $('#success_message').fadeIn().html('<div class="alert alert-success message"><button type="button" class="close" data-dismiss="alert">x</button>Social site added successfully.</div>');
        //             //     setTimeout(function() {
        //             //         $('#success_message').fadeOut("milliseconds");
        //             //     }, 500 );
        //             // }
        //             // location.href = '<?php echo base_url();?>social';

        //         }            
        //     });
        // }
    });

</script>


<script type="text/javascript">
 $(function () {
     var today = new Date();	 
     $('#datetimepicker').datetimepicker({
        ignoreReadonly: true,
        format: 'YYYY-MM-DD', 			   
        minDate: today
    });
 });  
</script>



<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>











