<head>
    
</head><style>
   


.card_rows.second_card_row div.col-lg-4 {
    min-height: 310px !important;
    height: 100%;
}



.w3-black, .w3-hover-black:hover {
    color: #000!important;
    background-color: #fff!important;
   
}
.w3-bar {
    width: 100%;
    overflow: hidden;
}

.w3-bar:before, .w3-bar:after {
    content: "";
    display: table;
    clear: both;
}

.w3-bar .w3-button {
    white-space: normal;
}
.w3-bar .w3-bar-item {
    padding: 0px 16px;
    float: left;
    width: auto;
    border: none;
    display: block;
    outline: 0;
    width:33.3%;
    border: 1px solid #ccc;
    font-size: 15px;
}

.w3-button {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.w3-btn, .w3-button {
    border: none;
    display: inline-block;
    padding: 8px 16px;
    vertical-align: middle;
    overflow: hidden;
    text-decoration: none;
    color: inherit;
    background-color: inherit;
    text-align: center;
    cursor: pointer;
    white-space: nowrap;
}

.w3-red, .w3-hover-red:hover {
    color: #000!important;
    background-color:  #8fddff!important;
}

.w3-border {
    border: 0px solid #ccc!important;
}

.table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 0px;
}

 .tabsbn {
    width: 100%;
    line-height: 25px;
    height: 100%;
    display: absolute;
}

.tabsbn .tabs-nav {
    height: 50%;
}


 
</style>




<?php

error_reporting(E_ALL);


require 'vendor/autoload.php';

putenv('GOOGLE_CLOUD_PROJECT=soultab-298313');
putenv('GOOGLE_APPLICATION_CREDENTIALS=vendor/credentials.json');


use Google\Cloud\BigQuery\BigQueryClient;
if($_SERVER['HTTP_HOST'] == 'soultab.devhost') {
} else {
$bigQuery = new BigQueryClient();
}
$date_now = time();
$date_now_1 = date("Ymd", $date_now);

$date_start_1 = date("Ymd");

$try_catch_flag = 0;

$xDisplay = "Display Daily";
if (!isset($_GET['range'])) {
    $_GET['range'] = "daily";
}
if (isset($_GET['range']) && $_GET['range'] == 'week') {
    $xDisplay = "Display Week";
    $date_start = strtotime("-7 day", $date_now);

    $date_start_1 = date("Ymd", $date_start);

    $query = "SELECT param1.value.string_value as firebase_previous_screen,
SUM(param2.value.int_value)/3600000 as engagement_time_msec,
FORMAT_DATE('%d-%m-%Y', PARSE_DATE('%Y%m%d', event_date)) AS date, 
FROM `soultab-298313.analytics_261354965.events_*`,
UNNEST(event_params) as param1,
UNNEST(event_params) as param2
where
event_name = 'screen_view'
and param1.key='firebase_screen_class'
and param2.key='engagement_time_msec'
and user_id = '".$user_id."'
and param1.value.string_value in ('Yoga','Spirituality','Splash','Dashboard','Social')
AND event_timestamp > UNIX_MICROS(TIMESTAMP_SUB(CURRENT_TIMESTAMP, INTERVAL 7 DAY))
AND _TABLE_SUFFIX BETWEEN '" . $date_start_1 . "' AND '" . $date_now_1 . "'
group by 3,1
order by date asc";
} else if (isset($_GET['range']) && $_GET['range'] == 'month') {
    $xDisplay = "Display Months";
    $date_start = strtotime("-30 day", $date_now);

    $date_start_1 = date("Ymd", $date_start);

    $query = "SELECT param1.value.string_value as firebase_previous_screen,
SUM(param2.value.int_value)/3600000 as engagement_time_msec,
FORMAT_DATE('%d-%m-%Y', PARSE_DATE('%Y%m%d', event_date)) AS date, 
FROM `soultab-298313.analytics_261354965.events_*`,
UNNEST(event_params) as param1,
UNNEST(event_params) as param2
where
event_name = 'screen_view'
and param1.key='firebase_screen_class'
and param2.key='engagement_time_msec'
and user_id = '".$user_id."'
and param1.value.string_value in ('Yoga','Spirituality','Splash','Dashboard','Social')
AND event_timestamp > UNIX_MICROS(TIMESTAMP_SUB(CURRENT_TIMESTAMP, INTERVAL 30 DAY))
AND _TABLE_SUFFIX BETWEEN '" . $date_start_1 . "' AND '" . $date_now_1 . "'
group by 3,1
order by date asc";
} else if (isset($_GET['range']) && $_GET['range'] == '3month') {
    $xDisplay = "Display 3 month";
    $date_start = strtotime("-90 day", $date_now);

    $date_start_1 = date("Ymd", $date_start);

    $query = "SELECT param1.value.string_value as firebase_previous_screen,
SUM(param2.value.int_value)/108000000 as engagement_time_msec,
FORMAT_DATE('%d-%m-%Y', PARSE_DATE('%Y%m%d', event_date)) AS date, 
FROM `soultab-298313.analytics_261354965.events_*`,
UNNEST(event_params) as param1,
UNNEST(event_params) as param2
where
event_name = 'screen_view'
and param1.key='firebase_screen_class'
and param2.key='engagement_time_msec'
and user_id = '".$user_id."'
and param1.value.string_value in ('Yoga','Spirituality','Splash','Dashboard','Social')
AND event_timestamp >
UNIX_MICROS(TIMESTAMP_SUB(CURRENT_TIMESTAMP, INTERVAL 90 DAY))
AND _TABLE_SUFFIX BETWEEN '" . $date_start_1 . "' AND '" . $date_now_1 . "'
group by 3,1
order by date asc";
} else if (isset($_GET['range']) && $_GET['range'] == '6month') {
    $xDisplay = "Display 6 Month";
    $date_start = strtotime("-180 day", $date_now);

    $date_start_1 = date("Ymd", $date_start);

    $query = "SELECT param1.value.string_value as firebase_previous_screen,
SUM(param2.value.int_value)/108000000 as engagement_time_msec,
FORMAT_DATE('%d-%m-%Y', PARSE_DATE('%Y%m%d', event_date)) AS date, 
FROM `soultab-298313.analytics_261354965.events_*`,
UNNEST(event_params) as param1,
UNNEST(event_params) as param2
where
event_name = 'screen_view'
and param1.key='firebase_screen_class'
and param2.key='engagement_time_msec'
and user_id = '".$user_id."'
and param1.value.string_value in ('Yoga','Spirituality','Splash','Dashboard','Social')
AND event_timestamp >
UNIX_MICROS(TIMESTAMP_SUB(CURRENT_TIMESTAMP, INTERVAL 180 DAY))
AND _TABLE_SUFFIX BETWEEN '" . $date_start_1 . "' AND '" . $date_now_1 . "'
group by 3,1
order by date asc";
} else if (isset($_GET['range']) && $_GET['range'] == '12month') {
    $xDisplay = "Display 12 Month";
    $date_start = strtotime("-360 day", $date_now);

    $date_start_1 = date("Ymd", $date_start);

    $query = "SELECT param1.value.string_value as firebase_previous_screen,
SUM(param2.value.int_value)/108000000 as engagement_time_msec,
FORMAT_DATE('%d-%m-%Y', PARSE_DATE('%Y%m%d', event_date)) AS date, 
FROM `soultab-298313.analytics_261354965.events_*`,
UNNEST(event_params) as param1,
UNNEST(event_params) as param2
where
event_name = 'screen_view'
and param1.key='firebase_screen_class'
and param2.key='engagement_time_msec'
and user_id = '".$user_id."'
and param1.value.string_value in ('Yoga','Spirituality','Splash','Dashboard','Social')
AND event_timestamp >
UNIX_MICROS(TIMESTAMP_SUB(CURRENT_TIMESTAMP, INTERVAL 360 DAY))
AND _TABLE_SUFFIX BETWEEN '" . $date_start_1 . "' AND '" . $date_now_1 . "'
group by 3,1
order by date asc";
} else  if (isset($_GET['range']) && $_GET['range'] == 'daily') {
    $date_start_daily = strtotime("-1 day", $date_now);

    $date_start_daily_1 = date("Ymd", $date_start_daily);
    $query = "SELECT param1.value.string_value as firebase_previous_screen,
param2.value.int_value/60000 as engagement_time_msec,
FORMAT_TIME('%T', TIME(TIMESTAMP_MICROS(event_timestamp))) time, 
FROM `soultab-298313.analytics_261354965.events_intraday_".$date_start_daily_1."`,
UNNEST(event_params) as param1,
UNNEST(event_params) as param2
where
event_name = 'screen_view'
and param1.key='firebase_screen_class'
and param2.key='engagement_time_msec'
and user_id = '".$user_id."'
and param1.value.string_value in ('Yoga','Spirituality','Splash','Dashboard','Social')
group by 3,1,2
order by firebase_previous_screen asc
";
}
if($_SERVER['HTTP_HOST'] == 'soultab.devhost') {

} else {

try{
    $queryJobConfig = $bigQuery->query($query);
    $queryResults = $bigQuery->runQuery($queryJobConfig);
} catch(Exception $e) {
    $try_catch_flag = 1;
  
}


$x_array = [];

$y_array = [];

$temp_array = [];

$count = 0;

$bar_array = [];

$color_set_array = ["green", 'Red', 'Blue', 'Purple', 'Yellow', 'brown', 'orange', 'pink'];


if (isset($_GET['range']) && !empty($_GET['range']) && ($_GET['range'] == 'week' || $_GET['range'] == 'month')) {

    //echo $query;

    foreach ($queryResults as $row) {
        $x_array[] = $row["firebase_previous_screen"];
        $y_array[] = $row["engagement_time_msec"];

        $day_find = $row['date'];

        if (in_array($row["firebase_previous_screen"], $temp_array)) {
            if (in_array($day_find, $temp_array[$row["firebase_previous_screen"]]["day"])) {
                $temp_array[$row["firebase_previous_screen"]]["avg_time"][$day_find] = $temp_array[$row["firebase_previous_screen"]]["avg_time"][$day_find] + number_format($row['engagement_time_msec'] * (10 / 6), 2);
            } else {
                $temp_array[$row["firebase_previous_screen"]]["day"][] = $day_find;
                $temp_array[$row["firebase_previous_screen"]]["avg_time"][$day_find] = number_format($row['engagement_time_msec'] * (10 / 6), 2);
            }
        } else {
            $count++;
            $temp_array[] = $row["firebase_previous_screen"];
            $temp_array[$row["firebase_previous_screen"]]["day"][] = $day_find;
            $temp_array[$row["firebase_previous_screen"]]["avg_time"][$day_find] = number_format($row['engagement_time_msec'] * (10 / 6), 2);
        }
    }

    $daily_data = [];

    $x_label_array = getDatesFromRange1($date_start_1, $date_now_1);
    $x_label_array0 = explode(",", substr($x_label_array, 0, -1));
    $x_label_array = array_flip(explode(",", substr($x_label_array, 0, -1)));

    $x_label_array1 = [];

    foreach ($x_label_array as $key => $value) {
        $x_label_array1[$key] = 0;
    }

    for ($i = 0; $i < $count; $i++) {
        $daily_data[] = array("color_set" => $color_set_array[$i], "name" => $temp_array[$i], "yaxis" => array_values(array_replace($x_label_array1, $temp_array[$temp_array[$i]]['avg_time'])));
        $yaxis_ar = array_values(array_replace($x_label_array1, $temp_array[$temp_array[$i]]['avg_time']));
        $bar_array[] = array("name" => $temp_array[$i], "value" => array_sum($yaxis_ar) / sizeof($yaxis_ar), "color_set" => $color_set_array[$i]);
    }
}
else if (isset($_GET['range']) && !empty($_GET['range']) && ($_GET['range'] == '3month' || $_GET['range'] == '12month' || $_GET['range'] == '6month')) {



    foreach ($queryResults as $row) {
        $x_array[] = $row["firebase_previous_screen"];
        $y_array[] = $row["engagement_time_msec"];

        $day_find = date("M", strtotime($row['date']));

        if (in_array($row["firebase_previous_screen"], $temp_array)) {
            if (in_array($day_find, $temp_array[$row["firebase_previous_screen"]]["day"])) {
                $temp_array[$row["firebase_previous_screen"]]["avg_time"][$day_find] = $temp_array[$row["firebase_previous_screen"]]["avg_time"][$day_find] + number_format($row['engagement_time_msec'] * (10 / 6), 2);
            } else {
                $temp_array[$row["firebase_previous_screen"]]["day"][] = $day_find;
                $temp_array[$row["firebase_previous_screen"]]["avg_time"][$day_find] = number_format($row['engagement_time_msec'] * (10 / 6), 2);
            }
        } else {
            $count++;
            $temp_array[] = $row["firebase_previous_screen"];
            $temp_array[$row["firebase_previous_screen"]]["day"][] = $day_find;
            $temp_array[$row["firebase_previous_screen"]]["avg_time"][$day_find] = number_format($row['engagement_time_msec'] * (10 / 6), 2);
        }
    }

    $daily_data = [];





    $x_label_array = getDatesFromRange2($date_start_1, $date_now_1);
    $x_label_array0 = array_values(array_unique(explode(",", substr($x_label_array, 0, -1))));
    $x_label_array = array_flip(array_unique(explode(",", substr($x_label_array, 0, -1))));





    $x_label_array1 = [];

    foreach ($x_label_array as $key => $value) {
        $x_label_array1[$key] = 0;
    }

    for ($i = 0; $i < $count; $i++) {
        $daily_data[] = array("color_set" => $color_set_array[$i], "name" => $temp_array[$i], "yaxis" => array_values(array_replace($x_label_array1, $temp_array[$temp_array[$i]]['avg_time'])));
        $yaxis_ar = array_values(array_replace($x_label_array1, $temp_array[$temp_array[$i]]['avg_time']));
        $bar_array[] = array("name" => $temp_array[$i], "value" => array_sum($yaxis_ar) / sizeof($yaxis_ar), "color_set" => $color_set_array[$i]);
    }
}
else if (isset($_GET['range']) && !empty($_GET['range']) && $_GET['range'] == 'daily') {

    if($try_catch_flag != 1){
        foreach ($queryResults as $row) {
            $x_array[] = $row["firebase_previous_screen"];
            $y_array[] = $row["engagement_time_msec"];

            $hour_find = explode(":", $row['time'])[0] . ":00";

            if (in_array($row["firebase_previous_screen"], $temp_array)) {
                if (in_array($hour_find, $temp_array[$row["firebase_previous_screen"]]["hour"])) {
                    $temp_array[$row["firebase_previous_screen"]]["avg_time"][$hour_find] = $temp_array[$row["firebase_previous_screen"]]["avg_time"][$hour_find] + number_format($row['engagement_time_msec'] * (10 / 6), 2);
                } else {
                    $temp_array[$row["firebase_previous_screen"]]["hour"][] = $hour_find;
                    $temp_array[$row["firebase_previous_screen"]]["avg_time"][$hour_find] = number_format($row['engagement_time_msec'] * (10 / 6), 2);
                }
            } else {
                $count++;
                $temp_array[] = $row["firebase_previous_screen"];
                $temp_array[$row["firebase_previous_screen"]]["hour"][] = $hour_find;
                $temp_array[$row["firebase_previous_screen"]]["avg_time"][$hour_find] = number_format($row['engagement_time_msec'] * (10 / 6), 2);
            }

            //print_r($row);
        }

        $daily_data = [];

        $x_label_array =  ['01:00' => 0, '02:00' => 0, '03:00' => 0, '04:00' => 0, '05:00' => 0, '06:00' => 0, '07:00' => 0, '08:00' => 0, '09:00' => 0, "10:00" => 0, '11:00' => 0, '12:00' => 0, '13:00' => 0, '14:00' => 0, '15:00' => 0, '16:00' => 0, '17:00' => 0, '18:00' => 0, '19:00' => 0, '20:00' => 0, '21:00' => 0, '22:00' => 0, '23:00' => 0, '00:00' => 0];

        $x_label_array0 = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '00'];

        for ($i = 0; $i < $count; $i++) {
            $daily_data[] = array("color_set" => $color_set_array[$i], "name" => $temp_array[$i], "yaxis" => array_values(array_merge($x_label_array, $temp_array[$temp_array[$i]]['avg_time'])));
            //print_r($x_label_array);
            $yaxis_ar = array_values(array_merge($x_label_array, $temp_array[$temp_array[$i]]['avg_time']));
            $bar_array[] = array("name" => $temp_array[$i], "value" => array_sum($yaxis_ar) / sizeof($yaxis_ar), "color_set" => $color_set_array[$i]);
        }
    } else {
        $daily_data = [];

        $x_label_array =  ['01:00' => 0, '02:00' => 0, '03:00' => 0, '04:00' => 0, '05:00' => 0, '06:00' => 0, '07:00' => 0, '08:00' => 0, '09:00' => 0, "10:00" => 0, '11:00' => 0, '12:00' => 0, '13:00' => 0, '14:00' => 0, '15:00' => 0, '16:00' => 0, '17:00' => 0, '18:00' => 0, '19:00' => 0, '20:00' => 0, '21:00' => 0, '22:00' => 0, '23:00' => 0, '00:00' => 0];

        $x_label_array0 = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '00'];

        $daily_data[] = array("color_set" => $color_set_array[0], "name" => "", "yaxis" => $x_label_array);

        $yaxis_ar = array();

        $bar_array = array();

    }
}

}


function getDatesFromRange1($start, $end)
{
    $startDate = new DateTime($start);
    $endDate = new DateTime($end);
    $endDate->modify('+1 day');
    $daterange = new DatePeriod($startDate, new DateInterval('P1D'), $endDate);
    $result = '';

    foreach ($daterange as $date) {
        $result .= $date->format("d-m-Y") . ',';
    }
    return $result;
}

function getDatesFromRange2($start, $end)
{
    $startDate = new DateTime($start);
    $endDate = new DateTime($end);
    $endDate->modify('+1 day');
    $daterange = new DatePeriod($startDate, new DateInterval('P1D'), $endDate);
    $result = '';

    foreach ($daterange as $date) {
        $result .= $date->format("M") . ',';
    }
    return $result;
}


?>
<input type="hidden" name="get_lat" id="get_lat" value="">

<input type="hidden" name="get_long" id="get_long" value="">



<!-- /# row -->
<div class=" card_rows col-lg-12">
 <div class="col-lg-8 col-sm ">
     <div class="card alert">
         <div class="card-header">


         </div>
         <div class="sales-chart  card-content">
             <canvas id="canvas"></canvas>
         </div>

     </div>
 </div>
 <div class="col-lg-4 col-sm-4 remove_padding">
     <div class="card alert">
         <div class="card-header">
             <div class="ti-checkbox">
                 <div class="tdl-holder">
                     <div class="tdl-content">
                         <label class="label_heading">Daily
                             <input type="checkbox" data="daily" class="statics_data" <?php if (isset($_GET['range']) && $_GET['range'] == "daily") { ?> checked="checked" <?php } ?>>
                             <span class="checkmark"></span>
                         </label>
                         <label class="label_heading">Weekly
                             <input type="checkbox" data="week" class="statics_data" <?php if (isset($_GET['range']) && $_GET['range'] == "week") { ?> checked="checked" <?php } ?>>
                             <span class="checkmark"></span>
                         </label>
                         <label class="label_heading">Monthly
                             <input type="checkbox" data="month" class="statics_data" <?php if (isset($_GET['range']) && $_GET['range'] == "month") { ?> checked="checked" <?php } ?>>
                             <span class="checkmark"></span>
                         </label>
                         <label class="label_heading">Last 3 Months
                             <input type="checkbox" data="3month" class="statics_data" <?php if (isset($_GET['range']) && $_GET['range'] == "3month") { ?> checked="checked" <?php } ?>>
                             <span class="checkmark"></span>
                         </label>
                         </label>
                         <label class="label_heading">Last 6 Months
                             <input type="checkbox" data="6month" class="statics_data" <?php if (isset($_GET['range']) && $_GET['range'] == "6month") { ?> checked="checked" <?php } ?>>
                             <span class="checkmark"></span>
                         </label>
                         </label>
                         <label class="label_heading">Last 1 Year
                             <input type="checkbox" data="12month" class="statics_data" <?php if (isset($_GET['range']) && $_GET['range'] == "12month") { ?> checked="checked" <?php } ?>>
                             <span class="checkmark"></span>
                         </label>


                     </div>
                 </div>
             </div>
         </div>
     </div>
     <!-- /# column -->
 </div>
</div>
<!-- /# row -->
<div class=" card_rows second_card_row">
 <?php if(!empty($bar_array)) {
     ?>

 <div class="col-lg-4 col-sm-4">
     <div class="card alert">
         <div class="card">
             <div class="stat-widget-two">

                 <?php
                    foreach ($bar_array as $value) {
                    ?>
                     <div class="stat-content">
                         <div class="stat-text"><?php echo $value['name']; ?></div>

                     </div>
                     <div class="progress">
                         <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $value['value']; ?>%;background-color:<?php echo $value['color_set']; ?>" color="#1fb71f"></div>
                     </div>
                 <?php } ?>
             </div>
         </div>
     </div>
 </div>
 <?php } ?>
 <div class="col-lg-4 col-sm-4">
     <div class="card alert">
         <div class="card">
             <div class="stat-widget-two">


                 <div class="stat-content">
                     <div class="stat-text">Doctor's Appointment</div>

                 </div>
                 <div class="progress">
                     <div class="progress-bar progress-bar-success6" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 90%;" color="#1fb71f"></div>
                 </div>
                 <div class="stat-content">
                     <div class="stat-text">Test Reports</div>

                 </div>
                 <div class="progress">
                     <div class="progress-bar progress-bar-success6" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;"></div>
                 </div>
                 <div class="stat-content">
                     <div class="stat-text">Vital Signs</div>

                 </div>
                 <div class="progress">
                     <div class="progress-bar progress-bar-success6" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 35%;"></div>
                 </div>
                 <div class="stat-content">
                     <div class="stat-text">Medicine Prescription</div>

                 </div>
                 <div class="progress">
                     <div class="progress-bar progress-bar-success6" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 15%;"></div>
                 </div>
                 <div class="stat-content">
                     <div class="stat-text">Recovery Rate</div>

                 </div>
                 <div class="progress">
                     <div class="progress-bar progress-bar-success6" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 95%;"></div>
                 </div>
             </div>
         </div>
     </div>
 </div>

 <div class="col-lg-4 col-sm-4">
     <div class="card alert">
         <div class="card">
             <h5>Compliance</h5>
                <div class="tabsbn">
                    <div class="w3-bar w3-black tabs-nav">
                        <button class="w3-bar-item w3-button tablink w3-red atabs" onclick="openCity(event,'Daily')">Daily</button>
                        <button class="w3-bar-item w3-button tablink atbas" onclick="openCity(event,'Weekly')">Weekly</button>
                        <button class="w3-bar-item w3-button tablink atabs" onclick="openCity(event,'Monthly')">Monthly</button>
                    </div>
  
                    <div id="Daily" class="w3-container w3-border city tabs-stagebn">
                         <table class="table table-responsive table-hover ">
                            <thead>
                             <tr>
                                 <th>Name</th>
                                 <th>Missed events</th>

                             </tr>
                            </thead>
                            <tbody>
                             <tr>
                                 <td data-label="Name"> <?php echo isset($compliance['daily']) ? $compliance['daily']->type : '';  ?> </td>
                                 <td data-label="Missed events"> <?php echo isset($compliance['daily']) ? $compliance['daily']->count : '';  ?> </td>
                               
                             </tr>
                            </tbody>
                        </table>
                    </div>

                    <div id="Weekly" class="w3-container w3-border city tabs-stagebn" style="display:none">
                        <table class="table table-responsive table-hover ">
                            <thead>
                             <tr>
                                 <th>Name</th>
                                 <th>Missed events</th>

                             </tr>
                            </thead>
                            <tbody>
                             <tr>
                                 <td data-label="Name"> <?php echo isset($compliance['weekly']) ? $compliance['weekly']->type : '';  ?> </td>
                                 <td data-label="Missed events"> <?php echo isset($compliance['weekly']) ? $compliance['weekly']->count : '';  ?> </td>
                             
                             </tr>
                            </tbody>
                        </table>
                    </div>

                    <div id="Monthly" class="w3-container w3-border city tabs-stagebn" style="display:none">
                        <table class="table table-responsive table-hover ">
                            <thead>
                             <tr>
                                 <th>Name</th>
                                 <th>Missed events</th>

                             </tr>
                            </thead>
                            <tbody>
                             <tr>
                                 <td data-label="Name"> <?php echo isset($compliance['monthly']) ? $compliance['monthly']->type : '';  ?> </td>
                                 <td data-label="Missed events"> <?php echo isset($compliance['monthly']) ? $compliance['monthly']->count : '';  ?> </td>

                             </tr>
                            </tbody>
                        </table>
                    </div>
                </div>



         </div>

     </div>
 

</div>


<!-- /# row -->



<?php //echo "<pre>";print_r($weather); 

//echo $weather['coord']['lon'];

//echo $weather['main']['temp_max'];

?>



<!-- <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'> -->









<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>



<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>


<script>
 var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
 var array_daily = <?php echo json_encode($daily_data); ?>;
 var dataset_daily = [];
 var label_1 = <?php echo json_encode($x_label_array0); ?>;
 var it = 0;
 var flag_range = "<?php echo $_GET['range']; ?>";
 array_daily.forEach(function(item, index) {
     console.log(item);
     dataset_daily.push({
         label: item.name,
         backgroundColor: item.color_set,
         borderColor: item.color_set,
         data: item.yaxis,
         fill: false,
     })
     it++;
 });

 console.log("ram" + flag_range);

 var config = {
     type: 'line',
     data: {
         labels: label_1,
         datasets: dataset_daily
     },
     options: {
         responsive: true,
         title: {
             display: true,
             text: 'User Activity'
         },
         tooltips: {
             mode: 'index',
             intersect: false,
         },
         hover: {
             mode: 'nearest',
             intersect: true
         },
         scales: {
             xAxes: [{
                 display: true,
                 scaleLabel: {
                     display: true,
                     labelString: '<?php echo $xDisplay; ?>'
                 }
             }],
             yAxes: [{
                 display: true,
                 scaleLabel: {
                     display: true,
                     labelString: 'Engagement Time in Hours'
                 }
             }]
         }
     }
 };




 window.onload = function() {
     var ctx = document.getElementById('canvas').getContext('2d');
     window.myLine = new Chart(ctx, config);
 };

 document.getElementById('randomizeData').addEventListener('click', function() {
     config.data.datasets.forEach(function(dataset) {
         dataset.data = dataset.data.map(function() {
             return randomScalingFactor();
         });

     });

     window.myLine.update();
 });

 var colorNames = Object.keys(window.chartColors);
 document.getElementById('addDataset').addEventListener('click', function() {
     var colorName = colorNames[config.data.datasets.length % colorNames.length];
     var newColor = window.chartColors[colorName];
     var newDataset = {
         label: 'Dataset ' + config.data.datasets.length,
         backgroundColor: newColor,
         borderColor: newColor,
         data: [],
         fill: false
     };

     for (var index = 0; index < config.data.labels.length; ++index) {
         newDataset.data.push(randomScalingFactor());
     }

     config.data.datasets.push(newDataset);
     window.myLine.update();
 });

 document.getElementById('addData').addEventListener('click', function() {
     if (config.data.datasets.length > 0) {
         var month = MONTHS[config.data.labels.length % MONTHS.length];
         config.data.labels.push(month);

         config.data.datasets.forEach(function(dataset) {
             dataset.data.push(randomScalingFactor());
         });

         window.myLine.update();
     }
 });

 document.getElementById('removeDataset').addEventListener('click', function() {
     config.data.datasets.splice(0, 1);
     window.myLine.update();
 });

 document.getElementById('removeData').addEventListener('click', function() {
     config.data.labels.splice(-1, 1); // remove the label first

     config.data.datasets.forEach(function(dataset) {
         dataset.data.pop();
     });

     window.myLine.update();
 });
</script>


<script>
  $('.tabs-stagebn div').hide();
$('.tabs-stagebn div:first').show();
$('.tabs-nav li:first').addClass('tab-active');

// Change tab class and display content
$('.tabs-nav a').on('click', function(event){
  event.preventDefault();
  $('.tabs-nav li').removeClass('tab-active');
  $(this).parent().addClass('tab-active');
  $('.tabs-stagebn div').hide();
  $($(this).attr('href')).show();
});
</script>

<script>
function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-red";
}
</script>

