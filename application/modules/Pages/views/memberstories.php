<section class="member_story_s">
 	<div class="container">
 		<div id="next_sec1"></div>
 		<div class="bigHeading wow fadeInDown  animated">
 			<div class="bigText">Stories</div>
 			<h2 class="margin-bottom-xxl text-center">Member<strong>Stories</strong></h2>
 		</div>
 		<?php foreach ($memberStoryData as  $story) { ?>

 			<?php if($story->id % 2 == 0){ ?>
 				<div class="member_story_s_one">

 					<div class="col-md-7">
 						<div class="member_story_s_on_l wow fadeInDown  animated">
 							<img src="assets/images/video_img_one.jpg">
 							<!-- <span class="vid_butt"><a href="#"> <img src="assets/images/play_button.png"></a></span> -->
 						</div>
 					</div>
 					<div class="col-md-5">
 						<div class="member_story_s_on_r wow fadeInDown  animated">
 							<h2><?php echo $story->story_desc; ?></h2> 
 							<p><?php echo $story->member_name; ?></p>
 							<!-- <span class="click_plus"><a href=""> <img src="assets/images/plus_icon.png"> </a> </span> -->
 						</div>
 					</div>
 				</div>
 			<?php }else{ ?>
 				<div class="member_story_s_three for_desktop_s">
 					<div class="col-md-5">
 						<div class="member_story_s_three_r_l wow fadeInDown   animated">
 							<h2><?php echo $story->story_desc; ?></h2> 
 							<p><?php echo $story->member_name; ?></p>
 						</div>
 					</div>
 					<div class="col-md-7">
 						<div class="member_story_s_three_r_r wow fadeInDown   animated">
 							<img src="assets/images/video_img_one.jpg">
 							<!-- <span class="vid_butt"><a href="#"> <img src="assets/images/play_button.png"></a></span> -->
 						</div>
 					</div>       
 				</div>

 				<div class="member_story_s_one for_mob_s">
 					<div class="col-md-7">
 						<div class="member_story_s_on_l wow fadeInDown  animated">
 							<img src="assets/images/video_img_one.jpg">
 							<span class="vid_butt"><a href="#"> <img src="assets/images/play_button.png"></a></span>
 						</div>
 					</div>
 					<div class="col-md-5">
 						<div class="member_story_s_on_r wow fadeInDown  animated">
 							<h2><?php echo $story->story_desc; ?></h2> 
 							<p><?php echo $story->member_name; ?></p>
 							<!-- <span class="click_plus"><a href=""> <img src="assets/images/plus_icon.png"> </a> </span> -->
 						</div>
 					</div>     
 				</div>
 				
 			<?php } ?>

 		<?php } ?>
 			<!-- <div class="member_story_s_three">
 				<div class="col-md-5">
 					<div class="member_story_s_three_r_l wow fadeInDown   animated">
 						<h2>“Why Tom love the app”</h2> 
 						<p>TOM LANGHOF</p>
 					</div>
 				</div>
 				<div class="col-md-7">
 					<div class="member_story_s_three_r_r wow fadeInDown   animated">
 						<img src="assets/images/video_img_one.jpg">
 						<span class="vid_butt"><a href="#"> <img src="assets/images/play_button.png"></a></span>
 					</div>
 				</div>       
 			</div> -->

 		</div>
 	</section>