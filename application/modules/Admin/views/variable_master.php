
<div class="container-fluid">
	<div class="dash-counter users-main my-sts-table">
		<p align="right"><button type="button" class="btn btn-primary" id="add_variabel">Add Variable</button></p>
		<div class="card-body">
			<div id="msg_show" align="center"></div>
			<div class="status-cng"></div>
			<table class="table table-hover tab_comn " id="sampleTable">
				<thead>
					<tr>
						<th>Sr no.</th>
						<th>Variable name</th>
						<th>Add date</th>
						<th>Update date</th>
						<th>Username</th>
						<th>Staus</th>
						<th>Action</th>
					</tr>
				</thead>

				<tbody>
					<?php //echo TXT_SITE_WEL_MSG; ?>

					<?php if(!empty($variable_data) && $variable_data !='') {?>
						<?php foreach ($variable_data as $key => $val) {?>
							<input type="hidden" name="vm_id" value="<?php echo $val->vm_id; ?>">
							<tr>
								<td><?php echo ($key+1); ?></td>
								<td><?php echo $val->vm_name; ?></td>
								<td><?php echo $val->vm_add_date; ?></td>
								<td><?php echo $val->vm_update_date; ?></td>
								<td><?php echo $val->vm_user_id; ?></td>
								<?php if($val->vm_staus ==1){ ?> 
									<td><?php echo "<button type='' class='btn ybt btn-xs'  disabled>Active</button>"; ?></td>
								<?php }else{ ?>
									<td><?php echo "<button type='' class='btn xbt btn-xs'  disabled>Inactive</button>"; ?></td>
								<?php } ?>
								<td>
									<a href="<?php echo base_url() . "admin/variable_create/{$val->vm_id}" ?>">
										<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
									</a> &nbsp;&nbsp;&nbsp;

									<a href="javascript:void(0)" onclick="delete_variable('<?php echo $val->vm_id; ?>');">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</a>
								</td>
							</tr>
						<?php } ?>

					<?php }else{?>
							<tr>
								<td  align="center" colspan="7">No records availabe.</td>
							</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.5.1.js"></script>
<script>
	$('#add_variabel').click(function() {
		var url = '<?php echo base_url() . "admin/variable_create" ?>';
		window.location.href = url ;
		return false;
	});

	function delete_variable(vm_id) {
		var id = '';
		var id = vm_id;
		var url = "<?php echo base_url() . 'admin/variable_delete/'; ?>";
		var url_mix = url + id;
		$.ajax({
			type: 'POST',
			url: url_mix,
			data: { id: id },

			success: function(resultData) {
				if (parseInt(resultData) != '') {
					var msg= "Variable deleted suscessfully";
					$("#msg_show").html("<div class='alert alert-success' id='hide_msg'>" + msg + "</div>");
					setTimeout(function() {
						$('#hide_msg').fadeOut('fast');
						location.reload();
					}, 2500);
				}
			},
			error: function(errorData) {}
		});
	}

</script>
