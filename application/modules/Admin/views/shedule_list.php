<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="row">

			<div class="col-md-12">

				<div class="">

					<div class="card-body">

						<div class="status-cng"></div>

						<?php if ($this->session->flashdata('success')) { ?>

							<div class="alert alert-success message">

								<button type="button" class="close" data-dismiss="alert">x</button>

								<?php echo $this->session->flashdata('success'); ?></div>

							<?php } ?>

							<?php  //echo "<pre>"; print_r($shedule_list); echo "</pre>"; ?>

							<form id="schedule_search" name="schedule_search" method="post" class="form-appointment" action="">

								<div class="row">



									<div class="col-md-4">

										<div class="form-group">

											<label class="control-label" for="">From Date</label>

											<div class="input-group date shedule" id="datepickers">

												<input type='text' class="form-control" readonly id="datepicker" name="datepicker" value="" placeholder="From Date" />

												<div class="input-group-addon">

													<span class="glyphicon glyphicon-calendar"></span>

												</div>



											</div>

										</div>

									</div>



									<div class="col-md-4">

										<div class="form-group">

											<label class="control-label" for="">To Date</label>

											<div class="input-group date shedule" id="datepickers2">

												<input type='text' class="form-control" readonly id="datepicker2" name="datepicker2" value="" placeholder="To Date" />

												<div class="input-group-addon">

													<span class="glyphicon glyphicon-calendar"></span>

												</div>



											</div>

										</div>

									</div>



									<div class="col-md-4">

										<div class="form-group">

											<label class="control-label" for="">Doctor Name</label>

											<input type="text" name="doctor_name" id="doctor_name" value="" class="form-control" >

										</div>

									</div>

								</div>



								<div class="row">

									<div class="col-md-6">

										<div class="form-group">

											<input type="submit" name="submit" id="submit" class="btn btn-primary" value="Submit" />

										</div>

									</div>

								</div>

							</form>

							<div id="schedule_search_data">

								<table class="table table-hover tab_comn" id="sampleTable" >

									<thead>

										<tr>

											<th>S no.</th>

											<th>Schedule Dates</th>

											<th>Doctor Name</th>

											<th>Action</th>

										</tr>

									</thead>

									<tbody>

										<?php

										$count = max(0, ( $this->uri->segment(3) -1 ) * $per_page1);

										if($this->uri->segment(3) == '')

										{

											$i = 1;

										}

										else

										{

											$i = $count+1;

										}

										foreach ($shedule_list as $shedule) {

											?>

											<tr>

												<td><?php echo $i++; ?></td>

												<td><?php echo date('d-m-Y',strtotime($shedule['avdate_date'])) ; ?></td>

												<td><?php echo $shedule['doctor_name']; ?></td>

												<td>

													<div class="link-del-view">

														<?php

														if (isset($staff_permission[3]['view_permission']) == 1) {

															?>

															<div class="tooltip-2">

																<a href="admin/view_schedule_times/<?php echo $shedule['avdate_id']; ?>/<?php echo $shedule['doctor_id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

																<span class="tooltiptext">View</span>

															</div>

															<?php

														} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

															?>

															<div class="tooltip-2">

																<a href="admin/view_schedule_times/<?php echo $shedule['avdate_id']; ?>/<?php echo $shedule['doctor_id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

																<span class="tooltiptext">View</span>

															</div>

															<?php

														}

														?>

														

														<?php /* ?><div class="tooltip-2">

															<a href="admin/edit_shedule/<?php echo $shedule['avdate_id']; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>

															<span class="tooltiptext">Edit</span>

															</div><?php */ ?>

															<?php

															if (isset($staff_permission[3]['delete_permission']) == 1) {

																?>

																<div class="tooltip-2">

																	<a href="admin/delete_shedule_date/<?php echo $shedule['avdate_id']; ?>" onclick="return confirm('Delete this shedule date ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

																</div>

																<?php

															} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

																?>

																<div class="tooltip-2">

																	<a href="admin/delete_shedule_date/<?php echo $shedule['avdate_id']; ?>" onclick="return confirm('Delete this shedule date ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

																</div>

																<?php

															}

															?>	

															

														</div>

													</td>

												</tr>

											<?php } ?>

										</tbody>

									</table>

									<?php //echo $links; ?>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

		<script>

			$(function () {



				$('#datepickers').datetimepicker({

					ignoreReadonly: true,

					format: 'YYYY-MM-DD',



				});

			});

			$(function () {



				$('#datepickers2').datetimepicker({

					ignoreReadonly: true,

					format: 'YYYY-MM-DD',



				});

			});



			$(document).on('submit','#schedule_search', function(e){

				event.preventDefault();

				$.ajax

				({

					url: "<?php echo base_url(); ?>Admin/Doctor/schedule_search",

					type: "POST",

					data: $('#schedule_search').serializeArray(),

      //dataType: "json",

      success: function(response)

      {

      	$('#schedule_search_data').html(response);

      }

  });

			});



		</script>