<?php defined('BASEPATH') or exit('No direct script access allowed');



class Subscriptionexpir extends MX_Controller
{



  public function __construct()
  {

    parent::__construct();

    $this->load->library('session');

    $this->load->library('form_validation');

    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

    $this->load->model('testreport_model');

    $this->load->model('Common_model_new');

    $this->load->helper(array('common_helper'));

    $this->load->helper(array('url'));

    $this->load->helper(array('form'));
  }







  public function subscription_expiation()
  {

    $wher = array('status' => 1, 'user_verify_by_app' => 1, 'device_verify' => 1, 'device_verify_by_token' => 1, 'user_role' => 2);

    $users_list  = $this->Api_model->getAllUsers('cp_users', 'id', 'desc', $wher);

    foreach ($users_list as $Usr) {

      $user_id = $Usr->id;

      $totUsr[] = $this->common_model->getsingle('subscription_payment_transactions', array('user_id' => $user_id));
    }
    //$check='';

    foreach ($totUsr as $Usr) {

      $expiry_date = $Usr->endDate; // example

      //$expiry_date = '2020-07-18'; // example

      $timestamp = strtotime($expiry_date);

      $warning_days = 2;

      $seconds_diff = $warning_days * 24 * 3600; // taking 60 (days) * 24 (hours) * 3600 (seconds)

      $warning_timestamp = $timestamp - $seconds_diff;

      $warning_date = date('Y-m-d', $warning_timestamp);

      if (date('Y-m-d') == $warning_date) {

        $TemplateData = $this->common_model->getSingleRecordById('mail_template', array('template_id' => 7));

        $userId = $Usr->user_id;

        $planId = $Usr->plan_id;

        $UsrData = $this->common_model->getSingleRecordById('cp_users', array('id' => $userId));

        $PlanData = $this->common_model->getSingleRecordById('subscription_plan', array('id' => $planId));

        $plan_name = $PlanData['plan_name'];

        $plan_days = $PlanData['plan_days'];

        $usrsName = $UsrData['name'] . ' ' . $UsrData['lastname'];

        $usrEmail = $UsrData['email'];

        $enddate = $Usr->endDate;

        $findArray = array("%Drname%", "%planname%", "%Plan_days%", "%expirdate%");

        $replaaceArray = array($usrsName, $plan_name, $plan_days, $enddate);

        $description = str_replace($findArray, $replaaceArray, $TemplateData['description']);

        $message = $description;

        $subject = $TemplateData['subject'];

        $check = new_send_mail($message, $subject, $usrEmail, '');
      }
    }


    if ($check) {

      $resp = array('status_code' => SUCCESS, 'status' => 'true', 'message' => 'SUCCESS', 'response' => 'true');
    } else {

      $resp = array('status_code' => FAILURE, 'status' => 'false', 'message' => 'SUCCESS', 'response' => 'false');
    }

    //$this->response($resp);

    echo json_encode($resp);
  }
}
?>