<style>
  .text_heading {
    border: 0;
    margin-bottom: 14px;
  }
  span.blnk_photo {
    float: left;
    width: 100%;
    text-align: center;
    margin-bottom: 10px;
}
</style>
<script src="<?php echo base_url(); ?>assets/js/jquery.min5.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-latest.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/user_dashboard/css/jquery.fancybox.css" media="screen" />

<script type="text/javascript" src="<?php echo base_url(); ?>assets/user_dashboard/js/jquery.fancybox.pack.js"></script>



<script type="text/javascript">
  var checkboxes = $("input[type='checkbox']"),

    submitButt = $("#btn_delete");



  checkboxes.click(function() {

    submitButt.attr("disabled", !checkboxes.is(":checked"));

  });
</script>





<script type="text/javascript">
  $(document).ready(function() {

    $(".fancybox").fancybox();

  });
</script>

<div class=" text_heading">



  <h3 class="text-center">

    <i class="fa fa-heart" aria-hidden="true"></i> Favorite Photos




  </h3>


  <div class="rem_add">

    <button class="btn btn-primary"><a style="color:#fff;" href="<?php echo base_url('user_photos'); ?>">Back</a></button> <button class="btn btn-primary chk">Settings</button>

  </div>



</div>

<section class="content weather-bg news_section">

  <!-- setting start -->
  <div class="col-md-12">
    <div class="col-md-10">
      <?php if (!empty($btnsetingArray)) { ?>
        <?php if ($btnsetingArray[0]->settings == '1') { ?>

          <label><input type="radio" name="btnshow" value='1' checked="checked">Now Favorite Photos showing on the mysoultab APP<br></label>

        <?php } else { ?>

          <label> <input type="radio" name="btnshow" value='1' checked="checked">Now Favorite Photos not showing on the mysoultab APP </label>

        <?php } ?>

      <?php } ?>
    </div>

    <div id="para" style="display: none ">



      <form id="submit_form" method="post" action="<?php echo base_url() ?>savsettings ">
        <div class="col-md-10">
          <label> <input type="checkbox" name="butn_seting" id="butn_seting" value="">Please check if want to show the Favorite Photos button in mysoultab App </label>
          <input type="hidden" name="btn" value="favorite_photos">
        </div>
        <div class="col-md-2">
          <input class="btn btn-primary chkdd" type="submit" name="submit" value="submit">
        </div>
      </form>
      <div id="response"></div>
    </div>

    <!-- </div> -->

    <!-- End -->
</section>

<!-- Main content -->

<section class="content photo-list">

  <div class="photo-listMain">

    <div class="buttondiv clearfix text-center">

      <!-- <a href="#" class="bttn opn_mdl"><i class="fa fa-picture-o"></i> Add Photo</a>

                <a href="photos_favourite" class="bttn"><i class="fa fa-heart-o"></i>Favourite Photo</a>

                <a href="#" class="bttn" id="btn_delete"><i class="fa fa-trash-o"></i> Photo Delete</a> -->

      <?php

      if (!empty($user_photo_data)) { ?>

        <button style="float:left;" type="button" id="btn_delete" disabled><i class="fa fa-heart-o"></i> Unfavorite Photo </button>

      <?php } ?>

    </div>

    <div id="deletemsg" style="text-align: center;"></div>

    <div class="row tz-gallery" id="gal_row">
      <!--photos row start-->

      <?php

      if (!empty($user_photo_data)) {

        foreach ($user_photo_data as $key => $value) {

          $fav = $this->common_model->getSingleRecordById('cp_photo_favourite', array('photo_id' => $value['u_photo_id'])); ?>

          <div class="col-xs-6 col-sm-4 col-md-3">

            <div class="sc_photo_list_item ">

              <div class="sc_photo_list_item_photo">

                <a href="<?php echo base_url() . 'uploads/photos/' . $value['u_photo']; ?>" class="lightbox">

                  <img class="wp-post-image" alt="" src="<?php echo base_url() . 'uploads/photos/' . $value['u_photo']; ?>">

                </a>

                <label class="checkbox_con">

                  <input type="checkbox" class="photo_check_box" value="<?php echo $value['u_photo_id']; ?>">

                  <span class="checkmark"></span>

                </label>

                <div class="red box <?php echo $value['u_photo_id']; ?>">

                  <div class="sc_team_item_hover">

                    <div class="sc_team_item_socials" style="display: inline-block;">

                      <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">

                        <div class="sc_socials_item">

                          <a href="#" class="social_icons social_facebook btn_favourite" id="btn_favourite" onclick="unfavrt('<?php echo $fav['photo_id']; ?>')" title="Favourite">

                            <?php

                            if (empty($fav['photo_id'])) {

                            ?>

                              <i class="fa fa-heart-o"></i>

                            <?php

                            } else {

                            ?>

                              <i id="need_unfvt<?php echo $fav['photo_id']; ?>" class="fa fa-heart dipH" style="display: block;"></i>

                            <?php

                            }

                            ?>

                          </a>

                        </div>

                        <div class="sc_socials_item">

                          <!-- <a href="#" class="social_icons social_twitter btnnew_delete" id="btnnew_delete"><i class="fa fa-trash-o"></i></a> -->

                        </div>



                      </div>

                    </div>

                  </div>

                </div>

              </div>



            </div>

          </div>

      <?php }
      } else {

        /*echo '<div class="photo-list-empty">

		    	<span class="blnk_photo"><i class="fa fa-picture-o" aria-hidden="true"></i></span>

		    	<h4>Photos not found</h4>

          </div>';  */

        echo '<div class="photo-list-empty">

          <span class="blnk_photo"><i class="fa fa-picture-o" aria-hidden="true"></i></span>

          <h4>Favorite photos not found</h4>

          </div>';
      } ?>

    </div>

  </div>

</section>

</aside>

<div class="modal fade deleteModal" id="deleteModal" role="dialog">

  <div class="modal-dialog modal-sm">

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Delete</h4>

      </div>

      <div class="modal-body">

        <h3>Are you sure ? You want to delete this</h3>

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-primary" id="ok" data-dismiss="modal">Ok</button>

        <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>

      </div>

    </div>

  </div>

</div>

<div class="modal fade" id="photo-upload" tabindex="-1" role="dialog" aria-hidden="true">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

        <h4 class="modal-title"><i class="fa fa-picture-o   "></i> Add New Photo</h4>

      </div>

      <form method="post" action="uploading_image" id="user_images" enctype="multipart/form-data">

        <div class="modal-body">

          <div class="">

            <div class="clearfix mrT2 pd2 uploader">



              <div class="banpic"><img src="assets/user_dashboard/img/default.png" alt="banner-image"></div>





              <div class="img-upload">

                <label for="imgInp" class="img-upload__label"><i class="fa fa-cloud-upload"></i> Upload Photo</label>

                <input class="img-upload__input" type="file" name="imgInp" id="imgInp" accept="image/x-png,image/gif,image/jpeg" required="">

              </div>

              <button type="submit" class="btn btn-primary pull-right" id="uploading_image"><i class="fa fa-refresh"></i> Update</button>

            </div>

          </div>

        </div>

      </form>

    </div><!-- /.modal-content -->

  </div><!-- /.modal-dialog -->

  <script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

  <script src="<?php echo base_url(); ?>assets/user_dashboard/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>



  <!-- Script add by 95 for add and remove css class to show enable or disable delete photo button start -->

  <script type="text/javascript">
    $(document).ready(function() {

      $('input[type="checkbox"]').click(function() {

        if ($(this).prop("checked") == true) {



          $("#btn_delete").addClass("enbl_btn");

        } else if ($(this).prop("checked") == false) {





          var check_count = $('input:checkbox:checked').length;



          if (check_count == 0) {



            $("#btn_delete").removeClass("enbl_btn");

          }



        }

      });

    });
  </script>

  <!-- Script add by 95 for add and remove css class to show enable or disable delete photo button end -->



  <script type="text/javascript">
    function unfavrt(id) {

      // if(confirm("Are you sure you want to unfavourite?"))

      //   {

      $.ajax({

        url: "<?php echo base_url() . 'add_Favourite_Photo' ?>",

        method: "POST",

        data: {
          id: id
        },

        dataType: "json",

        success: function(response)

        {

          //alert(response.newdata);

          //$("#deletemsg").html('<div class="alert alert-success">unfavourite successfully</div>');

          $("#gal_row").html(response.newdata);



          /*$("#loading").hide();  

            //$("#deletemsg").html("Photo add favourite or unfavourite successfully");

      if(data ==0){

        var msg = 'Successfully Unfavourite';

        

      }

      // else if(data == 0){

      //   var msg = 'Successfully Unfavourite';

      // }

            setTimeout(function(){





                  $("#need_unfvt"+id).removeClass("fa-heart");

                  $("#need_unfvt"+id).addClass("fa-heart-o");



                  $("#deletemsg").html('<div class="alert alert-success">'+'Successfully Unfavourite'+'</div>');

                setTimeout(function(){

                      $("#deletemsg").html("");

                     }, 1500);



        

                //$("#deletemsg").html('<div class="alert alert-success">'+msg+'</div>');

                //window.location.reload(true);

              }, 1000);*/





        }

      })

      //}

    }
  </script>



  <script type="text/javascript">
    var checkboxes = $("input[type='checkbox']"),

      submitButt = $("#btn_delete");



    checkboxes.click(function() {

      submitButt.attr("disabled", !checkboxes.is(":checked"));

    });
  </script>

  <script type="text/javascript">
    $(document).ready(function() {

      $('input[type="checkbox"]').click(function() {

        var inputValue = $(this).attr("value");

        $("." + inputValue).toggle();

      });

    });
  </script>

  <style type="text/css">
    label.checkbox_con {

      position: absolute !important;

      top: 0px;

    }

    .box {

      display: none;

    }

    .red {

      padding: 0px;

      margin: 0px;

      background: rgba(0, 0, 0, 0.46);

      position: absolute;

      left: 0;

      right: 0;

      top: 0;

      bottom: 0;

      filter: alpha(opacity=0);

      text-align: center;

      -webkit-transition: all ease .3s;

      -moz-transition: all ease .3s;

      -ms-transition: all ease .3s;

      -o-transition: all ease .3s;

      transition: all ease .3s;

      height: auto;

      border: 0px;

    }

    .sc_photo_list_item:hover .sc_photo_list_item_photo {

      background: rgba(0, 0, 0, 0.46);

      left: 0;

      right: 0;

      top: 0;

      bottom: 0;

      filter: alpha(opacity=0);

      text-align: center;

      -webkit-transition: all ease .3s;

      -moz-transition: all ease .3s;

      -ms-transition: all ease .3s;

      -o-transition: all ease .3s;

      transition: all ease .3s;

      height: auto;

    }

    .sc_photo_list_item:hover img {

      opacity: 0.3;

    }
  </style>

  <script>
    //js for photo upload model start

    $(document).ready(function() {

      $(".opn_mdl").click(function() {

        $("#photo-upload").modal();

      });

    });

    //js for photo upload model end

    //js for photo upload start

    function readURL(input) {

      if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function(e) {

          $('.banpic img').attr('src', e.target.result);

        }

        reader.readAsDataURL(input.files[0]);

      }

    }

    $("#imgInp").change(function() {

      readURL(this);

    });

    //js for photo upload start                
  </script>

  <script type="text/javascript">
    $(function() {

      $("#user_images").validate({

        // Specify validation rules

        rules: {

          imgInp: {

            required: true,

            email: true

          }

        },

        // Specify validation error messages

        messages: {

          //email: "Enter your email"

          imgInp: {

            required: "Please select image",

          },

        },

        // Make sure the form is submitted to the destination defined

        // in the "action" attribute of the form when valid

        submitHandler: function(form) {

          form.submit();

        }

      });

    });
  </script>

  <script type="text/javascript">
    // $(document).ready(function(){  

    //    $('#btn_delete1').click(function(){

    //          var email_url =  "<?php echo base_url() ?>";

    //          if($("#deleteModal").modal('show'))

    //          {

    //           var id = [];



    //           $(':checkbox:checked').each(function(i){

    //            id[i] = $(this).val();

    //           });

    //           if(id.length === 0) //tell you if the array is empty

    //           {

    //            alert("Please Select atleast one checkbox");

    //           }

    //           else

    //           {

    //            $.ajax({

    //             url:'<?php echo base_url() . 'delete_photos' ?>',

    //             method:'POST',

    //             data:{id:id},

    //             success:function()

    //             {

    //              for(var i=0; i<id.length; i++)

    //              {

    //                $("#deletemsg").html("Photos delete successfully");

    //                setTimeout(function(){

    //                  $("#deletemsg").html("Photos delete successfully");

    //                    window.location.href = email_url+"user_photos";

    //                }, 1000);

    //              }

    //             }



    //            });

    //           }



    //          }

    //          else

    //          {

    //           return false;

    //          }

    //    });

    // });





    // $(document).ready(function(){

    //    $('.btn_favourite').click(function(){

    //        var id = [];

    //       $(':checkbox:checked').each(function(i){

    //        id[i] = $(this).val();

    //      });

    //      if(confirm("Are you sure you want to unfavourite?"))

    //      {

    //       $.ajax({

    //        url:"<?php //echo base_url().'addphotos_favourite'
                    ?>",

    //        method:"POST",

    //        data:{id:id},

    //        success:function(data)

    //        {

    //            $("#loading").hide();  

    //            //$("#deletemsg").html("Photo add favourite or unfavourite successfully");

    //            setTimeout(function(){

    //                $("#deletemsg").html("");

    //                window.location.reload(true);

    //            }, 1000);

    //        }

    //       })

    //      }

    //      else

    //      {

    //       return false;

    //      }

    //    });

    // });

    $(document).on('click', '#btn_delete', function() {

      var id = [];

      $(':checkbox:checked').each(function(i) {

        id[i] = $(this).val();

      });

      if (confirm("Are you sure you want to unfavourite?"))

      {

        $.ajax({

          url: "<?php echo base_url() . 'delete_photo' ?>",

          method: "POST",

          data: {
            id: id
          },

          success: function(data)

          {

            $("#loading").hide();

            $("#deletemsg").html("Successfully Unfavourite");

            setTimeout(function() {

              $("#deletemsg").html("");

              window.location.reload(true);

            }, 1000);



          }

        })

      } else

      {

        return false;

      }

    });

    $(document).on('click', '.btnnew_delete', function() {

      var id = [];

      $(':checkbox:checked').each(function(i) {

        id[i] = $(this).val();

      });

      if (confirm("Are you sure you want to delete photo ?"))

      {

        $.ajax({

          url: "<?php echo base_url() . 'delete_photo' ?>",

          method: "POST",

          data: {
            id: id
          },

          success: function(data)

          {

            $("#loading").hide();

            $("#deletemsg").html("Photo delete successfully");

            setTimeout(function() {

              $("#deletemsg").html("");

              window.location.reload(true);

            }, 1000);

          }

        })

      } else

      {

        return false;

      }

    });
  </script>

