<aside class="right-side new-music artic_list">
	<div class="searchbar-wrap">
 		<div class="src_sbmit">
			<input autocomplete="off" class="form-control" id="article_search" name="article_search" placeholder="Search Article" value="" type="text">
			 <button type="submit" id="btn_music" value="Submit"><i class="fa fa-search"></i></button>				
		</div>		
	</div>
	<section class="content-header no-margin">
        <h1 class="text-center"><i class="fa fa-file-text-o" aria-hidden="true"></i> Article List <div class="rem_add">
        	<!-- <a href="<?php echo base_url();?>add_article" class="btn btn-primary">Add</a> -->

        	<a href="javascript:window.history.back()" class="btn btn-primary">Back</a>
        </div></h1>
    </section>
	<section class="doc_cate">
		<div class="row">
			<div class="col-md-12">
				<div class="bx_leftScrollmain mCustomScrollbar cat-art-new-cls">
					<div class="bx_leftScroll">

						<?php 

						if(!empty($popular_array) || !empty($recent_array) || !empty($myarticle) || !empty($UserFavarticle)){


							?>


						<div id="combine_tab">
                      

							<?php if(!empty($popular_array)){ ?>

						    <div class="widget-header">
						    	<h2>Populars</h2>
						    	<div class="wid-header-rhs">

						    		<?php //if(count($popular_array)>10){ ?>

						    		<a href="<?php echo base_url();?>view_All_articles/popular/<?php echo $this->uri->segment(2); ?>">View all</a>
						    	<?php //} ?>
						    	</div>
						    </div>
						    <div class="demo">
        						<div class="item"></div>
						        <div class="item">
						            <ul id="content-slider" class="content-slider">
						           	<?php foreach ($popular_array as $popular) { ?>
						                <li>
						                	<div class="rail-thumb">
						                   		<a class="dropdown-item" href="<?php echo base_url();?>read_article/<?php echo base64_encode($popular->article_id); ?>/<?php echo $this->uri->segment(2); ?>">
						                   			<img src="<?php echo base_url().'uploads/articles/'.$popular->article_image;?>" class="mCS_img_loaded"></a>
								    		</div>
								    		<div class="rail-content" title="">
								    			<a title="<?php echo $popular->article_title; ?>" href="<?php echo base_url();?>read_article/<?php echo base64_encode($popular->article_id); ?>/<?php echo $this->uri->segment(2); ?>"><?php echo $popular->article_title; ?></a>
							    				<div class="btn-group new-music-btn">
													<a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
													<div class="dropdown-menu read_menu">  
														<a class="dropdown-item" href="<?php echo base_url();?>read_article/<?php echo base64_encode($popular->article_id); ?>/<?php echo $this->uri->segment(2); ?>">Read More</a>
														
												    </div>
												</div>			    			
								    		</div>
						                </li> 

						               <?php } ?>
						                               							                     
						            </ul>
						        </div>
						    </div>

						    <?php } ?>


						    <?php if(!empty($recent_array)){ ?>
						    <div class="widget-header">
						    	<h2>Latest</h2>
						    	<div class="wid-header-rhs">


						    		<?php //if(count($recent_array)>10){ ?>
						    		<a href="<?php echo base_url();?>view_All_articles/recent/<?php echo $this->uri->segment(2); ?>"> View all</a>
						    	<?php //} ?>
						    	</div>
						    </div>
						    <div class="demo">
        						<div class="item"></div>
						        <div class="item">
						            <ul id="content-slider" class="content-slider">
						            	<?php foreach ($recent_array as $recent) { ?>

						            	<li>
						                	<div class="rail-thumb">
						                   		<a class="dropdown-item" href="<?php echo base_url();?>read_article/<?php echo base64_encode($recent->article_id);?>/<?php echo $this->uri->segment(2); ?>">
						                   			<img src="<?php echo base_url().'uploads/articles/'.$recent->article_image;?>" class="mCS_img_loaded"></a>
								    		</div>
								    		<div class="rail-content" title="">
								    			<a title="<?php echo $recent->article_title; ?>" href="<?php echo base_url();?>read_article/<?php echo base64_encode($recent->article_id); ?>/<?php echo $this->uri->segment(2); ?>"><?php echo $recent->article_title; ?></a>
							    				<div class="btn-group new-music-btn">
													<a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
													<div class="dropdown-menu read_menu">
														<a class="dropdown-item" href="<?php echo base_url();?>read_article/<?php echo base64_encode($recent->article_id); ?>/<?php echo $this->uri->segment(2); ?>">Read More</a>
														
												    </div>
												</div>			    			
								    		</div>
						                </li>

						            <?php } ?>
						                
						                                							                     
						            </ul>
						        </div>
						    </div>

						<?php } ?>

						
						    
						   <?php if(!empty($myarticle)){ ?>


						      <div class="widget-header">
						    	<h2>My Articles</h2>
						    	<div class="wid-header-rhs">

						    		<?php //if(count($myarticle)>10){ ?>

						    		<a href="<?php echo base_url();?>view_All_articles/my/<?php echo $this->uri->segment(2); ?>"> View all</a>

						    	<?php //} ?>
						    	</div>
						    </div>
						    <div class="demo">
        						<div class="item"></div>
						        <div class="item">
						            <ul id="content-slider" class="content-slider">

						            	<?php foreach ($myarticle as $my) { ?>

						                <li class="card_one">
						                	<div class="rail-thumb">
						                   		<a class="dropdown-item" href="<?php echo base_url();?>read_article/<?php echo base64_encode($my->article_id); ?>/<?php echo $this->uri->segment(2); ?>">
						                   			<img src="<?php echo base_url().'uploads/articles/'.$my->article_image;?>" class="mCS_img_loaded"></a>
								    		</div>
								    		<div class="rail-content" title="">
								    			<a title="<?php echo $my->article_title; ?>" href="<?php echo base_url();?>read_article/<?php echo base64_encode($my->article_id); ?>/<?php echo $this->uri->segment(2); ?>"><?php echo $my->article_title; ?></a>
							    				<div class="btn-group new-music-btn">
													<a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
													<div class="dropdown-menu ">
														<a class="dropdown-item" href="<?php echo base_url();?>read_article/<?php echo base64_encode($my->article_id); ?>/<?php echo $this->uri->segment(2); ?>">Read More</a>
														
														<a class="dropdown-item" href="<?php echo base_url();?>edit_article/<?php echo $my->article_id; ?>">Edit</a>
														

														<a class="dropdown-item" data-id="<?php echo $my->article_id; ?>" id="deletemoviecat_<?php echo $my->article_id; ?>" href="javascript:void(0);">Delete</a>
												    </div>
												</div>			    			
								    		</div>
						                </li> 

						            <?php } ?>
						                             							                     
						            </ul>
						        </div>
						    </div>

						<?php } ?>



						<?php if(!empty($UserFavarticle)){ ?>

						      <div class="widget-header">
						    	<h2>Favourite</h2>
						    	<div class="wid-header-rhs">

						    		<?php //if(count($UserFavarticle)>10){ ?>
						    		<a href="<?php echo base_url();?>view_All_articles/favorite/<?php echo $this->uri->segment(2); ?>"> View all</a>
						    	<?php //} ?>
						    	</div>
						    </div>
						    <div class="demo">
        						<div class="item"></div>
						        <div class="item">
						            <ul id="content-slider" class="content-slider">

						            	<?php foreach ($UserFavarticle as $userFavrt) { ?>

						                <li>
						                	<div class="rail-thumb">
						                   		<a class="dropdown-item" href="<?php echo base_url();?>read_article/<?php echo base64_encode($userFavrt->article_id); ?>/<?php echo $this->uri->segment(2); ?>">
						                   			<img src="<?php echo base_url().'uploads/articles/'.$userFavrt->article_image;?>" class="mCS_img_loaded"></a>
								    		</div>
								    		<div class="rail-content music_dropdown" title="">
								    			<a title="<?php echo $userFavrt->article_title; ?>" href="<?php echo base_url();?>read_article/<?php echo base64_encode($userFavrt->article_id); ?>/<?php echo $this->uri->segment(2); ?>"><?php echo $userFavrt->article_title; ?></a>
							    				<div class="btn-group new-music-btn">
													<a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
													<div class="dropdown-menu  ">
														<a class="dropdown-item" href="<?php echo base_url();?>read_article/<?php echo base64_encode($userFavrt->article_id); ?>/<?php echo $this->uri->segment(2); ?>">Read More</a>
														
														
												    </div>
												</div>			    			
								    		</div>
						                </li> 

						            <?php } ?>
						                              							                     
						            </ul>
						        </div>
						    </div>
						<?php } ?>

						</div>
						
						<?php }else{ ?>

						<div class="photo-list-empty">
		    	<span class="blnk_photo"><i class="fa fa-picture-o" aria-hidden="true"></i></span>
		    	<h4>Articles not found</h4>
		    </div>

					<?php } ?>



					</div>
				</div>
			</div>					
		</div>	
	</section>
</aside>		


<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->

<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightslider.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightslider.js"></script> 
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.css">
<script src="<?php echo base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>

<script>	
    	 $(document).ready(function() {
			$(".content-slider").lightSlider({
                loop:true,
                keyPress:true,
                responsive: [
                	{
              		breakpoint:1200,
                		settings: {
                    		item:4,
                        }                   
            		},
            		{
            		breakpoint:1100,
	            		settings: {
	                		item:3,
	                    }
            		},
            		{
            		breakpoint:520,
	            		settings: {
	                		item:2,
	                    }
            		},
            		{
            		breakpoint:360,
	            		settings: {
	                		item:1,
	                    }
            		}
            	],
            });
            $('#image-gallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:9,
                slideMargin: 0,    
                speed:500,
                auto:true,
                loop:true,
                onSliderLoad: function() {
                    $('#image-gallery').removeClass('cS-hidden');
                }  
            });
		});
</script>


<script type="text/javascript">
	$(document).on('click','[id^="deletemoviecat_"]',function(){ 
var delete_article = $(this).attr('data-id');

//alert(delete_movie);

if(confirm("Are you sure, you want to delete article ?")){	
	$.ajax({
            type: "POST", 
            url: '<?php echo base_url(); ?>Pages/Article/delete_article/',
            data: 'delete_article='+delete_article,
            //dataType: "json",
			success: function(data)
			{	
			//console.log(data);	
			setTimeout(function(){
            //$("#deletemsg").html("<div class='alert alert-success'>Deleted Successfully</div>");

            // alert('Deleted Successfully');
            // location.reload();
				
				}, 1000);
            }
        });
		$(this).parents(".card_one").animate({ backgroundColor: "#fbc7c7" }, "fast")
			.animate({ opacity: "hide" }, "slow");
	}
});

$(document).on('click','#btn_music',function(e){ 
	var article_search = $("#article_search").val();
	var segment = '<?php echo $this->uri->segment("2"); ?>';
	
window.location = "<?php echo base_url(); ?>category_articles/"+segment+'/'+article_search;	 
	}); 
</script>

<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>