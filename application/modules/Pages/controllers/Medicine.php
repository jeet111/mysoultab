<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Medicine extends MX_Controller {



  public function __construct() {

    parent:: __construct();

    $this->load->library('session');

    $this->load->library('form_validation');

    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');       

    $this->load->model('medicine_model');

    $this->load->model('Common_model_new'); 

    $this->load->helper(array('common_helper'));

    $this->load->helper(array('url'));

    $this->load->helper(array('form'));      

  }

  

  /*Created by 95 for show all Medicine shedule*/

  public function show_medicine()

  {

    $data['menuactive'] = $this->uri->segment(1);

    $checkLogin = $this->session->userdata('logged_in');
    $user_id = $checkLogin['id'];

    if(!empty($checkLogin))

    {

      $data['medicineData']  = $this->medicine_model->jointwotablenn('medicine_schedule','doctor_id', 'cp_doctor','doctor_id',$where=array('user_id'=>$this->session->userdata('logged_in')['id']),'*','medicine_id','desc');

      $data['btnsetingArray'] = $this->Common_model_new->getAllwhere("setting_butns",array("btnname" =>'Medicine Schedule',"user_id"=>$user_id));

      $this->template->set('title', 'Medicine Shedule');

      $this->template->load('new_user_dashboard_layout', 'contents', 'medicine_list',$data);

    }else{

     redirect('login'); 

   }

 }

 /*Created by 95 for add new medicine shedule*/

 public function add_medicine()

 {

  $data['menuactive'] = $this->uri->segment(1);

  $checkLogin = $this->session->userdata('logged_in');

  if(!empty($checkLogin))

  {





   $data['segment'] = $this->uri->segment("2");

   

   if(isset($_POST['msubmit'])){

      //$this->form_validation->set_rules('doctor_name','Doctor Name','required');

    $this->form_validation->set_rules('medicine_name','Medicine Name','required');

    $this->form_validation->set_rules('no_of_days', 'No. of days','required');

      //$this->form_validation->set_rules('medicine_time','Medicine time', 'required');

    if($this->form_validation->run()==TRUE){



      if($this->input->post('remind')){

        $remind_me=$this->input->post('remind');

      }else{

        $remind_me=0;

      }

      

      if(!empty($data['segment'])){

       $new_doc_id = $data['segment'];

     }else{

       $new_doc_id = $this->input->post('doctor_name');

     }
     
     $medicine_time  = $this->input->post('medicine_time');
      $end_date         = date('Y-m-d',strtotime($this->input->post('end_date')));
      $medicine_description = $this->input->post('medicine_description');
     $medicine_times = implode(', ',$medicine_time);

     $data = array(

      'user_id'=>$this->session->userdata('logged_in')['id'],

      'doctor_id'=>$new_doc_id,

      'medicine_name'=>$this->input->post('medicine_name'),

      'medicine_take_days'=>$this->input->post('no_of_days'),

      'medicine_time'=>$medicine_times,

      'medicine_reminder'=>$remind_me,
      'description'=>$medicine_description,
      'end_date'=>$end_date,
      
      'medicine_create_date'=>date('Y-m-d H:i:s')

    );
// _dx($data);
     $insert = $this->medicine_model->insertData('medicine_schedule',$data);

     

     if($remind_me==1){

      $NotiData=array(

        'common_id'=>$insert,

        'user_id'=>$this->session->userdata('logged_in')['id'],

        'is_read'=>0,

        'type'=>1,

        'notification_date'=>date('Y-m-d H:i:s'),

      );

      $this->medicine_model->insertData('cp_common_notification',$NotiData);

    }

    if($insert){

      $this->session->set_flashdata('success','Medicine successfully added.');

      redirect('medicine_list');

    }

  }

}

$data['Doctors']  = $this->medicine_model->getAllOrderBy('cp_doctor','doctor_name','asc');

$this->template->set('title', 'Medicine Shedule');

$this->template->load('new_user_dashboard_layout', 'contents', 'add_medicine',$data);

}else{

 redirect('login'); 

}

}



/*Created by 95 on 27-2-2019 for edit medicine shedule*/

public function edit_medicine($id)

{

  $data['menuactive'] = $this->uri->segment(1);

  $checkLogin = $this->session->userdata('logged_in');

  if(!empty($checkLogin))

  {



    $data['MedicineData'] = $this->medicine_model->getsingle('medicine_schedule',array('medicine_id'=>$id));

    

    if(isset($_POST['msubmit'])){

      $this->form_validation->set_rules('doctor_name','Doctor Name','required');

      $this->form_validation->set_rules('medicine_name','Medicine Name','required');

      $this->form_validation->set_rules('no_of_days', 'No. of days','required');

      //$this->form_validation->set_rules('medicine_time','Medicine time', 'required');

      if($this->form_validation->run()==TRUE){



        if($this->input->post('remind')){

          $remind_me=$this->input->post('remind');

        }else{

          $remind_me=0;

        }  



        $medicine_time  = $this->input->post('medicine_time');



        $medicine_times = implode(',',$medicine_time); 



        $data = array(

          'user_id'=>$this->session->userdata('logged_in')['id'],

          'doctor_id'=>$this->input->post('doctor_name'),

          'medicine_name'=>$this->input->post('medicine_name'),

          'medicine_take_days'=>$this->input->post('no_of_days'),

          'medicine_time'=>$medicine_times,

          'medicine_reminder'=>$remind_me,

          'medicine_create_date'=>date('Y-m-d H:i:s')

        );

        $update = $this->medicine_model->updateData('medicine_schedule',$data,array('medicine_id'=>$id));

        if($update){

          $this->session->set_flashdata('success','Medicine successfully updated.');

          redirect('medicine_list');

        }

      }

    }

    $data['Doctors']  = $this->medicine_model->getAllOrderBy('cp_doctor','doctor_name','asc');

    $this->template->set('title', 'Medicine Shedule');

    $this->template->load('new_user_dashboard_layout', 'contents', 'edit_medicine',$data);

  }else{

   redirect('login'); 

 }

}





/*Created by 95 on 27-2-2019 for multiple delete medicine shedules*/

public function medicine_all_del()

{


  $bodytype = $this->input->post("bodytype");

  $explode = explode(',',$bodytype);

  foreach($explode as $del){

    $this->medicine_model->delete("medicine_schedule",array("medicine_id" => $del));

  }

  echo '1';

}





/*Created by 95 on 27-2-2019 for single delete medicine shedule*/

public function delete_medicineShedule()

{

  $delete_id = $this->uri->segment('2');

  $this->medicine_model->delete("medicine_schedule",array("medicine_id" => $delete_id));

  $this->session->set_flashdata('success', 'Successfully deleted');

  redirect('medicine_list'); 

}





/*Created by 95 on 27-2-2019 for single delete medicine shedule*/

public function view_shedule($medicine_id='')

{

  $data['menuactive'] = $this->uri->segment(1);

    //$delete_id = $this->uri->segment('2');

    //$data['scheduleDetail']=$this->medicine_model->getsingle("medicine_schedule",array("medicine_id" => $medicine_id));



  $data['scheduleDetail']=$this->medicine_model->getMedicineDetail($medicine_id);

  

  $this->template->set('title', 'Medicine Shedule');

  $this->template->load('new_user_dashboard_layout', 'contents', 'view_medicine',$data);

}



public function add_pickup_medicine()

{

	$data['menuactive'] = $this->uri->segment(1);



	// $doctor_id = $this->input->post("doctor_id");

 $medicine_id = $this->input->post("medicine_id");

 $caregiver_id	= $this->input->post("caregiver_id");

 $user_id =  $this->session->userdata('logged_in')['id'];

 

 $data['medicine_name'] = $this->Common_model_new->getAll("medicine_schedule");

 

 $array_user = 'id !='.$user_id.' or user_role=2';

 $data['caregiver_name'] = $this->Common_model_new->getAllwhere("cp_users",$array_user);

 

 $data['doctor_name'] = $this->Common_model_new->getAll("cp_doctor");

 

 if(isset($_POST['submit'])){



  $array = array(

      // 'doctor_id' => $doctor_id,

    'medicine_id' => $medicine_id,

    'caregiver_id' => $caregiver_id,

    'user_id' => $user_id,

    'create_date' => date('Y-m-d H:i:s')

  );	 

  $this->Common_model_new->insertData('picup_medicine',$array);

  

  $caregiver_data = $this->common_model->getsingle("cp_users",array("id" => $caregiver_id));

  $doctormail = $this->common_model->getsingle("cp_doctor",array("doctor_id" => $doctor_id));

  $username = $this->common_model->getsingle("cp_users",array("id" => $user_id));

  $medicine = $this->common_model->getsingle("medicine_schedule",array("medicine_id" => $medicine_id));

  

  $subject = "Pickup Medicine";

  

  $message =

  "<table>

  <tr><th>Caregiver:</th>

  <td>".$caregiver_data->name."</td></tr>

  

  <tr><th>Username:</th>

  <td>".$username->username."</td></tr>

  <tr><th>Medicine:</th>

  <td>".$medicine->medicine_name."</td></tr>

  

  </table>";

  

  $chk_mail1 = sendEmail($caregiver_data->email,$subject,$message);

  $this->session->set_flashdata('success', 'Successfully Inserted');

  redirect('pickupmedicine_list'); 

}



$this->template->set('title', 'Pickup Medicine');

$this->template->load('new_user_dashboard_layout', 'contents', 'add_pickup_medicine',$data); 

}



public function pickupmedicine_list()

{

 $data['menuactive'] = $this->uri->segment(1);

 $user_id =  $this->session->userdata('logged_in')['id'];

 

 

 $data['pickupmedicine_list'] = $this->Common_model_new->getAllwhereorderby("picup_medicine",array("user_id" => $user_id),"id","desc");

 $data['btnsetingArray'] = $this->Common_model_new->getAllwhere("setting_butns",array("btnname" =>'Medicine Schedule',"user_id" => $user_id));

 $this->template->set('title', 'Pickup Medicine'); 

 $this->template->load('new_user_dashboard_layout', 'contents', 'pickupmedicine_list',$data); 

}



public function delete_pickup()

{

 $data['menuactive'] = $this->uri->segment(1);

 

 $segment = $this->uri->segment("2");

 

 $this->Common_model_new->delete("picup_medicine",array("id" => $segment));

 $this->session->set_flashdata('success', 'Successfully deleted');

 

 redirect('pickupmedicine_list'); 

}



public function delete_all_pickup_med()

{

	$data['menuactive'] = $this->uri->segment(1);

  $medicine_id = $this->input->post('bodytype');

  $session_id = $this->session->userdata('logged_in')['id'];

  $explode = explode(",",$medicine_id);

  foreach($explode as $exp){

    $this->Common_model_new->delete('picup_medicine',array('id' => $exp)); 

		// echo $this->db->last_query();

  }

  echo '1';

  

}



public function medicine_reminder()

{ 

  $date = date('d/m/Y H:i:s');



  $user_id = $this->session->userdata('logged_in')['id']; 

  $user = $this->Common_model_new->getAllwhere("cp_users",array("status" => 1));

  



  foreach($user as $u){

   $medicine_data = $this->Common_model_new->getAllwhere("medicine_schedule",array("user_id" => $u->id));

   foreach($medicine_data as $data){

    if($data->medicine_reminder == 1){ 

     $medicine_date = date('Y-m-d',strtotime($data->medicine_create_date ."+$data->medicine_take_days days"));

     $test = $data->medicine_create_date;

     

     

     $explode = explode(' ',$test);

     $dates = $this->getDatesFromRange($explode[0], $medicine_date);

     

     foreach($dates as $dd){

      $today = date('Y-m-d');

      

      if($today == $dd){echo 'test'.'<br>';

      $times = date('h:i A', strtotime($date));

		   //echo $data->medicine_time;

      $explode_time = explode(',',$data->medicine_time);

      foreach($explode_time as $ti){

       if($times == $ti){echo 'testtt'.'<br>';

       $email = $u->email;

       $message =

       "<table>

       <tr><th>Medicine Name:</th>

       <td>".$data->medicine_name."</td></tr>

       <tr><th>Date:</th>

       <td>".$dd."</td></tr>

       <tr><th>Time:</th>

       <td>".$data->medicine_time."</td></tr>

       

       </table>";

       $subject = "Medicine Reminder";

       sendEmail($email,$subject,$message);

       $notification_table = $this->Common_model_new->insertData("cp_notification",array("user_id" => $u->id,"medicine_id" => $data->medicine_id,"notification_date" => date("Y-m-d H:i:s")));

     }}}else{

			  // echo 'error'.'<br>';

     }

   }

   

 }}}die;

 

}



public function readnotification() {

 $medicine = $this->input->post("medicine");

 $update = $this->Common_model_new->updateData('cp_notification', array('is_read' => 1), array('user_id' => $this->session->userdata('logged_in')['id'],'notification_id' => $medicine));

 $response = array('success' => true, 'message' => 'Notification read successfully');

 echo json_encode($response);

}



function getDatesFromRange($start, $end, $format = 'Y-m-d') {

  $array = array();

  $interval = new DateInterval('P1D');



  $realEnd = new DateTime($end);

  $realEnd->add($interval);



  $period = new DatePeriod(new DateTime($start), $interval, $realEnd);



  foreach($period as $date) { 

    $array[] = $date->format($format); 

  }



  return $array;

}





}