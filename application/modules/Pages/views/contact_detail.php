<aside class="right-side">
  <section class="content-header no-margin">
    <h1 class="text-center"><i class="fa fa-newspaper-o"></i> Contact Detail
    <div class="back_reminder"><a href="#" class="btn btn-danger">Back</a></div>
    </h1>
  </section>
  <section class="content photo-list">
    <div class="con_detailMain">
      <div class="row">       
        <div class="col-md-5">
          <div class="pro_img">            
            <img src="http://votivelaravel.in/carepro/uploads/doctor/happydoctor.jpg" class="" alt="">
          </div>
        </div>
        <div class="col-md-7">
          <div class="bx_drDetail">
            <div class="row">
              <div class="col-md-12">
                <div class="bx_nam">
                  <h2 class="tm_member">Dr. Rohit CUTY</h2>
                  <h5 class="tm_position">9826037358</h5>
                </div>
              </div>                        
            </div>
            <ul class="tm-team-details-list">
              <li>                
                <div class="tm-team-list-value">
                  <div class="bx_apoi">
                    <button type="button" class="btn_apoi" name="submit">
                      <i class="fa fa-phone" aria-hidden="true"></i></button>
                  </div> 
                </div>
                <div class="tm-team-list-title">Voice Call</div>
              </li>
              <li>                
                <div class="tm-team-list-value">
                  <div class="bx_apoi">
                    <button type="button" class="btn_apoi" name="submit" id="appointment">
                      <i class="fa fa-video-camera" aria-hidden="true"></i></button>
                  </div> 
                </div>
                <div class="tm-team-list-title">Video Call</div>
              </li>
              <li>                
                <div class="tm-team-list-value">
                  <div class="bx_apoi">
                    <button type="button" class="btn_apoi" name="submit" id="appointment">
                     <i class="fa fa-comments-o" aria-hidden="true"></i></button>
                  </div> 
                </div>
                <div class="tm-team-list-title">Chat</div>
              </li>
            </ul>          
          </div>          
        </div>        
      </div>
    </div>    
  </div>
</section>
</aside>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="assets/admin/js/jquery.validate.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>