<div class="bx_user">

	<!-- <div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_vital_sign">Add Vital Test Type</a></div> -->

</div>



<div class="container-fluid">

	<div class="dash-counter users-main my-sts-table">

		<div class="row">

			<div class="col-md-12">

				<div class="">

					<div class="card-body">

						<div class="status-cng"></div>

						<?php if ($this->session->flashdata('success')) { ?>

							<div class="alert alert-success message">

								<button type="button" class="close" data-dismiss="alert">x</button>

								<?php echo $this->session->flashdata('success'); ?></div>

							<?php } ?>



							<!-- <div class="add-btn"><a href="<?php echo base_url(); ?>admin/add_test_type">Add test type</a></div> -->





							<table class="table table-hover tab_comn" id="sampleTable">

								<thead>

									<tr>

										<th>S no.</th>

										<th>Doctor</th>

										<th>Unit</th>

										<th>Test type</th>

										<th>User</th>

										<th>Create Date</th>

										<th>Action</th>

									</tr>

								</thead>

								<tbody>

									<?php

									$i = 1;

									foreach ($vitalSignData as $list) {



										?>

										<tr>

											<td><?php echo $i++; ?></td>

											<td><?php echo $list->doctor; ?></td>

											<td><?php echo $list->unit; ?></td>

											<td><?php echo $list->test_type; ?></td>

											<td><?php

											foreach ($Users as  $user) {

												if($user['id']==$list->user_id){

													echo $user['name'];

												}

											}

											?></td>

											<td><?php echo $list->create_at; ?></td>





											<td>

												<div class="link-del-view">



													

													

													<!-- <div class="tooltip-2">

														<a href="<?php echo base_url(); ?>admin/edit_test_type/<?php echo $list->id; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>

														<span class="tooltiptext">Edit</span>

													</div> -->

													



													<?php

													if (isset($staff_permission[7]['view_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/view_vital_sign/<?php echo $list->vital_sign_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

															<span class="tooltiptext">View</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="admin/view_vital_sign/<?php echo $list->vital_sign_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

															<span class="tooltiptext">View</span>

														</div>

														<?php

													}

													?>

													

													<?php

													if (isset($staff_permission[7]['delete_permission']) == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/delete_vital_detail/<?php echo $list->vital_sign_id; ?>" onclick="return confirm('Are you sure, you want to delete this vital detail ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

														</div>

														<?php

													} elseif($this->session->userdata('logged_ins')['user_role'] == 1) {

														?>

														<div class="tooltip-2">

															<a href="<?php echo base_url(); ?>admin/delete_vital_detail/<?php echo $list->vital_sign_id; ?>" onclick="return confirm('Are you sure, you want to delete this vital detail ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>

														</div>

														<?php

													}

													?>



												</div>

											</div>

										</td>

									</tr>

								<?php } ?>

							</tbody>

						</table>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>