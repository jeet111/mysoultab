<!DOCTYPE html>
<html lang="en">
<head>
	<title>Health Care</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="assets/styles/bootstrap-4.1.2/bootstrap.min.css">
	<link href="assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="assets/plugins/OwlCarousel2-2.3.4/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="assets/plugins/OwlCarousel2-2.3.4/owl.theme.default.css">
	<link rel="stylesheet" type="text/css" href="assets/plugins/OwlCarousel2-2.3.4/animate.css">
	<!-- <link href="plugins/jquery-datepicker/jquery-ui.css" rel="stylesheet" type="text/css"> -->
	<!-- <link href="plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css"> -->
	<link rel="stylesheet" type="text/css" href="assets/styles/main_styles.css">
	<link rel="stylesheet" type="text/css" href="assets/styles/responsive.css">
	<link href="<?php echo base_url();?>assets/user_dashboard/img/logo.png" rel="icon">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">


	
<!-- 	<link rel="stylesheet" type="text/css" href="assets/styles/all.min.css"> -->


</head>
<body>
	<div class="super_container about-us">
		<!-- Header -->

		<header class="header">
			<div class="header_content d-flex flex-row align-items-center justify-content-start">
				<div class="logo"><a href="<?php echo base_url(); ?>">Health Care</a></div>
				<div class="ml-auto d-flex flex-row align-items-center justify-content-start">
					<nav class="main_nav">
						<ul class="d-flex flex-row align-items-start justify-content-start">
							<li <?php if($menuactive == ''){
								echo 'class="active"';
							}?>><a href='<?php echo base_url();?>'>Home</a></li>
							<li><a href="#">Features</a></li>

							<li <?php if($menuactive == 'about-us'){
								echo 'class="active"';
							}?>><a href="<?php echo base_url(); ?>about-us">About</a></li>

							<?php /* ?>

							<li <?php if($menuactive == 'subscriptions'){
								echo 'class="active"';
							}?>><a href="<?php echo base_url(); ?>subscriptions">Subscriptions</a></li>

							<?php */ ?>

							<li  <?php if($menuactive == 'contact'){
								echo 'class="active"';
							}?>><a href="<?php echo base_url(); ?>contact">Contact</a></li>
						</ul>
					</nav>
					<div class="book_button"><a href="signup"><i class="fa fa-lock"></i> Sign Up</a></div>


					<!-- Hamburger Menu -->
					<div class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></div>
				</div>
			</div>
		</header>
		<div class="menu trans_400 d-flex flex-column align-items-end justify-content-start">
			<div class="menu_close"><i class="fa fa-times" aria-hidden="true"></i></div>
			<div class="menu_content">
				<nav class="menu_nav text-right">
					<ul>
						<li <?php if($menuactive == ''){
							echo 'class="active"';
						}?>><a href='<?php echo base_url();?>'>Home</a></li>
						<li><a href="#">Features</a></li>
						<li <?php if($menuactive == 'about-us'){
							echo 'class="active"';
						}?>><a href="<?php echo base_url(); ?>about-us">About</a></li>

						<?php /* ?>
						<li <?php if($menuactive == 'subscriptions'){
							echo 'class="active"';
						}?>><a href="<?php echo base_url(); ?>subscriptions">Subscriptions</a></li>

						<?php */ ?>

						<li  <?php if($menuactive == 'contact'){
							echo 'class="active"';
						}?>><a href="<?php echo base_url(); ?>contact">Contact</a></li>
						<li  <?php if($menuactive == 'contact'){
							echo 'class="active"';
						}?>><a href="<?php echo base_url(); ?>signup">Sign Up</a></li>
					</ul>
				</nav>
			</div>
			<div class="menu_extra">
				<div class="menu_book text-right"><a href="#">Book online</a></div>
				<div class="menu_phone d-flex flex-row align-items-center justify-content-center">
					<img src="assets/images/phone-2.png" alt="">
					<span>0183-12345678</span>
				</div>
			</div>
		</div>
