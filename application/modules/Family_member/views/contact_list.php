<aside class="right-side">
    <section class="content-header no-margin">
        <h1 class="text-center"><i class="fa fa-list"></i> Contact List<div class="rem_add"><a href="<?php echo base_url();?>family_member/add_contact" class="btn btn-primary" >Add</a></div></h1>
    </section>
<?php //echo "<pre>"; print_r($ContactData); echo "</pre>"; ?>
<section class="content photo-list">
    <div class="photo-listMain reminder_listMain">
        <div class="row swt_cor">
            <div class="col-sm-6">
                <label style="margin-right: 10px;">
                    <input type="checkbox" id="checkallFamilyContacts">
                </label>
                <div class="delete-email">
                    <a href="#" name="familycontact_all_del" id="familycontact_all_del" class="familycontact_all_del" data-base="<?php echo base_url();?>"><i aria-hidden="true" class="fa fa-trash-o"></i></a>
                </div>
            </div>
        </div>
        <div class="table-responsive tb_swt">
            <div id="expire_msg"></div>
            <?php if ($this->session->flashdata('success')) { ?>
            <div class="alert alert-success message">
                <button type="button" class="close" data-dismiss="alert">x</button>
            <?php echo $this->session->flashdata('success'); ?></div>
            <?php } ?>
            <table class="table table-hover table-bordered table-mailbox send-user-mail tab_medi" id="sampleTable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Contact Name</th>
                        <th>Contact Number</th>
                        <!-- <th>Description</th> -->
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($ContactData as $contact) {
                    ?>
                    <tr>
                        <td class="small-col" >
                            <input type="checkbox" name="familycontact_chk[]"  class="familycontact_chk" value="<?php echo $contact->contact_id;?>" data-id="<?php echo $report->report_id;  ?>"/>
                        </td>
                        <td><?php echo $contact->contact_name; ?></td>
                        <td><?php echo $contact->contact_mobile_number; ?></td>
                        <!-- <td><?php echo $report->description; ?></td> -->
                        <td>

                            <a href="<?php echo base_url();?>family_member/view_contact/<?php echo $contact->contact_id;?>" class="edit_btn">View</a>

                            <a href="<?php echo base_url();?>family_member/contact_edit/<?php echo $contact->contact_id;?>" class="edit_btn">Edit</a>
                            <a href="<?php echo base_url();?>family_member/delete_contact/<?php echo $contact->contact_id;?>" onclick="return confirm('Are you sure you want to remove this contact?')" class="edit_btn">Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
</aside>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>


<!-- <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->

