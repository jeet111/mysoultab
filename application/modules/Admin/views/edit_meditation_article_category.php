<!-- <div class="back_ad"><a class="admin_back" href="<?php echo base_url(); ?>admin/music_category_list" >Back</a></div> -->
<style type="text/css">
	div#video_loader {
	position: fixed;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	background-color: #0a0a0aa6;
	z-index: 999999;
	text-align: center;
	padding-top: 23%;
}
div#video_loader img#loading_img {
	width: 70px !important;
}
</style>
<div class="container-fluid">
	<div class="dash-counter">
		<div class="Schedule_main_one">
			<div class="users-main">
				<?php //echo "<pre>"; print_r($singleData); echo "</pre>"; ?>


				<?php
				echo "<h2>" ?><?php if($this->uri->segment('3')){ ?>Edit Article Category<?php }else{ ?>Add Article Category <?php } ?></h2>

				<div class="btn_topBack">
					<a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">
						Back
					</a>
				</div>

				<form class="form-horizontal" action="" method="post" enctype="multipart/form-data" name="test_type_add" id="edit_article">
					<?php if ($this->session->flashdata('success')) { ?>
						<div class="alert alert-success message">
							<button type="button" class="close" data-dismiss="alert">x</button>
							<?php echo $this->session->flashdata('success'); ?></div>
						<?php } ?>
						<div id="image_msg"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Category Name:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<input type="text" placeholder="Category Name" class="form-control" name="category_name" id="category_name" value="<?php echo $singleData->article_category_name; ?>">
										<?php echo form_error('category_name'); ?>

									</div>
								</div>
							</div>
							<input type="hidden" name="cat_id" id="cat_id" value="<?php echo $segment; ?>">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Category Icon:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<input type="file" class="form-control" name="category_icon" id="category_icon" value="<?php echo $singleData->article_category_icon; ?>" onchange="loadFile(event)">
										<?php echo form_error('category_icon'); ?>
										<div id="flupmsgs"></div>

										<div class="cat-img">   
											<img class="cat_music" id='img-upload' src="<?php echo base_url().'uploads/meditation_articles/category_icon/'.$singleData->article_category_icon; ?>">
										</div>

										<div>
											<span id="demo" style="color: red"></span>
										</div>
									</div>
								</div>
							</div>
							<div id="loders"></div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4" for="">Category Description:<span class="field_req">*</span></label>
									<div class="col-md-8">
										<textarea class="form-control" name="category_desc" id="category_desc" rows="10" cols="50"><?php echo $singleData->article_category_desc; ?></textarea>
										<?php echo form_error('category_desc'); ?>

									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button name="submit" type="submit" id="postdata"  class="btn btn-default pst-btn-new ">Submit</button>



							<!-- <a class="cancel-btn btn" href="javascript:window.history.back()">
                        Back
                    </a> -->
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>

<!--Added by 95 on 5-2-2019 For Doctor category for validation-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js?version=2.3"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>
<script type="text/javascript">

	$("#edit_article").on('submit',(function(e)
	{
		e.preventDefault();
		var isvalidate=$("#edit_article").valid();
		if(isvalidate)
		{


			// <img id="loading_img" style="width:20px" src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif" />


			$("#loders").append('<div id="video_loader" class="loading_class"><img id="loading_img" style="width:20px" src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif" /></div>');
			$.ajax({
				url : "<?php echo base_url(); ?>Admin/Meditation_article/ajax_edit_meditation_article_category",
				type: "POST",
				data: new FormData(this),
				dataType: "json",
				contentType: false,
				cache: false,
				processData:false,
				success: function(data) {
				//console.log(data);
				$("#loading_img").hide();
				if(data.code == 1){
					$("#loading_img").hide();
					$("#sub").prop('disabled', true);

					$("#image_msg").html('<div class="alert alert-success">'+data.message+'<div>');
					setTimeout(function(){


						$("#image_msg").html("");
            //location.reload();
            window.location.href = '<?php echo base_url(); ?>admin/meditation_article_category_list';
        }, 1000);
				}else{
					$("#loading_img").hide();

					$("#image_msg").html('<div class="alert alert-danger">'+data.message+'<div>');

				}
			}
		});
		}
	}));
	$(document).ready(function() {
		$("#edit_article").validate({
			rules: {
				category_name: "required",
				category_desc: "required",

			},
			messages:{
				category_name:"Plesase enter a category name.",
				category_desc: "Plesase enter a category description.",
			}

		});

	});

	function showFileSize(){
		var img  = document.getElementById('profile_images').value;
		if(img==='' || img===null){
			document.getElementById("demo").innerHTML="Please choose an image.";
		}else{
			document.getElementById("demo").innerHTML=" ";
		}
	}

	$('#doctor_fees').keypress(function (e) {
		var regex = new RegExp("^[0-9]+$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test(str)) {
			return true;
		}

		e.preventDefault();
		return false;
	});


	/*Created by 95 on 22-2-2019 for disable alphabets in dr. mobile field*/
	$('#doctor_mobile').keypress(function (e) {
		var regex = new RegExp("^[0-9]+$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test(str)) {
			return true;
		}
		e.preventDefault();
		return false;
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#doctor_category').multiselect({
			includeSelectAllOption: true,
			allSelectedText: 'No option left ...'
		});
	});


	function loadFile (event) {
		var pcFile = $('#category_icon').val().split('\\').pop();
		var pcExt     = pcFile.split('.').pop();
		var output = document.getElementById('img-upload');
		if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"
			|| pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){

			$('#img-upload').show();
		$("#postdata").prop('disabled', false);
		output.src = URL.createObjectURL(event.target.files[0]);
		$('#flupmsgs').html('');
	}else{
		$("#postdata").prop('disabled', true);
		$('#flupmsgs').html('Please select only Image file.');
		$('#img-upload').hide();
	}
};

</script>

<style>
article, aside, figure, footer, header, hgroup,
menu, nav, section { display: block; }




.btn.btn-default.pst-btn-new {
	display: inline-block;
	margin-bottom: 0px;
	font-size: 14px;
	font-weight: 400;
	line-height: 1.42857;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	touch-action: manipulation;
	cursor: pointer;
	user-select: none;
	background-image: none;
	background-color: rgb(38, 156, 231);
	color: rgb(255, 255, 255);
	padding: 6px 12px;
	border-width: 1px;
	border-style: solid;
	border-color: transparent;
	border-image: initial;
	border-radius: 0px;
}
</style>