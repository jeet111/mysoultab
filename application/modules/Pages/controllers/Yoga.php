<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Yoga extends MX_Controller {
  public function __construct() {
    parent:: __construct();
    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->library('table');
    $this->load->library('pagination');
    $this->load->model('common_model');
    $this->load->model('common_model_new');
    $this->load->model('Common_model_new');
    $this->load->helper(array('common_helper'));
    $this->load->helper(array('url'));
    $this->load->helper(array('form'));
    not_login();
  }
  public function yoga(){ 
    $checkLogin = $this->session->userdata('logged_in');
    $user_id = $this->session->userdata('logged_in')['id'];
    if(!empty($checkLogin)) 
    {
      $data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'yoga','user_id'=>$user_id));
      $data['YogaDataUser'] = $this->common_model_new->getAllwhere("yoga_videos",array("active" =>'1'));
      $data['YogaSearchUser'] = $this->common_model_new->getAllYogavideowhere("yoga_videos",array("active" =>'1'));      
      $data['getAllbodypart'] = $this->common_model_new->getAllbodypart("yoga_videos",array("active" =>'1'));
      $data['getAllyogastyle'] = $this->common_model_new->getAllyogastyle("yoga_videos",array("active" =>'1'));
      $data['getAllyogaduration'] = $this->common_model_new->getAllyogaduration("yoga_videos",array("active" =>'1'));
      $data['getAllyogatype'] = $this->common_model_new->getAllyogatype("yoga_videos",array("active" =>'1'));
      $data['getAllyogapose'] = $this->common_model_new->getAllyogapose("yoga_videos",array("active" =>'1'));
      $data['getAllyogalevel'] = $this->common_model_new->getAllyogalevel("yoga_videos",array("active" =>'1')); 
      $data['getAllhelpwith'] = $this->common_model_new->getAllhelpwith("yoga_videos",array("active" =>'1'));
      $data['yogaFavVideoresult'] = $this->common_model_new->getAllwhere("yoga_fav_video",array("user_id" => $user_id));
      $this->template->set('title', 'Yoga');
      $this->template->load('new_user_dashboard_layout', 'contents', 'yoga_view',$data); 
    }else{
      redirect('login');
    }
  }

}
?>
