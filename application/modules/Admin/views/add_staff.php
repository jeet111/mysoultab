<div class="container-fluid">
   <div class="dash-counter">
      <div class="Schedule_main_one">
         <div class="users-main">
            <h2>
               <?php if($this->uri->segment('3')){ ?>
                  Edit Staff
               <?php }else{ ?>
                  Add Staff
               <?php } ?>
            </h2>
            <div class="btn_topBack">
               <a class="cancel-btn btn bk_btn" href="javascript:window.history.back()">
                  Back
               </a>
            </div>
            <?php
            if (empty($segment)) {
               $url = base_url().'/admin/add_staff';
            } else {
               $url = base_url().'/admin/edit_staff/'.$segment;
            }
            ?>
            <form class="form-horizontal" action="<?php echo $url; ?>" method="post" enctype="multipart/form-data" name="addStaffForm" id="addStaffForm">
               <?php 
               if ($this->session->flashdata('success')) 
               { 
                  ?>
                  <div class="alert alert-success message">
                     <button type="button" class="close" data-dismiss="alert">x</button>
                     <?php echo $this->session->flashdata('success'); ?>
                  </div>
                  <?php 
               } 
               ?>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label col-md-4" for="">
                           First Name:<span class="field_req">*</span>
                        </label>
                        <div class="col-md-8">
                           <input type="text" placeholder="First Name" class="form-control" name="first_name" id="first_name" value="<?php echo $singleData->name; ?>">
                           <?php echo form_error('first_name'); ?>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label col-md-4" for="">
                           Last Name:<span class="field_req">*</span>
                        </label>
                        <div class="col-md-8">
                           <input type="text" placeholder="Last Name" class="form-control" name="last_name" id="last_name" value="<?php echo $singleData->lastname; ?>">
                           <?php echo form_error('last_name'); ?>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label col-md-4" for="">
                           Username:<span class="field_req">*</span>
                        </label>
                        <div class="col-md-8">
                           <input type="text" placeholder="Username" class="form-control" name="username" id="username" value="<?php echo $singleData->username; ?>">
                           <?php echo form_error('username'); ?>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label col-md-4" for="">
                           Email:<span class="field_req">*</span>
                        </label>
                        <div class="col-md-8">
                           <input type="email" placeholder="Email" class="form-control" name="email" id="email" value="<?php echo $singleData->email; ?>">
                           <?php echo form_error('email'); ?>
                        </div>
                     </div>
                  </div>
               </div>

               <?php
               if (empty($segment)) {
                  ?>
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label col-md-4" for="">
                              Password:<span class="field_req">*</span>
                           </label>
                           <div class="col-md-8">
                              <input type="password" placeholder="Password" class="form-control" name="password" id="password" value="<?php echo $singleData->password; ?>">
                              <?php echo form_error('password'); ?>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label col-md-4" for="">
                              Confirm Password:<span class="field_req">*</span>
                           </label>
                           <div class="col-md-8">
                              <input type="password" placeholder="Confirm Password" class="form-control" name="confirm_password" id="confirm_password">
                              <?php echo form_error('confirm_password'); ?>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php
               }
               ?>

               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label col-md-4" for="">
                           Mobile:<span class="field_req">*</span>
                        </label>
                        <div class="col-md-8">
                           <input type="text" placeholder="Mobile" class="form-control" name="mobile" id="mobile" value="<?php echo $singleData->mobile; ?>">
                           <?php echo form_error('mobile'); ?>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                     <?php
                     if (empty($segment)) {
                        ?><input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Submit"><?php
                     } else {
                        ?><input type="submit" class="btn btn-default up_but" class="form-control" name="submit" value="Update"><?php
                     }
                     ?>
                     
                     <!-- <a class="cancel-btn btn" href="javascript:window.history.back()">Back</a> -->
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!--Added by 95 on 5-2-2019 For Doctor category for validation-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
      $("#addStaffForm").validate({
         rules: {
            first_name: {
               required: true
            },
            last_name: {
               required: true
            },
            username: {
               required: true,
               remote: "<?php echo base_url().'admin/check_username'; ?>"
            },
            email: {
               required: true,
               email: true,
               remote: "<?php echo base_url().'admin/check_email'; ?>"
            },
            password: {
               required: true,
               minlength: 5
            },
            confirm_password: {
               minlength: 5,
               equalTo: "#password"
            },    
            mobile:  {
               required: true,
               digits: true,
               remote: "<?php echo base_url().'admin/check_mobile'; ?>"
            }
         },
         messages:{
            /*first_name:"Please enter first name.",
            last_name:"Please enter last name.",
            username:"Please enter username.",
            email:"Please enter email.",
            password:"Please enter password.",
            mobile:"Please enter mobile."*/
            username: {
               remote: "Username already exist."
            },
            email: {
               remote: "Email already exist."
            },
            mobile:  {
               remote: "Mobile already exist."
            }
         }
      });
   });
   
   
   /*Created by 95 on 6-2-2019 For Image validation*/
   // function showFileSize(){
   //    var img  = document.getElementById('profile_images').value;
   //     var file = img.lastIndexOf('.');
   //     var fileType  = img.slice(file);
   
   //     if(img==='' || img===null){
   //       document.getElementById("demo").innerHTML="Please choose an image.";
   //   }else if(fileType!='.jpg'||fileType!='.png'||fileType!='.gif'||fileType!='.jpeg'){
   
   //       document.getElementById("demo").innerHTML="Only allowed image formats jpg,png,gif,jpeg";
   //     }else{
   //       document.getElementById("demo").innerHTML=" ";
   //     }
   //   }
   
   
   function showFileSize(){
      var img  = document.getElementById('profile_images').value;
      if(img==='' || img===null){
         document.getElementById("demo").innerHTML="Please choose an image.";
      }else{
         document.getElementById("demo").innerHTML=" ";
      }
   }
   
   
   // /*Created by 95 on 6-2-2019 For Image validation*/
   // function showFileSize(){
   //    var img  = document.getElementById('profile_images').value;
   //     if(img==='' || img===null){
   //       document.getElementById("demo").innerHTML="Please choose an image.";
   //   }else{
   //       document.getElementById("demo").innerHTML=" ";
   //     }
   //   }
   
   
   /*Created by 95 on 22-2-2019 for disable alphabets in dr. fees field*/
   $('#doctor_fees').keypress(function (e) {
      var regex = new RegExp("^[0-9]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
         return true;
      }
      
      e.preventDefault();
      return false;
   });
   
   
   /*Created by 95 on 22-2-2019 for disable alphabets in dr. mobile field*/
   $('#doctor_mobile').keypress(function (e) {
      var regex = new RegExp("^[0-9]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
         return true;
      }
      e.preventDefault();
      return false;
   });
</script>
<script type="text/javascript">
   $(document).ready(function() {
      $('#doctor_category').multiselect({
         includeSelectAllOption: true,
         allSelectedText: 'No option left ...'
      });
   });
   
   
   // function readURL(input) {
   //         if (input.files && input.files[0]) {
   //             var reader = new FileReader();
   
   //             reader.onload = function (e) {
   //                 $('#img-upload')
   //                     .attr('src', e.target.result)
   //                     .width(150)
   //                     .height(200);
   //             };
   
   //             reader.readAsDataURL(input.files[0]);
   //         }
   //     }
   
   function readURL (event) {
      var pcFile = $('input[type=file]').val().split('\\').pop();
      var pcExt     = pcFile.split('.').pop();
      var output = document.getElementById('img-upload');
      if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"
       || pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){
         $('#img-upload').show();
         output.src = URL.createObjectURL(event.target.files[0]);
         $('#flupmsg').html('');
      }else{
         $('#flupmsg').html('Please select only Image file');
         $('#img-upload').hide();
      }
   };
   
</script>
<style>
   article, aside, figure, footer, header, hgroup,
   menu, nav, section { display: block; }
</style>