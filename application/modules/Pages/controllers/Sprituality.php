<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Sprituality extends MX_Controller {
  public function __construct() {
    parent:: __construct();
    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->library('table');
    $this->load->library('pagination');
    $this->load->model('common_model');
    $this->load->model('common_model_new');
    $this->load->model('Common_model_new');
    $this->load->helper(array('common_helper'));
    $this->load->helper(array('url'));
    $this->load->helper(array('form'));
  

    not_login();
  }
  public function sprituality(){ 
    $checkLogin = $this->session->userdata('logged_in');
    $user_id = $this->session->userdata('logged_in')['id'];

    if(!empty($checkLogin)) 
    {
      $data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'Sprituality',"user_id"=>$user_id));
      $this->template->set('title', 'Sprituality');
      // $this->template->load('user_dashboard_layout', 'contents', 'sprituality_view',$data); 
      $this->template->load('new_user_dashboard_layout', 'contents', 'spirituality',$data); 
      
    }else{
      redirect('login');
    }
  }
}
?>
