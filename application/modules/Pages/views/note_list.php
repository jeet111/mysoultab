<style>
  div#msg_show {
    float: left;
    width: 100%;
    margin-top: 15px !important;
  }
</style>
<div id="msg_show"></div>

<div class=" text_heading">

  <h3 class="text-center">Note List <div class="rem_add"><a href="<?php echo base_url(); ?>add_note" class="btn btn-primary">Add</a></div>
  </h3>
</div>
<section class="content weather-bg news_section">
  <!-- setting start -->
  <div class="col-md-12">

    <div class="col-md-10">
      <h3>Note List</h3>

      <?php
      if (is_array($btnsetingArray) && !empty($btnsetingArray) && $btnsetingArray != '') {
        foreach ($btnsetingArray as $res) {
          if ($res->settings == 1) { ?>
            <div class="tooltip-2"><a data-btn="<?php echo 'note_list' ?>" href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
              <span class="tooltiptext">Active</span>
            </div>
          <?php } else { ?>
            <div class="tooltip-2 "><a data-btn="<?php echo 'note_list' ?>" href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
              <span class="tooltiptext">Deactive</span>
            </div>
          <?php } ?>
      <?php }
      } ?>
    </div>

</section>

<section class="content photo-list">

  <div class="photo-listMain reminder_listMain">

    <div class="row swt_cor">

      <div class="col-sm-6">

        <label style="margin-right: 10px;">

          <input type="checkbox" id="checkall">

        </label>

        <div class="delete-email">

          <a href="javascript:void(0);" name="btn_delete" id="btn_delete" class="btn_delete"><i aria-hidden="true" class="fa fa-trash-o"></i></a>

        </div>

      </div>

    </div>



    <div class="table-responsive tb_swt">

      <table class="table table-hover table-bordered table-mailbox send-user-mail tab_medi" id="sampleTable">

        <thead>

          <tr>

            <th>#</th>

            <th>Username</th>

            <th>To Caregiver</th>

            <th>Action</th>

          </tr>

        </thead>

        <tbody>

          <?php
          //  print_r($note_list);
          if (!empty($note_list) && !empty($note_list) && $note_list != '') {

            foreach ($note_list as $note) {


              if (!empty($note->from_caregiver)) {
                $note->from_caregiver = $note->from_caregiver;
              }


              $fromcaregiver = $this->Common_model_new->getsingle("cp_users", array("id" => $note->from_caregiver));

              if (!empty($note->to_caregiver)) {
                $note->to_caregiver = $note->to_caregiver;
              }


              $tocaregiver = $this->Common_model_new->getsingle("cp_users", array("id" => $note->to_caregiver));

          ?>

              <tr>

                <td class="small-col">

                  <input type="checkbox" name="sub_chk[]" class="sub_chk" value="<?php echo $note->note_id; ?>" />

                </td>



                <td data-label="Username"><?php if (!empty($fromcaregiver->name)) {
                      echo $fromcaregiver->name . ' ' . $fromcaregiver->lastname;
                    } ?></td>

                <td data-label="To Caregiver"><?php if (!empty($tocaregiver->username)) {
                      echo $tocaregiver->username;
                    } ?></td>



                <td data-label="Action">

                  <a href="<?php echo base_url(); ?>view_note/<?php echo $note->note_id; ?>" class="edit_btn">View</a>

                  <a href="<?php echo base_url(); ?>edit_note/<?php echo $note->note_id; ?>" class="edit_btn">Edit</a>

                  <a href="<?php echo base_url(); ?>delete_note/<?php echo $note->note_id; ?>" onclick="return confirm('Are you sure, you want to remove this note ?')" class="edit_btn">Delete</a>

                </td>

              </tr>



          <?php }
          } ?>



        </tbody>

      </table>

    </div>

  </div>

</section>

</aside>
<script src="<?php echo base_url(); ?>assets/js/jquery.min7.js"></script>;

<script>
  jQuery('#description').val($.trim(jQuery('#description').val()));

  jQuery(document).on('click', '.updateStatus', function() {
    var update_status = $(this).attr('data-btn');
    var update_st = $(this).attr('data-st');
    //alert(update_status+'-'+update_st);
    $.ajax({
      url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
      type: "POST",
      data: {
        'btn': update_status
      },
      dataType: "json",
      success: function(data) {
        setTimeout(function() {
          $("#msg_show").html("<div class='alert alert-success'>" + data.status + "</div>");
          location.reload();

        }, 400);
      }
    });
  });




  jQuery('#checkall').on('click', function(e) {

    if ($(this).is(':checked', true))

    {

      $(".sub_chk").prop('checked', true);

    } else

    {

      $(".sub_chk").prop('checked', false);

    }

  });

  jQuery('.sub_chk').on('click', function() {

    if ($('.sub_chk:checked').length == $('.sub_chk').length) {

      $('#checkall').prop('checked', true);

    } else {

      $('#checkall').prop('checked', false);

    }

  });





  $(document).on('click', '.btn_delete', function(e) {





    var bodytype = $('input[name="sub_chk[]"]:checked').map(function() {

      return this.value;

    }).get().join(",");



    if (bodytype != '') {

      if (confirm("Are you sure you want to remove this note ?")) {

        $.ajax({

          url: '<?php echo base_url(); ?>Pages/Note/delete_checkboxes',

          type: 'POST',

          data: {
            bodytype: bodytype
          },

          success: function(data) {
            setTimeout(function() {
              $("#expire_msg").html("<div class='alert alert-success message'>Successfully deleted !</div>");
              location.reload();

            }, 1300);

          }

        });



      }

    } else {

      alert("Please select atleast one note.");

    }

  });
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>