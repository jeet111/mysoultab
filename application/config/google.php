<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
|  Google API Configuration
| -------------------------------------------------------------------
| 
| To get API details you have to create a Google Project
| at Google API Console (https://console.developers.google.com)
| 
|  client_id         string   Your Google API Client ID.
|  client_secret     string   Your Google API Client secret.
|  redirect_uri      string   URL to redirect back to after login.
|  application_name  string   Your Google application name.
|  api_key           string   Developer key.
|  scopes            string   Specify scopes
*/
$config['google_client_id']        = '828382321944-ffujajclhfquarkc0kjgefkt41pil9r4.apps.googleusercontent.com';
$config['google_client_secret']    = 'CPHxncKN05JaQ9wecGx4VGGP';
$config['google_redirect_uri']     = base_url().'google/login';
$config['google_application_name'] = 'Grab It';
$config['google_api_key']          = '';
$config['google_scopes']           = array();