

<!-- Right side column. Contains the navbar and content of the page -->

<style>

   .loading {

  background: transparent url('http://thinkfuture.com/wp-content/uploads/2013/10/loading_spinner.gif') center no-repeat;

}

</style>



<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-latest.min.js"></script>

<link rel="stylesheet" type="text/css" href="assets/user_dashboard/css/jquery.fancybox.css" media="screen"/>

<script type="text/javascript" src="assets/user_dashboard/js/jquery.fancybox.pack.js"></script> 







 <script type="text/javascript">

    $(document).ready(function() {

        $(".fancybox").fancybox();

    });

</script>



<aside class="right-side" id="contents">

    <!-- Content Header (Page header) -->

    <section class="content-header no-margin">

        <h1 class="text-center">

        <i class="fa fa-picture-o" aria-hidden="true"></i> All Photos

        </h1>

    </section>



    <!-- Main content -->

    <section class="content photo-list">

	

	



        <div class="photo-listMain">

            <div class="buttondiv clearfix text-center">

                <a href="#" class="bttn opn_mdl"><i class="fa fa-plus"></i> Add Photo</a>

                <a href="photos_favourite" class="bttn"><i class="fa fa-heart-o"></i> Favorite Photo</a>

                

                 <button  type="button" id="btn_delete" disabled><i class="fa fa-trash-o"></i> Delete Photo </button>

                <!-- <a href="#" class="bttn" id="btn_delete"><i class="fa fa-trash-o"></i> Photo Delete</a> -->

                

            </div>

			

			

			       <?php if($this->session->flashdata('susccess')){ ?>

			         

               <div class="alert alert-success">

                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>



         <?php echo $this->session->flashdata('susccess'); ?>

          </div>



            <?php } ?>

			

            <div id="deletemsg" style="text-align: center;"></div>

            <div class="row tz-gallery"> <!--photos row start-->

			

                <?php 

                if(!empty($user_photo_data)){

                foreach ($user_photo_data as $key => $value) {

                  $fav = $this->common_model->getSingleRecordById('cp_photo_favourite',array('photo_id'=> $value['u_photo_id']));?>

                   <div class="col-md-3">

                    <div class="sc_photo_list_item ">

                        <div class="sc_photo_list_item_photo">

                          <!--<a href="<?php echo base_url().'uploads/photos/'.$value['u_photo']; ?>" class="lightbox">

						  </a>-->

                            <img class="wp-post-image loading" alt="" src="<?php echo base_url().'uploads/photos/'.$value['u_photo']; ?>">

                         

						 <!-- <img class="lazy wp-post-image" src="<?php //echo base_url(); ?>assets/images/lazy.png" alt="P51 Mustang fighter" class="lazy article_img_320 img-responsive" data-original="<?php //echo base_url().'uploads/photos/'.$value['u_photo']; ?>"> -->

                            <label class="checkbox_con">

                                <input type="checkbox" class="photo_check_box" value="<?php echo $value['u_photo_id'];?>">

                                <span class="checkmark"></span>

                            </label>

                            <div class="red box <?php echo $value['u_photo_id'];?>">

                                <div class="sc_team_item_hover">

                                    <div class="sc_team_item_socials" style="display: inline-block;">

                                        <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">

                                            



                                  <div class="sc_socials_item">



                              <?php 

                              $u_photo_id= $value['u_photo_id'];

                              $photo_id =$fav['photo_id']; 

                              ?>



  

                                          <?php if(empty($fav['photo_id'])){ ?>



                                            <a href="#" class="social_icons social_facebook btn_favourite" id="btn_favourite" onclick="add_favrt('<?php echo $u_photo_id;?>','<?php echo $photo_id; ?>')" title="Favourite">





                                              <i id="need_fvt<?php echo $u_photo_id;?>" class="fa fa-heart-o"></i>





                                            </a>

                                              <?php }else{ ?>





                                       <a href="#" class="social_icons social_facebook btn_favourite" id="btn_favourite" onclick="add_favrt('<?php echo $u_photo_id;?>','<?php echo $photo_id; ?>')" title="Unfavourite">         



                                      <i id="need_unfvt<?php echo $fav['photo_id'];?>" class="fa fa-heart dipH" style="display: block;"></i><?php } ?></a>



                                            </div>

											<div class="sc_socials_item">

                                                <a href="<?php echo base_url().'uploads/photos/'.$value['u_photo']; ?>" class="social_icons social_twitter lightbox" id="btnnew_delete"><i class="fa fa-eye"></i></a>

                                            </div>

                                            <div class="sc_socials_item">

                                                <!-- <a href="#" class="social_icons social_twitter btnnew_delete" id="btnnew_delete"><i class="fa fa-trash-o"></i></a> -->

                                            </div>

											

                                            

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                      

                    </div>

                </div>

                <?php }}else{

                 echo "<center>Photos Not Found</center>";

               } ?>

            </div>

        </div>

    </section>

</aside>

<div class="modal fade deleteModal" id="deleteModal" role="dialog">

            <div class="modal-dialog modal-sm">

                <div class="modal-content">

                  <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    <h4 class="modal-title">Delete</h4>

                  </div>

                  <div class="modal-body">

                    <h3>Are you sure ? You want to delete this</h3>

                  </div>

                  <div class="modal-footer">

                    <button type="button" class="btn btn-primary" id="ok" data-dismiss="modal">Ok</button>

                    <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>

                  </div>

                </div>

            </div>

</div>

<div class="modal fade" id="photo-upload" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title"><i class="fa fa-picture-o   "></i> Add New Photo</h4>

            </div>

            <form method="post" action="uploading_image" id="user_images" enctype="multipart/form-data">

                <div class="modal-body">

                    <div class="">

					<label style="color:red">Note :  upload image only 300px * 300px aprox dimension of  images for better result</label>

                        <div class="clearfix mrT2 pd2 uploader">

                            

                            <div class="banpic"><img src="assets/user_dashboard/img/default.png" alt="banner-image"></div>





                            <div class="img-upload">

                                <label for="imgInp" class="img-upload__label"><i class="fa fa-cloud-upload"></i> Upload Photo</label>

                                <input class="img-upload__input" type="file" 

                                name="imgInp" id="imgInp" >

                            </div>

                            <button type="submit" class="btn btn-primary pull-right" id="uploading_image"><i class="fa fa-refresh"></i> Update</button>

                        </div>

                    </div>

                </div>                     

            </form>

        </div><!-- /.modal-content -->

</div><!-- /.modal-dialog -->

<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

<!-- <script src="assets/user_dashboard/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script> -->



<script src="assets/admin/js/jquery.validate.js"></script>



<!-- script added by 95 for add or remove css calss to show enable or disable delete photo button start -->

<script type="text/javascript">



    $(document).ready(function(){

        $('input[type="checkbox"]').click(function(){

            if($(this).prop("checked") == true){

              $("#btn_delete").addClass("enbl_btn");

            }

            else if($(this).prop("checked") == false){

               var check_count = $('input:checkbox:checked').length;

               if(check_count==0){

                 $("#btn_delete").removeClass("enbl_btn");

              }

               

            }

        });

    });

</script>

<!-- script added by 95 for add or remove css calss to show enable or disable delete photo button end -->







<!-- Script added by 95 for enable or disable delete photo button start-->

<script type="text/javascript">

  var checkboxes = $("input[type='checkbox']")

    submitButt = $("#btn_delete");

checkboxes.click(function() {

    submitButt.attr("disabled", !checkboxes.is(":checked"));

});

</script>

<!-- Script added by 95 for enable or disable delete photo button end-->







<!-- Script added by 95 for add photos in fvrt section start-->

<script type="text/javascript">

  function add_favrt(id,fav_flag){

      // if(fav_flag){

      //   var msg="Are you sure you want unfavourite ?";

      // }else{

      //   var msg="Are you sure you want favourite ?";

      // }

    // if(confirm(msg))

    //   {

       $.ajax({

        url:"<?php echo base_url().'add_Favourite_Photo'?>",

        method:"POST",

        data:{id:id},

        success:function(data)

        { 



            $("#loading").hide();  

            //$("#deletemsg").html("Photo add favourite or unfavourite successfully");

      if(data ==1){

        //var msg = 'Successfully Favourite';

        

      }else if(data ==0){

        //var msg = 'Successfully Unfavourite';  

      }

            setTimeout(function(){

        

                //$("#deletemsg").html('<div class="alert alert-success">'+msg+'</div>');



                if(fav_flag){





                  $("#need_unfvt"+fav_flag).removeClass("fa-heart");

                  $("#need_unfvt"+fav_flag).addClass("fa-heart-o");

                  

                  $("#deletemsg").html('<div class="alert alert-success">'+'Successfully Unfavourite'+'</div>');

                setTimeout(function(){

                      $("#deletemsg").html("");

                     }, 1500);



                }else{



                  $("#need_fvt"+id).removeClass("fa-heart-o");

                  $("#need_fvt"+id).addClass("fa-heart");

                  

                  $("#deletemsg").html('<div class="alert alert-success">'+'Successfully Favourite'+'</div>');

                setTimeout(function(){

                      $("#deletemsg").html("");

                     }, 1500);  

                }





                //window.location.reload(true);

            }, 1000);

        }

       })

      //}

  }









 //  $(document).ready(function(){

 //    $('.btn_favourite').click(function(){

 //        var id = [];

 //       $(':checkbox:checked').each(function(i){

 //        id[i] = $(this).val();

 //      });

 //      if(confirm("Are you sure you want to add favourite or unfavourite image?"))

 //      {

 //       $.ajax({

 //        url:"<?php //echo base_url().'addphotos_favourite'?>",

 //        method:"POST",

 //        data:{id:id},

 //        success:function(data)

 //        { 

 //            $("#loading").hide();  

 //            //$("#deletemsg").html("Photo add favourite or unfavourite successfully");

 //      if(data = 'success'){

 //        var msg = 'Successfully Favourite';

        

 //      }else{

 //        var msg = 'Successfully Unfavourite';

 //      }

 //            setTimeout(function(){

        

 //                $("#deletemsg").html('<div class="alert alert-success">'+msg+'</div>');

 //                window.location.reload(true);

 //            }, 1000);

 //        }

 //       })

 //      }

 //      else

 //      {

 //       return false;

 //      }

 //    });

 // });

</script>

<!-- Script added by 95 for add photos in fvrt section end-->





<script type="text/javascript">

    $(document).ready(function(){

    $('input[type="checkbox"]').click(function(){

    var inputValue = $(this).attr("value");

    $("." + inputValue).toggle();

    });

    });

</script>

<style type="text/css">

    label.checkbox_con{

    position: absolute !important;

    top: 0px;

    }

    .box{

    display:none;

    }

    .red{

    padding: 0px;

    margin: 0px;

    background: rgba(0, 0, 0, 0.6);

    position: absolute;

    left: 0;

    right: 0;

    top: 0;

    bottom: 0;

    filter: alpha(opacity=0);

    text-align: center;

    -webkit-transition: all ease .3s;

    -moz-transition: all ease .3s;

    -ms-transition: all ease .3s;

    -o-transition: all ease .3s;

    transition: all ease .3s;

    height: auto;

    border:0px;

    }

    .sc_photo_list_item:hover .sc_photo_list_item_photo {

    /*background: #000;*/

    left: 0;

    right: 0;

    top: 0;

    bottom: 0;

    filter: alpha(opacity=0);

    text-align: center;

    -webkit-transition: all ease .3s;

    -moz-transition: all ease .3s;

    -ms-transition: all ease .3s;

    -o-transition: all ease .3s;

    transition: all ease .3s;

    height: auto;

    }

    .sc_photo_list_item:hover img {

    opacity: 0.3;

    }



</style>

<script>

    //js for photo upload model start

    $(document).ready(function(){

    $(".opn_mdl").click(function(){

    $("#photo-upload").modal();

    });

    });

    //js for photo upload model end

    //js for photo upload start

    function readURL(input) {

    if (input.files && input.files[0]) {

    var reader = new FileReader();

    reader.onload = function (e) {

    $('.banpic img').attr('src', e.target.result);

    }

    reader.readAsDataURL(input.files[0]);

    }

    }

    $("#imgInp").change(function(){

    readURL(this);

    });

//js for photo upload start                

</script>  

  <script type="text/javascript">

        $(function() {

          $("#user_images").validate({

            // Specify validation rules

            rules: {

              imgInp: {

                required: true,

                //email: true

              }

            },

            // Specify validation error messages

            messages: {

              //email: "Enter your email"

              imgInp: {

                required: "<font style='font-weight:normal;color:red;'>Please select image</font>",

              },

            },

            // Make sure the form is submitted to the destination defined

            // in the "action" attribute of the form when valid

            submitHandler: function(form) {

              form.submit();

            }

          });

        });

    </script>

<script type="text/javascript">

 // $(document).ready(function(){  

 //    $('#btn_delete1').click(function(){

 //          var email_url =  "<?php echo base_url()?>";

 //          if($("#deleteModal").modal('show'))

 //          {

 //           var id = [];

           

 //           $(':checkbox:checked').each(function(i){

 //            id[i] = $(this).val();

 //           });

 //           if(id.length === 0) //tell you if the array is empty

 //           {

 //            alert("Please Select atleast one checkbox");

 //           }

 //           else

 //           {

 //            $.ajax({

 //             url:'<?php echo base_url().'delete_photos'?>',

 //             method:'POST',

 //             data:{id:id},

 //             success:function()

 //             {

 //              for(var i=0; i<id.length; i++)

 //              {

 //                $("#deletemsg").html("Photos delete successfully");

 //                setTimeout(function(){

 //                  $("#deletemsg").html("Photos delete successfully");

 //                    window.location.href = email_url+"user_photos";

 //                }, 1000);

 //              }

 //             }

             

 //            });

 //           }

           

 //          }

 //          else

 //          {

 //           return false;

 //          }

 //    });

 // });

 



 // $(document).ready(function(){

 //    $('.btn_favourite').click(function(){

 //        var id = [];

 //       $(':checkbox:checked').each(function(i){

 //        id[i] = $(this).val();

 //      });

 //      if(confirm("Are you sure you want to add favourite or unfavourite image?"))

 //      {

 //       $.ajax({

 //        url:"<?php //echo base_url().'addphotos_favourite'?>",

 //        method:"POST",

 //        data:{id:id},

 //        success:function(data)

 //        { 

 //            $("#loading").hide();  

 //            //$("#deletemsg").html("Photo add favourite or unfavourite successfully");

	// 		if(data = 'success'){

	// 			var msg = 'Successfully Favourite';

				

	// 		}else{

	// 			var msg = 'Successfully Unfavourite';

	// 		}

 //            setTimeout(function(){

				

 //                $("#deletemsg").html('<div class="alert alert-success">'+msg+'</div>');

 //                window.location.reload(true);

 //            }, 1000);

 //        }

 //       })

 //      }

 //      else

 //      {

 //       return false;

 //      }

 //    });

 // });

 $(document).on('click', '#btn_delete', function(){

  var id = [];

   $(':checkbox:checked').each(function(i){

    id[i] = $(this).val();

  });

  if(confirm("Are you sure you want to delete photo ?"))

  {

   $.ajax({

    url:"<?php echo base_url().'delete_photos'?>",

    method:"POST",

    data:{id:id},

    success:function(data)

    {

        $("#loading").hide();  

        $("#deletemsg").html("Photos delete successfully");

        setTimeout(function(){

                  $("#deletemsg").html("");

                    window.location.reload(true);

                }, 1000);

        

    }

   })

  }

  else

  {

   return false;

  }

 });

 $(document).on('click', '.btnnew_delete', function(){

  var id = [];

   $(':checkbox:checked').each(function(i){

    id[i] = $(this).val();

  });

  if(confirm("Are you sure you want to delete photo ?"))

  {

   $.ajax({

    url:"<?php echo base_url().'delete_photos'?>",

    method:"POST",

    data:{id:id},

    success:function(data)

    {

        $("#loading").hide();  

        $("#deletemsg").html("Photo delete successfully");

        setTimeout(function(){

                  $("#deletemsg").html("");

                    window.location.reload(true);

                }, 1000); 

    }

   })

  }

  else

  {

   return false;

  }

 });

</script>



<script src="assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>





                