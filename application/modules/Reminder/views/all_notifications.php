<aside class="right-side">    
    <section class="content-header no-margin">
        <h1 class="text-center">
        <i class="fa fa-bell-o" >
        </i> 
            All Notifications
            <!-- <div class="rem_add"><a href="<?php echo base_url(); ?>add_reminder" class="btn btn-primary" >Add</a></div> -->
        </h1>
    </section> 
    
    <section class="content photo-list">
	    <div class="photo-listMain reminder_listMain">
            <div class="row swt_cor">
                <div class="col-sm-6">
                    <label style="margin-right: 10px;">
                        <input type="checkbox" id="checkall">
                    </label>
                   <div class="delete-email">
                    <a href="javascript:void(0);" name="reminder_all_del" id="reminder_all_del" data-base="<?php echo base_url(); ?>" class="reminder_all_del"><i aria-hidden="true" class="fa fa-trash-o"></i></a>
                    <!-- <a href="#" name="btn_delete" id="btn_delete" class="btn_delete"><i aria-hidden="true" class="fa fa-trash-o"></i></a> -->
                    </div>
                </div>                    
            </div>
           <div class="table-responsive tb_swt">
            <div id="expire_msg"></div>
	              <?php if ($this->session->flashdata('success')) { ?>
                    <div class="alert alert-success message">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <?php echo $this->session->flashdata('success'); ?></div>
                <?php } ?> 
				
                <table class="table table-hover table-bordered table-mailbox send-user-mail" id="sampleTable">        
                    <thead>
                        <tr>
        			    <th>#</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Date</th>
                        <th>Time</th>				
                    </tr>
                </thead>
                <tbody>
                <?php 
                $i = 1;
            //echo "<pre>"; print_r($user_data);
    		//if(!empty($reminder_list)){
                //foreach ($reminder_list as $list) {?>
                    <tr>
                    <td class="small-col" ><input type="checkbox" name="reminder_chk[]"  class="reminder_chk" value="<?php echo $list->reminder_id; ?>" data-id="<?php echo $list->reminder_id; ?>"/></td>
                     <td><?php //echo $list->reminder_title; ?></td>
                     <td><?php //echo $list->reminder_date; ?></td>
                     <td><?php //echo $list->reminder_time; ?></td> 
    				 <td>
    			         <?php /* ?>
                         <a href="<?php echo base_url(); ?>edit_reminder/<?php echo $list->reminder_id; ?>" class="edit_btn">Edit</a>
    					<a href="<?php echo base_url(); ?>delete_reminder/<?php echo $list->reminder_id; ?>" onclick="return confirm('Are you sure delete this record?')" class="edit_btn">Delete</a>
                        <?php */?>
    				 </td>
                 </tr>
    			<?php //}} ?>  
         </tbody>
                </table>
            </div>              
        </div>
    </section>
</aside>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>
               