 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
 <link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightslider.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightslider.js"></script>
 <div class="widget-header">
			    	<h2>My Music</h2>
			    	<div class="wid-header-rhs">
			    		<a href="#"> View all </a>
			    	</div>
			    </div>

			    <ul id="content-slider" class="content-slider">
					<?php foreach ($check_music as $music) { ?>                                
                <li>
                   <div class="rail-thumb">
			    			
                   			<a class="dropdown-item play_now_<?php echo $music->music_id; ?>" href="javascript:void(0);" id="play_now_<?php echo $music->music_id; ?>" data-music="<?php echo $music->music_id; ?>"><img src="<?php echo base_url().'uploads/music/image/'.$music->music_image; ?>"></a>
                   			   
			    			<span class="playbutton">
			    				<i class="fa fa-play-circle"></i>
			    			</span>			    			
			    		</div>
			    		<div class="rail-content" title="">
			    			<a href="#"> <?php echo $music->music_title;  ?> </a>
		    				<div class="btn-group new-music-btn">
									<a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
								  <div class="dropdown-menu">
									
									<a class="dropdown-item play_now_<?php echo $music->music_id; ?>" href="javascript:void(0);" id="play_now_<?php echo $music->music_id; ?>" data-music="<?php echo $music->music_id; ?>">Play Now</a>
									
									<a class="dropdown-item download_music_<?php echo $music->music_id; ?>" href="<?php echo base_url(); ?>Pages/Music/download_music/<?php echo $music->music_id; ?>" id="download_music_<?php echo $music->music_id; ?>" data-music-id="<?php echo $recent->music_id; ?>">Download</a>
																		<a class="dropdown-item" href="javascript:void(0);" id="remove_mymusic_<?php echo $music->music_id; ?>" data-mymusic="<?php echo $music->music_id; ?>" >Remove</a>
																		
<a class="dropdown-item" href="<?php echo base_url(); ?>edit_music/<?php echo base64_encode($music->music_id); ?>">Edit</a>																		
									<a class="dropdown-item" href="#">Get info</a>
								  </div>
							</div>			    			
			    		</div>

                </li>

                <?php } ?>
            </ul>


			<script>
			    $(document).ready(function() {
			$(".content-slider").lightSlider({
                loop:true,
                keyPress:true
            });
            $('#image-gallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:9,
                slideMargin: 0,    
                speed:500,
                auto:true,
                loop:true,
                onSliderLoad: function() {
                    $('#image-gallery').removeClass('cS-hidden');
                }  
            });
		});
		
		
		(function($){
			$(window).on("load",function(){
				
				$("a[rel='load-content']").click(function(e){
					e.preventDefault();
					var url=$(this).attr("href");
					$.get(url,function(data){
						$(".content .mCSB_container").append(data); //load new content inside .mCSB_container
						//scroll-to appended content 
						$(".content").mCustomScrollbar("scrollTo","h2:last");
					});
				});
				
				$(".content").delegate("a[href='top']","click",function(e){
					e.preventDefault();
					$(".content").mCustomScrollbar("scrollTo",$(this).attr("href"));
				});
			});
		})(jQuery);
			</script>