<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * CSV Helpers
 * Inspiration from PHP Cookbook by David Sklar and Adam Trachtenberg
 * 
 * @author		Jérôme Jaglale
 * @link		http://maestric.com/en/doc/php/codeigniter_csv
 */

// ------------------------------------------------------------------------

/**
 * Array to CSV
 *
 * download == "" -> return CSV string
 * download == "toto.csv" -> download file toto.csv
 */
if ( ! function_exists('array_to_csv'))
{
	function array_to_csv($array, $download = "")
	{
		if ($download != "")
		{	
			header('Content-Type: application/csv');
			header('Content-Disposition: attachement; filename="' . $download . '"');
		}		

		ob_start();
		$f = fopen('php://output', 'w') or show_error("Can't open php://output");
		$n = 0;		
		foreach ($array as $line)
		{
			$n++;
			if ( ! fputcsv($f, $line))
			{
				show_error("Can't write line $n: $line");
			}
		}
		fclose($f) or show_error("Can't close php://output");
		$str = ob_get_contents();
		ob_end_clean();

		if ($download == "")
		{
			return $str;	
		}
		else
		{	
			echo $str;
		}		
	}
}

// ------------------------------------------------------------------------

/**
 * Query to CSV
 *
 * download == "" -> return CSV string
 * download == "toto.csv" -> download file toto.csv
 */
if ( ! function_exists('query_to_csv'))
{
	function query_to_csv($query, $headers = TRUE, $download = "")
	{
		if ( ! is_object($query) OR ! method_exists($query, 'list_fields'))
		{
			show_error('invalid query');
		}
		
		$array = array();
		
		if ($headers)
		{
			$line = array();
			foreach ($query->list_fields() as $name)
			{
				$line[] = $name;
			}
			$array[] = $line;
		}
		
		foreach ($query->result_array() as $row)
		{
			$line = array();
			foreach ($row as $item)
			{
				$line[] = $item;
			}
			$array[] = $line;
		}

		echo array_to_csv($array, $download);
	}
}



/**
 *  For profile pic */
 
if ( ! function_exists('profile_pic'))
{
	function profile_pic($img_name,$category_id = NULL)
	{
        if(!empty($category_id) && $img_name=='default/default.jpg'){
			$temp_cate = explode(",",$category_id);
			$cateimg   =  get_img($temp_cate);
			if($cateimg=='default.png'){
			  	$src = base_url("assets/profile_pics/default/default.jpg");		 
			}else{
			  	$src = base_url("assets/img/category/".$cateimg);				  
			}
		}else{
			$src = base_url("assets/profile_pics/".$img_name);
		}
        echo '<img src="'.$src.'">';
	}
}
if ( ! function_exists('get_img'))
{
	function get_img($images_id){
		 $img = '';
		 $ci=& get_instance(); 
		 for($i=0;$i<count($images_id);$i++){
			 
			 $result = $ci->db->get_where("otd_business_category",array("cat_id"=>$images_id[$i]))->row_array();
			  
			  
			 if($result['cat_img_path'] != 'default.png'){
				 $img = $result['cat_img_path'];
				   
				 break;
				 
			 }else{
				 $j = $i+1;
				 if($j == count($images_id)){
					     
						$img = $result['cat_img_path'];
						break;
					 
				 }
			 }
		 }
		// echo $img;
		  return $img;		 
		 
	 }
	
}
/* End of file csv_helper.php */
/* Location: ./system/helpers/csv_helper.php */