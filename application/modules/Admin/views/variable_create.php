<?php 
	//_dx($variable_data);
if(!empty($variable_data) && $variable_data !=''){
?>
	<form id="variable_edit" enctype="multipart/form-data" method="POST" autocomplete="off">
		<input type="hidden" name="vm_id" value="<?php echo $variable_data->vm_id; ?>">
		<div class="row">
			<div class="col-sm-6 col-md-6">
				<div class="form-group">
					<label>Variable name<font style="color:red;">*</font>:</label>
					<input type="text" class="form-control" id="vm_name" name="vm_name"  readonly value="<?php echo $variable_data->vm_name; ?>" placeholder="Enter variable name">
					<?php echo form_error('vm_name'); ?>
				</div>
			</div>

			<div class="col-md-8">
				<label>Variable originl value<font style="color:red;">*</font>:</label>
				<textarea name="vm_value" id="vm_value" class="form-control" placeholder="Doctor Address"><?php echo $variable_data->vm_value; ?></textarea>
			</div>
			
			<div class="col-md-8">
				<label>Variable temp value<font style="color:red;">*</font>:</label>
				<textarea name="vm_value_temp" id="vm_value_temp" class="form-control" placeholder="Doctor Address"><?php echo $variable_data->vm_value_temp; ?></textarea>
			</div>
			
			<!-- Rounded switch -->
			<div class="col-md-8">
				<label class="switch">Variable status<font style="color:red;">*</font>:</label>
				<?php if($variable_data->vm_staus == 1 && $variable_data->vm_staus !=''){ ?>
					<input type="checkbox" name="vm_staus" value="0" checked>
				<?php }else{ ?>
					<input type="checkbox" name="vm_staus" value="1">
				<?php } ?>
			</div>
		</div>

		<div class="box-footer">
			<p align="right">
			<input type="submit" name="submit" class="btn btn-primary" value="update">
			<button type="button" class="btn btn-primary" id="back">Cancel</button>
			</p>
		</div>
	</form>
<?php }else{ ?>

	<form id="variable_add" enctype="multipart/form-data" method="POST" autocomplete="off">
		<input type="hidden" name="vm_id" value="">
		
			<div class="form-group row">
				<label for="vm_name" class="col-sm-2 col-form-label">Variable name<font style="color:red;">*</font>:</label>
				<div class="col-sm-5">
					<input type="text" class="form-control" id="vm_name" name="vm_name"  placeholder="Enter variable name" oninput="this.value = this.value.toUpperCase()">
					<?php echo form_error('vm_name'); ?>
				</div>
				<button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="i.e is TXT_SITE_WEL_MSG">Tips</button>
			</div>

			<div class="form-group">
				<label for="vm_value">Variable originl value</label>
				<textarea class="form-control col-md-8" name="vm_value" id="vm_value" rows="5"></textarea>
				<?php echo form_error('vm_value'); ?>
			</div>

			<div class="form-group">
				<label for="vm_value_temp">Variable temp. value</label>
				<textarea class="form-control col-md-8" name="vm_value_temp" id="vm_value_temp" rows="5"></textarea>
				<?php echo form_error('vm_value_temp'); ?>
			</div>

		<div class="box-footer">
			<p align="right">
				<input type="submit" name="submit" class="btn btn-primary" value="submit">
				<button type="button" class="btn btn-primary" id="back">Cancel</button>
			</p>
		</div>
	</form>


<?php } ?>

<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.validate.js"></script>

<?php 
	if(!empty($variable_data) && $variable_data !=''){
?>
<script>

	$('#back').click(function() {
		var url = '<?php echo base_url() . "admin/variable_master" ?>';
		window.location.href = url;
		return false;
	});

	$("#variable_edit").validate({
		rules: {
			/*vm_name:{
				required: true,
				remote: {
					url: '<?php base_url(); ?>'+"Admin/Admin_user/validate_variableData",
					type: "post",
				},
			},*/
		},
		messages: {
			/*vm_name:{
			required: 'Please enter variable name.',
				remote: 'Variable already used.',
			}*/
		},
		
	});

</script>
<?php }else{ ?>
	<script>

	$('#back').click(function() {
		var url = '<?php echo base_url() . "admin/variable_master" ?>';
		window.location.href = url;
		return false;
	});

	$("#variable_add").validate({
		rules: {
			vm_name:{
				required: true,
				remote: {
					url: '<?php base_url(); ?>'+"Admin/Admin_user/validate_variableData",
					type: "post",
				},
			},
		},
		messages: {
			vm_name:{
			required: 'Please enter variable name.',
				remote: 'Variable already used.',
			}
		},
		
	});
	</script>
<?php } ?>
