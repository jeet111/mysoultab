<aside class="right-side">    

    <section class="content-header no-margin">

        <h1 class="text-center"><i class="fa fa-pencil-square-o"></i> Edit Remainder</h1>

    </section>    

    <section class="content photo-list">

        <div class="photo-listMain">

            <form role="form" class="frm_add">

                <div class="row"> 

                    <div class="col-md-6">

                        <div class="form-group">

                            <label for="">Name</label>

                            <input type="text" class="form-control" id="" placeholder="Name">

                        </div>

                    </div>

                    <div class="col-md-6">

                        <div class="form-group">

                            <label for="">Date</label>

                            <div class="input-group date" data-provide="datepicker">

                                <input type="text" class="form-control">

                                <div class="input-group-addon">

                                    <span class="glyphicon glyphicon-calendar"></span>

                                </div>

                            </div>

                        </div>



                    </div>

                </div>

                <div class="row"> 

                    <div class="col-md-6">

                        <div class="bootstrap-timepicker">

                            <div class="form-group">

                                <label>Time:</label>

                                <div class="input-group">

                                    <input type="text" class="form-control timepicker"/>

                                    <div class="input-group-addon">

                                        <i class="fa fa-clock-o"></i>

                                    </div>

                                </div>

                            </div>

                        </div>             

                    </div>

                    <div class="col-md-6">

                        <div class="form-group">

                            <label for="">Remainder Before</label>

                            <input type="text" class="form-control" id="" placeholder="Remainder Before">

                        </div>

                    </div>

                </div>

                <div class="row">

                     <div class="col-md-6">

                        <div class="form-group">

                            <label for="">Repeat</label>

                            <select class="selectpicker form-control">

                              <option>Everyday</option>

                              <option>Every Week</option>

                              <option>Month</option>

                              <option>Year</option>                                      

                            </select>

                        </div>

                    </div>

                </div>

                <div class="box-footer">

                    <button type="submit" class="btn btn-primary">Submit</button>

                </div>

            </form>              

        </div>

    </section>

</aside>















<script type="text/javascript">

   $('.datepicker').datepicker({

    format: 'mm/dd/yyyy',

    startDate: '-3d'

    });

    $(function() {

    $('#datetimepicker3').datetimepicker({

      pickDate: false

    });

  });

</script>







<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>



<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>



    



                