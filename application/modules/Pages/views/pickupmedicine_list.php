<style>
.text_heading {
    border-bottom: none;
    margin-bottom: 20px;
}
</style>
<div class=" text_heading">
  <div id="msg_show"></div>
  <h3><i class="fa fa-list"></i> Pickup Medicine List <div class="rem_add">
  </h3>



  <div class="tooltip-2">
    <h2>Display Transportation button on Mobile App

      <?php
      if (is_array($btnsetingArray)) {

        foreach ($btnsetingArray as $res) {
          if ($res->settings == 1) { ?>
            <label class="switch ">
              <input type="checkbox" checked data-btn="<?php echo 'Medicine Schedule' ?>" class="updateStatus">
              <span class="slider round"></span>
            </label>

          <?php } else { ?>
            <label class="switch space">
              <input type="checkbox" data-btn="<?php echo 'Medicine Schedule' ?>" class="updateStatus">
              <span class="slider round"></span>
            </label>


          <?php } ?>
      <?php }
      }
      ?>
    </h2>
  </div>
  <div class="rem_add">
    <a href="<?php echo base_url(); ?>add_pickup_medicine" class="btn btn-primary">Add</a>
  </div>

</div>




<section class="content photo-list">

  <div class="photo-listMain reminder_listMain">

    <div class="row swt_cor">

      <div class="col-sm-12">

        <label style="margin-right: 10px;">

          <input type="checkbox" id="checkall">

        </label>

        <div class="delete-email">

          <a href="javascript:void(0);" name="btn_delete" id="btn_delete" class="btn_delete"><i aria-hidden="true" class="fa fa-trash-o"></i></a>

        </div>

      </div>

    </div>



    <div class="table-responsive tb_swt">

      <div id="expire_msg"></div>

      <table class="table table-hover table-bordered table-mailbox send-user-mail tab_medi" id="sampleTable">

        <thead>

          <tr>

            <th>#</th>

            <!-- <th>Doctor Name</th> -->

            <th>User</th>

            <th>Medicine Name</th>

            <th>Action</th>

          </tr>

        </thead>

        <tbody>

          <?php

          if (!empty($pickupmedicine_list)) {

            foreach ($pickupmedicine_list as $list) {

              $doctormail = $this->Common_model_new->getsingle("cp_doctor", array("doctor_id" => $list->doctor_id));



              $caregiver_data = $this->Common_model_new->getsingle("cp_users", array("id" => $list->caregiver_id));



              $medicine = $this->Common_model_new->getsingle("medicine_schedule", array("medicine_id" => $list->medicine_id));



          ?>

              <tr>

                <td class="small-col">

                  <input type="checkbox" name="sub_chk[]" class="sub_chk" value="<?php echo $list->id; ?>" />

                </td>



                <?php /* ?><td><?php echo $doctormail->doctor_name; ?></td><?php */ ?>

                <td><?php echo $caregiver_data->name; ?></td>

                <td><?php echo $medicine->medicine_name; ?></td>



                <td>

                  <a href="<?php echo base_url(); ?>delete_pickup/<?php echo $list->id; ?>" onclick="return confirm('Are you sure, you want to remove this pickup medicine ?')" class="edit_btn">Delete</a>

                </td>

              </tr>



          <?php }
          } ?>



        </tbody>

      </table>

    </div>

  </div>

</section>

</aside>



<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>



<script>
  $(document).on('click', '.updateStatus', function() {
    var update_status = $(this).attr('data-btn');
    var update_st = $(this).attr('data-st');
    //alert(update_status+'-'+update_st);
    $.ajax({
      url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
      type: "POST",
      data: {
        'btn': update_status
      },
      dataType: "json",
      success: function(data) {
        console.log(data);
        $("#msg_show").html("<div class='alert alert-success'>" + data.status + "</div>");

      }
    });
  });







  jQuery('#checkall').on('click', function(e) {

    if ($(this).is(':checked', true))

    {

      $(".sub_chk").prop('checked', true);

    } else

    {

      $(".sub_chk").prop('checked', false);

    }

  });

  jQuery('.sub_chk').on('click', function() {

    if ($('.sub_chk:checked').length == $('.sub_chk').length) {

      $('#checkall').prop('checked', true);

    } else {

      $('#checkall').prop('checked', false);

    }

  });





  $(document).on('click', '.btn_delete', function(e) {





    var bodytype = $('input[name="sub_chk[]"]:checked').map(function() {

      return this.value;

    }).get().join(",");



    if (bodytype != '') {

      if (confirm("Are you sure, you want to remove this pickup medicine ?")) {

        $.ajax({

          type: "POST",

          url: '<?php echo base_url(); ?>Pages/Medicine/delete_all_pickup_med',

          data: {
            bodytype: bodytype
          },

          //dataType: "json",

          success: function(data) { //alert('swati');





            setTimeout(function() {

              $("#expire_msg").html("<div class='alert alert-success message'>Successfully deleted !</div>");

              location.reload();

              //window.location.href = email_url+"email_list";

            }, 1300);

          }

        });



      }

    } else {

      alert("Please select atleast one pickup medicine.");

    }

  });
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		setTimeout(function() {
		$('#success').fadeOut('fast');
		}, 2500);
	});

	 $(document).on('click', '.updateStatus', function() {
        var update_status = $(this).attr('data-btn');
        var update_st = $(this).attr('data-st');
        //alert(update_status+'-'+update_st);
        $.ajax({
            url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
            type: "POST",
            data: {
                'btn': update_status
            },
            dataType: "json",
            success: function(data) {
                    $("#msg_show").html("<div class='alert alert-success'>" + data.status + "</div>");
                   
            }
        });
    });
</script>