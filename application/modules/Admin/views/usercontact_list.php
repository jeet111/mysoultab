<div class="container-fluid">
	<div class="dash-counter users-main my-sts-table">
		<div class="row">
			<div class="col-md-12">
				<div class="">
					<?php //echo "<pre>"; print_r($ContactData); echo "</pre>";?>
					<div class="card-body">
						<div class="status-cng"></div>
						<?php if ($this->session->flashdata('success')) { ?>
							<div class="alert alert-success message">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<?php echo $this->session->flashdata('success'); ?></div>
							<?php } ?>


							<div id="deletemsg"></div>

							<table class="table table-hover tab_comn" id="sampleTable">
								<thead>
									<tr>
										<th>S no.</th>
										<th>Contact Name</th>
										<th>Contact Number</th>
										<th>User</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i = 1;
									foreach ($ContactData as $Contact) {

										?>
										<tr>
											<td><?php echo $i++; ?></td>
											<td><?php echo $Contact->contact_name; ?></td>
											<td><?php echo $Contact->contact_mobile_number; ?></td>
											<td><?php echo $Contact->username; ?></td>

											<td>
												<div class="link-del-view">

													<div class="tooltip-2">

														<a href="javascript:void(0);" data-id="<?php echo $Contact->contact_id; ?>" id="deletemoviecat_<?php echo $Contact->contact_id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span>



														<!-- <a href="<?php echo base_url(); ?>admin/delete_contacts/<?php echo $Contact->contact_id; ?>" onclick="return confirm('Are you sure, you want to delete this contact ?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Delete</span> -->


													</div>


												</div>

											</div>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>


<script type="text/javascript">
	$(document).on('click','[id^="deletemoviecat_"]',function(){



		var delete_photo = $(this).attr('data-id');
		if(confirm("Are you sure, you want to delete this contact ?")){
			$.ajax({
				type: "POST",
				url: '<?php echo base_url(); ?>Admin/Contacts/deletecontactRecords/',
				data: 'id='+delete_photo,
            //dataType: "json",
            success: function(data)
            {


            	setTimeout(function(){
            		$("#deletemsg").html("<div class='alert alert-success'>Deleted Successfully.</div>");
            		location.reload();
//window.location.href = email_url+"email_list";
}, 1000);

            }
        });
			$(this).parents(".card_one").animate({ backgroundColor: "#fbc7c7" }, "fast")
			.animate({ opacity: "hide" }, "slow");
		}
	});
</script>