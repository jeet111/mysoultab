<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StaffManagement extends MX_Controller {

    private $staff_permission = array();
    private $access_permission = true;
  	public function __construct(){   

    	parent:: __construct();
    	$this->load->library('session');
    	$this->load->library('form_validation');
    	$this->load->model('Common_model');
      $this->load->model('login_model');
    	$this->load->model('StaffModel');
    	$this->load->helper(array('url'));
    	$this->load->helper(array('form'));

    	$user_id = $this->session->userdata('logged_ins')['id'];
    	if(empty($user_id)) {
      	redirect('admin/login');
    	}

      if ($this->session->userdata('logged_ins')['user_role'] == 5) {
        $uid = $this->session->userdata('logged_ins')['id'];
        $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
        if (!empty($permission_list)) 
        {
           foreach ($permission_list as $k => $v) 
           {
              $this->staff_permission[$v['permission_id']] = $v;
           }
        }
      }

  	}
  
    public function permission_access($permission_id=5)
    {
      if ($this->session->userdata('logged_ins')['user_role'] != 1) 
      {
         if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
         {
            $this->access_permission = false;
         }
      }
    }
  
  	public function staff_list()
  	{
  		
  		$data['menuactive'] = $this->uri->segment(2);

  		//$data['staff_list'] = $this->StaffModel->getAllRecordsById('cp_users',array('user_role' => 2));
	 	$staff_list = $this->Common_model->getAllRecordsById('cp_users',array('user_role' => 5));
	 	if (!empty($staff_list)) 
	 	{
	 		foreach ($staff_list as $key => $value) 
	 		{
	 			$permission_list = $this->Common_model->getAllRecordsById('staff_permission',array('user_id'=>$value['id']));
	 			if (!empty($permission_list)) {
	 				foreach ($permission_list as $k => $v) 
	 				{
	 					$staff_list[$key]['permission_id'][$v['permission_id']] = array('view_permission'=>$v['view_permission'],'add_permission'=>$v['add_permission'],'edit_permission'=>$v['edit_permission'],'delete_permission'=>$v['delete_permission']);
	 				}
	 			} else {
	 				$staff_list[$key]['permission_id'] = array();
	 			}
	 		}
	 	}

	 	  //echo "<pre>";print_r($staff_list);die;
	 	  $data['staff_list'] = $staff_list;
	 	  
      $data['staff_permission'] = $this->staff_permission;
    	
      $this->permission_access(5);
      if ($this->access_permission) {
        $this->template->set('title', 'Staff Management');
        $this->template->load('admin_dashboard_layout', 'contents', 'staff_list', $data);
      } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
      }
  	}
  
  
  	public function add_staff()
  	{
  	 	$data['menuactive'] = $this->uri->segment(2);
	 	
	 	$this->form_validation->set_rules('first_name', 'First Name', 'required');
	 	$this->form_validation->set_rules('last_name', 'Last Name', 'required');
	 	$this->form_validation->set_rules('username', 'User Name', 'required');
	 	$this->form_validation->set_rules('email', 'Email', 'required');
	 	$this->form_validation->set_rules('password', 'Password', 'required');
	 	$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required');
	 	$this->form_validation->set_rules('mobile', 'Mobile', 'required');

	 	$first_name = $this->input->post('first_name');
	 	$last_name = $this->input->post('last_name');
	 	$username = $this->input->post('username');
	 	$email = $this->input->post('email');
	 	$password = $this->input->post('password');
	 	$confirm_password = $this->input->post('confirm_password');
	 	$mobile = $this->input->post('mobile');
	 
	  	if ($this->form_validation->run() == TRUE) 
		{  
	      $array = array(
		    	'name' => $first_name,
		    	'lastname' => $last_name,
		    	'username' => $username,
		    	'email' => $email,
		    	'mobile' => $mobile,
		    	'password' => md5($password),
		    	'user_role' => 5,
            'status' => 1,
				'create_user' => date('Y-m-d H:i:s'),
				'update_user' => date('Y-m-d H:i:s'),
		 	);
		 	$this->StaffModel->insertData('cp_users',$array);

         $message = "<h3>Dear ".$first_name." ".$last_name.",</h3><br>
                        <h1 style='text-align: center;'>Welcome!</h1>
                        <table>
                           <thead>
                              <tr>
                                 <th>Email</th>
                                 <th>Password</th>
                                 <th>Url</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <th>".$email."</th>
                                 <th>".$password."</th>
                                 <th>".base_url()."</th>
                              </tr>
                           </tbody>
                        </table>";
         $subject = "Care Pro - Account Credential";
         sendEmail($email, $subject, $message);

		 	$this->session->set_flashdata('success', 'Staff Successfully Added');
		 	redirect('admin/staff_list');
		}
		
    $data['staff_permission'] = $this->staff_permission;
    	
      $this->permission_access(5);
      if ($this->access_permission) {
        $this->template->set('title', 'Staff Management');
        $this->template->load('admin_dashboard_layout', 'contents' , 'add_staff', $data);
      } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
      }
  	}
  
  public function edit_staff()
  {
  		$data['menuactive'] = $this->uri->segment(2);

	   $this->form_validation->set_rules('first_name', 'First Name', 'required');
	   $this->form_validation->set_rules('last_name', 'Last Name', 'required');
	   $this->form_validation->set_rules('username', 'User Name', 'required');
	   $this->form_validation->set_rules('email', 'Email', 'required');
	   $this->form_validation->set_rules('mobile', 'Mobile', 'required');
	 
	   $segment = $this->uri->segment('3');
	   $data['segment'] = $segment;

	   $first_name = $this->input->post('first_name');
	   $last_name = $this->input->post('last_name');
	   $username = $this->input->post('username');
	   $email = $this->input->post('email');
	   $mobile = $this->input->post('mobile');
	  
	   $data['singleData'] = $this->Common_model->getsingle('cp_users',array('id' => $segment));
	 
	  
	   if ($this->form_validation->run() == TRUE) 
		{  
	      $array = array(
			   'name' => $first_name,
		    	'lastname' => $last_name,
		    	'username' => $username,
		    	'email' => $email,
		    	'mobile' => $mobile,
				'update_user' => date('Y-m-d H:i:s'),			
			);
			
			$this->StaffModel->updateData('cp_users',$array,array('id' => $segment));

			$this->session->set_flashdata('success', 'Successfully updated');
			redirect('admin/staff_list'); 
		}
		
    $data['staff_permission'] = $this->staff_permission;
    	
      $this->permission_access(5);
      if ($this->access_permission) {
        $this->template->set('title', 'Staff Management');
        $this->template->load('admin_dashboard_layout', 'contents', 'add_staff', $data);
      } else {
        $this->template->set('title', 'Admin Panel - Staff');
        $this->template->load('admin_dashboard_layout', 'contents', 'no_access', $data);
      }
  }
  
  public function delete_test_type()
  {
	  $segment = $this->uri->segment('3');
	  $this->StaffModel->delete('cp_users',array('id' => $segment));
	   $this->session->set_flashdata('success', 'Successfully deleted');
			 redirect('admin/staff_list');
  }
  
  public function check_email() {
  		$email = $this->input->post('email');
  		$user_info = $this->StaffModel->getSingleRecordById('cp_users',array('email'=>$email));
  		if (empty($user_info['id'])) {
			echo 'true';
  		} else {
  			echo 'false';
  		}
  }

  public function check_username() {
  		$username = $this->input->post('username');
  		$user_info = $this->StaffModel->getSingleRecordById('cp_users',array('username'=>$username));
  		if (empty($user_info['id'])) {
			echo 'true';
  		} else {
  			echo 'false';
  		}
  }

  public function check_mobile() {
  		$mobile = $this->input->post('mobile');
  		$user_info = $this->StaffModel->getSingleRecordById('cp_users',array('mobile'=>$mobile));
  		if (empty($user_info['id'])) {
			echo 'true';
  		} else {
  			echo 'false';
  		}
  }

  	public function submit_permission() {
  		$user_id = $this->input->post('user_id');
  		$staff_permission = $this->input->post('staff_permission');

      $permission_list = $this->StaffModel->getAllRecords('permission_modules');
      if (!empty($permission_list)) {
        foreach ($permission_list as $key1 => $val1) 
        {
          $module_id = $val1['id'];
          //$val1['module_name']
          $user_info = $this->StaffModel->getSingleRecordById('staff_permission',array('user_id'=>$user_id,'permission_id'=>$module_id));
          if (!empty($user_info['id'])) {
            //update
            $post_data = array(
                      'view_permission' => 0,
                      'add_permission' => 0,
                      'edit_permission' => 0,
                      'delete_permission' => 0,
                      'updated_at' => date('Y-m-d H:i:s')
                     ); 
            
            $where_condition = array('user_id' => $user_id,'permission_id' => $module_id);
            $this->StaffModel->updateRecords('staff_permission', $post_data, $where_condition);
          } else {
            //insert
            $view_permission = 0;
            $add_permission = 0;
            $edit_permission = 0;
            $delete_permission = 0;

            $post_data = array(
                      'user_id' => $user_id,
                      'permission_id' => $module_id,
                      'view_permission' => $view_permission,
                      'add_permission' => $add_permission,
                      'edit_permission' => $edit_permission,
                      'delete_permission' => $delete_permission,
                      'status' => 1,
                      'created_at' => date('Y-m-d H:i:s'),
                      'updated_at' => date('Y-m-d H:i:s')
                     );
            $this->StaffModel->addRecords('staff_permission',$post_data);
          }
        }
      }

  		if (!empty($staff_permission)) {
  			for ($i=0; $i < count($staff_permission); $i++) { 
  				$input_arr = explode('_', $staff_permission[$i]);
  				$module_id = $input_arr[0];
  				$permission_id = $input_arr[1];
  				$user_info = $this->StaffModel->getSingleRecordById('staff_permission',array('user_id'=>$user_id,'permission_id'=>$module_id));
  				if (!empty($user_info['id'])) {
  					//update
  					if ($permission_id == 1) {
  						$post_data = array(
  										'view_permission' => 1,
  										'updated_at' => date('Y-m-d H:i:s')
  									 );	
  					} elseif ($permission_id == 2) {
  						$post_data = array(
  										'add_permission' => 1,
  										'updated_at' => date('Y-m-d H:i:s')
  									 );
  					} elseif ($permission_id == 3) {
  						$post_data = array(
  										'edit_permission' => 1,
  										'updated_at' => date('Y-m-d H:i:s')
  									 );
  					} elseif ($permission_id == 4) {
  						$post_data = array(
  										'delete_permission' => 1,
  										'updated_at' => date('Y-m-d H:i:s')
  									 );
  					} 
  					
  					$where_condition = array('user_id' => $user_id,'permission_id' => $module_id);
  					$this->StaffModel->updateRecords('staff_permission', $post_data, $where_condition);
  				} else {
  					//submit
  					$view_permission = 0;
					  $add_permission = 0;
					  $edit_permission = 0;
					  $delete_permission = 0;
  					if ($permission_id == 1) {
  						$view_permission = 1;	
  					} elseif ($permission_id == 2) {
  						$add_permission = 1;
  					} elseif ($permission_id == 3) {
  						$edit_permission = 1;
  					} elseif ($permission_id == 4) {
  						$delete_permission = 1;
  					} 
  					$post_data = array(
  										'user_id' => $user_id,
  										'permission_id' => $module_id,
  										'view_permission' => $view_permission,
  										'add_permission' => $add_permission,
  										'edit_permission' => $edit_permission,
  										'delete_permission' => $delete_permission,
  										'status' => 1,
  										'created_at' => date('Y-m-d H:i:s'),
  										'updated_at' => date('Y-m-d H:i:s')
  									 );
  					$this->StaffModel->addRecords('staff_permission',$post_data);
  				}
  			}
  		}
  		echo 1;
  	}

   public function update_status()
   {
      $user_id = $this->input->post('user_id');
      $user_info = $this->StaffModel->getSingleRecordById('cp_users',array('id'=>$user_id));
      $status = ($user_info['status'] == 1 ? 0 : 1);

      $array = array(
                  'status' => $status,
                  'update_user' => date('Y-m-d H:i:s'),        
               );
      
      $res = $this->StaffModel->updateData('cp_users',$array,array('id' => $user_id));
      if ($res) {
         echo 1;
      } else {
         echo 0;
      }
      
   }
}