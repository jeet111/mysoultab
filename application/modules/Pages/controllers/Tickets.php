<?php defined('BASEPATH') or exit('No direct script access allowed');



class Tickets extends MX_Controller
{



  public function __construct()
  {

    parent::__construct();

    $this->load->library('session');

    $this->load->library('form_validation');

    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

    $this->load->model('testreport_model');

    $this->load->model('Common_model_new');

    $this->load->model('common_model');



    $this->load->helper(array('common_helper'));

    $this->load->helper(array('url'));

    $this->load->helper(array('form'));
  }







  public function ticket_list()

  {



    $data['menuactive'] = $this->uri->segment(1);

    $checkLogin = $this->session->userdata('logged_in');

    if (!empty($checkLogin)) {

      //$data['TestReportData']  = $this->testreport_model->getAllTestReports($this->session->userdata('logged_in')['id']);

      $user_id = $this->session->userdata('logged_in')['id'];

      // die;

      $data['ticketData'] = $this->Common_model_new->getAllwhere("user_tickets", array("user_id" => $this->session->userdata('logged_in')['id']));



      $this->template->set('title', 'Tickets');

      $this->template->load('user_dashboard_layout', 'contents', 'ticket_list', $data);
    } else {

      redirect('login');
    }
  }





  public function genrate_ticket()

  {

    if ($this->session->userdata('logged_in')) {

      $data = array();

      $data['menuactive'] = $this->uri->segment(1);

      $this->template->set('title', 'Tickets');

      $this->template->load('user_dashboard_layout', 'contents', 'genrate_ticket', $data);
    } else {

      redirect('login');
    }
  }



  function random_strings($length_of_string)

  {



    $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    return substr(
      str_shuffle($str_result),

      0,
      $length_of_string
    );
  }



  public function genrateTicket()
  {

    $data = array();

    $data['menuactive'] = $this->uri->segment(1);

    $email = $this->input->post('email');

    $message = $this->input->post('message');

    $GenrateTicket_id = $this->random_strings(11);





    $data['singleData'] =  array();

    //$reminder_time = $this->input->post('reminder_time');

    $array = array(

      'user_id' => $this->session->userdata('logged_in')['id'],

      'email' => $email,

      'devicce_id' => '',

      'genrate_ticketid' => $GenrateTicket_id,

      'ticket_desc' => $message,

      'created' => date('Y-m-d H:i:s')

    );

    $result = $this->Common_model_new->insertData('user_tickets', $array);

    if ($result) {

      $returnData = array('response' => 1, 'ticket_id' => $GenrateTicket_id);

      //echo 1;

      echo json_encode($returnData);
    } else {

      $returnData = array('response' => 0);

      //echo 1;

      echo json_encode($returnData);

      //echo 0;

    }
  }









  public function reply()
  {

    $data = array();

    $data['menuactive'] = $this->uri->segment(1);

    $ticket_id = $this->input->post('ticket_id');

    $genrated_id = $this->input->post('genrated_id');

    $reply_desc = $this->input->post('message');

    //$GenrateTicket_id =$this->random_strings(11);





    $data['singleData'] =  array();



    $array = array(

      'ticket_id' => $ticket_id,

      'by_user' => $this->session->userdata('logged_in')['id'],

      'by_admin' => '',

      'genrate_ticketid' => $genrated_id,

      'user_id' => $this->session->userdata('logged_in')['id'],

      'reply_desc' => $reply_desc,

      'created' => date('Y-m-d H:i:s')

    );

    //echo '<pre>';print_r($array );die;

    $result =  $this->Common_model_new->insertData('tickets_reply', $array);



    //$result = $this->Common_model_new->insertData('user_tickets',$array);

    if ($result) {

      $returnData = array('response' => 1);

      //echo 1;

      echo json_encode($returnData);
    } else {

      $returnData = array('response' => 0);

      //echo 1;

      echo json_encode($returnData);

      //echo 0;

    }
  }





  public function reply_to_ticket()

  {

    $data['menuactive'] = $this->uri->segment(2);

    $checkLogin = $this->session->userdata('logged_in');

    if (!empty($checkLogin)) {

      //$this->form_validation->set_rules('feature_name', 'Feature Name', 'required');

      $this->form_validation->set_rules('message', 'Description.', 'required');

      //$this->form_validation->set_rules('feature_Cat', 'Feature Category.', 'required');





      $ticke_id = $this->uri->segment('2');

      $data['singleData'] = $this->common_model->getsingle('user_tickets', array('ticket_id' => $ticke_id));



      $data['userData'] = $this->common_model->getAllorderby('cp_users', 'id', 'desc');



      $ticket_id = $data['singleData']->ticket_id;

      $genrated_id = $data['singleData']->genrate_ticketid;

      $user_id = $data['singleData']->user_id;

      $reply_desc = $this->input->post('message');



      if ($this->form_validation->run() == TRUE) {



        $array = array(

          'ticket_id' => $ticket_id,

          'by_user' => $this->session->userdata('logged_in')['id'],

          'by_admin' => '',

          'genrate_ticketid' => $genrated_id,

          'user_id' => $this->session->userdata('logged_in')['id'],

          'reply_desc' => $reply_desc,

          'created' => date('Y-m-d H:i:s')

        );

        //echo '<pre>';print_r($array );die;

        $this->common_model->insertData('tickets_reply', $array);



        $this->session->set_flashdata('success', 'Message sent successfully.');

        redirect();
      }







      $this->template->set('title', 'Tickets');

      $this->template->load('user_dashboard_layout', 'contents', 'reply_ticket', $data);
    } else {

      redirect('login');
    }
  }



  // End







  public function view_ticke_detail()

  {



    $data['menuactive'] = $this->uri->segment(1);

    $ticketId = $this->uri->segment(2);



    $checkLogin = $this->session->userdata('logged_in');

    if (!empty($checkLogin)) {



      $data['ticketDetail'] = $this->common_model->getSingleRecordById('user_tickets', array('ticket_id ' => $ticketId));

      // print_r($data['ticketDetail']);

      // die;

      //$data['TestReportView']  = $this->testreport_model->getSingleTestReport($testReportId,$this->session->userdata('logged_in')['id']);

      $this->template->set('title', 'Tickets');

      $this->template->load('user_dashboard_layout', 'contents', 'view_ticket', $data);
    } else {

      redirect('login');
    }
  }



  public function view_conversation()

  {







    $data['menuactive'] = $this->uri->segment(1);

    $ticketId = $this->uri->segment(2);



    $checkLogin = $this->session->userdata('logged_in');

    if (!empty($checkLogin)) {



      $data['singleData'] = $this->common_model->getAllwhereorderby('tickets_reply', array('ticket_id' => $ticketId), 'reply_id ', 'ASC');



      // echo "<pre>";

      // print_r($data['singleData']);

      // die;



      $data['userData'] = $this->common_model->getAllorderby('cp_users', 'id', 'desc');



      //$data['ticketDetail'] = $this->common_model->getSingleRecordById('user_tickets',array('ticket_id ' => $ticketId));

      // print_r($data['ticketDetail']);

      // die;

      //$data['TestReportView']  = $this->testreport_model->getSingleTestReport($testReportId,$this->session->userdata('logged_in')['id']);

      $this->template->set('title', 'Tickets');

      $this->template->load('user_dashboard_layout', 'contents', 'view_ticket_conver', $data);
    } else {

      redirect('login');
    }
  }
}
?>
