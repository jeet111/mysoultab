<!-- jQuery UI 1.10.3 -->
        
<aside class="right-side">    
    <section class="content-header no-margin">
        <h1 class="text-center"><i class="fa fa-pencil-square-o"></i> Edit Appointment
		<div class="back_reminder"><a href="#" class="btn btn-danger">Back</a></div>
		</h1>
    </section>    
    <section class="content photo-list">
        <div class="photo-listMain">
            
			<form id="reminder" enctype="multipart/form-data" method="post" class="frm_add" >  
                <div class="row">                     
                    <div class="col-md-6">
                        <div class="bootstrap-timepicker">
                            <div class="form-group">
                                <label>Date:<span class="red_star">*</span></label>
                                <div class="input-group date costom_err" id="datetimepicker">
                                    <input type='text' class="form-control" readonly name="reminder_date" value="" placeholder="Select Date" />
                                    <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </span>                
                                </div>
                            </div>
                        </div>             
                    </div>                 
                    <div class="col-md-6">
                        <div class="bootstrap-timepicker">
                            <div class="form-group">
                                <label>Time:<span class="red_star">*</span></label>
								<ul class="list_time">
                                  <li>
                                    <input type="radio" id="f-option" name="selector" value="Morning">
                                    <label for="f-option">Morning</label>
                                    <div class="check"></div>
                                  </li>                                  
                                  <li>
                                    <input type="radio" id="s-option" name="selector" value="Noon">
                                    <label for="s-option">Noon</label>  
                                    <div class="check"><div class="inside"></div></div>
                                  </li>                                  
                                  <li>
                                    <input type="radio" id="t-option" name="selector" value="Evening">
                                    <label for="t-option">Evening</label>
                                    <div class="check"><div class="inside"></div></div>
                                  </li>
                                  <li>
                                    <input type="radio" id="p-option" name="selector" value="Night">
                                    <label for="p-option">Night</label>
                                    <div class="check"><div class="inside"></div></div>
                                  </li>
                                </ul>
                            </div>
                        </div>             
                    </div>					
                </div>
				<div class="box-footer">
                    <input type="submit" name="submit" class="btn btn-primary" value="Appointment">
                    <input type="submit" name="submit" class="btn btn-primary" value="Reminder">
                </div>               
            </form>              
        </div>
    </section>
</aside>

<!--Added by 95 on 5-2-2019 For Doctor category for validation-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script src="assets/admin/js/jquery.validate.js"></script>

  <script type="text/javascript">
     $(function () {
	var today = new Date();	 
  $('#datetimepicker').datetimepicker({
    ignoreReadonly: true,
	format: 'YYYY-MM-DD',				   
	minDate: today
  });
});
  
      $(document).ready(function() {
         $("#reminder").validate({
            rules: {
                reminder_title: "required",
                reminder_time:"required",
                reminder_date:"required",
             },
            messages: {
                reminder_title: "Please enter reminder title.",
                reminder_time:"Please select reminder time.",
                reminder_date:"Please select reminder date.",
            }
        });
    });
  </script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

<script src="assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>



    


    

                