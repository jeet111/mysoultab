﻿<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Note extends MX_Controller {
	public function __construct() {
		parent:: __construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('table');
		$this->load->library('pagination');
		$this->load->model('common_model');
		$this->load->model('common_model_new');
		$this->load->model('Common_model_new');
		$this->load->helper(array('common_helper'));
		$this->load->helper(array('url'));
		$this->load->helper(array('form')); 
		ob_start();
		not_login();	
	}
	public function note_list()
	{
		$user_id = $this->session->userdata('logged_in')['id'];
		
		$data['menuactive'] = $this->uri->segment(1);
		$data['note_list'] = $this->Common_model_new->getAllwhere("cp_note",array("user_id"=>$user_id));
		$data['btnsetingArray'] = $this->common_model_new->getAllwhere("setting_butns",array("btnname" =>'Note',"user_id"=>$user_id));	
		$data['care_giver'] = $this->Common_model_new->getAllwhere("cp_users",array("user_role" => 4)); 

		$this->template->set('title', 'Note');
		// $this->template->load('user_dashboard_layout', 'contents', 'note_list',$data);
		$this->template->load('new_user_dashboard_layout', 'contents', 'note_view',$data);

	}
	public function add_note()
	{
		$from_caregiver = $this->input->post("from_caregiver");
		$to_caregiver = $this->input->post("to_caregiver");
		$description = $this->input->post("description");
		$data['menuactive'] = $this->uri->segment(1);
		$this->form_validation->set_rules('from_caregiver', 'From caregiver', 'required');
		$this->form_validation->set_rules('to_caregiver', 'To caregiver', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$checkLogin = $this->session->userdata('logged_in');
		$data['care_giver'] = $this->Common_model_new->getAllwhere("cp_users",array("user_role" => 4)); 
	
		$user_id = $checkLogin['id'];
		// if ($this->form_validation->run() == TRUE) 
		// {    
			$array = array(
				'from_caregiver' => $from_caregiver,
				'to_caregiver' => $to_caregiver,
				'description' => $description,
				'user_id' => $user_id,
				'create_date' => date('Y-m-d H:i:s')
			);
			$getcaregiver = $this->Common_model_new->getsingle("cp_users",array("id" => $to_caregiver)); 
			$subject = "Note";
			$status = sendEmail($getcaregiver->email, $subject, $description);
		
			$this->Common_model_new->insertData("cp_note",$array);
			$this->session->set_flashdata('success', 'Note Successfully Added');
			//redirect('note_view');
		// }
		// $data['singledata'] = $this->Common_model_new->getsingle("cp_note",array("note_id" => $segment));
		// $data["fromcaregiver"] = $this->Common_model_new->getsingle("cp_users",array("id" => $data['singledata']->from_caregiver));
		// $data["tocaregiver"] = $this->Common_model_new->getsingle("cp_users",array("id" => $data['singledata']->to_caregiver));
		// $this->template->set('title', 'Note');
		// $this->template->load('new_user_dashboard_layout', 'contents', 'note_list',$data);
		$return = array('code' => 1,'message' => 'Successfully updated.!');
		echo json_encode($return);	

	}
	public function edit_note()
	{
		$data['menuactive'] = $this->uri->segment(1);
		$this->form_validation->set_rules('from_caregiver', 'From caregiver', 'required');
		$this->form_validation->set_rules('to_caregiver', 'To caregiver', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$checkLogin = $this->session->userdata('logged_in');
		$segment = $this->uri->segment("2");
		$data['segment'] = $segment;
		$data['singledata'] = $this->Common_model_new->getsingle("cp_note",array("note_id" => $segment));
		$data["fromcaregiver"] = $this->Common_model_new->getsingle("cp_users",array("id" => $data['singledata']->from_caregiver));
		$data["tocaregiver"] = $this->Common_model_new->getsingle("cp_users",array("id" => $data['singledata']->to_caregiver));
		$data['care_giver'] = $this->Common_model_new->getAllwhere("cp_users",array("user_role" => 4)); 
		// echo "<pre>";
		// print_r($data);die;
		$from_caregiver = $this->input->post("from_caregiver");
		$to_caregiver = $this->input->post("to_caregiver");
		$description = $this->input->post("description");
		$user_id = $checkLogin['id'];
		if ($this->form_validation->run() == TRUE) 
		{    
			$array = array(
				'from_caregiver' => $from_caregiver,
				'to_caregiver' => $to_caregiver,
				'description' => $description,
				'user_id' => $user_id,
				'create_date' => date('Y-m-d H:i:s')
			);
			$getcaregiver = $this->Common_model_new->getsingle("cp_users",array("id" => $to_caregiver)); 
			$subject = "Note";
			$explode = explode(" ",$description);
			$explode2 = explode(" ",$data['singledata']->description);
			if($from_caregiver != $data['singledata']->from_caregiver || $to_caregiver != $data['singledata']->to_caregiver || $explode[0] != $explode2[0]){ 
				$status = sendEmail($getcaregiver->email, $subject, $description);
			}
			$this->Common_model_new->updateData("cp_note",$array,array("note_id" => $segment));
			$this->session->set_flashdata('success', 'Note Successfully updated');
			redirect('note_list');
		}
		$this->template->set('title', 'Note');
		$this->template->load('new_user_dashboard_layout', 'contents', 'add_note',$data);
	}
	public function view_note()
	{
		$data['menuactive'] = $this->uri->segment(1);
		$segment = $this->uri->segment("2");
		$data['singledata'] = $this->Common_model_new->getsingle('cp_note',array("note_id" => $segment));
		$data["fromcaregiver"] = $this->Common_model_new->getsingle("cp_users",array("id" => $data['singledata']->from_caregiver));
		$data["tocaregiver"] = $this->Common_model_new->getsingle("cp_users",array("id" => $data['singledata']->to_caregiver));
		$this->template->set('title', 'Note');
		$this->template->load('new_user_dashboard_layout', 'contents', 'view_note',$data);
	}
	public function delete_note()
	{
		$data['menuactive'] = $this->uri->segment(1);
		$segment = $this->uri->segment("2");
		$this->Common_model_new->delete("cp_note",array("note_id" => $segment));
		$this->session->set_flashdata('success', 'Note Successfully deleted');
		redirect('note_list');
	}
	public function delete_checkboxes()
	{ 
		$data['menuactive'] = $this->uri->segment(1);
		$checkLogin = $this->session->userdata('logged_in');
		$explode = explode(',',$_POST["bodytype"]);
		foreach($explode as $id)
		{
			$where_condition = array('note_id' => $id);
			$upd = $this->Common_model_new->delete('cp_note',$where_condition);
      //if($upd){
			echo 'success';
     // }
		}
	}
}    
?>
