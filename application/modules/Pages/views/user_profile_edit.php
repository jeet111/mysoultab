<style>
	.text_heading {
		border-bottom: none;
	}

	img#img-upload {
		height: 70px;
		width: 70px;
		margin-bottom: 10px;
		float: left;
	}

	.row .col-md-8,
	.col-sm-10 {
		margin-bottom: 10px !important;
	}

	input.btn.btn-primary.up_but {
		margin: 0 !important;
	}

	.form-horizontal h3 {
		color: #000;
		text-align: center;
		margin: 0 0 17px 0px;
		/* font-weight: 600; */
	}
</style>

<!--- Success Message --->
<?php if ($this->session->flashdata('success')) { ?>
	<div class="container box">
		<div class="alert alert-success message" id="success_msg">
			<button type="button" class="close" data-dismiss="alert">x</button>
			<p align="center"><?php echo $this->session->flashdata('success'); ?></p>
		</div>
	</div>
<?php } ?>

<div class=" text_heading">
	<div id="msg_show"></div>
	<h3><i class="fa fa-list"></i> User Edit Profile<div class="rem_add">
	</h3>



</div>
<section class="content photo-list">
	<div class="photo-listMain reminder_listMain">
		<div class="swt_cor">

			<div class="col-sm-12">
				<?php
				if (!empty($users_data) && $users_data) {
					$user = $users_data;
				?>

					<form class="form-horizontal" action="<?php echo base_url(); ?>user_profile_edit/<?php echo $user['id']; ?>" method="POST" enctype="multipart/form-data" name="AddUsers" id="ADD_USER">
						<div class="form-group">
							<label class="control-label col-md-4" for="">Profile Image:<span class="field_req">*</span>
							</label>
							<div class="input-group">
								<?php if (!empty($user['profile_image'])) { ?>
									<img id='img-upload' src="<?php echo base_url() . 'uploads/profile_images/' . $user['profile_image'] ?>" width="100px">
								<?php } else { ?>
									<img id='img-upload' src="<?php echo base_url() . 'uploads/profile_images/user.png' ?>" width="100px">
								<?php } ?>
								<input type="file" id="profile_image" class="form-control col-md-8" name="profile_image" onchange="loadFile(event)">
								<!-- <span class="input-group-btn">
			 <span class="btn btn-default btn-file"></span>
		</span> -->
								<!-- <input type="hidden" class="form-control" name="user_profile_image" id="user_profile_image" value="<?php echo $user['profile_image']; ?>">

			<input type="hidden" class="form-control" name="exting_profile_image" id="exting_profile_image" value="<?php echo $user['profile_image']; ?>"> -->
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" for="name">First Name: <font style="color: red;">*</font></label>
							<div class="col-md-6">
								<input type="text" class="txtOnly form-control required" id="name" name="name" placeholder="Enter First name" value="<?php echo $user['name']; ?>">
							</div>
							<span class=""><?php echo form_error('name'); ?></span>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" for="lastname">Last Name: <font style="color: red;">*</font></label>
							<div class="col-md-6">
								<input type="text" class="txtOnly form-control required" id="lastname" name="lastname" placeholder="Enter Last name" value="<?php echo $user['lastname']; ?>">
							</div>
							<span class=""><?php echo form_error('lastname'); ?></span>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" for="gender">Gender <font style="color: red;">*</font></label>
							<div class="col-md-6">
								<select name="gender" class="form-control" id="gender">
									<option value="">Select Gender</option>
									<option value="<?php echo $user['gender']; ?>" id="male" <?php echo set_value('gender', $user['gender']) == '1' ? "selected=selected" : ""; ?>> Male</option>
									<option value="<?php echo $user['gender']; ?>" id="female" <?php echo set_value('gender', $user['gender']) == '2' ? "selected=selected" : ""; ?>> Female</option>
									<option value="<?php echo $user['gender']; ?>" id="other" <?php echo set_value('gender', $user['gender']) == '0' ? "selected=selected" : ""; ?>> Other</option>
								</select>
								<span class=""><?php echo form_error('gender'); ?></span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" for="email">Email:<font style="color: red;">*</font></label>
							<div class="col-md-6">
								<input type="text" placeholder="Enter email address" class="form-control" name="email" id="email" disabled value="<?php echo $user['email']; ?>">
							</div>
							<span class=""><?php echo form_error('email'); ?></span>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" for="street_name">Street Address<font style="color: red;">*</font></label>
							<div class="col-md-6">
								<input type="text" class="form-control required" id="street_name" name="street_name" placeholder="Street Address" value="<?php echo $user['street_name']; ?>">
							</div>
							<span class=""><?php echo form_error('street_name'); ?></span>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" for="city">City <font style="color: red;">*</font></label>
							<div class="col-md-6">
								<input type="text" class="form-control required" id="city" name="city" placeholder="city" value="<?php echo $user['city']; ?>">
							</div>
							<span class=""><?php echo form_error('city'); ?></span>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" for="state">State <font style="color: red;">*</font></label>
							<div class="col-md-6">
								<input type="text" class="form-control required" id="state" name="state" placeholder="state" value="<?php echo $user['state']; ?>">
							</div>
							<span class=""><?php echo form_error('state'); ?></span>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" for="zipcode">Zipcode <font style="color: red;">*</font></label>
							<div class="col-md-6">
								<input type="text" class="form-control required" id="zipcode" name="zipcode" placeholder="zipcode" value="<?php echo $user['zipcode']; ?>" onkeyup="this.value=this.value.replace(/[^\d]/,'')" pattern="[0][0-9]{9}">
							</div>
							<span class=""><?php echo form_error('zipcode'); ?></span>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" for="country">Country <font style="color: red;">*</font></label>
							<div class="col-md-6">
								<input type="text" class="txtOnly form-control required" id="country" name="country" placeholder="country" value="<?php echo $user['country']; ?>">
							</div>
							<span class=""><?php echo form_error('country'); ?></span>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" for="mobile">Phone/Mobile No. <font style="color: red;">*</font></label>
							<div class="col-md-6">
								<input type="text" class="form-control required" id="mobile" name="mobile" placeholder="mobile" value="<?php echo $user['mobile']; ?>" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
							</div>
							<span class=""><?php echo form_error('mobile'); ?></span>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" for="language">Language <font style="color: red;">*</font></label>
							<div class="col-md-6">
								<input type="text" class="form-control required" id="language" name="language" placeholder="language" value="<?php echo $user['language']; ?>">
							</div>
							<span class=""><?php echo form_error('language'); ?></span>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" for="timezone">Timezone <font style="color: red;">*</font></label>
							<div class="col-md-6">
								<select name="timezone" class="form-control" id="timezone">
									<option value="">Select</option>
									<?php if (!empty($zone) && $zone != '') {  ?>
										<?php foreach ($zone as $k => $v) { ?>
											<option value="<?php echo $v['cp_ct_id']; ?>" <?php if ($v['cp_ct_id'] == $user['timezone']) {
																								echo 'selected=selected';
																							} ?>> <?php echo $v['cp_ct_name']; ?> </option>
										<?php } ?>
									<?php } ?>
								</select>
								<span class=""><?php echo form_error('timezone'); ?></span>
							</div>
						</div>
						<div class="form-group row">

						<label class="col-sm-4 col-form-label">User Name:<font style="color:red;">*</font>:</label>
							<div class="col-md-6">
								<input type="text" placeholder="Enter user name" class="form-control" id="username" value="<?php echo $user['username']; ?>" disabled>
								<span class=""><?php echo form_error('username'); ?></span>
							</div>
						</div>
						<div class="form-group row">

						<label class="col-sm-4 col-form-label">Password:<font style="color:red;">*</font>:</label>
							<div class="col-md-6">
								<input type="text" placeholder="Password" class="form-control" name="password" id="password">
							</div>
						</div>
						<div class="form-group row">

						<label class="col-sm-4 col-form-label">Confirm password:<font style="color:red;">*</font>:</label>
							<div class="col-md-6">
								<input type="text" placeholder="confirm password" class="form-control" name="cpassword" id="cpassword">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label">
								<b>Notification Preference Panel</b>
							</label>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" for="snt_txt_msg_alrt">Sent me text msg for alert <font style="color: red;">*</font></label>
							<div class="col-md-6">
								<select name="snt_txt_msg_alrt" class="form-control" id="snt_txt_msg_alrt">
									<option value="">Select</option>
									<?php if (!empty($snt_txt_msg_alrt) && $snt_txt_msg_alrt != '') {  ?>
										<?php foreach ($snt_txt_msg_alrt as $k => $v) { ?>
											<option value="<?php echo $k; ?>" <?php if ($user['snt_txt_msg_alrt'] == $k) {
																					echo 'selected=selected';
																				} ?>> <?php echo $v; ?> </option>
										<?php } ?>
									<?php } ?>
								</select>
								<span class=""><?php echo form_error('snt_txt_msg_alrt'); ?></span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" for="snt_eml_alrt">Sent me email for alert <font style="color: red;">*</font></label>
							<div class="col-md-6">
								<select name="snt_eml_alrt" class="form-control" id="snt_eml_alrt">
									<option value="">Select</option>
									<?php if (!empty($snt_eml_alrt) && $snt_eml_alrt != '') {  ?>
										<?php foreach ($snt_eml_alrt as $k => $v) { ?>
											<option value="<?php echo $k; ?>" <?php if ($user['snt_eml_alrt'] == $k) {
																					echo 'selected=selected';
																				} ?>> <?php echo $v; ?> </option>
										<?php } ?>
									<?php } ?>
								</select>
								<span class=""><?php echo form_error('snt_eml_alrt'); ?></span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" for="snt_daily_eml_daily_rutin_alrt">Sent me daily email routin alert <font style="color: red;">*</font></label>
							<div class="col-md-6">
								<select name="snt_daily_eml_daily_rutin_alrt" class="form-control" id="snt_daily_eml_daily_rutin_alrt">
									<option value="">Select</option>
									<?php if (!empty($snt_daily_eml_daily_rutin_alrt) && $snt_daily_eml_daily_rutin_alrt != '') {  ?>
										<?php foreach ($snt_daily_eml_daily_rutin_alrt as $k => $v) { ?>
											<option value="<?php echo $k; ?>" <?php if ($user['snt_daily_eml_daily_rutin_alrt'] == $k) {
																					echo 'selected=selected';
																				} ?>> <?php echo $v; ?> </option>
										<?php } ?>
									<?php } ?>
								</select>
								<span class=""><?php echo form_error('snt_daily_eml_daily_rutin_alrt'); ?></span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" for="snt_daily_eml_daily_rutin_alrt">Sent me email Their is no activity in app <font style="color: red;">*</font></label>
							<div class="col-md-6">
								<select name="snt_eml_no_activity_in_app_alrt" class="form-control" id="snt_eml_no_activity_in_app_alrt">
									<option value="">Select</option>
									<?php if (!empty($snt_eml_no_activity_in_app_alrt) && $snt_eml_no_activity_in_app_alrt != '') {  ?>
										<?php foreach ($snt_eml_no_activity_in_app_alrt as $k => $v) { ?>
											<option value="<?php echo $k; ?>" <?php if ($user['snt_eml_no_activity_in_app_alrt'] == $k) {
																					echo 'selected=selected';
																				} ?>> <?php echo $v; ?> </option>
										<?php } ?>
									<?php } ?>
								</select>
								<span class=""><?php echo form_error('snt_eml_no_activity_in_app_alrt'); ?></span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" for="snt_eml_alrm_is_missed">Sent me email when medicine alram missed <font style="color: red;">*</font></label>
							<div class="col-md-6">
								<select name="snt_eml_alrm_is_missed" class="form-control" id="snt_eml_alrm_is_missed">
									<option value="">Select</option>
									<?php if (!empty($snt_eml_alrm_is_missed) && $snt_eml_alrm_is_missed != '') {  ?>
										<?php foreach ($snt_eml_alrm_is_missed as $k => $v) { ?>
											<option value="<?php echo $k; ?>" <?php if ($user['snt_eml_alrm_is_missed'] == $k) {
																					echo 'selected=selected';
																				} ?>> <?php echo $v; ?> </option>
										<?php } ?>
									<?php } ?>
								</select>
								<span class=""><?php echo form_error('snt_eml_alrm_is_missed'); ?></span>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<input type="submit" class="btn btn-primary" class="form-control" name="update" value="Submit">
							</div>
						</div>

					</form>
				<?php } ?>
			</div>
		</div>
	</div>
	</div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

<script>
	$(document).ready(function() {
		setTimeout(function() {
			$('#success_msg').fadeOut('fast');
		}, 2500);
	});

	function loadFile(event) {

		$(document).ready(function () {
				setTimeout(function() {
					$('#success_msg').fadeOut('fast');
				}, 2500);
			}
		);

		var pcExt = pcFile.split('.').pop();

		var output = document.getElementById('img-upload');

		if (pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"

			||
			pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF") {

			$('#img-upload').show();

			output.src = URL.createObjectURL(event.target.files[0]);

			$('#flupmsg').html('');

		} else {

			$('#flupmsg').html('Please select only Image file');

			$('#img-upload').hide();

		}

	};
</script>

<script type="text/javascript">
	$(document).ready(function() {

		$(".txtOnly").keypress(function(e) {
			var key = e.keyCode;
			if (key >= 48 && key <= 57) {
				e.preventDefault();
			}
		});

		jQuery.validator.addMethod("lettersonly", function(value, element) {
			return this.optional(element) || /^[a-z]+$/i.test(value);
		}, "Letters only please");

		$.validator.addMethod("regx", function(value, element, regexpr) {
			return regexpr.test(value);
		}, "Please upload valid extension jpg,jpeg,png file.");

		$("#ADD_USER").validate({
			rules: {
				name: {
					required: true,
					lettersonly: true
				},
				lastname: {
					required: true,
					lettersonly: true
				},
				gender: {
					required: true,
				},
				street_name: {
					required: true,
				},
				mobile: {
					required: true,
					number: true,
					minlength: 10,
					maxlength: 14,
				},
				password: {
					minlength: 6,
					maxlength: 30,
				},
				cpassword: {
					minlength: 6,
					maxlength: 30,
					equalTo: "#password"
				},
				city: {
					required: true,
				},
				state: {
					required: true,
				},
				zipcode: {
					required: true,
					minlength: 5,
					maxlength: 5,
					digits: true
				},
				country: {
					required: true,
					lettersonly: true
				},
				language: {
					required: true,
				},
				email: {
					required: true,
					maxlength: 40,
					remote: {
						url: '<?php base_url(); ?>' + "Pages/UserContact/validateData",
						type: "post",
					}
				},
				timezone: {
					required: true,
				},
				username: {
					required: true,
					maxlength: 40,
					remote: {
						url: '<?php base_url(); ?>' + "Pages/UserContact/validateData",
						type: "post",
					}
				},
				snt_txt_msg_alrt: {
					required: true,
				},
				snt_eml_alrt: {
					required: true,
				},
				snt_daily_eml_daily_rutin_alrt: {
					required: true,
				},
				snt_eml_no_activity_in_app_alrt: {
					required: true,
				},
				snt_eml_alrm_is_missed: {
					required: true,
				}
			},
			messages: {
				name: {
					required: 'Please enter first name.',
					lettersonly: 'Only aphabet awllowed',
				},
				lastname: {
					required: 'Please enter last name.',
					lettersonly: 'Only aphabet awllowed',
				},
				gender: {
					required: "Please select your gender.",
				},
				street_name: {
					required: "Please enter street address.",
				},
				city: {
					required: "Please enter city.",
				},
				state: {
					required: "Please enter state.",
				},
				zipcode: {
					required: "Please enter zipcode.",
					//required: "Please enter your Postal Code!",
					minlength: "Your Postal Code must be 5 numbers!",
					maxlength: "Your Postal Code must be 5 numbers!",
					digits: "Your Postal Code must be 5 numbers!"
				},
				country: {
					required: "Please enter country.",
					lettersonly: 'Only aphabet awllowed',
				},
				language: {
					required: "Please enter language.",
				},
				email: {
					required: "Please enter a email.",
					email: "Please enter a valid email.",
					maxlength: 40,
					remote: 'Email already used.',
				},
				timezone: {
					required: "Please Select timezone.",
				},
				username: {
					required: 'Please enter username.',
					maxlength: 40,
					remote: 'Username already used.',
				},
				mobile: {
					required: "Please enter a mobile number.",
					number: "Please enter a valid number.",
					minlength: "Please enter at least 10 characters.",
					maxlength: "please enter max. 14 characters.",
				},

				password: {
					required: "Please enter a password.",
					minlength: "Please enter at least 6 characters.",
					maxlength: "please enter max. 30 characters.",
				},

				cpassword: {
					required: "Please enter a confirm password.",
					minlength: "Please enter at least 6 characters.",
					maxlength: "Please enter no more than 30 characters.",
					equalTo: "Confirm password not matched.",
				},
				snt_txt_msg_alrt: {
					required: "Please select text message status.",
				},
				snt_eml_alrt: {
					required: "Please select email status.",
				},
				snt_daily_eml_daily_rutin_alrt: {
					required: "Please select email for daily routine acivites status.",
				},
				snt_eml_no_activity_in_app_alrt: {
					required: "Please select email thier is no activity in app status.",
				},
				snt_eml_alrm_is_missed: {
					required: "Please select medicine alram status.",
				}
			},
		});
	});
</script>
