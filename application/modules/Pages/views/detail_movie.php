<style>

.rew, .fwd{

  display:none;

}

</style>

<?php //echo "<pre>"; print_r($detail_movie); echo "</pre>"; ?>

<aside class="right-side">

  <section class="content-header no-margin">

    <h1 class="text-center"><i class="fa fa-pencil-square-o"></i> Movie Details

      <div class="back_reminder"><a href="<?php echo base_url(); ?>movie_list" class="btn btn-danger">Back</a></div>

    </h1>

  </section>

  <section class="content player_main">

    <div class="photo-listMain">

      <div class="movieSize">

        <div class="main_movie">

          <div class="sub_movie2">

            <ul class="list_lk">

              <!--<li><a href="#"><i class="fa fa-heart-o remove"></i></a></li>-->

              <li id="re"><a href="javascript:void(0);" class="fav_none" id="favorite_movie" data-movie="<?php echo $detail_movie->movie_id; ?>"><?php if($check_movie){ ?><i class="fa fa-heart remove" aria-hidden="true"></i><?php }else{ ?><i class="fa fa-heart-o remove"></i><?php } ?></li>

               <li style="display:none" id="renew"><a href="javascript:void(0);" id="favorite_movie" data-movie="<?php echo $this->uri->segment('2'); ?>"><i class="fa fa-heart" aria-hidden="true"></i></a></li>

               <li style="display:none" id="renew2"><a href="javascript:void(0);" id="favorite_movie" data-movie="<?php echo $this->uri->segment('2'); ?>"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>

               <li><a href="<?php echo base_url(); ?>delete_favoriteMovie/<?php echo $this->uri->segment('2'); ?>" onclick="return confirm('Are you sure want to delete this movie ?')"><i class="fa fa-trash-o"></i></a></li>

             </ul>

             <video  controls autoplay>

              <source src="<?php echo base_url().'uploads/movie/'.$detail_movie->movie_file; ?>" type="video/mp4">

                Sorry, your browser doesn't support the video element.

              </video></div>

            </div>

          </div>

        </div>

      </section>

    </aside>

    <script>

      $(".example").musicPlayer({

elements: ['artwork', 'information', 'controls', 'progress', 'time', 'volume'], // ==> This will display in  the order it is inserted

});

      $(document).on("click","#favorite_movie",function(){

        var movie_id = $(this).attr('data-movie');

        $.ajax({

          type:'POST',

          url: "<?php echo base_url().'Pages/Movie/favorite_movie'; ?>",

          data: 'movie_id='+movie_id,

          success:function(data){

            $("#re").hide();

            if(data == '0'){

              $("#renew").hide();

              $(".fav_none").hide();

              $("#renew2").show();

            }else{

              $(".fav_none").hide();

              $("#renew2").hide();

              $("#renew").show();

            }

          }

        });

      });

    </script>

    <script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>



    <script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

    <script src="<?php echo base_url();?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>



