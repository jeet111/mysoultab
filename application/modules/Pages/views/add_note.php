<!-- jQuery UI 1.10.3 -->
<style>
      .text_heading {
    border-bottom: none !important;
}
  .photo-listMain {
    background: #fff;
    padding: 20px;
    box-sizing: border-box;
    float: left;
    width: 100%;
    box-shadow: rgba(100, 100, 111, 0.4) 0px 7px 29px 0px;
  }

  .main_section h1 {
    color: #000;
    padding-top: 30px;
    text-align: center;
    margin-top: 30px;
    margin-bottom: 27px;
  }

  .addNote a,
  .btn.btn-primary {
    margin: 10px 0 !important;
    padding: 10px 28px !important;
    max-width: 126px !important;
    width: 100% !important;
    border-radius: 50px !important;
  }

  input#from_caregiver {
    float: left;
    width: 100%;
    padding: 9px;
    border: 1px solid #ddd;
  }

  form#detail_acknowledge {
    background: #fff;
    padding: 20px;
  }

  .bx_med {
    color: #000;
    font-size: 17px;
    margin-bottom: 10px;
    border-bottom: 1px solid #ddd;
    padding-bottom: 6px;
  }

  form#add_note label {
    color: #000;
    font-size: 17px;
    margin-bottom: 5px;
    padding-bottom: 6px;
  }

  section.content.photo-list {
    float: left;
    width: 100%;
    margin-bottom: 30px;
  }
</style>
<div class="text_heading">  
  <h3>
    <!-- <i class="fa fa-paper-plane"></i>  -->
    <i class="fa fa-file-text" aria-hidden="true"></i>
    <?php if (!empty($segment)) { ?> Edit <?php } else { ?> Add <?php } ?>Note
  </h3>
  <div class="back_reminder addNote"><a href="<?php echo base_url(); ?>note_list" class="btn btn-primary">Back</a></div>

</div>

<section class="content photo-list">
  <div class="photo-listMain">
    <form id="add_note" enctype="multipart/form-data" method="post" class="frm_add">
      <div class="row">
        <div class="col-sm-6 col-md-6">
          <div class="form-group">
        
            <label>Username:</label>
            <?php
            $user = $this->session->userdata('logged_in');
            // print_r($user_id);
            ?>
            <input type="text" name="from_caregiver" id="from_caregiver" data-value="<?php echo $user['id']; ?>" value="<?php echo $username = $user['name'] . ' ' . $user['lastname']; ?>" >

            <?php echo form_error('from_caregiver'); ?>
          </div>
        </div>
        <div class="col-sm-6 col-md-6">
          <div class="form-group">
            <label>To caregiver<font style="color:red;">*</font>:</label>
            <select name="to_caregiver" id="to_caregiver" class="form-control valid">
              <option value="">Select Caregiver</option>
              <?php foreach ($care_giver as $to_caregiver) { 
                // print_r($to_caregiver);
                ?>
                <option value="<?php echo $to_caregiver->id; ?>" <?php if ($to_caregiver->id == $to_caregiver->id) {
                                                                    echo 'selected';
                                                                  } ?>><?php echo $to_caregiver->name .' '.$to_caregiver->lastname ; ?></option>
              <?php } ?>
            </select>
            <?php echo form_error('to_caregiver'); ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 col-md-12">
          <div class="form-group">
            <label>Description:</label>
            <textarea cols="80"label="notes"rows="4" class="form-control valid" name="description" id="description">
                <?php echo $singledata->description; ?> 
              </textarea>
          </div>
        </div>
      </div>
      <div class="">
        <input type="submit" name="submit" class="btn btn-primary" value="Edit Note">
      </div>
    </form>
  </div>
</section>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
<script type="text/javascript">
 jQuery(document).ready(function() {
  jQuery('#description').val(jQuery.trim($('#description').val()));

  jQuery("#add_note").validate({
      rules: {
        to_caregiver: "required",
        description: "required",
        from_caregiver: {
          required: true,
        },
      },
      messages: {
        to_caregiver: "Please enter To caregiver.",
        description: "Please enter description.",
        from_caregiver: "Please enter From caregiver."
      }
    });
  });
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery.min1.js"></script>
