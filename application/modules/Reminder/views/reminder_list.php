<style>
  .bootstrap-timepicker-widget.dropdown-menu.open {
    display: inline-block;
    z-index: 99999 !important;
  }

  .modal-header .close {
    margin-top: -22px;
  }

  .bootstrap-datetimepicker-widget.timepicker-picker table td,
  .bootstrap-datetimepicker-widget.timepicker-picker table td span,
  .bootstrap-datetimepicker-widget.timepicker-picker table td a span {
    height: 30px;
    line-height: 30px;
    width: 30px;
    padding: 0px;
  }

  .btn.btn-primary {
    margin: 0px 0 14px !important;
  }

  .text_heading {
    border-bottom: 0;
  }

  th.sorting_asc i {
    font-size: 18px;
  }

  th {
    font-size: 13px;
    line-height: normal;
    padding: 2px 10px 0 7px !important;
    margin: 0 !important;
  }

  .table-responsive {
    overflow-x: initial !important;
  }

  table#sampleTable th {
    border-bottom: none;
  }

  textarea.form-control {
    width: 100% !important;
  }

  .modal-header {
    padding: 8px 20px 0 20px;
    border-bottom: none;
  }

  .modal-footer {
    padding: 0;
    border-top: 0;
  }

  .modal-body.content.photo-list {
    float: left;
    width: 100%;
  }

  .modal-body h2 {
    color: green;
    font-size: 20px;
    border: 0 !important;
    padding: 0;
    text-align: center;
    margin: 0;
  }
</style>
<div class="text_heading">
  <div id="msg_show"></div>
  <h3><i class="fa fa-bell" aria-hidden="true"></i> Reminder List</h3>
  <div class="tooltip-2">
    <h2>Display Reminder List button on Mobile App

      <?php
      if (is_array($btnsetingArray)) {

        foreach ($btnsetingArray as $res) {
          if ($res->settings == 1) { ?>
            <label class="switch ">
              <input type="checkbox" checked data-btn="<?php echo 'Reminder' ?>" class="updateStatus">
              <span class="slider round"></span>
            </label>

          <?php } else { ?>
            <label class="switch space">
              <input type="checkbox" data-btn="<?php echo 'Reminder' ?>" class="updateStatus">
              <span class="slider round"></span>
            </label>


          <?php } ?>
      <?php }
      }
      ?>
    </h2>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
      Add
    </button>
  </div>
</div>
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Reminder</h5>
        <button type="button" class="close" data-dismiss="modal" onclick="this.form.reset();" aria-label="Close" id="closeButton">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body content photo-list">


        <div class="photo-listMain">

          <form id="reminder">



            <div id="success_message"></div>
            <div class="row">

              <div class="col-sm-6 col-md-6">

                <div class="form-group">

                  <label for="">Title:<span class="red_star">*</span></label>

                  <input type="text" class="form-control" id="reminder_title" name="reminder_title" value="" placeholder="Reminder Title" maxlength="100">

                  <td class="error"><?php echo form_error('reminder_title'); ?></td>

                </div>

              </div>

              <div class="col-sm-6 col-md-6">

                <div class="bootstrap-timepicker">

                  <div class="form-group">

                    <label>Date:<span class="red_star">*</span></label>

                    <div class="input-group date costom_err" id="datetimepicker">



                      <input type='text' class="form-control" readonly name="reminder_date" value="" placeholder="Reminder Date" />

                      <span class="input-group-addon">

                        <span class="glyphicon glyphicon-calendar"></span>

                      </span>

                      <td class="error"><?php echo form_error('reminder_date'); ?></td>

                    </div>

                  </div>

                </div>

              </div>
            </div>
            <div class="row">
              <div class="col-sm-6 col-md-6">

                <div class="bootstrap-timepicker">

                  <div class="form-group">

                    <label>Time:<span class="red_star">*</span></label>

                    <div class="input-group" id='datetimepicker3'>



                      <input type='text' data-format="hh:mm:ss" name="reminder_time" class="form-control" value="" placeholder="Reminder Time" onkeydown="return false" />

                      <span class="input-group-addon add-on">

                        <span class="glyphicon glyphicon-time"></span>

                      </span>

                    </div>


                  </div>
                  <td class="error"><?php echo form_error('reminder_time'); ?></td>

                </div>

              </div>

              <div class="col-sm-6 col-md-6">

                <div class="form-group">

                  <label>Remind Before:<span class="red_star">*</span></label>

                  <select name="before_time" class="form-control">

                    <option value="">Select Before Time</option>

                    <option value="5">5 Minutes</option>

                    <option value="10">10 Minutes</option>

                    <option value="15">15 Minutes</option>

                    <option value="20">20 Minutes</option>

                  </select>

                  <td class="error"><?php echo form_error('reminder_date'); ?></td>

                </div>

              </div>
            </div>

            <div class="row">
              <div class="col-sm-6 col-md-6">

                <div class="form-group">

                  <label>Repeat Reminder:<span class="red_star">*</span></label>

                  <select name="repeat_time" class="form-control  ">

                    <option value="">Select Repeat Time</option>

                    <option value="Once">Once</option>

                    <option value="Every Day">Every Day</option>

                    <option value="Every Week">Every Week</option>

                    <option value="Every Year">Every Year</option>


                  </select>

                  <td class="error"><?php echo form_error('reminder_date'); ?></td>

                </div>

              </div>





              <div class="col-sm-6 col-md-6">

                        <div class="form-group">

                          <label for="">Description:<span class="red_star">*</span></label>

                          <textarea class="form-control" id="reminder_description" name="reminder_description" placeholder="Reminder Description"></textarea>





                          <td class="error"><?php echo form_error('reminder_description'); ?></td>

                        </div>

                      </div>

            </div>

            <div class="box-footerOne">
              <!-- <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button> -->

              <input type="submit" name="submit" class="btn btn-primary" value="Submit">

            </div>

          </form>

        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>



<section class="content photo-list">

  <div class="photo-listMain reminder_listMain">



    <div class="table-responsive tb_swt">

      <table class="table table-hover table-bordered table-mailbox send-user-mail tab_medi " id="sampleTable">

        <thead>

          <tr>

            <th> <label style="margin-right: 10px;">

                <input type="checkbox" id="checkall">

              </label>


              <a href="javascript:void(0);" name="reminder_all_del" id="reminder_all_del" data-base="<?php echo base_url(); ?>" class="reminder_all_del"><i aria-hidden="true" class="fa fa-trash-o"></i></a> Select All
            </th>

            <th>Reminder Title</th>

            <th>Reminder Date</th>

            <th>Reminder Time</th>

            <th>Action</th>

          </tr>

        </thead>

        <tbody>

          <?php

          $i = 1;

          //echo "<pre>"; print_r($user_data);

          if (!empty($reminder_list)) {

            foreach ($reminder_list as $list) { ?>

              <tr>

                <td class="small-col"><input type="checkbox" name="reminder_chk[]" class="reminder_chk" value="<?php echo $list->reminder_id; ?>" data-id="<?php echo $list->reminder_id; ?>" /></td>

                <td><?php echo $list->reminder_title; ?></td>

                <td><?php echo $list->reminder_date; ?></td>

                <td><?php echo $list->reminder_time; ?></td>

                <td class="act">

                  <a href="<?php echo base_url(); ?>view_reminder/<?php echo $list->reminder_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

                  <a href="<?php echo base_url(); ?>edit_reminder/<?php echo $list->reminder_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                  <a href="<?php echo base_url(); ?>delete_reminder/<?php echo $list->reminder_id; ?>" onclick="return confirm('Are you sure, you want to remove this reminder ?')"><i class="fa fa-trash" aria-hidden="true"></i></a>

                </td>

              </tr>

          <?php }
          } ?>

        </tbody>

      </table>

    </div>

  </div>

</section>

</aside>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

<script type="text/javascript">
  $(document).ready(function() {

    $('#sampleTable').DataTable();
  });
  jQuery('#checkall').on('click', function(e) {

    if ($(this).is(':checked', true))

    {

      $(".reminder_chk").prop('checked', true);

    } else

    {

      $(".reminder_chk").prop('checked', false);

    }

  });
  $(document).on('click', '.reminder_all_del', function(e) {





    var bodytype = $('input[name="reminder_chk[]"]:checked').map(function() {

      return this.value;

    }).get().join(",");



    if (bodytype != '') {

      if (confirm("Are you sure you want to remove this note ?")) {

        $.ajax({

          url: '<?php echo base_url(); ?>Reminder/delete_checkboxes',

          type: 'POST',

          data: {
            bodytype: bodytype
          },
          //dataType: "json",

          success: function(data) { //alert('swati');
            setTimeout(function() {

              $("#msg_show").html("<div class='alert alert-success message'>Successfully deleted !</div>");

              location.reload();

              //window.location.href = email_url+"email_list";

            }, 1300);

          }

        });



      }

    } else {

      alert("Please select atleast one note.");

    }

  });
  $(function() {

    var today = new Date();

    $('#datetimepicker').datetimepicker({

      ignoreReadonly: true,

      format: 'YYYY-MM-DD',

      minDate: today

    });
    $("#datetimepicker3").datetimepicker({
      format: "LT",

    });
  });
  $('#reminder').validate({

    rules: {

      reminder_title: "required",

      reminder_description: "required",

      reminder_date: "required",

      reminder_time: "required",

      before_time: "required",

      repeat_time: "required",

    },

    messages: {

      reminder_title: "Please enter reminder title.",

      reminder_description: "Please enter reminder descripiton.",

      reminder_date: "Please select reminder date.",

      reminder_time: "Please select reminder time.",

      before_time: "Please select remind before time.",

      repeat_time: "Please select repeat reminder time.",

    },

    submitHandler: function(form) {
      $.ajax({
        url: '<?php echo base_url(); ?>Reminder/add_reminder1',
        type: 'post',
        data: $(form).serialize(),
        success: function(response) {
          if (response == 1) {
            $('#success_message').fadeIn().html('<div class="alert alert-success message"><button type="button" class="close" data-dismiss="alert">x</button>Reminder added successfully.</div>');

            setTimeout(function() {
              $('#success_message').fadeOut("milliseconds");

            }, 500);
            location.reload();

          }

        }

      });

    }

  });

  $(document).ready(function() {

    $('#exampleModal').on('hidden.bs.modal', function() {
      var $alertas = $('#reminder');
      $alertas.validate().resetForm();
      $alertas.find('.error').removeClass('error');
      $alertas.resetForm();
    });
  });
  $(document).on('click', '.updateStatus', function() {
    var update_status = $(this).attr('data-btn');
    var update_st = $(this).attr('data-st');
    //alert(update_status+'-'+update_st);
    $.ajax({
      url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
      type: "POST",
      data: {
        'btn': update_status
      },
      dataType: "json",
      success: function(data) {
        $("#msg_show").html("<div class='alert alert-success'>" + data.status + "</div>");
      }
    });
  });
</script>