<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Doctor extends MX_Controller {

	private $staff_permission = array();
	private $access_permission = true;
	public function __construct(){

		parent:: __construct();

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model('Common_model');
		$this->load->model('login_model');
		$this->load->helper(array('url'));
		$this->load->helper(array('form'));
		$this->load->library("pagination");

		$user_id = $this->session->userdata('logged_ins')['id'];
		if(empty($user_id)) {
			redirect('admin/login');
		}

		if ($this->session->userdata('logged_ins')['user_role'] == 5) {
			$uid = $this->session->userdata('logged_ins')['id'];
			$permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
			if (!empty($permission_list)) 
			{
				foreach ($permission_list as $k => $v) 
				{
					$this->staff_permission[$v['permission_id']] = $v;
				}
			}
		}

	}

	public function permission_access($permission_id=3)
	{
		if ($this->session->userdata('logged_ins')['user_role'] != 1) 
		{
			if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
			{
				$this->access_permission = false;
			}
		}
	}

	public function doctor_list()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$data['doctor_list'] = $this->Common_model->getAllorderby('cp_doctor','doctor_id','desc');

		$data['staff_permission'] = $this->staff_permission;
		

		$this->permission_access(3);
		if ($this->access_permission) {
			$this->template->set('title', 'Doctor Management');
			$this->template->load('admin_dashboard_layout', 'contents', 'doctor_list', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}

	public function add_doctor()
	{


  	// echo $this->uri->segment(2);
  	// die();

		$data['menuactive'] = $this->uri->segment(2);
		$this->form_validation->set_rules('doctor_name', 'Doctor Name', 'required');

		//$this->form_validation->set_rules('doctor_address', 'Address', 'required');

		//$this->form_validation->set_rules('fax_num', 'Fax Number', 'required');
		

		$this->form_validation->set_rules('doctor_mobile', 'Mobile', 'required');
		//$this->form_validation->set_rules('doctor_fees', 'Fees', 'required');
		$this->form_validation->set_rules('doctor_email', 'Email', 'required');

		$doctor_name = $this->input->post('doctor_name');
		$doctor_category = $this->input->post('doctor_category');
		$doctor_address = $this->input->post('doctor_address');
		$doctor_mobile = $this->input->post('doctor_mobile');
		$doctor_fees = $this->input->post('doctor_fees');
		$doctor_email = $this->input->post('doctor_email');
		$fax_num = $this->input->post('fax_num');

		if(!empty($doctor_category)){
			$implode = implode(',',$doctor_category);
		}
		$data['doctor_category'] = $this->Common_model->getAllwhereorderby('cp_doctor_categories',array('dc_status' => 1),'dc_id','desc');

		if ($this->form_validation->run() == TRUE)
		{
		 	//Check whether user upload picture
			if(!empty($_FILES['picture']['name'])){
				$config['upload_path'] = 'uploads/doctor/';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = $_FILES['picture']['name'];
                //Load upload library and initialize configuration
				$this->load->library('upload',$config);
				$this->upload->initialize($config);

				// $rtun = $this->upload->do_upload('picture');
				// echo $rtun;
				// die;
				if($this->upload->do_upload('picture')){
					$uploadData = $this->upload->data();
					$picture = $uploadData['file_name'];
				}else{
					$picture = '';

					// echo  $this->upload->display_errors();
					// echo "<br>";
					// echo "check1";
					// die;
				}
			}else{
				$picture = '';
				// echo "check2";
				// die;

			}

			//echo "<pre>";
			// echo $picture;
			// die;


			$array = array(
				'doctor_name' => $doctor_name,
				'doctor_category_id' => $implode,
				'doctor_address' => $doctor_address,
				'doctor_mob_no' => $doctor_mobile,
				'doctor_fees' => $doctor_fees,
				'doctor_email'=>$doctor_email,
				'fax_num'=>$fax_num,
				'doctor_pic' =>$picture,
				'doctor_created' => date('Y-m-d H:i:s')
			);
			 //echo '<pre>';print_r($array );die;
			$this->Common_model->insertData('cp_doctor',$array);

			$this->session->set_flashdata('success', 'Doctor Successfully Added');
			redirect('admin/doctor_list');

		}
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(3);
		if ($this->access_permission) {
			$this->template->set('title', 'Doctor Management');
			$this->template->load('admin_dashboard_layout', 'contents' , 'add_doctor', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}

	}

	public function edit_doctor()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$this->form_validation->set_rules('doctor_name', 'Doctor Name', 'required');

		$this->form_validation->set_rules('doctor_address', 'Address', 'required');

		$this->form_validation->set_rules('fax_num', 'Fax Number', 'required');

		$this->form_validation->set_rules('doctor_mobile', 'Mobile', 'required');
		$this->form_validation->set_rules('doctor_fees', 'Fees', 'required');
		$this->form_validation->set_rules('doctor_email', 'Email', 'required');

		$segment = $this->uri->segment('3');

		$doctor_name = $this->input->post('doctor_name');
		$doctor_category = $this->input->post('doctor_category');
		$doctor_address = $this->input->post('doctor_address');
		$doctor_mobile = $this->input->post('doctor_mobile');
		$doctor_fees = $this->input->post('doctor_fees');
		$doctor_email = $this->input->post('doctor_email');
		$fax_num = $this->input->post('fax_num');

		if(!empty($doctor_category)){

			$implode = implode(',',$doctor_category);
	   //echo $implode;die;
		}
		$data['doctor_category'] = $this->Common_model->getAllorderby('cp_doctor_categories','dc_id','desc');
		$data['singleData'] = $this->Common_model->getsingle('cp_doctor',array('doctor_id' => $segment));


		if ($this->form_validation->run() == TRUE)
		{
	         //Check whether user upload picture
			if(!empty($_FILES['picture']['name'])){
				$config['upload_path'] = 'uploads/doctor/';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = $_FILES['picture']['name'];
                //Load upload library and initialize configuration
				$this->load->library('upload',$config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('picture')){
					$uploadData = $this->upload->data();
					$picture = $uploadData['file_name'];
				}else{
					$picture = '';
				}





				$array = array(
					'doctor_name' => $doctor_name,
					'doctor_category_id' => $implode,
					'doctor_address' => $doctor_address,
					'doctor_mob_no' => $doctor_mobile,
					'doctor_fees' => $doctor_fees,
					'doctor_email'=>$doctor_email,
					'fax_num'=>$fax_num,
					'doctor_pic' =>$picture,
					'doctor_created' => date('Y-m-d H:i:s')
				);
			// echo '<pre>';print_r($array );die;
				$this->Common_model->updateData('cp_doctor',$array,array('doctor_id' => $segment));

			}else{
				$picture = '';
			}


			$array = array(
				'doctor_name' => $doctor_name,
				'doctor_category_id' => $implode,
				'doctor_address' => $doctor_address,
				'doctor_mob_no' => $doctor_mobile,
				'doctor_fees' => $doctor_fees,
				'fax_num'=>$fax_num,
				'doctor_email'=>$doctor_email,
				'doctor_created' => date('Y-m-d H:i:s')
			);
			// echo '<pre>';print_r($array );die;
			$this->Common_model->updateData('cp_doctor',$array,array('doctor_id' => $segment));

			$this->session->set_flashdata('success', 'Successfully updated');
			redirect('admin/doctor_list');

		}
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(3);
		if ($this->access_permission) {
			$this->template->set('title', 'Doctor Management');
			$this->template->load('admin_dashboard_layout', 'contents' , 'edit_doctor', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}

	public function view_doctor()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$segment = $this->uri->segment('3');

		$data['view_doctor'] = $this->Common_model->getsingle('cp_doctor',array('doctor_id' => $segment));

		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(3);
		if ($this->access_permission) {
			$this->template->set('title', 'Doctor Management');
			$this->template->load('admin_dashboard_layout', 'contents' , 'view_doctor', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}

	/*Delete for all records related to this doctor.*/
	public function delete_doctor()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$segment = $this->uri->segment('3');


		$Doctordata = $this->Common_model->getsingle('cp_doctor',array('doctor_id' => $segment));
	  //echo "<pre>";print_r($Doctordata->doctor_pic);die;
		$imageFilePath=getcwd().'/uploads/doctor/';
		$imageFile=$Doctordata->doctor_pic;
		unlink($imageFilePath.$imageFile);


		$this->Common_model->delete('cp_doctor',array('doctor_id' => $segment));

		$this->Common_model->delete('doctor_appointments',array('doctor_id' => $segment));

		$allDates = $this->Common_model->getAllwhereorderby('dr_available_date',array('avdate_dr_id' => $segment),'avdate_id','desc');

		foreach ($allDates as $date) {
			$this->Common_model->delete('dr_available_time',array('avtime_date_id' => $date->avdate_id));
		}
		$this->Common_model->delete('dr_available_date',array('avdate_dr_id' => $segment));

		$this->session->set_flashdata('success', 'Successfully deleted');
		redirect('admin/doctor_list');
	}


  // public function delete_doctor()
  // {
  // 		 $data['menuactive'] = $this->uri->segment(2);
	 //  $segment = $this->uri->segment('3');
	 //  $this->Common_model->delete('cp_doctor',array('doctor_id' => $segment));
	 //   $this->session->set_flashdata('success', 'Successfully deleted');
		// 	 redirect('admin/doctor_list');
  // }

	public function change_category_status()
	{
		$update_status = $this->input->post('update_status');
		$getdata = $this->Common_model->getsingle('cp_doctor_categories',array('dc_id' => $update_status));
		if($getdata->dc_status == '1'){
			$category_status = $this->Common_model->updateData('cp_doctor_categories',array('dc_status' => 0),array('dc_id' => $update_status));
			$error['status'] = 'Deactive Successfully';
		}else{
			$category_status = $this->Common_model->updateData('cp_doctor_categories',array('dc_status' => 1),array('dc_id' => $update_status));
			$error['status'] = 'Active Successfully';
		}

		echo json_encode($error);

	}


	/*Created by 95 on 20-2-2019 for add doctor shedules*/

	public function add_doctor_shedule($id)
	{
		$data['menuactive'] = $this->uri->segment(2);
		$this->form_validation->set_rules('shedule_date', 'Shedule Date', 'required');
		if ($this->form_validation->run() == TRUE)
		{
			$var = $this->input->post('shedule_date');
			$shedule_date = date("Y-m-d", strtotime($var));
			$result = $this->Common_model->checkDate($shedule_date,$id);
			//if(!empty($result)){
			if($result[0]['avdate_id']){
				$date_id = $result[0]['avdate_id'];
			}
			$timeResult = $this->Common_model->checkTimeSlotes($date_id);
			$allScheduls=array();
			foreach ($timeResult as $scheduleTime) {
				$allScheduls[] = $scheduleTime['avtime_text'];
			}
			$res = array_intersect($allScheduls,$_POST['shedules']);
			$times = implode(', ',$res);
			if(empty($times)){
				$result = $this->Common_model->checkDate($shedule_date,$id);
				if(empty($result)){
					$date_array=array(
						'avdate_dr_id'=>$id,
						'avdate_date'=>$shedule_date,
						'avdate_created_date'=>date('Y-m-d H:i:s')
					);
					$insert_id = $this->Common_model->insertDoctorShedule($date_array);
				}
				//else{
				foreach ($_POST['shedules'] as $time){
					if($time=="06:00 AM" || $time=="07:00 AM" || $time=="08:00 AM" || $time=="09:00 AM" || $time=="10:00 AM" || $time=="11:00 AM"){
						$avtime_day_slot='Morning';
						$avtime_day_slot_id = 1;
					}else if($time=="06:00 PM" || $time=="07:00 PM" || $time=="08:00 PM" || $time=="09:00 PM" || $time=="10:00 PM" || $time=="11:00 PM"){
						$avtime_day_slot='Evening';
						$avtime_day_slot_id = 3;
					}else if($time=="12:00 PM" || $time=="01:00 PM" || $time=="02:00 PM" || $time=="03:00 PM" || $time=="04:00 PM" || $time=="05:00 PM"){
						$avtime_day_slot='Noon';
						$avtime_day_slot_id = 2;
					}
					$result = $this->Common_model->checkDate($shedule_date,$id);
					//echo '<pre>';print_r($result);die;
					if($result[0]['avdate_id']){
						$data_id = $result[0]['avdate_id'];
					}
					if(!empty($data_id)){
						$timeArray=array(
							'avtime_date_id'=>$data_id,
							'avtime_text'=>$time,
							'avtime_day_slot'=>$avtime_day_slot,
							'avtime_day_slot_id' => $avtime_day_slot_id,
							'avtime_created_date'=>date('Y-m-d H:i:s'),
						);
					}else{
						$timeArray=array(
							'avtime_date_id'=>$insert_id,
							'avtime_text'=>$time,
							'avtime_day_slot'=>$avtime_day_slot,
							'avtime_day_slot_id' => $avtime_day_slot_id,
							'avtime_created_date'=>date('Y-m-d H:i:s'),
						);
					}
					$this->Common_model->insertDoctorTime($timeArray);
				}
				$this->session->set_flashdata('success', 'Successfully Added');
				redirect('admin/doctor_schedules');
			}else{
				$this->session->set_flashdata('faild_date', 'This date is already sheduled with '.$times.' time slotes.');
			}
		}
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(3);
		if ($this->access_permission) {
			$this->template->set('title', 'Doctor Management');
			$this->template->load('admin_dashboard_layout', 'contents', 'add_shedule', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}

	// public function add_doctor_shedule($id)
	// {
	// 	$data['menuactive'] = $this->uri->segment(2);
	// 	$this->form_validation->set_rules('shedule_date', 'Shedule Date', 'required');
	// 	if ($this->form_validation->run() == TRUE)
	// 	{
	// 		$var = $this->input->post('shedule_date');
	// 		$shedule_date = date("Y-m-d", strtotime($var));
	// 		$result = $this->Common_model->checkDate($shedule_date,$id);
	// 		if(empty($result)){
	// 			$date_array=array(
	// 				'avdate_dr_id'=>$id,
	// 				'avdate_date'=>$shedule_date,
	// 				'avdate_created_date'=>date('Y-m-d H:i:s')
	// 			);
	// 			$insert_id = $this->Common_model->insertDoctorShedule($date_array);
	// 			foreach ($_POST['shedules'] as $time){
	// 				if($time=="06:00 AM" || $time=="07:00 AM" || $time=="08:00 AM" || $time=="09:00 AM" || $time=="10:00 AM" || $time=="11:00 AM"){
	// 					$avtime_day_slot='Morning';
	// 					$avtime_day_slot_id = 1;
	// 				}else if($time=="06:00 PM" || $time=="07:00 PM" || $time=="08:00 PM" || $time=="09:00 PM" || $time=="10:00 PM" || $time=="11:00 PM"){
	// 					$avtime_day_slot='Evening';
	// 					$avtime_day_slot_id = 3;
	// 				}else if($time=="12:00 PM" || $time=="01:00 PM" || $time=="02:00 PM" || $time=="03:00 PM" || $time=="04:00 PM" || $time=="05:00 PM"){
	// 					$avtime_day_slot='Noon';
	// 					$avtime_day_slot_id = 2;
	// 				}
	// 				$timeArray=array(
	// 					'avtime_date_id'=>$insert_id,
	// 					'avtime_text'=>$time,
	// 					'avtime_day_slot'=>$avtime_day_slot,
	// 					'avtime_day_slot_id' => $avtime_day_slot_id,
	// 					'avtime_created_date'=>date('Y-m-d H:i:s'),
	// 				);
	// 				$this->Common_model->insertDoctorTime($timeArray);
	// 			}
	// 			$this->session->set_flashdata('success', 'Successfully Added');
	// 			redirect('admin/doctor_shedules');
	// 		}else{
	// 			$this->session->set_flashdata('faild_date', 'This date already sheduled.');
	// 		}
	// 	}
	// 	$this->template->set('title', 'Doctor Management');
	// 	$this->template->load('admin_dashboard_layout', 'contents' , 'add_shedule', $data);
	// }





	/*Created by 95 for show doctor shedule date list*/
	public function doctor_shedule_list()
	{

		$data['menuactive'] = $this->uri->segment(2);
		$next = "<i class='fa fa-angle-double-right' aria-hidden='true'></i>";
		$prev = "<i class='fa fa-angle-double-left' aria-hidden='true'></i>";
		$tot_row = $this->Common_model->getAllSheduleDates();
		$data['total_count'] = $tot_row;

		$data['per_page1'] = 20;
		$config = array();
		$config["base_url"] = base_url() . "admin/doctor_schedules";
		$config["total_rows"] = count($tot_row);
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	  	//echo $page;die;
		$user_appointments1 = $this->Common_model->getnewscheduledata($config["per_page"],$page);
		//echo "<pre>";print_r($user_appointments1);die;
		$data["links"] = $this->pagination->create_links();
		
		/*$data['shedule_list'] = $user_appointments1['rows'];*/
		$data['shedule_list'] = $user_appointments1['rows'];
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(3);
		if ($this->access_permission) {
			$this->template->set('title', 'Doctor Management');
			$this->template->load('admin_dashboard_layout', 'contents' , 'shedule_list',$data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
		}
	}

	/*Created by 95 for show doctor shedule time list*/
	public function view_shedule_times($id='')
	{
		$data['menuactive'] = $this->uri->segment(2);
		$data['shedule_times'] = $this->Common_model->getAllSheduleTimes($id);
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(3);
		if ($this->access_permission) {
			$this->template->set('title', 'Doctor Shedules');
			$this->template->load('admin_dashboard_layout', 'contents', 'shedule_time_list', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents', 'no_access', $data);
		}
	}
	/*Created by 95 for delete all the shedule time slotes of a date by single click*/
	public function delete_shedule_date($id='')
	{
		$this->Common_model->DeleteAllTimeShedulesByDate($id);
		$this->session->set_flashdata('success', 'Shedule Delete Successfully');
		redirect('admin/doctor_schedules');
	}
	/*Created by 95 for delete one shedule time slote of a date*/
	public function delete_shedule_time($id='')
	{
		$this->Common_model->DeleteOneSheduleTimeSlot($id);
		$this->session->set_flashdata('success', 'Time Slot Delete Successfully');
		redirect('admin/doctor_schedules');
	}


	/*Created by 95 on 20-2-2019 for edit doctor shedules*/
// public function edit_shedule($id)
// {
// 	$this->form_validation->set_rules('shedule_date', 'Shedule Date', 'required');
// 	if ($this->form_validation->run() == TRUE)
// 	{
// 		if($_POST['submit']){
// 		$var = $this->input->post('shedule_date');
// 		$shedule_date = date("Y-m-d", strtotime($var));
// 		$date_array=array(
// 			//'avdate_dr_id'=>$id,
// 			'avdate_date'=>$shedule_date,
// 			'avdate_created_date'=>date('Y-m-d H:i:s')
// 		);
// 	$this->Common_model->updateData('dr_available_date',$date_array,array('avdate_id' => $id));
// 			$this->session->set_flashdata('success', 'Successfully Updated');
// 	redirect('admin/doctor_shedules');
// 		}
// 	}
// 	$data['editData'] = $this->Common_model->getAllShedules($id);
// 	$this->template->set('title', 'Doctor Management');
// 	$this->template->load('admin_dashboard_layout', 'contents' , 'edit_shedule', $data);
// }

	/*Updated by 95 on 7-3-2019 for edit doctor shedules*/
	public function edit_shedule($id)
	{
		$data['menuactive'] = $this->uri->segment(2);
		$this->form_validation->set_rules('shedule_date', 'Shedule Date', 'required');
		if ($this->form_validation->run() == TRUE)
		{
			if($_POST['submit']){

				$doctor_id	= $this->input->post('doctor_id');
				$var = $this->input->post('shedule_date');
				$shedules = $this->input->post('shedules');
				$shedule_date = date("Y-m-d", strtotime($var));
				$result = $this->Common_model->checkDate($shedule_date,$doctor_id);

				if($result){

					if(!empty($result[0]['avdate_id'])){
						$avdate_id = $result[0]['avdate_id'];
					}
					$this->Common_model->delete('dr_available_time',array('avtime_date_id'=>$avdate_id));



					foreach ($shedules as $time){

						if($time=="06:00 AM" || $time=="07:00 AM" || $time=="08:00 AM" || $time=="09:00 AM" || $time=="10:00 AM" || $time=="11:00 AM"){
							$avtime_day_slot='Morning';
							$avtime_day_slot_id = 1;
						}else if($time=="06:00 PM" || $time=="07:00 PM" || $time=="08:00 PM" || $time=="09:00 PM" || $time=="10:00 PM" || $time=="11:00 PM"){
							$avtime_day_slot='Evening';
							$avtime_day_slot_id = 3;
						}else if($time=="12:00 PM" || $time=="01:00 PM" || $time=="02:00 PM" || $time=="03:00 PM" || $time=="04:00 PM" || $time=="05:00 PM"){
							$avtime_day_slot='Noon';
							$avtime_day_slot_id = 2;
						}


						$timeArray=array(
							'avtime_date_id'=>$avdate_id,
							'avtime_text'=>$time,
							'avtime_day_slot'=>$avtime_day_slot,
							'avtime_day_slot_id' => $avtime_day_slot_id,
							'avtime_created_date'=>date('Y-m-d H:i:s'),
						);


						$this->Common_model->insertDoctorTime($timeArray);
					}
				}else{

					$date_array=array(
						'avdate_date'=>$shedule_date,
						'avdate_created_date'=>date('Y-m-d H:i:s')
					);

					$where=array('avdate_id'=>$id,'avdate_dr_id'=>$doctor_id);

					$rows= $this->Common_model->updateOnlyDate($date_array,$where);

					$this->Common_model->delete('dr_available_time',array('avtime_date_id'=>$id));

					foreach ($shedules as $time){

						if($time=="06:00 AM" || $time=="07:00 AM" || $time=="08:00 AM" || $time=="09:00 AM" || $time=="10:00 AM" || $time=="11:00 AM"){
							$avtime_day_slot='Morning';
							$avtime_day_slot_id = 1;
						}else if($time=="06:00 PM" || $time=="07:00 PM" || $time=="08:00 PM" || $time=="9:00 PM" || $time=="10:00 PM" || $time=="11:00 PM"){
							$avtime_day_slot='Evening';
							$avtime_day_slot_id = 3;
						}else if($time=="12:00 PM" || $time=="01:00 PM" || $time=="02:00 PM" || $time=="03:00 PM" || $time=="04:00 PM" || $time=="05:00 PM"){
							$avtime_day_slot='Noon';
							$avtime_day_slot_id = 2;
						}

						$timeArray=array(
							'avtime_date_id'=>$id,
							'avtime_text'=>$time,
							'avtime_day_slot'=>$avtime_day_slot,
							'avtime_day_slot_id' => $avtime_day_slot_id,
							'avtime_created_date'=>date('Y-m-d H:i:s'),
						);
						$this->Common_model->insertDoctorTime($timeArray);
					}


				}


				$this->session->set_flashdata('success', 'Successfully Updated');
				redirect('admin/doctor_schedules');
			}
		}
		$data['editData'] = $this->Common_model->getAllShedules($id);
		$data['edittimeData'] = $this->Common_model->getAllShedulesTime($id);

		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(3);
		if ($this->access_permission) {
			$this->template->set('title', 'Doctor Management');
			$this->template->load('admin_dashboard_layout', 'contents', 'edit_shedule', $data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents', 'no_access', $data);
		}
	}



	/*Created by 95 on 20-2-2019 for edit doctor shedule time slots*/
	public function edit_shedule_time($id)
	{


		$time_id = $this->uri->segment(3);
		$date_id = $this->uri->segment(4);
		$doctor_id = $this->uri->segment(5);
		$data['check_time'] = $this->Common_model->getsingledoctorappoint('doctor_appointments',array('doctor_id' => $doctor_id,'dr_appointment_date_id'=>$date_id,'dr_appointment_time_id'=>$time_id));
		
		$data['menuactive'] = $this->uri->segment(2);
		$this->form_validation->set_rules('shedules', 'Shedule Time', 'required');
		if ($this->form_validation->run() == TRUE)
		{
			if($_POST['submit']){
				$sheduleTime = $this->input->post('shedules');
				$date_array=array(
					'avtime_text'=>$sheduleTime,
					'avtime_created_date'=>date('Y-m-d H:i:s')
				);
				$this->Common_model->updateData('dr_available_time',$date_array,array('avtime_id' => $id));
				$this->session->set_flashdata('success', 'Successfully Updated');
				redirect('admin/doctor_schedules');
			}
		}
		$data['TimeSlots'] = $this->Common_model->getSheduleTimes($id);
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(3);
		if ($this->access_permission) {
			$this->template->set('title', 'Doctor Management');
			$this->template->load('admin_dashboard_layout', 'contents', 'edit_time_slot',$data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents', 'no_access', $data);
		}
	}




	/*Created by 95 for show all the medicine shedule */
	public function medicine_shedules()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$data['medicineShedule'] = $this->Common_model->getAllMedicineShedule();
		
		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(3);
		if ($this->access_permission) {
			$this->template->set('title', 'Doctor Management');
			$this->template->load('admin_dashboard_layout', 'contents', 'medicine_shedule_list',$data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents', 'no_access', $data);
		}
	}


	/*Created by 95 for show medicine shedule*/
	public function view_medicine_shedule($id='')
	{
		$data['menuactive'] = $this->uri->segment(2);
		$data['medicine_shedule'] = $this->Common_model->getMedicineShedule($id);

		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(3);
		if ($this->access_permission) {
			$this->template->set('title', 'Doctor Shedules');
			$this->template->load('admin_dashboard_layout', 'contents', 'view_medicine_shedule',$data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents', 'no_access', $data);
		}
	}

	/*Created by 95 for delete medicine*/
	public function delete_medicine($id='')
	{
		$segment = $this->uri->segment('3');
		$this->Common_model->delete('medicine_schedule',array('medicine_id' => $segment));
		$this->session->set_flashdata('success', 'Successfully deleted');
		redirect('admin/medicine_shedules');
	}

	/*Created for show all appointments*/
	public function doctor_appointmensts()
	{
		$data['menuactive'] = $this->uri->segment(2);
		$next = "<i class='fa fa-angle-double-right' aria-hidden='true'></i>";
		$prev = "<i class='fa fa-angle-double-left' aria-hidden='true'></i>";
		$tot_row = $this->Common_model->getAllappointments();
		$data['total_count'] = $tot_row;

		$data['per_page1'] = 20;
		$config = array();
		$config["base_url"] = base_url() . "admin/doctor_appointmensts";
		$config["total_rows"] = count($tot_row);
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	  //echo $page;die;
		$user_appointments1 = $this->Common_model->getAllappointmentsnew($config["per_page"],$page);
		$data["links"] = $this->pagination->create_links();
	//echo $this->db->last_query();die;
		$data['user_appointments'] = $user_appointments1['rows'];

	//echo '<pre>'; print_r($data['user_appointments']);die;
		$data['doctor_name_data'] = $this->Common_model->getAllorderby('cp_doctor','doctor_id','desc');

		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(3);
		if ($this->access_permission) {
			$this->template->set('title', 'Doctor Management');
			$this->template->load('admin_dashboard_layout', 'contents', 'appointment_list',$data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents', 'no_access', $data);
		}
	}


	/*Created by 95 for show single appointment*/
	public function view_appointment($id='')
	{
		$data['menuactive'] = $this->uri->segment(2);
		$data['appointment'] = $this->Common_model->getAppointment($id);

		$data['staff_permission'] = $this->staff_permission;
		
		$this->permission_access(3);
		if ($this->access_permission) {
			$this->template->set('title', 'Doctor Management');
			$this->template->load('admin_dashboard_layout', 'contents', 'view_appointment',$data);
		} else {
			$this->template->set('title', 'Admin Panel - Staff');
			$this->template->load('admin_dashboard_layout', 'contents', 'no_access', $data);
		}
	}

	public function appointment_search()
	{
		$data_post['datepicker'] = $this->input->post("datepicker");
		$data_post['datepicker2'] = $this->input->post("datepicker2");
		$data_post['username'] = $this->input->post("username");
		$data_post['selector'] = $this->input->post("selector");
		$data_post['doctor_name'] = $this->input->post("doctor_name");

		$date = explode('/',$datepicker);
		$data_post['new_date'] = $date[2].'-'.$date[0].'-'.$date[1];

		$appointmentSearch = $this->Common_model->getDetailField(array("search" => $data_post));
		$data = '<table class="table table-hover table-bordered" >
		<thead>
		<tr>
		<th>S no.</th>
		<th>User Name</th>
		<th>Doctor Name</th>
		<th>Appointment Date</th>
		<th>Appointment Time</th>
		<th>Appointment Time Slot</th>
		<th>Action</th>
		</tr>
		</thead>
		<tbody >';
		$i=1;
		if(!empty($appointmentSearch)){
			foreach($appointmentSearch as $appoint_data)
			{
				$data .= '<tr>
				<td>'.$i++.'</td>
				<td>'.$appoint_data->username.'</td>
				<td>'.$appoint_data->doctor_name.'</td>

				<td>'.$appoint_data->avdate_date.'</td>
				<td>'.$appoint_data->avtime_text.'</td>

				<td>'.$appoint_data->avtime_day_slot.'</td>

				<td>
				<div class="link-del-view">
				<div class="tooltip-2">
				<a href='.base_url().'admin/view_appointment/'.$appoint_data->dr_appointment_id.'><i class="fa fa-eye" aria-hidden="true"></i></a>
				<span class="tooltiptext">View</span>
				</div>

				</div>
				</td>
				</tr>';
			}}else{
				$data .= '<tr><td colspan="7">No record found</td></tr>';
			}
			$data .= '</tbody>
			</table>

			</div>';

			echo $data;


		}

		public function schedule_search()
		{
			$data_post['datepicker'] = $this->input->post("datepicker");
			$data_post['datepicker2'] = $this->input->post("datepicker2");
			$data_post['doctor_name'] = $this->input->post("doctor_name");

			$scheduleSearch = $this->Common_model->getDetailFieldschedule(array("search" => $data_post));

			$data = '<table class="table table-hover table-bordered" >
			<thead>
			<tr>
			<th>S no.</th>
			<th>Shedule Dates</th>
			<th>Shedule Times</th>
			<th>Action</th>
			</tr>
			</thead>
			<tbody >';
			$i=1;
			if(!empty($scheduleSearch)){
				foreach($scheduleSearch as $schedule_data)
				{
					$data .= '<tr>
					<td>'.$i++.'</td>
					<td>'.date('d-m-Y',strtotime($schedule_data->avdate_date)).'</td>
					<td>'.$schedule_data->doctor_name.'</td>

					<td>
					<div class="link-del-view">
					<div class="tooltip-2">
					<a href='.base_url().'admin/view_shedule_times/'.$schedule_data->avdate_id.'><i class="fa fa-eye" aria-hidden="true"></i></a>
					<span class="tooltiptext">View</span>
					</div>

					</div>
					</td>
					</tr>';
				}}else{
					$data .= '<tr><td colspan="7"><center> No matching records found </center></td></tr>';
				}
				$data .= '</tbody>
				</table>

				</div>';

				echo $data;


			}

		}