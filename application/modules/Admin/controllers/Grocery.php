  <?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  class Grocery extends MX_Controller {
    private $staff_permission = array(); 
    private $access_permission = true; 
    public function __construct(){   
      parent:: __construct();
      $this->load->library('session');
      $this->load->library('form_validation');
      $this->load->model('grocery_model');
      $this->load->model('login_model');
      $this->load->helper(array('url'));
      $this->load->helper(array('form'));
      $user_id = $this->session->userdata('logged_ins')['id'];
      if(empty($user_id)) {
        redirect('admin/login');
      }

      if ($this->session->userdata('logged_ins')['user_role'] == 5) {
        $uid = $this->session->userdata('logged_ins')['id'];
        $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
        if (!empty($permission_list)) 
        {
           foreach ($permission_list as $k => $v) 
           {
              $this->staff_permission[$v['permission_id']] = $v;
           }
        }
      }
    }

    public function permission_access($permission_id=12)
    {
      if ($this->session->userdata('logged_ins')['user_role'] != 1) 
      {
         if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
         {
            $this->access_permission = false;
         }
      }
    }


    /*grocery SECTON*/

    /*Created by 95 for show all grocery*/
    public function grocery_list()
    {
     $data['menuactive'] = $this->uri->segment(2);
     $data['groceryData'] = $this->grocery_model->getAllgrocery();
     
     $data['staff_permission'] = $this->staff_permission;
     
     $this->permission_access(12);
     if ($this->access_permission) {
      $this->template->set('title', 'Grocery');
      $this->template->load('admin_dashboard_layout', 'contents', 'grocery_list', $data);
     } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }


   /*Created by 95 for delete bank*/
   public function delete_grocery($grocery_id='')
   {
    $this->grocery_model->delete('cp_grocery',array('grocery_id' =>$grocery_id));

    $this->grocery_model->delete('cp_fav_grocery',array('grocery_id' =>$grocery_id));
    $this->session->set_flashdata('success', 'Deleted successfully.');
    redirect('admin/grocery_list');
  }

  /*View single bank*/
  public function view_grocery($Grocery_id='')
    {
     $data['viewGrocery']=$this->grocery_model->getGrocery($Grocery_id);
     
     $data['staff_permission'] = $this->staff_permission;
     
     $this->permission_access(12);
     if ($this->access_permission) {
      $this->template->set('title', 'Grocery');
      $this->template->load('admin_dashboard_layout', 'contents', 'view_grocery', $data);
     } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }




/*FAVOURITE grocery SECTON*/

  /*Created by 95 for show all favourite grocery*/
    public function favourite_grocery_list()
    {

     $data['menuactive'] = $this->uri->segment(2);
     $data['favgroceryData'] = $this->grocery_model->getAllfavourite_Grocery_list();
     
     $data['staff_permission'] = $this->staff_permission;
     
     $this->permission_access(12);
     if ($this->access_permission) {
      $this->template->set('title', 'Grocery');
      $this->template->load('admin_dashboard_layout', 'contents' , 'favourite_grocery_list', $data);
     } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }


   /*Created by 95 for delete favourite grocery*/
   public function delete_favourite_grocery($fav_grocery_id='')
   {
    $this->grocery_model->delete('cp_fav_grocery',array('fav_grocery_id' =>$fav_grocery_id));
    $this->session->set_flashdata('success', 'Deleted successfully.');
    redirect('admin/favourite_grocery');
  }

  /*View single favourite grocery*/
  public function view_favourite_grocery($fav_grocery_id='')
  {
     $data['menuactive'] = $this->uri->segment(2);
     $data['viewfavgrocery']=$this->grocery_model->getFavourite_Grocery($fav_grocery_id);
     
     $data['staff_permission'] = $this->staff_permission;
     
     $this->permission_access(12);
     if ($this->access_permission) {
      $this->template->set('title', 'Grocery');
      $this->template->load('admin_dashboard_layout', 'contents', 'view_favourite_grocery', $data);
     } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
  }



  }