<!-- jQuery UI 1.10.3 -->

  <style>

    #flupmsg .red {

	  color:red;	 

	 }

	 

	 #flupmsgs .red1 {

	  color:red;	 

	 } 

	 div#video_loader {

    position: fixed;

    top: 0;

    bottom: 0;

    left: 0;

    right: 0;

    background-color: #0a0a0aa6;

    z-index: 999999;

    text-align: center;

    padding-top: 23%;

}

div#video_loader img#loading_img {

    width: 70px !important;

}

  </style>      

<aside class="right-side">    

    <section class="content-header no-margin">

        <h1 class="text-center">

          <!-- <i class="fa fa-paper-plane"></i>  -->

         <i class="fa fa-music" aria-hidden="true"></i>

          Edit Music

    <div class="back_reminder"><a href="<?php echo base_url(); ?>music_list" class="btn btn-danger">Back</a></div>

    </h1>

    </section>

	<div id="image_msg"></div>

<div id="loders"></div>

    <?php //echo "<pre>"; print_r($singleData); echo "<pre>"; ?>    

    <section class="content photo-list">

        <div class="photo-listMain">            

          <form id="edit_music" enctype="multipart/form-data" method="post" class="frm_add">

            <div class="row">

                    <div class="col-sm-4 col-md-4">                        

                      <div class="form-group">

                          <label>Music Title<font style="color:red;">*</font>:</label>

                          <input type="text" placeholder="Music Title" class="form-control" name="music_title" id="music_title" value="<?php echo $singleData->music_title; ?>">

				

                          <?php echo form_error('music_title'); ?>

                      </div>                                  

                    </div>



                     <div class="col-sm-4 col-md-4">                        

                      <div class="form-group">

                          <label>Music Type<font style="color:red;">*</font>:</label>

                          <select name="music_type" type="text" class="form-control" >

					<option value="">Select</option>

					<?php foreach($category_name as $category){ ?>

					  <option value="<?php echo $category->music_category_id; ?>" <?php if($category->music_category_id == $singleData->music_type){  echo 'selected'; }?> ><?php echo $category->music_category_name; ?></option>

					<?php } ?>

				</select>

				<?php echo form_error('music_type'); ?>	

                      </div>                                  

                    </div>





<input type="hidden" id="music_id" name="music_id" value="<?php echo $singleData->music_id; ?>" >

                 



 <div class="col-sm-4 col-md-4">

                      <div class="form-group">

                          <label>Music Artist<font style="color:red;">*</font>:</label>

                          <input name="music_artist" type="text" placeholder="Music Artist" class="form-control"  id="" value="<?php echo $singleData->music_artist; ?>">

				<?php echo form_error('music_artist'); ?>

                      </div>               

                    </div>				 

                    

                </div>

				

				<div class="row">

                   





                   

                    <div class="col-sm-6 col-md-6">                        

                      <div class="form-group">

                          <label>Music File<font style="color:red;">*</font>:</label>

                          <input type="file" id="profile_imagess" class="form-control" name="picture" onchange="loadFile1(event)">

				<div id="flupmsg"></div>

                      </div> 

<span id="demo1" style="color: red"></span>

<div id="progress-wrp" style="display:none"><div class="progress-bar"></div ><div class="status">0%</div></div>

<?php if(!empty($singleData->music_file)){ ?>

<audio controls="controls" id="dfds" > <source src= "<?php echo base_url(); ?>uploads/music/<?php echo $singleData->music_file; ?>" type="audio/mpeg" > </source> </audio>	

<?php } ?>				  

                    </div>

					

					 <div class="col-sm-6 col-md-6">                        

                      <div class="form-group">

                          <label>Music Image<font style="color:red;">*</font>:</label>

                          <input type="file" id="profile_images" class="form-control" name="pictureFile" onchange="loadFile(event)">

				<div id="flupmsgs"></div>

                      </div> 

<span id="demo" style="color: red"></span>

<?php if(!empty($singleData->music_image)){ ?>

	<div class="bx_upImg">

<img id='img-upload' src="<?php echo base_url().'uploads/music/image/'.$singleData->music_image; ?>">

</div>

<?php } ?>				  

                    </div>

                    

                </div>

				

				<div class="row">

					 <div class="col-sm-12 col-md-12">

                      <div class="form-group">

                          <label>Music Description<font style="color:red;">*</font>:</label>

                          <textarea name="music_description" id="" class="form-control" placeholder="Music Description" ><?php echo $singleData->music_desc; ?></textarea>

				<?php echo form_error('music_description'); ?>

                      </div>               

                    </div>

                    





                   

                    

                </div>

                

              

                <div class="">

                    <input type="submit" name="submit" class="btn btn-primary" value="Edit Music" onclick="showFileSize()" >

                </div>               

            </form>              

        </div>

    </section>

</aside>

<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script> 

  <script type="text/javascript">

   $("#edit_music").on('submit',(function(e) 

	{  

e.preventDefault();

	var isvalidate=$("#edit_music").valid();

	 if(isvalidate)

            {

				

	  $("#loders").append('<div id="video_loader" class="loading_class"><img id="loading_img" style="width:20px" src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif" /></div>');

	  

           	

	  $.ajax({

                url : "<?php echo base_url(); ?>Pages/Music/ajax_music_edit",

              type: "POST",             

			data: new FormData(this), 

			dataType: "json",

			contentType: false,       

			cache: false,             

			processData:false,        

			success: function(data) {    

				$("#loading_img").hide();

				$("#video_loader").hide();

				      if(data.code == 1){

						  $("#loading_img").hide();

						  $("#video_loader").hide();

						  $("#sub").prop('disabled', true);

						  

						 $("#image_msg").html('<div class="alert alert-success">'+data.message+'<div>');

						   setTimeout(function(){

			

			

            $("#image_msg").html(""); 

            //location.reload();

window.location.href = '<?php echo base_url(); ?>music_list';

}, 1000);

					  }else{ 

                          $("#loading_img").hide();

						$("#video_loader").hide();

						

						  $("#image_msg").html('<div class="alert alert-danger">'+data.message+'<div>');

						 

					  }

				   }

				});

		

			}

  }));







    function loadFile (event) {

		var pcFile = $('#profile_images').val().split('\\').pop();

		var pcExt     = pcFile.split('.').pop();

		var output = document.getElementById('img-upload');

		if(pcExt == "png" || pcExt == "PNG" || pcExt == "jpg" || pcExt == "JPG" || pcExt == "JEPG"

			|| pcExt == "jpeg" || pcExt == "gif" || pcExt == "GIF"){

			$('#img-upload').show();

		output.src = URL.createObjectURL(event.target.files[0]);

		$('#flupmsgs').html('');

		$('.red1').html('');

		$('#demo').html('');

	}else{

		$('#demo').html('');

		$('#flupmsgs').html('<div class="red1">Please select only Image file.</div>');

		$('#img-upload').hide();

	}

};





function loadFile1 (event) {

		var pcFile = $('#profile_imagess').val().split('\\').pop();

		var pcExt     = pcFile.split('.').pop();

		var output = document.getElementById('img-uploads');

		if(pcExt == "mp3"){

			$('#img-uploads').show();

			$('.red').html('');

		$('#flupmsg').html('');

		

			

		$('#demo1').html('');

	}else{

		$('#demo1').html('');

		$('#dfds').hide();

		$('#flupmsg').html('<div class="red">Please select only audio file.</div>');

		$('#img-uploads').hide();

	}

};





$(document).ready(function() {

		$.validator.addMethod("regx", function(value, element, regexpr) {          

			return regexpr.test(value);

		}, "Please upload valid extension mp3 file."); 

		$('#edit_music').validate({

			rules: {

				music_title: 'required',

				music_description:'required',

				music_artist:'required',

				music_type:'required',				

				

			},

			messages: {

				music_title: 'Please enter music title.',

				music_description:'Please enter music description.',

				music_artist:'Please enter an artist name.',

				music_type:'Please select a music type.',				

				

			}

		});

	});



		$(document).ready(function(){

	var progress_bar_id 		= '#progress-wrp';

        $('#profile_imagess').change(function(e){

		var pcFile = $('#profile_imagess').val().split('\\').pop();

		var pcExt     = pcFile.split('.').pop();

		var output = document.getElementById('img-uploads');

		if(pcExt == "mp3"){	

			$('#progress-wrp').show();

			

$(progress_bar_id +" .progress-bar").css("width", "0%");

	$(progress_bar_id + " .status").text("0%");

           // var file = this.files[0];

			var file = this.files[0];

            var form = new FormData();

            form.append('360_image_upload', file);	

			

            $.ajax({

                url : "<?php echo base_url(); ?>Pages/Music/image_upload",

                type: "POST",

                cache: false,

                contentType: false,

                processData: false,

                data : form,

				dataType: "json",

				xhr: function(){

		//upload Progress

		var xhr = $.ajaxSettings.xhr();

		if (xhr.upload) {

			xhr.upload.addEventListener('progress', function(event) {

				var percent = 0;

				var position = event.loaded || event.position;

				var total = event.total;

				if (event.lengthComputable) {

					percent = Math.ceil(position / total * 100);

				}

				//update progressbar

				$(progress_bar_id +" .progress-bar").css("width", + percent +"%");

				$(progress_bar_id + " .status").text(percent +"%");

			}, true);

		}

		return xhr;

	},

                success: function(data){                 

				

				

                 $("#progress-wrp").hide();	 

	             $("#dfds").show();

	          var fileInput = document.getElementById('profile_imagess');

   var fileUrl = window.URL.createObjectURL(fileInput.files[0]);

   $("#dfds").attr("src", fileUrl);

                }

            });

		}

        });

    });

	

	







function showFileSize(){

		var img  = document.getElementById('profile_imagess').value;



		var img1  = document.getElementById('profile_images').value;



<?php if(empty($singleData->music_file)){ ?>

		if(img==='' || img===null){

			document.getElementById("demo1").innerHTML="Please choose a audio file.";

			

		}else{

			document.getElementById("demo1").innerHTML=" ";	

		}



<?php } ?>

<?php if(empty($singleData->music_image)){ ?>

		if(img1==='' || img1===null){

			document.getElementById("demo").innerHTML="Please choose a Image file.";

			

		}else{

			document.getElementById("demo").innerHTML=" ";	

		}



<?php } ?> 

		

	}



</script>





<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>







    





    



                