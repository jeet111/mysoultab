<style>

form#add_medicine .btn {
    max-width: 150px !important;
}
</style>
<div class=" text_heading">

        <h1 class="text-center"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Edit Medicine

		<div class="back_reminder"><a href="javascript:window.history.back()" class="btn btn-primary">Back</a></div>

		</h1>

</div>

    <section class="content photo-list">

        <div class="photo-listMain">



            <?php if ($this->session->flashdata('success')) { ?>

                    <div class="alert alert-success message">

                        <button type="button" class="close" data-dismiss="alert">x</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                <?php }?>

   

<?php //echo "<pre>"; print_r($MedicineData);echo "</pre>"; ?>

     

            <form role="form" class="frm_add" id="add_medicine" method="post">

                <div class="row"> 

                    <div class="col-sm-6 col-md-6">

                        <div class="form-group">

                            <label for="">Medicine Name<font style="color: red">*</font></label>

                            <input type="text" name="medicine_name" class="form-control" id="" placeholder="Medicine Name" value="<?php echo $MedicineData->medicine_name; ?>">

                        <?php echo form_error('medicine_name'); ?>

                        </div>

                    </div>

                    <div class="col-sm-6 col-md-6">

                        <div class="form-group">

                            <label for="">How Many Days Will Take Medicine<font style="color: red">*</font></label>

                            <input type="text" name="no_of_days" class="form-control" id="" placeholder="Number of days" value="<?php echo $MedicineData->medicine_take_days; ?>">

                            <?php echo form_error('no_of_days'); ?>

                        </div>

                    </div> 



                    <div class="col-sm-6 col-md-6">

                        <div class="form-group">

                            <label for="">Doctor Name<font style="color: red">*</font></label>

                            <select name="doctor_name" class="form-control" >

                            <option value="">Select Doctor</option>

                            <?php foreach ($Doctors as $doctor) {?>

                             <option  value="<?php echo $doctor->doctor_id; ?>" <?php if($doctor->doctor_id==$MedicineData->doctor_id){ echo 'selected="selected"';} ?>><?php echo $doctor->doctor_name; ?> </option>   



                            <?php } ?>

                            </select>

                            <?php echo form_error('doctor_name'); ?>

                            

                        </div>

                    </div>

					<?php  //echo '<pre>'; print_r($MedicineData); ?>

                    <div class="col-sm-6 col-md-6">

                        <div class="form-group">

                            <label for="">Time To Take Medicine<font style="color: red">*</font></label>

							

                            <select name="medicine_time[]" id="medicine_time" class="form-control" multiple="multiple">

                        <?php 

                      

                         $time_slot = explode(',',$MedicineData->medicine_time);  

                    

                             ?>

                            



                             <option value="06:00 AM" <?php foreach ($time_slot as $value){ if($value=='06:00 AM') echo 'selected'; }?>>06:00 AM</option>

                <option value="07:00 AM" <?php foreach ($time_slot as $value){ if($value=='07:00 AM') echo 'selected'; }?>>07:00 AM</option>

                <option value="08:00 AM" <?php foreach ($time_slot as $value){ if($value=='08:00 AM') echo 'selected'; }?>>08:00 AM</option>

                <option value="09:00 AM" <?php foreach ($time_slot as $value){ if($value=='09:00 AM') echo 'selected'; }?>>09:00 AM</option>

                <option value="10:00 AM" <?php foreach ($time_slot as $value){ if($value=='10:00 AM') echo 'selected'; }?>>10:00 AM</option>

                <option value="11:00 AM" <?php foreach ($time_slot as $value){ if($value=='11:00 AM') echo 'selected'; }?>>11:00 AM</option>

                <option value="12:00 PM" <?php foreach ($time_slot as $value){ if($value=='12:00 PM') echo 'selected'; }?>>12:00 PM</option>

                <option value="01:00 PM" <?php foreach ($time_slot as $value){ if($value=='01:00 PM') echo 'selected'; }?>>01:00 PM</option>

                <option value="02:00 PM" <?php foreach ($time_slot as $value){ if($value=='02:00 PM') echo 'selected'; }?>>02:00 PM</option>

                <option value="03:00 PM" <?php foreach ($time_slot as $value){ if($value=='03:00 PM') echo 'selected'; }?>>03:00 PM</option>

                <option value="04:00 PM" <?php foreach ($time_slot as $value){ if($value=='04:00 PM') echo 'selected'; }?>>04:00 PM</option>

                <option value="05:00 PM" <?php foreach ($time_slot as $value){ if($value=='05:00 PM') echo 'selected'; }?>>05:00 PM</option>

                <option value="06:00 PM" <?php foreach ($time_slot as $value){ if($value=='06:00 PM') echo 'selected'; }?>>06:00 PM</option>

                <option value="07:00 PM" <?php foreach ($time_slot as $value){ if($value=='07:00 PM') echo 'selected'; }?>>07:00 PM</option>

                <option value="08:00 PM" <?php foreach ($time_slot as $value){ if($value=='08:00 PM') echo 'selected'; }?>>08:00 PM</option>

                <option value="09:00 PM" <?php foreach ($time_slot as $value){ if($value=='09:00 PM') echo 'selected'; }?>>09:00 PM</option>

                <option value="10:00 PM" <?php foreach ($time_slot as $value){ if($value=='10:00 PM') echo 'selected'; }?>>10:00 PM</option>

                <option value="11:00 PM" <?php foreach ($time_slot as $value){ if($value=='11:00 PM') echo 'selected'; }?>>11:00 PM</option>

</select>

                            <!-- <option value="Morning" <?php foreach ($time_slot as $value){ if($value=='Morning') echo 'selected'; }?>>Morning</option>

                            <option value="Noon" <?php foreach ($time_slot as $value){ if($value=='Noon') echo 'selected'; }?>>Noon</option>

                            <option value="Evening" <?php foreach ($time_slot as $value){ if($value=='Evening') echo 'selected'; }?>>Evening</option>

                            <option value="Night" <?php foreach ($time_slot as $value){ if($value=='Night') echo 'selected'; }?>>Night</option>

                        </select> -->

                            <!-- <input type="text" name="medicine_time" class="form-control" id="" placeholder="Morning or evening" value="<?php echo $MedicineData->medicine_time; ?>"> -->

                            <?php echo form_error('medicine_time'); ?>

                        </div>

                    </div> 



                </div>

                <div class="row">





                    <div class="col-sm-6 col-md-6">

                        <div class="form-group">

                            <label for="">Reminder</label>

                        <input type="checkbox" name="remind" value="1" <?php echo ($MedicineData->medicine_reminder=='1')?'checked':''?> data-toggle="toggle" data-style="ios" >

                    </div>

                    </div>





                    <!-- <div class="col-md-6">

                        <div class="bootstrap-timepicker">

                            <div class="form-group">

                                <label>Date:<span class="red_star">*</span></label>

                                <div class="input-group date costom_err" id="datetimepicker8">

                                    <input type="text" name="reminder_date" class="form-control valid" value="" placeholder="Reminder Date">

                                    <span class="input-group-addon">

                                        <span class="glyphicon-calendar glyphicon"></span>

                                    </span>                

                                </div>

                            </div>

                        </div>                      

                    </div>  -->

                    <!-- <div class="col-md-6">

                        <div class="bootstrap-timepicker">

                            <div class="form-group">

                                <label>Time:<span class="red_star">*</span></label>

                                <div class="input-group" id="datetimepicker3">                                   

                                    <input type="text" data-format="hh:mm:ss" name="reminder_time" class="form-control valid" value="" placeholder="Reminder Time">

                                    <span class="input-group-addon add-on">

                                        <span class="glyphicon glyphicon-time"></span>

                                    </span>

                                </div>                              

                            </div>

                        </div>                                    

                    </div> -->                    

                </div>               

                <div class="box-footer">

                    <button type="submit" name="msubmit" class="btn btn-primary"><i class="fa fa-plus"></i> Add Medicine</button>

                    <!-- <button type="submit" class="btn btn-primary"><i class="fa fa-clock-o"></i> Set Reminder</button> -->

                </div>

            </form>              

        </div>

    </section>

</aside>

<script type="text/javascript">

   $('.datepicker').datepicker({

    format: 'mm/dd/yyyy',

    startDate: '-3d'

    });

    $(function() {

    $('#datetimepicker3').datetimepicker({

      pickDate: false

    });

  });

</script>



<!--Added by 95 on 26-2-2019 For Doctor category for validation-->

<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>

  <script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

  <script type="text/javascript">



    $(document).ready(function() {

    $("#add_medicine").validate({

            rules: {

                medicine_name: "required",

                 no_of_days: "required",

                 doctor_name: "required",

                 medicine_time: "required",

             },

        

            messages: {

                medicine_name:"Please enter medicine name.",

                no_of_days:"Please enter number of days.",

                doctor_name:"Please select a doctor.",

                medicine_time:"Please enter medicine time.",

            },

        });



    $('#medicine_time').multiselect({

            includeSelectAllOption: true,

            allSelectedText: 'No option left ...',

            maxHeight: 200

        });

      

    });

</script>





<!-- Added by 95 for toggle reminder check box start -->

<link href="<?php echo base_url(); ?>assets/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="<?php echo base_url(); ?>assets/js/bootstrap-toggle.min.js"></script>



<style>

  .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }

  .toggle.ios .toggle-handle { border-radius: 20px; }

</style>



<!-- Added by 95 for toggle reminder check box end -->



<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>



<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>





