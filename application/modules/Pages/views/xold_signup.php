

<h2 class="form-title rig_tx">Register Information</h2> 

<?php

if($this->session->flashdata('success')) {

 $message = $this->session->flashdata('success');

 ?>

 <div style="color:green;text-align: center;"><?php echo $message['message']; ?></div>

<?php } ?>





<form method="POST" id="signupform" class="signup-form" action="<?php echo base_url();?>signup/<?php echo base64_encode($plan_id); ?>" enctype="multipart/form-data">

  <input type="hidden" name="plan_id" value="<?php echo $plan_id; ?>">

  <label for="fname">First name:</label>



  <input type="text" class="form-input" name="firstname" id="firstname" placeholder="Enter your first name" value="<?php echo set_value('firstname'); ?>" onkeypress="return onlyAlphabets(event,this);"/>



  <?php echo form_error('firstname'); ?>

  <!-- <span id="errnamemsg"></span> -->

  <br>



  <label for="fname">Last name:</label>

  <input type="text" class="form-input" name="lastname" id="lastname" placeholder="Enter your last name" value="<?php echo set_value('lastname'); ?>" onkeypress="return onlyAlphabets(event,this);"/>



  <?php echo form_error('lastname'); ?>







  <br>



  <label for="fname">Address:</label>

  <input type="text" class="form-input" name="address" id="address" placeholder="Enter your address" value="<?php echo set_value('address'); ?>" />



  <?php echo form_error('address'); ?>







  <br>

  <label for="fname">City:</label>

  <input type="text" class="form-input" name="city" id="city" placeholder="City" value="<?php echo set_value('city'); ?>" />

  

  <?php echo form_error('city'); ?>

  <br>



  <label for="fname">State:</label>

  <input type="text" class="form-input" name="state" id="state" placeholder="State" value="<?php echo set_value('state'); ?>" />

  

  <?php echo form_error('state'); ?>

  <br>

  <label for="fname">Zipcode:</label>

  <input type="text" class="form-input" name="zipcode" id="zipcode" placeholder="Zip code" value="<?php echo set_value('zipcode'); ?>" />

  

  <?php echo form_error('zipcode'); ?>

  <br>



  <label for="fname">Email:</label>

  <input type="email" class="form-input" name="email" id="email" placeholder="Enter your email" value="<?php echo set_value('email'); ?>" />



  <?php echo form_error('email'); ?>

  <br>



  <label for="fname">Area Code:</label>

  <input type="text" class="form-input" name="area_code" id="area_code" placeholder="Area code" value="<?php echo set_value('area_code'); ?>" />



  <?php echo form_error('area_code'); ?>

  <span id="errmsgarea"></span>

  <br>



  <label for="fname">Mobile:</label>

  <input type="text" class="form-input" name="mobile" id="mobile" placeholder="Enter your phone number" value="<?php echo set_value('mobile'); ?>" />



  <?php echo form_error('mobile'); ?>



  <span id="errmsg"></span>

  <br>

  <label for="fname">Company / School:</label>

  <input type="text" class="form-input" name="company_school" id="company_school" placeholder="Company/ School" value="<?php echo set_value('company_school'); ?>" />



  <?php echo form_error('company_school'); ?>

  <br>

  <label for="fname">Password:</label>

  <input type="password" class="form-input" name="password" id="password" placeholder="Enter your password" value="<?php echo set_value('password'); ?>" />

  <span toggle="#password" class="toggle-password"></span>

  

  <?php echo form_error('password'); ?>



  <br>

  <label for="fname">Confirm password:</label>

  <input type="password" class="form-input" name="re_password" id="re_password" placeholder="Repeat your password" value="<?php echo set_value('re_password'); ?>" />

  <span toggle="#password" class="toggle-password"></span>

  

  

  <?php echo form_error('re_password'); ?>

  <br>

  <!-- <label for="fname">First name:</label> -->

  <input type="checkbox" name="agreeterm" id="agreeterm" class="agree-term" value="1" />

  <label for="agree-term" class="label-agree-term"><span>I agree all statements in <a href="term_and_condition" class="term-service">Terms of service</a></span></label>



  

  <?php echo form_error('agree-term'); ?>



  <br>

  <!-- <label for="fname">First name:</label> -->



  <div class="loader-image" id="loading2" style="display:none;height: auto;width: auto;">

    <img src="<?php echo base_url('assets/images/ajax-loader-blue.gif') ?>" alt="" />

  </div>

  <br>

  <!-- <label for="fname">First name:</label> -->



  <input type="submit" name="nnsubmit" id="btnsubmit" class="form-submit" value="Continue"/>

  <br>





  <!-- <input type="submit" value="Submit"> -->

</form>



<p class="loginhere">

  Already have an account ? <a href="login" class="loginhere-link">Login here</a>

</p>







<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>

<script type="text/javascript">

 // $(function() {

 //  $.addMethod("pwcheck", function (value) { return /[\@\#\$\%\^\&\*\(\)\_\+\!]/.test(value) && /[a-z]/.test(value) && /[0-9]/.test(value) && /[A-Z]/.test(value) });

 // //  $.validator.addMethod("useridmatch", function (value){

 // //   return  /[a-zA-Z]/.test(value) && /[0-9]/.test(value)

 // // });



 $("#signupform").validate({

           // Specify validation rules

           rules: {

            // username:{

            //   required: true,

            //   maxlength: 30,

            // },

            firstname:{

              required: true

            },

            lastname:{

              required: true

            },

            // gender:{

            //   required: true

            // },

            address:{

              required: true

            },



            city:{

              required: true

            },





            state:{

              required: true

            },



            zipcode:{

              required: true

            },





            area_code:{

              required: true

            },





            company_school:{

              required: true

            },









            // dob:{

            //   required: true

            // },

            // user_id: {

            //   required: true,

            //   maxlength: 40,

            //   minlength: 6,

            //   useridmatch:true,

            //      //useridmatch:true

            //      remote: {

            //       url: "<?php echo base_url(); ?>Pages/Login/register_username_exists",

            //       type: "post",

            //       data: {

            //         //email: function(){ return $("#username").val(); }

            //       }

            //     }

            //   },

            email: {

             required: true,

             email: true,

             maxlength: 40,

             

             remote: {

              url: "<?php base_url(); ?>validatedata",

              type: "post",



            }

          },

          mobile:{

           required:true,

           number:true,

           minlength: 10,

           maxlength: 14,

         },

         password: {

           required: true,

           minlength: 6,

           maxlength: 30,

           pwcheck:true

         },

         re_password : {

           required:true,

           minlength : 6,

           maxlength: 30,

           equalTo : "#password"

         },

              //user_role : {

              //required:true

              //},

              agreeterm : {

                required:true

              }

            },

           // Specify validation error messages

           messages: {

            username: {

              required:"Please enter your user name.",

              maxlength: "Please enter maximum 30 characters."

            },

            // user_id: {

            //   required: "Please enter your user id ",

            //   remote: 'This userid is already exit, please login.',

            //   minlength:'Please enter at least 6 characters.',

            //   useridmatch:'Please enter number and words only.',

            // },



            city:{

              required: "Please enter city.",

            },





            state:{

              required: "Please enter state.",

            },



            zipcode:{

              required: "Please enter zipcode.",

            },





            area_code:{

              required: "Please enter area code.",

            },





            company_school:{

              required: "Please enter company/school name.",

            },





            firstname:{

              required: "Please enter first name.",

            },

            lastname:{

              required: "Please enter last name.",

            },



            email: {

              required: "Please enter your email.",

              email: "Please enter valid email.",

              remote: "Email id is already exist, please login.",

              maxlength: "Please enter maximum 40 characters.",

            },



            // email: {

            //   remote: "Email id is already exist, please login.",

            // },



            mobile:{

              required:"Please enter your phone number.",

              number:"Please enter only number",

              minlength: "Please enter at least 10 number.",

              maxlength: "Please enter maximum 14 number.",

            },

            address:{

              required: "Please enter address",

            },

            password: {

             required: "Please enter your password.",

             minlength: "Password must be at least 6 characters.",

             maxlength: "Password at maximum 30 characters.",

             pwcheck:"Must contain at least one number and one uppercase and lowercase and  punctuation mark letter, and at least 8 or more characters."

           },

           re_password: {

             required: "Please enter your confirm password.",

             equalTo: "Not match your confirm password.",

           },

              //user_role: {

              //required: "Please select your user type.",

              //},

              agreeterm: {

                required: "Please check terms and conditions.",

              }

            },

           // Make sure the form is submitted to the destination defined

           // in the "action" attribute of the form when valid

           submitHandler: function(form) {

            form.submit();

            document.getElementById('btnsubmit').style.display = 'none';

            document.getElementById('loading2').style.display = 'block';

            return false;

          }

        });

  //});

  $(document).ready(function () {

        //called when key is pressed in textbox

        $("#mobile").keypress(function (e) {

           //if the letter is not digit then display error and don't type anything

           if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {

              //display error message

              //alert('sdfsdf');

              $("#errmsg").html("Please enter only number").show().fadeOut("slow");

              return false;

            }

          });

        // $(function(){

        //   $("#username").lettersOnly();

        // });

      });





  $(document).ready(function () {

        //called when key is pressed in textbox

        $("#area_code").keypress(function (e) {

           //if the letter is not digit then display error and don't type anything

           if (e.which != 8 && e.which != 43 && e.which != 0 && (e.which < 48 || e.which > 57)) {

              //display error message

              //alert('sdfsdf');

              $("#errmsgarea").html("Please enter valid area code").show().fadeOut("slow");

              return false;

            }

          });

        // $(function(){

        //   $("#username").lettersOnly();

        // });

      });

    </script>

    <script language="Javascript" type="text/javascript">

     //function onlyAlphabets(e, t) {



      $('#firstname').keypress(function (e) {

        var regex = new RegExp("^[a-zA-Z ]+$");

        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);

        if (regex.test(str)) {

          return true;

        }

        else

        {

          e.preventDefault();

          alert('Please Enter Alphabate');

          return false;

        }

      });

    //   try {

    //    if (window.event) {

    //     var charCode = window.event.keyCode;

    //   }

    //   else if (e) {

    //     var charCode = e.which;

    //   }

    //   else { return true; }

    //   if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode ==32))

    //     return true;

    //   else

    //     return false;

    // }

    // catch (err) {

    //   alert('sdf');

    //   //alert(err.Description);

    // }

  //}

</script>