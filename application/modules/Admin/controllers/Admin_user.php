<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_user extends MX_Controller {

 private $staff_permission = array();
 private $access_permission = true;
 public function __construct(){

  parent:: __construct();
  $this->config->load('ga_api');
  $this->load->library('session');
  $this->load->library('form_validation');
  $this->load->model('login_model');
  $this->load->model('Common_model');
  $this->load->helper(array('url'));
  $this->load->helper(array('form'));

  $user_id = $this->session->userdata('logged_ins')['id'];
  if(empty($user_id)) {
    redirect('admin/login');
  }

  if ($this->session->userdata('logged_ins')['user_role'] == 5) {
    $uid = $this->session->userdata('logged_ins')['id'];
    $permission_list = $this->login_model->getAllRecordsById('staff_permission',array('user_id'=>$uid));   
    
    // echo "<pre>";
    // print_r($permission_list);
    // die;

    if (!empty($permission_list)) 
    {

      foreach ($permission_list as $k => $v) 
      {
       $this->staff_permission[$v['permission_id']] = $v;
     }
   }
 }



}

public function permission_access($permission_id=1)
{
  if ($this->session->userdata('logged_ins')['user_role'] != 1) 
  {
   if ($this->staff_permission[$permission_id]['view_permission'] != 1 && $this->staff_permission[$permission_id]['add_permission'] != 1 && $this->staff_permission[$permission_id]['edit_permission'] != 1 && $this->staff_permission[$permission_id]['delete_permission'] != 1) 
   {
    $this->access_permission = false;
  }
}
}


  /*function index() {

    $data = array();

    if($this->session->userdata('isUserLoggedIn')) {

      $this->dashboard();

    } else {

      redirect(base_url().'admin');

    }

  }*/


  /*
   * Admin Dashboard
   */
  public function dashboard() {

    //die('tyiutiuiyut');


    $data = array();
    $data['menuactive'] = $this->uri->segment(2);

    //$data['menuactive_add'] = $this->uri->segment(3);

    $con['conditions'] = array(
      'user_role '=> 2,
      'status !=' => 3
    );
    $con['returnType'] = 'count';
    $data['user_count'] = $this->login_model->getRows('cp_users',$con);

    $conf['conditions'] = array(
      'user_role'=> 3,
      'status !=' => 3
    );
    $conf['returnType'] = 'count';
    $data['family_count'] = $this->login_model->getRows('cp_users',$conf);
    //print_r($data['family_count']);

    $conc['conditions'] = array(
      'user_role '=> 4,
      'status !=' => 3
    );
    $conc['returnType'] = 'count';
    $data['care_giver_count'] = $this->login_model->getRows('cp_users',$conc);

    $conp['conditions'] = array(
      'u_photo_status !=' => 3
    );
    $conp['returnType'] = 'count';
    $data['photos_count'] = $this->login_model->getRows('cp_user_photo',$conp);

    $udata['conditions'] = array(
      'user_role '=> 2,
      'status !=' => 3
    );
    $udata['sorting'] = array(
      'order_by'=> 'create_user',
    );
    $udata['limit'] = 10;
    $data['user_data'] = $this->login_model->getRows('cp_users',$udata);
    
    // echo "<pre>";
    // print_r($this->staff_permission);
    // die;

    $staff_permission= $this->staff_permission;

    if(!empty($staff_permission)){
      $data['staff_permission']=$staff_permission;
    }else{
      $data['staff_permission']=array();
    }

    //echo "<pre>";print_r($data['staff_permission']);die;
    // echo $this->permission_access(1);
    // die; 
    $this->permission_access(1);
    if ($this->access_permission) {
      $this->template->set('title', 'Admin Panel');
      $this->template->load('admin_dashboard_layout', 'contents' , 'dashboard', $data);
    } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
    }
    

  }



  public function SetDate()
  {
    $data['menuactive'] = $this->uri->segment(2);

    if(isset($_POST['submit'])){
      $shedule_date = $this->input->post('shedule_date');
      $setTime = $this->input->post('setTime');

      if($setTime=='12 am'){
       $set_time=  date("h:i a", strtotime($setTime));;

     }elseif($setTime=='04 am'){
       $set_time=  date("h:i a", strtotime($setTime));
       
     }else if($setTime=='08 am'){

       $set_time=  date("h:i a", strtotime($setTime));
       
     }else if($setTime=='12 pm'){

      $set_time=  date("h:i a", strtotime($setTime));

    }else if($setTime=='04 pm'){

      $set_time=  date("h:i a", strtotime($setTime));
      
    }else{
      $set_time=  date("h:i a", strtotime($setTime));
      
    }

    $setDateArr=array(
      'date'=>$shedule_date,
      'time'=>$set_time,
      'created_at'=>date('Y-m-d H:i:s')
    );

    $insertId = $this->login_model->insertrecords('api_hit_date', $setDateArr);

    if($insertId){

      $messge = 'Date set successfully';

      $this->session->set_flashdata('success', $messge);
      redirect('admin/list-date');
        //$this->session->set_flashdata('success', $messge);        
    }

  }

  $this->permission_access(2);
  if ($this->access_permission) {
    $this->template->set('title', 'Admin Panel');
    $this->template->load('admin_dashboard_layout', 'contents' , 'set_date', $data);
  } else {
    $this->template->set('title', 'Admin Panel - Staff');
    $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
  }



}



public function listDate()
{
  $data['menuactive'] = $this->uri->segment(2);

  $data['testType_list'] = $this->Common_model->getAllorderby('api_hit_date','id','desc');



  $this->permission_access(2);
  if ($this->access_permission) {
    $this->template->set('title', 'Admin Panel');
    $this->template->load('admin_dashboard_layout', 'contents' , 'date_list', $data);
  } else {
    $this->template->set('title', 'Admin Panel - Staff');
    $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
  }



}



public function users() {
  $data['menuactive'] = $this->uri->segment(2);
  $con['conditions'] = array(
    'user_role '=> 2,
    'status !=' => 3
  );
  $con['sorting'] = array(
    'order_by'=> 'create_user',
    'user_role'=>2,
  );
  $data['users_data'] = $this->login_model->getRows('cp_users',$con);

  $data['staff_permission'] = $this->staff_permission;

  $this->permission_access(2);
  if ($this->access_permission) {
    $this->template->set('title', 'Admin Panel');
    $this->template->load('admin_dashboard_layout', 'contents' , 'users-listing', $data);
  } else {
    $this->template->set('title', 'Admin Panel - Staff');
    $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
  }
}

public function family_member() {
  $data['menuactive'] = $this->uri->segment(2);
  $con['conditions'] = array(
    'user_role' => 3,
    'status !=' => 3
  );
  $con['sorting'] = array(
    'order_by'=> 'create_user',
  );
  $data['users_data'] = $this->login_model->getRows('cp_users',$con);

  $data['staff_permission'] = $this->staff_permission;

  $this->permission_access(1);
  if ($this->access_permission) {
    $this->template->set('title', 'Admin Panel');
    $this->template->load('admin_dashboard_layout', 'contents' , 'family-member-listing', $data);
  } else {
    $this->template->set('title', 'Admin Panel - Staff');
    $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
  }
}
public function caregiver_list() {
  $data['menuactive'] = $this->uri->segment(2);
  $con['conditions'] = array(
    'user_role' => 4,
    'status !=' => 3
  );
  $con['sorting'] = array(
    'order_by'=> 'create_user',
  );
  $data['users_data'] = $this->login_model->getRows('cp_users',$con);

  $data['staff_permission'] = $this->staff_permission;

  $this->permission_access(1);
  if ($this->access_permission) {
    $this->template->set('title', 'Admin Panel');
    $this->template->load('admin_dashboard_layout', 'contents' , 'care-giver-listing', $data);
  } else {
    $this->template->set('title', 'Admin Panel - Staff');
    $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
  }
}

public function validateData() {

  if ($this->input->post("email") != '') {
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[cp_users.email]');
    if ($this->form_validation->run() == true) {
      die('true');
    } else {
      die('false');
    }
  }

  if ($this->input->post("mobile") != '') {
    $this->form_validation->set_rules('mobile', 'Mobile', 'required|is_unique[cp_users.mobile]');
    if ($this->form_validation->run() == true) {
      die('true');
    } else {
      die('false');
    }
  }

  if ($this->input->post("username") != '') {
    $this->form_validation->set_rules('username', 'Username', 'required|is_unique[cp_users.username]');
    if ($this->form_validation->run() == true) {
      die('true');
    } else {
      die('false');
    }
  }

}


  // public function adminEditProfile(){

  //   $data = array();

  //   $data['menuactive'] = $this->uri->segment(2);

  //   //$uid = 1;
  //   $uid=$this->session->userdata('logged_in')['id'];
  //   //$this->session->userdata(logged_in)

  //   //$exting_image = $this->input->post('exting_profile_image');
  //   $data['admin_data'] = $this->login_model->getAllRecordsById('cp_users', array('id' => $uid,'user_role'=>1));
  //   if(isset($_POST['update'])){


  //     //Check whether user upload picture
  //     if(!empty($_FILES['picture']['name'])){
  //       $config['upload_path'] = 'uploads/profile_images/';
  //       $config['allowed_types'] = 'jpg|jpeg|png|gif';
  //       $config['file_name'] = $_FILES['picture']['name'];
  //                 //Load upload library and initialize configuration
  //       $this->load->library('upload',$config);
  //       $this->upload->initialize($config);
  //       if($this->upload->do_upload('picture')){
  //         $uploadData = $this->upload->data();
  //         $picture = $uploadData['file_name'];
  //       }else{
  //         $picture = '';
  //       }

  //       $data = array(
  //       'username' => $this->input->post('username'),
  //       'email' =>  $this->input->post('email'),
  //       'mobile'  => $this->input->post('mobile'),
  //       'profile_image'=>$picture
  //     );

  //     $where_condition = array('id' => $uid);
  //     $update_banner = $this->login_model->updateRecords('cp_users',$data,$where_condition);

  //     $messge = array('message' => 'Admin has been updated successfully .<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');

  //     $this->session->set_flashdata('success', $messge);
  //     redirect('admin/adminedit');

  //     }else{
  //       $picture = '';
  //     }

  //     $data = array(
  //       'username' => $this->input->post('username'),
  //       'email' =>  $this->input->post('email'),
  //       'mobile'  => $this->input->post('mobile'),
  //       //'profile_image'=>$picture
  //     );

  //     $where_condition = array('id' => $uid);
  //     $update_banner = $this->login_model->updateRecords('cp_users',$data,$where_condition);

  //     $messge = array('message' => 'Admin has been updated successfully .<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');

  //     $this->session->set_flashdata('success', $messge);
  //     redirect('admin/adminedit');




  //   } else {
  //     $this->template->set('title', 'Edit Profile');
  //     $this->template->load('admin_dashboard_layout', 'contents' , 'admin-edit-profile', $data);
  //   }
  // }

/*Delete doctor category*/
public function delete_persone(){

    //die('sdfrtr');
  $id = $_POST['delete_music'];

  $this->login_model->deleteRecords("cp_token", array('user_id'=> $id));
  $return = $this->login_model->deleteRecords("cp_users", array('id'=> $id));
  if($return){
    echo 1;
  }else{
    echo 0;
  }
}




public function adduser_person(){

  $data = array();

  $data['menuactive_add'] = $this->uri->segment(3);

  $exting_image = $this->input->post('exting_profile_image');
  if(isset($_POST['update'])){

    if(!empty($this->input->post('new_password'))){

     $data = array(
       'mobile'  => $this->input->post('mobile'),
       'name' => $this->input->post('full_name'),
       'password' => md5($this->input->post('new_password')),
     );

   }else{
    $data = array(
     'mobile'  => $this->input->post('mobile'),
     'name' => $this->input->post('full_name')
     //'password' => md5($this->input->post('new_password')),
   );

  }
  if(!empty($_FILES['profile_image']['name'])) {
    $config['file_name']     = $_FILES['profile_image']['name'];
    $config['upload_path']   = 'uploads/profile_images/';
    $config['allowed_types'] = 'gif|jpg|jpeg|png';
    $config['max_size']      = '10000';
    $config['max_width']     = '0';
    $config['max_height']    = '0';
    $config['remove_spaces'] = true;
    $this->load->library('upload', $config);
    $this->upload->initialize($config);
    if (!$this->upload->do_upload('profile_image')) {
     $error = array('error' => $this->upload->display_errors());
     print_r($error);
   }
   else
   {
    if(!empty($exting_image)){
      unlink('./uploads/profile_images/'.$exting_image);
    }
    $img = array('upload_data' => $this->upload->data());
    $data['profile_image'] = $img['upload_data']['file_name'];
  }
}
$id = $this->input->post('id');
$where_condition = array('id' => $id);
$update_banner = $this->login_model->updateRecords('cp_users',$data,$where_condition);
$messge = array('message' => 'User has been updated successfully .<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');

$this->session->set_flashdata('success', $messge);
$user_data = $this->login_model->getSingleRecordById('cp_users',$data,$where_condition);
if($user_data['user_role'] == '2'){
 redirect('admin/users/list');
}elseif($user_data['user_role'] == '3'){
  redirect('admin/family_member/list');
}else{
  redirect('admin/caregiver/list');
}
}
if(isset($_POST['submit'])){
  $data = array(
    'username' => $this->input->post('username'),
    'email' =>  $this->input->post('email'),
    'password' => md5($this->input->post('password')),
    'mobile'  => $this->input->post('mobile'),
    'name' => $this->input->post('full_name'),
    'user_role' => 2,
    'create_user' => date('Y-m-d H:i:s'),
    'status' =>1
  );
  if(!empty($_FILES['profile_image']['name'])) {
    $config['file_name']     = time().$_FILES['profile_image']['name'];
    $config['upload_path']   = 'uploads/profile_images/';
    $config['allowed_types'] = 'gif|jpg|jpeg|png';
    $config['max_size']      = '10000';
    $config['max_width']     = '0';
    $config['max_height']    = '0';
    $config['remove_spaces'] = true;
    $this->load->library('upload', $config);
    $this->upload->initialize($config);
    if (!$this->upload->do_upload('profile_image')) {
      $error = array('error' => $this->upload->display_errors());
    }
    else
    {
      $img = array('upload_data' => $this->upload->data());
      $data['profile_image'] = $img['upload_data']['file_name'];
    }
  }
  $insertId = $this->login_model->insertrecords('cp_users', $data);
  
  /*Start add user setting btn*/
          if(!empty($insertId) && $insertId !=''){
            
            $setting_btn_arr = setting_btn_arr($insertId);
                
                
            if(!empty($setting_btn_arr) && $setting_btn_arr !=''){
              $insert_activities_record = $this->db->insert_batch('setting_butns', $setting_btn_arr);
            }
          }


  if ($insertId) {
    $email = $data['email'];
    $password = $this->input->post('password');

    $message = "Your account has been Registered by Admin. Please <a href='".base_url()."'> Login </a> with the following details.<br>Email:".$email."<br>Password:".$password." ";

    $subject = "New user registration by admin";

    sendEmail($email,$subject,$message);
  }

  $messge = array('message' => 'User has been added successfully .<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');

  $this->session->set_flashdata('success', $messge);
  redirect('admin/users/list');
} else {
  if($this->uri->segment(4)) {
    $uid = $this->uri->segment(4);
    if(isset($uid))
    {
      $data['users_data'] = $this->login_model->getAllRecordsById('cp_users', array('id' => $uid));
    }
  }
  
  $data['staff_permission'] = $this->staff_permission;

  $this->permission_access(1);
  if ($this->access_permission) {
    $this->template->set('title', 'Admin Panel');
    $this->template->load('admin_dashboard_layout', 'contents' , 'add-user', $data);
  } else {
    $this->template->set('title', 'Admin Panel - Staff');
    $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
  }
}
}

public function adduser_family()
{
  $data = array();

  $data['menuactive_add'] = $this->uri->segment(2);
  $exting_image = $this->input->post('exting_profile_image');
  if(isset($_POST['update'])){

    if(!empty($this->input->post('new_password'))){

     $data = array(
       'mobile'  => $this->input->post('mobile'),
       'name' => $this->input->post('full_name'),
       'password' => md5($this->input->post('new_password')),
     );

   }else{

    $data = array(
     'mobile'  => $this->input->post('mobile'),
     'name' => $this->input->post('full_name'),
       //'password' => md5($this->input->post('new_password')),
   );


  }
  if(!empty($_FILES['profile_image']['name'])) {
    $config['file_name']     = $_FILES['profile_image']['name'];
    $config['upload_path']   = 'uploads/profile_images/';
    $config['allowed_types'] = 'gif|jpg|jpeg|png';
    $config['max_size']      = '10000';
    $config['max_width']     = '0';
    $config['max_height']    = '0';
    $config['remove_spaces'] = true;
    $this->load->library('upload', $config);
    $this->upload->initialize($config);
    if (!$this->upload->do_upload('profile_image')) {
     $error = array('error' => $this->upload->display_errors());
     print_r($error);
   }
   else
   {
    if(!empty($exting_image)){
      unlink('./uploads/profile_images/'.$exting_image);
    }
    $img = array('upload_data' => $this->upload->data());
    $data['profile_image'] = $img['upload_data']['file_name'];
  }
}
$id = $this->input->post('id');
$where_condition = array('id' => $id);
$update_banner = $this->login_model->updateRecords('cp_users',$data,$where_condition);
$messge = array('message' => 'User has been updated successfully .<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');

$this->session->set_flashdata('success', $messge);
$user_data = $this->login_model->getSingleRecordById('cp_users',$data,$where_condition);
if($user_data['user_role'] == '2'){
 redirect('admin/users/list');
}elseif($user_data['user_role'] == '3'){
  redirect('admin/family_member/list');
}else{
  redirect('admin/caregiver/list');
}
}
if(isset($_POST['submit'])){



  $data = array(
    'username' => $this->input->post('username'),
    'email' =>  $this->input->post('email'),
    'password' => md5($this->input->post('password')),
    'mobile'  => $this->input->post('mobile'),
    'name' => $this->input->post('full_name'),
    'user_role' => 3,
    'create_user' => date('Y-m-d H:i:s'),
    'status' =>1
  );
  if(!empty($_FILES['profile_image']['name'])) {
    $config['file_name']     = time().$_FILES['profile_image']['name'];
    $config['upload_path']   = 'uploads/profile_images/';
    $config['allowed_types'] = 'gif|jpg|jpeg|png';
    $config['max_size']      = '10000';
    $config['max_width']     = '0';
    $config['max_height']    = '0';
    $config['remove_spaces'] = true;
    $this->load->library('upload', $config);
    $this->upload->initialize($config);
    if (!$this->upload->do_upload('profile_image')) {
      $error = array('error' => $this->upload->display_errors());
    }
    else
    {
      $img = array('upload_data' => $this->upload->data());
      $data['profile_image'] = $img['upload_data']['file_name'];
    }
  }
  $insertId = $this->login_model->insertrecords('cp_users', $data);

  if ($insertId) {
    $email = $data['email'];
    $password = $this->input->post('password');

    $message = "Your account has been Registered by Admin. Please <a href='".base_url()."'> Login </a> with the following details.<br>Email:".$email."<br>Password:".$password." ";

    $subject = "New user registration by admin";

    sendEmail($email,$subject,$message);
  }

  $messge = array('message' => 'User has been added successfully .<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');

  $this->session->set_flashdata('success', $messge);
  redirect('admin/family_member/list');
} else {
  if($this->uri->segment(4)) {
    $uid = $this->uri->segment(4);
    if(isset($uid))
    {
      $data['users_data'] = $this->login_model->getAllRecordsById('cp_users', array('id' => $uid));
    }
  }

  $data['staff_permission'] = $this->staff_permission;
  
  $this->permission_access(1);
  if ($this->access_permission) {
    $this->template->set('title', 'Admin Panel');
    $this->template->load('admin_dashboard_layout', 'contents' , 'adduser_family', $data);
  } else {
    $this->template->set('title', 'Admin Panel - Staff');
    $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
  }
}
}

public function adduser_caregiver()
{
  $data = array();

  $data['menuactive_add'] = $this->uri->segment(3);

  $exting_image = $this->input->post('exting_profile_image');
  if(isset($_POST['update'])){

   if(!empty($this->input->post('new_password'))){

     $data = array(
       'mobile'  => $this->input->post('mobile'),
       'name' => $this->input->post('full_name'),
       'password' => md5($this->input->post('new_password'))
     );

   }else{
     $data = array(
       'mobile'  => $this->input->post('mobile'),
       'name' => $this->input->post('full_name'),
     );
   }
   if(!empty($_FILES['profile_image']['name'])) {
    $config['file_name']     = $_FILES['profile_image']['name'];
    $config['upload_path']   = 'uploads/profile_images/';
    $config['allowed_types'] = 'gif|jpg|jpeg|png';
    $config['max_size']      = '10000';
    $config['max_width']     = '0';
    $config['max_height']    = '0';
    $config['remove_spaces'] = true;
    $this->load->library('upload', $config);
    $this->upload->initialize($config);
    if (!$this->upload->do_upload('profile_image')) {
     $error = array('error' => $this->upload->display_errors());
     print_r($error);
   }
   else
   {
    if(!empty($exting_image)){
      unlink('./uploads/profile_images/'.$exting_image);
    }
    $img = array('upload_data' => $this->upload->data());
    $data['profile_image'] = $img['upload_data']['file_name'];
  }
}
$id = $this->input->post('id');
$where_condition = array('id' => $id);
$update_banner = $this->login_model->updateRecords('cp_users',$data,$where_condition);
$messge = array('message' => 'User has been updated successfully .<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');

$this->session->set_flashdata('success', $messge);
$user_data = $this->login_model->getSingleRecordById('cp_users',$data,$where_condition);
if($user_data['user_role'] == '2'){
 redirect('admin/users/list');
}elseif($user_data['user_role'] == '3'){
  redirect('admin/family_member/list');
}else{
  redirect('admin/caregiver/list');
}
}
if(isset($_POST['submit'])){



  $data = array(
    'username' => $this->input->post('username'),
    'email' =>  $this->input->post('email'),
    'password' => md5($this->input->post('password')),
    'mobile'  => $this->input->post('mobile'),
    'name' => $this->input->post('full_name'),
    'user_role' => 4,
    'create_user' => date('Y-m-d H:i:s'),
    'status' =>1
  );
  if(!empty($_FILES['profile_image']['name'])) {
    $config['file_name']     = time().$_FILES['profile_image']['name'];
    $config['upload_path']   = 'uploads/profile_images/';
    $config['allowed_types'] = 'gif|jpg|jpeg|png';
    $config['max_size']      = '10000';
    $config['max_width']     = '0';
    $config['max_height']    = '0';
    $config['remove_spaces'] = true;
    $this->load->library('upload', $config);
    $this->upload->initialize($config);
    if (!$this->upload->do_upload('profile_image')) {
      $error = array('error' => $this->upload->display_errors());
    }
    else
    {
      $img = array('upload_data' => $this->upload->data());
      $data['profile_image'] = $img['upload_data']['file_name'];
    }
  }
  $insertId = $this->login_model->insertrecords('cp_users', $data);

  if ($insertId) {
    $email = $data['email'];
    $password = $this->input->post('password');

    $message = "Your account has been Registered by Admin. Please <a href='".base_url()."'> Login </a> with the following details.<br>Email:".$email."<br>Password:".$password." ";

    $subject = "New user registration by admin";

    sendEmail($email,$subject,$message);
  }

  $messge = array('message' => 'User has been added successfully .<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');

  $this->session->set_flashdata('success', $messge);
  redirect('admin/caregiver/list');
} else {
  if($this->uri->segment(4)) {
    $uid = $this->uri->segment(4);
    if(isset($uid))
    {
      $data['users_data'] = $this->login_model->getAllRecordsById('cp_users', array('id' => $uid));
    }
  }
  
  $data['staff_permission'] = $this->staff_permission;

  $this->permission_access(1);
  if ($this->access_permission) {
    $this->template->set('title', 'Admin Panel');
    $this->template->load('admin_dashboard_layout', 'contents' , 'adduser_caregiver', $data);
  } else {
    $this->template->set('title', 'Admin Panel - Staff');
    $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
  }
}
}

private function username_exists($email)
{
  $this->db->where('username', $email);
  $query = $this->db->get('cp_users');
  if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; }
}

function register_username_exists()
{
  if (array_key_exists('username',$_POST)) {
   if ( $this->username_exists($this->input->post('username')) == TRUE ) {
    echo json_encode(FALSE);
  } else {
    echo json_encode(TRUE);
  }
}
}

public function deleteuserRecords(){

  //die('uirtur');

  $id = $_POST['id'];
    //$return = $this->login_model->updateRecordsUser("cp_users", array('status' => 3), array('id'=> $id));
  $return = $this->login_model->deleteRecords("cp_users",array('id'=> $id));


  //$this->login_model->deleteRecords("cp_users",array('id'=> $id));
  $this->login_model->deleteRecords("acknowledge",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_articles",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_article_favourite",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_article_trash",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_bank",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_emails",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_fav_banks",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_fav_grocery",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_fav_restaurant",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_grocery",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_movie_favorite",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_movie_trash",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_music_favorite",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_music_trash",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_note",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_photo_favourite",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_reminder",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_restaurant",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_test_report",array('user_id'=> $id));
  $this->login_model->deleteRecords("cp_user_contact",array('contact_user_id'=> $id));
  $this->login_model->deleteRecords("cp_user_photo",array('user_id'=> $id));
  $this->login_model->deleteRecords("doctor_appointments",array('user_id'=> $id));
  $this->login_model->deleteRecords("medicine_schedule",array('user_id'=> $id));
  $this->login_model->deleteRecords("music",array('user_id'=> $id));
  $this->login_model->deleteRecords("picup_medicine",array('user_id'=> $id));
  $this->login_model->deleteRecords("send_test_report",array('user_id'=> $id));
  $this->login_model->deleteRecords("movie",array('user_id'=> $id));




  //  if($return > 0){
  echo json_encode(array('status'=>1, 'msg'=>'User Deleted Successfully'));
   // }else{
    //  echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
   // }
}

  //Active/Deactive User
public function updateStatusUser() {
  $id = $_POST['id'];
  $con['conditions'] = array(
    'id'=> $id
  );
  $data['users_data'] = $this->login_model->getRows('cp_users',$con);
  if($data['users_data'][0]['status']==1){
    $return = $this->login_model->updateRecords("cp_users", array('status' => 0), array('id'=> $id));
  }else{
    $return = $this->login_model->updateRecords("cp_users", array('status' => 1), array('id'=> $id));
  }



  if($return > 0){

    $user_status = $this->Common_model->getsingle('cp_users',array('id'=>$id));

    if($user_status->status==1){

      echo json_encode(array('status'=>1, 'msg'=>'User active successfully.'));

    }else{

      echo json_encode(array('status'=>1, 'msg'=>'User deactive successfully.'));
    }


  }else{
        //echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
  }
}


  //Active/Deactive User
public function verifyStatusUser() {
  $id = $_POST['id'];
  $con['conditions'] = array(
    'id'=> $id
  );
  $data['users_data'] = $this->login_model->getRows('cp_users',$con);
  if($data['users_data'][0]['status']==1){
    $return = $this->login_model->updateRecords("cp_users", array('status' => 0), array('id'=> $id));
  }else{
    $return = $this->login_model->updateRecords("cp_users", array('status' => 1), array('id'=> $id));
  }



  if($return > 0){

    $user_status = $this->Common_model->getsingle('cp_users',array('id'=>$id));

    if($user_status->status==1){

      echo json_encode(array('status'=>1, 'msg'=>'User active successfully.'));

    }else{

      echo json_encode(array('status'=>1, 'msg'=>'User deactive successfully.'));
    }


  }else{
        //echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
  }
}


public function viewuser($user_id = false) {

  $data['menuactive'] = $this->uri->segment(2);
  $uid = $this->session->userdata('logged_ins')['id'];
  if(!empty($user_id))
  {
   $userTrans= $this->login_model->getSingleRecordById('subscription_payment_transactions',array('user_id' => $user_id));


//    $tabletdetail = $this->login_model->getSingleRecordById('tabdetails',array('user_id' => $user_id));

//    if(!empty($tabletdetail))
//      $data['tabData'] =$tabletdetail;
//  }else{
//   $data['tabData'] ='';
// }

   if(!empty($userTrans)){

     $planTrans = $this->login_model->getSingleRecordById('subscription_plan',array('id' => $userTrans['plan_id']));

		 if(!empty($planTrans) && $planTrans !=''){
				$plan_name = $planTrans['plan_name'];

				$data['userstransdata']=array(
					'plan_name'=> $plan_name,
				);
		 }

     $data['userstransdata']=array(
				'transaction_date'=>$userTrans['txn_date'],
				'transaction_status'=>$userTrans['payment_status'],
				'amount'=>$userTrans['amount'],
				'transaction_id'=>$userTrans['txn_id'],
				'start_date'=>$userTrans['startDate'],
				'end_date'=>$userTrans['endDate']
				);
		
   }else{
    $data['userstransdata']='';
  }


  $data['users_data'] = $this->login_model->getSingleRecordById('cp_users',array('id' => $user_id,'user_role' => 2));
  if(empty($data['users_data']))
  {
    $u = "admin/users/list";
    $this->session->set_flashdata('error', 'User is not available');
    redirect($u);
  }else{
      //print_r($data); die;

    $data['token_info'] = $this->login_model->getSingleRecordById('cp_token',array('user_id' => $user_id));

    $data['staff_permission'] = $this->staff_permission;

    $this->permission_access(1);
    if ($this->access_permission) {
     $this->template->set('title', 'Admin Panel');
     $this->template->load('admin_dashboard_layout', 'contents', 'view-user-profile',$data);
   } else {
     $this->template->set('title', 'Admin Panel - Staff');
     $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
   }
 }
}else
{
  redirect("admin/users/list");
}
}

public function submit_token() {



  $user_id = $this->input->post('user_id');
  $token = $this->input->post('token');
  //$device_id = $this->input->post('device_id');
  $tablet_name = $this->input->post('tablet_name');

  $token_info = $this->login_model->getSingleRecordById('cp_token',array('user_id' => $user_id));

  if(!empty($token_info)) {
    $array = array(
      'token' => $token,
      //'device_id' => $device_id,
      'tablet_name' => $tablet_name,
      'updated_at' => date('Y-m-d H:i:s')     
    );

    $this->login_model->updateData('cp_token',$array,array('user_id' => $user_id));
  } else {
    $post_data = array(
     'user_id' => $user_id,
     'token' => $token,
     //'device_id' => $device_id,
     'tablet_name' => $tablet_name,
     'status' => 1,
     'created_at' => date('Y-m-d H:i:s'),
     'updated_at' => date('Y-m-d H:i:s')
   );
    $this->login_model->addRecords('cp_token',$post_data);
  }



  echo 1;
}

public function viewfamily($user_id = false) {

  $data['menuactive'] = $this->uri->segment(2);
  if(!empty($user_id))
  {
    $data['users_data'] = $this->login_model->getSingleRecordById('cp_users',array('id' => $user_id,'user_role' => 3));
    if(empty($data['users_data']))
    {
      $u = "admin/family_member/list";
      $this->session->set_flashdata('error', 'Family Member is not available');
      redirect($u);
    }else{
          //print_r($data); die;
      $data['staff_permission'] = $this->staff_permission;

      $this->permission_access(1);
      if ($this->access_permission) {
       $this->template->set('title', 'Admin Panel');
       $this->template->load('admin_dashboard_layout', 'contents', 'view-user-profile',$data);
     } else {
       $this->template->set('title', 'Admin Panel - Staff');
       $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }
 }else
 {
  redirect("admin/family_member/list");
}
}
public function viewcaregiver($user_id = false) {

  $data['menuactive'] = $this->uri->segment(2);
  if(!empty($user_id))
  {
    $data['users_data'] = $this->login_model->getSingleRecordById('cp_users',array('id' => $user_id,'user_role' => 4));
    if(empty($data['users_data']))
    {
      $u = "admin/caregiver/list";
      $this->session->set_flashdata('error', 'Care Giver is not available');
      redirect($u);
    }else{
          //print_r($data); die;
      $data['staff_permission'] = $this->staff_permission;

      $this->permission_access(1);
      if ($this->access_permission) {
       $this->template->set('title', 'Admin Panel');
       $this->template->load('admin_dashboard_layout', 'contents', 'view-user-profile',$data);
     } else {
       $this->template->set('title', 'Admin Panel - Staff');
       $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }
 }else
 {
  redirect("admin/caregiver/list");
}
}
public function user_photo() {

 $data['menuactive'] = $this->uri->segment(2);
 $con['conditions'] = array(
  'u_photo_status !=' => 3
);
 $con['sorting'] = array(
  'order_by'=> 'u_photo_created',
);
 $data['menuactive'] = $this->uri->segment(2);
 $data['users_data'] = $this->login_model->getRows('cp_user_photo',$con);

 $data['staff_permission'] = $this->staff_permission;

 $this->permission_access(2);
 if ($this->access_permission) {
   $this->template->set('title', 'Admin Panel');
   $this->template->load('admin_dashboard_layout', 'contents' , 'photos-listing', $data);
 } else {
   $this->template->set('title', 'Admin Panel - Staff');
   $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
 }
}

public function adduser_photo(){
 $data = array();
 $data['menuactive'] = $this->uri->segment(2);
 $exting_image = $this->input->post('exting_profile_image');
 if(isset($_POST['update'])){
   $data = array(
     'user_id' => $this->input->post('user_id'),
   );
   if(!empty($_FILES['u_photo']['name'])) {
     $config['file_name']     = $_FILES['u_photo']['name'];
     $config['upload_path']   = 'uploads/photos/';
     $config['allowed_types'] = 'gif|jpg|jpeg|png';
     $config['max_size']      = '10000';
     $config['max_width']     = '0';
     $config['max_height']    = '0';
     $config['remove_spaces'] = true;
     $this->load->library('upload', $config);
     $this->upload->initialize($config);
     if (!$this->upload->do_upload('u_photo')) {
      $error = array('error' => $this->upload->display_errors());
      print_r($error);
    }
    else
    {
     if(!empty($exting_image)){
       unlink('./uploads/photos/'.$exting_image);
     }
     $img = array('upload_data' => $this->upload->data());
     $data['u_photo'] = $img['upload_data']['file_name'];
   }
 }
 $id = $this->input->post('id');
 $where_condition = array('u_photo_id' => $id);
 $update_banner = $this->login_model->updateRecords('cp_user_photo',$data,$where_condition);

 $messge = array('message' => 'Photo has been updated successfully .<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');

 $this->session->set_flashdata('success', $messge);
 redirect('admin/userphoto/list');
}

if(isset($_POST['submit'])){
 $data = array(
   'user_id' => $this->input->post('user_id'),
   'u_photo_created' => date('Y-m-d H:i:s'),
   'status' =>1
 );
 if(!empty($_FILES['u_photo']['name'])) {
   $config['file_name']     = time().$_FILES['u_photo']['name'];
   $config['upload_path']   = 'uploads/photos/';
   $config['allowed_types'] = 'gif|jpg|jpeg|png';
   $config['max_size']      = '10000';
   $config['max_width']     = '0';
   $config['max_height']    = '0';
   $config['remove_spaces'] = true;
   $this->load->library('upload', $config);
   $this->upload->initialize($config);
   if (!$this->upload->do_upload('u_photo')) {
     $error = array('error' => $this->upload->display_errors());
   }
   else
   {
     $img = array('upload_data' => $this->upload->data());
     $data['u_photo'] = $img['upload_data']['file_name'];
   }
 }
 $insertId = $this->login_model->insertrecords('cp_user_photo', $data);
 $messge = array('message' => 'Photo has been added successfully .<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');

 $this->session->set_flashdata('success', $messge);
 redirect('admin/userphoto/list');
} else {
 if($this->uri->segment(3)) {
   $uid = $this->uri->segment(3);
   if(isset($uid))
   {
     $data['users_data'] = $this->login_model->getAllRecordsById('cp_user_photo', array('u_photo_id' => $uid));
   }
 }
 $data['user_data'] = $this->login_model->getAllRecordsById('cp_users', array('user_role' => 2));

 $data['staff_permission'] = $this->staff_permission;

 $this->permission_access(1);
 if ($this->access_permission) {
  $this->template->set('title', 'Admin Panel');
  $this->template->load('admin_dashboard_layout', 'contents' , 'add-photo', $data);
} else {
  $this->template->set('title', 'Admin Panel - Staff');
  $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
}
}
}

public function viewuser_photo($user_id = false) {

  $data['menuactive'] = $this->uri->segment(2);
  if(!empty($user_id))
  {
    $data['users_data'] = $this->login_model->getSingleRecordById('cp_user_photo',array('u_photo_id	' => $user_id));
    if(empty($data['users_data']))
    {
      $u = "admin/userphoto/list";
      $this->session->set_flashdata('error', 'Photo is not available');
      redirect($u);
    }else{
          //print_r($data); die;

      $data['staff_permission'] = $this->staff_permission;

      $this->permission_access(1);
      if ($this->access_permission) {
       $this->template->set('title', 'Admin Panel');
       $this->template->load('admin_dashboard_layout', 'contents', 'view-photo',$data);
     } else {
       $this->template->set('title', 'Admin Panel - Staff');
       $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }
 }else
 {
  redirect("admin/userphoto/list");
}
}
public function updateStatusphoto() {
  $id = $_POST['id'];
  $con['conditions'] = array(
    'u_photo_id'=> $id
  );
  $data['trans_data'] = $this->login_model->getRows('cp_user_photo',$con);
  if($data['trans_data'][0]['u_photo_status']==1){
    $return = $this->login_model->updateRecords("cp_user_photo", array('u_photo_status' => 0), array('u_photo_id'=> $id));
  }else{
    $return = $this->login_model->updateRecords("cp_user_photo", array('u_photo_status' => 1), array('u_photo_id'=> $id));
  }

  if($return > 0){

    $user_photo = $this->Common_model->getsingle('cp_user_photo',array('u_photo_id'=>$id));

    if($user_photo->u_photo_status==1){


      echo json_encode(array('status'=>1, 'msg'=>'Photo active successfully.'));


    }else{

      echo json_encode(array('status'=>1, 'msg'=>'Photo deactive successfully.'));

    }


  }else{
    echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
  }
}


public function deleteuserphotoRecords(){
  $id = $_POST['id'];
    //$return = $this->login_model->updateRecords("cp_user_photo", array('u_photo_status' => 3), array('u_photo_id'=> $id));

  $photoDetail = $this->login_model->getSingleRecordById('cp_user_photo',array('u_photo_id ' => $id));
  $photoFilePath=getcwd().'/uploads/photos/';
  $photoFile=$photoDetail['u_photo'];
  unlink($photoFilePath.$photoFile);

  $return = $this->login_model->deleteRecords("cp_user_photo", array('u_photo_id'=> $id));
  $returne = $this->login_model->deleteRecords("cp_photo_favourite", array('photo_id'=> $id));


  if($returne > 0){
    echo json_encode(array('status'=>1, 'msg'=>'Photo Deleted Successfully'));
  }else{
    echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
  }
}



  // public function deleteuserphotoRecords(){
  //   $id = $_POST['id'];
  //   $return = $this->login_model->updateRecords("cp_user_photo", array('u_photo_status' => 3), array('u_photo_id'=> $id));



  //   if($return > 0){
  //     echo json_encode(array('status'=>1, 'msg'=>'Photo Deleted Successfully'));
  //   }else{
  //     echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
  //   }
  // }


public function email_list() {
  $data['menuactive'] = $this->uri->segment(2);
  $con['conditions'] = array(
    'email_status !=' => 3
  );
  $con['sorting'] = array(
    'order_by'=> 'create_email',
  );
  $data['emails_data'] = $this->login_model->getRows('cp_emails',$con);


  $data['staff_permission'] = $this->staff_permission;

  $this->permission_access(2);
  if ($this->access_permission) {
   $this->template->set('title', 'Admin Panel');
   $this->template->load('admin_dashboard_layout', 'contents' , 'emails-listing', $data);
 } else {
   $this->template->set('title', 'Admin Panel - Staff');
   $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
 }
}
public function viewEmail($email_id = false) {

  $data['menuactives'] = $this->uri->segment(3);

  // echo $email_id;
  // die;
  if(!empty($email_id)) 
  { 
    $data['email_data'] = $this->login_model->getSingleRecordById('cp_emails',array('email_id ' => $email_id));
    $data['to_emails'] = $this->Common_model->getAllwhere('cp_to_email',array('email_id ' => $email_id));
    if(empty($data['email_data']))
    {
      $u = "admin/email_list/list";
      $this->session->set_flashdata('error', 'Email is not available');
      redirect($u);
    }else{
      //echo "<pre>"; print_r($data); die;

     $data['staff_permission'] = $this->staff_permission;

     $this->permission_access(1);
     if ($this->access_permission) {
       $this->template->set('title', 'Admin Panel');
       $this->template->load('admin_dashboard_layout', 'contents', 'view-email',$data);
     } else {
       $this->template->set('title', 'Admin Panel - Staff');
       $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }
 }else
 {
  redirect("admin/email_list/list");
}
}
public function updateStatusemails() {
  $id = $_POST['id'];
  $con['conditions'] = array(
    'email_id'=> $id
  );
  $data['email_data'] = $this->login_model->getRows('cp_emails',$con);
  if($data['email_data'][0]['email_status']==1){
    $return = $this->login_model->updateRecords("cp_emails", array('email_status' => 0), array('email_id'=> $id));
  }else{
    $return = $this->login_model->updateRecords("cp_emails", array('email_status' => 1), array('email_id'=> $id));
  }

  if($return > 0){
    echo json_encode(array('status'=>1, 'msg'=>'Email Status Updated Successfully'));
  }else{
    echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
  }
}

/*Delete email and there attachments from table and uploads also*/
public function deleteemailRecords(){
  $id = $_POST['id'];
  $emailData = $this->login_model->getAllRecordsById('cp_emails_attachment', array('email_id' => $id));

  foreach ($emailData as $attachment) {

    $imageFilePath=getcwd().'/uploads/email/';
    $imageFile=$attachment['attachment_file'];
    unlink($imageFilePath.$imageFile);

    $this->login_model->deleteRecords("cp_emails_attachment", array('email_id'=> $id));
  }

  $return= $this->login_model->deleteRecords("cp_emails", array('email_id'=> $id));
  //die('eroeuroe');

  if($return > 0){
    echo json_encode(array('status'=>1, 'msg'=>'Email Deleted Successfully'));
  }else{
    echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
  }
}



  // public function deleteemailRecords(){
  //   $id = $_POST['id'];
  //   $return = $this->login_model->updateRecords("cp_emails", array('email_status' => 3), array('email_id'=> $id));
  //   if($return > 0){
  //     echo json_encode(array('status'=>1, 'msg'=>'Email Deleted Successfully'));
  //   }else{
  //     echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
  //   }
  // }

public function doctor_categories() {

  $data['menuactive'] = $this->uri->segment(2);




  $con['sorting'] = array(
    'order_by'=> 'dc_created',
  );
    //$data['menuactive'] = $this->uri->segment(2);
  $data['dc_categories_data'] = $this->login_model->getRows('cp_doctor_categories',$con);
    //print_r($data['dc_categories_data']);die;
  
  $data['staff_permission'] = $this->staff_permission;

  $this->permission_access(1);
  if ($this->access_permission) {
    $this->template->set('title', 'Admin Panel');
    $this->template->load('admin_dashboard_layout', 'contents' , 'doctor_categories', $data);
  } else {
    $this->template->set('title', 'Admin Panel - Staff');
    $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
  }
}

public function viewDc_cat($dc_id = false) {
    //echo $dc_id;die;
  $data['menuactive'] = $this->uri->segment(2);
  if(!empty($dc_id))
  {
    $data['dc_categories_data'] = $this->login_model->getSingleRecordById('cp_doctor_categories',array('dc_id ' => $dc_id));
    if(empty($data['dc_categories_data']))
    {
      $u = "admin/dc_category/list";
      $this->session->set_flashdata('error', 'Category is not available');
      redirect($u);
    }else{
          //print_r($data); die;

      $data['staff_permission'] = $this->staff_permission;
      $this->permission_access(1);
      if ($this->access_permission) {
       $this->template->set('title', 'Admin Panel');
       $this->template->load('admin_dashboard_layout', 'contents', 'view-dc_cat',$data);
     } else {
       $this->template->set('title', 'Admin Panel - Staff');
       $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }
 }else
 {
  redirect("admin/dc_category/list");
}
}

public function adminProfile() {

  $data = array();
  $data['menuactive'] = $this->uri->segment(2);

  $user_id=$this->session->userdata('logged_ins')['id'];

    //$user_id = 1;

  if(!empty($user_id)) {
    $data['users_data'] = $this->login_model->getSingleRecordById('cp_users',array('id' => $user_id,'user_role' => 1));
    $data['users_address'] = $this->login_model->get_address_data($data['users_data']['address']);
        //print_r($data); die;

    $data['staff_permission'] = $this->staff_permission;

      /*$this->permission_access(1);
      if ($this->access_permission) {
         $this->template->set('title', 'Admin Panel');
         $this->template->load('admin_dashboard_layout', 'contents', 'view-user-profile',$data);
      } else {
         $this->template->set('title', 'Admin Panel - Staff');
         $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
       }*/
       $this->template->set('title', 'Admin Panel');
       $this->template->load('admin_dashboard_layout', 'contents', 'view-user-profile',$data);
     } else {
      redirect("admin/dashboard");
    }
  }


  public function adminEditProfile(){

    $data = array();

    $data['menuactive'] = $this->uri->segment(2);

    $uid = $this->session->userdata('logged_ins')['id'];

    //$exting_image = $this->input->post('exting_profile_image');
    $data['admin_data'] = $this->login_model->getAllRecordsById('cp_users', array('id' => $uid,'user_role'=>1));
    if(isset($_POST['update'])){


      //Check whether user upload picture
      if(!empty($_FILES['picture']['name'])){
        $config['upload_path'] = 'uploads/profile_images/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['picture']['name'];
                  //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('picture')){
          $uploadData = $this->upload->data();
          $picture = $uploadData['file_name'];
        }else{
          $picture = '';
        }

        $data = array(
          'username' => $this->input->post('username'),
          'email' =>  $this->input->post('email'),
          'mobile'  => $this->input->post('mobile'),
          'profile_image'=>$picture
        );

        $where_condition = array('id' => $uid);
        $update_banner = $this->login_model->updateRecords('cp_users',$data,$where_condition);

        $messge = array('message' => 'Admin has been updated successfully .<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');

        $this->session->set_flashdata('success', $messge);
        redirect('admin/adminedit');

      }else{
        $picture = '';
      }

      $data = array(
        'username' => $this->input->post('username'),
        'email' =>  $this->input->post('email'),
        'mobile'  => $this->input->post('mobile'),
        //'profile_image'=>$picture
      );

      $where_condition = array('id' => $uid);
      $update_banner = $this->login_model->updateRecords('cp_users',$data,$where_condition);

      $messge = array('message' => 'Admin has been updated successfully .<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');

      $this->session->set_flashdata('success', $messge);
      redirect('admin/adminedit');




    } else {

      $data['staff_permission'] = $this->staff_permission;

      /*$this->permission_access(1);
      if ($this->access_permission) {
         $this->template->set('title', 'Edit Profile');
         $this->template->load('admin_dashboard_layout', 'contents' , 'admin-edit-profile', $data);
      } else {
         $this->template->set('title', 'Admin Panel - Staff');
         $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
       }*/
       $this->template->set('title', 'Edit Profile');
       $this->template->load('admin_dashboard_layout', 'contents' , 'admin-edit-profile', $data);
     }
   }

  // public function adminEditProfile(){

  //   $data = array();

  //   $data['menuactive'] = $this->uri->segment(2);

  //   $uid = 1;
  //   $exting_image = $this->input->post('exting_profile_image');
  //   $data['admin_data'] = $this->login_model->getAllRecordsById('cp_users', array('id' => $uid));
  //   if(isset($_POST['update'])){
  //     $data = array(
  //       'username' => $this->input->post('username'),
  //       'email' =>  $this->input->post('email'),
  //       'mobile'  => $this->input->post('mobile'),
  //     );
  //       if(!empty($_FILES['profile_image']['name'])) {
  //       $config['file_name']     = $_FILES['profile_image']['name'];
  //       $config['upload_path']   = 'uploads/profile_images/';
  //       $config['allowed_types'] = 'gif|jpg|jpeg|png';
  //       $config['max_size']      = '10000';
  //       $config['max_width']     = '0';
  //       $config['max_height']    = '0';
  //       $config['remove_spaces'] = true;
  //       $this->load->library('upload', $config);
  //       $this->upload->initialize($config);
  //       if (!$this->upload->do_upload('profile_image')) {
  //          $error = array('error' => $this->upload->display_errors());
  //          print_r($error);
  //       } else {
  //         if(!empty($exting_image)){
  //           unlink('./uploads/profile_images/'.$exting_image);
  //         }
  //         $img = array('upload_data' => $this->upload->data());
  //         $data['profile_image'] = $img['upload_data']['file_name'];
  //       }
  //     }
  //     $where_condition = array('id' => $uid);
  //     $update_banner = $this->login_model->updateRecords('cp_users',$data,$where_condition);

  //     $messge = array('message' => 'User has been updated successfully .<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>','class' => 'alert alert-success fade in');

  //     $this->session->set_flashdata('success', $messge);
  //     redirect('admin/adminedit');
  //   } else {
  //     $this->template->set('title', 'Edit Profile');
  //     $this->template->load('admin_dashboard_layout', 'contents' , 'admin-edit-profile', $data);
  //   }
  // }

  /*
  * Change Password for users
  */
  public function change_password(){
    $data = array();
    $data['menuactive'] = $this->uri->segment(2);

    $data['staff_permission'] = $this->staff_permission;

   /*$this->permission_access(1); 
   if ($this->access_permission) {
      $this->template->set('title', 'Change Password');
      $this->template->load('admin_dashboard_layout', 'contents' , 'change_password', $data);
   } else {
      $this->template->set('title', 'Admin Panel - Staff');
      $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
    }*/
    $this->template->set('title', 'Change Password');
    $this->template->load('admin_dashboard_layout', 'contents', 'change_password', $data);
  }

  /*
  * Update User password
  */
  public function update_Password(){
      
   $userid = $this->session->userdata('logged_ins')['id'];  
    $old = $this->input->post("oldpass");
    $new = $this->input->post("newpass");
    if(!empty($old) && !empty($new)){
    //  $userid = 1;
      $con['conditions'] = array("id"=>$userid,"password"=>md5($old));
      $user = $this->login_model->getRows('cp_users',$con);
   
     // if($user[0]['password'] == md5($old)){  amit comment this condition
      if(!empty( $user)){
        $data = array("password"=>md5($new));

        $where_condition = array('id' => $userid);
        $check = $this->login_model->updateRecords('cp_users',$data,$where_condition);
        $cont = '<div class="alert alert-success" role="alert"><strong>Success!</strong> Password updated successfully.<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a></div>';
        echo json_encode(array("status"=>200, "msg"=>$cont));
      } else{
        $cont = '<div class="alert alert-danger" role="alert"><strong>Sorry!</strong> Old password not matched.<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a></div>';
        echo json_encode(array("status"=>1, "msg"=>$cont));
      }
    } else{
      $cont = '<div class="alert alert-danger" role="alert"><strong>Sorry!</strong> Invalid access.<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a></div>';
      echo json_encode(array("status"=>1, "msg"=>$cont));
    }
  }
  public function status_active()
  {
    $tabname = $this->input->post('tabname');
    $status = $this->input->post('status');
    $id = $this->input->post('id');
    if(!empty($id))
    {
      $result = $this->common_model->updateRecords($tabname,array('status' => $status), array('id' => $id));
    }
  }
  public function logout() {
    $sess_array = array(
      'id' => '',
      'email' => '',
      'user_role' => ''
    );
    $this->session->unset_userdata('logged_ins',$sess_array);
   //$this->session->sess_destroy();
    redirect('admin/login');
  }


  /*Created by 95 on 4-1-2019 For Add New Category*/
  public function adddoctor_category(){
   $data['menuactive'] = $this->uri->segment(3);
   $this->form_validation->set_rules('category_name', 'Category Name', 'required');

   if($this->form_validation->run() == True){


    $data['menuactive'] = $this->uri->segment(3);
    if($this->input->post('submit')){
            //Check whether user upload picture
      if(!empty($_FILES['picture']['name'])){
        $config['upload_path'] = 'uploads/doctor_category/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['picture']['name'];
                //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('picture')){
          $uploadData = $this->upload->data();
          $picture = $uploadData['file_name'];
        }else{
          $picture = '';
        }
      }else{
        $picture = '';
      }
      $data = array(
        'dc_name' => $this->input->post('category_name'),
        'dc_created' => date('Y-m-d'),
        'dc_icon' => $picture,
        'dc_status' => 1,
      );
      $insertId = $this->login_model->insertrecords('cp_doctor_categories', $data);
      if($insertId){
        $this->session->set_flashdata('success_msg', 'Doctor Category have been added successfully.');
        redirect('admin/doctor-category/list');
      }else{
        $this->session->set_flashdata('error_msg', 'Some problems occured, please try again.');
      }

    }

  }else{



  }

  $data['staff_permission'] = $this->staff_permission;

  $this->permission_access(1);
  if ($this->access_permission) {
    $this->template->set('title', 'Admin Panel');
    $this->template->load('admin_dashboard_layout', 'contents', 'add-doctor-category',$data);
  } else {
    $this->template->set('title', 'Admin Panel - Staff');
    $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
  }

}


/*Created by 95 on 4-2-2019 For Doctor Category Listing*/
public function doctors() {

  $data['menuactive'] = $this->uri->segment(3);
  $data['dc_data'] = $this->Common_model->getAllorderby('cp_doctor_categories','dc_id','desc');

  $data['staff_permission'] = $this->staff_permission;

  $this->permission_access(3);
  if ($this->access_permission) {
    $this->template->set('title', 'Admin Panel');
    $this->template->load('admin_dashboard_layout', 'contents', 'doctor-category-list', $data);
  } else {
    $this->template->set('title', 'Admin Panel - Staff');
    $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
  }
}

/*Created by 95 on 4-2-2019 For View Doctor Category detail*/
public function viewdoctor_category($category_id = false) {
  $data['menuactive'] = $this->uri->segment(2);
  if(!empty($category_id))
  {
    $data['categoryDetail'] = $this->login_model->getSingleRecordById('cp_doctor_categories',array('dc_id' => $category_id));

    $data['dc_data'] = $this->Common_model->getAllorderby('cp_doctor_categories','dc_id','desc');
    if(empty($data['categoryDetail']))
    {
      $u = "admin/doctor-category/list";
      $this->session->set_flashdata('error', 'Category is not available');
      redirect($u);
    }else{
          //print_r($data); die;
      $data['staff_permission'] = $this->staff_permission;

      $this->permission_access(1);
      if ($this->access_permission) {
       $this->template->set('title', 'Admin Panel');
       $this->template->load('admin_dashboard_layout', 'contents', 'doctorcategory-view-detail',$data);
     } else {
       $this->template->set('title', 'Admin Panel - Staff');
       $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
     }
   }
 }else
 {
  redirect("admin/doctor-category/list");
}
}

/*Created by 95 on 4-2-2019 For Delete Doctor Category*/
public function deletedoctor_categoryRecords(){
  $id = $_POST['delete_music'];
  $return = $this->login_model->deleteRecords("cp_doctor_categories", array('dc_id'=> $id));
  if($return > 0){
    echo json_encode(array('status'=>1, 'msg'=>'Category Deleted Successfully'));
  }else{
    echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
  }
}


/*Delete doctor category*/
public function deletedoctor_category(){
  $id = $_POST['delete_music'];
  $return = $this->login_model->deleteRecords("cp_doctor_categories", array('dc_id'=> $id));
  if($return){
    echo 1;
  }else{
    echo 0;
  }
}



/*Created by 95 on 4-1-2019 For Edit Doctor Category*/
public function editdoctor_category($id=''){
 $data['menuactive'] = $this->uri->segment(2);
 $data['editData'] = $this->login_model->getSingleRecordById('cp_doctor_categories',array('dc_id' => $id));
 if($this->input->post('submit')){
            //Check whether user upload picture
  if(!empty($_FILES['picture']['name'])){
    $config['upload_path'] = 'uploads/doctor_category/';
    $config['allowed_types'] = 'jpg|jpeg|png|gif';
    $config['file_name'] = $_FILES['picture']['name'];
                //Load upload library and initialize configuration
    $this->load->library('upload',$config);
    $this->upload->initialize($config);
    if($this->upload->do_upload('picture')){
      $uploadData = $this->upload->data();
      $picture = $uploadData['file_name'];
    }else{
      $picture = '';
    }
    $data = array(
      'dc_name' => $this->input->post('category_name'),
      'dc_created' => date('Y-m-d'),
      'dc_icon' => $picture,
    );
    $insertId = $this->login_model->update_category($data,$id);
  }else{
    $picture = '';
  }
  $data = array(
    'dc_name' => $this->input->post('category_name'),
    'dc_created' => date('Y-m-d'),
  );
  $insertId = $this->login_model->update_category($data,$id);
  if($insertId){
    $this->session->set_flashdata('success_msg', 'Doctor Category have been updated successfully.');
    redirect('admin/doctor-category/list');
  }else{
    $this->session->set_flashdata('error_msg', 'Some problems occured, please try again.');
  }
}else{

  $data['staff_permission'] = $this->staff_permission;

  $this->permission_access(1);
  if ($this->access_permission) {
    $this->template->set('title', 'Admin Panel');
    $this->template->load('admin_dashboard_layout', 'contents', 'edit-doctor-category',$data);
  } else {
    $this->template->set('title', 'Admin Panel - Staff');
    $this->template->load('admin_dashboard_layout', 'contents' , 'no_access', $data);
  }
}
}

public function activation_status()
{
	$activation_id = $this->input->post("activation_id");
	$user_id = $this->input->post("user_id");
	$user_data = $this->Common_model->getsingle("cp_users",array("id" => $user_id));
	
	
	if($user_data->conversation_status == 1){
		$this->Common_model->updateData("cp_users",array("conversation_status" => 0),array("id" => $user_id));
		echo '0';
	}else{
		$this->Common_model->updateData("cp_users",array("conversation_status" => 1),array("id" => $user_id));
		echo '1';
	}
	
}



public function verify_status()
{
  $activation_id = $this->input->post("activation_id");
  $user_id = $this->input->post("user_id");

  $column_id = $this->input->post("column_id");

  if($column_id=='device_verify_by_token'){

    $user_data = $this->Common_model->getsingle("cp_users",array("id" => $user_id));
    
    
    if($user_data->device_verify_by_token == 1){
      $this->Common_model->updateData("cp_users",array("device_verify_by_token" => 0),array("id" => $user_id));
      echo '0';
    }else{
      $this->Common_model->updateData("cp_users",array("device_verify_by_token" => 1),array("id" => $user_id));
      echo '1';
    }

  }elseif($column_id=='user_verify_by_app'){

    $user_data = $this->Common_model->getsingle("cp_users",array("id" => $user_id));
    
    
    if($user_data->user_verify_by_app == 1){
      $this->Common_model->updateData("cp_users",array("user_verify_by_app" => 0),array("id" => $user_id));
      echo '0';
    }else{
      $this->Common_model->updateData("cp_users",array("user_verify_by_app" => 1),array("id" => $user_id));
      echo '1';
    }

  }elseif($column_id=='status'){
    $user_data = $this->Common_model->getsingle("cp_users",array("id" => $user_id));


    if($user_data->status == 1){
      $this->Common_model->updateData("cp_users",array("status" => 0),array("id" => $user_id));
      echo '0';
    }else{
      $this->Common_model->updateData("cp_users",array("status" => 1),array("id" => $user_id));
      echo '1';
    }
  }else{
   $user_data = $this->Common_model->getsingle("cp_users",array("id" => $user_id));


   if($user_data->device_verify == 1){
    $this->Common_model->updateData("cp_users",array("device_verify" => 0),array("id" => $user_id));
    echo '0';
  }else{
    $this->Common_model->updateData("cp_users",array("device_verify" => 1),array("id" => $user_id));
    echo '1';
  }
}








}


public function getall_variable_list(){

	$userid = $this->session->userdata('logged_ins')['id'];

	if(!empty($userid) && $userid !=''){

		$variable_data = $this->Common_model->getAllorderby('variable_master','vm_id','desc');
		
		if(!empty($variable_data) && $variable_data !=''){

			$data_key =  array() ;
			$data_val =  array() ;
			foreach($variable_data as $key=> $val){

				$data_key[]  = $key;
				$data_val[]  = $val;
			}

			$data['variable_data'] = $variable_data;
			
			$this->template->set('title', 'Admin Panel - Variable master');
			$this->template->load('admin_dashboard_layout', 'contents' , 'variable_master', $data);
		}else{
			$this->template->set('title', 'Admin Panel - Variable master');
			$this->template->load('admin_dashboard_layout', 'contents' , 'variable_master');
		}

	}else{
		redirect('admin/login');
	}

}

public function variable_add(){
	$checkLogin = $this->session->userdata('logged_ins')['id'];
	if (!empty($checkLogin) && $checkLogin != '') {

		$this->form_validation->set_rules('vm_name', 'variable name', 'required');

		if ($this->form_validation->run() == TRUE){

			$submit = isset($_POST['submit']) ? $_POST['submit'] : "";
			if(isset($submit) && $submit !=''){

				$vm_name = isset($_POST['vm_name']) ? $_POST['vm_name'] : "";
				$vm_value = isset($_POST['vm_value']) ? $_POST['vm_value'] : "";
				$vm_value_temp = isset($_POST['vm_value_temp']) ? $_POST['vm_value_temp'] : "";
				$vm_staus = 1;
				$vm_add_date = date('Y-m-d H:i:s');
				$vm_update_date = date('Y-m-d H:i:s');
				
				$user_id = isset($checkLogin) ? $checkLogin : "";
					
				$insert_arr = array(
							'vm_user_id' => $user_id,
							'vm_name'	 => trim($vm_name),
							'vm_value'	 => $vm_value,
							'vm_value_temp' => $vm_value_temp,
							'vm_staus' => $vm_staus,
							'vm_add_date' => trim($vm_add_date),
							'vm_update_date' => trim($vm_update_date),
						);

				$insertId = $this->Common_model->insertData('variable_master', $insert_arr);
				if (!empty($insertId) || $insertId  !='') {
					$messge = "Variable has been insert successfully";
					$this->session->set_flashdata('success_msg', $messge);
					$variable_master= base_url().'admin/variable_master';
					redirect($variable_master);
				}

			}

		}

		$this->template->set('title', 'Variable master add');
		$this->template->load('admin_dashboard_layout', 'contents' , 'variable_create');
	}
}


public function variable_edit($vm_id = ''){

	$checkLogin = $this->session->userdata('logged_ins')['id'];

	if (!empty($checkLogin) && $checkLogin != '') {
	
		$user_id = isset($checkLogin) ? $checkLogin : "";
		$vm_id = isset($vm_id) ? $vm_id : "";
		$update = isset($_POST['submit']) ? $_POST['submit'] : "";
		

		if(!empty($vm_id) && $vm_id !=''){
			$data['variable_data'] = $this->Common_model->getsingle("variable_master", array("vm_id" => $vm_id));
		}else{
			$data =array();
		}

		if(!empty($vm_id) && $vm_id !=''){

			if(isset($update) && $update !='' && $update=='update'){

				$vm_value = isset($_POST['vm_value']) ? $_POST['vm_value'] : "";
				$vm_value_temp = isset($_POST['vm_value_temp']) ? $_POST['vm_value_temp'] : "";
				$vm_staus = isset($_POST['vm_staus']) ? $_POST['vm_staus'] : "";
				$vm_update_date	= date('Y-m-d H:i:s');	

				$variable_update_arr = array(
																'vm_user_id' => $user_id,
																'vm_value' => $vm_value,
																'vm_value_temp' => $vm_value_temp,
																'vm_staus'	=> $vm_staus,
																'vm_update_date'	=> $vm_update_date
															);

					$update = $this->Common_model->updateData('variable_master',$variable_update_arr,array('vm_id' => $vm_id));
					if (!empty($update) || $update  !='') {
						$messge = "Variable has been update successfully";
						$this->session->set_flashdata('success_msg', $messge);
						$variable_master= base_url().'admin/variable_master';
						redirect($variable_master);
					}

			}
		}

		//_dx($data);
		$this->template->set('title', 'Variable master edit');
		$this->template->load('admin_dashboard_layout', 'contents' , 'variable_create', $data);

	}else{
		redirect('admin/login');
	}
}

public function delete_variable(){
	$data['menuactive'] = $this->uri->segment(1);
	$segment = $this->uri->segment("3");

	if(!empty($segment) && $segment !=''){
		//_dx($segment);
		$result =$this->Common_model->delete("variable_master",array('vm_id' => $segment));
		
		if(!empty($result) && $result !=''){
			$messge = "variable has been deleted successfully";
			$this->session->set_flashdata('success_msg', $messge);
			redirect('variable_master');
		}
	}
}

function validate_variableData(){

	if ($this->input->post("vm_name") != '') {
		$this->form_validation->set_rules('vm_name', 'vm_name', 'required|is_unique[variable_master.vm_name]');
		if ($this->form_validation->run() == true) {
		die('true');
		} else {
		die('false');
		}
	}
	
}


}
?>
