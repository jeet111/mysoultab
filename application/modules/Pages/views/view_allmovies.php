<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.css">
<aside class="right-side new-music mus_list">
	<div class="searchbar-wrap">
		<div class="src_sbmit">
			<input autocomplete="off" class="form-control" id="movie_search" name="movie_search" placeholder="Search Movie" value="" type="text">
			<button type="submit" id="btn_music" value="Submit"><i class="fa fa-search"></i></button>
		</div>
	</div>
	<div id="playthis">
		<?php if($type=='popular'){ ?>
		<!-- By popular -->
		<section class="doc_cate">
			<div class="row">
				<div class="col-md-9">
					<div class="bx_leftScrollmain mCustomScrollbar">
						<div class="bx_leftScroll">
							<div class="bx_viewAll">
								
								<div class="inside-pageinfo-wrap" >
									<div class="bx_moviMain">
										<video  controls autoplay style="width:100%; height:auto;" >
											<source id="moivedata" data-myval="" src="<?php echo base_url().'uploads/movie/'.$PlayMovie[0]->playmovie_file; ?>" type="video/mp4">
											
										</video>
									</div>
									<div class="bx_movName">
										<h5><?php echo $PlayMovie[0]->playmovie_title; ?></h5>
										<span><?php echo $PlayMovie[0]->playmovie_views; ?> views</span>
									</div>
									<div class="mov_dec">
										<h4>Description</h4>
										<p><?php echo $PlayMovie[0]->playmovie_desc; ?></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 mrg_non">
					<div class="bx_right bx_movRight">
						<div class="up_box">
							<h5 class="pull-left">Up next</h5>
							<div class="back-info">
								<a href="javascript:window.history.back()"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</a>
							</div>
						</div>
						<div class="listing_sng mCustomScrollbar">
							<ul class="list_songs">
								<?php foreach ($popular_array as $popular) { ?>
								<li>
									
									<div class="queue-movie">
										<div class="mov_qu">
											<a href="javascript:void(0);" onclick="getId('<?php echo $popular->movie_id; ?>')">
												<img alt="" src="<?php echo base_url().'uploads/movie/image/'.$popular->movie_image; ?>">
												<span class="iconplay"><i class="fa fa-play-circle"></i></span>
											</a>
											
										</div>
										<div class="nam_view">
											
											<h5 >
											<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($popular->movie_id); ?>">
												<?php echo $popular->movie_title; ?>
											</a>
											</h5>
											
											<span><?php echo $popular->movie_view; ?> Views</span>
											
										</div>
									</div>
								</a>
							</li>
							<?php } ?>
							
							
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php }elseif ($type=='recent'){?>
	<!-- By recent  -->
	<section class="doc_cate">
		<div class="row">
			<div class="col-md-9">
				<div class="bx_leftScrollmain mCustomScrollbar">
					<div class="bx_leftScroll">
						<div class="bx_viewAll">
							
							<div class="inside-pageinfo-wrap" >
								
								<div class="bx_moviMain">
									<video  controls autoplay style="width:100%; height:auto;" >
										<source id="moivedata" data-myval="" src="<?php echo base_url().'uploads/movie/'.$PlayMovie[0]->playmovie_file; ?>" type="video/mp4">
										
									</video>
								</div>
								<div class="bx_movName">
									<h5><?php echo $PlayMovie[0]->playmovie_title; ?></h5>
									<span><?php echo $PlayMovie[0]->playmovie_views; ?> views</span>
								</div>
								<div class="mov_dec">
									<h4>Description</h4>
									<p><?php echo $PlayMovie[0]->playmovie_desc; ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 mrg_non">
				<div class="bx_right bx_movRight">
					<div class="up_box">
						<h5 class="pull-left">Up next</h5>
						<div class="back-info">
							<a href="javascript:window.history.back()"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</a>
						</div>
					</div>
					<div class="listing_sng mCustomScrollbar">
						<ul class="list_songs">
							<?php foreach ($recent_array as $recent) { ?>
							<li>
								
								<div class="queue-movie">
									<div class="mov_qu">
										<a href="javascript:void(0);" onclick="getId('<?php echo $recent->movie_id; ?>')">
											<img alt="" src="<?php echo base_url().'uploads/movie/image/'.$recent->movie_image; ?>">
											<span class="iconplay"><i class="fa fa-play-circle"></i></span>
										</a>
										
									</div>
									<div class="nam_view">
										
										<h5 >
										
										<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($recent->movie_id); ?>">
											<?php echo $recent->movie_title; ?>
										</a>
										</h5>
										
										<span><?php echo $recent->movie_view; ?> Views</span>
										
									</div>
								</div>
							</a>
						</li>
						<?php } ?>
						
						
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<?php }elseif ($type=='my') { ?>
<!-- By my movies -->
<section class="doc_cate">
	<div class="row">
		<div class="col-md-9">
			<div class="bx_leftScrollmain mCustomScrollbar">
				<div class="bx_leftScroll">
					<div class="bx_viewAll">
						
						<div class="inside-pageinfo-wrap" >
							
							<div class="bx_moviMain">
								<video  controls autoplay style="width:100%; height:auto;" >
									<source id="moivedata" data-myval="" src="<?php echo base_url().'uploads/movie/'.$PlayMovie[0]->playmovie_file; ?>" type="video/mp4">
									
								</video>
							</div>
							<div class="bx_movName">
								<h5><?php echo $PlayMovie[0]->playmovie_title; ?></h5>
								<span><?php echo $PlayMovie[0]->playmovie_views; ?> views</span>
							</div>
							<div class="mov_dec">
								<h4>Description</h4>
								<p><?php echo $PlayMovie[0]->playmovie_desc; ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 mrg_non">
			<div class="bx_right bx_movRight">
				<div class="up_box">
					<h5 class="pull-left">Up next</h5>
					<div class="back-info">
						<a href="javascript:window.history.back()"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</a>
					</div>
				</div>
				<div class="listing_sng mCustomScrollbar">
					<ul class="list_songs">
						<?php foreach ($UserMovieData as $mymovie) { ?>
						<li>
							
							<div class="queue-movie">
								<div class="mov_qu">
									<a href="javascript:void(0);" onclick="getId('<?php echo $mymovie->movie_id; ?>')">
										<img alt="" src="<?php echo base_url().'uploads/movie/image/'.$mymovie->movie_image; ?>">
										<span class="iconplay"><i class="fa fa-play-circle"></i></span>
									</a>
									
								</div>
								<div class="nam_view">
									
									<h5 >
									<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($mymovie->movie_id); ?>">
										<?php echo $mymovie->movie_title; ?>
									</a>
									</h5>
									
									<span><?php echo $mymovie->movie_view; ?> Views</span>
									
								</div>
							</div>
						</a>
					</li>
					<?php } ?>
					
					
				</ul>
			</div>
		</div>
	</div>
</div>
</section>
<?php }elseif ($type=='favorite') {?>
<!-- By favorite -->
<section class="doc_cate">
<div class="row">
	<div class="col-md-9">
		<div class="bx_leftScrollmain mCustomScrollbar">
			<div class="bx_leftScroll">
				<div class="bx_viewAll">
					
					<div class="inside-pageinfo-wrap">
						
						<div class="bx_moviMain">
							<video  controls autoplay style="width:100%; height:auto;" >
								<source id="moivedata" data-myval="" src="<?php echo base_url().'uploads/movie/'.$PlayMovie[0]->playmovie_file; ?>" type="video/mp4">
								
							</video>
						</div>
						<div class="bx_movName">
							<h5><?php echo $PlayMovie[0]->playmovie_title; ?></h5>
							<span><?php echo $PlayMovie[0]->playmovie_views; ?> views</span>
						</div>
						<div class="mov_dec">
							<h4>Description</h4>
							<p><?php echo $PlayMovie[0]->playmovie_desc; ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3 mrg_non">
		<div class="bx_right bx_movRight">
			<div class="up_box">
				<h5 class="pull-left">Up next</h5>
				<div class="back-info">
					<a href="javascript:window.history.back()"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</a>
				</div>
			</div>
			<div class="listing_sng mCustomScrollbar">
				<ul class="list_songs">
					<?php foreach ($UserFavMovie as $FavMovie) { ?>
					<li>
						
						<div class="queue-movie">
							<div class="mov_qu">
								<a href="javascript:void(0);" onclick="getId('<?php echo $FavMovie->movie_id; ?>')">
									<img alt="" src="<?php echo base_url().'uploads/movie/image/'.$FavMovie->movie_image; ?>">
									<span class="iconplay"><i class="fa fa-play-circle"></i></span>
								</a>
								
							</div>
							<div class="nam_view">
								
								<h5 >
								<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($FavMovie->movie_id); ?>">
									<?php echo $FavMovie->movie_title; ?>
								</a>
								</h5>
								
								<span><?php echo $FavMovie->movie_view; ?> Views</span>
								
							</div>
						</div>
					</a>
				</li>
				<?php } ?>
				
				
			</ul>
		</div>
	</div>
</div>
</div>
</section>
<?php }else{ ?>
<!-- By Categories  -->
<section class="doc_cate">
<div class="row">
<div class="col-md-9">
	<div class="bx_leftScrollmain mCustomScrollbar">
		<div class="bx_leftScroll">
			<div class="bx_viewAll">
				
				<?php if(!empty($MoviesBycategories)){ ?>
				<div class="inside-pageinfo-wrap" >
					<div class="bx_moviMain">
						<video  controls autoplay style="width:100%; height:auto;" >
							<source id="moivedata" data-myval="" src="<?php echo base_url().'uploads/movie/'.$PlayMovie[0]->playmovie_file; ?>" type="video/mp4">
							
						</video>
					</div>
					<div class="bx_movName">
						<h5><?php echo $PlayMovie[0]->playmovie_title; ?></h5>
						<span><?php echo $PlayMovie[0]->playmovie_views; ?> views</span>
					</div>
					<div class="mov_dec">
						<h4>Description</h4>
						<p><?php echo $PlayMovie[0]->playmovie_desc; ?></p>
					</div>
				</div>
				<?php }else{ ?>

					<div class="photo-list-empty">
		    	<span class="blnk_photo"><i class="fa fa-picture-o" aria-hidden="true"></i></span>
		    	<h4>Movie not found</h4>  
		    </div>

				
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<div class="col-md-3 mrg_non">
	<div class="bx_right bx_movRight">
		<div class="up_box">
			<h5 class="pull-left">Up next</h5>
			<div class="back-info">
				<a href="javascript:window.history.back()"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</a>
			</div>
		</div>
		<div class="listing_sng mCustomScrollbar">
			
			<?php if(!empty($MoviesBycategories)){ ?>
			<ul class="list_songs">
				<?php foreach ($MoviesBycategories as $movie) { ?>
				<li>
					
					<div class="queue-movie">
						<div class="mov_qu">
							<a href="javascript:void(0);" onclick="getId('<?php echo $movie->movie_id; ?>')">
								<img alt="" src="<?php echo base_url().'uploads/movie/image/'.$movie->movie_image; ?>">
								<span class="iconplay"><i class="fa fa-play-circle"></i></span>
							</a>
							
						</div>
						<div class="nam_view">
							
							<h5 >
							<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_movie/<?php echo base64_encode($movie->movie_id); ?>">
								<?php echo $movie->movie_title; ?>
							</a>
							</h5>
							
							<span><?php echo $movie->movie_view; ?> Views</span>
							
						</div>
					</div>
				</a>
			</li>
			<?php } ?>
			
			
		</ul>
		<?php }else{ ?>

			
		<?php echo "<center>Movies Not Found.</center>"; ?>
		<?php } ?>
	</div>
</div>
</div>
</div>
</section>
<?php } ?>
</div>
</aside>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->
<script src="<?php echo base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script> -->
<script>
		(function($){
			$(window).on("load",function(){
				
				$("a[rel='load-content']").click(function(e){
					e.preventDefault();
					var url=$(this).attr("href");
					$.get(url,function(data){
						$(".content .mCSB_container").append(data); //load new content inside .mCSB_container
						//scroll-to appended content
						$(".content").mCustomScrollbar("scrollTo","h2:last");
					});
				});
				
				$(".content").delegate("a[href='top']","click",function(e){
					e.preventDefault();
					$(".content").mCustomScrollbar("scrollTo",$(this).attr("href"));
				});
				
			});
		})(jQuery);
</script>
<script type="text/javascript">
	function getId(movie_id) {
			var pathname = window.location.pathname;
			var n = pathname.lastIndexOf('/');
			var type = pathname.substring(n + 1);
			$.ajax({
type: "POST",
url: '<?php echo base_url(); ?>Pages/Movie/Paymovie/',
data: {'movie_id': movie_id,'type':type},
//dataType: "json",
success: function(data)
{
$("#playthis").html(data);
}
});
}

$(document).on('click','#btn_music',function(e){
var movie_search = $("#movie_search").val();
var segment = "<?php echo $this->uri->segment("2"); ?>";

window.location = "<?php echo base_url(); ?>view_allmovies/"+segment+"/"+movie_search;
});
</script>