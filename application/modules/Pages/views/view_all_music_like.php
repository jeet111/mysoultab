
<aside class="right-side new-music mus_list">
	<div class="searchbar-wrap">
		<div class="src_sbmit">
				<input autocomplete="off" class="form-control" id="music_search" name="music_search" placeholder="Search Music" value="<?php echo $segment2; ?>" type="text">
				 <button type="submit" id="btn_music" value="Submit"><i class="fa fa-search"></i></button>				
			</div>
	</div>
	<section class="doc_cate">
		<div class="row">
			<div class="col-md-9">
				<div class="bx_leftScrollmain mCustomScrollbar">
				<div class="bx_leftScroll">
					<div class="bx_viewAll">
						<div class="inside-pageinfo-wrap">
							<div class="inside-pageinfo-lhs">
								<div class="image-cont colimg ">
								<?php if($segment == 'my_music'){ ?>
									<img id="" src="<?php echo base_url(); ?>uploads/music/image/<?php echo $single_music_image->music_image; ?>" alt="">
								<?php }else if($segment == 'recent_music' || $segment == 'popular_music'){ ?>
<img id="" src="<?php echo base_url(); ?>uploads/music/image/<?php echo $single_music_image[0]->music_image; ?>" alt="">
                                <?php }elseif($segment == 'favorite_music'){ ?>
	<img id="" src="<?php echo base_url(); ?>uploads/music/image/<?php echo $single_music_image[0]['music_image']; ?>" alt="">				
								
								<?php } else{ ?>
								<img id="" src="<?php echo base_url(); ?>uploads/music/image/<?php echo $single_music_image->music_image; ?>" alt="">
								<?php } ?>
									
									<span class="but-play icon-play-filled-trans-black"></span>
									<span class="blk-bg-trans"></span>
								</div>
							</div>
							<div class="inside-pageinfo-rhs">
								<div class="pageinfo-top back_common">
								<div class="back-info"><a href="javascript:window.history.back()"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</a></div>
									<div class="pageinfo-top-lhs">
										<h1><?php if($segment == 'my_music'){ echo 'My Music'; }else if($segment == 'popular_music'){ echo 'Popular Music'; }else if($segment == 'favorite_music'){ echo 'Favorite Music'; }else if($segment == 'recent_music'){ echo 'Recent Music';} ?> </h1> 
										<div class="myplaylist_totalsong"><?php echo count($view_all_music); ?> Songs</div>
									</div>
									<div class="pageinfo-top-rhs">
										<span class="icon-but-overflow"></span>
									</div>
								</div>
								<!--<div class="pageinfo-bottom">
									<button><i class="fa fa-play"></i> Play All</button>
									<button class="white-button"><i class="fa fa-download"></i> Download</button>
								</div>-->
							</div>
						</div>

<div id="combine_tab">
						<div class="queue-header">						
							<div class="main_chk">
								<div class="bx_chkAll">
									<span class="select-input">
										<input class="chk_bx" id="checkall_playlist" type="checkbox">
										<span> Select All</span>
									</span>
								</div>
								<div class="bx_rgt">
									<a href="javascript:void(0);" class="add_to_playlist">Add to playlist</a>									
								</div>
							</div>
						</div>
						<ul class="list_songs">
						
						<?php  
						 if(!empty($view_all_music)){
						foreach($view_all_music as $view_music){
						
						?>
							<li>
								<div class="select-input_one"><input type="checkbox" name="sub_chk_playlist[]" value="<?php echo $view_music['music_id']; ?>" data-id="<?php echo $view_music['music_id']; ?>" class="sub_chk_playlist"></div>
								<div class="queue-songlist-lhs">
									<div class="queue-songlist-img">
										<div class="song-current">
										<?php if(!empty($view_music['music_file'])){ ?>
											<img alt="" src="<?php echo base_url(); ?>uploads/music/image/<?php echo $view_music['music_image']; ?>" class="mCS_img_loaded">
										<?php }else{ ?>
										
										<img src="<?php echo base_url();?>assets/images/no-image-available.png" class="" alt="">
										
										<?php } ?>
										</div>
									</div>
									<div class="queue-songlist-info">
										<p><a class="dropdown-item" id="play_now_<?php echo $view_music['music_id']; ?>" data-music="<?php echo $view_music['music_id']; ?>" href="#"><?php echo $view_music['music_title']; ?></a></p>
										<span><a class="dropdown-item" id="play_now_<?php echo $view_music['music_id']; ?>" data-music="<?php echo $view_music['music_id']; ?>" href="#"><?php echo $view_music['music_artist']; ?></a></span>
									</div>
								</div>
								<div class="queue-songlist-rhs queue-play-list">
									<div class="btn-group new-music-btn">
										<a class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
										<div class="dropdown-menu">
											<a class="dropdown-item" id="play_now_<?php echo $view_music['music_id']; ?>" data-music="<?php echo $view_music['music_id']; ?>" href="#">Play Now</a>
											<a href="#" id="downloadd_music_<?php echo $view_music['music_id']; ?>" class="dropdown-item" data-music-id="<?php echo $view_music['music_id']; ?>">Download</a>
											<a class="dropdown-item" href="<?php echo base_url(); ?>getinfo_music/<?php echo base64_encode($view_music['music_id']); ?>">Get info</a>
										</div>
									</div>
								</div>
								<div class="songlist-rhs">
		<span class="duration"><?php echo calculateFileSize('uploads/music/'.$view_music['music_file']); ?></span>
									
									<span class="icon-download-cont">
										<a href="#" class="download_music_<?php echo $view_music['music_id']; ?>" data-music-id="<?php echo $view_music['music_id']; ?>"><i class="fa fa-download"></i></a>
									</span>									
								</div>
							</li>
						 <?php }}else{ ?>
						<div class="photo-list-empty">
		    	<span class="blnk_photo"><i class="fa fa-picture-o" aria-hidden="true"></i></span>
		    	<h4>Music not found</h4>
		    </div>
						 <?php } ?>	
						</ul>
					</div>	</div>
			</div>
		</div>
			</div>
			<div class="col-md-3 mrg_non">
				<div class="bx_right" id="upload_music">	
<?php
           if(!empty($playlist_music)){ ?>				
					<div class="queue-header">
						<div class="hd_que">
							<h5 class="pull-left">Queue</h5>
						</div>
						<div class="main_chk">
							<div class="bx_chkAll">
								<span class="select-input">
									<input class="chk_bx" id="checkall" type="checkbox">
									<span> Select All</span>
								</span>
							</div>
							<div class="bx_rgt">
								
								<a href="javascript:void(0);" class="clear-queue-popover btn_delete" data-id="" id="btn_delete"> Remove</a>
							</div>
						</div>
					</div>


					<div class="listing_sng mCustomScrollbar">
						<ul class="list_songs playlist1" id="playlist">
							<?php
                         if(!empty($playlist_music)){
							foreach($playlist_music as $playlist){ ?>
							<li class="active test_play">
								<div class="select-input_one"><input type="checkbox" name="sub_chk[]" value="<?php echo $playlist->music_playlist_id; ?>" data-id="<?php echo $playlist->music_playlist_id; ?>" class="sub_chk" ></div>
								<a href="<?php echo base_url(); ?>uploads/music/<?php echo $playlist->music_file; ?>"><div class="queue-songlist-lhs">
									<div class="queue-songlist-img">
										<div class="song-current">
											<img class="click_image_change_<?php echo $playlist->music_id; ?>" data-image-change="<?php echo $playlist->music_id; ?>" alt="" src="<?php echo base_url(); ?>uploads/music/image/<?php echo $playlist->music_image; ?>">
										</div>
									</div>
									<div class="queue-songlist-info">
										<p class="click_image_change_<?php echo $playlist->music_id; ?>" data-image-change="<?php echo $playlist->music_id; ?>"><?php echo $playlist->music_title; ?></p>
										<span class="click_image_change_<?php echo $playlist->music_id; ?>" data-image-change="<?php echo $playlist->music_id; ?>"><?php echo $playlist->music_artist; ?></span>
									</div>
								</div></a>
								<div class="queue-songlist-rhs queue-play-list">
									<div class="btn-group new-music-btn">
										<button class="mus-new dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
										<div class="dropdown-menu">
											
											<button class="dropdown-item" href="javascript:void(0);" id="remove_play_now_<?php echo $playlist->music_playlist_id; ?>" data-music-remove="<?php echo $playlist->music_playlist_id; ?>">Remove</button>
											
											<button class="dropdown-item download_music_<?php echo $playlist->music_id; ?>" id="testi_<?php echo $playlist->music_id; ?>" href="<?php echo base_url(); ?>Pages/Music/download_music/<?php echo $playlist->music_id; ?>"  data-music-id="<?php echo $playlist->music_id; ?>" >Download</button>
											
											<button class="dropdown-item" href="javascript:void(0);" id="getinfo_m_<?php echo $playlist->music_id ?>" data-getinfo="<?php echo base64_encode($playlist->music_id); ?>">Get info</button>
										</div>
									</div>
								</div>
							</li>
							
						<?php } ?>

						 <?php }else{ ?>	
						<div class="music_norecord">No record found</div> 
						 <?php } ?>
						</ul>
					</div>


					<div class="bx_mus">
						
							<div class="ply_sng">
							<div class="sng-crnt">
								<?php if($single_playlist_music->music_image){ ?>
								<img alt="" src="<?php echo base_url(); ?>uploads/music/image/<?php echo $single_playlist_music->music_image; ?>">
							<?php }else{ ?>
							<img src="<?php echo base_url().'uploads/music/defualt_music.png' ?>">
							<?php } ?>
							</div>
							<div class="sng-txt">
								<h4><?php echo $single_playlist_music->music_title; ?></h4>
								<p><?php echo $single_playlist_music->music_artist; ?></p>
							</div>
						</div>
						

						<audio id="audio" preload="auto" tabindex="0" controls="" type="audio/mpeg">
        <source type="audio/mp3" id="audiotest" src="<?php echo base_url(); ?>uploads/music/<?php echo $single_playlist_music->music_file; ?>">
        Sorry, your browser does not support HTML5 audio.
    </audio>
						
					</div>
		   <?php }else{ ?>
		    <div class="play-list-empty">
		    	<span class="blnk_queue"><i class="fa fa-music" aria-hidden="true"></i></span>
		    	<h4>Your queue is empty</h4>
		    </div>
		   <?php } ?>

				</div>
			</div>
		</div>

	</section>
</aside>

<script>
   
   $(document).on('click','[id^="play_now_"]',function(){ 

var music_id = $(this).attr("data-music");
$.ajax({
    url:"<?php echo base_url().'play_music'?>",
    method:"POST",
    data:{music_id:music_id},
    success:function(data)
    {
		 $("#upload_music").show();
        $("#upload_music").html(data);
    }
   })

 }); 
   
     $(document).on('click','[id^="downloadd_music_"]',function(){ 
	 
	 var music_id = $(this).attr("data-music-id");
window.location = "<?php echo base_url(); ?>Pages/Music/download_music/"+music_id;

 });
   
   	$(document).on('click','.btn_delete', function(e){
		
		var bodytype = $('input[name="sub_chk[]"]:checked').map(function () {  
        return this.value;
        }).get().join(",");
		if(bodytype != ''){
	if(confirm("Are you sure you want to remove this audio?")){	
		$.ajax({
                url: '<?php echo base_url(); ?>Pages/Music/delete_checkboxes',
                type: 'POST',
                data: {bodytype:bodytype},
				dataType: "json",
                success: function(data) { //alert('swati');
                    
					
					 setTimeout(function(){
            $("#expire_msg").html("<div class='alert alert-success'>Successfully deleted !</div>");
			
			$("#upload_music").load(location.href + " #upload_music");
            //location.reload();
//window.location.href = email_url+"email_list";
}, 1000);
                }
            });
			
			}
		}else{
			alert("Please select atleast one audio.");
		}
		
		
  });
   
    $(document).on('click','#checkall',function(){
        if($(this).is(':checked',true))  
        { 
            $(".sub_chk").prop('checked', true); 
        }  
        else  
        { 
            $(".sub_chk").prop('checked',false);  
        }  
    });
   //jQuery('.sub_chk').on('click',function(){
	  $(document).on('click','#checkall',function(){   
        if($('.sub_chk:checked').length == $('.sub_chk').length){
            $('#checkall').prop('checked',true);
        }else{
            $('#checkall').prop('checked',false);
        }
    });
	
	$(document).on('click','#checkall_playlist',function(){
        if($(this).is(':checked',true))  
        { 
            $(".sub_chk_playlist").prop('checked', true); 
        }  
        else  
        { 
            $(".sub_chk_playlist").prop('checked',false);  
        }  
    });
   //jQuery('.sub_chk').on('click',function(){
	  $(document).on('click','#checkall_playlist',function(){   
        if($('.sub_chk_playlist:checked').length == $('.sub_chk_playlist').length){
            $('#checkall_playlist').prop('checked',true);
        }else{
            $('#checkall_playlist').prop('checked',false);
        }
    });
   
   <?php
    if(!empty($playlist_music)){ ?>
var audio;
var playlist;
var tracks;
var current;

init();
function init(){
    current = 0;
    audio = $('audio');
    playlist = $('.playlist1');
    tracks = playlist.find('li a');
    len = tracks.length - 1;
    audio[0].volume = .10;
    audio[0].play();
    playlist.find('a').click(function(e){ 
        e.preventDefault();
        link = $(this);
        current = link.parent().index();
        run(link, audio[0]);
    });
    audio[0].addEventListener('ended',function(e){
       
        if(current == len){
            current = 0;
            link = playlist.find('a')[0];
        }else{
			 current++;
            link = playlist.find('a')[current];    
        }
        run($(link),audio[0]);
    });
}

function run(link, player){
        player.src = link.attr('href');
		
        par = link.parent();
        par.addClass('active').siblings().removeClass('active');
        audio[0].load();
        audio[0].play();
}
	<?php } ?>
	
	$(document).on('click','[id^="testi_"]',function(){ 
	 var music_id = $(this).attr("data-music-id");
window.location = "<?php echo base_url(); ?>Pages/Music/download_music/"+music_id;
	});
	
   
   
   $(document).on('click','[class^="download_music_"]',function(){
	 var music_id = $(this).attr("data-music-id");
window.location = "<?php echo base_url(); ?>Pages/Music/download_music/"+music_id;

 });
 
 $(document).on('click','.add_to_playlist',function(){ 
 
var bodytype = $('input[name="sub_chk_playlist[]"]:checked').map(function () {  
        return this.value;
        }).get().join(",");
		
	$.ajax({
    url:"<?php echo base_url().'Pages/Music/add_to_playlist'?>",
    method:"POST",
    data:{bodytype:bodytype},
    success:function(data)
    {
		
		$("#combine_tab").load(location.href + " #combine_tab");
		$("#upload_music").load(location.href + " #upload_music");
    }
   })
	
 });
 
 
 $(document).on('click','[id^="play_now_"]',function(){ 

var music_id = $(this).attr("data-music");
$.ajax({
    url:"<?php echo base_url().'play_music'?>",
    method:"POST",
    data:{music_id:music_id},
    success:function(data)
    {
		$("#upload_music").show();
        $("#upload_music").html(data);
		
    }
   })

 }); 
 
$(document).on('click','[id^="remove_play_now_"]',function(){
	
 var music_id = $(this).attr("data-music-remove");
 
 if(confirm("Are you sure you want to remove this audio?")){
        
$.ajax({
    url:"<?php echo base_url().'Pages/Music/remove_play_music'?>",
    method:"POST",
    data:{music_id:music_id},
    success:function(data)
    {
	  /*if(data == 2){
		   $("#upload_music").html("<div class='play-list-empty'>Your Queue is empty</div>");
		 
	   }*/
	    setTimeout(function(){
            $("#expire_msg").html("<div class='alert alert-success'>Successfully deleted !</div>");
			
			$("#upload_music").load(location.href + " #upload_music");
            //location.reload();
//window.location.href = email_url+"email_list";
}, 1000);
    }
	
   })
   
   //$(this).parents(".test_play").animate({ backgroundColor: "#fbc7c7" }, "fast")
			//.animate({ opacity: "hide" }, "slow");
}

});

$(document).on('click','#btn_music',function(e){ 
	var music_search = $("#music_search").val();
	var music_segment = "<?php echo $this->uri->segment("2"); ?>";
	
window.location = "<?php echo base_url(); ?>view_all_music/"+music_segment+'/'+music_search;	
	}); 

$(document).on('click','[id^="getinfo_m_"]',function(){
	 var music_id = $(this).attr("data-getinfo");
window.location = "<?php echo base_url(); ?>getinfo_music/"+music_id;

 });	
   
		(function($){
			$(window).on("load",function(){
				
				$("a[rel='load-content']").click(function(e){
					e.preventDefault();
					var url=$(this).attr("href");
					$.get(url,function(data){
						$(".content .mCSB_container").append(data); //load new content inside .mCSB_container
						//scroll-to appended content 
						$(".content").mCustomScrollbar("scrollTo","h2:last");
					});
				});
				
				$(".content").delegate("a[href='top']","click",function(e){
					e.preventDefault();
					$(".content").mCustomScrollbar("scrollTo",$(this).attr("href"));
				});
				
			});
		})(jQuery);
	</script>



<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.css">
<script src="<?php echo base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>