<?php echo $singleData['description']; ?> 

<div class="more_butt_sec-ar ne_d_i">
  <span class="down_aro">
    <a href="#next_sec2"> <img src="<?php echo base_url();?>assets/images/down_arroe.png"> </a></span>
</div>



<section  class="our_plan_s" data-aos="fade-up">
    <div class="container">

     <div id="next_sec2"></div>
     

     <div   id="section2"  class="bigHeading wow fadeInDown animated">
      <div class="bigText">Plans</div>
      <h2 class="margin-bottom-xxl text-center">Choose your<strong> Subscription</strong></h2>
  </div>




  <div class="our_plan_s_main">



    <div id="generic_price_table">   
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!--PRICE HEADING START-->

                        <!--//PRICE HEADING END-->
                    </div>
                </div>
            </div>
            <div class="container">

                <!--BLOCK ROW START-->
                <div class="row">

                  <div class="price_tab_s_new  wow fadeInDown animated">

                    <?php foreach ($subscriptionData as $sub) { ?>
                        <div class="col-md-4">

                            <!--PRICE CONTENT START-->
                            <div class="generic_content clearfix">

                                <!--HEAD PRICE DETAIL START-->
                                <div class="generic_head_price clearfix">

                                    <!--HEAD CONTENT START-->
                                    <div class="generic_head_content clearfix">

                                        <!--HEAD START-->
                                        <div class="head_bg"></div>
                                        <div class="head">
                                            <span><?php echo $sub->plan_name; ?></span>
                                        </div>
                                        <!--//HEAD END-->

                                    </div>
                                    <!--//HEAD CONTENT END-->

                                    <!--PRICE START-->
                                    <div class="generic_price_tag clearfix">    
                                        <span class="price">
                                            <span class="sign">$</span>
                                            <span class="currency"><?php echo $sub->amount; ?></span>
                                            <!-- <span class="cent">.99</span> -->
                                            <!-- <span class="month">/MON</span> -->
                                        </span>
                                    </div>
                                    <!--//PRICE END-->

                                </div>                            
                                <!--//HEAD PRICE DETAIL END-->

                                <!--FEATURE LIST START-->
                                <div class="generic_feature_list">
                                    <ul>
                                        <li><span><?php echo $sub->description; ?></span></li>
                                            <!-- <li><span>Lorem Ipsum</span>  is simply dummy</li>
                                            <li><span>Lorem Ipsum</span>  is simply dummy</li>
                                            <li><span>Lorem Ipsum</span>  is simply dummy</li>
                                            <li><span>Lorem Ipsum</span>  is simply dummy</li> -->
                                        </ul>
                                    </div>
                                    <!--//FEATURE LIST END-->

                                    <!--BUTTON START-->

                                    <div class="generic_price_btn clearfix">
                                        <!-- <form action="<?php //echo base_url('signup');?>/<?php //echo base64_encode($sub->id); ?>" method="post"> -->
                                            <!-- <input type="hidden" name="plan_id" value="<?php echo $sub->id; ?>"> -->
                                            <a class="" href="<?php echo base_url('signup');?>/<?php echo base64_encode($sub->id); ?>">Get started</a>
                                            <!-- </form> -->
                                        </div>

                                    </div>

                                </div>

                            <?php } ?>
                        

                      

                </div>
            </div>  

        </div>
    </section>             

</div>


</div>
</div>


</section>




</main>  