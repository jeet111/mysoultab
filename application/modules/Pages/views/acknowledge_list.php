<aside class="right-side">

  <section class="content-header no-margin">

    <h1 class="text-center"><i class="fa fa-list"></i> Acknowledge List <div class="rem_add"><a href="<?php echo base_url(); ?>add_acknowledge" class="btn btn-primary" >Add</a></div></h1>

  </section>

  <section class="content weather-bg news_section">

    <!-- setting start -->
    <div class="col-md-12">
      <div class="col-md-10">
        <h3>Acknowledge List</h3>
        <?php if(!empty($btnsetingArray)){ ?>
          <?php if($btnsetingArray[0]->settings=='1'){ ?>

            <label><input type="radio" name="btnshow" value='1' checked="checked">Now Acknowledge List app showing on the mysoultab APP<br></label>

          <?php }else{ ?>

           <label> <input type="radio" name="btnshow" value='1' checked="checked">Now Acknowledge List app not showing on the mysoultab APP </label>

         <?php } ?>

       <?php } ?> 
     </div>
     <div class="col-md-2 delete-email">
      <button class="btn btn-primary chk">Settings</button>
    </div> 
    <div id="para" style="display: none "> 



      <form id="submit_form" method="post" action="<?php echo base_url()?>savsettings "> 
        <div class="col-md-10">
          <label> <input type="checkbox" name="butn_seting" id="butn_seting" value="">Please check if want to show the Acknowledge List button in mysoultab App </label>
          <input type="hidden" name="btn" value="acknowledge_list">
        </div>
        <div class="col-md-2">
         <input class="btn btn-primary chkdd" type="submit" name="submit" value="submit" >
       </div>
     </form>
     <div id="response"></div>
   </div>

   <!-- </div> -->

   <!-- End -->
 </section>


 <?php if ($this->session->flashdata('success')) { ?>

  <div class="alert alert-success message">

    <button type="button" class="close" data-dismiss="alert">x</button>

    <?php echo $this->session->flashdata('success'); ?></div>

  <?php } ?>

  <section class="content photo-list">

   <div class="photo-listMain reminder_listMain">

    <div class="row swt_cor">

      <div class="col-sm-6">

        <label style="margin-right: 10px;">

          <input type="checkbox" id="checkall">

        </label>

        <div class="delete-email">

         <a href="javascript:void(0);" name="btn_delete" id="btn_delete" class="btn_delete"><i aria-hidden="true" class="fa fa-trash-o"></i></a>

       </div>

     </div>

   </div>



   <div class="table-responsive tb_swt">

    <div id="expire_msg" ></div>

    <table class="table table-hover table-bordered table-mailbox send-user-mail tab_medi" id="sampleTable">

      <thead>

        <tr>

         <th>#</th>

         <th>Date</th>

         <th>Acknowledge Title</th>

         <th>Medicine Name</th>

         <th>Pharma Company Name</th>

         <th>Action</th>

       </tr>

     </thead>

     <tbody>

      <?php

      if(!empty($acknowledge_list)){

       foreach($acknowledge_list as $acknowledge){

         ?>

         <tr>

          <td class="small-col" >

            <input type="checkbox" name="sub_chk[]"  class="sub_chk" value="<?php echo $acknowledge->acknowledge_id; ?>" />

          </td>



          <td><?php echo date('Y-m-d',strtotime($acknowledge->acknowledge_created)); ?></td>

          <td><?php echo $acknowledge->acknowledge_title; ?></td>

          <td><?php echo $acknowledge->medicine_name; ?></td>

          <td><?php echo $acknowledge->name; ?></td>

          <td class="act">

            <a href="<?php echo base_url(); ?>acknowledge_detail/<?php echo $acknowledge->acknowledge_id; ?>" class="edit_btn">View</a>

            <a href="<?php echo base_url(); ?>edit_acknowledge/<?php echo $acknowledge->acknowledge_id; ?>" class="edit_btn">Edit</a>

            <a href="<?php echo base_url(); ?>delete_acknowledge/<?php echo $acknowledge->acknowledge_id; ?>" onclick="return confirm('Are you sure, you want to remove this acknowledge ?')" class="edit_btn">Delete</a>

          </td>

        </tr>



      <?php }} ?>



    </tbody>

  </table>

</div>

</div>

</section>

</aside>



<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>



<script>









  jQuery('#checkall').on('click', function(e) {

    if($(this).is(':checked',true))

    {

      $(".sub_chk").prop('checked', true);

    }

    else

    {

      $(".sub_chk").prop('checked',false);

    }

  });

  jQuery('.sub_chk').on('click',function(){

    if($('.sub_chk:checked').length == $('.sub_chk').length){

      $('#checkall').prop('checked',true);

    }else{

      $('#checkall').prop('checked',false);

    }

  });





  $(document).on('click','.btn_delete', function(e){





    var bodytype = $('input[name="sub_chk[]"]:checked').map(function () {

      return this.value;

    }).get().join(",");



    if(bodytype != ''){

     if(confirm("Are you sure you want to remove this acknowledge ?")){

      $.ajax({

        url: '<?php echo base_url(); ?>Pages/Acknowledge/delete_checkboxes',

        type: 'POST',

        data: {bodytype:bodytype},

				//dataType: "json",

                success: function(data) { //alert('swati');





                setTimeout(function(){

                  $("#expire_msg").html("<div class='alert alert-success message'>Successfully deleted !</div>");

                  location.reload();

//window.location.href = email_url+"email_list";

}, 1300);

              }

            });



    }

  }else{

   alert("Please select atleast one acknowledge.");

 }

});

</script>





<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>





