	<style type="text/css">

           html {

            width: 100%;

            overflow-x: hidden;

            overflow-y: hidden;

        }



    </style>







    <div class="signup-page for_pass_nnn">

		<section class="signup">

            <!-- <img src="images/signup-bg.jpg" alt=""> -->

            <div class="container">

                <div class="signup-content forgot_pass_sec_n">

                    <form method="POST" id="forgot_password" class="forgot-password" action="forgotpassword">

                        <h2 class="form-title">Forgot your password?  Lets get you a new one</h2>

                        <p class="for_subHd">Enter your email below to send you the instruction on how to reset your password.</p>

                        <?php if($this->session->flashdata('message')){

                                ?>

                                <div class="text-center" style="color: green; font-size: 18px;">

                                    <?php echo $this->session->flashdata('message'); ?>

                                </div>

                                <?php

                            }elseif($this->session->flashdata('errorr')){

                                ?>

                                <div class="text-center" style="color: red; font-size: 18px;">

                                    <?php echo $this->session->flashdata('errorr'); ?>

                                </div>

                                <?php

                            }  

                        ?>  

                        <div class="form-group">

                            <input type="text" class="form-input" name="user_email" id="user_email" placeholder="Enter your registered email" value="<?php echo set_value('user_email'); ?>"/>

                            <td class="error"><?php echo form_error('user_email'); ?></td>

                        </div>                       

                        <div class="form-group">

                            <input type="submit" name="submit" id="submit" class="form-submit" value="Send Reset Link"/>

                        </div>

                    </form>

                    <p class="loginhere">

                        Already have an account ?<a href="<?php echo base_url(); ?>login" class="loginhere-link"> Sign in now</a>

                    </p>

                </div>

            </div>

        </section>

	</div>

    <script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>

    <script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

    <script type="text/javascript">

        $(function() {

          $("#forgot_password").validate({

            // Specify validation rules

            rules: {

              user_email: {

                required: true,

                email: true

              }

            },

            // Specify validation error messages

            messages: {

              //email: "Enter your email"

              user_email: {

                required: "Please enter your email",

                email: "Please enter valid email"

              },

            },

            // Make sure the form is submitted to the destination defined

            // in the "action" attribute of the form when valid

            submitHandler: function(form) {

              form.submit();

            }

          });

        });

    </script>

