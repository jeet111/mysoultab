<div class="container-fluid">

  <div class="dash-counter">

    <div class="Schedule_main_one">

      <div class="users-main">

        <h2><?php echo (!empty($plan_detail) ? 'Edit' : 'Add'); ?> FAQ</h2>

        <div class="btn_topBack">

          <a class="cancel-btn btn bk_btn" href="<?php echo base_url().'admin/plan_list'; ?>">

            Back

          </a>

        </div>



        <?php

        if (!empty($success)) {

          ?>

          <div class="alert alert-success alert-dismissible">

           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

           <strong>Success!</strong> <?php echo $success; ?>

         </div>

         <?php

       }



       if (!empty($error)) {

        ?>

        <div class="alert alert-danger alert-dismissible">

         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

         <strong>Error!</strong> <?php echo $error; ?>

       </div>

       <?php

     }

     ?>



     <?php //echo "<pre>"; print_r($plan_detail['id']); die; ?>



     <form class="form-horizontal" action="<?php echo base_url(); ?>admin/add_plan<?php echo (!empty($plan_detail['id']) ? '/'.$plan_detail['id'] : ''); ?>" method="post" enctype="multipart/form-data" name="AddPlan" id="AddPlan">

      <input type="hidden" name="plan_id" value="<?php echo (!empty($plan_detail['id']) ? $plan_detail['id'] : ''); ?>">

      <div class="form-group">

        <label class="control-label col-sm-2" for="plan_name">Plan Name:</label>

        <div class="col-sm-10">

          <input type="text" value="<?php echo (!empty($plan_detail['plan_name']) ? $plan_detail['plan_name'] : ''); ?>" class="form-control" name="plan_name" id="plan_name" placeholder="Enter plan name">

        </div>

      </div>



      <div class="form-group">

        <label class="control-label col-sm-2" for="amount">Amount:</label>

        <div class="col-sm-10">

          <input type="text" value="<?php echo (!empty($plan_detail['amount']) ? $plan_detail['amount'] : ''); ?>" class="form-control" name="amount" id="amount" placeholder="Enter amount">

        </div>

      </div>



      <div class="form-group">

        <label class="control-label col-sm-2" for="plan_days">Days:</label>

        <div class="col-sm-10">

          <input type="text" value="<?php echo (!empty($plan_detail['plan_days']) ? $plan_detail['plan_days'] : ''); ?>" class="form-control" name="plan_days" id="plan_days" placeholder="Enter plan days">

        </div>

      </div>



      <div class="form-group">

        <label class="control-label col-sm-2" for="description">Description:</label>

        <div class="col-sm-10">

          <textarea class="form-control" name="description" id="description" placeholder="Enter description"><?php echo (!empty($plan_detail['description']) ? $plan_detail['description'] : ''); ?></textarea>

        </div>

      </div>



      <div class="form-group">

        <div class="col-sm-offset-2 col-sm-10">

          <button type="submit" name="btnSubmit" value="Submit" class="btn btn-default"><?php echo (!empty($plan_detail) ? 'Update' : 'Submit'); ?></button>

        </div>

      </div>



    </form>

  </div>

</div>

</div>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>



<script>

  $(document).ready(function () {



    $("#AddPlan").validate({

      rules: {

        plan_name: {

          required: true

        },

        amount: {

          required: true

        },

        plan_days: {

          required: true

        },

        description: {

          required: true

        }

      },

      messages: {

        plan_name: {

          required: "This field is required."

        },

        amount: {

          required: "This field is required."

        },

        plan_days: {

          required: "This field is required."

        },

        description: {

          required: "This field is required."

        }

      },

                submitHandler: function (form) { // for demo

                  form.submit();

                }

              });



  });

</script>