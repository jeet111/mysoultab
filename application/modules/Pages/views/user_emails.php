<link

rel="stylesheet"

href="<?php echo base_url(); ?>assets/css/jquery-ui1.css" />



<style>

    ul.ui-autocomplete {

        z-index: 1100;

    }

    #progress-wrp {

        border: 1px solid #0099CC;

        padding: 1px;

        position: relative;

        border-radius: 3px;

        margin: 10px;

        text-align: left;

        background: #fff;

        box-shadow: inset 1px 3px 6px rgba(0, 0, 0, 0.12);

    }

    #progress-wrp .progress-bar {

        height: 20px;

        border-radius: 3px;

        background-color: #f39ac7;

        width: 0;

        box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);

    }

    #progress-wrp .status {

        top: 3px;

        left: 50%;

        position: absolute;

        display: inline-block;

        color: #000000;

    }

    li.attach_file_li {

        list-style: none;   

    }



    a#link {

        z-index: 999999 !important;

        position: relative !important;

        display: inline-block !important;

        width: 100% !important;

    }



    iframe#myiframe {

        z-index: -2 !important;

        position: relative;

        width: 100%;

    }



    a#link:before {

        position: absolute;

        content: "";

        width: 100%;

        height: 100%;

    }

</style>





<aside class="right-side">

    <!-- Content Header (Page header) -->

    <section class="content-header no-margin">

        <h1 class="text-center">

           <i class="fa fa-envelope" aria-hidden="true"></i> Mailbox

       </h1>

   </section>

   <section class="content weather-bg news_section">
    <!-- setting start -->
    <div class="col-md-12">

      <div id="msg_show"></div>

      <div class="col-md-10">
        <h3>Email</h3>

		<?php $current_user_id = $_SESSION['logged_in']['id'];
		if(!empty($btnsetingArray) && $btnsetingArray !=''){
         foreach($btnsetingArray as $res){
          $userid = $res->user_id;
          if($current_user_id == $userid){
            if($res->settings == 1) { ?>
            <div class="tooltip-2"><a data-btn="<?php echo 'email'?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
              <span class="tooltiptext">Active</span>
          </div>
      <?php }else { ?>
        <div class="tooltip-2 "><a data-btn="<?php echo 'email'?>"  href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
          <span class="tooltiptext">Deactive</span>
      </div>
  <?php } ?>
<?php } } }?>
</div>
</section>

<?php /* ?>

 <section class="content weather-bg news_section">

  <!-- setting start -->
  <div class="col-md-12">
      <div class="col-md-10">
          <h3>Email</h3>
          <?php if(!empty($btnsetingArray)){ ?>
            <?php if($btnsetingArray[0]->settings=='1'){ ?>

               <label><input type="radio" name="btnshow" value='1' checked="checked">Now social app showing on the mysoultab APP<br></label>

           <?php }else{ ?>

               <label> <input type="radio" name="btnshow" value='1' checked="checked">Now social app not showing on the mysoultab APP </label>

           <?php } ?>

       <?php } ?> 
   </div>
   <div class="col-md-2 delete-email">
    <button class="btn btn-primary chk">Settings</button>
</div> 
<div id="para" style="display: none "> 



    <form id="submit_form" method="post" action="<?php echo base_url()?>savsettings "> 
        <div class="col-md-10">
            <label> <input type="checkbox" name="butn_seting" id="butn_seting" value="">Please check if want to show the social button in mysoultab App </label>
            <input type="hidden" name="btn" value="email">
        </div>
        <div class="col-md-2">
           <input class="btn btn-primary chkdd" type="submit" name="submit" value="submit" >
       </div>
   </form>
   <div id="response"></div>
</div>

<!-- </div> -->

<!-- End -->
</section>  
<?php */ ?>

<!-- Main content -->

<section class="content">

    <!-- MAILBOX BEGIN -->

    <div class="mailbox row">

        <div class="col-xs-12">

            <div class="box box-solid">

                <div class="box-body box-bSwt">

                  <div id="expire_msg"></div>

                  <div class="row">

                    <div class="col-md-3 col-sm-4">

<!-- BOXES are complex enough to move the .box-header around.

   This is an example of having the box header within the box body -->



   <div class="box-header">

    <i class="fa fa-inbox"></i>

    <h3 class="box-title">INBOX</h3>

</div>

<!-- compose message btn -->

<a class="btn btn-block btn-primary" data-toggle="modal" data-target="#compose-modal"><i class="fa fa-pencil"></i> Compose Message</a>

<!-- Navigation - folders-->

<div style="margin-top: 15px;">

    <ul class="nav nav-pills nav-stacked">

       <li <?php if($menuactive == 'email_list'){echo 'class="active"';} ?>>

        <a href="email_list"><i class="fa fa-inbox"></i> Inbox </a></li>

        <li <?php if($menuactive == 'send_email_list'){echo 'class="active"';} ?>><a href="send_email_list"><i class="fa fa-mail-forward"></i> Sent</a></li>

        <li <?php if($menuactive == 'trash_email'){echo 'class="active"';} ?>><a href="trash_email"><i class="fa fa-trash-o" aria-hidden="true"></i> Trash</a></li>

    </ul>

</div>

</div><!-- /.col (LEFT) -->



<div class="col-md-9 col-sm-8">

    <div class="row pad swt_cor">

        <div class="col-sm-6">

            <label style="margin-right: 10px;">

                <input type="checkbox" id="checkall"/>

            </label>

            <div class="delete-email">

               <a href="#" name="btn_delete" id="btn_delete" class="btn_delete"><i aria-hidden="true" class="fa fa-trash-o"></i></a>

           </div>

       </div>    

   </div><!-- /.row -->

   <div class="table-responsive tb_swt separate_tble_cls">

    <?php /*?><div class="row pad"></div><?php */?>



    <div id="deletemsg"></div>

    <table class="table table-hover table-bordered table-mailbox send-user-mail" id="sampleTable">





        <thead>

            <tr>

                <th style="display: none;">Mail</th>

                <th style="display: none;">Username</th>

                <th style="display: none;">Email</th>

                <th style="display: none;">Mobile</th> 

            </tr>

        </thead>

        <tbody>

            <?php 



            // echo "<pre>"; print_r($user_data);
			if(!empty($user_data) && $user_data !=''){
            	foreach($user_data as $key => $user){
               
//    echo "<pre>"; print_r($user);


                $user_email = $this->Common_model_new->getsingle("cp_users",array("id" => $user['id']));

            


                ?>

                <tr data-href='#' id="detail_user_<?php echo $user['email_id']; ?>" data-row-id="<?php echo $user['email_id']; ?>">

                 <td class="small-col" ><input type="checkbox" name="sub_chk[]"  class="sub_chk" value="<?php echo $user['email_id']; ?>" data-id="<?php echo $user['email_id']; ?>"/></td>

                 <td width="20%" data-row-id="<?php echo $user['email_id']; ?>" class="short-discrp detail_user_<?php echo $user['email_id']; ?>"><?php //echo $user_email->email; ?></td>

                 <td><?php echo $user['subject']; ?></td>

                 <td style="text-align: right;width: 100px;"><?php echo $user['create_email']; ?></td> 

             </tr>

         <?php } ?>

		 <?php } ?>

    </tbody> 

 </table>

</div>  





</div>

</div>

</div>

</div>

</div>

</div>

</section>

</aside>

<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close close_reload" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Compose New Message </h4>

            </div>

            <div id="addmsg" style="text-align: center;"></div>



            <form enctype="multipart/form-data" id="modal_form_id"  method="POST" >    

                <!-- <form method="post" id="send_email" enctype="multipart/form-data"> -->

                    <div class="modal-body">

                        <div class="form-group">

                            <div class="input-group">

                                <span class="input-group-addon">To:</span>

                                <input name="email_to" id="email_to" type="text" class="form-control" placeholder="Enter your email">

                                <div id="suggesstion-box"></div>

                            </div>

                            <p id="emailerror" style="color: red"></p>

                            <p id="valideemailerror" style="color: red"></p>

                        </div>



                        <div class="form-group">

                            <div class="input-group">

                                <span class="input-group-addon">Subject:</span>

                                <input name="subject" id="subject" type="text" class="form-control" placeholder="Enter your subject">

                            </div>

                            <p id="subjecterror" style="color: red"></p>

                        </div>

                        <div class="form-group">

                            <textarea name="message" id="email_message" name="message" class="form-control" placeholder="Enter your description" style="height: 120px;"></textarea>



                            <p id="emailmsgerror" style="color: red"></p>

                        </div>

                        <div class="">

                            <!-- <input class="form-control" type="file" name="attachment_file" id="upload_file" /> -->



                            <input type="hidden" name="new_hidden" id="new_hidden" value="" >

                            <div class="btn btn-success btn-file">

                                <i class="fa fa-paperclip"></i> Attachment





                                <input class="form-control" type="file" multiple name="attachment_file[]" id="upload_file" onchange="showMyImage(this)" />





                            </div>

                            <div id="progress-wrp" style="display:none"><div class="progress-bar"></div ><div class="status">0%</div></div>

                            <div id="testing"></div>



                            <!-- <p class="help-block">Max. 32MB</p>-->

                        </div>



                    </div>

                    <div class="modal-footer clearfix">



                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-trash-o" aria-hidden="true"></i></button>



                        <button type="submit" class="btn btn-primary pull-left form-submit" name="submit" id="sendemail1">

                            <i class="fa fa-envelope"></i> Send Message</button>

                        </div>

                        <div id='loadingmessage' style='display:none'>

                            <img src='assets/img/loading.gif'/>

                        </div>

                    </form>

                </div><!-- /.modal-content -->

            </div><!-- /.modal-dialog -->

        </div><!-- /.modal -->





        <!------------------------detail mail-------------------- -->

        <div class="modal fade" id="detail_mail" tabindex="-1" role="dialog" aria-hidden="true">

            <div class="modal-dialog">

                <div class="modal-content">

                  <div id="getCode">

                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                        <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Compose New Message </h4>

                    </div>

                    <div id="addmsg" style="text-align: center;"></div>



                    <form enctype="multipart/form-data" id="getcode1"  method="POST" >    

                        <!-- <form method="post" id="send_email" enctype="multipart/form-data"> -->

                            <div class="modal-body">

                                <div class="form-group">

                                    <div class="input-group">

                                        <span class="input-group-addon">To:</span>

                                        <input name="email_to" id="email_to" type="email" class="form-control" placeholder="Enter your email">

                                    </div>

                                    <p id="emailerror" style="color: red"></p>

                                    <p id="valideemailerror" style="color: red"></p>

                                </div>



                                <div class="form-group">

                                    <div class="input-group">

                                        <span class="input-group-addon">Subject:</span>

                                        <input name="subject" id="subject" type="text" class="form-control" placeholder="Enter your subject">

                                    </div>

                                    <p id="subjecterror" style="color: red"></p>

                                </div>



                                <div class="form-group">

                                    <textarea name="message" id="email_message" name="message" class="form-control" placeholder="Enter your description" style="height: 120px;"></textarea>



                                    <p id="emailmsgerror" style="color: red"></p>

                                </div>

                                <div class="form-group">

                                    <!-- <input class="form-control" type="file" name="attachment_file" id="upload_file" /> -->



                                    <input type="hidden" name="new_hidden" id="new_hidden" value="" >

                                    <div class="btn btn-success btn-file">

                                        <i class="fa fa-paperclip"></i> Attachment





                                        <input class="form-control" type="file" multiple name="attachment_file[]" id="upload_file" onchange="showMyImage(this)" />





                                    </div>

                                    <div id="progress-wrp"><div class="progress-bar"></div ><div class="status">0%</div></div>

                                    <div id="testing"></div>



                                    <!-- <p class="help-block">Max. 32MB</p>-->

                                </div>



                            </div>

                            <div class="modal-footer clearfix">



                                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Discard</button>



                                <button type="submit" class="btn btn-primary pull-left form-submit" name="submit" id="sendemail1">

                                    <i class="fa fa-envelope"></i> Send Message</button>

                                </div>

                                <div id='loadingmessage' style='display:none'>

                                    <img src='assets/img/loading.gif'/>

                                </div>

                            </form>

                        </div>

                    </div><!-- /.modal-content -->

                </div><!-- /.modal-dialog -->

            </div>



            <!------------------------------------------- -->



            <div class="modal fade" id="deleteModal" role="dialog">

                <div class="modal-dialog modal-sm">

                    <div class="modal-content">

                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                            <h4 class="modal-title">Delete</h4>

                        </div>

                        <div class="modal-body">

                            <h3>Are you sure ? You want to delete this</h3>

                        </div>

                        <div class="modal-footer">

                            <button type="button" class="btn btn-primary" id="ok" data-dismiss="modal">Ok</button>

                            <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>

                        </div>

                    </div>

                </div>

            </div>

            <!-- jQuery 2.0.2 -->

            <script src="<?php echo base_url(); ?>assets/user_dashboard/js/jquery.min.js"></script>

            <!-- jQuery UI 1.10.3 -->

            <script src="<?php echo base_url(); ?>assets/user_dashboard/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>

            <script>

                time = 5;

                document.getElementById("counter").innerHTML = time;

                timer = setInterval("count()",1000);



                function count()

                {

                 if(time == 1)

                 {

  //Cleare Timer and remove counter from page

  clearInterval(timer);

  //document.getElementById("counter-text").style.display="none";

  //If you want to display link

  //document.getElementById("link").style.display="block";

  //Or directly start download

  //download('file.ext');

}

else

{

  time--;

  document.getElementById("counter").innerHTML = time;

}

}

function download(file)

{

 window.location=file;

}

</script>

<script type="text/javascript"> 



 $(document).on('click','.short-discrp', function(e){ 

  var email_id = $(this).attr('data-row-id');



  $.ajax({   

      type: "POST",  

      url: "<?php echo base_url().'Pages/Attachment/detail_email'?>",  

      cache:false,  

      data: 'email_id='+email_id,  

      success: function(response)   

      {   

          $("#getCode").html(response);

          $("#detail_mail").modal('show');  



      }   

  });

});



// $(function() {

//   $("#send_email").validate({

//     // Specify validation rules

//     rules: {

//       email_to: {

//         required: true,

//         email: true

//       },

//       subject: {

//         required: true

//       },

//       email_message: {

//         required: true

//       }

//     },

//     // Specify validation error messages

//     messages: {

//       //email: "Enter your email"

//       email: {

//         required: "Please enter email",

//         email: "Please enter valid email"

//       },

//       subject: {

//         required: "Please enter your subject",

//       },

//       email_message: {

//         required: "Please enter your text",

//       },

//     },

//     // Make sure the form is submitted to the destination defined

//     // in the "action" attribute of the form when valid

//     submitHandler: function(form) {

//       form.submit();

//     }

//   });

// });

</script>

<script type="text/javascript">

    $(document).ready(function(){



      $("#modal_form_id").validate({

        debug: true,

        rules: {

            email_to: {

                required: true,

                    //email: true,

                },

                subject:{

                    required: true,                    

                },

                message:{

                 required: true,                    

             },

         },



           /* submitHandler: function () {

			

	var formDatas = new FormData($("#modal_form_id")[0]);

				$.ajax({

    type: "POST",

    url: "<?php echo base_url().'Pages/Attachment/sendemail'?>",

   // data: fd,

  data: formDatas,

    contentType: false,

    processData: false,

    cache: false,

    success: function(result){

		

        $('#loadingmessage').hide();

        $("#addmsg").html("<div class='alert alert-success'>Email send successfully</div>");

        setTimeout(function(){

			$("#video_loader").hide();

			$("#new_loading_img").hide();

            $("#addmsg").html(""); 

            location.reload();

//window.location.href = email_url+"email_list";

}, 1000);



    }

});

}*/

});



      $(document).on('submit','#modal_form_id', function(e){



       var isvalidate=$("#modal_form_id").valid();

       if(!isvalidate)

       {

        e.preventDefault();

        alert("invalid");

    }else{

       $('#loadingmessage').show();

       var formDatas = new FormData($("#modal_form_id")[0]);

       $.ajax({

        type: "POST",

        url: "<?php echo base_url().'Pages/Attachment/sendemail'?>",

   // data: fd,

   data: formDatas,

   contentType: false,

   processData: false,

   cache: false,

   success: function(result){



    $('#loadingmessage').hide();

    if(result == '3'){

      $("#addmsg").html("<div class='alert alert-danger'>User not registered</div>");	

  }else{

    $("#addmsg").html("<div class='alert alert-success'>Email sent successfully</div>");

    setTimeout(function(){

       $("#video_loader").hide();

       $("#new_loading_img").hide();

       $("#addmsg").html(""); 

       location.reload();

//window.location.href = email_url+"email_list";

}, 1000);

}

}

});



   }			



});



       // $("#sendemail1").click(function(){

		  /* $(document).on('submit','#modal_form_id', function(e){



            var email_to = $.trim($('#email_to').val());



            var subject = $.trim($('#subject').val());



            var email_message = $.trim($('#email_message').val());



            var matches = email_to.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

           //console.log(matches);



            if(email_to==='' ){



                var msg="Please enter email address";

                document.getElementById("emailerror").innerHTML = msg;



                return false;





            }else if(matches==null){



                var msg="Please enter valid email.";

                document.getElementById("valideemailerror").innerHTML = msg;



                return false;





            }else if(subject===''){



                var msg="Please enter subject";

                document.getElementById("subjecterror").innerHTML = msg;



                return false;





            }else if(email_message===''){





                var msg="Please write somthing in the message";

                document.getElementById("emailmsgerror").innerHTML = msg;



                return false;





            }else{



                var fd = new FormData();

                var file_data = $('input[type="file"]')[0].files; 

                for(var i = 0;i<file_data.length;i++){

                    fd.append("file_"+i, file_data[i]);

                }

                var other_data = $('form').serializeArray();





                $.each(other_data,function(key,input){

                    fd.append(input.name,input.value);

                });





                $('#loadingmessage').show();

                if(email_to==''||subject==''||email_message=='')

                {

                    alert("Please Fill All Fields");

                }

                else

                {

// AJAX Code To Submit Form.

$.ajax({

    type: "POST",

    url: "<?php echo base_url().'Pages/Attachment/sendemail'?>",

   // data: fd,

  data: new FormData(this),

    contentType: false,

    processData: false,

    cache: false,

    success: function(result){

		

        $('#loadingmessage').hide();

        $("#addmsg").html("Email send successfully");

        setTimeout(function(){

            $("#addmsg").html("");

            location.reload();

//window.location.href = email_url+"email_list";

}, 1000);



    }

});

}

return false;



}

});*/

});





//     $("#sendemail1").click(function(){

//     var email_url =  "<?php //echo base_url()?>";

//     var email_to = $("#email_to").val();

//     var subject = $("#subject").val();

//     var email_message = $("#email_message").val();

//     var dataString = 'email_to='+ email_to + '&subject='+ subject + '&email_message='+ email_message;

//     $('#loadingmessage').show();

//     if(email_to==''||subject==''||email_message=='')

//     {

//      alert("Please Fill All Fields");

//     }

//     else

//     {

//     // AJAX Code To Submit Form.

//     $.ajax({

//     type: "POST",

//     url: "<?php //echo base_url().'sendemail'?>",

//     data: dataString,

//     cache: false,

//     success: function(result){

//         $('#loadingmessage').hide();

//     $("#addmsg").html("Email send successfully");

//           setTimeout(function(){

//               $("#addmsg").html("");

//                 window.location.href = email_url+"email_list";

//     }, 1000);



//     }

//     });

//     }

//     return false;

//     });

// });

</script>

<script type="text/javascript">



  /*  jQuery('.btn_delete').on('click', function(e) { 

        var allVals = [];  



        $(".sub_chk:checked").each(function() {  

            allVals.push($(this).attr('data-id'));

        });  

//alert(allVals.length); return false;  

if(allVals.length <=0)  

{  

    alert("Please select row.");  

}  

else {  

//$("#loading").show(); 

WRN_PROFILE_DELETE = "Are you sure you want to delete this email?";  

var check = confirm(WRN_PROFILE_DELETE);  

if(check == true){  

//for server side

var join_selected_values = allVals.join(","); 

$.ajax({   

    type: "POST",  

    url: "<?php echo base_url().'delete_emails'?>",  

    cache:false,  

    data: 'ids='+join_selected_values,  

    success: function(response)  

    {   

        $("#loading").hide();  

        $("#deletemsg").html("Email delete successfully");

//referesh table

}   

});

//for client side

$.each(allVals, function( index, value ) {

    $('tbody tr').filter("[data-row-id='" + value + "']").remove();

});

}  

}  

});*/



$(document).on('click','.btn_delete', function(e){



  var email_id = $(this).attr('data-id');

  var segment = '<?php echo $this->uri->segment('1'); ?>';



  var bodytype = $('input[name="sub_chk[]"]:checked').map(function () {  

    return this.value;

}).get().join(",");

  if(bodytype != ''){

     if(confirm("Are you sure, you want to remove this mail?")){	

      $.ajax({

        url: '<?php echo base_url(); ?>Pages/Attachment/delete_checkboxes',

        type: 'POST',

        data: {bodytype:bodytype,email_id:email_id,segment:segment},

        dataType: "json",

                success: function(data) { //alert('swati');





                setTimeout(function(){

                    $("#expire_msg").html("<div class='alert alert-success'>Successfully deleted !</div>");

                    location.reload();

//window.location.href = email_url+"email_list";

}, 1000);

            }

        });



  }

}else{

   alert("Please select atleast one mail.");

}

});





jQuery('#checkall').on('click', function(e) {

    if($(this).is(':checked',true))  

    {

        $(".sub_chk").prop('checked', true);  

    }  

    else  

    {  

        $(".sub_chk").prop('checked',false);  

    }  

});

jQuery('.sub_chk').on('click',function(){

    if($('.sub_chk:checked').length == $('.sub_chk').length){

        $('#checkall').prop('checked',true);

    }else{

        $('#checkall').prop('checked',false);

    }

});



	/*$(document).ready(function(){

	$(document).on('change','input[name^="attachment_file"]', function(e){	

		$('#modal_form_id').submit();

	});

});	

	

	$(document).ready(function() {

	

		  $("#modal_form_id").on('submit',(function(e) {

e.preventDefault();

$("#message").empty();

$('#loading').show();

$(this).append('<div id="video_loader" class="loading_class"><img id="new_loading_img" style="width:20px" src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif" /></div>');

$.ajax({

url: "<?php echo base_url(); ?>Pages/Attachment/multiple_image_upload", 

type: "POST",   

 dataType: "json",       

data: new FormData(this),

contentType: false,       

cache: false,

             

processData:false,        

success: function(data)   

{

	$("#new_loading_img").remove();

	

	 $("#video_loader").remove();

	$("#testing").html(data.testing);

}

});

}));

});*/



$(document).on('click','[id^="removeImg-"]',function(){

  var attachment_id = $(this).attr('data-attach-id');

  $.ajax({

    url: '<?php echo base_url(); ?>Pages/Attachment/removeImg',

    type: 'POST',

    data: {attachment_id:attachment_id},

				//dataType: "json",

                success: function(data) { //alert('swati');





                $('#delete_imgs').hide();

                $("#testing").html(data);

            }

        });

});



$(document).ready(function(){

	var progress_bar_id 		= '#progress-wrp';

    $('#upload_file').change(function(e){

      var num_of_images = $("input[type='file']")[0].files.length;

      if(num_of_images <= 5){ 	

       $('#progress-wrp').show();

			//$('.attach_file_li').append('<div id="video_loaders" class="loading_class"><img id="new_loading_img" style="width:20px" src="<?php echo base_url(); ?>assets/images/ajax-loader-blue.gif" /></div>');

			

            $(progress_bar_id +" .progress-bar").css("width", "0%");

            $(progress_bar_id + " .status").text("0%");

           // var file = this.files[0];



           var form = new FormData();

           $.each($("input[type='file']")[0].files, function(i, file) {

            form.append('attachment_file[]', file);



        });





           // form.append('upload_video', file);

			//form.append('hidden_id', hidden_id);

            $.ajax({

                url : "<?php echo base_url(); ?>Pages/Attachment/multiple_image_upload",

                type: "POST",

                cache: false,

                contentType: false,

                processData: false,

                data : form,

                dataType: "json",

                xhr: function(){

		//upload Progress

		var xhr = $.ajaxSettings.xhr();

		if (xhr.upload) {

			xhr.upload.addEventListener('progress', function(event) {

				var percent = 0;

				var position = event.loaded || event.position;

				var total = event.total;

				if (event.lengthComputable) {

					percent = Math.ceil(position / total * 100);

				}

				//update progressbar

				$(progress_bar_id +" .progress-bar").css("width", + percent +"%");

				$(progress_bar_id + " .status").text(percent +"%");

			}, true);

		}

		return xhr;

	},

    success: function(data){                 



     $("#new_loading_img").remove();



     $("#progress-wrp").hide();	 

     $("#video_loader").remove(); 

     $("#testing").html(data.testing);

 }

});

        }else{ 

            alert('Please select only 5 attachment');

        }

    });

});





$( function() {



  var availableTags = [<?php echo $test; ?>];

  //alert(availableTags);

  function split( val ) { 

   return val.split( /,\s*/ );

}

function extractLast( term ) { 

   return split( term ).pop();

}



$( "#email_to" )

			// don't navigate away from the field on tab when selecting an item

			.on( "keydown", function( event ) {

				if ( event.keyCode === $.ui.keyCode.TAB &&

                  $( this ).autocomplete( "instance" ).menu.active ) {

					event.preventDefault();

            }

        })

			.autocomplete({

			//	minLength: 0,

            source: function( request, response ) { 

					// delegate back to autocomplete, but extract the last term

					response( $.ui.autocomplete.filter(

						availableTags, extractLast( request.term ) ) );

				},

				focus: function() {

					// prevent value inserted on focus

					return false;

				},

				select: function( event, ui ) {

					var terms = split( this.value );

					

					// remove the current input

					terms.pop();

					// add the selected item

					terms.push( ui.item.value );

					// add placeholder to get the comma-and-space at the end

					terms.push( "" );

					this.value = terms.join( ", " );

					return false;

				}

			});

     } );





$(document).on('click','.close_reload',function(){

  location.reload();

});

</script>



<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>



<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).on('click','.updateStatus',function(){
        var update_status = $(this).attr('data-btn');
        var update_st = $(this).attr('data-st');
    //alert(update_status+'-'+update_st);
    $.ajax({
      url: '<?php echo base_url();?>Pages/pages/change_button_status',
      type: "POST",
      data: {'btn':update_status},
      dataType: "json",
      success:function(data){
        setTimeout(function(){
          $("#msg_show").html("<div class='alert alert-success'>"+data.status+"</div>");
          location.reload();

      }, 1500);
    }
}); 
});
</script>
