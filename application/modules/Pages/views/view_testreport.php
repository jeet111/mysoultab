<style>

span.testo {

    word-break: break-all;

}
.box-footer a:nth-child(2) {
    max-width: 167px !important;
}
</style>

<div class=" text_heading">

    <h1 class="text-center">

      <!-- <i class="fa fa-paper-plane"></i>-->



      <i class="fa fa-file-text" aria-hidden="true"></i>

      Test Report Detail

      <div class="back_reminder"><a href="<?php echo base_url(); ?>testreport_list" class="btn btn-danger">Back</a></div>

    </h1>

  </div>



  <?php //echo "<pre>"; print_r($TestReportView); echo "</pre>"; ?>

  <section class="content photo-list">

    <div class="photo-listMain">

	    <form id="detail_acknowledge" method="post" class="frm_add">

        <div class="row">

          <div class="col-md-6">                        

            <div class="form-group">

              <div class="bx_med">Test Report Title:</div>

              <div class="bx_meSec"><?php echo $TestReportView->test_report_title; ?></div>  

            </div>                                  

          </div>

          <div class="col-md-6">                        

            <div class="form-group">

              <div class="bx_med">Doctor Name:</div>

              <div class="bx_meSec"><?php echo $TestReportView->doctor_name; ?></div>  

            </div>                                  

          </div>





        </div>





        <div class="row">

          

          <div class="col-md-6">                        

            <div class="form-group">

              <div class="bx_med">Test Type:</div>

              <div class="bx_meSec"><?php echo $TestReportView->test_type; ?></div>

            </div>                                  

          </div>



          <div class="col-md-6">

            <div class="form-group">

                <div class="bx_med">Description:</div>

                <div class="bx_meSec"><span class="testo"><?php echo $TestReportView->description; ?></span></div>

            </div>              

          </div>





        </div>

        <div class="row">

          

          <div class="col-md-6">

            <div class="form-group">

              <div class="bx_med">Report File:</div>

              <div class="bx_meSec">



                <?php if($TestReportView->image==''){ ?>

                  N.A.



                <?php }else{

$ext = explode('.',$TestReportView->image); ?>



				

<?php if($ext[1]=="pdf" ){ ?>

		<a target="_blank" href="<?php echo base_url(); ?>uploads/test_report/<?php echo $TestReportView->image; ?>"><?php echo $TestReportView->image; ?></a>

	<?php  }elseif($ext[1]=="docx" || $ext[1]=="csv"){ ?>

		<a href="<?php echo base_url(); ?>uploads/test_report/<?php echo $TestReportView->image; ?>"><?php echo $TestReportView->image; ?>

		</a>	

	 <?php }

	  elseif($ext[1]=="jpg" || $ext[1]=="png" || $ext[1]=="jpeg" || $ext[1]=="gif"){ ?>

		<img height="50px" width="50px"  src="<?php echo base_url().'uploads/test_report/'.$TestReportView->image; ?>">

		   

		  

	   <?php }elseif($ext[1]=="txt"){ ?>

		  <a href="<?php echo base_url().'uploads/test_report/'.$TestReportView->image; ?>" download><?php echo $TestReportView->image; ?></a>

		  

	  <?php }} ?>				

				

				

                

                </div>

            </div>

          </div>

        </div>

       <!-- <div class="row">

          

          <div class="col-md-6">

              <div class="form-group">

                  <div class="bx_med">Acknowledge Status:</div>

                  <div class="bx_meSec">Done</div>

              </div>

          </div>

        </div>-->

        <div class="box-footer">

            <a href="<?php echo base_url();?>edit_testreport/<?php echo $TestReportView->report_id;?>" class="btn btn-primary" >Edit</a>            

			<a href="<?php echo base_url(); ?>send_testreport/<?php echo $TestReportView->report_id;?>" class="btn btn-primary"  class="edit_btn">Send To Caregiver</a>

        </div>

      </form>           

    </div>

  </section>

</aside>

<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>  

<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>





