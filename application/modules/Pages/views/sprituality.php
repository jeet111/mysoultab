    <style>

.text_heading {
        border-bottom: 0;
    }
    </style><div class="row">
        <div class="col-md-12">
            <div id="msg_show"></div>
            <div class=" text_heading">
                <h3><i class='fas fa-atom' aria-hidden="true"></i> Sprituality</h3>
                <div class="tooltip-2">
                    <h2>Display Spirituality button on Mobile App

                    <?php
                    foreach ($btnsetingArray as $res) {
                        if ($res->settings == 1) { ?>
                            <a data-btn="<?php echo 'sprituality' ?>" href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>

                </div>
            <?php } else { ?>
                <a data-btn="<?php echo 'sprituality' ?>" href="javascript:void(0);" class="updateStatus"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>


            <?php } ?>
        <?php }
        ?>
        </h2>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>

    <script type="text/javascript">
        $(document).on('click', '.updateStatus', function() {
            var update_status = $(this).attr('data-btn');
            var update_st = $(this).attr('data-st');
            //alert(update_status+'-'+update_st);
            $.ajax({
                url: '<?php echo base_url(); ?>Pages/pages/change_button_status',
                type: "POST",
                data: {
                    'btn': update_status
                },
                dataType: "json",
                success: function(data) {
                    setTimeout(function() {
                        $("#msg_show").html("<div class='alert alert-success'>" + data.status + "</div>");
                        location.reload();

                    }, 1500);
                }
            });
        });
    </script>