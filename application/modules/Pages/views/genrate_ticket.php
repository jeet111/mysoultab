<!-- jQuery UI 1.10.3 -->



<aside class="right-side">    

    <section class="content-header no-margin">

        <h1 class="text-center"><i class="fa fa-ticket" style="font-size:48px;color:red"></i> Genrate Ticket

            <div class="back_reminder"><a href="<?php echo base_url(); ?>all_tickets" class="btn btn-danger">Back</a></div>

        </h1>

    </section>    

    <section class="content photo-list">

        <div class="photo-listMain">            

           <form id="addressform">



            <div id="success_message"></div>



                    <?php /* ?>

                <?php if ($this->session->flashdata('success')) { ?>

                    <div class="alert alert-success message">

                        <button type="button" class="close" data-dismiss="alert">x</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                <?php } ?>			



                <?php */?>

               <!--  <div class="row"> 

                    <div class="col-sm-4 col-md-4">

                        <div class="form-group">

                            <label for="">Title:<span class="red_star">*</span></label>

                            <input type="text" class="form-control" id="reminder_title" name="reminder_title" value="" placeholder="Reminder Title">

                            <td class="error"><?php echo form_error('reminder_title'); ?></td>

                        </div>

                    </div>

                    <div class="col-sm-4 col-md-4">

                        <div class="bootstrap-timepicker">

                            <div class="form-group">

                                <label>Date:<span class="red_star">*</span></label>

                                <div class="input-group date costom_err" id="datetimepicker">  



                                    <input type='text' class="form-control" readonly name="reminder_date" value="" placeholder="Reminder Date" />

                                    <span class="input-group-addon">

                                      <span class="glyphicon glyphicon-calendar"></span>

                                  </span> 

                                  <td class="error"><?php echo form_error('reminder_date'); ?></td>

                              </div>

                          </div>

                      </div>             

                  </div>

                  <div class="col-sm-4 col-md-4">

                    <div class="bootstrap-timepicker">

                        <div class="form-group">

                            <label>Time:<span class="red_star">*</span></label>

                            <div class="input-group" id='datetimepicker3'>



                                <input type='text' data-format="hh:mm:ss" name="reminder_time"  class="form-control" value="" placeholder="Reminder Time" />

                                <span class="input-group-addon add-on">

                                    <span class="glyphicon glyphicon-time"></span>

                                </span>

                            </div>

                            <td class="error"><?php echo form_error('reminder_time'); ?></td>

                        </div>

                    </div>             

                </div>                   

            </div>



            <div class="row">

                <div class="col-sm-4 col-md-6">

                    <div class="form-group">

                        <label>Remind Before:<span class="red_star">*</span></label>

                        <select name="before_time" class="form-control  ">

                            <option value="">Select Before Time</option>

                            <option value="5" >5 Minutes</option>

                            <option value="10" >10 Minutes</option>

                            <option value="15" >15 Minutes</option>

                            <option value="20" >20 Minutes</option>

                        </select>

                        <td class="error"><?php echo form_error('reminder_date'); ?></td>

                    </div>

                </div>

                <div class="col-sm-4 col-md-6">

                    <div class="form-group">

                        <label>Repeat Reminder:<span class="red_star">*</span></label>

                        <select name="repeat_time" class="form-control  ">

                            <option value="">Select Repeat Time</option>

                            <option value="Daily" >Daily Basis</option>

                            <option value="Weekly" >Weekly Basis</option>

                            <option value="Monthly" >Monthly Basis</option>

                            <option value="Yearly" >Yearly Basis</option>

                            <option value="Never" >Never</option>

                        </select>

                        <td class="error"><?php echo form_error('reminder_date'); ?></td>

                    </div>

                </div>



            </div> -->



            <div class="row">

                <div class="col-md-12">

                    <div class="form-group">

                        <label for="">Email Address:<span class="red_star">*</span></label>

                        <input type="text" class="form-control"  name="email" id="email" placeholder="Enter email address">

                        

                        <td class="error"><?php echo form_error('email'); ?></td>                            

                    </div>

                </div>                  

            </div>





            <div class="row">

                <div class="col-md-12">

                    <div class="form-group">

                        <label for="">Message:<span class="red_star">*</span></label>

                        <textarea class="form-control" id="message" name="message"  placeholder="Description"></textarea>





                        <td class="error"><?php echo form_error('message'); ?></td>                            

                    </div>

                </div>					

            </div>

            <div class="box-footerOne">

                <input type="submit" name="submit" class="btn btn-primary" value="Submit">

            </div>               

        </form>              

    </div>

</section>

</aside>



<!--Added by 95 on 5-2-2019 For Doctor category for validation-->

<script src="<?php echo base_url(); ?>assets/js/jquery.minn.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>





<script type="text/javascript">

    $('#addressform').validate({

        rules: {

            message: "required",

            email:{required:true,email:true,},

            // reminder_date:"required",

            // reminder_time:"required",

            // before_time:"required",

            // repeat_time:"required",

        },

        messages: {

            //reminder_title: "Please enter reminder title.",

            message:"Please describe your issue.",

            email:{

                required:"Please enter your email.",email:"Please enter a valid email address.",

            },

            //email:"Please enter email address.",

            // reminder_time:"Please select reminder time.",

            // before_time:"Please select remind before time.",

            // repeat_time:"Please select repeat reminder time.",

        },

        submitHandler: function(form){

            $.ajax({

                url: '<?php echo base_url();?>Pages/Tickets/genrateTicket',

                type: 'post',

                data: $(form).serialize(),

                success: function(data) {

                   returnData = JSON.parse(data);

                   console.log(returnData.response);

                   if(returnData.response==1){

                    //console.log(response);

                    $('#success_message').fadeIn().html('<div class="alert alert-success message"><button type="button" class="close" data-dismiss="alert">x</button>Ticket genrated successfully '+returnData.ticket_id+'.</div>');

                    setTimeout(function() {

                        $('#success_message').fadeOut();

                    }, 5000 );

                }

                location.href = '<?php echo base_url();?>all_tickets';



            }            

        });

        }

    });



</script>





<script type="text/javascript">

   $(function () {

       var today = new Date();	 

       $('#datetimepicker').datetimepicker({

        ignoreReadonly: true,

        format: 'YYYY-MM-DD', 			   

        minDate: today

    });

   });  

</script>







<script src="<?php echo base_url(); ?>assets/js/jquery.minnn.js"></script>



<script src="<?php echo base_url(); ?>assets/user_dashboard/js/AdminLTE/app_without_checkbox.js" type="text/javascript"></script>























